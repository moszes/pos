<?php

use App\Models\ApplicationSetting;
use App\Models\Branch;
use App\Models\Category;
use App\Models\PackageItem;
use App\Models\UnitOfMeasure;
use App\Models\UserTransaction;
use Illuminate\Database\Eloquent\Collection;
use Spatie\Activitylog\Models\Activity;
use Spatie\Permission\Models\Permission;

if (! function_exists('getSetting')) {
    /**
     * Get all branches.
     *
     * @param $name
     * @param $module
     * @return bool
     */
    function getSetting($name, $module): bool
    {
        return ApplicationSetting::where('name', $name)
            ->where('module', $module)
            ->where('status', 'Y')
            ->first() != null;
    }
}

if (! function_exists('generateBranchCode')) {
    /**
     * Generate branch code.
     */
    function generateBranchCode(): string
    {
        $branch = Branch::latest()->first();
        $code = $branch ? $branch->code : '';

        return 'BR'.str_pad(
            (int) substr($code, 2, 3) + 1,
            3,
            '0',
            STR_PAD_LEFT
        );
    }
}

if (! function_exists('loggedInUser')) {
    /**
     * Get the ID of the currently logged-in user.
     *
     * @return int|null The ID of the logged-in user, or null if no user is logged in.
     */
    function loggedInUser(): ?int
    {
        // Check if a user is authenticated
        if (auth()->check()) {
            // Return the ID of the authenticated user
            return auth()->user()?->id;
        }

        // No user is logged in
        return null;
    }
}

if (! function_exists('getSellPrice')) {
    /**
     * Retrieve the sell price for a user transaction.
     *
     * @param  int  $userTransactionId  The ID of the user transaction.
     * @return float The sell price of the package item, or 0 if not found.
     */
    function getSellPrice(int $userTransactionId): float
    {
        // Find the user transaction by ID
        $userTransaction = UserTransaction::find($userTransactionId);

        // Get the package ID and treatment ID from the user transaction
        $packageId = $userTransaction->reference_id;
        $treatmentId = $userTransaction->module_id;

        // Retrieve the package item based on package ID and treatment ID
        $packageItem = PackageItem::where('package_id', $packageId)
            ->where('module', 'treatment')
            ->where('module_id', $treatmentId)->first();

        // Initialize the sell price to 0
        $sellPrice = 0;

        // If the package item is found, update the sell price
        if ($packageItem) {
            $sellPrice = $packageItem->sell_price;
        }

        return $sellPrice ?? 0;
    }
}

if (! function_exists('removeArrayKeys')) {
    /**
     * Remove specified keys from the given array.
     *
     * @param  array  $myArray  The array from which keys need to be removed.
     * @param  array  $arrayKeyToRemove  The keys to be removed from the array.
     * @return array The array after removing the specified keys.
     */
    function removeArrayKeys(array $myArray = [], array $arrayKeyToRemove = []): array
    {
        // Loop through the keys to remove
        foreach ($arrayKeyToRemove as $key) {
            // Check if the key exists in the array and unset it
            if (array_key_exists($key, $myArray)) {
                unset($myArray[$key]);
            }
        }

        return $myArray;
    }
}

if (! function_exists('getDataByModule')) {
    /**
     * Retrieve data from a module by its ID.
     *
     * @param  string  $module  The name of the module.
     * @param  int  $id  The ID of the data.
     * @return mixed The retrieved data or null if not found.
     */
    function getDataByModule(string $module, int $id): mixed
    {
        // Check if the module is 'marketplace' and update it to 'MarketPlace'
        if ($module == 'marketplace') {
            $module = 'MarketPlace';
        }

        // Create the model class name using the module name
        $modelClass = 'App\\Models\\'.ucfirst($module);

        // Instantiate the model class and find the data by ID
        $model = new $modelClass;

        return $model->find($id);
    }
}

if (! function_exists('getDefaultPromo')) {
    /**
     * Get the default promo based on the quantity.
     *
     * This function determines the default promo code based on the quantity provided.
     *
     * @param  int  $qty  The quantity for which to determine the default promo.
     * @return string|null The default promo code or null of none.
     */
    function getDefaultPromo(int $qty): ?string
    {
        // Return 'B10' if quantity is 10 or more, 'B5' if quantity is 5 or more, otherwise null
        return $qty >= 10 ? 'B10' : ($qty >= 5 ? 'B5' : 'B1');
    }
}

if (! function_exists('getRandomDefaultPromo')) {

    /**
     * Retrieves a random default promo from the available promo options.
     *
     * @return string|null The random default promo.
     */
    function getRandomDefaultPromo(): ?string
    {
        $promos = ['B1', 'B5', 'B10'];

        return $promos[array_rand($promos)];
    }
}

if (! function_exists('getActivity')) {
    /**
     * Retrieve all activities from the database.
     *
     * @return \Illuminate\Support\Collection|null The collection of activities, or null if none found.
     */
    function getActivity(): ?Collection
    {
        // Retrieve all activities from the database
        $activities = Activity::all();

        // Return the collection of activities if it is not empty, otherwise return null
        return $activities->isEmpty() ? null : $activities;
    }
}

if (! function_exists('getPermissions')) {
    /**
     * Get the permissions from the database.
     *
     * @return \Illuminate\Support\Collection|null The collection of permissions or null if empty.
     */
    function getPermissions(): ?\Illuminate\Support\Collection
    {
        // Retrieve all permissions from the database
        $permissions = Permission::all();

        // Return the collection of permission names if it is not empty, otherwise return null
        return $permissions->isEmpty() ? null : $permissions->pluck('name');
    }
}

if (! function_exists('getApplicationVersion')) {
    /**
     * Retrieves the version of the application from the Git repository.
     *
     * @return string The version of the application.
     *
     * @throws Exception If there is an error executing the Git commands.
     */
    function getApplicationVersion(): string
    {
        $packageJson = json_decode(file_get_contents(base_path('package.json')), true);

        return $packageJson['version'];
    }
}

if (! function_exists('getUnitOfMeasureOptions')) {
    /**
     * Get the options for a specific category type.
     *
     * @return string The collection of category options.
     */
    function getUnitOfMeasureOptions(): string
    {
        // Get all the categories
        $units = UnitOfMeasure::all();

        // Check if there are no categories
        if ($units->isEmpty()) {
            return '';
        }

        // Generate the option HTML for each category and combine them into one string
        return implode('', $units->map(function ($unit) {
            return '<option value="'.$unit->id.'">'.$unit->name.'</option>';
        })->toArray());
    }
}

if (! function_exists('getUnitOfMeasureById')) {
    /**
     * Get the unit of measure by its ID.
     *
     * @param  ?int  $id  The ID of measurement is a unit of measurement.
     * @return string|null The unit of measure.
     */
    function getUnitOfMeasureById(?int $id): ?string
    {
        return UnitOfMeasure::find($id);
    }
}

if (! function_exists('generateCategoryCode')) {
    /**
     * Generate a unique category code.
     *
     * @return string The generated category code.
     */
    function generateCategoryCode(): string
    {
        return 'CAT'.str_pad(
            Category::withTrashed()->count() + 1,
            10,
            '0',
            STR_PAD_LEFT
        );
    }
}