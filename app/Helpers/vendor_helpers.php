<?php

use App\Models\Vendor;

/*
|--------------------------------------------------------------------------
| Vendor Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Vendor Module
|
*/

if (! function_exists('generateVendorCode')) {
    /**
     * Generate a unique vendor code.
     *
     * This function generates a unique vendor code by counting the number of vendors
     * (including soft deleted ones) and padding it with zeros to a total length of 10.
     *
     * @return string The generated vendor code.
     */
    function generateVendorCode(): string
    {
        // Count the number of vendors (including soft deleted ones)
        $vendorCount = Vendor::withTrashed()->count();

        // Generate the vendor code by padding the count with zeros
        return 'VDR'.str_pad($vendorCount + 1, 10, '0', STR_PAD_LEFT);
    }
}

if (! function_exists('getFirstVendor')) {
    /**
     * Retrieve the first vendor from the database.
     *
     * @return Vendor|null The first vendor object, or null if no vendor exists.
     */
    function getFirstVendor(): ?Vendor
    {
        return Vendor::first();
    }
}

if (! function_exists('getVendorById')) {
    /**
     * Retrieves a vendor by its ID.
     *
     * @param  int  $id  The ID of the vendor.
     * @return Vendor|null The vendor object if found, null otherwise.
     */
    function getVendorById(int $id): ?Vendor
    {
        // Use the Vendor model to find the vendor by ID
        return Vendor::find($id);
    }
}

if (! function_exists('getVendorOptions')) {
    /**
     * Get the HTML options for the vendor select dropdown.
     *
     * @return string The HTML options for the vendor select dropdown.
     */
    function getVendorOptions(): string
    {
        // Initialize the variable to hold the HTML options
        $vendorOption = '';

        // Retrieve all vendors from the database
        $vendors = Vendor::all();

        // Loop through each vendor and append the HTML option to the variable
        foreach ($vendors as $vendor) {
            $vendorOption .= '<option value="'.$vendor->id.'">'.$vendor->name.'</option>';
        }

        // Return the HTML options if there are vendors, otherwise return an empty string
        return $vendors->isEmpty() ? '' : $vendorOption;
    }
}
