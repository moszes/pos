<?php

/*
|--------------------------------------------------------------------------
| Treatment Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Treatment Module
|
*/

use App\Models\Treatment;

if (! function_exists('generateTreatmentCode')) {
    /**
     * Generates a unique treatment code.
     */
    function generateTreatmentCode(): string
    {
        // Retrieve the count of all treatments including soft deleted ones and increment by 1
        return str_pad('SVC-'.Treatment::withTrashed()->count() + 1, 5, '0', STR_PAD_LEFT);
    }
}

if (! function_exists('getFirstTreatment')) {
    /**
     * Retrieve the first treatment from the database.
     *
     * @return Treatment The first treatment found.
     */
    function getFirstTreatment(): Treatment
    {
        return Treatment::first();
    }
}

if (! function_exists('getTreatmentById')) {
    /**
     * Retrieve a treatment by its ID.
     *
     * @param  int  $id  The ID of the treatment to retrieve.
     * @return Treatment|array|null The retrieved treatment or null if not found.
     */
    function getTreatmentById(int $id): Treatment|array|null
    {
        return Treatment::find($id);
    }
}

if (! function_exists('getTreatmentFee')) {
    /**
     * Retrieve the fee for a given marketplace and role.
     *
     * @param  int  $treatmentId  The ID of the marketplace.
     * @param  string  $role  The role for which to retrieve the fee.
     * @return float The fee value, or 0 if not found.
     */
    function getTreatmentFee(int $treatmentId, string $role): float
    {
        // Find the marketplace by its ID
        $treatment = Treatment::find($treatmentId);

        // If the marketplace is not found, return 0
        if (! $treatment) {
            return 0;
        }

        // Retrieve the fee from the marketplace's fees based on the given role
        $fee = $treatment->fees?->where('role', $role)->first()?->value;

        // If the fee is not found, return 0
        if (! $fee) {
            return 0;
        }

        // Return the fee value
        return $fee;
    }
}

if (! function_exists('countTreatmentBonus')) {
    /**
     * Calculate the treatment bonus based on the quantity.
     *
     * @param  float|int  $qty  The quantity for calculating the bonus.
     * @return float The bonus amount based on the quantity.
     */
    function countTreatmentBonus(float|int $qty): float
    {
        // Calculate the bonus based on the quantity
        return $qty >= 10 ? floatval($qty) + 3 : ($qty >= 5 ? floatval($qty) + 1 : $qty);
    }
}
