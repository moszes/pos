<?php

/*
|--------------------------------------------------------------------------
| Purchase Order Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Purchase Order Module
|
*/

use App\Models\PurchaseOrder;

if (! function_exists('generatePurchaseOrderNumber')) {
    /**
     * Generates a purchase order number.
     *
     * @return string The generated purchase order number.
     */
    function generatePurchaseOrderNumber(): string
    {
        // Get the count of purchase orders, including soft-deleted ones.
        $purchaseOrderCount = PurchaseOrder::withTrashed()->count();

        // Increment the count by 1.
        $incrementedCount = $purchaseOrderCount + 1;

        // Pad the count with leading zeros to a length of 10.
        $paddedCount = str_pad($incrementedCount, 10, '0', STR_PAD_LEFT);

        // Return the generated purchase order number.
        return 'PO'.$paddedCount;
    }
}

if (! function_exists('getFirstPurchaseOrder')) {
    /**
     * Get the first purchase order.
     *
     * @return PurchaseOrder|null The first purchase order or null if no purchase orders exist.
     */
    function getFirstPurchaseOrder(): ?PurchaseOrder
    {
        return PurchaseOrder::first();
    }
}

if (! function_exists('getPurchaseOrderById')) {
    /**
     * Retrieves a purchase order by its ID.
     *
     * @param  int  $id  The ID of the purchase order.
     * @return PurchaseOrder|null The purchase order with the given ID, or null if not found.
     */
    function getPurchaseOrderById(int $id): ?PurchaseOrder
    {
        return PurchaseOrder::find($id);
    }
}

if (! function_exists('getPurchaseOrderSubTotal')) {
    /**
     * Retrieves the subtotal of a purchase order.
     *
     * @param  int  $id  The ID of the purchase order.
     * @return float The subtotal of the purchase order.
     */
    function getPurchaseOrderSubTotal(int $id): float
    {
        // Retrieve the purchase order by ID.
        $purchaseOrder = PurchaseOrder::find($id);

        // Initialize the subtotal.
        $subTotal = 0;

        // Calculate the subtotal by summing the total price of each item in the purchase order.
        foreach ($purchaseOrder->items as $item) {
            $subTotal += $item->total_price;
        }

        // Return the subtotal.
        return $subTotal;
    }
}
