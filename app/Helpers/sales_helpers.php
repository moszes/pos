<?php

/*
|--------------------------------------------------------------------------
| Sales Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Sales
|
*/

use App\Models\Company;
use App\Models\User;
use Spatie\Permission\Models\Role;

if (! function_exists('getFirstSales')) {
    /**
     * Get the first user with the role 'sales'.
     *
     * @return User|null The first user with the role 'sales', or null if no such user exists.
     */
    function getFirstSales(): ?User
    {
        return User::role('sales')->first();
    }
}

if (! function_exists('getSalesById')) {
    /**
     * Retrieve a sales user by their ID.
     *
     * @param  int  $id  The ID of the user to retrieve.
     * @return User|null The sales user with the given ID, or null if not found.
     */
    function getSalesById(int $id): ?User
    {
        // Use the User model's role method to filter by the 'sales' role,
        // and retrieve the first user with the given ID.
        return User::role('sales')->where('id', $id)->first();
    }
}

if (! function_exists('getSalesAddress')) {
    /**
     * Get the sales address for a user with the given ID.
     *
     * @param  int  $id  The ID of the user.
     * @return string|null The sales address of the user, or null if not found.
     */
    function getSalesAddress(int $id): ?string
    {
        // Find the user with the given ID and with the role 'sales'
        $user = User::role('sales')->where('id', $id)->first();

        // If no user is found, return null
        if (! $user) {
            return null;
        }

        // Return the sales address of the user
        return $user->details->address;
    }
}

if (! function_exists('getSalesPhone')) {
    /**
     * Retrieve the phone number of a sales user by their ID.
     *
     * @param  int  $id  The ID of the sales user.
     * @return string|null The phone number of the sales user, or null if not found.
     */
    function getSalesPhone(int $id): ?string
    {
        // Retrieve the sales user with the given ID
        $salesUser = User::role('sales')->where('id', $id)->first();

        // If the sales user is found, return their phone number
        if ($salesUser) {
            return $salesUser->details->phone;
        }

        // Return null if the sales user is not found
        return null;
    }
}

if (! function_exists('generateSalesEmail')) {
    /**
     * Generate a sales email address.
     *
     * @return string The generated sales email address.
     */
    function generateSalesEmail(): string
    {
        // Generate a unique identifier based on the current time
        $uniqueIdentifier = bcrypt(time());

        // Retrieve the company's website URL and remove the 'https://' prefix
        $companyWebsite = str_replace('https://', '', Company::find(auth()->user()->details->branch_id)->website);

        // Return the generated sales email address
        return 'DOCA'.$uniqueIdentifier.'@'.$companyWebsite;
    }
}

if (! function_exists('salesOption')) {
    /**
     * Generate sales options for a dropdown select input.
     *
     * @return string The generated sales options.
     */
    function salesOption(): string
    {
        // Initialize the sales options string.
        $salesOption = '';

        // Check if the roles exist.
        $roles = [
            'business owner',
            'manager',
            'therapist',
            'doctor',
            'sales',
            'supervisor',
        ];

        foreach ($roles as $role) {
            if (! Role::where('name', $role)->exists()) {
                $roles = array_slice($roles, array_search($role, $roles) + 1);
            }
        }

        // Query users with roles related to sales.
        $sales = User::role($roles)->get();

        // Generate HTML options for each sales user.
        foreach ($sales as $sale) {
            $salesOption .= '<option value="'.$sale->id.'">'.$sale->name.'</option>';
        }

        // Return the sales options string if there are sales users, or an empty string if there are no sales users.
        return $sales->isEmpty() ? '' : $salesOption;
    }
}
