<?php

/*
|--------------------------------------------------------------------------
| Room Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Room Module
|
*/

use App\Models\Room;

if (! function_exists('generateRoomCode')) {
    /**
     * Generate a unique room code.
     *
     * This function generates a unique room code by counting the number of rooms
     * (including soft deleted ones) and padding it with zeros to a total length of 10.
     *
     * @return string The generated room code.
     */
    function generateRoomCode(): string
    {
        // Count the number of rooms (including soft deleted ones)
        $roomCount = Room::withTrashed()->count();

        // Generate the room code by padding the count with zeros
        return 'ROOM'.str_pad($roomCount + 1, 10, '0', STR_PAD_LEFT);
    }
}

if (! function_exists('firstRoom')) {
    /**
     * Retrieve the first room from the database.
     *
     * @return Room|null The first room or null if none found.
     */
    function firstRoom(): ?Room
    {
        // Retrieve the first room from the database.
        return Room::first();
    }
}

if (! function_exists('getRoomById')) {
    /**
     * Retrieve a room by its ID.
     *
     * @param  mixed  $id  The ID of the room.
     * @return Room|null The room with the given ID, or null if not found.
     */
    function getRoomById(mixed $id): ?Room
    {
        // Find the room with the given ID
        return Room::find($id);
    }
}

if (! function_exists('roomOption')) {
    /**
     * This function generates a string of HTML options for a select dropdown, populated with all the rooms from the database.
     *
     * @return string The HTML string of options for the select dropdown. If no rooms are found, an empty string is returned.
     */
    function roomOption(): string
    {
        // Initialize an empty string to store the HTML options
        $roomOption = '';

        // Retrieve all rooms from the database
        $rooms = Room::all();

        // Loop through each room and generate an HTML option for it
        foreach ($rooms as $room) {
            // Generate the HTML option with the room's ID as the value and name
            $roomOption .= '<option value="'.$room->id.'">'.$room->name.'</option>';
        }

        // If there are no rooms, return an empty string
        return $rooms->isEmpty() ? '' : $roomOption;
    }
}
