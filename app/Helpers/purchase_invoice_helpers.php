<?php

/*
|--------------------------------------------------------------------------
| Purchase Invoice Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Purchase Invoice Module
|
*/

use App\Models\PurchaseInvoice;
use LaravelIdea\Helper\App\Models\_IH_PurchaseInvoice_C;

if (! function_exists('generatePurchaseInvoiceNumber')) {
    /**
     * Generate a purchase invoice number.
     *
     * @return string The generated purchase invoice number.
     */
    function generatePurchaseInvoiceNumber(): string
    {
        // Get the count of purchase invoices, including those that have been soft deleted.
        $count = PurchaseInvoice::withTrashed()->count();

        // Increment the count by 1 and pad it with leading zeros to make it 10 characters long.
        $paddedCount = str_pad($count + 1, 10, '0', STR_PAD_LEFT);

        // Prepend the "PI" prefix to the padded count to form the purchase invoice number.
        return 'PI'.$paddedCount;
    }
}

if (! function_exists('getFirstPurchaseInvoice')) {
    /**
     * Retrieves the first purchase invoice from the database.
     *
     * @return PurchaseInvoice The first purchase invoice.
     */
    function getFirstPurchaseInvoice(): PurchaseInvoice
    {
        return PurchaseInvoice::first();
    }
}

if (! function_exists('getPurchaseInvoiceById')) {
    function getPurchaseInvoiceById($id): PurchaseInvoice|array|_IH_PurchaseInvoice_C|null
    {
        return PurchaseInvoice::find($id);
    }
}

if (! function_exists('getPurchaseInvoiceSubTotal')) {
    /**
     * Retrieves the subtotal of a purchase invoice.
     *
     * @param  int  $id  The ID of the purchase invoice.
     * @return float The subtotal of the purchase invoice.
     */
    function getPurchaseInvoiceSubTotal(int $id): float
    {
        // Initialize the subtotal.
        $subTotal = 0;

        // Retrieve the purchase invoice by ID.
        $purchaseInvoice = PurchaseInvoice::find($id);

        // Calculate the subtotal by summing the total price of each item in the purchase invoice.
        foreach ($purchaseInvoice->items as $item) {
            $subTotal += $item->total_price;
        }

        // Return the subtotal.
        return $subTotal;
    }
}
