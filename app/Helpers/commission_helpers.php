<?php

use App\Models\Commission;
use App\Models\SalesInvoice;
use App\Models\SalesInvoiceItem;
use App\Models\ServiceFee;
use App\Models\User;

if (! function_exists('deleteCommissionFee')) {

    /**
     * Delete a commission fee from the database.
     *
     * @param  string  $module  The module of the commission fee.
     * @param  int  $module_id  The ID of the module.
     * @param  string  $fee_type  The type of the fee.
     * @param  string  $role  The role of the fee.
     */
    function deleteCommissionFee(
        string $module,
        int $module_id,
        string $fee_type,
        string $role
    ): void {
        // Delete the commission fee from the database
        ServiceFee::where('module', $module)
            ->where('module_id', $module_id)
            ->where('fee_type', $fee_type)
            ->where('role', $role)
            ->delete();
    }
}

if (! function_exists('generateCommissionFee')) {

    /**
     * Create Fee.
     */
    function generateCommissionFee($item, $module)
    {
        $roles = [
            'senior_doctor',
            'doctor',
            'doctor_assistance',
            'therapist',
            'sales',
        ];

        foreach ($roles as $role) {

            // Get the fee symbol based on the role
            $feeSymbol = in_array($role, ['doctor_assistance', 'therapist']) ? 'fixed' : 'percentage';

            $feeTypes = [
                'normal',
                'discount',
                'target',
            ];

            foreach ($feeTypes as $feeType) {

                ServiceFee::where('module', $module)
                    ->where('module_id', $item->id)
                    ->where('fee_type', $feeType)
                    ->where('fee_symbol', $feeSymbol)
                    ->where('role', $role)->first()?->delete();

                // Update or create a product fee record in the database
                ServiceFee::create([
                    'module' => $module,
                    'module_id' => $item->id,
                    'role' => $role,
                    'value' => 0,
                    'fee_type' => $feeType,
                    'fee_symbol' => $feeSymbol,
                ]);
            }
        }

        return $item;
    }
}

if (! function_exists('getDoctorCommission')) {

    /**
     * Get getDoctorCommission.
     */
    function getDoctorCommission($salePrice): float
    {
        return $salePrice * 0.05;
    }
}

if (! function_exists('countCommission')) {

    /**
     * Count the commission based on sales invoice items.
     */
    function countCommission(): void
    {
        // Retrieve all sales invoice items
        $sales_invoice_items = SalesInvoiceItem::all();

        // Iterate through each sales invoice item
        foreach ($sales_invoice_items as $sales_invoice_item) {
            // Check if the sales invoice item has module_id and module
            if (! $sales_invoice_item->module_id || ! $sales_invoice_item->module) {
                continue; // Skip if module_id or module is missing
            }

            // Get the data related to the module and module ID
            $item = getDataByModule($sales_invoice_item->module, $sales_invoice_item->module_id);

            // Skip if the data is not found
            if (! $item) {
                continue;
            }

            // Retrieve the sales invoice by its ID
            $sales_invoice = getSalesInvoiceById($sales_invoice_item->sales_invoice_id);

            // Skip if the sales invoice is not found
            if (! $sales_invoice) {
                continue;
            }

            // Store the commission for the sales invoice item
            storeCommission($sales_invoice_item, $item, $sales_invoice);
        }
    }
}

if (! function_exists('storeCommission')) {
    function storeCommission(mixed $sales_invoice_item, mixed $item, SalesInvoice|array $sales_invoice): int|float
    {
        $total_commission = 0;

        // check if doctor_id then count commission base on sales invoices if exists
        if ($sales_invoice_item->doctor_id) {
            $total_commission = $item->fees?->where('role', 'doctor')->first() ? floatval($item->fees->where('role', 'doctor')->first()->value) : 0;

            Commission::create([
                'sales_invoice_item_id' => $sales_invoice_item->id,
                'user_id' => $sales_invoice_item->doctor_id,
                'sales_invoice_date' => $sales_invoice->sales_invoice_date,
                'type' => $sales_invoice_item->module,
                'code' => $item->code,
                'name' => $item->name,
                'role' => 'doctor',
                'fee' => $total_commission,
            ]);
        }

        if ($sales_invoice_item->doctor_assistance_id) {
            $total_commission = $item->fees?->where('role', ['doctor', 'therapist'])->first() ? floatval($item->fees->where('role', ['doctor', 'therapist'])->first()->value) : 0;

            Commission::create([
                'sales_invoice_item_id' => $sales_invoice_item->id,
                'user_id' => $sales_invoice_item->doctor_assistance_id,
                'sales_invoice_date' => $sales_invoice->sales_invoice_date,
                'type' => $sales_invoice_item->module,
                'code' => $item->code,
                'name' => $item->name,
                'role' => 'doctor_assistance',
                'fee' => $total_commission,
            ]);
        }

        if ($sales_invoice_item->therapist_id) {

            $total_commission = $item->fees?->where('role', 'therapist')->first() ? floatval($item->fees->where('role', 'therapist')->first()->value) : 0;

            Commission::create([
                'sales_invoice_item_id' => $sales_invoice_item->id,
                'user_id' => $sales_invoice_item->therapist_id,
                'sales_invoice_date' => $sales_invoice->sales_invoice_date,
                'type' => $sales_invoice_item->module,
                'code' => $item->code,
                'name' => $item->name,
                'role' => 'therapist',
                'fee' => $total_commission,
            ]);
        }

        if ($sales_invoice_item->sales_id) {
            $total_commission = $item->fees?->where('role', 'sales')->first() ? floatval($item->fees->where('role', 'sales')->first()->value) : 0;

            Commission::create([
                'sales_invoice_item_id' => $sales_invoice_item->id,
                'user_id' => $sales_invoice_item->sales_id,
                'sales_invoice_date' => $sales_invoice->sales_invoice_date,
                'type' => $sales_invoice_item->module,
                'code' => $item->code,
                'name' => $item->name,
                'role' => 'sales',
                'fee' => $total_commission,
            ]);
        }

        return $total_commission;
    }
}

if (! function_exists('getCommissionByDate')) {

    /**
     * Get getCommissionByDate.
     */
    function getCommissionByDate($employeeId, $dateFrom, $dateTo): float
    {
        $employee = User::find($employeeId);

        $total_commission = 0;

        $sales_invoices = SalesInvoice::whereBetween('sales_invoice_date', [$dateFrom, $dateTo])
            ->pluck('id')->toArray();

        $sales_invoice_items = SalesInvoiceItem::whereIn('id', $sales_invoices)
            ->where(function ($query) use ($employee) {
                $query->where('doctor_id', $employee->id)
                    ->orWhere('doctor_assistance_id', $employee->id)
                    ->orWhere('therapist_id', $employee->id)
                    ->orWhere('sales_id', $employee->id);
            })
            ->get();

        foreach ($sales_invoice_items as $sales_invoice_item) {

            $role = $employee->getRoleNames()[0];

            $fees = getDataByModule($sales_invoice_item->module, $sales_invoice_item->module_id)->fees;

            if ($fees) {
                foreach ($fees as $fee) {
                    if ($fee->role == '') {
                        continue;
                    }

                    $total_commission += countFormat($fee->where('role', $role)->first()->value);
                }
            }
        }

        return $total_commission;
    }
}

if (! function_exists('getUserCommissionByMonth')) {

    /**
     * Get getUserCommissionByDate.
     */
    function getUserCommissionByMonth($employeeId, $month): float
    {
        $employee = User::find($employeeId);

        $total_commission = 0;

        $sales_invoices = SalesInvoiceItem::whereMonth('created_at', $month)->get();

        foreach ($sales_invoices as $sales_invoice) {

            $role = $employee->getRoleNames()[0];

            $fees = getDataByModule($sales_invoice->module, $sales_invoice->module_id)->fees;

            if ($fees) {
                foreach ($fees as $fee) {
                    if ($fee->role == '') {
                        continue;
                    }

                    $total_commission = countFormat($fee->where('role', $role)->first()->value);
                }
            }
        }

        return $total_commission;
    }
}
