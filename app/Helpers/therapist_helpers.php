<?php

/*
|--------------------------------------------------------------------------
| Therapist Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Therapist
|
*/

use App\Models\Company;
use App\Models\User;

if (! function_exists('getFirstTherapist')) {
    /**
     * Retrieve the first therapist user.
     *
     * @return User|null The first therapist user or null if not found.
     */
    function getFirstTherapist(): ?User
    {
        return User::role('therapist')->first();
    }
}

if (! function_exists('getTherapistById')) {
    /**
     * Get the therapist by their ID.
     *
     * @param  int  $id  The ID of the therapist.
     * @return User|null The therapist object or null if not found.
     */
    function getTherapistById(int $id): ?User
    {
        // Search for the user with the given ID and the role of therapist
        return User::role('therapist')->where('id', $id)->first();
    }
}

if (! function_exists('getTherapistAddress')) {
    /**
     * Get the address of a therapist by their ID.
     *
     * @param  int  $id  The ID of the therapist.
     * @return string|null The address of the therapist, or null if not found.
     */
    function getTherapistAddress(int $id): ?string
    {
        // Find the therapist with the given ID and their address.
        $therapist = User::role('therapist')->where('id', $id)->first();

        return $therapist?->details->address;
    }
}

if (! function_exists('getTherapistPhone')) {
    /**
     * Get the phone number of a therapist based on their ID.
     *
     * @param  int  $id  The ID of the therapist.
     * @return string|null The phone number of the therapist, or null if not found.
     */
    function getTherapistPhone(int $id): ?string
    {
        // Find the therapist with the given ID and their role is 'therapist'
        $therapist = User::role('therapist')->where('id', $id)->first();

        if ($therapist) {
            // Return the phone number of the therapist
            return $therapist->details->phone;
        }

        // Return null if therapist is not found
        return null;
    }
}

if (! function_exists('generateTherapistEmail')) {
    /**
     * Generate a therapist email.
     *
     * @return string The generated therapist email
     */
    function generateTherapistEmail(): string
    {
        // Generate a unique identifier for the email
        $uniqueId = 'THR'.bcrypt(time());

        // Find the company website URL and remove "https://" from it
        $company = Company::find(auth()->user()->details->branch_id);
        $website = str_replace('https://', '', $company->website);

        // Combine the unique identifier and the website URL to form the therapist email
        return $uniqueId.'@'.$website;
    }
}
