<?php

/*
|--------------------------------------------------------------------------
| Package Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Package Module
|
*/

use App\Models\Package;

if (! function_exists('getPackageFee')) {
    /**
     * Retrieve the fee for a given marketplace and role.
     *
     * @param  int  $packageId  The ID of the marketplace.
     * @param  string  $role  The role for which to retrieve the fee.
     * @return float The fee value, or 0 if not found.
     */
    function getPackageFee(int $packageId, string $role): float
    {
        // Find the marketplace by its ID
        $package = Package::find($packageId);

        // If the marketplace is not found, return 0
        if (! $package) {
            return 0;
        }

        // Retrieve the fee from the marketplace's fees based on the given role
        $fee = $package->fees?->where('role', $role)->first()?->value;

        // If the fee is not found, return 0
        if (! $fee) {
            return 0;
        }

        // Return the fee value
        return $fee;
    }
}

if (! function_exists('getFirstPackage')) {
    /**
     * Retrieve the first package from the database.
     *
     * @return Package|null The first package, or null if no package is found.
     */
    function getFirstPackage(): ?Package
    {
        // Use the Package model's first method to retrieve the first package from the database.
        return Package::first();
    }
}

if (! function_exists('getPackageById')) {
    /**
     * Retrieve the first package from the database.
     *
     * @return Package|null The first package, or null if no package is found.
     */
    function getPackageById($id): ?Package
    {
        return Package::find($id);
    }
}
