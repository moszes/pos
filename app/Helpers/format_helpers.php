<?php

/*
|--------------------------------------------------------------------------
| Format Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Format Module
|
*/

if (! function_exists('convertStringToDecimal')) {
    /**
     * Converts a string to a decimal number.
     *
     * @param  string  $string  The input string.
     * @return float|int The converted decimal number.
     */
    function convertStringToDecimal(string $string): float|int
    {
        // Remove all non-digit characters from the string
        $string = preg_replace('/[^0-9.]/', '', $string);

        // Check if the string is empty
        if (empty($string)) {
            return 0;
        }

        // If the string contains a decimal point, convert it to a float
        if (str_contains($string, '.')) {
            return floatval($string);
        }

        // Otherwise, convert it to an integer
        return intval($string);
    }
}

if (! function_exists('priceFormat')) {
    /**
     * Format the price with currency symbol and thousands separator.
     *
     * @param  float|null  $price  The price to be formatted.
     * @return string The formatted price.
     */
    function priceFormat(?float $price): string
    {
        // Set the default price to 0 if it is null.
        $formattedPrice = $price ?? 0;

        // Add currency symbol and format the price with thousands separator.
        return 'Rp. '.number_format($formattedPrice, 2, ',', '.');
    }
}

if (! function_exists('numberFormat')) {
    /**
     * Format a number as a string with thousands separator and decimal point.
     *
     * @return string The formatted number.
     */
    function numberFormat($price = 0): string
    {
        // If the price is null, set it to 0.
        $price = $price ?? 0;

        // Format the number with thousands separator and decimal point.
        return number_format($price, 0, ',', '.');
    }
}

if (! function_exists('discountFormat')) {
    /**
     * Formats the price with two decimal places and returns it as a string.
     *
     * @param  float|null  $price  The price to be formatted.
     * @return string The formatted price.
     */
    function discountFormat(?float $price): string
    {
        // If the price is null, set it to 0
        $price = $price ?? 0;

        // Format the price with 2 decimal places
        return number_format($price, 2);
    }
}

if (! function_exists('countFormat')) {
    /**
     * Format the price and convert it to float or int.
     *
     * @param  float|int|null  $price  The price to be formatted and converted.
     * @return float|int The formatted and converted price.
     */
    function countFormat(float|int|null $price): float|int
    {
        /**
         * If the price is null, set it to 0.
         * Otherwise, convert it to float.
         */
        return $price === null ? 0 : (float) $price;
    }
}

if (! function_exists('datetimeFormat')) {
    /**
     * Format a date string to 'Y-m-d' format.
     *
     * @param  string|null  $date  - The date string to be formatted.
     * @return string - The formatted date string.
     */
    function dateFormat(?string $date): string
    {
        // If the date is null, use the current date.
        if (is_null($date)) {
            return '';
        }

        // Otherwise, format the given date string to 'Y-m-d'.
        return date('Y-m-d', strtotime($date));
    }
}

if (! function_exists('timeFormat')) {
    /**
     * Formats a date to the time format 'H:i:s'
     *
     * @param  string|null  $date  The date to format
     * @return string The formatted time
     */
    function timeFormat(?string $date): string
    {
        // Check if the date is null
        if (is_null($date)) {
            // Return the current time formatted to 'H:i:s'
            return '';
        }

        // Return the date formatted to 'H:i:s'
        return date('H:i:s', strtotime($date));
    }
}
