<?php

/*
|--------------------------------------------------------------------------
| Product Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Product Module
|
*/

use App\Models\Cogs;
use App\Models\Product;

if (! function_exists('getFirstProduct')) {
    /**
     * Retrieves the first product from the database.
     *
     * @return Product|null The first product, or null if no products exist.
     */
    function getFirstProduct(): ?Product
    {
        return Product::first();
    }
}

if (! function_exists('getProductFee')) {
    /**
     * Retrieves the fee of a product based on the given role from the database.
     *
     * @param  int  $productId  The ID of the product.
     * @param  string  $role  The role for which the fee is to be retrieved.
     * @return float|null The fee value, or null if the product or its fee is not found.
     */
    function getProductFee(int $productId, string $role): ?float
    {
        // Find the product by its ID
        $product = Product::find($productId);

        // If the product is not found, return null
        if (! $product) {
            return 0;
        }

        // Retrieve the fee from the product's fees based on the given role
        $fee = $product->fees?->where('role', $role)->first()?->value;

        // If the fee is not found, return 0
        if (! $fee) {
            return 0;
        }

        // Return the fee value
        return $fee;
    }
}

if (! function_exists('getProductById')) {
    /**
     * Retrieve a product by its ID.
     *
     * @param  int  $id  The ID of the product.
     * @return Product|null The product if found, null otherwise.
     */
    function getProductById(int $id): ?Product
    {
        // Use the find method of the Product model to retrieve the product by its ID
        return Product::find($id);
    }
}

if (! function_exists('getLastPriceByProductId')) {
    /**
     * Retrieve the last price of a product by its ID.
     *
     * @param  int  $id  The ID of the product.
     * @return float|int The last price of the product. Returns 0 if no price is found.
     */
    function getLastPriceByProductId(int $id): float|int
    {
        // Retrieve the last COGS record for the given product ID
        $cogs = Cogs::where('module', 'product')
            ->where('module_id', $id)
            ->orderBy('id', 'desc')
            ->first();

        // Return the COGS price if found, otherwise return 0
        return $cogs ? $cogs->cogs : 0;
    }
}
