<?php

/*
|--------------------------------------------------------------------------
| Manager Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Manager
|
*/

use App\Models\Company;
use App\Models\User;

if (! function_exists('firstManager')) {
    /**
     * Retrieve the first manager.
     *
     * @return User The manager model instance.
     */
    function firstManager(): User
    {
        return User::role('manager')->first();
    }
}

if (! function_exists('getManagerById')) {
    /**
     * Retrieve a manager by ID.
     *
     * @param  int  $id  The ID of the manager.
     * @return User The manager model instance.
     */
    function getManagerById(int $id): User
    {
        return User::role('manager')->where('id', $id)->first();
    }
}

if (! function_exists('getManagerAddress')) {
    /**
     * Retrieves the address of the manager with the given ID.
     *
     * @param  int  $id  The ID of the manager.
     * @return mixed|null The manager's address or null if not found.
     */
    function getManagerAddress(int $id): mixed
    {
        return User::role('manager')->where('id', $id)->first()->details->address;
    }
}

if (! function_exists('getManagerPhone')) {
    /**
     * Get the phone number of a manager.
     *
     * @param  int  $id  The ID of the manager.
     * @return string|null The phone number of the manager or null if the manager does not exist or has no phone number.
     */
    function getManagerPhone(int $id): ?string
    {
        return User::role('manager')->where('id', $id)->first()->details->phone;
    }
}

if (! function_exists('generateManagerEmail')) {
    /**
     * Generates a unique email for a manager.
     *
     * @return string The generated manager email.
     */
    function generateManagerEmail(): string
    {
        return 'MNG'.bcrypt(time()).'@'.str_replace('https://', '', Company::find(Company::first()->id)->website);
    }
}
