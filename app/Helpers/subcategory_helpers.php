<?php

/*
|--------------------------------------------------------------------------
| Sub Category Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Sub Category Module
|
*/

use App\Models\SubCategory;

if (! function_exists('getFirstSubCategory')) {
    /**
     * Retrieve the first subcategory.
     *
     * @return SubCategory The first subcategory.
     */
    function getFirstSubCategory(): SubCategory
    {
        return SubCategory::first();
    }
}

if (! function_exists('getSubCategoryById')) {
    /**
     * Get a subcategory by its ID.
     *
     * @param  mixed  $id  The ID of the subcategory.
     * @return SubCategory|null The subcategory object if found, null otherwise.
     */
    function getSubCategoryById(mixed $id): ?SubCategory
    {
        // Return the subcategory object if found, null otherwise
        return SubCategory::find($id);
    }
}

if (! function_exists('getSubCategoryByCode')) {
    /**
     * Retrieves a SubCategory object by its code.
     *
     * @param  string  $code  The code of the SubCategory to retrieve.
     * @return SubCategory|null The retrieved SubCategory object or null if not found.
     */
    function getSubCategoryByCode(string $code): ?SubCategory
    {
        // Use the 'where' method of the SubCategory model to filter by code
        // and retrieve the first matching record
        return SubCategory::where('code', $code)->first();
    }
}
