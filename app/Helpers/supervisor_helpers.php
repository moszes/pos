<?php

/*
|--------------------------------------------------------------------------
| Supervisor Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Supervisor
|
*/

use App\Models\Company;
use App\Models\User;

if (! function_exists('firstSupervisor')) {
    function firstSupervisor(): User
    {
        return User::role('supervisor')->first();
    }
}

if (! function_exists('getSupervisorById')) {
    /**
     * Get the supervisor by their ID.
     *
     * @param  int  $id  The ID of the supervisor
     * @return User The supervisor model
     */
    function getSupervisorById(int $id): User
    {
        return User::role('supervisor')->where('id', $id)->first();
    }
}

if (! function_exists('getSupervisorAddress')) {
    /**
     * Retrieves the address of the supervisor with the given ID.
     *
     * @param  int  $id  The ID of the supervisor
     * @return string The address of the supervisor
     *
     * @throws Exception if the supervisor is not found or if their details are not available
     */
    function getSupervisorAddress(int $id): string
    {
        return User::role('supervisor')->where('id', $id)->first()->details->address;
    }
}

if (! function_exists('getSupervisorPhone')) {
    /**
     * Get the phone number of a supervisor.
     *
     * @param  int  $id  The ID of the supervisor
     * @return string The phone number of the supervisor
     *
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException if the supervisor or their details are not found
     */
    function getSupervisorPhone(int $id): string
    {
        return User::role('supervisor')->where('id', $id)->first()->details->phone;
    }
}

if (! function_exists('generateSupervisorEmail')) {
    /**
     * Generates a unique supervisor email based on the current time and the company's website.
     *
     * @return string The generated supervisor email
     *
     * @throws RuntimeException if the company or its website is not found
     */
    function generateSupervisorEmail(): string
    {
        return 'SPV'.bcrypt(time()).'@'.str_replace('https://', '', Company::find(Company::first()->id)->website);
    }
}
