<?php

/*
|--------------------------------------------------------------------------
| Payment Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Payment Module
|
*/

use App\Models\Payment;
use App\Models\SalesInvoicePayment;
use Illuminate\Support\Collection;

if (! function_exists('generatePaymentCode')) {
    /**
     * Generate a unique payment code.
     *
     * This function generates a unique payment code by counting the number of payments
     * (including soft deleted ones) and padding it with zeros to a total length of 10.
     *
     * @return string The generated payment code.
     */
    function generatePaymentCode(): string
    {
        // Count the number of payments (including soft deleted ones)
        $paymentCount = Payment::withTrashed()->count();

        // Generate the payment code by padding the count with zeros
        return 'PAY'.str_pad($paymentCount + 1, 10, '0', STR_PAD_LEFT);
    }
}

if (! function_exists('firstPayment')) {
    /**
     * Retrieve the first payment from the database.
     *
     * @return Payment|null The first payment object, or null if no payment exists.
     */
    function firstPayment(): ?Payment
    {
        // Retrieve the first payment from the database
        return Payment::first();
    }
}

if (! function_exists('getPaymentBySalesInvoiceId')) {
    /**
     * Retrieves the payment types associated with a given sales invoice ID.
     *
     * @param  int  $id  The ID of the sales invoice.
     * @return Collection The collection of payment types.
     */
    function getPaymentBySalesInvoiceId(int $id): Collection
    {
        // Retrieve all payments associated with the given sales invoice ID
        $payments = SalesInvoicePayment::where('sales_invoice_id', $id)->get();

        // Initialize an empty array to store the payment types
        $paymentType = [];

        // Iterate over each payment and retrieve its corresponding payment type
        foreach ($payments as $payment) {
            // Retrieve the payment object by its ID and get its name
            $paymentType[] = getPaymentById($payment->payment_id)->name;
        }

        // Return the collection of payment types
        return collect($paymentType);
    }
}

if (! function_exists('getPaymentById')) {
    /**
     * Retrieve a payment by its ID.
     *
     * This function fetches a payment from the database based on its ID.
     * If the payment is not found, it returns null.
     *
     * @param  int  $id  The ID of the payment to retrieve.
     * @return Payment | array|null The payment object if found, or null if not found.
     */
    function getPaymentById(int $id): Payment|array|null
    {
        // Use the Payment model's find method to retrieve the payment by its ID
        return Payment::find($id);
    }
}

if (! function_exists('paymentOption')) {
    /**
     * Generate a string of HTML options for payment options.
     *
     * @return string The HTML string of payment options.
     */
    function paymentOption(): string
    {
        // Initialize an empty string to store the payment options
        $paymentOption = '';

        // Retrieve all payments from the database
        $payments = Payment::all();

        // Iterate over each payment and generate an HTML option
        foreach ($payments as $payment) {
            // Generate the HTML option with the payment ID as the value and name
            $paymentOption .= '<option value="'.$payment->id.'">'.$payment->name.'</option>';
        }

        // If there are no payments, return an empty string
        return $payments->isEmpty() ? '' : $paymentOption;
    }
}
