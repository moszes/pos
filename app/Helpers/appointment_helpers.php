<?php

/*
|--------------------------------------------------------------------------
| Appointment Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Appointment Module
|
*/

use App\Models\Appointment;

if (! function_exists('generateAppointmentNumber')) {
    /**
     * Generate a unique appointment number.
     *
     * This function generates a unique appointment number by counting the number of appointments
     * (including soft deleted ones) and padding it with zeros to a total length of 10.
     *
     * @return string The generated appointment number.
     */
    function generateAppointmentNumber(): string
    {
        // Count the number of appointments (including soft deleted ones)
        $appointmentCount = Appointment::withTrashed()->count();

        // Generate the appointment number by padding the count with zeros
        return 'APM'.str_pad($appointmentCount + 1, 10, '0', STR_PAD_LEFT);
    }
}

if (! function_exists('getFirstAppointment')) {
    /**
     * Retrieve the first appointment from the database.
     *
     * @return Appointment|null The first appointment, or null if no appointments are found.
     */
    function getFirstAppointment(): ?Appointment
    {
        return Appointment::first();
    }
}

if (! function_exists('getAppointmentById')) {
    /**
     * Retrieve an appointment by its ID.
     *
     * @param  int  $id  The ID of the appointment.
     * @return array|Appointment|null The appointment object, or null if no appointment is found.
     */
    function getAppointmentById($id): array|Appointment|null
    {
        // Use the Appointment model's find method to retrieve the appointment by ID.
        return Appointment::find($id);
    }
}

if (! function_exists('getAppointmentModuleId')) {
    /**
     * Retrieve the ID of an appointment module by its name and ID.
     *
     * @param  string  $module  The name of the module. If 'marketplace', it will be converted to 'MarketPlace'.
     * @param  int  $id  The ID of the appointment module.
     * @return mixed The ID of the appointment module, or null if not found.
     */
    function getAppointmentModuleId(string $module, int $id): mixed
    {
        // If the module name is 'marketplace', convert it to 'MarketPlace'
        if ($module == 'marketplace') {
            $module = 'MarketPlace';
        }

        // Create the model class name using the module name
        $modelClass = 'App\\Models\\'.ucfirst($module);

        // Instantiate the model class and find the module by ID
        $model = new $modelClass;

        // Return the found module ID or null if not found
        return $model->find($id);
    }
}
