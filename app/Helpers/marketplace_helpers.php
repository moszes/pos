<?php

/*
|--------------------------------------------------------------------------
| Market Place Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Market Place Module
|
*/

use App\Models\MarketPlace;

if (! function_exists('firstMarketPlace')) {
    /**
     * Retrieve the first marketplace from the database.
     *
     * This function uses the Eloquent ORM to retrieve the first instance of the MarketPlace model from the database.
     * If no marketplace exists, it will return null.
     *
     * @return MarketPlace|null The first marketplace object, or null if no marketplace exists.
     */
    function firstMarketPlace(): ?MarketPlace
    {
        // Use the MarketPlace model's first() method to retrieve the first marketplace from the database
        // The first() method returns the first result of the query; if there are no results, it returns null
        return MarketPlace::first();
    }
}

if (! function_exists('getMarketPlaceById')) {
    /**
     * Retrieve a MarketPlace model by its ID.
     *
     * @param  int  $id  The ID of the MarketPlace model.
     * @return MarketPlace|null The MarketPlace model or null if not found.
     */
    function getMarketPlaceById(int $id): ?MarketPlace
    {
        // Find the MarketPlace model by its ID
        return MarketPlace::find($id);
    }
}

if (! function_exists('generateMarketPlaceNumber')) {
    /**
     * Generate a unique marketplace number.
     *
     * This function generates a unique marketplace number by counting the number of marketplaces
     * (including soft deleted ones) and padding it with zeros to a total length of 10.
     *
     * @return string The generated marketplace number.
     */
    function generateMarketPlaceNumber(): string
    {
        // Count the number of marketplaces (including soft deleted ones)
        $marketPlaceCount = MarketPlace::withTrashed()->count();

        // Generate the marketplace number by padding the count with zeros
        return 'MARK'.str_pad($marketPlaceCount + 1, 10, '0', STR_PAD_LEFT);
    }
}

if (! function_exists('getMarketPlaceFee')) {

    /**
     * Retrieve the fee for a given marketplace and role.
     *
     * @param  int  $marketPlaceId  The ID of the marketplace.
     * @param  string  $role  The role for which to retrieve the fee.
     * @return float The fee value, or 0 if not found.
     */
    function getMarketPlaceFee(int $marketPlaceId, string $role): float
    {
        // Find the marketplace by its ID
        $marketPlace = MarketPlace::find($marketPlaceId);

        // If the marketplace is not found, return 0
        if (! $marketPlace) {
            return 0;
        }

        // Retrieve the fee from the marketplace's fees based on the given role
        $fee = $marketPlace->fees?->where('role', $role)->first()?->value;

        // If the fee is not found, return 0
        if (! $fee) {
            return 0;
        }

        // Return the fee value
        return $fee;
    }
}
