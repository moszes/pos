<?php

/*
|--------------------------------------------------------------------------
| Category Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Category Module
|
*/

use App\Models\Category;
use Illuminate\Support\Collection;

if (! function_exists('getFirstCategory')) {
    /**
     * Get the first category from the database.
     *
     * @return Category|null The category object if found, or null if not found.
     */
    function getFirstCategory(): ?Category
    {
        return Category::first();
    }
}

if (! function_exists('getCategoryById')) {
    /**
     * Get a category by its ID.
     *
     * @param  int|null  $id  The ID of the category.
     * @return Category|null The category object if found, or null if not found.
     */
    function getCategoryById(?int $id = null): ?Category
    {
        // Find the category by ID
        // Return the category object if found, or null if not found
        return Category::find($id);
    }
}

if (! function_exists('getCategoryByCode')) {
    /**
     * Retrieves the category object by code.
     *
     * @param  string  $code  - The category code.
     * @return Category|null - The category object or null if not found.
     */
    function getCategoryByCode(string $code): ?Category
    {
        // Retrieve the category by code from the database
        $category = Category::where('code', $code)->first();

        // Return the category object if found, otherwise return null
        return is_null($category) ? null : $category;
    }
}

if (! function_exists('getCategoryOptions')) {
    /**
     * Get the options for a specific category type.
     *
     * @return string The collection of category options.
     */
    function getCategoryOptions(): string
    {
        // Get all the categories
        $categories = Category::all();

        // Check if there are no categories
        if ($categories->isEmpty()) {
            return '';
        }

        // Generate the option HTML for each category and combine them into one string
        return $categories->map(function ($category) {
            return '<option value="'.$category->id.'">'.$category->name.'</option>';
        })->join('');
    }
}

if (! function_exists('getCategoryType')) {
    /**
     * Get a category option
     */
    function getCategoryType(): array
    {
        return [
            'treatment',
            'product',
            'package',
            'giftcard',
            'marketplace',
        ];
    }
}
