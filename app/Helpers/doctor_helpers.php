<?php

/*
|--------------------------------------------------------------------------
| Doctor Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Doctor
|
*/

use App\Models\User;

if (! function_exists('getFirstDoctor')) {
    /**
     * Retrieve the first User with the 'doctor' role.
     *
     * @return User|null The first User with the 'doctor' role, or null if no User is found.
     */
    function getFirstDoctor(): ?User
    {
        // Retrieve the User with the 'doctor' role
        return User::role('doctor')->first();
    }
}

if (! function_exists('getDoctorById')) {
    /**
     * Get a doctor by their ID.
     *
     * @param  int  $id  The ID of the doctor.
     * @return User|null The doctor with the specified ID, or null if not found.
     */
    function getDoctorById(int $id): ?User
    {
        // Retrieve the doctor with the specified ID and the role of 'doctor'.
        return User::role('doctor')->where('id', $id)->first();
    }
}

if (! function_exists('getDoctorAddress')) {
    /**
     * Get the address of a doctor based on their ID.
     *
     * @param  int  $id  The ID of the doctor.
     * @return string|null The address of the doctor, or null if not found.
     */
    function getDoctorAddress(int $id): ?string
    {
        // Find the doctor based on their ID and get their details.
        $doctor = User::role('doctor')->where('id', $id)->first();

        // If the doctor is found, return their address.
        if ($doctor) {
            return $doctor->details->address;
        }

        // Return null if the doctor is not found.
        return null;
    }
}

if (! function_exists('getDoctorPhone')) {
    /**
     * Get the phone number of a doctor based on their ID.
     *
     * @param  int  $id  The ID of the doctor.
     * @return string|null The phone number of the doctor, or null if not found.
     */
    function getDoctorPhone(int $id): ?string
    {
        // Find the doctor with the given ID and retrieve their phone number
        $doctor = User::role('doctor')->where('id', $id)->first();

        // If the doctor is found, return their phone number
        if ($doctor) {
            return $doctor->details->phone;
        }

        // Return null if the doctor is not found
        return null;
    }
}
