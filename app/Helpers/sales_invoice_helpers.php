<?php

/*
|--------------------------------------------------------------------------
| Sales Order Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Sales Module
|
*/

use App\Models\Approval;
use App\Models\SalesInvoice;

if (! function_exists('generateSalesInvoiceNumber')) {
    /**
     * Generate a sales invoice number.
     *
     * @return string The generated sales invoice number.
     */
    function generateSalesInvoiceNumber(): string
    {
        // Get the total count of sales invoices, including soft deleted ones,
        // and add 1 to get the next invoice number.
        $nextInvoiceNumber = SalesInvoice::withTrashed()->count() + 1;

        // Pad the invoice number with zeros to a total length of 10 characters,
        // and prepend 'SI' to the padded number.
        $paddedInvoiceNumber = str_pad($nextInvoiceNumber, 10, '0', STR_PAD_LEFT);

        // Return the generated sales invoice number.
        return 'SI'.$paddedInvoiceNumber;
    }
}

if (! function_exists('getFirstSalesInvoice')) {
    /**
     * Get the first sales invoice.
     *
     * @return SalesInvoice The first sales invoice.
     */
    function getFirstSalesInvoice(): SalesInvoice
    {
        // Use the `SalesInvoice` model to get the first sales invoice.
        // Return the first sales invoice.
        return SalesInvoice::first();
    }
}

if (! function_exists('getSalesInvoiceById')) {
    /**
     * Get a sales invoice by its ID.
     *
     * @param  mixed  $id  The ID of the sales invoice.
     * @return SalesInvoice|array|null The sales invoice or null if not found.
     */
    function getSalesInvoiceById(mixed $id): SalesInvoice|array|null
    {
        return SalesInvoice::find($id);
    }
}

if (! function_exists('getFirstSalesInvoice')) {
    /**
     * Retrieve the first sales invoice from the database.
     */
    function getFirstSalesInvoice(): SalesInvoice
    {
        return SalesInvoice::first();
    }
}

if (! function_exists('getSalesInvoiceApproval')) {
    /**
     * Retrieve the approval for a sales invoice by its ID.
     *
     * @param  mixed  $id  The ID of the sales invoice.
     * @return Approval|null The approval for the sales invoice, or null if not found.
     */
    function getSalesInvoiceApproval(mixed $id): ?Approval
    {
        // Retrieve the approval with the specified module, module ID, and status
        return Approval::where('module', 'sales_invoice')
            ->where('module_id', $id)
            ->where('status', 'approved')
            ->first();
    }
}
