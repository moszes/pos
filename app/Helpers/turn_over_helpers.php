<?php

use App\Models\SalesInvoiceItem;
use App\Models\SalesInvoicePayment;
use Illuminate\Database\Eloquent\Collection;

if (! function_exists('getTurnOverTreatmentByDate')) {

    /**
     * Get Turn Over By Date
     */
    function getTurnOverTreatmentByDate(mixed $day, mixed $dateStart, mixed $dateEnd): mixed
    {
        $timeStart = date('H:i:s', strtotime($dateStart));
        $timeEnd = date('H:i:s', strtotime($dateEnd));
        $dateStart = date('Y-m', strtotime($dateStart)).'-'.$day.' '.$timeStart;
        $dateEnd = date('Y-m', strtotime($dateEnd)).'-'.$day.' '.$timeEnd;

        return SaLesInvoiceItem::where('module', 'treatment')
            ->whereBetween('created_at', [$dateStart, $dateEnd])
            ->sum('total_price');
    }
}

if (! function_exists('getTurnOverRecordsByDate')) {

    /**
     * Retrieves the sales invoice items within a given date range.
     *
     * @param  mixed  $day  The day of the month to filter by.
     * @param  mixed  $dateStart  The start date of the range.
     * @param  mixed  $dateEnd  The end date of the range.
     * @return bool A collection of SalesInvoiceItem models.
     */
    function getTurnOverRecordsByDate(mixed $day, mixed $dateStart, mixed $dateEnd): bool
    {
        // Convert the start and end dates to the desired format
        $timeStart = date('H:i:s', strtotime($dateStart));
        $timeEnd = date('H:i:s', strtotime($dateEnd));
        $dateStart = date('Y-m', strtotime($dateStart)).'-'.$day.' '.$timeStart;
        $dateEnd = date('Y-m', strtotime($dateEnd)).'-'.$day.' '.$timeEnd;

        // Retrieve the sales invoice items within the date range
        return SalesInvoiceItem::whereBetween('created_at', [$dateStart, $dateEnd])->count('total_price') > 0;
    }
}

if (! function_exists('getTurnOverMarketplaceByDate')) {

    /**
     * Get Turn Over By Date
     */
    function getTurnOverMarketplaceByDate(mixed $day, mixed $dateStart, mixed $dateEnd): mixed
    {
        $timeStart = date('H:i:s', strtotime($dateStart));
        $timeEnd = date('H:i:s', strtotime($dateEnd));
        $dateStart = date('Y-m', strtotime($dateStart)).'-'.$day.' '.$timeStart;
        $dateEnd = date('Y-m', strtotime($dateEnd)).'-'.$day.' '.$timeEnd;

        return SaLesInvoiceItem::where('module', 'marketplace')
            ->whereBetween('created_at', [$dateStart, $dateEnd])
            ->sum('total_price');
    }
}

if (! function_exists('getTurnOverPackageByDate')) {

    /**
     * Get Turn Over By Date
     */
    function getTurnOverPackageByDate(mixed $day, mixed $dateStart, mixed $dateEnd): mixed
    {
        $timeStart = date('H:i:s', strtotime($dateStart));
        $timeEnd = date('H:i:s', strtotime($dateEnd));
        $dateStart = date('Y-m', strtotime($dateStart)).'-'.$day.' '.$timeStart;
        $dateEnd = date('Y-m', strtotime($dateEnd)).'-'.$day.' '.$timeEnd;

        return SaLesInvoiceItem::where('module', 'package')
            ->whereBetween('created_at', [$dateStart, $dateEnd])
            ->sum('total_price');
    }
}

if (! function_exists('getTurnOverProductByDate')) {

    /**
     * Get Turn Over By Date
     */
    function getTurnOverProductByDate(mixed $day, mixed $dateStart, mixed $dateEnd): mixed
    {
        $timeStart = date('H:i:s', strtotime($dateStart));
        $timeEnd = date('H:i:s', strtotime($dateEnd));
        $dateStart = date('Y-m', strtotime($dateStart)).'-'.$day.' '.$timeStart;
        $dateEnd = date('Y-m', strtotime($dateEnd)).'-'.$day.' '.$timeEnd;

        return SaLesInvoiceItem::where('module', 'product')
            ->whereBetween('created_at', [$dateStart, $dateEnd])
            ->sum('total_price');
    }
}

if (! function_exists('getTurnOverSubscriptionByDate')) {

    /**
     * Get Turn Over By Date
     */
    function getTurnOverSubscriptionByDate(mixed $day, mixed $dateStart, mixed $dateEnd): mixed
    {
        $timeStart = date('H:i:s', strtotime($dateStart));
        $timeEnd = date('H:i:s', strtotime($dateEnd));
        $dateStart = date('Y-m', strtotime($dateStart)).'-'.$day.' '.$timeStart;
        $dateEnd = date('Y-m', strtotime($dateEnd)).'-'.$day.' '.$timeEnd;

        return SaLesInvoiceItem::where('module', 'subscription')
            ->whereBetween('created_at', [$dateStart, $dateEnd])
            ->sum('total_price');
    }
}

if (! function_exists('getTurnOverCashPaymentByDate')) {

    /**
     * Get Turn Over By Date
     */
    function getTurnOverCashPaymentByDate(mixed $day, mixed $dateStart, mixed $dateEnd): mixed
    {
        $timeStart = date('H:i:s', strtotime($dateStart));
        $timeEnd = date('H:i:s', strtotime($dateEnd));
        $dateStart = date('Y-m', strtotime($dateStart)).'-'.$day.' '.$timeStart;
        $dateEnd = date('Y-m', strtotime($dateEnd)).'-'.$day.' '.$timeEnd;

        return SalesInvoicePayment::whereBetween('created_at', [$dateStart, $dateEnd])
            ->where('payment_id', 1)
            ->sum('amount');
    }
}

if (! function_exists('getTurnOverCreditDebitPaymentByDate')) {

    /**
     * Get Turn Over By Date
     */
    function getTurnOverCreditDebitPaymentByDate(mixed $day, mixed $dateStart, mixed $dateEnd): mixed
    {
        $timeStart = date('H:i:s', strtotime($dateStart));
        $timeEnd = date('H:i:s', strtotime($dateEnd));
        $dateStart = date('Y-m', strtotime($dateStart)).'-'.$day.' '.$timeStart;
        $dateEnd = date('Y-m', strtotime($dateEnd)).'-'.$day.' '.$timeEnd;

        return SalesInvoicePayment::whereBetween('created_at', [$dateStart, $dateEnd])
            ->where('payment_id', 2)
            ->sum('amount');
    }
}

if (! function_exists('getTurnOverCustomPaymentByDate')) {

    /**
     * Get Turn Over By Date
     */
    function getTurnOverCustomPaymentByDate(mixed $day, mixed $dateStart, mixed $dateEnd): mixed
    {
        $timeStart = date('H:i:s', strtotime($dateStart));
        $timeEnd = date('H:i:s', strtotime($dateEnd));
        $dateStart = date('Y-m', strtotime($dateStart)).'-'.$day.' '.$timeStart;
        $dateEnd = date('Y-m', strtotime($dateEnd)).'-'.$day.' '.$timeEnd;

        return SalesInvoicePayment::whereBetween('created_at', [$dateStart, $dateEnd])
            ->where('payment_id', 3)
            ->sum('amount');
    }
}

if (! function_exists('getTurnOverPrepaidPaymentByDate')) {

    /**
     * Get Turn Over By Date
     */
    function getTurnOverPrepaidPaymentByDate(mixed $day, mixed $dateStart, mixed $dateEnd): mixed
    {
        $timeStart = date('H:i:s', strtotime($dateStart));
        $timeEnd = date('H:i:s', strtotime($dateEnd));
        $dateStart = date('Y-m', strtotime($dateStart)).'-'.$day.' '.$timeStart;
        $dateEnd = date('Y-m', strtotime($dateEnd)).'-'.$day.' '.$timeEnd;

        return SalesInvoicePayment::whereBetween('created_at', [$dateStart, $dateEnd])
            ->where('payment_id', 4)
            ->sum('amount');
    }
}

if (! function_exists('getTurnOverQrPaymentByDate')) {

    /**
     * Get Turn Over By Date
     */
    function getTurnOverQrPaymentByDate(mixed $day, mixed $dateStart, mixed $dateEnd): mixed
    {
        $timeStart = date('H:i:s', strtotime($dateStart));
        $timeEnd = date('H:i:s', strtotime($dateEnd));
        $dateStart = date('Y-m', strtotime($dateStart)).'-'.$day.' '.$timeStart;
        $dateEnd = date('Y-m', strtotime($dateEnd)).'-'.$day.' '.$timeEnd;

        return SalesInvoicePayment::whereBetween('created_at', [$dateStart, $dateEnd])
            ->where('payment_id', 5)
            ->sum('amount');
    }
}

if (! function_exists('getTurnOverVoucherPaymentByDate')) {

    /**
     * Get Turn Over By Date
     */
    function getTurnOverVoucherPaymentByDate(mixed $day, mixed $dateStart, mixed $dateEnd): mixed
    {
        $timeStart = date('H:i:s', strtotime($dateStart));
        $timeEnd = date('H:i:s', strtotime($dateEnd));
        $dateStart = date('Y-m', strtotime($dateStart)).'-'.$day.' '.$timeStart;
        $dateEnd = date('Y-m', strtotime($dateEnd)).'-'.$day.' '.$timeEnd;

        return SalesInvoicePayment::whereBetween('created_at', [$dateStart, $dateEnd])
            ->where('payment_id', 6)
            ->sum('amount');
    }
}
