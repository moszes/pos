<?php

/*
|--------------------------------------------------------------------------
| Customer Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for Customer
|
*/

use App\Models\Company;
use App\Models\User;

if (! function_exists('getFirstCustomer')) {
    /**
     * Retrieves the first customer user.
     *
     * @return User|null The first customer user or null if not found.
     */
    function getFirstCustomer(): ?User
    {
        return User::role('customer')->first();
    }
}

if (! function_exists('getCustomerById')) {
    function getCustomerById($id): array
    {
        $customer = [];

        $cust = User::role('customer')->where('id', $id)->first();
        $customer['users'] = $cust?->toArray();
        $customer['details'] = $cust->details?->toArray();

        // Retrieve the customer by their I D only if they have the role of "customer".
        return $customer;
    }
}

if (! function_exists('getCustomerAddress')) {
    /**
     * Get the address of a customer based on their ID.
     *
     * @param  int  $id  The ID of the customer.
     * @return string|null The address of the customer or null if no customer is found.
     */
    function getCustomerAddress(mixed $id): ?string
    {
        // Find the customer with the given ID who has the role of 'customer'
        $customer = User::role('customer')->where('id', $id)->first();

        // If no customer is found, return null
        if (! $customer) {
            return null;
        }

        // Return the address of the customer
        return $customer->details->address;
    }
}

if (! function_exists('getCustomerPhone')) {
    /**
     * Retrieve the phone number of a customer by their ID.
     *
     * @param  int  $id  The ID of the customer.
     * @return string|null The phone number of the customer, or null if not found.
     */
    function getCustomerPhone(mixed $id): ?string
    {
        // Find the customer with the given ID and the role of "customer".
        $customer = User::role('customer')->where('id', $id)->first();

        // If no customer is found, return null.
        if (! $customer) {
            return null;
        }

        // Retrieve the phone number from the customer's details.
        return env('APP_ENV') != 'production' ? '1234567890' : $customer->details->phone;
    }
}

if (! function_exists('generateCustomerEmail')) {
    /**
     * Generates a customer email.
     *
     * @return string The generated customer email.
     */
    function generateCustomerEmail(): string
    {
        // Generate a unique customer identifier based on the current time
        $customerId = 'CUST'.bcrypt(time());

        // Get the company website URL and remove the "https://" prefix
        $companyWebsite = str_replace('https://', '', Company::find(auth()->user()->details->branch_id)->website);

        // Combine the customer identifier and the company website to form the email
        return $customerId.'@'.$companyWebsite;
    }
}

if (! function_exists('generateCustomerCode')) {
    /**
     * Generate a unique customer code.
     *
     * @return string The generated customer code.
     */
    function generateCustomerCode(): string
    {
        // Get the count of all users, including soft deleted ones.
        $userCount = User::withTrashed()->count();

        // Increment the count by 1 and pad it with leading zeros.
        $paddedCount = str_pad($userCount + 1, 3, '0', STR_PAD_LEFT);

        // Combine the prefix 'CUST' with the padded count to generate the customer code.
        // Return the generated customer code.
        return 'CUST'.$paddedCount;
    }
}
