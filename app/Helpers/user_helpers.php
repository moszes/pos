<?php

/*
|--------------------------------------------------------------------------
| User Helper
|--------------------------------------------------------------------------
|
| Here we put all the helper for User Module
|
*/

use App\Models\User;
use Spatie\Permission\Models\Role;

if (! function_exists('getFirstUser')) {
    /**
     * Retrieve the first user from the database.
     *
     * @return User|null The first user, or null if no user is found.
     */
    function getFirstUser(): ?User
    {
        return User::first();
    }
}

if (! function_exists('lastUser')) {
    /**
     * Retrieve the last user from the database.
     *
     * @return User|null The last user or null if no user exists.
     */
    function lastUser(): ?User
    {
        return User::orderBy('id', 'desc')->first();
    }
}

if (! function_exists('getUserById')) {
    /**
     * Get a user by their ID.
     *
     * @param  mixed  $id  The ID of the user.
     * @return User|null The user object.
     */
    function getUserById(mixed $id): ?User
    {
        // Use the User model's find method to retrieve the user by ID.
        return User::find($id);
    }
}

if (! function_exists('roleOption')) {
    /**
     * Generates a string containing HTML options for all roles except 'godadmin'.
     *
     * @return string The HTML options string.
     */
    function roleOption(): string
    {
        // Initialize the role options string.
        $roleOption = '';

        // Query all roles except 'godadmin'.
        $roles = Role::where('name', '!=', 'godadmin')->get();

        // Generate HTML options for each role.
        foreach ($roles as $role) {
            $roleOption .= '<option value="'.$role->name.'">'.$role->name.'</option>';
        }

        // Return the role options string if there are roles, or an empty string if there are no roles.
        return $roles->isEmpty() ? '' : $roleOption;
    }
}

if (! function_exists('doctorOption')) {
    /**
     * Generate a string of HTML options for doctors.
     *
     * @return string The HTML options for doctors.
     */
    function doctorOption(): string
    {
        // Initialize the doctor options string.
        $doctorOptions = '';

        // Check if the roles "doctor" and "senior doctor" exist.
        $doctorRole = Role::where('name', 'doctor')->first();
        $seniorDoctorRole = Role::where('name', 'senior doctor')->first();
        if (! $doctorRole || ! $seniorDoctorRole) {
            return '';
        }

        // Get all users with the role of "doctor".
        $doctors = User::role(['doctor', 'senior doctor'])->get();

        // Iterate through each doctor and generate an HTML option.
        foreach ($doctors as $doctor) {
            $doctorOptions .= '<option value="'.$doctor->id.'">'.$doctor->name.'</option>';
        }

        // If no doctors are found, return an empty string.
        return $doctors->isEmpty() ? '' : $doctorOptions;
    }
}

if (! function_exists('doctorAssistanceOption')) {
    function doctorAssistanceOption(): string
    {
        $doctorAssistanceOption = '';

        // Check if the roles "doctor assistance" and "therapist" exist.
        $doctorAssistanceRole = Role::where('name', 'doctor assistance')->first();
        $therapistRole = Role::where('name', 'therapist')->first();
        if (! $doctorAssistanceRole || ! $therapistRole) {
            return '';
        }

        $doctor_assistants = User::role(['therapist', 'doctor assistance'])->get();

        foreach ($doctor_assistants as $doctor_assistance) {
            $doctorAssistanceOption .= '<option value="'.$doctor_assistance->id.'">'.$doctor_assistance->name.'</option>';
        }

        return $doctor_assistants->isEmpty() ? '' : $doctorAssistanceOption;
    }
}

if (! function_exists('therapistOption')) {
    /**
     * Generate HTML options for therapist selection.
     *
     * @return string The HTML options for therapist selection.
     */
    function therapistOption(): string
    {
        // Initialize the variable to store the HTML options.
        $therapistOption = '';

        // Check if the role "therapist" exists.
        $therapistRole = Role::where('name', 'therapist')->first();
        if (! $therapistRole) {
            return '';
        }

        // Get the therapists with the role 'therapist'.
        $therapists = User::role(['therapist'])->get();

        // Loop through each therapist and generate the HTML option.
        foreach ($therapists as $therapist) {
            $therapistOption .= '<option value="'.$therapist->id.'">'.$therapist->name.'</option>';
        }

        // Check if there are no therapists, return an empty string. Otherwise, return the generated HTML options.
        return $therapists->isEmpty() ? '' : $therapistOption;
    }
}

if (! function_exists('getUserTransaction')) {
    function getUserTransaction($module, $id)
    {
        $modelClass = new ('App\\Models\\'.ucfirst($module))();

        return $modelClass::find($id);
    }
}

if (! function_exists('generateUserCode')) {
    /**
     * Generate a unique user code.
     *
     * @return string The generated user code.
     */
    function generateUserCode(): string
    {
        return str_pad('USR-'.User::count() + 1, 5, '0', STR_PAD_LEFT);
    }
}
