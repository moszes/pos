<?php

use App\Models\Subscription;
use App\Models\SubscriptionPlan;

if (! function_exists('getSubscriptionById')) {
    /**
     * Retrieve a subscription plan by its ID.
     *
     * @param  int  $id  The ID of the subscription plan.
     * @return SubscriptionPlan|null The subscription plan with the given ID, or null if not found.
     */
    function getSubscriptionById(int $id): ?SubscriptionPlan
    {
        // Find the subscription plan by ID
        return SubscriptionPlan::find($id);
    }
}

if (! function_exists(function: 'generateSubscriptionCode')) {
    /**
     * Generate the code for a new subscription based on the number of existing subscriptions.
     *
     * @return string The generated subscription code.
     */
    function generateSubscriptionCode(): string
    {
        $subscriptionCount = Subscription::withTrashed()->count() + 1;
        $paddedCount = str_pad($subscriptionCount, 10, '0', STR_PAD_LEFT);

        return 'SUBS'.$paddedCount;
    }
}
