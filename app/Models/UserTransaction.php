<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class UserTransaction extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'branch_id',
        'user_id',
        'sales_invoice_id',
        'appointment_id',
        'default_promo',
        'transaction_status',
        'module',
        'module_id',
        'module_code',
        'module_name',
        'reference_module',
        'reference_id',
        'reference_code',
        'reference_name',
        'doctor_id',
        'doctor_assistance_id',
        'therapist_id',
        'sales_id',
        'doctor_fee',
        'doctor_assistance_fee',
        'therapist_fee',
        'sales_fee',
        'qty',
        'item_price',
        'total_price',
        'sell_price',
        'total_sell_price',
        'note',
    ];

    /**
     * Define a relationship with the sales_invoice table.
     */
    public function sales_invoice(): BelongsTo
    {
        return $this->belongsTo(
            SalesInvoice::class, // Model class
            'sales_invoice_id' // Foreign key
        );
    }

    /**
     * Get the service associated with the module.
     */
    public function treatment(): BelongsTo
    {
        // Define the relationship with the Service model using the module_id foreign key
        return $this->belongsTo(Treatment::class, 'module_id');
    }

    /**
     * Retrieves the activity log options.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Set the default log options
        $logOptions = LogOptions::defaults();

        // Enable logging for fillable attributes
        $logOptions->logFillable();

        // Set the log name for the user transaction log
        $logOptions->useLogName('User Transaction Log');

        // Return the activity log options
        return $logOptions;
    }
}
