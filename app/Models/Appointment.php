<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @property $items
 */
class Appointment extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'branch_id',
        'customer_id',
        'sales_invoice_number',
        'appointment_number',
        'appointment_date',
        'tax',
        'others',
        'discount',
        'grand_total',
        'reference_module',
        'reference_id',
        'note',
    ];

    /**
     * Get the items associated with the appointment.
     */
    public function items(): HasMany
    {
        // Return the hasMany relationship for the AppointmentItem model
        return $this->hasMany(AppointmentItem::class);
    }

    /**
     * Get the customer associated with the user.
     */
    public function customer(): HasOne
    {
        // Return the HasOne relationship between the User model and the customer model.
        return $this->hasOne(User::class, 'id', 'customer_id');
    }

    /**
     * Returns the activity log options for the function.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Set the default log options
        $options = LogOptions::defaults();

        // Enable logging of fillable attributes
        $options->logFillable();

        // Set the log name to "Appointment Log"
        $options->useLogName('Appointment Log');

        // Return the activity log options
        return $options;
    }
}
