<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Package extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'branch_id',
        'code',
        'name',
        'price',
    ];

    /**
     * Get the company that owns the resource.
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Get the branch associated with this model.
     */
    public function branch(): BelongsTo
    {
        return $this->belongsTo(Branch::class);
    }

    /**
     * Get the items associated with the package.
     */
    public function items(): HasMany
    {
        return $this->hasMany(PackageItem::class);
    }

    /**
     * Get the normal fees associated with the product.
     */
    public function targetFees(): HasMany
    {
        // This method returns the relationship between the product and the ProductFee model
        // where the fee type is 'normal'.
        return $this->hasMany(ServiceFee::class, 'module_id')
            ->where('module', 'package')
            ->where('fee_type', 'target');
    }

    /**
     * Get the normal fees associated with the product.
     */
    public function normalFees(): HasMany
    {
        // This method returns the relationship between the product and the ProductFee model
        // where the fee type is 'normal'.
        return $this->hasMany(ServiceFee::class, 'module_id')
            ->where('module', 'package')
            ->where('fee_type', 'normal');
    }

    /**
     * Get the discount fees associated with the product.
     */
    public function discountFees(): HasMany
    {
        // This method returns the relationship between the product and the ProductFee model
        // where the fee type is 'discount'.
        return $this->hasMany(ServiceFee::class, 'module_id')
            ->where('module', 'package')
            ->where('fee_type', 'discount');
    }

    /**
     * Retrieve the activity log options.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Set the default options
        $options = LogOptions::defaults();

        // Enable logging of fillable attributes
        $options->logFillable();

        // Set the log name
        $options->useLogName('Package Log');

        // Return the activity log options
        return $options;
    }
}
