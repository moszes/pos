<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class MarketPlaceMaterialUsage extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'market_place_id',
        'product_id',
        'normal_price',
        'agent_price',
        'qty',
        'size',
        'usage',
        'cost',
    ];

    /**
     * Get the product that belongs to this application.
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Get the activity log options for the application.
     */
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->useLogName('Market Place Item Log');
    }
}
