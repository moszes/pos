<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Subscription extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'branch_id',
        'subscription_plan_id',
        'customer_id',
        'code',
        'buy_date',
        'active_date',
        'expired_date',
        'status',
    ];

    /**
     * Get the options for logging the model.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Create a new instance of LogOptions with default options
        $options = LogOptions::defaults();

        // Enable logging for fillable attributes
        $options->logFillable();

        // Set the log name to 'Category Log'
        $options->useLogName('Subscription Log');

        // Return the modified options
        return $options;
    }
}
