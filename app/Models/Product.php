<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Product extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'branch_id',
        'category_id',
        'sub_category_id',
        'unit_of_measure_id',
        'code',
        'name',
        'size',
        'cost',
        'agent_price',
        'normal_price',
    ];

    /**
     * Get the company associated with the user.
     */
    public function company(): BelongsTo
    {
        // Return the relationship between the user and the company
        return $this->belongsTo(Company::class);
    }

    /**
     * Get the branch associated with the model.
     */
    public function branch(): BelongsTo
    {
        // Return the relationship between the model and the Branch model
        return $this->belongsTo(Branch::class);
    }

    /**
     * Get the normal fees associated with the product.
     */
    public function targetFees(): HasMany
    {
        // This method returns the relationship between the product and the ProductFee model
        // where the fee type is 'normal'.
        return $this->hasMany(ServiceFee::class, 'module_id')
            ->where('module', 'product')
            ->where('fee_type', 'target');
    }

    /**
     * Get the normal fees associated with the product.
     */
    public function normalFees(): HasMany
    {
        // This method returns the relationship between the product and the ProductFee model
        // where the fee type is 'normal'.
        return $this->hasMany(ServiceFee::class, 'module_id')
            ->where('module', 'product')
            ->where('fee_type', 'normal');
    }

    /**
     * Get the discount fees associated with the product.
     */
    public function discountFees(): HasMany
    {
        // This method returns the relationship between the product and the ProductFee model
        // where the fee type is 'discount'.
        return $this->hasMany(ServiceFee::class, 'module_id')
            ->where('module', 'product')
            ->where('fee_type', 'discount');
    }

    /**
     * Get the category associated with the model.
     */
    public function category(): BelongsTo
    {
        // Return the relationship with the Category model
        return $this->belongsTo(Category::class);
    }

    /**
     * Get the subcategory associated with the model.
     */
    public function sub_category(): BelongsTo
    {
        // Return the relationship with the Category model
        return $this->belongsTo(SubCategory::class);
    }

    public function unitOfMeasure(): BelongsTO
    {
        return $this->belongsTo(UnitOfMeasure::class);
    }

    /**
     * Retrieve the activity log options.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Create default log options
        $options = LogOptions::defaults();

        // Enable logging for fillable attributes
        $options->logFillable();

        // Set the log name to "Product Log"
        $options->useLogName('Product Log');

        return $options;
    }
}
