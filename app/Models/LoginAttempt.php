<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class LoginAttempt extends Model
{
    use LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'user_id',
        'ip_address',
    ];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->useLogName('Login Attemp Log');
    }
}
