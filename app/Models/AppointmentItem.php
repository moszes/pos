<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class AppointmentItem extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'appointment_id',
        'module',
        'module_id',
        'code',
        'name',
        'doctor_id',
        'doctor_assistance_id',
        'therapist_id',
        'sales_id',
        'agent_id',
        'travel_agent_id',
        'doctor_fee',
        'doctor_assistance_fee',
        'therapist_fee',
        'sales_fee',
        'agent_fee',
        'travel_agent_fee',
        'room_id',
        'room_start',
        'room_end',
        'qty',
        'price',
        'sell_price',
        'total_price',
        'total_sell_price',
        'discount_item',
        'total_price',
        'transaction_status',
    ];

    /**
     * Retrieve the activity log options for the AppointmentItem model.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Create a new LogOptions object with default options
        $logOptions = LogOptions::defaults();

        // Enable logging of fillable attributes
        $logOptions->logFillable();

        // Set the log name to 'AppointmentItem Item Log'
        $logOptions->useLogName('AppointmentItem Item Log');

        // Return the activity log options
        return $logOptions;
    }
}
