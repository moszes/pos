<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @property int $id
 * @property int $purchase_invoice_id
 * @property int $product_id
 * @property int $qty
 * @property float $price
 * @property float $discount_item
 * @property float $total_price
 * @property mixed $purchase_invoice_number
 */
class PurchaseInvoiceItem extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'purchase_invoice_id',
        'product_id',
        'qty',
        'price',
        'discount_item',
        'total_price',
    ];

    /**
     * Get the purchase invoice associated with this model.
     */
    public function purchase_invoices(): BelongsTo
    {
        return $this->belongsTo(PurchaseInvoice::class);
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->useLogName('Purchase Invoice Item Log');
    }
}
