<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class MarketPlaceFee extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'market_place_id',
        'role',
        'value',
        'type',
    ];

    /**
     * Retrieve the activity log options for the MarketPlaceFee model.
     *
     * This function is used to define the activity log options for the MarketPlaceFee model.
     * The activity log options define how the model's activity should be logged.
     *
     * @return LogOptions The activity log options for the MarketPlaceFee model.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Create a new LogOptions object with default options
        $logOptions = LogOptions::defaults();

        // Enable logging of fillable attributes
        //  are the attributes that can be logged when an activity is recorded
        $logOptions->logFillable();

        // Set the log name to 'Market Place Fee Log'
        // The log name is used to identify the type of activity being logged
        $logOptions->useLogName('Market Place Fee Log');

        // Return the modified LogOptions object
        return $logOptions;
    }
}
