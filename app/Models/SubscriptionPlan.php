<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class SubscriptionPlan extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'branch_id',
        'code',
        'name',
        'price',
    ];

    public function treatments(): BelongsToMany
    {
        return $this->belongsToMany(Treatment::class, 'subscription_plan_treatment', 'subscription_plan_id', 'treatment_id');
    }

    /**
     * Get the options for logging the model.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Create a new instance of LogOptions with default options
        $options = LogOptions::defaults();

        // Enable logging for fillable attributes
        $options->logFillable();

        // Set the log name to 'Category Log'
        $options->useLogName('Subscription Log');

        // Return the modified options
        return $options;
    }
}
