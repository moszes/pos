<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Branch extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'code',
        'name',
    ];

    /**
     * Get the company associated with the user.
     *
     * @return BelongsTo The company relationship
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Retrieves the activity log options.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Create a new instance of LogOptions
        $logOptions = LogOptions::defaults();

        // Enable logging for fillable attributes
        $logOptions->logFillable();

        // Set the log name to 'Branch Log'
        $logOptions->useLogName('Branch Log');

        // Return the log options
        return $logOptions;
    }
}
