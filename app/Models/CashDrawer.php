<?php

/** @noinspection PhpMethodNamingConventionInspection */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class CashDrawer extends Model
{
    use LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'branch_id',
        'user_id',
        'code',
        'sales_invoice',
        'credit_debit',
        'custom',
        'prepaid',
        'qrcode',
        'voucher',
        'grand_total',
        'date',
    ];

    /**
     * Get the company that owns this instance.
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Get the branch associated with this model.
     */
    public function branch(): BelongsTo
    {
        // Return the relationship between the model and the Branch model
        return $this->belongsTo(Branch::class);
    }

    /**
     * Get the activity log options.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable() // Log all fillable fields
            ->useLogName('Cash Drawer Log'); // Set the log name to 'Cash Drawer Log'
    }
}
