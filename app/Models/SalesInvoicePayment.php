<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class SalesInvoicePayment extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'sales_invoice_id',
        'payment_id',
        'amount',
        'reference_number',
        'status',
    ];

    /**
     * Get the activity log options for sales invoice payment.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Return the default log options with fillable logging enabled and a custom log name.
        return LogOptions::defaults()
            ->logFillable()
            ->useLogName('Sales Invoice Payment Log');
    }
}
