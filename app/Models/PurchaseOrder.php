<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @property int $id
 * @property int $branch_id
 * @property int $vendor_id
 * @property int $user_id
 * @property int $purchase_order_id
 * @property string $purchase_order_number
 * @property float $discount
 * @property float $tax
 * @property float $others
 * @property float $grand_total
 * @property string $date
 * @property string $approved_by
 * @property string $approved_date
 * @property string $note
 * @property mixed $items
 */
class PurchaseOrder extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'id',
        'branch_id',
        'vendor_id',
        'user_id',
        'purchase_order_id',
        'purchase_order_number',
        'discount',
        'tax',
        'others',
        'grand_total',
        'date',
        'approved_by',
        'approved_date',
        'note',
    ];

    /**
     * Get the branch that the user belongs to.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(Branch::class);
    }

    /**
     * Get the vendor associated with the product.
     */
    public function vendor(): BelongsTo
    {
        return $this->belongsTo(Vendor::class);
    }

    /**
     * Get the items associated with the purchase order.
     */
    public function items(): HasMany
    {
        return $this->hasMany(PurchaseOrderItem::class);
    }

    /**
     * Retrieves the options for the activity log.
     *
     * @return LogOptions The options for the activity log.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Set the default options
        $options = LogOptions::defaults();

        // Enable logging of fillable attributes
        $options->logFillable();

        // Set the log name
        $options->useLogName('Purchase Order Log');

        // Return the options
        return $options;
    }
}
