<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Commission extends Model
{
    use LogsActivity, SoftDeletes;

    protected $fillable = [
        'sales_invoice_item_id',
        'user_id',
        'sales_invoice_date',
        'type',
        'code',
        'name',
        'role',
        'fee',
        'fee_release',
    ];

    /**
     * Get the sales invoice item associated with the sales invoice.
     *
     * @return BelongsTo The sales invoice item model.
     */
    public function sales_invoice_items(): BelongsTo
    {
        return $this->belongsTo(SalesInvoiceItem::class, 'sales_invoice_item_id');
    }

    /**
     * Get the activity log options for the Commission model.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Return the default log options with additional configurations
        return LogOptions::defaults()
            ->logFillable()  // Enable logging of fillable attributes
            ->useLogName('Commission Log');  // Set the log name to 'Commission Log'
    }
}
