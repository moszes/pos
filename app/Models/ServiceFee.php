<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class ServiceFee extends Model
{
    use LogsActivity, SoftDeletes;

    const MODULES = [
        'product',
        'treatment',
        'marketplace',
        'package',
        'subscription',
    ];

    const ROLES = [
        'senior_doctor',
        'doctor',
        'doctor_assistance',
        'sales',
        'therapist',
    ];

    const FEE_TYPE = ['normal', 'discount', 'target'];

    const FEE_SYMBOL = ['fixed', 'percentage'];

    protected $fillable = [
        'module',
        'module_id',
        'role',
        'value',
        'fee_type',
        'fee_symbol',
    ];

    /**
     * Retrieve the activity log options for the ServiceFee model.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Create a new instance of LogOptions with default values
        $logOptions = LogOptions::defaults();

        // Enable logging of fillable attributes
        $logOptions->logFillable();

        // Set the log name to 'Service Fee Log'
        $logOptions->useLogName('Service Fee Log');

        // Return the log options
        return $logOptions;
    }
}
