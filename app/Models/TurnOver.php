<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class TurnOver extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'branch_id',
        'month',
        'amount',
    ];

    /**
     * Retrieves the activity log options.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Set the default log options
        $options = LogOptions::defaults();

        // Enable logging of fillable attributes
        $options->logFillable();

        // Set the log name to 'Turn Over Log'
        $options->useLogName('Turn Over Log');

        // Return the activity log options
        return $options;
    }
}
