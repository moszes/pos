<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class PackageItem extends Model
{
    use LogsActivity, SoftDeletes;

    protected $fillable = [
        'package_id',
        'module',
        'module_id',
        'code',
        'name',
        'qty',
        'package_item_price',
        'normal_item_price',
        'total_package_price',
    ];

    /**
     * Get the package that owns the package item.
     */
    public function package(): BelongsTo
    {
        return $this->belongsTo(Package::class);
    }

    /**
     * Retrieve the activity log options for the Package Item model.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Set the default log options
        $options = LogOptions::defaults();

        // Enable logging of fillable attributes
        $options->logFillable();

        // Set the log name to 'Package Item Log'
        $options->useLogName('Package Item Log');

        // Return the activity log options
        return $options;
    }
}
