<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @property $items
 */
class SalesInvoice extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'branch_id',
        'appointment_id',
        'customer_id',
        'sales_invoice_number',
        'sales_invoice_status',
        'discount',
        'tax',
        'others',
        'grand_total',
        'total_payment',
        'created_by',
        'sales_invoice_date',
        'discount_approved_by',
        'discount_approved_date',
        'release_by',
        'release_date',
        'note',
    ];

    /**
     * Retrieves the relationship between the model and the Company class.
     *
     * @return BelongsTo The relationship between the model and the Company class.
     */
    public function company(): BelongsTo
    {
        // Return the relationship between the model and the Company class
        return $this->belongsTo(Company::class);
    }

    /**
     * Retrieves the parent branch model for the current model.
     *
     * @return BelongsTo The relationship between the current model and the Branch model.
     */
    public function branch(): BelongsTo
    {
        // Return the relationship between the current model and the Branch model.
        return $this->belongsTo(Branch::class);
    }

    /**
     * Retrieves the sales invoice items for the sales invoice.
     *
     * @return HasMany The relationship between the sales invoice and the sales invoice items.
     */
    public function items(): HasMany
    {
        // Return the relationship between the sales invoice and the sales invoice items
        return $this->hasMany(SalesInvoiceItem::class);
    }

    /**
     * Retrieves the payment records associated with the given sales invoice.
     *
     * @return HasMany The payment records associated with the sales invoice.
     */
    public function payments(): HasMany
    {
        // The Return has many relationships with the SalesInvoicePayment model
        return $this->hasMany(SalesInvoicePayment::class);
    }

    /**
     * Get the appointment associated with the user.
     */
    public function appointment(): HasOne
    {
        // Return the appointment relationship using the Appointment model
        return $this->hasOne(Appointment::class, 'id', 'appointment_id');
    }

    /**
     * Retrieves the customer associated with the User model.
     *
     * @return HasOne The hasOne relationship between the User model and the customer model.
     */
    public function customer(): HasOne
    {
        // Return the HasOne relationship between the User model and the customer model.
        return $this->hasOne(User::class, 'id', 'customer_id');
    }

    /**
     * Retrieves the approval relationship for the given module.
     *
     * @return hasMany The approval relationship.
     */
    public function approval(): HasMany
    {
        return $this->hasMany(Approval::class, 'module_id', 'id');
    }

    /**
     * Get the user's transactions.
     */
    public function transactions(): HasMany
    {
        // Define the relationship with the UserTransaction model
        // using the 'sales_invoice_id' as the foreign key and 'id' as the local key
        return $this->hasMany(UserTransaction::class, 'sales_invoice_id', 'id')->whereNull('deleted_at');
    }

    /**
     * Retrieves the activity log options for the Sales Invoice Log.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Return the default log options with additional configurations
        return LogOptions::defaults()
            ->logFillable()  // Enable logging of fillable attributes
            ->useLogName('Sales Invoice Log');  // Set the log name to 'Sales Invoice Log'
    }
}
