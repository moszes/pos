<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VendorPayment extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'vendor_id',
        'period',
        'bank',
        'payment_name',
        'payment_number',
    ];

    /**
     * Get the company that owns this model.
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Retrieve the activity log options.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Create a new instance of LogOptions with default values
        $logOptions = LogOptions::defaults();

        // Enable logging for fillable attributes
        $logOptions->logFillable();

        // Set the log name to 'Vendor Payment Log'
        $logOptions->useLogName('Vendor Payment Log');

        // Return the log options
        return $logOptions;
    }
}
