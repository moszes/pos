<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class AdjustRedemption extends Model
{
    use LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'user_id',
        'user_transaction_id',
        'customer_id',
        'treatment_id',
        'qty',
        'note',
    ];

    /**
     * Get the activity log options for the model.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Configure the activity log options
        return LogOptions::defaults()
            // Log all fillable attributes
            ->logFillable()
            // Use the custom log name 'Adjust Redemption Log'
            ->useLogName('Adjust Redemption Log');
    }
}
