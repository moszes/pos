<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class ApplicationSetting extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'company_code',
        'name',
        'status',
        'module',
        'value',
        'redirect_url',
    ];

    /**
     * Retrieve the activity log options.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Create a new instance of LogOptions with default values
        $logOptions = LogOptions::defaults();

        // Enable logging of fillable attributes
        $logOptions->logFillable();

        // Set the log name to 'Application Settings Log'
        $logOptions->useLogName('Application Settings Log');

        // Return the log options
        return $logOptions;
    }
}
