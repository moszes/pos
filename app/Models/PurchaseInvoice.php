<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @property int $id
 * @property int $branch_id
 * @property int $vendor_id
 * @property int $user_id
 * @property string $invoice_number
 * @property float $discount
 * @property float $tax
 * @property float $others
 * @property float $grand_total
 * @property string $date
 * @property mixed $items
 */
class PurchaseInvoice extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'branch_id',
        'vendor_id',
        'user_id',
        'purchase_order_id',
        'invoice_number',
        'discount',
        'tax',
        'others',
        'grand_total',
        'date',
        'approved_by',
        'approved_date',
        'note',
    ];

    /**
     * Get the branch associated with the model.
     */
    public function branch(): BelongsTo
    {
        // Return the relationship between the model and the Branch model
        return $this->belongsTo(Branch::class);
    }

    /**
     * Get the vendor associated with the product.
     */
    public function vendor(): BelongsTo
    {
        // Return the relationship between the product and the vendor
        return $this->belongsTo(Vendor::class);
    }

    /**
     * Get the items associated with the PurchaseInvoice.
     */
    public function items(): HasMany
    {
        // Return the relationship between PurchaseInvoice and PurchaseInvoiceItem models
        return $this->hasMany(PurchaseInvoiceItem::class);
    }

    /**
     * Get the activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Set the default log options
        $options = LogOptions::defaults();

        // Enable logging for fillable attributes
        $options->logFillable();

        // Set the log name to 'Purchase Invoice Log'
        $options->useLogName('Purchase Invoice Log');

        // Return the log options
        return $options;
    }
}
