<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @method static create(array $array)
 * @method static where(string $string, int $id)
 */
class Approval extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'branch_id',
        'request_by',
        'request_date',
        'approve_by',
        'approve_date',
        'module',
        'module_id',
        'status',
        'note',
    ];

    /**
     * Retrieves the activity log options for the Approval Log.
     *
     * @return LogOptions The activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Return the default log options with additional configurations
        return LogOptions::defaults()
            ->logFillable()  // Enable logging of fillable attributes
            ->useLogName('Approval Log');  // Set the log name to 'Sales Invoice Log'
    }
}
