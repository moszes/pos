<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Category extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    const TYPES = [
        'treatment',
        'product',
        'package',
        'marketplace',
        'subscription',
    ];

    protected $fillable = [
        'company_id',
        'branch_id',
        'code',
        'name',
        'type',
    ];

    /**
     * Get the company associated with the user.
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Get the subcategories associated with the category.
     */
    public function subCategories(): HasMany
    {
        // Return the relationship between the category and its subcategories
        return $this->hasMany(SubCategory::class);
    }

    /**
     * Get the options for logging the model.
     */
    public function products(): HasMany
    {
        // Return the relationship for the 'products' table
        return $this->hasMany(Product::class);
    }

    /**
     * Get the services associated with the user.
     */
    public function treatment(): HasMany
    {
        // Return the hasMany relationship with the Service model
        return $this->hasMany(Treatment::class);
    }

    /**
     * Get the options for logging the model.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Create a new instance of LogOptions with default options
        $options = LogOptions::defaults();

        // Enable logging for fillable attributes
        $options->logFillable();

        // Set the log name to 'Category Log'
        $options->useLogName('Category Log');

        // Return the modified options
        return $options;
    }
}
