<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class MarketPlace extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'branch_id',
        'category_id',
        'sub_category_id',
        'code',
        'name',
        'price',
    ];

    /**
     * Get the items associated with the marketplace.
     */
    public function material_usages(): HasMany
    {
        return $this->hasMany(MarketPlaceMaterialUsage::class);
    }

    /**
     * This method establishes a relationship between the Treatment model and the Category model.
     * It is a one-to-many relationship where a Treatment belongs to a Category.
     */
    public function category(): BelongsTo
    {
        // Return the relationship between the Treatment model and the Category model
        return $this->belongsTo(Category::class);
    }

    /**
     * This method establishes a relationship between the Treatment model and the SubCategory model.
     * It is a one-to-many relationship where a Treatment belongs to a SubCategory.
     */
    public function sub_category(): BelongsTo
    {
        // Return the relationship between the Treatment model and the SubCategory model
        return $this->belongsTo(SubCategory::class);
    }

    /**
     * Get the normal fees associated with the product.
     */
    public function targetFees(): HasMany
    {
        // This method returns the relationship between the product and the ProductFee model
        // where the fee type is 'normal'.
        return $this->hasMany(ServiceFee::class, 'module_id')
            ->where('module', 'marketplace')
            ->where('fee_type', 'target');
    }

    /**
     * Get the normal fees associated with the product.
     */
    public function normalFees(): HasMany
    {
        // This method returns the relationship between the product and the ProductFee model
        // where the fee type is 'normal'.
        return $this->hasMany(ServiceFee::class, 'module_id')
            ->where('module', 'marketplace')
            ->where('fee_type', 'normal');
    }

    /**
     * Get the discount fees associated with the product.
     */
    public function discountFees(): HasMany
    {
        // This method returns the relationship between the product and the ProductFee model
        // where the fee type is 'discount'.
        return $this->hasMany(ServiceFee::class, 'module_id')
            ->where('module', 'marketplace')
            ->where('fee_type', 'discount');
    }

    /**
     * Get the options for the activity log.
     *
     * @return LogOptions The options for the activity log.
     */
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->useLogName('Market Place Log');
    }
}
