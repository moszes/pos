<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VendorTax extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'vendor_id',
        'company',
        'npwp',
        'npwp_name',
        'tax_address',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->useLogName('Vendor Tax Log');
    }
}
