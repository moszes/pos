<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Treatment extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'branch_id',
        'category_id',
        'sub_category_id',
        'code',
        'name',
        'price',
        'subscription_plan',
    ];

    /**
     * Get the items associated with the service.
     */
    public function material_usages(): HasMany
    {
        // Return the hasMany relationship for the ServiceItem model
        return $this->hasMany(TreatmentMaterialUsage::class);
    }

    /**
     * Define a many-to-many relationship between Treatment and SubscriptionPlan models.
     */
    public function subscriptionPlans(): BelongsToMany
    {
        return $this->belongsToMany(SubscriptionPlan::class, 'subscription_plan_treatment', 'treatment_id', 'subscription_plan_id');
    }

    /**
     * Get the normal fees associated with the product.
     */
    public function targetFees(): HasMany
    {
        // This method returns the relationship between the product and the ProductFee model
        // where the fee type is 'normal'.
        return $this->hasMany(ServiceFee::class, 'module_id')
            ->where('module', 'treatment')
            ->where('fee_type', 'target');
    }

    /**
     * Get the normal fees associated with the product.
     */
    public function normalFees(): HasMany
    {
        // This method returns the relationship between the product and the ProductFee model
        // where the fee type is 'normal'.
        return $this->hasMany(ServiceFee::class, 'module_id')
            ->where('module', 'treatment')
            ->where('fee_type', 'normal');
    }

    /**
     * Get the discount fees associated with the product.
     */
    public function discountFees(): HasMany
    {
        // This method returns the relationship between the product and the ProductFee model
        // where the fee type is 'discount'.
        return $this->hasMany(ServiceFee::class, 'module_id')
            ->where('module', 'treatment')
            ->where('fee_type', 'discount');
    }

    /**
     * This method establishes a relationship between the Treatment model and the Category model.
     * It is a one-to-many relationship where a Treatment belongs to a Category.
     */
    public function category(): BelongsTo
    {
        // Return the relationship between the Treatment model and the Category model
        return $this->belongsTo(Category::class);
    }

    /**
     * This method establishes a relationship between the Treatment model and the SubCategory model.
     * It is a one-to-many relationship where a Treatment belongs to a SubCategory.
     */
    public function sub_category(): BelongsTo
    {
        // Return the relationship between the Treatment model and the SubCategory model
        return $this->belongsTo(SubCategory::class);
    }

    /**
     * Retrieves the options for the activity log.
     *
     * @return LogOptions The options for the activity log.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Set the default log options
        $options = LogOptions::defaults();

        // Enable logging of fillable attributes
        $options->logFillable();

        // Set the log name to 'Service Log'
        $options->useLogName('Treatment Log');

        // Return the modified options
        return $options;
    }
}
