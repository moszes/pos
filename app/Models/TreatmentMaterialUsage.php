<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class TreatmentMaterialUsage extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'treatment_id',
        'product_id',
        'normal_price',
        'agent_price',
        'qty',
        'usage',
        'size',
        'unit_name',
        'cost',
    ];

    /**
     * Get the treatment for this record.
     */
    public function treatment(): BelongsTo
    {
        return $this->belongsTo(Treatment::class);
    }

    /**
     * Get the activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Return the default log options with additional settings applied.
        return LogOptions::defaults()
            ->logFillable()
            ->useLogName('Treatment Material Usage Log');
    }
}
