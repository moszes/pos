<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class SalesInvoiceItem extends Model
{
    use LogsActivity, SoftDeletes;

    protected $fillable = [
        'sales_invoice_id',
        'module',
        'module_id',
        'code',
        'name',
        'doctor_id',
        'doctor_assistance_id',
        'therapist_id',
        'sales_id',
        'doctor_fee',
        'doctor_assistance_fee',
        'therapist_fee',
        'sales_fee',
        'room_id',
        'room_start',
        'room_end',
        'qty',
        'price',
        'sell_price',
        'discount_item',
        'total_price',
        'total_sell_price',
        'transaction_status',
    ];

    /**
     * Get the treatment associated with the module.
     */
    public function treatment(): BelongsTo
    {
        // Define the relationship with the Treatment model
        return $this->belongsTo(Treatment::class, 'module_id');
    }

    /**
     * Retrieves the commission records associated with the given sales invoice.
     *
     * @return HasMany The commission records associated with the sales invoice.
     */
    public function commissions(): HasMany
    {
        // Return the relationship between the sales invoice and the commissions
        return $this->hasMany(Commission::class);
    }

    /**
     * Get the activity log options.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Set the default log options
        // Customize the log options by logging fillable attributes
        // Use the log name 'Sales Invoice Item Log'
        return LogOptions::defaults()
            ->logFillable()
            ->useLogName('Sales Invoice Item Log');
    }
}
