<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class TreatmentFee extends Model
{
    use HasFactory, LogsActivity, SoftDeletes;

    protected $fillable = [
        'treatment_id',
        'role',
        'value',
        'type',
    ];

    /**
     * Get the product associated with the model.
     */
    public function product(): BelongsTo
    {
        // Return the relationship between the model and the Treatment class
        return $this->belongsTo(Treatment::class);
    }

    /**
     * Retrieve the activity log options for the Treatment Fee Log.
     */
    public function getActivitylogOptions(): LogOptions
    {
        // Create a new LogOptions object with default options
        $options = LogOptions::defaults();

        // Enable logging of fillable attributes
        $options->logFillable();

        // Set the log name to 'Treatment Fee Log'
        $options->useLogName('Treatment Fee Log');

        // Return the modified LogOptions object
        return $options;
    }
}
