<?php

namespace App\Http\Controllers;

use App\Models\Package;

class ApiPackageController extends Controller
{
    public function packages()
    {
        $packages = Package::withTrashed()->get();

        foreach ($packages as $key => $value) {
            $packages[$key]['fee'] = @$value->fee;
            $packages[$key]['effective_date_sales'] = @$value->effective_date_sales;
            $packages[$key]['items'] = @$value->items;
        }

        return $packages;
    }
}
