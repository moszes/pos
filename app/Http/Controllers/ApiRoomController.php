<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Http\Request;

class ApiRoomController extends Controller
{
    public function rooms(Request $request)
    {
        return Room::withTrashed()->get();
    }
}
