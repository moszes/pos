<?php

namespace App\Http\Controllers;

use App\Models\SalesInvoice;
use Spatie\SimpleExcel\SimpleExcelWriter;

class GenerateExcel extends Controller
{
    /**
     * Download a sales invoice report.
     */
    public static function download(): void
    {
        // Define the request types and the corresponding method to call
        $requests = [
            'salesInvoice' => 'downloadSalesInvoice',
            'salesInvoices' => 'downloadSalesInvoices',
            'salesSummaries' => 'downloadSalesSummary',
            'salesDetails' => 'downloadSalesDetail',
            'customers' => 'downloadCustomers',
        ];

        // Use the new method
        self::processRequest($requests);
    }

    /**
     * Processes the requests based on the specified request types.
     *
     * @param  array  $requests  The array of request types.
     */
    public static function processRequest(array $requests): void
    {
        // Loop through the request types
        foreach ($requests as $request => $method) {
            if (request($request)) {
                call_user_func((self::class.'::'.$method)(...));
                break;
            }
        }
    }

    /**
     * Download customer details as a CSV file.
     */
    public static function downloadCustomers(): void
    {
        // Fetch all customers into an array
        $customers = new CustomerController;
        $customers = $customers->customers();

        // Create a new instance of SimpleExcelWriter
        $writer = SimpleExcelWriter::streamDownload('customers_data'.time().'.xlsx');

        // Define the heading of the CSV file
        $writer->addHeader([
            'Code',
            'Phone',
            'Username',
            'FirstName',
            'LastName',
            'Address',
            'Email',
            'Instagram',
            'Date Of Birth',
            'Place Of Birth',
        ]);

        // Iterate through each customer and add their information as a row
        foreach ($customers->toArray() as $customer) {
            $writer->addRow([
                $customer['details']['code'],
                $customer['details']['phone'],
                $customer['name'],
                $customer['details']['first_name'],
                $customer['details']['last_name'],
                $customer['details']['address'],
                $customer['details']['email'],
                $customer['details']['instagram'],
                $customer['details']['date_of_birth'],
                $customer['details']['place_of_birth'],
            ]);
        }

        // Output the file to the browser for download
        $writer->toBrowser();
    }

    /**
     * Downloads the sales invoice report in XLSX format.
     */
    public static function downloadSalesInvoice(): void
    {
        // Get the keys to remove from the sales invoice
        $arrayKeyToRemove = self::getArrayKeyToRemove();
        // Get the CSV header for the report
        $csvHeader = self::getCsvHeader();

        // Parse the salesInvoice parameter as JSON and convert it to an array
        $salesInvoice = collect(json_decode(request('salesInvoice')))->toArray();

        // Remove unnecessary keys from the sales invoice
        $salesInvoice = self::getInvoiceRow($salesInvoice, $arrayKeyToRemove);

        // Create a new instance of SimpleExcelWriter and stream download the report
        SimpleExcelWriter::streamDownload('sales_invoice_report_by_row'.time().'.xlsx')
            ->addHeader($csvHeader)
            ->addRow($salesInvoice)
            ->toBrowser();
    }

    /**
     * Download sales invoices matching the provided date as an Excel file.
     */
    public static function downloadSalesInvoices(): void
    {
        // Get array keys to remove from the invoice rows
        $arrayKeyToRemove = self::getArrayKeyToRemove();

        // Get the CSV header for the Excel file
        $csvHeader = self::getCsvHeader();

        // Fetch all sales invoices matching the provided date
        $salesInvoices = SalesInvoice::where('sales_invoice_date', 'like', '%'.request('date').'%')->get()->toArray();

        // Create the Excel writer and set the file name
        $writer = SimpleExcelWriter::streamDownload('sales_invoice_report'.time().'.xlsx');

        // Add the CSV header as the first row in the Excel file
        $writer->addHeader($csvHeader);

        // Add each sales invoice as a row in the Excel file
        foreach ($salesInvoices as $salesInvoice) {
            $salesInvoice = self::getInvoiceRow($salesInvoice, $arrayKeyToRemove); // Remove unnecessary keys
            $writer->addRow($salesInvoice);
        }

        // Send the generated Excel file to the browser for download
        $writer->toBrowser();
    }

    /**
     * Downloads the sales summary report based on the specified date range and optional customer filter.
     */
    public static function downloadSalesSummary(): void
    {
        // Get sales summaries based on the date range
        $salesSummaries = SalesInvoice::where('sales_invoice_date', '>=', request('dateFrom').' 00:00:00')
            ->where('sales_invoice_date', '<=', request('dateTo').' 23:59:59');

        // Filter sales summaries by customer if specified
        if (request('customer')) {
            $salesSummaries = $salesSummaries->where('customer_id', request('customer'));
        }

        // Convert sales summaries to array
        $salesSummaries = $salesSummaries->get()->toArray();

        // Convert each item in the sales summaries to a collection
        foreach ($salesSummaries as $key => $salesSummary) {
            $salesSummaries[$key] = collect($salesSummary)->toArray();
        }

        // Create a new instance of SimpleExcelWriter and stream download the report
        $writer = SimpleExcelWriter::streamDownload('sales_invoice_summary_report_'.time().'.xlsx');
        $writer->addHeader([
            'Date',
            'Invoice No.',
            'Customer',
            'Phone',
            'Sub Total',
            'Tax',
            'Others',
            'Discount',
            'Grand Total',
            'Cash',
            'Card',
            'Custom',
            'Prepaid',
            'Qr Code',
            'Voucher',
        ]);

        // Populate the report with sales summary details
        foreach ($salesSummaries as $salesSummary) {
            $customer = getCustomerById($salesSummary['customer_id']);
            $salesInvoice = SalesInvoice::where('sales_invoice_number', $salesSummary['sales_invoice_number'])->first();
            $customRow = [
                dateFormat($salesSummary['sales_invoice_date']),
                $salesSummary['sales_invoice_number'],
                $customer['details']['first_name'].' '.$customer['details']['last_name'],
                $customer['details']['phone'],
                numberFormat($salesInvoice->items->sum('total_price')),
                numberFormat($salesInvoice->tax),
                numberFormat($salesInvoice->others),
                numberFormat($salesInvoice->discount),
                numberFormat($salesInvoice->grand_total),
                numberFormat($salesInvoice->payments->where('payment_id', '1')->sum('amount')),
                numberFormat($salesInvoice->payments->where('payment_id', '2')->sum('amount')),
                numberFormat($salesInvoice->payments->where('payment_id', '3')->sum('amount')),
                numberFormat($salesInvoice->payments->where('payment_id', '4')->sum('amount')),
                numberFormat($salesInvoice->payments->where('payment_id', '5')->sum('amount')),
                numberFormat($salesInvoice->payments->where('payment_id', '6')->sum('amount')),
            ];
            $writer->addRow($customRow);
        }

        // Output the report to the browser
        $writer->toBrowser();
    }

    /**
     * Download sales detail report
     */
    public static function downloadSalesDetail(): void
    {
        // Get sales details based on date range and valid status
        $salesDetails = SalesInvoice::join('sales_invoice_items', 'sales_invoices.id', '=', 'sales_invoice_items.sales_invoice_id')
            ->where('sales_invoices.sales_invoice_date', '>=', request('dateFrom').' 00:00:00')
            ->where('sales_invoices.sales_invoice_date', '<=', request('dateTo').' 23:59:59')
            ->whereNull('sales_invoices.deleted_at')
            ->whereNull('sales_invoice_items.deleted_at')
            ->where('sales_invoices.sales_invoice_status', 'valid');

        // Filter sales details by customer if specified
        if (request('customer')) {
            $salesDetails = $salesDetails->where('sales_invoices.customer_id', request('customer'));
        }

        // Convert sales details to array
        $salesDetails = $salesDetails->get()->toArray();

        // Convert each sales detail to an associative array
        foreach ($salesDetails as $key => $salesDetail) {
            $salesDetails[$key] = collect($salesDetail)->toArray();
        }

        // Create a new instance of SimpleExcelWriter and stream download the report
        $writer = SimpleExcelWriter::streamDownload('sales_invoice_detail_report_'.time().'.xlsx');
        $writer->addHeader([
            'Date',
            'Invoice No.',
            'Customer',
            'Phone',
            'Doctor',
            'Assistance',
            'Therapist',
            'Sales',
            'module',
            'code',
            'name',
            'Price',
            'Qty',
            'DISC',
            'Total Price',
            'Payment Type',
        ]);

        // Build custom rows and add to the Excel writer
        foreach ($salesDetails as $salesDetail) {
            $customer = @getCustomerById($salesDetail['customer_id']);
            $customRow = [
                @dateFormat($salesDetail['sales_invoice_date']),
                @$salesDetail['sales_invoice_number'],
                @$customer['details']['first_name'].' '.$customer['details']['last_name'],
                @$customer['details']['phone'],
                @getUserById($salesDetail['doctor_id'])->details->first_name.' '.@getUserById($salesDetail['doctor_id'])->details->last_name,
                @getUserById($salesDetail['doctor_assistance_id'])->details->first_name.' '.@getUserById($salesDetail['doctor_assistance_id'])->details->last_name,
                @getUserById($salesDetail['therapist_id'])->details->first_name.' '.@getUserById($salesDetail['therapist_id'])->details->last_name,
                @getUserById($salesDetail['sales_id'])->details->first_name.' '.@getUserById($salesDetail['sales_id'])->details->last_name,
                @$salesDetail['module'],
                @$salesDetail['code'],
                @$salesDetail['name'],
                @numberFormat($salesDetail['price']),
                @numberFormat($salesDetail['qty']),
                @numberFormat($salesDetail['discount_item']),
                @numberFormat(floatval($salesDetail['price']) * $salesDetail['qty'] - $salesDetail['discount_item']),
                @getPaymentBySalesInvoiceId($salesDetail['sales_invoice_id']),
            ];

            $writer->addRow($customRow);
        }
        // Stream the report for download
        $writer->toBrowser();
    }

    /**
     * Retrieves specific details from a sales invoice and removes specified array keys.
     *
     * @param  mixed  $salesInvoice  The sales invoice data.
     * @param  array  $arrayKeyToRemove  The array keys to be removed.
     * @return array The modified sales invoice data.
     */
    public static function getInvoiceRow(mixed $salesInvoice, array $arrayKeyToRemove): array
    {
        // Retrieve customer-first name and phone from customer details
        $salesInvoice['customer_first_name'] = getCustomerById($salesInvoice['customer_id'])['details']['first_name'];
        $salesInvoice['customer_phone'] = getCustomerById($salesInvoice['customer_id'])['details']['phone'];

        // Retrieve appointment number and cashier details
        $salesInvoice['appointment_number'] = @getAppointmentById($salesInvoice['appointment_id'])->appointment_number;
        $salesInvoice['cashier'] = @getUserById($salesInvoice['created_by'])->details->first_name;

        // Retrieve release by details
        $salesInvoice['release_by'] = @getUserById($salesInvoice['release_by'])->details->first_name;

        // Remove specified array keys
        return removeArrayKeys($salesInvoice, $arrayKeyToRemove);
    }

    /**
     * Returns an array of keys to remove from the sales invoice data.
     */
    public static function getArrayKeyToRemove(): array
    {
        return [
            'id', 'company_id', 'branch_id', 'customer_id', 'customer',
            'appointment_id', 'created_by', 'is_discount', 'deleted_at',
            'created_at', 'updated_at', 'sales_invoice_status', 'appointment',
        ];
    }

    /**
     * Get the CSV header for the sales invoice report.
     */
    public static function getCsvHeader(): array
    {
        return [
            'INV_No', 'INV_Date', 'Release By', 'Release Date', 'Discount', 'Tax',
            'Others', 'Grand Total', 'Total Payment', 'Note', 'Customer First Name',
            'Customer Phone', 'Appointment Number', 'Cashier',
        ];
    }
}
