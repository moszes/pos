<?php

namespace App\Http\Controllers;

use App\Models\SalesInvoice;
use Carbon\Carbon;

class ApiSalesInvoiceController extends Controller
{
    public static function getSelectedMonthData($date): false|string
    {
        $monthlySalesData = [];

        return self::dataByMonth($date, $monthlySalesData);
    }

    public static function getBeforeSelectedMonthData($date): false|string
    {
        $monthlySalesData = [];

        $beforeDate = Carbon::parse($date)->subMonth()->format('Y-m-d');

        return self::dataByMonth($beforeDate, $monthlySalesData);
    }

    public static function dataByMonth($date, array $monthlySalesData): string|false
    {
        $daysInMonth = cal_days_in_month(
            CAL_GREGORIAN,
            date('m', strtotime($date)
            ),
            date('Y', strtotime($date)
            )
        );

        for ($day = 1; $day <= $daysInMonth; $day++) {
            $salesByDate = SalesInvoice::where(
                'sales_invoice_status',
                'valid'
            )->whereDate(
                'sales_invoice_date',
                date('Y-m-d', strtotime($date.'-'.$day))
            )->sum('grand_total');

            $monthlySalesData[] = countFormat($salesByDate);
        }

        return json_encode($monthlySalesData);
    }

    /**
     * Retrieve all sales invoices, including the soft deleted ones.
     */
    public function sales_invoices(): array
    {
        $sales_invoices = SalesInvoice::withTrashed()->get();

        foreach ($sales_invoices as $key => $value) {
            $sales_invoices[$key]['items'] = @$value->items;
        }

        return $sales_invoices;
    }

    /**
     * Retrieve a sales invoice by its invoice number
     */
    public function getSalesInvoiceByInvoiceNumber(string $salesInvoiceNumber): array
    {
        // Retrieve the sales invoice with the given invoice number
        $salesInvoice = SalesInvoice::withTrashed()
            ->where('sales_invoice_number', $salesInvoiceNumber)
            ->first();

        // Retrieve the items related to the sales invoice
        $salesInvoice['items'] = $salesInvoice->items;

        // Return the sales invoice details
        return $salesInvoice;
    }

    /**
     * Get a sales invoice by its ID.
     *
     * @param  int  $id  The ID of the sales invoice.
     */
    public function getSalesInvoiceById(int $id): SalesInvoice|array|null
    {
        return SalesInvoice::find($id) ?? null;
    }
}
