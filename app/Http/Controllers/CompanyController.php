<?php

namespace App\Http\Controllers;

use App\Models\Company;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Random\RandomException;

class CompanyController extends Controller
{
    /**
     * Retrieve all companies.
     *
     * @return Collection
     */
    public function index()
    {
        // Retrieve all companies from the database
        return Company::all();
    }

    /**
     * Store a new company.
     *
     * @throws RandomException
     */
    public function store(Request $request)
    {
        $validatedData = $this->validateRequest($request);
        $processedData = $this->processOptionalData($request, $validatedData);
        $company = $this->createNewCompany($processedData);

        return response()->json([
            'message' => 'created',
            'data' => $company,
        ], 201);
    }

    /**
     * Validate the request data.
     *
     * @param  Request  $request  The HTTP request object.
     * @return array The validated data.
     */
    private function validateRequest(Request $request): array
    {
        return $request->validate([
            'email' => 'required|email',
            'name' => 'required',
            'phone' => 'required',
        ]);
    }

    /**
     * Generate a unique company code.
     *
     * @return string
     *
     * @throws RandomException
     */
    public function generateCompanyCode()
    {
        do {
            $code = $this->createRandomCode();
        } while ($this->isCodeExists($code));

        return $code;
    }

    /**
     * Generate a random code.
     *
     * @return string They generated random code.
     *
     * @throws RandomException
     */
    private function createRandomCode(): string
    {
        return bin2hex(random_bytes(5)); // Generate a random string.
    }

    /**
     * Check if the code already exists in the Company table.
     *
     * @param  string  $code  The code to check
     * @return bool Returns true if the code exists, false otherwise
     */
    private function isCodeExists(string $code): bool
    {
        return Company::where('code', '=', $code)->exists(); // Check if the code exists
    }

    /**
     * Process optional data for a new company.
     *
     * @param  Request  $request  The incoming request.
     * @param  array  $data  The validated data.
     * @return array The processed data.
     *
     * @throws RandomException
     */
    private function processOptionalData(Request $request, array $data): array
    {
        $data['website'] = $request->website ?? null;
        $data['address'] = $request->address ?? null;
        $data['code'] = $this->generateCompanyCode();

        return $data;
    }

    /**
     * Create a new company in the database.
     *
     * @param  array  $data  The data to create the company with.
     * @return Company The created company instance.
     */
    private function createNewCompany(array $data): Company
    {
        return Company::create($data);
    }

    /**
     * Update an existing company.
     *
     * @return void
     */
    public function update(int $id, Request $request)
    {
        // Validate request data
        $data = $request->validate([
            'email' => 'required',
            'name' => 'required',
            'phone' => 'required',
        ]);

        // Set optional fields to null if not provided
        $data['website'] = $request->website ?? null;
        $data['address'] = $request->address ?? null;

        // Update a company
        $company = Company::find($id);

        DB::transaction(function () use ($company, $data) {

            try {
                $company->update($data);

                return response()->json([
                    'message' => 'Company updated successfully',
                ]);
            } catch (Exception) {
                // Return error response
                return response()->json([
                    'message' => 'Failed to update company',
                ], 500);
            }
        }, 5);
    }

    /**
     * Delete an existing company.
     *
     * @return JsonResponse
     */
    public function destroy(int $id)
    {
        // Delete company
        $company = Company::find($id);

        try {
            $company->delete();

            return response()->json([
                'message' => 'Company deleted successfully',
            ]);
        } catch (Exception) {
            // Return error response
            return response()->json([
                'message' => 'Failed to delete company',
            ], 500);
        }

    }
}
