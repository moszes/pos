<?php

namespace App\Http\Controllers;

use App\Models\Treatment;
use Illuminate\Support\Collection;

class ApiTreatmentController extends Controller
{
    public function treatments(): array|Collection
    {
        $treatments = Treatment::withTrashed()->get();

        foreach ($treatments as $key => $value) {
            $treatments[$key]['items'] = @$value->items;
            $treatments[$key]['fee'] = @$value->fee;
        }

        return $treatments;
    }
}
