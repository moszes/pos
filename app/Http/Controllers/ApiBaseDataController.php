<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\SubCategory;
use App\Models\UnitOfMeasure;
use Illuminate\Http\Request;

class ApiBaseDataController extends Controller
{
    public function categories(Request $request)
    {

        return Category::withTrashed()->get();
    }

    public function sub_categories()
    {
        return SubCategory::withTrashed()->get();
    }

    public function unit_of_measures()
    {
        return UnitOfMeasure::withTrashed()->get();
    }
}
