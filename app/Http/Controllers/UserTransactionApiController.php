<?php

namespace App\Http\Controllers;

use App\Models\PackageItem;
use App\Models\UserTransaction;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class UserTransactionApiController extends Controller
{
    /**
     * Creates a package transaction.
     *
     * @param  array  $transactionData  The item data for the package transaction.
     */
    public function createPackageHistoryTransaction(array $transactionData): void
    {
        $companyId = auth()->user()->details->branch_id;
        $branchId = auth()->user()->details->branch_id;

        $transaction = [
            'company_id' => $companyId,
            'branch_id' => $branchId,
            'sales_invoice_id' => $transactionData['sales_invoice_id'],
            'appointment_id' => $transactionData['appointment_id'] ?? null,
            'user_id' => $transactionData['customer_id'],
            'doctor_id' => $transactionData['doctor_id'] ?? null,
            'doctor_assistance_id' => $transactionData['doctor_assistance_id'] ?? null,
            'therapist_id' => $transactionData['therapist_id'] ?? null,
            'sales_id' => $transactionData['sales_id'] ?? null,
            'doctor_fee' => $transactionData['doctor_fee'],
            'doctor_assistance_fee' => $transactionData['doctor_assistance_fee'],
            'therapist_fee' => $transactionData['therapist_fee'],
            'sales_fee' => $transactionData['sales_fee'],
            'module' => $transactionData['module'],
            'module_id' => $transactionData['module_id'],
            'module_code' => $transactionData['code'],
            'module_name' => $transactionData['name'],
            'reference_module' => $transactionData['reference_module'] ?? null,
            'reference_id' => $transactionData['reference_id'] ?? null,
            'transaction_status' => $transactionData['transaction_status'] ?? null,
            'qty' => 0,
            'discount_item' => $transactionData['discount_item'],
            'item_price' => $transactionData['price'],
            'sell_price' => $transactionData['sell_price'],
            'total_price' => $transactionData['total_price'],
            'total_sell_price' => $transactionData['total_sell_price'],
        ];

        UserTransaction::create($transaction);

        $this->deployTreatment($transactionData);
    }

    /**
     * Deploy treatment items related to a package.
     *
     * @param  array  $transactionData  The transaction data containing necessary information.
     *                                  The array should have the following keys:
     *                                  - 'module_id': the ID of the package module
     *                                  - 'sales_invoice_id': the ID of the sales invoice
     *                                  - 'appointment_id': (optional) the ID of the appointment
     *                                  - 'customer_id': the ID of the customer
     *                                  - 'transaction_status': (optional) the transaction status
     *                                  - 'qty': the quantity of the package
     */
    public function deployTreatment(array $transactionData): void
    {
        // Retrieve the treatment items related to the package from the PackageItem model
        $treatmentItems = PackageItem::where('package_id', $transactionData['module_id'])
            ->where('module', 'treatment')
            ->get();

        // Iterate over each treatment item and create a new UserTransaction for each
        foreach ($treatmentItems as $treatmentItem) {
            // Prepare the dataset for creating a new UserTransaction
            $dataSet = [
                'company_id' => auth()->user()->details->branch_id,
                'branch_id' => auth()->user()->details->branch_id,
                'sales_invoice_id' => $transactionData['sales_invoice_id'],
                'appointment_id' => $transactionData['appointment_id'] ?? null,
                'user_id' => $transactionData['customer_id'],
                'doctor_id' => $treatmentItem->doctor_id ?? null,
                'doctor_assistance_id' => $treatmentItem->doctor_assistance_id ?? null,
                'therapist_id' => $treatmentItem->therapist_id ?? null,
                'sales_id' => $treatmentItem->sales_id ?? null,
                'doctor_fee' => getTreatmentFee($treatmentItem->module_id, 'doctor'),
                'doctor_assistance_fee' => getTreatmentFee($treatmentItem->module_id, 'doctor_assistance'),
                'therapist_fee' => getTreatmentFee($treatmentItem->module_id, 'therapist'),
                'sales_fee' => getTreatmentFee($treatmentItem->module_id, 'sales'),
                'module' => $treatmentItem->module,
                'module_id' => $treatmentItem->module_id,
                'module_code' => getDataByModule('treatment', $treatmentItem->module_id)->code,
                'module_name' => getDataByModule('treatment', $treatmentItem->module_id)->name,
                'reference_module' => 'package',
                'reference_id' => $transactionData['module_id'],
                'qty' => floatval($treatmentItem->qty) * $transactionData['qty'],
                'transaction_status' => $transactionData['transaction_status'] ?? null,
                'item_price' => getDataByModule($treatmentItem->module, $treatmentItem->module_id)->price,
                'sell_price' => getDataByModule($treatmentItem->module, $treatmentItem->module_id)->price,
                'total_price' => floatval($treatmentItem->qty) * getDataByModule($treatmentItem->module, $treatmentItem->module_id)->price,
                'total_sell_price' => floatval($treatmentItem->qty) * getDataByModule($treatmentItem->module, $treatmentItem->module_id)->price,
            ];

            // Create a new UserTransaction using the dataset
            UserTransaction::create($dataSet);
        }
    }

    /**
     * Creates a treatment history transaction.
     *
     * This method is responsible for creating a treatment history transaction.
     * It takes an array of item data as input and performs the following actions:
     *  - Sets the necessary data for the transaction in a $dataSet array.
     *  - Uses the UserTransaction::create() method to create a new user transaction with the $dataSet as input.
     *
     * @param  array  $item  The item data for the transaction.
     */
    public function createTreatmentHistoryTransaction(array $item): void
    {
        // Check if the quantity is less than 5 and the transaction status is not 'redemption'
        if ($item['qty'] < 5 && $item['transaction_status'] !== 'redemption') {
            // Set the quantity to 0 in such cases
            $item['qty'] = 0;
        }

        // Set the necessary data for the transaction in a $dataSet array
        $dataSet = [
            'company_id' => auth()->user()->details->branch_id, // Get the ID of the first company
            'branch_id' => auth()->user()->details->branch_id, // Get the ID of the first branch
            'sales_invoice_id' => $item['sales_invoice_id'], // Get the ID of the sales invoice
            'appointment_id' => @$item['appointment_id'], // Get the ID of the appointment
            'user_id' => $item['customer_id'], // Get the ID of the customer
            'doctor_id' => $item['doctor_id'], // Get the doctor ID from the item data
            'doctor_assistance_id' => $item['doctor_assistance_id'], // Get the doctor assistance ID from the item data
            'therapist_id' => $item['therapist_id'], // Get the therapist ID from the item data
            'sales_id' => $item['sales_id'], // Get the sales ID from the item data
            'doctor_fee' => $item['doctor_fee'], // Get the doctor fee from the item data
            'doctor_assistance_fee' => $item['doctor_assistance_fee'], // Get the doctor assistance fee from the item data
            'therapist_fee' => $item['therapist_fee'], // Get the therapist fee from the item data
            'sales_fee' => $item['sales_fee'], // Get the sales fee from the item data
            'default_promo' => @getDefaultPromo($item['qty']), // Get the default promo based on the quantity
            'module' => $item['module'], // Get the module from the item data
            'module_id' => $item['module_id'], // Get the module ID from the item data
            'module_code' => getDataByModule('treatment', $item['module_id'])->code,
            'module_name' => getDataByModule('treatment', $item['module_id'])->name,
            'reference_module' => @$item['reference_module'], // Get the reference module from the item data
            'reference_id' => @$item['reference_id'], // Get the reference ID from the item data
            'transaction_status' => $item['transaction_status'], // Get the transaction status from the item data
            'qty' => $item['qty'], // Get the quantity from the item data
            'discount_item' => $item['discount_item'], // Get the discount item from the item data
            'item_price' => $item['price'], // Get the price from the item data
            'sell_price' => $item['sell_price'], // Get the sell price from the item data
            'total_price' => $item['total_price'], // Get the total price from the item data
            'total_sell_price' => $item['total_sell_price'], // Get the total sell price from the item data
        ];

        // Create a new user transaction with the $dataSet as input
        UserTransaction::create($dataSet);
    }

    /**
     * Creates a marketplace history transaction.
     *
     * This method is responsible for creating a marketplace history transaction.
     * It takes an array of item data as input and performs the following actions:
     *  - Sets the necessary data for the transaction in a $dataSet array.
     *  - Uses the UserTransaction::create() method to create a new user transaction with the $dataSet as input.
     *
     * @param  array  $item  The item data for the transaction.
     */
    public function createMarketPlaceHistoryTransaction(array $item): void
    {
        // Set up the dataset array with the necessary values
        $dataSet = [
            'company_id' => auth()->user()->details->branch_id, // Get the ID of the first company
            'branch_id' => auth()->user()->details->branch_id, // Get the ID of the current branch
            'sales_invoice_id' => $item['sales_invoice_id'], // Get the sales invoice ID from the item data
            'user_id' => $item['customer_id'], // Get the customer ID from the item data
            'doctor_id' => $item['doctor_id'], // Get the doctor ID from the item data
            'doctor_assistance_id' => $item['doctor_assistance_id'], // Get the sales ID from the item data
            'therapist_id' => $item['therapist_id'], // Get the therapist ID from the item data
            'sales_id' => $item['sales_id'], // Get the sales ID from the item data
            'doctor_fee' => $item['doctor_fee'], // Get the sales fee from the item data
            'doctor_assistance_fee' => $item['doctor_assistance_fee'], // Get the sales fee from the item data
            'therapist_fee' => $item['therapist_fee'], // Get the sales fee from the item data
            'sales_fee' => $item['sales_fee'], // Get the sales fee from the item data
            'module' => $item['module'], // Get the module from the item data
            'module_id' => $item['module_id'], // Get the module ID from the item data
            'module_code' => getDataByModule('marketplace', $item['module_id'])->code,
            'module_name' => getDataByModule('marketplace', $item['module_id'])->name,
            'reference_module' => $item['reference_module'] ?? null, // Get the reference module from the item data or set it to null if it doesn't exist
            'reference_id' => $item['reference_id'] ?? null, // Get the reference ID from the item data or set it to null if it doesn't exist
            'qty' => $item['qty'], // Get the quantity from the item data
            'discount_item' => $item['discount_item'], // Get the discount item from the item data
            'item_price' => $item['price'], // Get the price from the item data
            'sell_price' => $item['sell_price'], // Get the sell price from the item data
            'total_price' => $item['total_price'], // Get the total price from the item data
            'total_sell_price' => $item['total_sell_price'], // Get the total sell price from the item data
        ];

        // Create the user transaction
        UserTransaction::create($dataSet);

    }

    /**
     * Create a product history transaction.
     *
     * @param  array  $item  The item data for the transaction.
     */
    public function createProductHistoryTransaction(array $item): void
    {
        // Set up the dataset array with the necessary values
        $dataSet = [
            'company_id' => auth()->user()->details->branch_id, // Get the ID of the first company
            'branch_id' => auth()->user()->details->branch_id, // Get the ID of the current branch
            'sales_invoice_id' => $item['sales_invoice_id'], // Get the sales invoice ID from the item data
            'user_id' => $item['customer_id'], // Get the customer ID from the item data
            'doctor_id' => $item['doctor_id'], // Get the doctor ID from the item data
            'doctor_assistance_id' => $item['doctor_assistance_id'], // Get the sales ID from the item data
            'therapist_id' => $item['therapist_id'], // Get the therapist ID from the item data
            'sales_id' => $item['sales_id'], // Get the sales ID from the item data
            'doctor_fee' => $item['doctor_fee'], // Get the sales fee from the item data
            'doctor_assistance_fee' => $item['doctor_assistance_fee'], // Get the sales fee from the item data
            'therapist_fee' => $item['therapist_fee'], // Get the sales fee from the item data
            'sales_fee' => $item['sales_fee'], // Get the sales fee from the item data
            'default_promo' => getDefaultPromo($item['qty']), // Get the default promo based on the quantity from the item data
            'module' => $item['module'], // Get the module from the item data
            'module_id' => $item['module_id'], // Get the module ID from the item data
            'module_code' => $item['code'], // Get the module code from the item data
            'module_name' => $item['name'], // Get the module name from the item data
            'reference_module' => $item['reference_module'] ?? null, // Get the reference module from the item data or set it to null if it doesn't exist
            'reference_id' => $item['reference_id'] ?? null, // Get the reference ID from the item data or set it to null if it doesn't exist
            'qty' => $item['qty'], // Get the quantity from the item data
            'discount_item' => $item['discount_item'], // Get the discount item from the item data
            'item_price' => $item['price'], // Get the price from the item data
            'sell_price' => $item['sell_price'], // Get the sell price from the item data
            'total_price' => $item['total_price'], // Get the total price from the item data
            'total_sell_price' => $item['total_sell_price'], // Get the total sell price from the item data
        ];

        // Create a new UserTransaction using the dataset
        UserTransaction::create($dataSet);
    }

    /**
     * Retrieves an array of user transactions by customer ID.
     *
     * @param  int  $customerId  The ID of the customer.
     * @return array An array of user transactions.
     */
    public function getUserTransactionsByCustomerId(int $customerId): array
    {
        // Retrieve user transactions from the database
        $userTreatments = DB::table('user_transactions')
            ->where('user_id', $customerId)
            ->whereIn('reference_module', ['package', 'seeder'])
            ->orWhere(function ($query) use ($customerId) {
                // Filter user transactions with default promo codes 'B10' or 'B5'
                $query->where('user_id', $customerId)
                    ->WhereIn('default_promo', ['B10', 'B5']);
            })
            ->orWhere(function ($query) use ($customerId) {
                // Filter user transactions with a non-null appointment ID
                $query->where('user_id', $customerId)
                    ->WhereNotNull('appointment_id');
            })
            ->where(function (Builder $query) {
                // Filter user transactions with module 'treatment'
                $query->where('module', 'treatment');
            })
            ->whereNull('deleted_at')
            ->get();

        // Iterate over each user transaction
        foreach ($userTreatments as $key => $userTreatment) {

            // Skip user transactions with a quantity less than 1
            if (floatval($userTreatment->qty) < 1) {
                unset($userTreatments[$key]);

                continue;
            }

            // Set redemption_left to 0 if it is not set
            if (! isset($userTreatments[$key]->redemption_left)) {
                $userTreatments[$key]->redemption_left = 0;
            }

            // Retrieve treatment code and name by module ID
            $userTreatments[$key]->code = getTreatmentById($userTreatment->module_id)?->code;
            $userTreatments[$key]->name = getTreatmentById($userTreatment->module_id)?->name;

            // Calculate redemption_left by summing the qty of user transactions with the same user ID,
            // module, and module ID
            $userTreatment->redemption_left = UserTransaction::where('user_id', $userTreatment->user_id)
                ->where('module', 'treatment')
                ->where('module_id', $userTreatment->module_id)
                ->sum('qty');

            // Skip user transactions with redemption_left less than 1
            if ($userTreatment->redemption_left < 1) {
                unset($userTreatments[$key]);
            }
        }

        // Return an array of unique user transactions based on code
        return $userTreatments->unique('code')->all();
    }
}
