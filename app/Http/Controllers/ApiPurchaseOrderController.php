<?php

namespace App\Http\Controllers;

use App\Models\PurchaseOrder;

class ApiPurchaseOrderController extends Controller
{
    public function purchase_orders()
    {
        $purchase_orders = PurchaseOrder::withTrashed()->get();

        foreach ($purchase_orders as $key => $value) {
            $purchase_orders[$key]['items'] = @$value->items;
        }

        return $purchase_orders;
    }
}
