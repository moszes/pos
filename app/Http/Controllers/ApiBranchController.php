<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ApiBranchController extends Controller
{
    /**
     * Retrieve all branches, including soft deleted ones.
     *
     * @return Collection The collection of branch models.
     */
    public function branches()
    {
        // Retrieve all branches, including soft deleted ones
        return Branch::withTrashed()->get();
    }

    /**
     * Store a new branch.
     *
     * @param  Request  $request  The HTTP request object.
     * @return JsonResponse The created branch model.
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'company_id' => 'required',
        ]);

        $data['code'] = generateBranchCode();

        try {
            $branch = Branch::create($data);

            return response()->json([
                'message' => 'Branch created successfully',
                'company' => $branch,
            ]);
        } catch (Exception) {
            return response()->json([
                'message' => 'Failed to create branch',
            ], 500);
        }
    }

    /**
     * Update an existing branch.
     *
     * @param  Request  $request  The HTTP request object.
     * @param  Branch  $branch  The branch model to update.
     * @return JsonResponse
     */
    public function update(Request $request, Branch $branch)
    {
        // Validate the request data
        $data = $request->validate([
            'name' => 'required',
        ]);

        try {
            $branch->update($data);

            return response()->json([
                'message' => 'Branch updated successfully',
            ]);
        } catch (Exception) {
            return response()->json([
                'message' => 'Failed to update branch',
            ], 500);
        }
    }

    /**
     * Delete an existing branch.
     *
     * @param  Branch  $branch  The branch model to delete.
     * @return JsonResponse
     */
    public function destroy(Branch $branch)
    {
        try {
            $branch->delete();

            return response()->json([
                'message' => 'Branch deleted successfully',
            ]);
        } catch (Exception) {
            return response()->json([
                'message' => 'Failed to delete branch',
            ], 500);
        }
    }
}
