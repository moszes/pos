<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ApiUserController extends Controller
{
    private const TOKEN_EXPIRATION_TIME = 3600; // 1 hour in seconds

    private const TOKEN_RIGHTS = ['*'];

    public function authenticateUser(Request $request): JsonResponse|array
    {
        $credentials = ['name' => $request->name, 'password' => $request->password];

        if (auth()->attempt($credentials)) {
            return $this->createUserToken();
        }

        return $this->unauthorizedResponse();
    }

    private function createUserToken(): array
    {
        $user = auth()->user();
        $token = $user->createToken(time(), self::TOKEN_RIGHTS, Carbon::now()->addSeconds(self::TOKEN_EXPIRATION_TIME));

        return [
            'token' => $token,
            'user' => $user,
        ];
    }

    private function unauthorizedResponse(): JsonResponse
    {
        return response()->json(['unauthorized'], 401);
    }
}
