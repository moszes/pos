<?php

namespace App\Http\Controllers;

use App\Models\User;
use Avatar;
use Illuminate\Database\Eloquent\Collection;

class CustomerController extends Controller
{
    /**
     * Retrieves all customers from the database, including soft-deleted customers.
     *
     * @return Collection A collection of customers.
     */
    public function customers(): Collection
    {
        $customers = User::role('customer')->get();

        foreach ($customers as $key => $value) {
            $customers[$key]['details'] = @$value->details;
        }

        return $customers;
    }

    /**
     * Retrieves the avatar image of a customer.
     *
     * @return string|null The URL of the avatar image or null if it doesn't exist.
     */
    public static function customerAvatarImage($customer)
    {
        if (! $customer) {
            return redirect(route('master.customers'));
        }

        return $customer->details->avatar_file_name == '' ? Avatar::create($customer->details->first_name)->toBase64() : $customer->details->avatar_file_name;
    }
}
