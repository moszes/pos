<?php

namespace App\Http\Controllers;

use App\Models\Vendor;
use Illuminate\Http\Request;

class ApiVendorController extends Controller
{
    public function vendors(Request $request)
    {
        $vendors = Vendor::withTrashed()->get();

        foreach ($vendors as $key => $vendor) {
            $vendors[$key]['tax'] = @$vendor->tax;
            $vendors[$key]['payment'] = @$vendor->payment;
        }

        return $vendors;
    }
}
