<?php

namespace App\Http\Controllers;

use App\Models\ApplicationSetting;
use App\Models\CashDrawer;
use App\Models\SalesInvoice;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Mike42\Escpos\CapabilityProfile;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\Printer;

class ReceiptController extends Controller
{
    public mixed $redirectSalesInvoiceUrl;

    public mixed $redirectTodaySalesUrl;

    public mixed $printerName;

    public $printer;

    public $salesInvoice;

    public function __construct()
    {
        $this->redirectSalesInvoiceUrl = $this->getAppSettingValue('http://localhost:8000/transaction/sales_invoices', 'Redirect Sales Invoice Url');
        $this->redirectTodaySalesUrl = $this->getAppSettingValue('http://localhost:8000/transaction/sales_invoices', 'Redirect Today Sales Url');
        $this->printerName = $this->getAppSettingValue('FK80 Printer', 'Printer Name');
    }

    private function getAppSettingValue($default, $settingName)
    {
        return env('APP_ENV') != 'local'
            ? $default
            : ApplicationSetting::where('company_id', 1)
                ->where('name', $settingName)->first()
                ?->value;
    }

    /**
     * Retrieve the sales invoice based on the provided ID and prepare the receipt.
     *
     * @param  int  $salesInvoiceId  The ID of the sales invoice.
     */
    public function receipt(int $salesInvoiceId): Redirector|RedirectResponse|Application
    {
        // Find the sales invoice based on the provided ID
        $this->salesInvoice = SalesInvoice::find($salesInvoiceId);

        // Get the default printer for printing the receipt
        $this->printer = $this->getPrinterDefault();

        // Generate the header of the receipt
        $this->receiptHeader();

        // Add sales invoice items to the receipt
        $this->receiptSalesInvoiceItems();

        // Include sales invoice payments in the receipt
        $this->receiptSalesInvoicePayments();

        // Add the footer to the receipt

        $this->receiptFooter();

        // Redirect to the specified sales invoice URL
        return redirect($this->redirectSalesInvoiceUrl);
    }

    /**
     * Generate the header of the receipt.
     */
    public function receiptHeader(): void
    {
        // Print customer details
        $this->printer->text('Customer: '.getCustomerById($this->salesInvoice->customer_id)['details']['first_name']."\n");

        // Print transaction date
        $this->printer->text('Tanggal Transaksi : '.$this->salesInvoice->created_at."\n");

        // Print sales invoice number
        $this->printer->text('Nomor Transaksi   : '.$this->salesInvoice->sales_invoice_number."\n");

        // Add spacing
        $this->printer->feed();

        // Set emphasis for the following text
        $this->printer->setEmphasis();

        // Set text justification
        $this->printer->setJustification();

        // Disable emphasis
        $this->printer->setEmphasis(false);

        // Print item header
        $this->printer->text('ITEM              QTY      DISC       PRICE'."\n");

        // Select print mode
        $this->printer->selectPrintMode();

        // Print separator line
        $this->printer->text("_ _ _ _ _ _ _ __ _ _ _ _ _ _ __ _ _ _ _ _ _ _\n");

        // Add spacing
        $this->printer->feed();

        // Disable emphasis
        $this->printer->setEmphasis(false);
    }

    /**
     * Receipt Sales Invoice Items
     *
     * This function prints the sales invoice items and calculates the total amounts on the receipt.
     */
    public function receiptSalesInvoiceItems(): void
    {
        /** Sales Invoice Items */
        if (! empty($this->salesInvoice->items)) {
            foreach ($this->salesInvoice->items as $item) {
                $qtySpace = $item->discount_item > 0 ? '  ' : '    ';
                $discountLength = $item->discount_item > 0 ? 10 : 4;
                $discountSpace = $item->discount_item > 0 ? ' ' : '    ';
                $priceLength = $item->price > 0 ? 11 : 0;
                $this->printer->selectPrintMode();
                /* Print Item Details */
                $this->printer->text(
                    str_pad(getAppointmentModuleId($item->module, $item->module_id)->code, 10).'         '.
                    str_pad(numberFormat($item->qty), 1, ' ', STR_PAD_LEFT).$qtySpace.
                    str_pad(numberFormat($item->discount_item), $discountLength, ' ', STR_PAD_LEFT).$discountSpace.
                    str_pad(numberFormat($item->price), $priceLength, ' ', STR_PAD_LEFT)."\n"
                );
                $this->printer->text(substr(getAppointmentModuleId($item->module, $item->module_id)->name, 0, 15)."\n");
            }
        }

        // Print Subtotal, Discount, Others, and Grand Total
        $this->printer->text("                                _ _ _ _ _ _ _\n");
        $this->printer->setEmphasis();
        $this->printer->text('Sub Total:      '.str_pad(numberFormat($this->salesInvoice->items->sum('total_price')), 28, ' ', STR_PAD_LEFT)."\n");
        $this->printer->text('Discount:     '.str_pad(numberFormat($this->salesInvoice->discount), 30, ' ', STR_PAD_LEFT)."\n");
        $this->printer->text('Others:         '.str_pad(numberFormat($this->salesInvoice->others), 28, ' ', STR_PAD_LEFT)."\n");
        $this->printer->text('Grand Total:  '.str_pad(numberFormat($this->salesInvoice->grand_total), 30, ' ', STR_PAD_LEFT)."\n");

        // Print separator line
        $this->printer->setEmphasis();
        $this->printer->selectPrintMode();
        $this->printer->text("_ _ _ _ _ _ _ __ _ _ _ _ _ _ __ _ _ _ _ _ _ _\n");
        $this->printer->setEmphasis(false);
        $this->printer->feed();
    }

    /**
     * Print Sales Invoice Payments
     *
     * This function prints the payment details of a sales invoice.
     */
    public function receiptSalesInvoicePayments(): void
    {
        // Set emphasis, justification, and print the header
        $this->printer->setEmphasis();
        $this->printer->setJustification();
        $this->printer->setEmphasis(false);
        $this->printer->text('Payment Options    Paid On           Amount'."\n");
        $this->printer->selectPrintMode();
        $this->printer->text("_ _ _ _ _ _ _ __ _ _ _ _ _ _ __ _ _ _ _ _ _ _\n");
        $this->printer->feed();
        $this->printer->setEmphasis(false);

        // Iterate through each payment
        foreach ($this->salesInvoice->payments as $payment) {
            $this->printer->selectPrintMode();
            /** Payment Details */
            $this->printer->text(
                str_pad(getPaymentById($payment->payment_id)->name, 18).' '.
                date('Y-m-d', strtotime($payment->created_at)).'  '.
                str_pad(numberFormat($payment->amount), 12, ' ', STR_PAD_LEFT)."\n"
            );
            if ($payment->reference_number) {
                $this->printer->text('Ref: '.$payment->reference_number."\n");
            } else {
                $this->printer->feed();
            }
        }

        // Print total paid, cashier details, and separator line
        $this->printer->text("                               _ _ _ _ _ _ _\n");
        $this->printer->setEmphasis();
        $this->printer->text('Total Paid:'.str_pad(numberFormat($this->salesInvoice->payments->sum('amount')), 32, ' ', STR_PAD_LEFT)."\n");
        $this->printer->feed(2);
        $this->printer->text('Cashier: '.str_pad(getUserById($this->salesInvoice->created_by)->name, 1, ' ', STR_PAD_LEFT)."\n");
    }

    /**
     * Generate the footer of the receipt.
     */
    public function receiptFooter(): void
    {
        /* Add spacing */
        $this->printer->feed(2);

        /* Center align text */
        $this->printer->setJustification(Printer::JUSTIFY_CENTER);

        /* Print footer lines */
        $this->printer->text("Harga sudah termasuk Pajak.\n");
        $this->printer->text("Terimakasih atas kunjungan anda.\n");
        $this->printer->text("Whatsapp 08111132900\n");
        $this->printer->text("Follow Instagram kami di @ministry.treatment\n");

        /* Add spacing */
        $this->printer->feed(2);

        /* Print sales invoice creation date */
        $this->printer->text($this->salesInvoice->created_at."\n");

        /* Add spacing */
        $this->printer->feed(2);

        /* Close the printer */
        $this->printer->pulse();
        $this->printer->cut();
        $this->printer->close();
    }

    /**
     * Print the closing details of the cash drawer on the receipt and redirect to today's sales URL.
     */
    public function printCloseCashDrawer(): Redirector|RedirectResponse|Application
    {
        // Retrieve the cash drawer details
        $cashDrawer = CashDrawer::where('date', request('date'))
            ->where('company_id', 1)
            ->first();

        // Get the default printer
        $this->printer = $this->getPrinterDefault();

        // Print cash drawer details on the receipt
        $this->printer->text('Last Closure '.$cashDrawer->created_at."\n");
        $this->printer->text('Closed By : '.getUserById($cashDrawer->user_id)->name."\n");
        $this->printer->selectPrintMode();
        $this->printer->text("_ _ _ _ _ _ _ __ _ _ _ _ _ _ __ _ _ _ _ _ _ _\n");
        $this->printer->feed();
        $this->printer->text('Cash: '.str_pad(numberFormat($cashDrawer->cash), '37', ' ', STR_PAD_LEFT)."\n");
        $this->printer->text('Credit/Debit: '.str_pad(numberFormat($cashDrawer->credit_debit), '29', ' ', STR_PAD_LEFT)."\n");
        $this->printer->text('Custom: '.str_pad(numberFormat($cashDrawer->custom), '35', ' ', STR_PAD_LEFT)."\n");
        $this->printer->text('Prepaid: '.str_pad(numberFormat($cashDrawer->prepaid), '34', ' ', STR_PAD_LEFT)."\n");
        $this->printer->text('QrCode: '.str_pad(numberFormat($cashDrawer->qrcode), '35', ' ', STR_PAD_LEFT)."\n");
        $this->printer->text('Voucher: '.str_pad(numberFormat($cashDrawer->voucher), '34', ' ', STR_PAD_LEFT)."\n");
        $this->printer->text('Disc: '.str_pad(numberFormat($cashDrawer->discount * -1), '37', ' ', STR_PAD_LEFT)."\n");
        $this->printer->text('Disc Item: '.str_pad(numberFormat($cashDrawer->discount_item * -1), '32', ' ', STR_PAD_LEFT)."\n");
        $this->printer->text("                               _ _ _ _ _ _ _\n");
        $this->printer->setEmphasis();
        $this->printer->text('Grand Total: '.str_pad(numberFormat($cashDrawer->grand_total), '30', ' ', STR_PAD_LEFT)."\n");
        $this->printer->feed();

        // Close the printer
        $this->printer->pulse();
        $this->printer->cut();
        $this->printer->close();

        // Redirect to today's sales URL
        return redirect($this->redirectTodaySalesUrl);
    }

    /**
     * Get the default printer settings and header for the receipt.
     */
    public function getPrinterDefault(): Printer
    {
        // Load a simple capability profile
        $profile = CapabilityProfile::load('simple');

        // Create a new Windows print connector using the provided printer name
        $connector = new WindowsPrintConnector($this->printerName);

        // Initialize the printer with the connector and profile
        $this->printer = new Printer($connector, $profile);

        /* Title Section */
        $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
        $this->printer->setJustification(Printer::JUSTIFY_CENTER);
        $this->printer->setTextSize(2, 2);
        $this->printer->setEmphasis();
        $this->printer->text("Ministry\n");
        $this->printer->feed();
        $this->printer->selectPrintMode();
        $this->printer->text("Jl. Samanhudi No.17, RT.1/RW.7\n");
        $this->printer->text("Ps. Baru, Kecamatan Sawah Besar\n");
        $this->printer->text("Kota Jakarta Pusat\n");
        $this->printer->text("Daerah Khusus Ibukota Jakarta 10710.\n");
        $this->printer->selectPrintMode();
        $this->printer->text("_ _ _ _ _ _ _ __ _ _ _ _ _ _ __ _ _ _ _ _ _ _\n");
        $this->printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT);
        $this->printer->setEmphasis(false);
        $this->printer->feed();

        // Set back to default for Date and Invoice Number
        $this->printer->selectPrintMode();
        $this->printer->setJustification();

        // Return the initialized printer
        return $this->printer;
    }
}
