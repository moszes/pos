<?php

namespace App\Http\Controllers;

use App\Models\Product;

class ApiProductController extends Controller
{
    public function products()
    {
        $products = Product::withTrashed()->get();

        foreach ($products as $key => $value) {
            $products[$key]['fee'] = @$value->fee;
        }

        return $products;
    }
}
