<?php

namespace App\Http\Controllers;

use App\Models\PurchaseInvoice;

class ApiPurchaseInvoiceController extends Controller
{
    public function purchase_invoices()
    {
        $purchase_invoices = PurchaseInvoice::withTrashed()->get();

        foreach ($purchase_invoices as $key => $value) {
            $purchase_invoices[$key]['items'] = @$value->items;
        }

        return $purchase_invoices;
    }
}
