<?php

namespace App\Http\Controllers;

use App\Models\UserDetail;
use Avatar;
use Storage;

class UserController extends Controller
{
    /**
     * Find a user by their code.
     */
    public static function findUserByCode(string $userCode)
    {
        return UserDetail::where('code', $userCode)->first();
    }

    /**
     * Get the avatar image of the authenticated user.
     *
     * @return string The URL or base64 representation of the avatar image.
     */
    public static function avatarImage($user): string
    {
        return $user->details?->avatar_file_name == null ? Avatar::create($user->name)->toBase64() : $user->details->avatar_file_name;
    }

    public static function coverImage($user): string
    {
        return $user->details->cover_image_file_name ? Storage::url($user->details->cover_image_file_name) : asset('/assets/img/generic/59.png');
    }
}
