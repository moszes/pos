<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class IfUserHasRole
{
    /**
     * Handle an incoming request.
     *
     * @param  Closure(Request): (Response|RedirectResponse)  $next
     */
    public function handle(Request $request, Closure $next, $role): Response|RedirectResponse
    {
        if (! auth()->user()?->hasRole([$role])) {
            return redirect()->route('dashboard');
        }

        return $next($request);
    }
}
