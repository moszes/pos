<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoice;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class SalesInvoiceTable extends Component
{
    use WithPagination;

    public $date;

    public $test;

    public $titlePage = 'Sales Invoice';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshSalesInvoice' => '$refresh',
        'destroy',
        'deleteByModule',
    ];

    /**
     * Set the current date in the format 'Y-m-d'.
     */
    public function mount(): void
    {
        $this->date = date('Y-m-d');
    }

    /**
     * Emit an event to refresh the sales invoice.
     */
    public function updatedDate(): void
    {
        // Emit event to refresh the sales invoice
        $this->emit('refreshSalesInvoice');
    }

    /**
     * Destroys a sales invoice.
     *
     * Deletes the sales invoice with the specified ID from the database.
     * Performs the following actions:
     *  - Retrieves the sales invoice with the given ID using SalesInvoice::find() method.
     *  - Calls the delete() method on the retrieved sales invoice.
     *  - Sets the $actionForm property to 'save'.
     *  - Clears the sales invoice form.
     *  - Emits a "dataDeleted" event.
     *  - Emits a "refreshSalesInvoice" event.
     *
     * @param  int  $id  The ID of the sales invoice to be destroyed.
     */
    public function deleteByModule($module, int $id): void
    {
        $salesInvoice = getDataByModule($module, $id);
        $salesInvoice->delete();

        // Emit a "dataDeleted" event
        $this->emit('dataDeleted');

        // Emit a "refreshSalesInvoice" event
        $this->emit('refreshSalesInvoice');
    }

    /**
     * Returns the view name for pagination.
     *
     * @return string The view name for pagination.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Render the sales invoice table.
     */
    public function render(): Factory|View|Application
    {
        // Retrieve sales invoices that match the search criteria
        $salesInvoices = SalesInvoice::where('sales_invoice_number', 'like', '%'.$this->search.'%')
            ->whereDate('sales_invoice_date', $this->date)
            ->orderBy('id', 'desc')
            ->paginate(10);

        // Return the view with the sales invoices data
        return view('livewire.sales-invoice-table', [
            'sales_invoices' => $salesInvoices,
        ]);
    }
}
