<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class DoctorAssistanceFee extends Component
{
    public $doctorAssistanceRp = 0;

    public $doctorAssistancePercent = 0;

    public $treatmentFeeType = 'fixed';

    public $treatmentFeeValue = 0;

    protected $listeners = [
        'clearDoctorAssistanceFee',
    ];

    /**
     * Update the treatment fee for the doctor.
     *
     * @param  mixed  $value  The new value of the treatment fee.
     */
    public function updatedDoctorAssistanceRp(mixed $value): void
    {
        // Update the type of treatment fee for the doctor to be fixed.
        $this->treatmentFeeType = 'fixed';

        // Update the value of the treatment fee for the doctor.
        $this->treatmentFeeValue = floatval($value);

        $this->emit('setDoctorAssistanceFee', $this->treatmentFeeType, $this->treatmentFeeValue);
    }

    /**
     * Update the value of the doctor's treatment fee.
     *
     * @param  mixed  $value  The new value of the treatment fee.
     */
    public function updatedDoctorAssistancePercent(mixed $value): void
    {
        // Set the type of treatment fee for the doctor to be a percentage.
        $this->treatmentFeeType = 'percentage';

        // Set the value of the treatment fee for the doctor.
        $this->treatmentFeeValue = floatval($value);

        // Emit an event to update the doctor's assistance fee.
        $this->emit('setDoctorAssistanceFee', $this->treatmentFeeType, $this->treatmentFeeValue);
    }

    /**
     * Clear the doctor's assistance fee.
     */
    public function clearDoctorAssistanceFee(): void
    {
        // Reset the object state.
        $this->reset();

        // Reset the error bag.
        $this->resetErrorBag();
    }

    /**
     * Render the view for the doctor assistance fee.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.doctor-assistance-fee');
    }
}
