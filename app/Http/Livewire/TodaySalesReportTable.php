<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoice;
use App\Models\SalesInvoicePayment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class TodaySalesReportTable extends Component
{
    public $titlePage;

    public $date;

    public $sumCash = 0;

    public $sumCreditDebit = 0;

    public $sumCustom = 0;

    public $sumPrepaid = 0;

    public $sumQrcode = 0;

    public $sumVoucher = 0;

    public $grandTotal = 0;

    public $totalSalesInvoice = 0;

    public $totalPayment = 0;

    public $salesData;

    public $salesLists;

    /**
     * Mounts the function.
     */
    public function mount(): void
    {
        // Set the date to current date formatted as 'Y-m-d'
        $this->date = date('Y-m-d');
    }

    /**
     * Renders the today sales table.
     */
    public function render(): Factory|View|Application
    {
        // Retrieve the sales data for the given date
        $this->salesData = SalesInvoice::whereDate('created_at', $this->date)->get();

        // Calculate the total sales invoice amount
        $this->totalSalesInvoice = $this->salesData->sum('grand_total');

        // Get the unique sales invoice numbers
        $this->salesLists = $this->salesData->unique('sales_invoice_number')->all();

        // Calculate the total payment amount
        $this->totalPayment = SalesInvoicePayment::whereIn(
            'sales_invoice_id',
            $this->salesData
                ->pluck('id')
        )
            ->sum('amount');

        $this->sumCash = SalesInvoicePayment::whereIn(
            'sales_invoice_id',
            $this->salesData
                ->pluck('id')
        )
            ->where('payment_id', 1)
            ->sum('amount');
        $this->sumCreditDebit = SalesInvoicePayment::whereIn(
            'sales_invoice_id',
            $this->salesData
                ->pluck('id')
        )
            ->where('payment_id', 2)
            ->sum('amount');
        $this->sumCustom = SalesInvoicePayment::whereIn(
            'sales_invoice_id',
            $this->salesData
                ->pluck('id')
        )
            ->where('payment_id', 3)
            ->sum('amount');
        $this->sumPrepaid = SalesInvoicePayment::whereIn(
            'sales_invoice_id',
            $this->salesData
                ->pluck('id')
        )
            ->where('payment_id', 4)
            ->sum('amount');
        $this->sumQrcode = SalesInvoicePayment::whereIn(
            'sales_invoice_id',
            $this->salesData
                ->pluck('id')
        )
            ->where('payment_id', 5)
            ->sum('amount');
        $this->sumVoucher = SalesInvoicePayment::whereIn(
            'sales_invoice_id',
            $this->salesData
                ->pluck('id')
        )
            ->where('payment_id', 6)
            ->sum('amount');

        // Render the today sales table view with the sales data
        return view('livewire.today-sales-report-table', [
            'sales' => $this->salesLists,
        ]);
    }
}
