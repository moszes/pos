<?php

namespace App\Http\Livewire;

use App\Models\Package;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class PackageTable extends Component
{
    use WithPagination;

    public $titlePage = 'Package';

    public $moduleName = 'package';

    public $moduleLabel = false;

    public $priceLabel = false;

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshPackage' => '$refresh',
        'packageFee',
        'packageItem',
    ];

    /**
     * Returns the view for pagination.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Redirect to the package fees page for a specific ID.
     *
     * @param  int  $id  The ID of the package fee.
     */
    public function packageItem(int $id): void
    {
        $this->redirect(route('master.packageItems', ['id' => $id]));
    }

    /**
     * Redirect to the package fees page for a specific ID.
     *
     * @param  int  $id  The ID of the package fee.
     */
    public function packageFee(int $id): void
    {
        $this->redirect(route('master.package_fees', ['id' => $id]));
    }

    /**
     * Render the package table view with the filtered packages.
     */
    public function render(): View|Factory|Application
    {
        // Retrieve and return the package table view with the filtered packages
        return view('livewire.package-table', [
            'packages' => Package::where('name', 'like', '%'.$this->search.'%')
                ->orWhere('code', 'like', '%'.$this->search.'%')
                ->orderBy('updated_at', 'desc')
                ->paginate(10),
        ]);
    }
}
