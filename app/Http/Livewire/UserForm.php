<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\UserDetail;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class UserForm extends Component
{
    public $formTitle = 'Form User';

    public $actionForm = 'store';

    public $user;

    public $role;

    public $name;

    public $email;

    public $phone;

    public $password;

    public $password_confirmation;

    protected $listeners = [
        'clearUserForm',
        'editUser',
        'destroy',
    ];

    protected $rules = [
        'name' => 'required',
        'password' => 'confirmed|min:6',
    ];

    /**
     * Set up the initial state for the component.
     */
    public function mount(): void
    {
        $this->formTitle = request()->routeIs('master.employees') ? 'Form Employee' : 'Form User';
    }

    /**
     * Returns an array containing user data.
     */
    public function userData(): array
    {
        return [
            // Get the name of the user
            'name' => $this->name,
            // Get the email of the user
            'email' => $this->email,
            // Hash the password for security
            'password' => bcrypt($this->password),
        ];
    }

    /**
     * Retrieve the user detail data.
     *
     * @return array The user detail data.
     */
    public function userDetailData(): array
    {
        // Generate the user code.
        $code = generateUserCode();
        // Get the user ID.
        $userId = $this->user->id;
        // Get the first company ID.
        $companyId = auth()->user()->details->branch_id;
        // Get the first branch ID.
        $branchId = auth()->user()->details->branch_id;
        // Get the user phone number.
        $phone = $this->phone;

        // Create and return the user detail data array.
        return [
            'code' => $code,
            'user_id' => $userId,
            'company_id' => $companyId,
            'branch_id' => $branchId,
            'phone' => $phone,
        ];
    }

    /**
     * Store a new User.
     */
    public function store(): void
    {
        // Check if the role is selected
        if ($this->role == '-- Choose --' || $this->role == null) {
            $this->addError('role', 'Pick Role...');

            return;
        }

        // Check if the phone number already exists
        if (UserDetail::where('phone', $this->phone)->count() > 0) {
            $this->addError('phone', 'Phone already exists');

            return;
        }

        // Validate the input data
        $this->validate();

        // Start a database transaction
        DB::transaction(function () {
            // Create a new User
            $this->user = User::create($this->userData());

            // Assign the role to the user
            $this->user->assignRole($this->role);

            // Create UserDetail for the new user
            $this->phone = $this->phone == '' || $this->phone == null ? fake()->unique()->phoneNumber() : $this->phone;

            UserDetail::create($this->userDetailData());
        }, 5);

        // Emit events for data saved and user refresh
        $this->emit('clearUserForm');
    }

    /**
     * Edit a user.
     *
     * @param  int  $id  The ID of the user to edit.
     */
    public function editUser(int $id): void
    {
        // Find the user by ID
        $this->user = User::find($id);

        // Set the name, email, and role of the user
        $this->name = $this->user->name;
        $this->email = $this->user->email;
        $this->phone = $this->user->details?->phone;

        $this->role = count($this->user->getRoleNames()) > 0 ? $this->user->getRoleNames()[0] : '-- Choose --';

        // Set the action form to 'update'
        $this->actionForm = 'update';

        // Emit an event to show the user form modal
        $this->emit('showUserForm');
    }

    /**
     * Updates the user's information.
     */
    public function update(): void
    {
        // Check if the role is selected or not
        if ($this->role == '-- Choose --' || $this->role == null) {
            $this->addError('role', 'Pick Role...');

            return;
        }

        // Use a database transaction to ensure data consistency
        DB::transaction(function () {
            // Update the user's information
            $this->user->update($this->userData());

            // Update the user's details
            $this->user->details->update($this->userDetailData());

            // If a new password is provided, update the password
            if ($this->password != null) {
                $this->user->password = bcrypt($this->password);
                $this->user->save();
            }

            // Remove all existing roles for the user
            $this->user->syncRoles([]);

            // Assign the selected role to the user
            $this->user->assignRole($this->role);
        }, 5);

        // Emit an event to notify that the data has been updated
        $this->emit('clearUserForm');
    }

    /**
     * Delete a user and their details.
     *
     * @param  int  $id  The ID of the user to delete.
     */
    public function destroy(int $id): void
    {
        // Find the user with the given ID
        $this->user = User::find($id);

        DB::transaction(function () {
            // Append '_deleted' and current timestamp to the user's email
            $this->user->email = $this->user->email.'_deleted'.time();
            $this->user->save();

            if ($this->user->details) {
                // Append '_deleted' and current timestamp to user's details phone number
                $this->user->details->phone = $this->user->details->phone.'_deleted'.time();
                $this->user->details->save();
            }

            // Delete the user and their details
            $this->user->delete();
            $this->user->details->delete();

        }, 5);

        // Emit events to notify that data has been deleted, and the user needs to be refreshed
        $this->emit('clearUserForm');
    }

    /**
     * Clear the form and hide the user form modal.
     */
    public function clearUserForm(): void
    {
        // Reset the form data
        $this->reset();

        // Reset the error bag
        $this->resetErrorBag();

        // Emit the 'refreshUser' event
        $this->emit('refreshUser');
    }

    /**
     * Render the user form view.
     *
     * @return View|Factory|Application The rendered user form view.
     */
    public function render(): View|Factory|Application
    {
        // Return the rendered user form view
        return view('livewire.user-form');
    }
}
