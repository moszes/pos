<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class CustomerSearch extends Component
{
    public $customer = [];

    public $customers;

    public $customerList = false;

    public $customerSearch;

    protected $listeners = [
        'selectCustomer',
        'customerRequired',
        'customerExists',
        'clearCustomerSearch',
        'setCustomer',
    ];

    /**
     * Updates the customer search value and performs the necessary actions.
     *
     * @param  string  $searchData  The new customer search value.
     */
    public function updatedCustomerSearch(string $searchData): void
    {
        // Reset the error bag
        $this->resetErrorBag();

        // Check if the search data is not empty
        if ($searchData != '') {
            // Set the customer list flag to true
            $this->customerList = true;

            // Update the customer search value
            $this->customer['customerSearch'] = $searchData;

            // Retrieve customer data for customers with the 'customer' role
            $customerData = User::role('customer')->pluck('id')->toArray();

            // Fetch customers based on the search value
            $this->customers = DB::table('users')
                ->join('user_details', 'user_details.user_id', '=', 'users.id')
                ->whereIn('users.id', $customerData)
                ->where(function (Builder $query) {
                    $query->where('users.name', 'like', '%'.$this->customerSearch.'%')
                        ->orWhere('user_details.phone', 'like', '%'.$this->customerSearch.'%')
                        ->orWhere('user_details.first_name', 'like', '%'.$this->customerSearch.'%');
                })
                ->take(10)
                ->get();

            // Check if the route for sales invoices is available
            if (route('transaction.sales_invoices')) {
                // Emit an event to clear sales invoice items
                $this->emit('clearSalesInvoiceItems');
            } elseif (route('transaction.appointments')) {
                // Emit an event to clear appointment items
                $this->emit('clearAppointmentItems');
            }
        } else {
            // Emit an event to indicate that a customer is required
            $this->emit('customerRequired');
            // Reset the customer list flag
            $this->customerList = false;
        }
    }

    /**
     * Set the selected customer.
     *
     * @param  mixed  $customer  The customer to be set.
     */
    public function setCustomer(mixed $customer): void
    {
        // Assign the $customer parameter to the class property $this->customer
        $this->customer = $customer;
        $this->customerSearch = $this->customer['details']['first_name'].', '.$this->customer['details']['phone'];
    }

    /**
     * Adds an error message to the 'customerSearch' field and resets the
     * 'customerSearch' value to null.
     */
    public function customerRequired(): void
    {
        $this->addError('customerSearch', 'Pick Customer');
    }

    /**
     * Selects a customer by ID.
     *
     * @param  int  $customerId  The ID of the customer to be selected.
     */
    public function selectCustomer(int $customerId): void
    {
        // Find the customer by ID
        $this->customer = getCustomerById($customerId);

        // Set customerSearch to the customer's first name and phone number
        $this->customerSearch = $this->customer['details']['first_name'].', '.$this->customer['details']['phone'];

        // Check if the route for appointments is available and emit 'cartOpen' event
        if (route('transaction.appointments')) {
            $this->emit('cartOpen', null, $customerId);
        }

        // Emit an event to indicate that the customer has been selected
        $this->emit('setCustomer', $this->customer);

        // Reset all properties except 'customerSearch'
        $this->resetExcept('customerSearch');
    }

    /**
     * Check if the customer exists
     */
    public function customerExists(): void
    {
        // Add an error message for the 'customerSearch' field
        $this->addError('customerSearch', 'Customer Exists');
    }

    /**
     * Clears the customer search and resets the error bag.
     *
     * This function resets the error bag and all the properties of the class.
     * It also emits an event to reset the customer.
     */
    public function clearCustomerSearch(): void
    {
        // Reset the error bag
        $this->resetErrorBag();

        // Reset all the properties
        $this->reset();

        // Emit an event to reset the customer
        $this->emit('resetCustomer');
    }

    /**
     * Render the customer search view.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.customer-search');
    }
}
