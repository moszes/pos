<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ManageUserTransactionPage extends Component
{
    public function render()
    {
        return view('livewire.manage-user-transaction-page');
    }
}
