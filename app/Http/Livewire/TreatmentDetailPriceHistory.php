<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class TreatmentDetailPriceHistory extends Component
{
    public $treatment;

    /**
     * Render the treatment detail price history view.
     *
     * @return View The rendered view.
     */
    public function render(): View
    {
        // Return the view for the treatment detail price history
        return view('livewire.treatment-detail-price-history');
    }
}
