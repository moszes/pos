<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Livewire\Component;

class CompanyDataPage extends Component
{
    /**
     * Render the view for the Company Data Page.
     */
    public function render(): Factory|Application|View
    {
        return view('livewire.company-data-page')->layout('layouts.guest');
    }
}
