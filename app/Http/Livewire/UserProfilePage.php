<?php

namespace App\Http\Livewire;

use App\Http\Controllers\UserController;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithFileUploads;

class UserProfilePage extends Component
{
    use WithFileUploads;

    public $userCode;

    public $coverImage;

    protected $queryString = ['userCode'];

    /**
     * Renders the employee detail page view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.user-profile-page', [
            'user' => UserController::findUserByCode($this->userCode)->user,
        ]);
    }
}
