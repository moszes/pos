<?php

namespace App\Http\Livewire;

use App\Models\PurchaseInvoice;
use App\Models\PurchaseOrder;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PurchaseOrderSearch extends Component
{
    public $purchaseOrder;

    public $purchaseOrderSearching = false;

    public $purchaseOrderSearch;

    public $purchaseOrderLists;

    public $items;

    protected $listeners = [
        'clearPurchaseOrderSearch',
        'setPurchaseOrderNumber',
        'resetPurchaseOrderSearch',
    ];

    /**
     * Perform a search for updated purchase orders.
     */
    public function updatedPurchaseOrderSearch(): void
    {
        if ($this->purchaseOrderSearch === '') {
            $this->purchaseOrderSearching = false;
            $this->purchaseOrderLists = null;

            return;
        }
        // Set flag to indicate that purchase order searching is in progress
        $this->purchaseOrderSearching = true;

        // Query the purchase orders table for matching records
        $this->purchaseOrderLists = PurchaseOrder::where('order_number', 'LIKE', "%$this->purchaseOrderSearch%")
            ->whereNotIn('id', PurchaseInvoice::whereNotNull('purchase_order_id')->pluck('purchase_order_id')->toArray())
            ->whereNotNull('approved_date')
            ->get();
    }

    /**
     * Set the purchase order number.
     *
     * @param  string  $purchaseOrderNumber  The purchase order number.
     */
    public function setPurchaseOrderNumber(string $purchaseOrderNumber): void
    {
        // Assign the purchase order number to the purchaseOrderSearch property.
        $this->purchaseOrderSearch = $purchaseOrderNumber;
    }

    /**
     * Selects a purchase order by its ID.
     *
     * @param  int  $id  The ID of the purchase order.
     */
    public function selectPurchaseOrderById(int $id): void
    {
        // Find the purchase order by its ID
        $this->purchaseOrder = PurchaseOrder::find($id);

        // Set the purchase order search to the order number
        $this->purchaseOrderSearch = $this->purchaseOrder->order_number;

        // Emit an event to select the vendor by its ID
        $this->emit('selectVendorById', $this->purchaseOrder->vendor_id);

        // Iterate over each purchase invoice item
        foreach ($this->purchaseOrder->items as $purchaseInvoiceItem) {
            $product = getProductById($purchaseInvoiceItem->product_id);

            // Add the item details to the items array
            $this->items[] = [
                'product_id' => $purchaseInvoiceItem->product_id,
                'qty' => numberFormat($purchaseInvoiceItem->qty),
                'price' => numberFormat($purchaseInvoiceItem->price),
                'unit' => numberFormat($product->size).' '.$product->unitOfMeasure->name,
                'itemCode' => $product->code,
                'itemName' => $product->name,
                'last_price' => numberFormat($product->cogs),
                'discount_item' => numberFormat($purchaseInvoiceItem->discount_item),
                'total_price' => numberFormat($purchaseInvoiceItem->price * $purchaseInvoiceItem->qty - $purchaseInvoiceItem->discount_item),
            ];
        }

        // Set the purchase order searching flag to false
        $this->purchaseOrderSearching = false;

        // Emit an event to indicate that the purchase order has been selected
        $this->emit('purchaseOrderSelected', $this->purchaseOrder);

        // Emit an event to set the items form the purchase order
        $this->emit('setItemsFormPurchaseOrder', $this->items);

        // Emit an event to clear the purchase order search
        $this->emit('clearPurchaseOrderSearch');
    }

    /**
     * Clears the purchase order search.
     */
    public function clearPurchaseOrderSearch(): void
    {
        // Reset all values of the purchase order search except the search itself.
        $this->resetExcept('purchaseOrderSearch');
    }

    /**
     * Resets the purchase order search.
     */
    public function resetPurchaseOrderSearch(): void
    {
        // Reset the search
        $this->reset();
    }

    /**
     * Render the purchase order search view.
     */
    public function render(): Factory|View|Application
    {
        // Return the view for the purchase order search
        return view('livewire.purchase-order-search');
    }
}
