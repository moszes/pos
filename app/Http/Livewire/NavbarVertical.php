<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class NavbarVertical extends Component
{
    /**
     * Render the component.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.navbar-vertical');
    }
}
