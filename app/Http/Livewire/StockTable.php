<?php

namespace App\Http\Livewire;

use App\Models\Stock;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;
use Livewire\WithPagination;

class StockTable extends Component
{
    use WithPagination;

    public $titlePage = 'Stock';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshStock' => '$refresh',
    ];

    /**
     * Resets the page number when the search input is updated.
     */
    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    /**
     * Define the View for pagination.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Perform search filtering on a Stock model based on the user's company ID, search keyword, and pagination.
     */
    public function searchFilter(): array|LengthAwarePaginator
    {
        return Stock::where('company_id', auth()->user()->details->company_id)
            ->where(function ($query) {
                $query->where('name', 'like', '%'.$this->search.'%')
                    ->orWhere('code', 'like', '%'.$this->search.'%')
                    ->orWhere('type', 'like', '%'.$this->search.'%');
            })
            ->orderBy('id', 'desc')
            ->paginate(10);
    }

    /**
     * Render the Component view with Stock data.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.stock-table', [
            'stocks' => self::searchFilter(),
        ]);
    }
}
