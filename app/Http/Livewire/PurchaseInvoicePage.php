<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PurchaseInvoicePage extends Component
{
    public function render()
    {
        return view('livewire.purchase-invoice-page');
    }
}
