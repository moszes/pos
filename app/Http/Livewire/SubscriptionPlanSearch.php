<?php

namespace App\Http\Livewire;

use App\Models\SubscriptionPlan;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SubscriptionPlanSearch extends Component
{
    public $search;

    public $subscriptionPlanSearch;

    public $subscriptionPlan;

    public $subscriptionPlans = [];

    public $subscriptionPlanList = false;

    protected $listeners = [
        'selectSubscriptionPlan',
        'clearSubscriptionPlanSearch',
        'subscriptionPlanRequired',
    ];

    /**
     * Handle the event when the subscription plan search input is updated.
     *
     * This function checks if the subscription plan search input is not null or empty,
     * then retrieves and populates the subscription plans list based on the search input.
     */
    public function updatedSubscriptionPlanSearch(): void
    {
        // Check if the subscription plan search input is not null or empty
        if (($this->subscriptionPlanSearch != null) || ($this->subscriptionPlanSearch != '')) {
            // Set the subscription plan list flag to true
            $this->subscriptionPlanList = true;

            // Retrieve and populate the subscription plans list based on the search input
            $this->subscriptionPlans = SubscriptionPlan::where('code', 'like', '%'.$this->subscriptionPlanSearch.'%')
                ->where('name', 'like', '%'.$this->subscriptionPlanSearch.'%')
                ->where('company_id', auth()->user()->details->branch_id)
                ->take(10)
                ->get();
        }
    }

    /**
     * Selects a subscription plan by ID and updates the necessary properties.
     *
     * @param  int  $subscriptionPlanId  The ID of the subscription plan to select.
     */
    public function selectSubscriptionPlan(int $subscriptionPlanId): void
    {
        // Reset the subscription plan list
        $this->subscriptionPlanList = false;

        // Find the subscription plan by ID
        $this->subscriptionPlan = SubscriptionPlan::find($subscriptionPlanId);

        if ($this->subscriptionPlan) {
            // Create a search string using the code and name of the subscription plan
            $this->subscriptionPlanSearch = $this->subscriptionPlan->code.', '.$this->subscriptionPlan->name;
        }

        // Emit an event to set the selected subscription plan
        $this->emit('setSubscriptionPlan', $subscriptionPlanId);
    }

    public function subscriptionPlanRequired(): void
    {
        $this->addError('subscriptionPlanSearch', 'Subscription Plan is required');
    }

    /**
     * Clear the subscription plans.
     */
    public function clearSubscriptionPlanSearch(): void
    {
        // Reset the component properties.
        $this->reset();

        // Clear any error messages related to the subscription plans.
        $this->resetErrorBag();

        $this->emit('resetSubscriptionPlan');
    }

    /**
     * Render the view for the subscription plan search.
     *
     * @return View|Factory|Application The rendered view for the subscription plan search.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.subscription-plan-search');
    }
}
