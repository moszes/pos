<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class ManageCompanyPage extends Component
{
    /**
     * A description of the entire PHP function.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.manage-company-page');
    }
}
