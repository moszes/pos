<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SalesDetailReportPage extends Component
{
    public $showDocument = false;

    public $salesDetails;

    protected $listeners = [
        'showSalesDetailReportTable',
        'hideSalesDetailReportTable',
    ];

    /**
     * Toggles the visibility of the document.
     */
    public function showSalesDetailReportTable($dateFrom, $dateTo, $customer): void
    {
        $this->emit('refreshSalesDetails', $dateFrom, $dateTo, $customer);

        // Invert the value of the showDocument property
        $this->showDocument = true;
    }

    public function hideSalesDetailReportTable(): void
    {
        // Invert the value of the showDocument property
        $this->showDocument = false;
    }

    /**
     * Render the sales detail report page.
     *
     * @return Factory|View|Application The rendered page.
     */
    public function render(): Factory|View|Application
    {
        // Render the sales detail report page using the 'livewire.sales-detail-report-page' view.
        return view('livewire.sales-detail-report-page');
    }
}
