<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class MarketPlaceDiscountFee extends Component
{
    public $marketPlaceDiscountFee = [];

    public $marketPlace;

    /**
     * Mount the component and initialize fee values.
     *
     * If the marketplace exists, update the fee values based on the marketplace's discount fees.
     */
    public function mount(): void
    {
        // Initialize the fee values
        $this->marketPlaceDiscountFee = [
            'senior_doctor' => 0, // Fee for the doctor
            'doctor' => 0, // Fee for the doctor
            'doctor_assistance' => 0, // Fee for the doctor's assistance
            'sales' => 0, // Fee for sales
            'therapist' => 0, // Fee for the therapist
        ];

        // If the marketplace exists, update the fee values
        if ($this->marketplace) {
            foreach ($this->marketplace->discountFees as $value) {
                // Update the fee value based on the role
                $this->marketPlaceDiscountFee[$value->role] = countFormat($value->value);
            }
        }
    }

    /**
     * Update the fee values based on the property name and value.
     *
     * @param  string  $propertyName  The name of the property that was updated.
     * @param  mixed  $value  The new value for the property.
     */
    public function updated(string $propertyName, mixed $value): void
    {
        // Check if the property name is one of the fee properties
        if (in_array($propertyName, [
            'marketPlaceDiscountFee.senior_doctor',
            'marketPlaceDiscountFee.doctor',
            'marketPlaceDiscountFee.doctor_assistance',
            'marketPlaceDiscountFee.sales',
            'marketPlaceDiscountFee.therapist',
        ])) {
            // Remove the 'marketPlaceDiscountFee.' prefix from the property name
            $marketPlaceDiscountFeeName = str_replace('marketPlaceDiscountFee.', '', $propertyName);

            // Convert the value to a float and update the marketPlaceDiscountFee property
            $this->marketPlaceDiscountFee[$marketPlaceDiscountFeeName] = floatval($value);

            $this->emit('feeUpdated', 'marketplace', 'discount', $this->marketPlaceDiscountFee);
        }
    }

    /**
     * Render the view for the MarketPlaceDiscountFee Livewire component.
     *
     * This method returns the view for the MarketPlaceDiscountFee Livewire component, which is 'livewire.marketplace-discount-fee'.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the livewire.marketplace-discount-fee view
        return view('livewire.market-place-discount-fee');
    }
}
