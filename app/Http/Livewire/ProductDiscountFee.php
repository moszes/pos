<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class ProductDiscountFee extends Component
{
    public $productDiscountFee = [];

    public $product;

    /**
     * Mount the component and initialize fee values.
     *
     * If the product exists, update the fee values based on the product's discount fees.
     */
    public function mount(): void
    {
        // Initialize the fee values
        $this->productDiscountFee = [
            'senior_doctor' => 0, // Fee for the doctor
            'doctor' => 0, // Fee for the doctor
            'doctor_assistance' => 0, // Fee for the doctor's assistance
            'sales' => 0, // Fee for sales
            'therapist' => 0, // Fee for the therapist
        ];

        // If the product exists, update the fee values
        if ($this->product) {
            foreach ($this->product->discountFees as $value) {
                // Update the fee value based on the role
                $this->productDiscountFee[$value->role] = countFormat($value->value);
            }
        }
    }

    /**
     * Update the fee values based on the property name and value.
     *
     * @param  string  $propertyName  The name of the property that was updated.
     * @param  mixed  $value  The new value for the property.
     */
    public function updated(string $propertyName, mixed $value): void
    {
        // Check if the property name is one of the fee properties
        if (in_array($propertyName, [
            'productDiscountFee.senior_doctor',
            'productDiscountFee.doctor',
            'productDiscountFee.doctor_assistance',
            'productDiscountFee.sales',
            'productDiscountFee.therapist',
        ])) {
            // Remove the 'productDiscountFee.' prefix from the property name
            $productDiscountFeeName = str_replace('productDiscountFee.', '', $propertyName);

            // Convert the value to a float and update the productDiscountFee property
            $this->productDiscountFee[$productDiscountFeeName] = floatval($value);

            $this->emit('feeUpdated', 'product', 'discount', $this->productDiscountFee);
        }
    }

    /**
     * Render the view for the ProductDiscountFee Livewire component.
     *
     * This method returns the view for the ProductDiscountFee Livewire component, which is 'livewire.product-discount-fee'.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the livewire.product-discount-fee view
        return view('livewire.product-discount-fee');
    }
}
