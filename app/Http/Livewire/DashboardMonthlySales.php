<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoice;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class DashboardMonthlySales extends Component
{
    public $selectedMonth;

    protected $queryString = [
        'selectedMonth',
    ];

    /**
     * Render a view to display the dashboard monthly sales.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.dashboard-monthly-sales', [
            'monthlySales' => SalesInvoice::where('sales_invoice_status', 'valid')
                ->whereYear('sales_invoice_date', date('Y', strtotime($this->selectedMonth)))
                ->whereMonth('sales_invoice_date', date('m', strtotime($this->selectedMonth)))
                ->sum('grand_total'),
        ]);
    }
}
