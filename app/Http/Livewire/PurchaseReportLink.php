<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PurchaseReportLink extends Component
{
    public function render()
    {
        return view('livewire.purchase-report-link');
    }
}
