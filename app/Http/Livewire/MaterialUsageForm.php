<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class MaterialUsageForm extends Component
{
    public $itemForm = false;

    public $productSearch;

    public $productList = false;

    public $products;

    public $product;

    public $items = [];

    public $itemCode;

    public $itemName;

    public $itemPrice = 0;

    public $itemDose = 0;

    public $itemCost = 0;

    public $totalCost = 0;

    /**
     * Updates the product search and retrieves matching products.
     */
    public function updatedProductSearch(): void
    {
        // Check if the product search is not empty
        if ($this->productSearch !== '') {
            // Set the product list flag to true
            $this->productList = true;

            // Retrieve products matching the search criteria
            $this->products = Product::where('code', 'LIKE', "%$this->productSearch%")
                ->orWhere('name', 'LIKE', "%$this->productSearch%")
                ->get();
        }
    }

    public function selectProduct($id): void
    {
        $this->product = Product::find($id);

        $this->itemForm = true;
        $this->itemCode = $this->product->code;
        $this->itemName = $this->product->name;

        $this->productList = false;

        $this->productSearch = null;
    }

    public function addItem(): void
    {
        if ($this->product) {
            $this->product->itemPrice = $this->itemPrice ?? 0;
            $this->product->itemDose = $this->itemDose ?? 0;
            $this->product->itemCost = $this->itemCost ?? 0;

            $this->items->filter(function ($value, $key) {
                if ($value['code'] == $this->itemCode) {
                    $this->items->forget($key);
                }
            });

            $this->items->push($this->product->toArray());
        }

    }

    public function removeItem($arrayKey): void
    {
        $this->items->forget($arrayKey);
    }

    /**
     * Render the view for the Material Usage Form.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.material-usage-form');
    }
}
