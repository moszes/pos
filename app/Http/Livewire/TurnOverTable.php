<?php

namespace App\Http\Livewire;

use App\Models\TurnOver;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;
use Livewire\WithPagination;

class TurnOverTable extends Component
{
    use WithPagination;

    public $titlePage = 'TurnOver';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshTurnOver' => '$refresh',
    ];

    /**
     * Resets the page number when the search input is updated.
     */
    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    /**
     * Define the View for pagination.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Perform search filtering on a TurnOver model based on the user's company ID, search keyword, and pagination.
     */
    public function searchFilter(): array|LengthAwarePaginator
    {
        return TurnOver::where('company_id', auth()->user()->details->company_id)
            ->where(function ($query) {
                $query->where('month', 'like', '%'.$this->search.'%');
            })
            ->orderBy('id', 'desc')
            ->paginate(10);
    }

    /**
     * Render the Component view with TurnOver data.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.turn-over-table', [
            'turnOvers' => self::searchFilter(),
        ]);
    }
}