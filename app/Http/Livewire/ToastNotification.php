<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class ToastNotification extends Component
{
    public $toastDisplay = 'fade';

    public $toastIcon = 'far fa-check-circle text-success';

    public $toastMessage;

    protected $listeners = [
        'dataSaved',
        'dataUpdated',
        'dataDeleted',
        'dataApproved',
        'wrongPassword',
        'newPasswordNotMatched',
        'newBranchSelected',
        'noItemSelected',
        'shouldInputQuantity',
        'hideToast',
    ];

    /**
     * Sets the toast icon, display, and message to indicate that the data has been saved.
     */
    public function dataSaved(): void
    {
        $this->toastIcon = 'fas fa-check-circle text-success';
        $this->toastDisplay = 'show';
        $this->toastMessage = 'Saved.';
    }

    /**
     * Sets the toast icon, display, and message when data is approved.
     */
    public function dataApproved(): void
    {
        $this->toastIcon = 'fas fa-check-circle text-success';
        $this->toastDisplay = 'show';
        $this->toastMessage = 'Approved.';
    }

    /**
     * Updates the data and shows a success toast message.
     */
    public function dataUpdated(): void
    {
        $this->toastIcon = 'fas fa-check-circle text-success';
        $this->toastDisplay = 'show';
        $this->toastMessage = 'Updated.';
    }

    /**
     * Set the toast properties to indicate a successful deletion.
     */
    public function dataDeleted(): void
    {
        $this->toastIcon = 'fas fa-check-circle text-success';
        $this->toastDisplay = 'show';
        $this->toastMessage = 'Deleted.';
    }

    /**
     * Display a toast notification for the wrong password.
     */
    public function wrongPassword(): void
    {
        $this->toastIcon = 'fas fa-exclamation text-danger';
        $this->toastDisplay = 'show';
        $this->toastMessage = 'Wrong Password';
    }

    /**
     * Sets the toast icon, display, and message when a new password is not matched.
     */
    public function newPasswordNotMatched(): void
    {
        $this->toastIcon = 'fas fa-exclamation text-danger';
        $this->toastDisplay = 'show';
        $this->toastMessage = 'Password Not Match!.';
    }

    /**
     * Sets the toast properties to display a success message when a new branch is selected.
     */
    public function newBranchSelected(): void
    {
        $this->toastIcon = 'fas fa-check-circle text-success';
        $this->toastDisplay = 'show';
        $this->toastMessage = 'New Branch Selected.';
    }

    /**
     * Handles the scenario when no item is selected.
     */
    public function noItemSelected(): void
    {
        $this->toastIcon = 'fas fa-exclamation text-warning';
        $this->toastDisplay = 'show';
        $this->toastMessage = 'New Item Selected.';
    }

    /**
     * Display a toast message indicating that the user should input a quantity.
     */
    public function shouldInputQuantity(): void
    {
        $this->toastIcon = 'fas fa-exclamation text-warning';
        $this->toastDisplay = 'show';
        $this->toastMessage = 'Please input quantity.';
    }

    /**
     * Resets the toast icon, display, and message to default values, hiding the toast.
     */
    public function hideToast(): void
    {
        $this->reset();
    }

    public function render(): Factory|View|Application
    {
        return view('livewire.toast-notification');
    }
}
