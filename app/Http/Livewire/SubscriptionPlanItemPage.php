<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SubscriptionPlanItemPage extends Component
{
    /**
     * Render the view for the subscription plan item page.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.subscription-plan-item-page');
    }
}
