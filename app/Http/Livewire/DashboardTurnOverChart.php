<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoice;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class DashboardTurnOverChart extends Component
{
    public $selectedMonth;

    public $dateStart;

    public $dateEnd;

    public $allDaysThisMonth;

    public $salesDataByMonth = [];

    protected $queryString = [
        'dateStart',
        'dateEnd',
    ];

    /**
     * Set the initial state of the component.
     */
    public function mount(): void
    {
        $this->selectedMonth = date('Y-m');
        $this->dateStart = date('m/d/Y', strtotime('first day of '.$this->selectedMonth));
        $this->dateEnd = date('m/d/Y', strtotime('last day of '.$this->selectedMonth));
        $this->getCurrentMonthSales($this->dateStart, $this->dateEnd);
    }

    /**
     * Get the sales data for the current month.
     */
    public function getCurrentMonthSales($dateStart, $dateEnd): void
    {
        $startDate = new DateTime($dateStart); // Set your custom start date here
        $endDate = new DateTime($dateEnd); // Set your custom end date here

        $period = new DatePeriod(
            $startDate,
            new DateInterval('P1D'),
            $endDate->modify('+1 day') // Adding one day to include the end date in the period
        );

        $this->allDaysThisMonth = [];
        foreach ($period as $day) {
            $this->allDaysThisMonth[] = $day->format('Y-m-d');
        }

        $this->salesDataByMonth = [];
        foreach ($this->allDaysThisMonth as $days) {
            $this->salesDataByMonth[] = SalesInvoice::whereDate('sales_invoice_date', $days)
                ->where('sales_invoice_status', 'valid')->sum('grand_total');
        }

    }

    /**
     * Update the selected month.
     */
    public function updatedSelectedMonth(): void
    {
        $this->emit('setSelectedMonth', $this->selectedMonth);

        $this->dateStart = date('m/d/Y', strtotime('first day of '.$this->selectedMonth));
        $this->dateEnd = date('m/d/Y', strtotime('last day of '.$this->selectedMonth));

        $this->emit('loadChart');

        $this->getCurrentMonthSales($this->dateStart, $this->dateEnd);
    }

    /**
     * Render the component.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.dashboard-turn-over-chart', [
            'dateStart' => date('m/d/Y', strtotime('first day of '.$this->selectedMonth)),
            'dateEnd' => date('m/d/Y', strtotime('last day of '.$this->selectedMonth)),
            'allDaysThisMonth' => $this->allDaysThisMonth,
        ]);
    }
}
