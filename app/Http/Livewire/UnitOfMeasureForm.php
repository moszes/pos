<?php

namespace App\Http\Livewire;

use App\Models\Product;
use App\Models\UnitOfMeasure;
use DB;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Livewire\Event;

class UnitOfMeasureForm extends Component
{
    public $formTitle = 'Form Unit';

    public $actionForm = 'store';

    public $unitOfMeasure;

    public $code;

    public $name;

    protected $listeners = [
        'clearUnitOfMeasureForm',
        'editUnitOfMeasure',
        'destroy',
    ];

    protected $rules = [
        'code' => 'required|unique:unit_of_measures',
        'name' => 'required',
    ];

    /**
     * Returns an array of unit of measure data.
     */
    public function unitOfMeasureData(): array
    {
        return [
            'company_id' => auth()->user()->details->company_id,
            'branch_id' => auth()->user()->details->branch_id,
            'code' => $this->code,
            'name' => $this->name,
        ];
    }

    /**
     * Store a new unit of measure.
     */
    public function store(): void
    {
        // Validate the request data
        $this->validate();

        // Start a database transaction
        DB::transaction(function () {
            // Create a new unit of measure using the request data
            UnitOfMeasure::create($this->unitOfMeasureData());
        }, 5);

        // Emit an event to clear the unit of measure form
        $this->emit('clearUnitOfMeasureForm');
    }

    /**
     * Edit a unit of measure by its ID.
     */
    public function editUnitOfMeasure(int $id): void
    {
        // Find the unit of measure by its ID
        $this->unitOfMeasure = UnitOfMeasure::find($id);

        // Set the code and name properties based on the unit of measure
        $this->code = $this->unitOfMeasure->code;
        $this->name = $this->unitOfMeasure->name;

        // Set the action form to 'update' and emit the event to show the unit of measure form
        $this->actionForm = 'update';
        $this->emit('showUnitOfMeasureForm');
    }

    /**
     * Update an existing unit of measure.
     */
    public function update(): void
    {
        DB::transaction(function () {
            $this->unitOfMeasure->update($this->unitOfMeasureData());
        }, 5);

        $this->emit('clearUnitOfMeasureForm');
    }

    /**
     * Check if any product is using a unit of measure.
     */
    public function isUsage($id): string|Event
    {
        return ! Product::where('unit_of_measure_id', $id)->first();
    }

    /**
     * Delete a unit of measure and clear the form.
     */
    public function destroy(int $id): void
    {
        if (! $this->isUsage($id)) {
            $this->emit('unitUsedAlready');

            return;
        }

        // Find the unit of measure by ID
        $this->unitOfMeasure = UnitOfMeasure::find($id);

        // Use a database transaction to update the unit of measure, mark it as deleted, and then actually delete it
        DB::transaction(function () {
            // Append '_deleted_' with the current timestamp to the unit of measure's code
            $this->unitOfMeasure->code = $this->unitOfMeasure->code.'-deleted';
            // Save the updated code
            $this->unitOfMeasure->save();
            // Delete the unit of measure
            $this->unitOfMeasure->delete();
        }, 5);

        // Emit an event to clear the unit of measure form
        $this->emit('clearUnitOfMeasureForm');
    }

    /**
     * Clear the unit of measure form.
     */
    public function clearUnitOfMeasureForm(): void
    {
        // Reset the component state
        $this->reset();
        $this->resetErrorBag();

        // Emit events to hide the form and refresh the unit of measure
        $this->emit('hideUnitOfMeasureForm');
        $this->emit('refreshUnitOfMeasure');
    }

    /**
     * Render the unit of measure form.
     */
    public function render(): View
    {
        return view('livewire.unit-of-measure-form');
    }
}
