<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoice;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SalesInvoiceToolPage extends Component
{
    public $salesInvoiceNumber;

    public $salesInvoiceWidget = false;

    public $searchSalesInvoiceNumber;

    public $salesInvoice;

    protected $listeners = [
        'setNullSalesInvoice',
        'openSalesInvoiceWidget',
        'closeSalesInvoiceWidget',
        'setSalesInvoice',
        'clearSalesInvoice',
        'clearSalesInvoiceNumber',
    ];

    protected $queryString = [
        'searchSalesInvoiceNumber',
    ];

    /**
     * Mounts the component and sets the initial state based on the request parameters.
     *
     * This function is called when the Livewire component is first mounted. It checks if the user is an admin,
     * and if not, redirects them to the home page. It then checks if the 'searchSalesInvoiceNumber' parameter
     * is present in the request. If it is, it sets the 'searchSalesInvoiceNumber' property to the value from the request
     * and retrieves the sales invoice with the given sales invoice number. If 'searchSalesInvoiceNumber' is not empty,
     * it sets the 'salesInvoiceWidget' property to true, which will display the sales invoice widget on the page.
     */
    public function mount(): void
    {
        $this->checkIfUserIsAdmin();
        $this->searchSalesInvoice();
        $this->setupSalesInvoiceWidget();
    }

    /**
     * Checks if the authenticated user is an admin.
     *
     * This function checks the user's role and redirects them to the homepage
     * if they are not an admin.
     */
    private function checkIfUserIsAdmin(): void
    {
        if (auth()->user()?->roles[0]->name !== 'godadmin') {
            redirect('/');
        }
    }

    /**
     * Searches for a sales invoice based on the provided searchSalesInvoiceNumber.
     *
     * This function checks if the request has a 'searchSalesInvoiceNumber' parameter,
     * and if so, it assigns its value to the $searchSalesInvoiceNumber property.
     * It then retrieves the sales invoice using the 'getSalesInvoiceByNumber' method
     * with the provided searchSalesInvoiceNumber as an argument and assigns it to the
     * $salesInvoice property.
     *
     * Return void
     */
    private function searchSalesInvoice(): void
    {
        if (request()->has('searchSalesInvoiceNumber')) {
            $this->searchSalesInvoiceNumber = request('searchSalesInvoiceNumber');
            $this->salesInvoice = $this->getSalesInvoiceByNumber($this->searchSalesInvoiceNumber);
        }
    }

    /**
     * Retrieves the sales invoice by its number.
     *
     * This function queries the database for the sales invoice with the given number.
     * It uses the `SalesInvoice` model and filters the records by the `sales_invoice_number` column,
     * returning the first result.
     *
     * @param  string  $number  The number of the sales invoice to retrieve.
     * @return SalesInvoice The sales invoice matching the specified number.
     */
    private function getSalesInvoiceByNumber(string $number): SalesInvoice
    {
        return SalesInvoice::where('sales_invoice_number', $number)->first();
    }

    /**
     * Sets up the sales invoice widget.
     *
     * This function is responsible for setting up the sales invoice widget based on the value of `searchSalesInvoiceNumber`.
     * If `searchSalesInvoiceNumber` is not empty, then it sets the `salesInvoiceWidget` property to `true`,
     * which will show the sales invoice widget on the page.
     */
    private function setupSalesInvoiceWidget(): void
    {
        if ($this->searchSalesInvoiceNumber) {
            $this->salesInvoiceWidget = true;
        }
    }

    /**
     * Opens the sales invoice widget.
     *
     * This function sets the `salesInvoiceWidget` property to `true`,
     * which will display the sales invoice widget on the page.
     */
    public function openSalesInvoiceWidget(): void
    {
        // Set the salesInvoiceWidget property to true
        $this->salesInvoiceWidget = true;
    }

    /**
     * Closes the sales invoice widget.
     *
     * This function sets the `salesInvoiceWidget` property to `false`,
     * which will hide the sales invoice widget on the page.
     */
    public function closeSalesInvoiceWidget(): void
    {
        $this->salesInvoiceWidget = false;
    }

    /**
     * Set the sales invoice number.
     *
     * @param  string  $salesInvoiceNumber  The sales invoice number to set.
     */
    public function setSalesInvoice(string $salesInvoiceNumber): void
    {
        // Retrieve the sales invoice with the given sales invoice number
        $this->salesInvoice = SalesInvoice::where('sales_invoice_number', $salesInvoiceNumber)->first();
        $this->searchSalesInvoiceNumber = $salesInvoiceNumber;
    }

    /**
     * Clears the search sales invoice number field.
     *
     * This function resets the 'searchSalesInvoiceNumber' property to empty.
     */
    public function clearSalesInvoiceNumber(): void
    {
        $this->reset('searchSalesInvoiceNumber');
    }

    /**
     * Render the sales invoice tool page.
     */
    public function render(): Factory|View|Application
    {
        // Render the view with the sales invoice data
        return view('livewire.sales-invoice-tool-page');
    }
}
