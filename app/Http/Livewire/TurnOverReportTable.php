<?php

namespace App\Http\Livewire;

use App\Models\Company;
use App\Models\TurnOver;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class TurnOverReportTable extends Component
{
    use WithPagination;

    public $titlePage = 'TurnOver';

    public $month;

    public $dateStart;

    public $dateEnd;

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshTurnOver' => '$refresh',
    ];

    /**
     * Sets the default month when the component mounts.
     *
     * This function sets the value of the 'month' property to the current year and month in the format 'Y-m'.
     * It also sets the values of the 'dateStart' and 'dateEnd' properties to the first and last day of the month,
     * respectively, with the time set to 09:00:00 and 23:59:59 respectively.
     */
    public function mount(): void
    {
        // Set the value of 'month' to the current year and month
        $this->month = date('Y-m');

        // Set the value of 'dateStart' to the first day of the month at 09:00:00
        $this->dateStart = $this->month.'-01 09:00:00';

        // Set the value of 'dateEnd' to the last day of the month at 23:59:59
        $this->dateEnd = $this->month.'-01 23:59:59';
    }

    /**
     * Returns the view for pagination.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Render the view for the turnover report table.
     *
     * This method fetches the number of days in the current month, retrieves the first company,
     * queries the TurnOver model to get the turnovers based on the search criteria, and returns
     * the view with the turnover's data.
     *
     * @return View|Factory|Application The rendered view.
     */
    public function render(): View|Factory|Application
    {
        // Get the number of days in the current month
        $daysInMonth = cal_days_in_month(CAL_GREGORIAN, date('m', strtotime($this->month)), date('Y', strtotime($this->month)));

        // Retrieve the first company
        $company = Company::first();

        // Query the TurnOver model to get the turnovers based on the search criteria
        $turnovers = TurnOver::where('created_at', 'like', '%'.$this->month.'%')
            ->where('company_id', $company->id)
            ->orderBy('id', 'desc')
            ->paginate(10);

        // Return the view with the turnovers data
        return view('livewire.turn-over-report-table', [
            'turnovers' => $turnovers, // The turnovers data
            'daysInMonth' => $daysInMonth, // The number of days in the current month
            'month' => $this->month, // The current month
        ]);
    }
}
