<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class ProductTable extends Component
{
    use WithPagination;

    public $titlePage = 'Product';

    public $moduleName = 'product';

    public $moduleLabel = false;

    public $priceLabel = false;

    public $redirectRoute;

    public $search;

    protected $queryString = [
        'search',
    ];

    protected $listeners = [
        'refreshProduct' => '$refresh',
        'productFee',
        'productMaterialUsage',
    ];

    /**
     * Reset the page when the search input is updated.
     */
    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    /**
     * Redirects to the product fees page for a specific ID.
     *
     * @param  int  $id  The ID of the product fee.
     */
    public function productFee(int $id): void
    {
        // Generate the route for the product fees page with the given ID
        $route = route('master.product_fees', ['id' => $id]);

        // Redirect to the generated route
        $this->redirect($route);
    }

    /**
     * Redirects to the route for viewing product material usages.
     *
     * @param  int  $id  The ID of the product material usage.
     */
    public function productMaterialUsage(int $id): void
    {
        $this->redirect(route('master.productMaterialUsages', ['id' => $id]));
    }

    /**
     * Returns the view for pagination.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Perform a search and filter query on the products table.
     */
    public function searchFilter(): LengthAwarePaginator|array
    {
        return Product::where('company_id', auth()->user()->details->company_id)
            ->where(function ($query) {
                $query->where('name', 'like', '%'.$this->search.'%')
                    ->orWhere('code', 'like', '%'.$this->search.'%');
            })
            ->orderBy('id', 'desc')
            ->paginate(10);
    }

    /**
     * Render the product table view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.product-table', [
            'products' => self::searchFilter(),
        ]);
    }
}
