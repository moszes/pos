<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class TreatmentDetailMaterialUsage extends Component
{
    public $treatment;

    /**
     * Render the treatment detail material usage view.
     */
    public function render(): View
    {
        $treatmentMaterialUsages = $this->treatment
            ? $this->treatment->material_usages->toArray()
            : [];

        // Return the view for the treatment detail material usage page
        return view('livewire.treatment-detail-material-usage', [
            'treatmentMaterialUsages' => $treatmentMaterialUsages,
        ]);
    }
}
