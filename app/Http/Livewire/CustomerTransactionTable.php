<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\UserTransaction;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class CustomerTransactionTable extends Component
{
    public $user;

    public $userTransactions;

    public $customerId;

    protected $queryString = ['customerId'];

    /**
     * Mount the component.
     */
    public function mount(): void
    {
        $this->userTransactions = collect();

        $this->user = User::find($this->customerId);

        if ($this->user) {
            // Retrieve the user transactions for the current user
            $this->userTransactions = UserTransaction::where('user_id', $this->customerId)
                ->where('module', 'treatment')
                ->where('qty', '!=', 0)
                ->orderBy('created_at', 'desc')
                ->limit(10)
                ->get();
        }
    }

    /**
     * Render the user transaction table.
     */
    public function render(): Factory|View|Application
    {
        // Render the view with the user transactions
        return view('livewire.customer-transaction-table');
    }
}
