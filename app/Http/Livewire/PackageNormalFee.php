<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PackageNormalFee extends Component
{
    public $packageNormalFee = [];

    public $package;

    /**
     * Mount the component and initialize fee values.
     *
     * If the package exists, update the fee values based on the package's normal fees.
     */
    public function mount(): void
    {
        // Initialize the fee values
        $this->packageNormalFee = [
            'senior_doctor' => 0, // Fee for the doctor
            'doctor' => 0, // Fee for the doctor
            'doctor_assistance' => 0, // Fee for the doctor's assistance
            'sales' => 0, // Fee for sales
            'therapist' => 0, // Fee for the therapist
        ];

        // If the package exists, update the fee values
        if ($this->package) {
            foreach ($this->package->normalFees as $value) {
                // Update the fee value based on the role
                $this->packageNormalFee[$value->role] = countFormat($value->value);
            }
        }
    }

    /**
     * Update the fee values based on the property name and value.
     *
     * @param  string  $propertyName  The name of the property that was updated.
     * @param  mixed  $value  The new value for the property.
     */
    public function updated(string $propertyName, mixed $value): void
    {
        // Check if the property name is one of the fee properties
        if (in_array($propertyName, [
            'packageNormalFee.senior_doctor',
            'packageNormalFee.doctor',
            'packageNormalFee.doctor_assistance',
            'packageNormalFee.sales',
            'packageNormalFee.therapist',
        ])) {
            // Remove the 'packageNormalFee.' prefix from the property name
            $packageNormalFeeName = str_replace('packageNormalFee.', '', $propertyName);

            // Convert the value to a float and update the packageNormalFee property
            $this->packageNormalFee[$packageNormalFeeName] = floatval($value);

            $this->emit('feeUpdated', 'package', 'normal', $this->packageNormalFee);
        }
    }

    /**
     * Render the view for the PackageNormalFee Livewire component.
     *
     * This method returns the view for the PackageNormalFee Livewire component, which is 'livewire.package-normal-fee'.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the livewire.package-normal-fee view
        return view('livewire.package-normal-fee');
    }
}
