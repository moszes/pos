<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Spatie\Activitylog\Models\Activity;

class MarketPlaceHistoryTable extends Component
{
    public $marketplace;

    /**
     * Render the view for the treatment history table.
     */
    public function render(): Factory|View|Application
    {
        // Return the view for the treatment history table
        return view('livewire.market-place-history-table', [
            'priceHistories' => Activity::where('log_name', 'Market Place Log')
                ->where('subject_type', 'App\Models\MarketPlace')
                ->where('subject_id', $this->marketplace->id)->paginate(10),
        ]);
    }
}
