<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class ProductDetailInfo extends Component
{
    public $product;

    public $moduleLabel;

    public $moduleName;

    public $redirectRoute;

    public $priceLabel = true;

    /**
     * Render the product detail information view.
     */
    public function render(): View
    {
        // Return the view for the product detail information
        return view('livewire.product-detail-info');
    }
}
