<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class EmployeePage extends Component
{
    /**
     * Render the employee page.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.employee-page');
    }
}
