<?php

namespace App\Http\Livewire;

use Illuminate\View\View;
use Livewire\Component;
use Storage;

class NavbarTopLogo extends Component
{
    public $logoPath;

    public $user;

    protected $listeners = [
        'refreshLogoImage',
    ];

    public function mount(): void
    {
        $this->user = auth()->user();

        $this->logoPath = $this->user->details?->company->logo_path ?
            (config('app.env') === 'local') ?
                asset('assets/img/logo.png') :
                Storage::url($this->user->details->company->logo_path)
            : asset('/assets/img/logo.png');
    }

    /**
     * Refresh the logo image path.
     *
     * @param  string  $value  The new logo path value.
     */
    public function refreshLogoImage(string $value): void
    {
        $this->logoPath = Storage::url($value);
    }

    public function render(): View
    {
        return view('livewire.navbar-top-logo');
    }
}
