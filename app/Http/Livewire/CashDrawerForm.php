<?php

namespace App\Http\Livewire;

use App\Models\CashDrawer;
use App\Models\SalesInvoice;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Livewire\Component;

class CashDrawerForm extends Component
{
    public $formTitle = 'Close Cash Drawer';

    public $userId;

    public $code;

    public $salesInvoice;

    public $cash = 0;

    public $creditDebit = 0;

    public $custom = 0;

    public $prepaid = 0;

    public $qrcode = 0;

    public $voucher = 0;

    public $grandTotal = 0;

    public $date;

    public $closedCashDrawer = false;

    public $totalInvoices;

    protected $listeners = [
        'openTodaySale',
    ];

    /**
     * Mounts the component.
     *
     * Sets the current date and retrieves the closed cash drawer for the day.
     */
    public function mount(): void
    {
        // Set the current date
        $this->date = date('Y-m-d');

        $this->salesInvoice = SalesInvoice::whereDate('sales_invoice_date', $this->date)->count();

        // Retrieve the closed cash drawer for the day
        $this->closedCashDrawer = CashDrawer::whereDate('date', today())->first();

        $this->totalInvoices = SalesInvoice::whereDate('sales_invoice_date', $this->date)->sum('grand_total');
    }

    /**
     * Updates the grand total by summing up the cash, creditDebit, custom, prepaid, qrcode, and voucher amounts.
     */
    public function updated($propertyName, $value): void
    {
        $paymentMethods = [
            'cash',
            'custom',
            'creditDebit',
            'prepaid',
            'qrcode',
            'voucher',
        ];

        if (in_array($propertyName, $paymentMethods)) {
            $this->{$propertyName} = floatval($value);
        }

        $this->grandTotal = 0;

        foreach ($paymentMethods as $paymentMethod) {
            $this->grandTotal += $this->{$paymentMethod};
        }
    }

    /**
     * Deletes all cash drawer records for a given date and redirects to the today sales report page.
     *
     * @param  string  $date  The date for which to delete the cash drawer records.
     */
    public function openTodaySale(string $date): void
    {
        // Delete all cash drawer records for the given date
        CashDrawer::whereDate('date', $date)->delete();

        // Redirect to the today sales report page
        $this->redirect(route('report.today_sales_report'));
    }

    /**
     * Store the cash drawer information
     *
     * @return Application|RedirectResponse|Redirector|void
     */
    public function store()
    {
        // check if sales invoice selected by date
        if ($this->salesInvoice < 1) {
            $this->emit('invoiceNotFound');

            return;
        }

        if (floatval($this->totalInvoices) > floatval($this->grandTotal)) {
            $this->emit('needMorePayment');

            return;
        }

        // Check if a cash drawer for the given date already exists
        if (CashDrawer::where('date', $this->date)->first()) {
            $this->emit('siClosedAlready');
        } else {
            // Create a new cash drawer entry
            CashDrawer::create([
                'company_id' => auth()->user()->details->branch_id,
                'branch_id' => auth()->user()->details->branch_id,
                'user_id' => auth()->user()?->id,
                'code' => 'CD'.str_pad(CashDrawer::withTrashed()->count() + 1, 10, '0', STR_PAD_LEFT),
                'sales_invoice' => SalesInvoice::where('sales_invoice_date', $this->date)->where('sales_invoice_status', 'valid')->where('sales_invoice_status', 'valid')->pluck('id')->toJson(),
                'cash' => $this->cash,
                'credit_debit' => $this->creditDebit,
                'custom' => $this->custom,
                'prepaid' => $this->prepaid,
                'qrcode' => $this->qrcode,
                'voucher' => $this->voucher,
                'grand_total' => $this->grandTotal,
                'date' => $this->date,
            ]);
        }

        // Emit an event to hide sales invoice items
        $this->emit('hideSalesInvoiceItems');

        // Reset the date to the current date
        $this->date = date('Y-m-d');

        // Redirect to the print close cash drawer page
        return redirect('http://clinic.local/printCloseCashDrawer?date='.date('Y-m-d'));
    }

    /**
     * Render the cash drawer form view.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.cash-drawer-form');
    }
}
