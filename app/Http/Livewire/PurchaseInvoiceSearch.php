<?php

namespace App\Http\Livewire;

use App\Models\PurchaseInvoice;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PurchaseInvoiceSearch extends Component
{
    public $purchaseInvoiceNumber;

    public $purchaseInvoice;

    public $purchaseInvoiceSearching;

    public $purchaseInvoiceSearch;

    public $purchaseInvoiceLists;

    public $purchaseInvoiceItems;

    public $purchaseInvoiceUsed;

    public $items;

    /**
     * Search and select a purchase invoice by its ID.
     *
     * @param  int  $id  The ID of the purchase invoice to search for.
     */
    public function purchaseInvoiceSearchSelected(int $id): void
    {
        // Find the purchase invoice by its ID
        $this->purchaseInvoice = PurchaseInvoice::find($id);

        // Iterate through each item in the purchase invoice
        foreach ($this->purchaseInvoice->items as $item) {
            // Get the product details for the item
            $product = getProductById($item->product_id);

            $this->emit('selectVendorById', $this->purchaseInvoice->vendor_id);

            // Set the necessary item properties
            $item['purchase_invoice_id'] = @$this->purchaseInvoice->id;
            $item['product_id'] = @$item->product_id;
            $item['code'] = @$product->code;
            $item['name'] = @$product->name;
            $item['qty_inventory'] = @numberFormat($item->qty);
            $item['qty_diff'] = 0;
            $item['price'] = $item->price;

            $this->items[] = $item;
        }

        // Update the state variables
        $this->purchaseInvoiceSearching = false;
        $this->purchaseInvoiceSearch = $this->purchaseInvoice->invoice_number;
    }

    /**
     * Render the view for the purchase invoice search.
     */
    public function render(): Factory|View|Application
    {
        // Return the view for the purchase invoice search
        return view('livewire.purchase-invoice-search');
    }
}
