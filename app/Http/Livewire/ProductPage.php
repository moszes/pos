<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class ProductPage extends Component
{
    /**
     * Render the product page view.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.product-page');
    }
}
