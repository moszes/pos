<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class CompanyDetailForm extends Component
{
    public $code;

    public $name;

    public $email;

    public $phone;

    public $token;

    protected $queryString = ['token'];

    public function updateCompany()
    {
        dd('test');
    }

    /**
     * Render the company detail form view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.company-detail-form');
    }
}
