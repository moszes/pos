<?php

namespace App\Http\Livewire;

use App\Models\Treatment;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class TreatmentDetail extends Component
{
    public $formTitle = 'Treatment Details';

    public $treatment;

    protected $listeners = [
        'showTreatmentDetail',
        'clearTreatmentDetail',
    ];

    /**
     * Show the details of a treatment.
     */
    public function showTreatmentDetail(mixed $id): void
    {
        // Find the treatment by its ID
        $this->treatment = Treatment::find($id);
    }

    /**
     * Clear the treatment detail by resetting the treatment property.
     */
    public function clearTreatmentDetail(): void
    {
        // Set the treatment property to null
        $this->treatment = null;
    }

    /**
     * Render the treatment detail view.
     */
    public function render(): View
    {
        // Return the view for the treatment detail page
        return view('livewire.treatment-detail');
    }
}
