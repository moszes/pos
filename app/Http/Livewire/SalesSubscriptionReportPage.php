<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SalesSubscriptionReportPage extends Component
{
    public $showDocument;

    public function render()
    {
        return view('livewire.sales-subscription-report-page');
    }
}
