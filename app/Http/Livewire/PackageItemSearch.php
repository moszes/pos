<?php

namespace App\Http\Livewire;

use App\Models\Product;
use App\Models\Treatment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PackageItemSearch extends Component
{
    public $packageItemList = false;

    public $packageItemForm = false;

    public $products;

    public $treatment;

    public $packageItemSearch;

    public $packageItemResults;

    public $packageItems = [];

    public $packageItem;

    public $packageItemModule;

    public $packageItemCode;

    public $packageItemName;

    public $packageItemPrice = 0;

    public $packageItemQty = 1;

    protected $listeners = [
        'setPackageItems',
        'clearPackageItemForm',
    ];

    /**
     * Update the package item search results
     */
    public function updatedPackageItemSearch(): void
    {
        // Set packageItemList to true
        $this->packageItemList = true;

        // Search products by code or name and add module and module_id to the array
        $this->products = Product::where('code', 'like', '%'.$this->packageItemSearch.'%')
            ->orWhere('name', 'like', '%'.$this->packageItemSearch.'%')
            ->get();

        $this->products->map(function ($product) {
            $product->module = 'product';
            $product->module_id = $product->id;

            return $product;
        });

        // Search treatment by code or name and add module and module_id to the array
        $this->treatment = Treatment::where('code', 'like', '%'.$this->packageItemSearch.'%')
            ->orWhere('name', 'like', '%'.$this->packageItemSearch.'%')
            ->get();

        $this->treatment->map(function ($treatment) {
            $treatment->module = 'treatment';
            $treatment->module_id = $treatment->id;

            return $treatment;
        });

        // Merge the search results into packageItemResults
        $this->packageItemResults = array_merge($this->products->toArray(), $this->treatment->toArray());

        // Clear packageItemResults if packageItemSearch is empty
        if ($this->packageItemSearch == '') {
            $this->packageItemResults = [];
        }
    }

    /**
     * Selects a package item based on the provided module ID, code, and module.
     *
     * @param  int  $moduleId  The ID of the module
     * @param  string  $code  The code of the package item
     * @param  string  $module  The module of the package item
     */
    public function selectPackageItem(int $moduleId, string $code, string $module): void
    {
        // Set the package item form flag to true
        $this->packageItemForm = true;

        // Retrieve the package item data based on the module and module ID
        $this->packageItem = getDataByModule($module, $moduleId);

        // Set the package item module, code, name, and price based on the retrieved package item data
        $this->packageItemModule = $module;
        $this->packageItemCode = $code;
        $this->packageItemName = $this->packageItem->name;
        $this->packageItemPrice = $this->packageItem->price;

        // Reset the package item list flag and search value
        $this->packageItemList = false;
        $this->packageItemSearch = null;
    }

    /**
     * Set the package items.
     *
     * @param  mixed  $packageItems  The package items to be set.
     */
    public function setPackageItems(mixed $packageItems): void
    {
        // Assign the provided package items to the class property.
        $this->packageItems = $packageItems;
    }

    /**
     * Add a package item to the list
     */
    public function addPackageItem(): void
    {
        // Update package item properties
        if ($this->packageItem) {
            $this->packageItem->price = $this->packageItemPrice;
            $this->packageItem->module = $this->packageItemModule;
            $this->packageItem->module_id = $this->packageItem->id;
            $this->packageItem->qty = $this->packageItemQty;
            $this->packageItem->normal_price = $this->packageItem->price;
            $this->packageItem->sell_price = $this->packageItem->price / $this->packageItem->qty;

            // Add package item to the list
            $this->packageItems[] = $this->packageItem->toArray();
        }

        $this->emit('setPackageItems', $this->packageItems);

        // Hide the package item form
        $this->packageItemForm = false;
    }

    /**
     * @param  int  $arrayKey  The key of the item to be removed.
     */
    public function removePackageItem(int $arrayKey): void
    {
        // Remove the package item from the array
        unset($this->packageItems[$arrayKey]);

        $this->emit('setPackageItems', $this->packageItems);
    }

    /**
     * Clears the package item form by resetting the form data and error bag.
     */
    public function clearPackageItemForm(): void
    {
        // Reset the form data
        $this->reset();

        // Reset the error bag
        $this->resetErrorBag();
    }

    /**
     * Render the view for the package item search.
     */
    public function render(): View|Factory|Application
    {
        // Return the view for the package item search
        return view('livewire.package-item-search');
    }
}
