<?php

namespace App\Http\Livewire;

use App\Models\Appointment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class AppointmentPage extends Component
{
    public $customer = [];

    public $addToCart = false;

    public $appointment = [];

    public $items = [];

    public $formAction = 'createAppointment';

    protected $listeners = [
        'cartOpen',
        'setActionButton',
        'cartClose',
    ];

    /**
     * Sets the value of the formAction property.
     *
     * @param  mixed  $value  The value to set for the formAction property.
     */
    public function setActionButton(mixed $value): void
    {
        // Set the value of the formAction property
        $this->formAction = $value;
    }

    /**
     * Opens the cart.
     *
     * @param  int|null  $appointmentId  The ID of the appointment to open the cart for.
     * @param  int|null  $customerId  The ID of the customer to associate with the cart.
     */
    public function cartOpen(?int $appointmentId = null, ?int $customerId = null): void
    {
        // Set addToCart flag to true
        $this->addToCart = true;

        // If appointmentId is provided, fetch the appointment and its items
        if (! is_null($appointmentId)) {
            $this->appointment = Appointment::find($appointmentId);
            $this->items = $this->appointment->items()->get()->toArray();
        }

        // If customerId is provided, get the customer details and emit 'setCustomer' event
        if (! is_null($customerId)) {
            $this->customer = getCustomerById($customerId);
            $this->emit('setCustomer', $this->customer);
        }
    }

    /**
     * Marks the cart as closed.
     */
    public function cartClose(): void
    {
        $this->addToCart = false;
        $this->appointment = [];
    }

    /**
     * Render the appointment page view.
     */
    public function render(): Factory|View|Application
    {
        // if appointment is object
        if (is_object($this->appointment)) {
            $this->appointment = $this->appointment->toArray();
        }

        // Return the appointment page view
        return view('livewire.appointment-page', [
            'appointment' => $this->appointment,
            'customer' => $this->customer,
            'items' => $this->items,
        ]);
    }
}
