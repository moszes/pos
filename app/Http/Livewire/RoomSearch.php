<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class RoomSearch extends Component
{
    public $roomId = '-- Choose --';

    public $roomStart;

    public $roomEnd;

    public $room = [];

    protected $listeners = [
        'clearRoomSearch',
        'clearRoomErrorBag',
        'roomRequired',
        'roomStartRequired',
        'roomEndRequired',
    ];

    public function updated(): void
    {
        $this->resetErrorBag();
    }

    /**
     * Update the room ID in the class variable.
     *
     * This function updates the 'room_id' property in the class variable based on the value of 'roomId'.
     * If 'roomId' is '-- Choose --', the 'room_id' property is set to null.
     * Otherwise, it is set to the value of 'roomId'.
     * After updating the 'room_id' property, the 'roomSelected' event is emitted.
     */
    public function updatedRoomId(): void
    {
        $this->room['room_id'] = $this->roomId == '-- Choose --' ? null : $this->roomId;
        $this->emit('roomSelected', $this->room);
    }

    /**
     * Updates the 'room_start' property of the room object.
     *
     * This function assigns the value of the 'roomStart' property to the 'room_start' property
     * of the room object.
     */
    public function updatedRoomStart(): void
    {
        $this->room['room_start'] = date('Y-m-d').' '.$this->roomStart;
        $this->emit('roomSelected', $this->room);
    }

    /**
     * Updates the 'room_end' property of the room object.
     *
     * This function assigns the value of the 'roomEnd' property to the 'room_end' property
     * of the room object.
     */
    public function updatedRoomEnd(): void
    {
        $this->room['room_end'] = date('Y-m-d').' '.$this->roomEnd;
        $this->emit('roomSelected', $this->room);
    }

    public function roomRequired(): void
    {
        $this->addError('roomId', 'Room is required');
    }

    public function roomStartRequired(): void
    {
        $this->addError('roomStart', 'Room start is required');
    }

    public function roomEndRequired(): void
    {
        $this->addError('roomEnd', 'Room end is required');
    }

    /**
     * Clears the error bag for the room.
     */
    public function clearRoomErrorBag(): void
    {
        // Reset the error bag for the room
        $this->resetErrorBag();
    }

    /**
     * Clears the room.
     */
    public function clearRoomSearch(): void
    {
        // Reset the state of the room
        $this->reset();
    }

    /**
     * Renders the view for the PHP function.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.room-search');
    }
}
