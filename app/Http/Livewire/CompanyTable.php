<?php

namespace App\Http\Livewire;

use App\Models\Company;
use Livewire\Component;
use Livewire\WithPagination;

class CompanyTable extends Component
{
    use WithPagination;

    public $titlePage = 'Company';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshCompany' => '$refresh',
    ];

    public function paginationView()
    {
        return 'pagination';
    }

    public function render()
    {
        return view('livewire.company-table', [
            'companies' => Company::where('name', 'like', '%'.$this->search.'%')
                ->orWhere('code', 'like', '%'.$this->search.'%')
                ->orWhere('email', 'like', '%'.$this->search.'%')
                ->orWhere('phone', 'like', '%'.$this->search.'%')
                ->orderBy('id', 'desc')
                ->paginate(10),
        ]);
    }
}
