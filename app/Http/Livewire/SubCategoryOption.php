<?php

namespace App\Http\Livewire;

use App\Models\SubCategory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SubCategoryOption extends Component
{
    public $subCategoryId;

    public $subCategoryOption;

    protected $listeners = [
        'generateSubCategoryOptions',
        'setSubCategoryId',
        'clearSubCategoryOption',
    ];

    /**
     * Notify when the subcategory id is updated.
     *
     * @param  mixed  $id  The updated subcategory id
     */
    public function updatedSubcategoryId(mixed $id): void
    {
        // Emit event to notify subcategory selection
        $this->emit('subCategorySelected', $id);
    }

    /**
     * Set the subcategory ID.
     *
     * @param  mixed  $id  The ID of the subcategory
     */
    public function setSubCategoryId(mixed $id): void
    {
        $this->subCategoryId = $id;
    }

    /**
     * Generates the subcategory options based on the provided category ID.
     *
     * @param  mixed  $id  The ID of the category.
     */
    public function generateSubCategoryOptions(mixed $id): void
    {
        // Retrieve all subcategories based on the provided category ID
        $subCategories = SubCategory::where('category_id', $id)->get();

        // Generate the HTML options for each subcategory
        foreach ($subCategories as $subCategory) {
            // Append the option HTML to the subCategoryOption property
            $this->subCategoryOption .= '<option value="'.$subCategory->id.'">'.$subCategory->name.'</option>';
        }
    }

    /**
     * Clears the subcategory option.
     */
    public function clearSubCategoryOption(): void
    {
        // Reset the form fields
        $this->reset();

        // Reset the error bag
        $this->resetErrorBag();
    }

    /**
     * Render the view for subcategory options.
     */
    public function render(): View|Factory|Application
    {
        // Return the view for the subcategory option
        return view('livewire.sub-category-option');
    }
}
