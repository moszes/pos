<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AdjustRedemptionPage extends Component
{
    public function render()
    {
        return view('livewire.adjust-redemption-page');
    }
}
