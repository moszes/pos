<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class UnitOfMeasurePage extends Component
{
    /**
     * Render the unit of measure page.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.unit-of-measure-page');
    }
}
