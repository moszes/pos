<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SalesInvoiceItemTable extends Component
{
    public $items = [];

    public $salesInvoiceTax = 0;

    public $salesInvoiceOthers = 0;

    public $salesInvoiceDiscount = 0;

    public $note;

    public $subTotal = 0;

    public $grandTotal = 0;

    public $showDetails = true;

    protected $listeners = [
        'setItems',
        'setSalesInvoiceOthers',
        'setSalesInvoiceTax',
        'setSalesInvoiceDiscount',
        'setSubTotal',
        'setGrandTotal',
        'setShowDetails',
        'setNote',
        'clearSalesInvoiceFormTable',
        'clearItems',
    ];

    /**
     * Enable showing details.
     */
    public function setShowDetails($showDetails): void
    {
        // Set the showDetails property to true.
        $this->showDetails = $showDetails;
    }

    /**
     * Set the sales invoice others value.
     *
     * @param  mixed  $salesInvoiceOthers  The value to set for sales invoice others.
     */
    public function setSalesInvoiceOthers(mixed $salesInvoiceOthers): void
    {
        $this->salesInvoiceOthers = $salesInvoiceOthers;
    }

    /**
     * Sets the sales invoice tax.
     *
     * @param  float|int  $salesInvoiceTax  The tax amount for the sales invoice.
     */
    public function setSalesInvoiceTax(float|int $salesInvoiceTax): void
    {
        $this->salesInvoiceTax = $salesInvoiceTax;
    }

    /**
     * Sets the sales invoice discount.
     *
     * @param  float|int  $salesInvoiceDiscount  The discount amount for the sales invoice.
     */
    public function setSalesInvoiceDiscount(float|int $salesInvoiceDiscount): void
    {
        $this->salesInvoiceDiscount = $salesInvoiceDiscount;
    }

    /**
     * Calculates the subtotal of an array of numbers.
     *
     * @return void The subtotal of the numbers in the array.
     */
    public function setSubTotal($subTotal): void
    {
        $this->subTotal = $subTotal;
    }

    /**
     * Calculates the grand total of an array of numbers.
     *
     * @return void The grand total of the numbers in the array.
     */
    public function setGrandTotal($grandTotal): void
    {
        $this->grandTotal = $grandTotal;
    }

    /**
     * Sets the note for the current object.
     *
     * @param  mixed  $note  The note to be set.
     */
    public function setNote(mixed $note): void
    {
        $this->note = $note;
    }

    /**
     * Sets the items array.
     *
     * @param  array  $items  An array of items.
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    /**
     * Removes an item from the array using the given key.
     *
     * @param  mixed  $arrayKey  The key of the item to be removed.
     */
    public function removeItem(mixed $arrayKey): void
    {
        // Remove the item from the array using the given key.
        unset($this->items[$arrayKey]);

        $this->subTotal = collect($this->items)->sum('total_price');

        $this->grandTotal = $this->subTotal - $this->salesInvoiceDiscount + $this->salesInvoiceTax + $this->salesInvoiceOthers;

        $this->emit('setItems', $this->items);
        $this->emit('setSubTotal', $this->subTotal);
        $this->emit('setGrandTotal', $this->grandTotal);
    }

    public function clearItems(): void
    {
        $this->items = [];
    }

    /**
     * Clears the sales invoice form table.
     */
    public function clearSalesInvoiceFormTable(): void
    {
        $this->resetErrorBag();
        $this->reset();
    }

    /**
     * Render the sales invoice form table view.
     */
    public function render(): Application|Factory|View
    {
        // Return the sales invoice form table view
        return view('livewire.sales-invoice-item-table');
    }
}
