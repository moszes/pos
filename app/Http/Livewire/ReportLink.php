<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ReportLink extends Component
{
    public function render()
    {
        return view('livewire.report-link');
    }
}
