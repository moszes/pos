<?php

namespace App\Http\Livewire;

use App\Models\Package;
use DB;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Livewire\Component;

class PackageForm extends Component
{
    public $formTitle = 'Package Form';

    public $actionForm = 'store';

    public $type = 'package';

    public $code;

    public $name;

    public $price = 0;

    public $package;

    protected $listeners = [
        'categorySelected',
        'subCategorySelected',
        'resetSubCategoryId',
        'clearPackageForm',
        'editPackage',
        'destroy',
    ];

    protected $rules = [
        'code' => 'required|unique:packages',
        'name' => 'required',
    ];

    /**
     * Update the price value based on the input value.
     *
     * @param  mixed  $value  The input value for the price.
     */
    public function updatedPrice(mixed $value): void
    {
        // Convert the input value to a float and assign it to the price property.
        $this->price = floatval($value);
    }

    /**
     * Generate an array of package data.
     *
     * @return array The generated package data.
     */
    public function packageData(): array
    {
        // Get the ID of the first company
        $companyId = auth()->user()->details->branch_id;

        // Get the ID of the first branch
        $branchId = auth()->user()->details->branch_id;

        // Generate the package data array
        return [
            'company_id' => $companyId,
            'branch_id' => $branchId,
            'code' => $this->code,
            'name' => $this->name,
            'price' => $this->price ?? 0,
        ];
    }

    /**
     * Store a new package record in the database.
     */
    public function store(): void
    {
        // Validate the data before proceeding
        $this->validate();

        // Start a database transaction
        DB::transaction(function () {
            // Create a new package record with the provided data
            $this->package = Package::create($this->packageData());
        }, 5);

        // Emit an event to clear the package form
        $this->emit('clearPackageForm');
    }

    /**
     * Edit a package.
     *
     * @param  int  $id  The ID of the package to edit.
     */
    public function editPackage(int $id): void
    {
        // Show the package form
        $this->emit('showPackageForm');

        // Set the category type to 'package'
        $this->emit('setCategoryType', 'package');

        // Find the package by ID
        $this->package = Package::find($id);

        // Set the code, name, and price of the package
        $this->code = $this->package->code;
        $this->name = $this->package->name;
        $this->price = countFormat($this->package->price);

        // Set the action form to 'update'
        $this->actionForm = 'update';
    }

    /**
     * Update the package data in a transaction and emit a refresh event.
     */
    public function update(): void
    {
        // Update the package data in a transaction
        DB::transaction(function () {
            $this->package->update($this->packageData());
        }, 5);

        // Clear the package form after updating
        $this->clearPackageForm();

        // Emit an event to refresh the package
        $this->emit('refreshPackage');
    }

    /**
     * Delete a package and its related fees and material usages.
     *
     * @param  int  $id  The ID of the package to be deleted
     */
    public function destroy(int $id): void
    {
        // Find the package by ID
        $this->package = Package::find($id);

        // Use a database transaction to ensure data consistency
        DB::transaction(function () {
            // Delete the package
            $this->package->delete();
            // Delete the related fees
            $this->package->normalFees()->delete();
            $this->package->discountFees()->delete();
            $this->package->targetFees()->delete();
            // Delete the related material usages
            $this->package->items()->delete();
        }, 5);

        // Emit an event to clear the package form
        $this->emit('clearPackageForm');
    }

    /**
     * Clears the package form.
     */
    public function clearPackageForm(): void
    {
        // Reset the component state
        $this->reset();
        $this->resetErrorBag();

        // Emit events to clear category, subcategory, and package fee options
        $this->emit('clearCategoryOption');
        $this->emit('clearSubCategoryOption');

        // Emit events to hide the package form and refresh the package
        $this->emit('hidePackageForm');
        $this->emit('refreshPackage');
    }

    /**
     * Render the view for the package form.
     *
     * @return View|Application|Factory The rendered view.
     */
    public function render(): View|Application|Factory
    {
        // Return the rendered view for the package form.
        return view('livewire.package-form');
    }
}
