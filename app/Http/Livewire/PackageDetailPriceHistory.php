<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class PackageDetailPriceHistory extends Component
{
    public $package;

    /**
     * Render the package detail price history view.
     *
     * @return View The rendered view.
     */
    public function render(): View
    {
        // Return the view for the package detail price history
        return view('livewire.package-detail-price-history');
    }
}
