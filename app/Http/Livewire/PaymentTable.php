<?php

namespace App\Http\Livewire;

use App\Models\Payment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class PaymentTable extends Component
{
    use WithPagination;

    public $titlePage = 'Payment';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshPaymentTable' => '$refresh',
    ];

    /**
     * Reset the pagination page when the search input is updated.
     */
    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    /**
     * Returns the name of the pagination view.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Apply search filter to retrieve payments.
     */
    public function searchFilter(): LengthAwarePaginator
    {
        return Payment::where('company_id', auth()->user()->details->company_id)
            ->where(function ($query) {
                $query->where('code', 'like', '%'.$this->search.'%')
                    ->orWhere('name', 'like', '%'.$this->search.'%');
            })
            ->paginate(10);
    }

    /**
     * Render the payment table view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.payment-table', [
            'payments' => $this->searchFilter(),
        ]);
    }
}
