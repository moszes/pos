<?php

namespace App\Http\Livewire;

use Livewire\Component;

class DepositPage extends Component
{
    public function render()
    {
        return view('livewire.deposit-page');
    }
}
