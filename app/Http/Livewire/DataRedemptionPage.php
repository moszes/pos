<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class DataRedemptionPage extends Component
{
    use WithPagination;

    public $i = 1;

    public $date;

    public $module = 'redemption';

    public $customerRedemptions;

    public $user;

    public $customerId;

    protected $queryString = [
        'customerId',
    ];

    /**
     * Mount the component.
     */
    public function mount(): void
    {
        $this->user = User::find($this->customerId);

        $this->date = now()->format('Y-m-d');
    }

    /**
     * Returns the view for pagination.
     *
     * @return string The name of the pagination view.
     */
    public function paginationView(): string
    {
        // Return the name of the pagination view
        return 'pagination';
    }

    /**
     * Render the view for the data redemption page.
     */
    public function render(): Factory|View|Application
    {
        // Return the view for the data redemption page with the user and date data
        return view('livewire.data-redemption-page', [
            'date' => $this->date,
        ]);
    }
}
