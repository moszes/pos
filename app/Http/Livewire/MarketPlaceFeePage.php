<?php

namespace App\Http\Livewire;

use App\Models\MarketPlace;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\View\View;
use Livewire\Component;

class MarketPlaceFeePage extends Component
{
    public $marketplaceId;

    public $marketplace = 'marketplace';

    public $moduleLabel = 'Market Place Details';

    public $moduleName = 'marketplace';

    public $redirectRoute;

    public $priceLabel = true;

    /**
     * Mounts the component and initializes the necessary data.
     *
     * This function retrieves the marketplace ID from the request segment, initializes the fee values,
     * finds the marketplace by ID, and updates the fee values if the marketplace exists.
     */
    public function mount(): void
    {
        // Get the marketplace ID from the request segment
        $this->marketplaceId = request()->segment(count(request()->segments()));

        // Find the marketplace by ID
        $this->marketplace = MarketPlace::find($this->marketplaceId);

        if (! $this->marketplace) {
            redirect(route('master.marketplaces'));
        }
    }

    /**
     * Render the marketplace fee page view.
     */
    public function render(): Factory|Application|View
    {
        // Retrieve the marketplace using the marketplaceId property
        $marketplace = MarketPlace::find($this->marketplaceId);

        // Return the livewire.marketplace-fee-page view with the marketplace data
        return view('livewire.market-place-fee-page', [
            'marketplace' => $marketplace,
        ]);
    }
}
