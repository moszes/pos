<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class ErrorPage extends Component
{
    /**
     * Renders the error page view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.error-page');
    }
}
