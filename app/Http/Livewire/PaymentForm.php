<?php

namespace App\Http\Livewire;

use App\Models\Payment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class PaymentForm extends Component
{
    public $formTitle = 'Form Payment';

    public $actionForm = 'store';

    public $payment;

    public $companyId;

    public $name;

    public $type;

    protected $listeners = [
        'clearForm',
        'editPayment',
        'destroy',
    ];

    protected $rules = [
        'name' => 'required',
    ];

    /**
     * Mount the component.
     */
    public function mount(): void
    {
        $this->companyId = auth()->user()?->details->company_id;
    }

    /**
     * Store a new payment record.
     */
    public function store(): void
    {
        $this->validate();

        Payment::create([
            'company_id' => $this->companyId,
            'code' => generatePaymentCode(),
            'name' => $this->name,
        ]);

        $this->clearForm();
        $this->emit('dataSaved');
        $this->emit('refreshPaymentTable');
    }

    /**
     * Edit a payment.
     *
     * @param  int  $id  The ID of the payment to edit.
     */
    public function editPayment(int $id): void
    {
        $this->payment = Payment::find($id);

        $this->companyId = $this->payment->company_id;
        $this->name = $this->payment->name;
        $this->type = $this->payment->type;

        $this->actionForm = 'update';
        $this->emit('showPaymentForm');
    }

    public function update(): void
    {
        $this->validate();

        $this->payment->update([
            'company_id' => $this->companyId,
            'name' => $this->name,
        ]);

        $this->actionForm = 'save';

        $this->clearForm();
        $this->emit('dataUpdated');
        $this->emit('refreshPaymentTable');
    }

    /**
     * Deletes a payment record.
     *
     * @param  int  $id  The ID of the payment record to delete.
     */
    public function destroy(int $id): void
    {
        Payment::find($id)->delete();

        $this->actionForm = 'save';

        $this->clearForm();
        $this->emit('dataDeleted');
        $this->emit('refreshPaymentTable');
    }

    /**
     * Clears the form by resetting the values and emitting an event to hide the payment form.
     */
    public function clearForm(): void
    {
        $this->emit('hidePaymentForm');
        $this->actionForm = 'store';

        $this->name = null;
    }

    /**
     * Renders the payment form view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.payment-form');
    }
}
