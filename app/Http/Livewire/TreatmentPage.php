<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class TreatmentPage extends Component
{
    /**
     * Renders the service page view.
     */
    public function render(): View|Factory|Application
    {
        // Return the service page view
        return view('livewire.treatment-page');
    }
}
