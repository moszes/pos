<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class RoomPage extends Component
{
    /**
     * Renders the view for the 'room-page' Livewire component.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.room-page');
    }
}
