<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class SubCategoryPage extends Component
{
    /**
     * Render the subcategory page.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.sub-category-page');
    }
}
