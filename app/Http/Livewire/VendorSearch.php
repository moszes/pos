<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class VendorSearch extends Component
{
    public $vendorId = '-- Choose --';

    protected $listeners = [
        'selectVendorById',
        'clearVendorSearch',
    ];

    /**
     * Sets the vendor ID.
     *
     * @param  int  $vendorId  The ID of the vendor
     */
    public function selectVendorById(mixed $vendorId): void
    {
        if ($vendorId !== null && $vendorId !== '-- Choose --') {
            $this->vendorId = $vendorId;
        }
    }

    /**
     * Update the vendor ID and emit an event.
     *
     * @param  int  $vendorId  The new vendor ID.
     */
    public function updatedVendorId(int $vendorId): void
    {
        // Emit the 'selectedVendor' event with the new vendor ID.
        $this->emit('setVendor', $vendorId);
    }

    /**
     * Clears the vendor search.
     */
    public function clearVendorSearch(): void
    {
        // Reset the search
        $this->reset();
    }

    /**
     * Render the view for the vendor search.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the rendered view for the vendor search
        return view('livewire.vendor-search');
    }
}
