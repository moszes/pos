<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class MarketPlaceDetailInfo extends Component
{
    public $marketplace;

    public $moduleLabel;

    public $moduleName;

    public $redirectRoute;

    public $priceLabel = true;

    /**
     * Render the view for the marketplace detail information.
     */
    public function render(): View
    {
        return view('livewire.market-place-detail-info');
    }
}
