<?php

namespace App\Http\Livewire;

use App\Http\Controllers\UserController;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithFileUploads;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Storage;

class CoverImageFileUpload extends Component
{
    use WithFileUploads;

    public $user;

    public $coverImage;

    public $coverImageUrl;

    protected $listeners = [
        'refresh' => '$refresh',
    ];

    /**
     * Mount the component.
     */
    public function mount(): void
    {
        $this->user = auth()->user();
        $this->coverImageUrl = UserController::coverImage($this->user);
    }

    /**
     * Update the cover image.
     *
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function updatedCoverImage(): void
    {
        $this->validate([
            'coverImage' => 'image', // Validate that the upload is an image
        ]);

        DB::transaction(function () {
            if ($this->coverImage && $this->coverImage->isValid()) {
                $this->user = auth()->user();

                $imageUrl = $this->coverImage->store('photos', 'gcs');

                $this->user->addMediaFromUrl(Storage::url($imageUrl))->toMediaCollection('users');

                $this->user->details->cover_image_file_name = $imageUrl;
                $this->user->details->save();

                $this->coverImageUrl = UserController::coverImage($this->user);
            }
        }, 5);
    }

    /**
     * Render the component.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.cover-image-file-upload');
    }
}
