<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class ProductSearch extends Component
{
    public $productList = false;

    public $productSearch;

    public $products;

    public $product = [];

    public $productQty = 1;

    public $productPrice = 0;

    public $productDiscountItem = 0;

    public $item;

    protected $listeners = [
        'clearProductSearch',
        'productRequired',
    ];

    /**
     * Updates the specified property and emits a productSelected event.
     *
     * @param  string  $name  The name of the property to update.
     * @return void
     */
    // Constants at the top of the ProductSearch class
    const PRODUCT_FIELD_MAPPINGS = [
        'productQty' => 'qty',
        'productDiscountItem' => 'discount_item',
        'productPrice' => 'price',
    ];

    /**
     * Reset the error bag and update the product based on the provided name.
     *
     * @param  mixed  $name  The name to update the product with.
     */
    public function updated(mixed $name): void
    {
        $this->emit('clearPersonInCharge');
        $this->emit('clearRoomSearch');
        $this->emit('setModule', 'product');

        $this->resetErrorBag();

        if ($this->shouldUpdateProduct($name)) {
            foreach (self::PRODUCT_FIELD_MAPPINGS as $field => $map) {
                $this->product[$map] = $this->$field;
            }
            $this->emit('productSelected', $this->product);
        }
    }

    /**
     * Check if the product discount item has been updated and emit an event to apply the discount.
     */
    public function updatedProductDiscountItem($value): void
    {
        $this->productDiscountItem = floatval($value);

        // Check if the product discount item is greater than 0
        if ($this->productDiscountItem > 0) {
            // Emit the 'applyDiscount' event
            $this->emit('applyDiscount');
        }
    }

    /**
     * @param  mixed  $name  The name to check against the product field mappings.
     * @return bool Returns true if the provided name is found in the product field mappings, otherwise returns false.
     */
    private function shouldUpdateProduct(mixed $name): bool
    {
        return in_array($name, array_keys(self::PRODUCT_FIELD_MAPPINGS));
    }

    /**
     * Update the product search results based on the provided search term.
     */
    public function updatedProductSearch(): void
    {
        if ($this->productSearch !== '') {
            $this->productList = true;
            $this->products = Product::where('company_id', auth()->user()->details->branch_id)
                ->where(function ($query) {
                    $query->where('code', 'LIKE', "%$this->productSearch%")
                        ->orWhere('name', 'LIKE', "%$this->productSearch%");
                })
                ->take(10)
                ->get();
        } else {
            $this->productList = false;
            $this->products = null;
        }
    }

    /**
     * Handle the selection of a product during a search.
     *
     * @param  int  $id  The ID of the selected product.
     */
    public function selectProduct(int $id): void
    {
        // Find the selected product by ID
        $this->product = Product::find($id)->toArray();
        $this->configureProduct($id);

        $this->productList = false;
        $this->products = null;

        // Emit 'productSelected' event
        $this->emit('productSelected', $this->product);
    }

    /**
     * Configure the product's properties.
     */
    protected function configureProduct($id): void
    {
        $this->productSearch = $this->product['code'].' - '.$this->product['name'];
        $this->productPrice = floatval($this->product['normal_price']);
        $this->product['qty'] = floatval($this->productQty);
        $this->product['price'] = $this->productPrice;
        $this->product['discount_item'] = floatval($this->productDiscountItem);
        $this->product['total_price'] = $this->productQty * $this->productPrice - $this->productDiscountItem;
        $this->product['transaction_status'] = 'buy';
        $this->product['module'] = 'product';
        $this->product['module_id'] = $id;
    }

    /**
     * Adds a product item.
     */
    public function addProductItem(): void
    {
        // Create an array with the details of the product item
        $this->item = $this->getProductItemDetails();

        // Emit event to notify that a product item has been added
        $this->emit('addProductItem', $this->item);

        // Emit event to update the subtotal
        $this->emit('setSubTotal');

        // Emit event to clear the product item data
        $this->emit('clearProductSearch');
    }

    /**
     * Get the details of the product item.
     *
     * @return array The details of the product item.
     */
    private function getProductItemDetails(): array
    {
        return [
            'product' => $this->product,
            'itemCode' => $this->productItemCode,
            'itemName' => $this->productItemName,
            'unit' => $this->product->size.' '.getUnitOfMeasureById($this->product->unit_of_measure_id)->name,
            'qty' => countFormat($this->productItemQty),
            'discount_item' => countFormat($this->productItemDiscount),
            'last_price' => countFormat(getLastPriceByProductId($this->product->id)),
            'price' => countFormat($this->productItemPrice),
            'total_price' => $this->productItemQty * $this->productItemPrice - $this->productItemDiscount,
        ];
    }

    /**
     * Add an error message to the 'productSearch' field indicating that a product is required.
     */
    public function productRequired(): void
    {
        $this->addError('productSearch', 'Please select a product.');
    }

    /**
     * Clears the product item.
     */
    public function clearProductSearch(): void
    {
        // Reset the product item
        $this->reset();
    }

    /**
     * Render the view for the product search component.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the rendered view for the product search component
        return view('livewire.product-search');
    }
}
