<?php

namespace App\Http\Livewire;

use App\Models\Product;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class MarketPlaceMaterialUsageTable extends Component
{
    public $marketplace;

    public $moduleLabel;

    public $moduleName;

    public $redirectRoute;

    public $priceLabel = false;

    public $productList = false;

    public $searchForProduct;

    public $products;

    public $product;

    public $productCode;

    public $productName;

    public $productCost = 0;

    public $productUsage = 1;

    public $productQty = 1;

    public $productForm = false;

    public $materialUsages = [];

    public $grandTotal = 0;

    /**
     * Mount function to initialize component data.
     */
    public function mount(): void
    {
        if ($this->marketplace) {
            $this->materialUsages = $this->marketplace->material_usages->toArray();
            $this->countGrandTotal();
        }
    }

    /**
     * Updates the value of the 'productCost' property with the given value.
     */
    public function updatedProductCost($value): void
    {
        $this->productCost = (float) $value;
    }

    /**
     * Update a product list based on search input
     *
     * @param  string  $search  The search input
     */
    public function updatedSearchForProduct(string $search): void
    {
        if ($search !== '') {
            // Set the product list to true
            $this->productList = true;

            // Search for products based on company ID and search input
            $this->products = Product::where('company_id', auth()->user()->details->branch_id)
                ->where(function ($query) use ($search) {
                    $query->where('code', 'LIKE', "%$search%")
                        ->orWhere('name', 'LIKE', "%$search%");
                })
                ->take(10)
                ->get();
        } else {
            // Set product list to false and reset products
            $this->productList = false;
            $this->products = null;
        }
    }

    /**
     * Select a product by its ID
     */
    public function selectProduct(int $id): void
    {
        $this->searchForProduct = null;
        // Set the product form flag to true
        $this->productForm = true;
        $this->productList = false;

        // Find the product by its ID
        $this->product = Product::find($id);

        $this->productCode = $this->product->code;
        $this->productName = $this->product->name;

    }

    /**
     * Adds a new material usage to the existing list.
     *
     * This function checks if the product code already exists in the materialUsages array.
     * If it doesn't exist, a new material usage is added to the array.
     * After adding the material usage, it clears the product form.
     */
    public function addMaterialUsage(): void
    {
        // Check if the product code exists in the materialUsages array
        $productExists = in_array($this->productCode, array_column($this->materialUsages, 'code'));

        // If product code does not exist, add a new material usage
        if (! $productExists) {
            $this->materialUsages[] = [
                'product_id' => $this->product->id,
                'qty' => $this->productQty,
                'size' => $this->product->size,
                'usage' => $this->productUsage,
                'normal_price' => floatval($this->product->normal_price),
                'agent_price' => floatval($this->product->agent_price),
                'cost' => $this->productCost,
            ];
        }

        $this->countGrandTotal();

        // Clear the product form
        $this->clearProductForm();
    }

    /**
     * Calculate the grand total of material usages.
     */
    public function countGrandTotal(): void
    {
        $this->grandTotal = collect($this->materialUsages)->sum('normal_price');
    }

    /**
     * This function is used to clear the product form.
     * It resets the values of various product form fields.
     */
    public function clearProductForm(): void
    {
        // Reset the product code to null
        $this->productCode = null;

        // Reset the product name to null
        $this->productName = null;

        // Reset the product quantity to 1
        $this->productQty = 1;

        // Reset the product usage to 1
        $this->productUsage = 1;

        // Reset the product cost to 0
        $this->productCost = 0;

        $this->productForm = false;
    }

    /**
     * Remove an item from the material usages array.
     */
    public function removeItem(int $key): void
    {
        unset($this->materialUsages[$key]);
        $this->countGrandTotal();
    }

    /**
     * Update the marketplace material usages.
     */
    public function update(): void
    {
        // Use a database transaction to ensure atomicity
        DB::transaction(function () {
            // Delete existing material usages
            $this->marketplace->material_usages()->delete();

            // Create new material usages
            $this->marketplace->material_usages()->createMany($this->materialUsages);
        }, 5);

        $this->redirect(route('master.market_places'));
    }

    /**
     * Render the view for marketplace material usage table.
     */
    public function render(): View|Factory|Application
    {
        // Return the view for marketplace material usage table
        return view('livewire.market-place-material-usage-table');
    }
}
