<?php

namespace App\Http\Livewire;

use Livewire\Component;

class TransactionLink extends Component
{
    public function render()
    {
        return view('livewire.transaction-link');
    }
}
