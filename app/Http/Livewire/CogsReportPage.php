<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CogsReportPage extends Component
{
    public function render()
    {
        return view('livewire.cogs-report-page');
    }
}
