<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoice;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SearchBySalesInvoiceNumber extends Component
{
    public $searchSalesInvoiceNumber;

    public $salesInvoiceLists = false;

    protected $listeners = [
        'getBySalesInvoiceNumber',
        'resetSalesInvoices',
        'selectSalesInvoice',
        'clearSearchBySalesInvoiceNumber',
    ];

    protected $queryString = [
        'searchSalesInvoiceNumber',
    ];

    /**
     * Set the sales invoice number and reset the sales invoice lists.
     *
     * @param  string  $salesInvoiceNumber  The sales invoice number to set.
     */
    public function selectSalesInvoice(string $salesInvoiceNumber): void
    {
        // Set the sales invoice number
        $this->searchSalesInvoiceNumber = $salesInvoiceNumber;

        $this->emit('setSalesInvoice', $salesInvoiceNumber);
        // Reset the sales invoice lists
        $this->reset('salesInvoiceLists');
    }

    /**
     * Update the sales invoices based on the sales invoice number.
     */
    public function updatedSearchSalesInvoiceNumber(): void
    {
        $this->salesInvoiceLists = true;
        // Emit an event to open the sales invoice widget
        $this->emit('openSalesInvoiceWidget');

        // Check if the sales invoice number is empty
        if ($this->searchSalesInvoiceNumber == '') {
            // Set the sales invoices to null and return
            $this->emit('closeSalesInvoiceWidget');
            $this->salesInvoiceLists = false;
        }
    }

    /**
     * Clears the search by sales invoice number and emits a close event.
     */
    public function clearSearchBySalesInvoiceNumber(): void
    {
        // Reset all properties of the component
        $this->reset();

        // Emit a close event to close the sales invoice widget
        $this->emit('closeSalesInvoiceWidget');
        $this->emit('clearSalesInvoiceNumber');
    }

    /**
     * Render the search-by-sales-invoice-number view.
     *
     * This function renders the view for the search-by-sales-invoice-number page.
     * It fetches the sales invoices based on the searchSalesInvoiceNumber property.
     */
    public function render(): Factory|View|Application
    {
        // Initialize an empty array for sales invoices
        $salesInvoices = [];

        // Check if the searchSalesInvoiceNumber property is not empty
        if ($this->searchSalesInvoiceNumber) {
            // Fetch the sales invoices whose sales_invoice_number matches the searchSalesInvoiceNumber
            $salesInvoices = SalesInvoice::where('sales_invoice_number', 'like', '%'.$this->searchSalesInvoiceNumber.'%')
                ->limit(10) // Limit the number of results to 10
                ->get(); // Get the results
        }

        // Return the view with the salesInvoices
        return view('livewire.search-by-sales-invoice-number', [
            'salesInvoices' => $salesInvoices,
        ]);
    }
}
