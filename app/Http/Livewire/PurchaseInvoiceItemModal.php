<?php

namespace App\Http\Livewire;

use App\Models\ApplicationSetting;
use App\Models\PurchaseInvoice;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PurchaseInvoiceItemModal extends Component
{
    public $formTitle = 'Purchase Invoice Details';

    public $purchaseInvoice;

    public $approvalPurchaseInvoice = false;

    public $purchaseInvoiceItems;

    public $subTotal = 0;

    public $grandTotal = 0;

    protected $listeners = [
        'showPurchaseInvoiceItems',
        'clearPurchaseInvoiceItems',
    ];

    /**
     * Mount the component.
     */
    public function mount(): void
    {
        $this->approvalPurchaseInvoice = ApplicationSetting::where('name', 'Approval Purchase Invoice')
            ->where('company_id', auth()->user()->details->branch_id)
            ->first()->status == 'Y';
    }

    /**
     * Approve a purchase invoice.
     *
     * @param  int  $id  The ID of the purchase invoice to be approved.
     */
    public function approve(int $id): void
    {
        $this->purchaseInvoice = PurchaseInvoice::find($id);

        $this->purchaseInvoice->update([
            'approved_date' => date('Y-m-d H:i:s'),
            'approved_by' => auth()->user()?->id,
        ]);

        $this->emit('approved');
        $this->emit('refreshPurchaseInvoice');
        $this->emit('hidePurchaseInvoiceItems');
    }

    /**
     * Show the purchase invoice items.
     *
     * @param  int  $id  The ID of the purchase invoice.
     */
    public function showPurchaseInvoiceItems(int $id): void
    {
        $this->purchaseInvoice = PurchaseInvoice::find($id);

        $this->purchaseInvoice->items->map(function ($item) {
            $item->purchase_invoice_number = $this->purchaseInvoice->purchase_invoice_number;
        });

        $this->subTotal = $this->purchaseInvoice->items->sum('total_price');

        $this->grandTotal = $this->purchaseInvoice->items->sum('total_price') + $this->purchaseInvoice->tax + $this->purchaseInvoice->others - $this->purchaseInvoice->discount;
    }

    /**
     * Clear purchase invoice items.
     *
     * Resets the purchaseInvoice property to null and sets the grandTotal property to 0.
     */
    public function clearPurchaseInvoiceItems(): void
    {
        $this->purchaseInvoice = null;
        $this->grandTotal = 0;
    }

    /**
     * Render the purchase invoice item modal.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the view for the purchase invoice item modal.
        return view('livewire.purchase-invoice-item-modal');
    }
}
