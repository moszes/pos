<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithPagination;
use Spatie\Permission\Models\Permission;

class PermissionTable extends Component
{
    use WithPagination;

    public $titlePage = 'Permission';

    public $search;

    public $selectedPermissions;

    public $userSearch;

    public $userSelected;

    public $userPermissions;

    public $users;

    public $searchingUser = false;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshPermission' => '$refresh',
    ];

    /**
     * Reset the page number when the search value is updated.
     */
    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    /**
     * Handle the User model "updated" event and search users based on their name.
     */
    public function updatedUserSearch(string $user): void
    {
        $this->searchingUser = true;
        $this->users = User::where('name', 'like', '%'.$user.'%')->get();
    }

    /**
     * Handle the User model "updated" permissions event.
     */
    public function userClicked(int $userId): void
    {
        $this->selectedPermissions = null;
        $this->userSelected = User::find($userId);
        $this->userPermissions = $this->userSelected->getAllPermissions();

        foreach ($this->userPermissions as $userPermission) {
            $this->selectedPermissions[$userPermission->id] = true;
        }

        $this->searchingUser = false;
        $this->userSearch = '';
    }

    /**
     * Handle the User model "updatedSelectedPermissions" event.
     */
    public function updatedSelectedPermissions(int $id): void
    {
        if (! $this->userSelected) {
            $this->emit('userNotSelected');

            $this->redirect(route('master.permissions'));
        }

        $permissions = $this->userSelected->permissions->push(Permission::find($id));

        $this->userSelected->syncPermissions($permissions->pluck('name')->toArray());
    }

    /**
     * Get the pagination view.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Apply search filter and return paginated results.
     */
    public function searchFilter(): LengthAwarePaginator|array
    {
        return Permission::where('name', 'like', '%'.$this->search.'%')
            ->orderBy('id', 'desc')
            ->paginate(100);
    }

    /**
     * Render the permission table view with permissions.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.permission-table', [
            'permissions' => $this->searchFilter(),
        ]);
    }
}
