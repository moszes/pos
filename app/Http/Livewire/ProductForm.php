<?php

namespace App\Http\Livewire;

use App\Models\Product;
use App\Models\SalesInvoiceItem;
use App\Models\ServiceFee;
use DB;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class ProductForm extends Component
{
    public $formTitle = 'Product Form';

    public $actionForm = 'store';

    public $type = 'product';

    public $product;

    public $categoryId;

    public $subCategoryId;

    public $code;

    public $name;

    public $size = 0;

    public $unitOfMeasureId = '-- Choose --';

    public $cost = 0;

    public $agentPrice = 0;

    public $normalPrice = 0;

    public $memberPrice = 0;

    protected $listeners = [
        'clearProductForm',
        'editProduct',
        'categorySelected',
        'subCategorySelected',
        'destroy',
    ];

    protected $rules = [
        'code' => 'required|unique:products',
        'name' => 'required',
        'unitOfMeasureId' => 'required',
        'size' => 'required',
        'normalPrice' => 'required',
    ];

    /**
     * Update the size property with the provided value.
     *
     * @param  float|string  $value  The new value for the size property.
     */
    public function updated(string $propertyName, float|string $value): void
    {
        $numberProperty = [
            'cost',
            'agentPrice',
            'normalPrice',
            'memberPrice',
            'size',
        ];

        if (in_array($propertyName, $numberProperty)) {
            $this->{$propertyName} = floatval($value);
        }
    }

    /**
     * Sets the selected category ID.
     *
     * @param  mixed  $categoryId  The ID of the selected category.
     */
    public function categorySelected(mixed $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * Sets the selected subCategory ID.
     *
     * @param  mixed  $subCategoryId  The ID of the selected category.
     */
    public function subCategorySelected(mixed $subCategoryId): void
    {
        $this->subCategoryId = $subCategoryId;
    }

    /**
     * Retrieve the product data as an array.
     *
     * @return array The product data array.
     */
    public function productData(): array
    {
        // Create and return the product data array
        return [
            'company_id' => auth()->user()->details->branch_id,
            'branch_id' => auth()->user()->details->branch_id,
            'category_id' => $this->categoryId == '-- Choose --' ? null : $this->categoryId,
            'sub_category_id' => $this->subCategoryId == '-- Choose --' ? null : $this->subCategoryId,
            'unit_of_measure_id' => $this->unitOfMeasureId == '-- Choose --' ? null : $this->unitOfMeasureId,
            'code' => $this->code,
            'name' => $this->name,
            'size' => $this->size,
            'cost' => $this->cost,
            'agent_price' => $this->agentPrice ?? 0,
            'normal_price' => $this->normalPrice ?? 0,
        ];
    }

    /**
     * Store the product data.
     */
    public function store(): void
    {
        // Validate the product data
        $this->validate();

        DB::transaction(function () {
            // Create a new product and save it to the database
            $this->product = Product::create($this->productData());

            generateCommissionFee($this->product, 'product');

        }, 5);

        // Emit events to indicate that the data has been saved and the product should be refreshed
        $this->emit('clearProductForm');
        $this->emit('refreshProduct');
    }

    /**
     * Edit a product.
     *
     * @param  int  $id  The ID of the product to edit.
     */
    public function editProduct(int $id): void
    {
        // Emit the 'showProductForm' event
        $this->emit('showProductForm');
        $this->emit('setCategoryType', 'product');

        // Find the product with the given ID
        $this->product = Product::find($id);

        $this->categoryId = $this->product->category_id;
        $this->subCategoryId = $this->product->sub_category_id;

        $this->emit('setCategoryId', $this->product->category_id);
        $this->emit('setSubCategoryId', $this->product->sub_category_id);
        $this->code = $this->product->code;
        $this->name = $this->product->name;
        $this->cost = countFormat($this->product->cost);
        $this->size = countFormat($this->product->size);

        // Retrieve the unit of measure ID of the product
        $this->unitOfMeasureId = $this->product->unit_of_measure_id;

        // Retrieve the customer price and member price of the product
        $this->agentPrice = countFormat($this->product->agent_price);
        $this->normalPrice = countFormat($this->product->normal_price);

        // Set the action form to 'update'
        $this->actionForm = 'update';
    }

    /**
     * Update the product and related entities.
     */
    public function update(): void
    {
        // check if code change
        if ($this->code != $this->product->code) {
            $this->validate([
                'code' => 'required|unique:products,code,'.$this->product->id,
            ]);
        }

        DB::transaction(function () {
            // Update the product with the updated data
            $this->product->update($this->productData());

        }, 5);

        // Emit 'refreshProduct' event
        $this->emit('clearProductForm');
        $this->emit('refreshProduct');
    }

    /**
     * Delete a product and its associated fees.
     *
     * @param  int  $id  The ID of the product to delete.
     */
    public function destroy(int $id): void
    {
        // Delete the product
        DB::transaction(function () use ($id) {

            // check if there are transaction on this product then prevent it from deleting

            $check = SalesInvoiceItem::where('module', 'product')
                ->where('module_id', $id)
                ->first();

            if ($check != null) {

                $this->emit('productHasBeenSold', 'Product cannot be deleted because it is in use.');

                return;
            }

            $product = Product::find($id);
            $product->code = $product->code.'-deleted';
            $product->save();

            $product->delete();

            // Delete the product fees
            ServiceFee::where('module', 'product')->where('module_id', $id)->delete();
        }, 5);

        $this->emit('clearProductForm');

        // Emit an event to refresh the product
        $this->emit('refreshProduct');
    }

    /**
     * Clears the product form and emits an event to hide the form.
     */
    public function clearProductForm(): void
    {
        // Reset the form
        $this->resetExcept('type');

        // Emit event to hide the form
        $this->emit('hideProductForm');
        $this->emit('clearCategoryOption');
        $this->emit('clearSubCategoryOption');
    }

    /**
     * Render the product form view.
     *
     * @return View The rendered view.
     */
    public function render(): View
    {
        return view('livewire.product-form');
    }
}
