<?php

namespace App\Http\Livewire;

use App\Models\AdjustRedemption;
use Livewire\Component;
use Livewire\WithPagination;

class AdjustRedemptionTable extends Component
{
    use WithPagination;

    public $titlePage = 'Adjustment Redemption';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshAdjustRedemption' => '$refresh',
    ];

    public function paginationView()
    {
        return 'pagination';
    }

    public function render()
    {
        return view('livewire.adjust-redemption-table', [
            'adjust_redemptions' => AdjustRedemption::where('qty', 'like', '%'.$this->search.'%')
                ->orderBy('id', 'desc')
                ->paginate(10),
        ]);
    }
}
