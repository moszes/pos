<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoice;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SalesReportTable extends Component
{
    public $titlePage = 'Sales Report Page';

    public $date;

    /**
     * Mounts the function and sets the date based on the request or current date.
     */
    public function mount(): void
    {
        // Check if the 'date' parameter is present in the request
        // If not, set the date to the current date
        $this->date = request('date') ?? date('Y-m-d');
    }

    /**
     * Downloads the sales invoice report by row.
     *
     * @param  mixed  $salesInvoice  The sales invoice data.
     */
    public function downloadSalesInvoiceReportByRow(mixed $salesInvoice): void
    {
        // Redirect to the generate_excel endpoint with the salesInvoice parameter encoded as JSON
        redirect('generate_excel?salesInvoice='.json_encode($salesInvoice));
    }

    /**
     * Download the sales invoice report.
     *
     * @param  array  $salesInvoices  The sales invoices data.
     */
    public function downloadSalesInvoiceReport(array $salesInvoices): void
    {
        // Generate the URL for generating the Excel file
        $url = 'generate_excel?salesInvoices='.json_encode($salesInvoices).'&date='.$this->date;

        // Redirect the user to the generated Excel file
        redirect($url);
    }

    /**
     * Render the sales report table.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Retrieve the sales invoices for the given date
        $salesInvoices = SalesInvoice::whereDate('sales_invoice_date', $this->date)->get();

        return view('livewire.sales-report-table', [
            'sales_invoices' => $salesInvoices,
        ]);
    }
}
