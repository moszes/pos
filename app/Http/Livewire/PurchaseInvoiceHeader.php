<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PurchaseInvoiceHeader extends Component
{
    public $purchaseInvoiceNumber;

    public $date;

    public $discount = 0;

    public $tax = 0;

    public $others = 0;

    public $note;

    /**
     * Render the purchase invoice header.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the rendered view
        return view('livewire.purchase-invoice-header');
    }
}
