<?php

namespace App\Http\Livewire;

use App\Models\Category;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Spatie\SimpleExcel\SimpleExcelReader;

class CategoryTable extends Component
{
    use WithFileUploads, WithPagination;

    public $titlePage = 'Category';

    public $search;

    public $importCategory;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshCategory' => '$refresh',
    ];

    /**
     * Resets the page number when the search input is updated.
     */
    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    public function updatedImportCategory(): void
    {
        $this->validate([
            'importCategory' => 'required|mimes:xlsx,csv',
        ]);

        $filePath = $this->importCategory->getRealPath();

        SimpleExcelReader::create($filePath)->getRows()
            ->each(function (array $rowProperties) {
                if (Category::where('name', $rowProperties['name'])->exists()) {
                    return;
                }
                $category = Category::firstOrNew([
                    'company_id' => auth()->user()->details->company_id,
                    'branch_id' => auth()->user()->details->branch_id,
                    'name' => $rowProperties['name'],
                    'type' => 'product',
                ]);

                $category->code = generateCategoryCode();
                $category->save();
            });

    }

    /**
     * Define the View for pagination.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Perform search filtering on a Category model based on the user's company ID, search keyword, and pagination.
     */
    public function searchFilter(): array|LengthAwarePaginator
    {
        return Category::where('company_id', auth()->user()->details->company_id)
            ->where(function ($query) {
                $query->where('name', 'like', '%'.$this->search.'%')
                    ->orWhere('code', 'like', '%'.$this->search.'%')
                    ->orWhere('type', 'like', '%'.$this->search.'%');
            })
            ->orderBy('id', 'desc')
            ->paginate(10);
    }

    /**
     * Render the Component view with Category data.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.category-table', [
            'categories' => self::searchFilter(),
        ]);
    }
}
