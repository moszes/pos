<?php

namespace App\Http\Livewire;

use App\Models\Branch;
use App\Models\Company;
use App\Models\SalesInvoice;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class DashboardPage extends Component
{
    public $user;

    public $userDetail;

    public $monthlySalesData;

    public $selectedMonth;

    public $company;

    public $companyCode;

    public $companyName;

    public $branch;

    public $branchCode;

    public $branchName;

    public $widget = false;

    protected $queryString = [
        'selectedMonth',
        'companyCode',
        'branchCode',
    ];

    protected $listeners = [
        'setSelectedMonth',
        'updateBranch',
        'refresh' => '$refresh',
    ];

    /**
     * Mount the component
     */
    public function mount(): void
    {
        $this->user = session('user');
        $this->userDetail = $this->user->details;

        $this->companyCode = session('company_code');

        $this->company = Company::where('code', $this->companyCode)->first();
        $this->userDetail->company_code ??= $this->company->code;
        $this->userDetail->save();

        $this->branchCode = session('branch_code');
        $this->branch = Branch::where('code', $this->branchCode)->first();
        $this->userDetail->branch_code ??= $this->branch->code;
        $this->userDetail->save();

        $this->selectedMonth = date('Y-m');

        $this->companyName = $this->company->name;
        $this->branchName = $this->branch->name;
    }

    /**
     * Selects a branch for the user.
     */
    public function updateBranch($branchCode): void
    {
        $this->branchCode = $branchCode;
        $this->branch = Branch::where('code', $branchCode)->first();
        $this->branchName = $this->branch->name;

        session()->put('company_code', $this->companyCode);
        session()->put('branch_code', $this->branchCode);
    }

    /**
     * Set the selected month.
     *
     * @param  string  $value  The value of the selected month.
     */
    public function setSelectedMonth(string $value): void
    {
        $this->selectedMonth = $value;
    }

    /**
     * Render the dashboard page.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.dashboard-page', data: [
            'todaySales' => SalesInvoice::where('sales_invoice_status', 'valid')
                ->whereDate('sales_invoice_date', date('Y-m-d'))
                ->sum('grand_total'),
        ]);
    }
}
