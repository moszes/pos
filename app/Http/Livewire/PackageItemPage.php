<?php

namespace App\Http\Livewire;

use App\Models\Package;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\View\View;
use Livewire\Component;

class PackageItemPage extends Component
{
    public $packageId;

    public $package;

    public $moduleLabel = 'Item Details';

    public $moduleName;

    public $redirectRoute;

    public $priceLabel = true;

    /**
     * Mount the package ID from the request segment
     */
    public function mount(): void
    {
        // Set the package ID from the last segment of the request URL
        $this->packageId = request()->segment(count(request()->segments()));

        // Find the package by ID
        $this->package = Package::find($this->packageId);

        $this->redirectRoute = route('master.packages', [
            'search' => $this->package->code,
        ]);

        if (! $this->package) {
            redirect($this->redirectRoute);
        }
    }

    /**
     * Render the package item page view.
     *
     * This function returns the rendered view for the package item page.
     *
     * @return View|Factory|Application The rendered view.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.package-item-page', [
            'package' => Package::find($this->packageId),
        ]);
    }
}
