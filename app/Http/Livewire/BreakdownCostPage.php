<?php

namespace App\Http\Livewire;

use Livewire\Component;

class BreakdownCostPage extends Component
{
    public function render()
    {
        return view('livewire.breakdown-cost-page');
    }
}
