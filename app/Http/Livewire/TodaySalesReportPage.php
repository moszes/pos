<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class TodaySalesReportPage extends Component
{
    public $titlePage = 'Today Sales Report';

    /**
     * Render the view for the Today Sales Page component.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.today-sales-report-page');
    }
}
