<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoiceItem;
use App\Models\Treatment;
use DB;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Livewire\Component;

class TreatmentForm extends Component
{
    public $formTitle = 'Treatment Form';

    public $actionForm = 'store';

    public $categoryId;

    public $subCategoryId;

    public $type = 'treatment';

    public $code;

    public $name;

    public $price = 0;

    public $subscriptionPlan;

    public $treatment;

    protected $listeners = [
        'setCategory',
        'subCategorySelected',
        'resetSubCategoryId',
        'clearTreatmentForm',
        'editTreatment',
        'setSubscriptionPlan',
        'destroy',
    ];

    protected $rules = [
        'code' => 'required|unique:treatments',
        'name' => 'required',
    ];

    /**
     * Update the price property with the provided value.
     *
     * @param  mixed  $value  The new value for the price property.
     */
    public function updatedPrice(mixed $value): void
    {
        // Convert the input value to a float and assign it to the price property.
        $this->price = (float) $value;
    }

    /**
     * Set the subscription plan option code.
     */
    public function setSubscriptionPlan($code): void
    {
        $this->subscriptionPlan = $code;
    }

    /**
     * Sets the selected category ID.
     */
    public function setCategory(mixed $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * Sets the selected subCategory ID.
     *
     * @param  mixed  $subCategoryId  The ID of the selected category.
     */
    public function subCategorySelected(mixed $subCategoryId): void
    {
        $this->subCategoryId = $subCategoryId;
    }

    /**
     * Reset the subCategoryId property.
     */
    public function resetSubCategoryId(): void
    {
        $this->reset('subCategoryId');
    }

    /**
     * Generate an array of treatment data.
     *
     * @return array The generated treatment data.
     */
    public function treatmentData(): array
    {
        // Get the ID of the first company
        $companyId = auth()->user()->details->branch_id;

        // Get the ID of the first branch
        $branchId = auth()->user()->details->branch_id;

        // Generate the treatment data array
        return [
            'company_id' => $companyId,
            'branch_id' => $branchId,
            'category_id' => $this->categoryId === '-- Choose --' ? null : $this->categoryId,
            'sub_category_id' => $this->subCategoryId === '-- Choose --' ? null : $this->subCategoryId,
            'code' => $this->code,
            'name' => $this->name,
            'price' => $this->price ?? 0,
            'subscription_plan' => $this->subscriptionPlan,
        ];
    }

    /**
     * Store a new treatment record in the database.
     */
    public function store(): void
    {
        // Validate the data before proceeding
        $this->validate();

        // Start a database transaction
        DB::transaction(function () {
            // Create a new treatment record with the provided data
            $this->treatment = Treatment::create($this->treatmentData());

            generateCommissionFee($this->treatment, 'treatment');

        }, 5);

        // Emit an event to clear the treatment form
        $this->emit('clearTreatmentForm');
    }

    /**
     * Edit a treatment.
     *
     * @param  int  $id  The ID of the treatment to edit.
     */
    public function editTreatment(int $id): void
    {
        // Show the treatment form
        $this->emit('showTreatmentForm');

        // Set the category type to 'treatment'
        $this->emit('setCategoryType', 'treatment');

        // Find the treatment by ID
        $this->treatment = Treatment::find($id);

        // Set the category ID and subcategory ID
        $this->categoryId = $this->treatment->category_id;
        $this->subCategoryId = $this->treatment->sub_category_id;

        $this->subscriptionPlan = $this->treatment->subscription_plan;

        // Set the category ID and subcategory ID in the event
        $this->emit('setCategoryId', $this->categoryId);
        $this->emit('setSubCategoryId', $this->subCategoryId);

        // Set the code, name, and price of the treatment
        $this->code = $this->treatment->code;
        $this->name = $this->treatment->name;
        $this->price = countFormat($this->treatment->price);

        // Set the action form to 'update'
        $this->actionForm = 'update';
    }

    /**
     * Update the treatment data in a transaction and emit a refresh event.
     */
    public function update(): void
    {
        // Update the treatment data in a transaction
        DB::transaction(function () {
            $this->treatment->update($this->treatmentData());
        }, 5);

        // Clear the treatment form after updating
        $this->emit('clearTreatmentForm');

        // Emit an event to refresh the treatment
        $this->emit('refreshTreatment');
    }

    /**
     * Delete a treatment and its related fees and material usages.
     *
     * @param  int  $id  The ID of the treatment to be deleted
     */
    public function destroy(int $id): void
    {
        // Find the treatment by ID
        $this->treatment = Treatment::find($id);

        // check if treatment has been sold
        $salesInvoiceItemContainTreatment = SalesInvoiceItem::where('module', 'treatment')
            ->where('module_id', $this->treatment->id)
            ->exists();
        if ($salesInvoiceItemContainTreatment) {
            $this->emit('treatmentHasBeenSold', 'Treatment cannot be deleted because it has been sold.');

            return;
        }

        // Use a database transaction to ensure data consistency
        DB::transaction(function () {

            $this->treatment->code = $this->treatment->code.'-deleted';
            $this->treatment->save();

            // Delete the treatment
            $this->treatment->delete();
            // Delete the related fees
            $this->treatment->normalFees()->delete();
            $this->treatment->discountFees()->delete();
            $this->treatment->targetFees()->delete();
            // Delete the related material usages
            $this->treatment->material_usages()->delete();
        }, 5);

        // Emit an event to clear the treatment form
        $this->emit('clearTreatmentForm');
    }

    /**
     * Clears the treatment form.
     */
    public function clearTreatmentForm(): void
    {
        // Reset the component state
        $this->reset();
        $this->resetErrorBag();

        // Emit events to clear category, subcategory, and treatment fee options
        $this->emit('clearCategoryOption');
        $this->emit('clearSubCategoryOption');

        // Emit events to hide the treatment form and refresh the treatment
        $this->emit('hideTreatmentForm');
        $this->emit('refreshTreatment');
    }

    /**
     * Render the view for the treatment form.
     *
     * @return View|Application|Factory The rendered view.
     */
    public function render(): View|Application|Factory
    {
        // Return the rendered view for the treatment form.
        return view('livewire.treatment-form');
    }
}
