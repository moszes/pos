<?php

namespace App\Http\Livewire;

use App\Http\Controllers\CustomerController;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithFileUploads;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Storage;

class CustomerAvatarFileUpload extends Component
{
    use WithFileUploads;

    public $user;

    public $customerAvatar;

    public $customerAvatarImage;

    /**
     * Mount the component.
     */
    public function mount(): void
    {
        if ($this->user) {
            $this->customerAvatar = CustomerController::customerAvatarImage($this->user);
        }
    }

    /**
     * Update the cover image.
     *
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function updatedCustomerAvatarImage(): void
    {
        $this->validate([
            'customerAvatarImage' => 'image', // Validate that the upload is an image
        ]);

        DB::transaction(function () {
            if ($this->customerAvatarImage && $this->customerAvatarImage->isValid()) {
                $imageUrl = $this->customerAvatarImage->store('photos', 'gcs');

                $media = $this->user->addMediaFromUrl(Storage::url($imageUrl))->toMediaCollection('users');

                $this->user->details->avatar_file_name = $media->getUrl('thumb-cropped');
                $this->user->details->save();

                CustomerController::customerAvatarImage($this->user);

                $this->customerAvatar = $this->user->details->avatar_file_name;
                $this->emit('refreshCustomerAvatarImage');
            }
        }, 5);
    }

    /**
     * Render the component.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.customer-avatar-file-upload');
    }
}
