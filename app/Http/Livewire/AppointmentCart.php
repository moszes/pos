<?php

namespace App\Http\Livewire;

use App\Models\Appointment;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class AppointmentCart extends Component
{
    public $items = [];

    public $grandTotal = 0;

    public $customer;

    public $formAction;

    protected $listeners = [
        'itemAdded',
        'clearAppointmentItems',
        'setCustomer',
    ];

    public $appointment;

    /**
     * Set the selected customer.
     *
     * @param  mixed  $customer  The customer to be set.
     */
    public function setCustomer(mixed $customer): void
    {
        // Assign the $customer parameter to the class property $this->customer
        $this->customer = $customer;
    }

    /**
     * Removes an item from the class's items array.
     *
     * @param  string  $key  The key of the item to remove.
     */
    public function removeItem(string $key): void
    {
        // Remove the item from the items array using the provided key
        unset($this->items[$key]);
    }

    /**
     * Add an item to the list of items.
     *
     * This function checks if the item has any null values for the PIC fields
     * (doctor_id, doctor_assistance_id, therapist_id).
     * If any of these fields are null, it emits an event to indicate that a PIC is required.
     * Otherwise, it adds the item to the items array.
     * After adding the item, it emits events to clear the redemption, staff, and room search fields.
     *
     * @param  mixed  $item  The item to be added.
     */
    public function itemAdded(mixed $item): void
    {
        // Check if any of the PIC fields (doctor_id, doctor_assistance_id, therapist_id) are null
        if (is_null($item['doctor_id']) && is_null($item['therapist_id'])) {
            // Emit an event to indicate that a PIC is required
            $this->emit('picRequired');

            return;
        }

        // Add the item to the items array
        $this->items[] = $item;

        // Emit events to clear the redemption, staff, and room search fields
        $this->emit('clearRedemptionSearch');
        $this->emit('clearEmployeeSearch');
        $this->emit('clearRoomSearch');
    }

    /**
     * Creates a new appointment.
     *
     * This function creates a new appointment by performing the following steps:
     * 1. Check if the cart is empty.
     * If it is, it emits an event indicating that there are no items in the cart.
     * 2. Start a database transaction.
     * 3. Create a new appointment record using the appointment data.
     * 4. Create multiple appointment items records using the items data.
     * 5. Redirects the user to the transaction appointments page.
     */
    public function createAppointment(): void
    {
        // Check if the cart is empty
        if (count($this->items) == 0) {
            // Emit an event indicating that there are no items in the cart
            $this->emit('zeroItemsOnCart');

            return;
        }

        // Start a database transaction
        DB::transaction(function () {
            // Create the appointment using the appointment data
            $this->appointment = Appointment::create($this->appointmentData());

            // Create multiple appointment items records using the items data
            $this->appointment->items()->createMany($this->items);

            // Redirect the user to the transaction appointments page
            $this->redirect(route('transaction.appointments'));
        }, 5);
    }

    /**
     * Update the appointment cart data.
     *
     * This function updates the appointment cart data by performing the following steps:
     * 1. Check if there are items in the cart.
     * If not, emits an event indicating zero items.
     * 2. Retrieves the appointment by ID.
     * 3. Start a database transaction to update appointment data,
     * delete existing items, create new items, and redirect the user.
     */
    public function update(): void
    {
        // Check if there are items in the cart
        if (count($this->items) == 0) {
            // Emit an event indicating zero items in the cart
            $this->emit('zeroItemsOnCart');

            return;
        }

        // Find the appointment by ID
        $this->appointment = Appointment::find($this->appointment['id']);

        // Start a database transaction
        DB::transaction(function () {
            // Update the appointment data
            $this->appointment->update($this->appointmentData());

            // Delete existing appointment items
            $this->appointment->items()->delete();

            // Create new appointment items
            $this->appointment->items()->createMany($this->items);

            // Redirect the user to the transaction appointments page
            $this->redirect(route('transaction.appointments'));
        }, 5);
    }

    /**
     * Returns the appointment data.
     *
     * This function retrieves the necessary data to create a new appointment.
     *
     * @return array The appointment data including company ID, branch ID, customer ID, appointment number, and date.
     */
    private function appointmentData(): array
    {
        // Retrieve the company ID using the getFirstCompany function
        $company_id = auth()->user()->details->branch_id;

        // Retrieve the branch ID using the getBranchId function
        $branch_id = auth()->user()->details->branch_id;

        // Retrieve the customer ID from the current customer object
        $customer_id = $this->customer['users']['id'];

        // Generate a unique appointment number
        $appointment_number = $this->appointment ? $this->appointment['appointment_number'] : generateAppointmentNumber();

        // Get the current date and time for the appointment
        $appointment_date = now();

        // Return the appointment data as an array
        return [
            'company_id' => $company_id,
            'branch_id' => $branch_id,
            'customer_id' => $customer_id,
            'appointment_number' => $appointment_number,
            'appointment_date' => $appointment_date,
        ];
    }

    /**
     * Clears all items in the class.
     */
    public function clearAppointmentItems(): void
    {
        // Set the items array to an empty array.
        $this->items = [];

        // Emit an event to indicate that the cart has been closed.
        $this->emit('cartClose');
    }

    /**
     * Render the appointment cart view.
     *
     * This function renders the appointment cart view by fetching the code and name for each item.
     */
    public function render(): Factory|View|Application
    {
        // Iterate through each item to fetch code and name
        foreach ($this->items as $key => $item) {
            // Retrieve code and name from the data module
            $data = getDataByModule($item['module'], $item['module_id']);
            $this->items[$key]['code'] = $data->code;
            $this->items[$key]['name'] = $data->name;
        }

        // Return the appointment cart view with appointment and items data
        return view('livewire.appointment-cart', [
            'appointment' => $this->appointment,
            'items' => $this->items,
        ]);
    }
}
