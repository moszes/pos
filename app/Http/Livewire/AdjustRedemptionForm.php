<?php

/** @noinspection PhpClassNamingConventionInspection */

namespace App\Http\Livewire;

use App\Models\AdjustRedemption;
use App\Models\Treatment;
use App\Models\UserDetail;
use App\Models\UserTransaction;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class AdjustRedemptionForm extends Component
{
    public $formTitle = 'Form Adjust Redemption';

    public $actionForm = 'store';

    public $customerId;

    public $phone;

    public $treatmentId;

    public $userTransaction;

    public $userTransactionId;

    public $adjustRedemption;

    public $treatmentCode;

    public $qty = -1;

    public $note = 'adjustment';

    protected $listeners = [
        'clearAdjustRedemptionForm',
        'editAdjustRedemption',
        'destroy',
    ];

    protected $rules = [
        'phone' => 'required',
        'treatmentCode' => 'required',
    ];

    /**
     * Generate the data array for adjusting redemption.
     */
    public function adjustRedemptionData(): array
    {
        // Calculate the quantity value, default to 0 if empty
        $qty = floatval($this->qty == '' ? 0 : $this->qty);

        return [
            'company_id' => auth()->user()->details->branch_id,
            'user_id' => auth()->user()?->id,
            'customer_id' => $this->customerId,
            'user_transaction_id' => $this->userTransactionId,
            'transaction_status' => 'redemption',
            'treatment_id' => $this->treatmentId,
            'qty' => $qty,
            'note' => $this->note,
        ];
    }

    /**
     * Returns an array containing the data for a user transaction.
     *
     * @return array The user transaction data.
     */
    public function userTransactionData(): array
    {
        // Get the ID of the first company
        $companyId = auth()->user()->details->branch_id;

        // Get the ID of the current branch
        $branchId = auth()->user()->details->branch_id;

        // Return the user transaction data
        return [
            'company_id' => $companyId, // Set the company ID
            'branch_id' => $branchId, // Set the branch ID
            'user_id' => $this->customerId, // Set the user ID
            'module' => 'treatment', // Set the module name
            'module_id' => $this->treatmentId, // Set the module ID
            'reference_module' => 'seeder', // Set the reference module name
            'qty' => $this->qty, // Set the quantity
            'note' => $this->note, // Set the note
        ];
    }

    /**
     * Store method to handle the creation of a new user transaction.
     */
    public function store(): void
    {
        // Validate the input data
        $this->validate();

        // Find the customer based on the provided phone number
        $this->customerId = UserDetail::where('phone', $this->phone)->first();

        // If customer not found, set the phone to null and add an error
        if (! $this->customerId) {
            $this->phone = null;
            $this->addError('phone', 'Pick Customer Phone');

            return;
        }

        // Set the customer ID
        $this->customerId = $this->customerId->user_id;

        // Find the treatment based on the provided code
        $this->treatmentId = Treatment::where('code', $this->treatmentCode)->first();

        // If treatment not found, set the treatment code to null and add an error
        if (! $this->treatmentId) {
            $this->treatmentCode = null;
            $this->addError('treatmentCode', 'Treatment Not Found...');

            return;
        }

        // Set the treatment ID
        $this->treatmentId = $this->treatmentId->id;

        // Create a new user transaction and retrieve the ID
        $this->userTransactionId = UserTransaction::create($this->userTransactionData())->id;

        // Create a new adjusted redemption entry
        AdjustRedemption::create($this->adjustRedemptionData());

        // Clear the form fields
        $this->clearAdjustRedemptionForm();

        // Emit events for data saved and refresh
        $this->emit('dataSaved');
        $this->emit('refreshAdjustRedemption');
    }

    /**
     * Edit an adjustment redemption.
     *
     * @param  int  $id  The ID of the adjustment redemption to edit.
     */
    public function editAdjustRedemption(int $id): void
    {
        // Find the adjustment redemption by ID
        $this->adjustRedemption = AdjustRedemption::find($id);

        // Set the properties of the adjustment redemption to the corresponding properties of the found redemption
        $this->userTransactionId = $this->adjustRedemption->user_transaction_id;
        $this->customerId = $this->adjustRedemption->customer_id;
        $this->treatmentId = $this->adjustRedemption->treatment_id;
        $this->phone = getUserById($this->adjustRedemption->user_id)->details->phone;
        $this->treatmentCode = getTreatmentById($this->adjustRedemption->treatment_id)->code;
        $this->qty = $this->adjustRedemption->qty;
        $this->note = $this->adjustRedemption->note;

        // Set the action form to 'update' and emit the 'showAdjustRedemptionForm' event
        $this->actionForm = 'update';
        $this->emit('showAdjustRedemptionForm');
    }

    /**
     * Update method to handle updating the user transaction and adjust redemption.
     */
    public function update(): void
    {
        // Validate the input data
        $this->validate();

        // Update the adjusted redemption with new data
        $this->adjustRedemption->update($this->adjustRedemptionData());

        // Find the user transaction and update quantity and note
        $this->userTransaction = UserTransaction::find($this->userTransactionId);
        $this->userTransaction->update([
            'qty' => $this->qty,
            'note' => $this->note,
        ]);

        // Set the action form to 'save' after updating
        $this->actionForm = 'save';

        // Clear the adjusted redemption form fields
        $this->clearAdjustRedemptionForm();

        // Emit event for data being updated
        $this->emit('dataUpdated');

        // Emit event to refresh the adjusted redemption form
        $this->emit('refreshAdjustRedemption');
    }

    /**
     * Delete an adjustment redemption and its associated user transaction.
     *
     * @param  int  $id  The ID of the adjustment redemption to delete.
     */
    public function destroy(int $id): void
    {
        // Find the adjustment redemption by ID
        $this->adjustRedemption = AdjustRedemption::find($id);

        // Find the associated user transaction by ID and delete it
        UserTransaction::find($this->adjustRedemption->user_transaction_id)->delete();

        // Delete the adjustment redemption
        $this->adjustRedemption->delete();

        // Set the action form to 'save' after deleting
        $this->actionForm = 'save';

        // Clear the adjusted redemption form fields
        $this->clearAdjustRedemptionForm();

        // Emit events for data deleted and refresh
        $this->emit('dataDeleted');
        $this->emit('refreshAdjustRedemption');
    }

    /**
     * Clear the adjusting redemption form fields and reset the form state.
     */
    public function clearAdjustRedemptionForm(): void
    {
        // Emit an event to hide the adjusted redemption form
        $this->emit('hideAdjustRedemptionForm');

        // Set the action form to 'store'
        $this->actionForm = 'store';

        // Reset the form fields
        $this->reset();
    }

    /**
     * Render the adjust redemption form view.
     *
     * @return View|Factory|Application The rendered view.
     */
    public function render(): View|Factory|Application
    {
        // Return the view for the adjusted redemption form
        return view('livewire.adjust-redemption-form');
    }
}
