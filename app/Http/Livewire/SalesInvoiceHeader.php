<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoice;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Livewire\Component;

class SalesInvoiceHeader extends Component
{
    public $salesInvoice;

    protected $listeners = [
        'refreshSalesInvoiceHeader' => '$refresh',
        'setSalesInvoice',
    ];

    /**
     * Set the sales invoice based on the sales invoice number.
     *
     * @param  string  $salesInvoiceNumber  The sales invoice number to set.
     */
    public function setSalesInvoice(string $salesInvoiceNumber): void
    {
        $this->salesInvoice = SalesInvoice::where('sales_invoice_number', $salesInvoiceNumber)->first();
    }

    /**
     * Render the sales invoice header view.
     *
     * This function returns the rendered view for the sales invoice header page.
     *
     * @return View|Factory|Application The rendered view.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.sales-invoice-header');
    }
}
