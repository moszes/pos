<?php

namespace App\Http\Livewire;

use App\Models\Product;
use App\Models\Treatment;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class PackageItemTable extends Component
{
    public $package;

    public $packageItems = [];

    public $packageItemList = false;

    public $packageItemForm = false;

    public $searchPackageItem;

    public $searchData = [];

    public $items = [];

    public $item = [];

    public $itemCode;

    public $itemName;

    public $itemModule;

    public $itemNormalPrice;

    public $itemModuleId;

    public $totalPackagePrice = 0;

    public $packageItemPrice = 0;

    public $itemQty = 1;

    /**
     * Mount the package items to the component.
     */
    public function mount(): void
    {
        // Retrieve the package items as an array and assign them to the packageItems property
        $this->items = $this->package?->items->toArray();
    }

    /**
     * Update the search results based on the provided value.
     *
     * @param  string  $value  The search keyword
     */
    public function updatedSearchPackageItem(string $value): void
    {
        // Reset the searchData array
        $this->searchData = [];

        // Check if the search keyword is provided
        if ($value != '') {
            // Set packageItemList to true to display search results
            $this->packageItemList = true;

            // Search for treatments by code or name
            $treatments = Treatment::where('code', 'like', '%'.$value.'%')
                ->orWhere('name', 'like', '%'.$value.'%')
                ->take(10)
                ->get()->toArray();

            // Add module and module id to treatments array
            foreach ($treatments as $key => $treatment) {
                $treatments[$key]['module'] = 'treatment';
                $treatments[$key]['module_id'] = $treatment['id'];
            }

            // Merge treatment search results with existing search data
            $this->searchData = array_merge($this->searchData, $treatments);

            // Search for products by code or name
            $products = Product::where('code', 'like', '%'.$value.'%')
                ->orWhere('name', 'like', '%'.$value.'%')
                ->take(10)
                ->get()->toArray();

            // Add module, module id, and price to products array
            foreach ($products as $key => $product) {
                $products[$key]['module'] = 'product';
                $products[$key]['module_id'] = $product['id'];
                $products[$key]['price'] = $product['normal_price'];
            }

            // Merge product search results with existing search data
            $this->searchData = array_merge($this->searchData, $products);
        }
    }

    /**
     * Update the item quantity and calculate the total price based on the input value.
     *
     * @param  mixed  $value  The new item quantity value
     */
    public function updatedItemQty(mixed $value): void
    {
        if ($value !== '') {
            // Set the item quantity to the float value of the provided input
            $this->itemQty = floatval($value);

            // Calculate the total price by multiplying the item price with the item quantity
            $this->totalPackagePrice = floatval($this->packageItemPrice) * $value;
        } else {
            $this->itemQty = 0;
            $this->totalPackagePrice = 0;
        }

    }

    /**
     * Update the item sell price based on the provided value.
     *
     * @param  mixed  $value  The new item price value
     */
    public function updatedPackageItemPrice(mixed $value): void
    {
        // Set the item price to the float value of the provided input
        $this->packageItemPrice = floatval($value);

        // Calculate the total price by multiplying the item price with the item quantity
        $this->totalPackagePrice = $this->packageItemPrice * floatval($this->itemQty);
    }

    /**
     * Selects a package item by module and module ID
     *
     * @param  string  $module  The module name
     * @param  int  $moduleId  The ID of the module
     */
    public function selectPackageItem(string $module, int $moduleId): void
    {
        // Reset package item list
        $this->packageItemList = false;

        // Get item data by module and module ID
        $this->item = getDataByModule($module, $moduleId);

        // Set package item form flag to true
        $this->packageItemForm = true;

        $this->itemCode = $this->item->code;
        $this->itemName = $this->item->name;
        $this->packageItemPrice = countFormat($module == 'product' ? $this->item->normal_price : $this->item->price);
        $this->itemNormalPrice = countFormat($module == 'product' ? $this->item->normal_price : $this->item->price);
        $this->itemModule = $module;
        $this->itemModuleId = $moduleId;

        // Calculate item sell price based on quantity
        $this->totalPackagePrice = floatval($this->packageItemPrice) / floatval($this->itemQty);
        $this->searchPackageItem = null;
    }

    /**
     * Add a package item to the items array.
     */
    public function addPackageItem(): void
    {
        $this->items[] = [
            'module' => $this->itemModule,
            'module_id' => $this->itemModuleId,
            'code' => $this->itemCode,
            'name' => $this->itemName,
            'package_item_price' => floatval($this->packageItemPrice),
            'total_package_price' => floatval($this->totalPackagePrice),
            'qty' => $this->itemQty,
        ];

        $this->reset([
            'searchPackageItem',
            'searchData',
            'itemModule',
            'itemModuleId',
            'itemCode',
            'itemName',
            'totalPackagePrice',
            'packageItemPrice',
            'itemQty',
        ]);
    }

    /**
     * Remove an item from the items array based on the provided key.
     *
     * @param  int|string  $key  The key of the item to be removed
     */
    public function removeItem(int|string $key): void
    {
        unset($this->items[$key]);
    }

    /**
     * Save or update the package items.
     */
    public function saveOrUpdate(): void
    {
        // Delete existing package items
        $this->package->items()->delete();

        // Use a database transaction to create multiple items
        DB::transaction(function () {
            $this->package->items()->createMany($this->items);
        }, 5);

        // redirect to package page
        $this->redirect(route('master.packages', $this->package->id));
    }

    /**
     * Render the view for the Livewire component.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.package-item-table');
    }
}
