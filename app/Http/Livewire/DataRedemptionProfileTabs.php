<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class DataRedemptionProfileTabs extends Component
{
    public $user;

    public $customerId;

    protected $queryString = ['customerId'];

    /**
     * Mount the component.
     */
    public function mount(): void
    {
        $this->user = User::find($this->customerId);
    }

    /**
     * Render the component.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.data-redemption-profile-tabs');
    }
}
