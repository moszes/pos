<?php

namespace App\Http\Livewire;

use App\Models\Company;
use App\Models\Subscription;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class SubscriptionTable extends Component
{
    use WithPagination;

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshSubscription' => '$refresh',
        'destroy',
    ];

    /**
     * Get the pagination view.
     *
     * This function returns the name of the pagination view to be used.
     *
     * @return string The name of the pagination view.
     */
    public function paginationView(): string
    {
        // Return the name of the pagination view.
        return 'pagination';
    }

    /**
     * Delete a subscription by its ID.
     *
     * @param  int  $id  The ID of the subscription to delete.
     */
    public function destroy(int $id): void
    {
        // Find the subscription by its ID
        $subscription = Subscription::find($id);

        // Delete the subscription within a database transaction
        DB::transaction(function () use ($subscription) {
            $subscription->delete();
        }, 5);

        $this->emit('refreshSubscription');
    }

    /**
     * Render the view for the subscription table component.
     *
     * @return View|Factory|Application The rendered view.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.subscription-table', [
            'subscriptions' => Subscription::where('code', 'like', '%'.$this->search.'%')
                ->where('company_id', Company::first()->id)
                ->orderBy('id', 'desc')
                ->paginate(10),
        ]);
    }
}
