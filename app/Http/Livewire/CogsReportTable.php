<?php

namespace App\Http\Livewire;

use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class CogsReportTable extends Component
{
    use WithPagination;

    public $titlePage = 'Cogs';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshCategory' => '$refresh',
    ];

    public function paginationView(): string
    {
        return 'pagination';
    }

    public function render(): View
    {
        return view('livewire.cogs-report-table', [
            'cogs_prices' => collect(),
        ]);
    }
}
