<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class CustomerPage extends Component
{
    /**
     * Render the view for the customer page.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.customer-page');
    }
}
