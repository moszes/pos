<?php

namespace App\Http\Livewire;

use Livewire\Component;

class OpenHour extends Component
{
    public function render()
    {
        return view('livewire.open-hour');
    }
}
