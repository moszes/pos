<?php

namespace App\Http\Livewire;

use App\Models\SubscriptionPlan;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Livewire\Component;

class SubscriptionPlanOption extends Component
{
    public $subscriptionPlans;

    public $subscriptionPlanOption;

    /**
     * Mount the component.
     */
    public function mount(): void
    {
        $this->subscriptionPlans = SubscriptionPlan::all();
    }

    /**
     * Handle the updated subscription plan option.
     */
    public function updatedSubscriptionPlanOption($code): void
    {
        $this->emit('setSubscriptionPlan', $code);
    }

    /**
     * Render the view for the SubscriptionPlanOption component.
     */
    public function render(): View|Application|Factory
    {
        return view('livewire.subscription-plan-option');
    }
}
