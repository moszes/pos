<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class CategoryOption extends Component
{
    public $categoryId;

    public $type;

    protected $listeners = [
        'clearCategoryOption',
        'setCategoryType',
        'setCategoryId',
    ];

    /**
     * Update the category ID and trigger an event to generate subcategory options.
     *
     * @param  mixed  $id  The new category ID.
     */
    public function updatedCategoryId(mixed $id): void
    {
        $this->emit('clearSubCategoryOption');
        $this->emit('resetSubCategoryId');

        // Trigger the event to generate subcategory options
        $this->emit('generateSubCategoryOptions', $id);
        $this->emit('setCategory', $id);

    }

    public function setCategoryType($type): void
    {
        $this->type = $type;
    }

    /**
     * Set the category ID.
     *
     * @param  mixed  $id  The ID of the category to set
     */
    public function setCategoryId(mixed $id): void
    {
        if ((int) $id) {
            $this->categoryId = $id;
            $this->emit('categorySelected', $id);
            $this->emit('generateSubCategoryOptions', $id);
        }
    }

    /**
     * Clears the category option by resetting the state and error bag.
     */
    public function clearCategoryOption(): void
    {
        $this->reset();
        $this->resetErrorBag();
    }

    /**
     * Render the view for the category option.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.category-option');
    }
}
