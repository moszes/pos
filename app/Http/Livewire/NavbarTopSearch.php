<?php

namespace App\Http\Livewire;

use Livewire\Component;

class NavbarTopSearch extends Component
{
    public function render()
    {
        return view('livewire.navbar-top-search');
    }
}
