<?php

namespace App\Http\Livewire;

use App\Events\UserEvent;
use Auth;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Livewire\Component;

class LoginPage extends Component
{
    public $name;

    public $email;

    public $password;

    /**
     * @return RedirectResponse|void
     */
    /**
     * Handle the login request.
     *
     * @return RedirectResponse|void
     */
    public function login()
    {
        // Validate the request input
        $credentials = $this->validate([
            'name' => ['required'],
            'password' => ['required'],
        ]);

        // Attempt to authenticate the user
        if (Auth::attempt($credentials)) {
            // Dispatch a user event
            UserEvent::dispatch(auth()->user());

            // Regenerate the session
            session()->regenerate();

            session([
                'user' => auth()->user(),
                'company_code' => auth()->user()->details->company_code,
                'branch_code' => auth()->user()->details->branch_code,
            ]);

            // Redirect to the intended dashboard page
            return redirect()->intended('dashboard');
        }

        // Add an error message if the credentials are invalid
        $this->addError('name', 'Credential is invalid.');
    }

    /**
     * Render the login page.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Render the login page view and set the layout to 'layouts.guest'
        return view('livewire.login-page')->layout('layouts.guest');
    }
}
