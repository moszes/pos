<?php

namespace App\Http\Livewire;

use App\Models\Approval;
use App\Models\SalesInvoice;
use App\Models\SalesInvoicePayment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SalesInvoiceDetail extends Component
{
    public $formTitle = 'Sales Invoice Details';

    public $salesInvoice;

    public $subTotal = 0;

    public $grandTotal = 0;

    public $totalPayment = 0;

    public $salesInvoiceDiscount = 0;

    public $salesInvoiceTax = 0;

    public $salesInvoiceOthers = 0;

    public $note;

    public $payments = [];

    public $items = [];

    public $hasDiscount = false;

    protected $listeners = [
        'showSalesInvoiceDetails',
        'clearSalesInvoiceItems',
        'clearSalesInvoiceDetailModal',
        'setHasDiscount',
    ];

    public function setHasDiscount(): void
    {
        $this->hasDiscount = true;
    }

    /**
     * Approve the discount for a sales invoice.
     *
     * @param  int  $id  The ID of the sales invoice to approve the discount for.
     */
    public function approveDiscount(int $id): void
    {
        $this->salesInvoice = SalesInvoice::find($id);
        $this->salesInvoice->is_discount = now();
        $this->salesInvoice->save();

        Approval::where('module', 'sales_invoice')
            ->where('module_id', $id)->update(
                [
                    'approve_by' => auth()->user()?->id,
                    'approve_date' => date('Y-m-d H:i:s'),
                    'status' => 'approved',
                ]
            );

        $this->hasDiscount = false;

        $this->emit('refreshSalesInvoice');
        $this->emit('hideSalesInvoiceDetails');
    }

    /**
     * Retrieves the details of a sales invoice and calculates the total payment
     *
     * @param  int  $id  The identifier of the sales invoice
     */
    public function showSalesInvoiceDetails(int $id): void
    {
        $this->salesInvoice = SalesInvoice::find($id);
        $this->totalPayment = SalesInvoicePayment::where('sales_invoice_id', $id)->where('status', 'paid')
            ->sum('amount');

        // map sales invoice item and a set has discount active if there is any discount
        if ($this->salesInvoice->discount > 0 && $this->salesInvoice->is_discount === null) {
            $this->hasDiscount = true;
        }

        $this->items = $this->salesInvoice->items;
        $this->emit('setItems', $this->items);

        // map sales invoice item and a set has discount active if there is any discount
        foreach ($this->items as $item) {
            if ($item->discount_item > 0 && $this->salesInvoice->is_discount === null) {
                $this->hasDiscount = true;
            }
        }

        $this->payments = $this->salesInvoice->payments;
        $this->emit('setPayments', $this->payments);

        $this->salesInvoiceDiscount = $this->salesInvoice->discount;
        $this->emit('setSalesInvoiceDiscount', $this->salesInvoiceDiscount);

        $this->salesInvoiceTax = $this->salesInvoice->tax;
        $this->emit('setSalesInvoiceTax', $this->salesInvoiceTax);

        $this->salesInvoiceOthers = $this->salesInvoice->others;
        $this->emit('setSalesInvoiceOthers', $this->salesInvoiceOthers);

        $this->note = $this->salesInvoice->note;
        $this->emit('setNote', $this->note);

        $this->subTotal = collect($this->items)->sum('total_price');
        $this->emit('setSubTotal', $this->subTotal);

        $this->grandTotal = $this->salesInvoice->grand_total;
        $this->emit('setGrandTotal', $this->grandTotal);

        $this->emit('setShowDetails', false);
    }

    /**
     * Clears the sales invoice items.
     *
     * This method clears the sales invoice items by setting the `salesInvoice`
     * property to null.
     */
    public function clearSalesInvoiceItems(): void
    {
        $this->salesInvoice = null;
    }

    public function clearSalesInvoiceDetailModal(): void
    {
        $this->reset();
    }

    /**
     * Render the sales invoice item modal view.
     *
     * This method returns the rendered view of the sales invoice item modal.
     * The view is generated using the 'livewire.sales-invoice-item-modal' blade template file.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.sales-invoice-detail');
    }
}
