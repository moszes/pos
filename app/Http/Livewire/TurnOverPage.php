<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class TurnOverPage extends Component
{
    public function render(): View|Factory|Application
    {
        return view('livewire.turn-over-page');
    }
}
