<?php

namespace App\Http\Livewire;

use App\Http\Controllers\UserTransactionApiController;
use App\Models\ApplicationSetting;
use App\Models\Appointment;
use App\Models\Approval;
use App\Models\CashDrawer;
use App\Models\SalesInvoice;
use App\Models\SalesInvoiceItem;
use App\Models\SalesInvoicePayment;
use App\Models\User;
use App\Models\UserTransaction;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Collection;
use Livewire\Component;

/**
 * The form title.
 *
 * @var string
 */
class SalesInvoiceForm extends Component
{
    public string $formTitle = 'Form Sales Invoice';

    public string $actionForm = 'store';

    public $customer = [];

    public $appointment;

    public $salesInvoice;

    public $salesInvoiceNumber;

    public $salesInvoiceDate;

    public $module = 'product';

    public $noteWidget = false;

    public $treatment = [];

    public $marketplace = [];

    public $package = [];

    public $product = [];

    public $room = [];

    public $personInCharge = [];

    public $items = [];

    public $item = [];

    public $salesInvoiceTax = 0;

    public $salesInvoiceOthers = 0;

    public $salesInvoiceDiscount = 0;

    public $subTotal = 0;

    public $grandTotal = 0;

    public $payments = [];

    public $payment;

    public $totalPayment = 0;

    public $diffPayment = 0;

    public $note;

    public $salesInvoiceStatus = 'invalid';

    public $hasDiscount = false;

    public $treatmentModule;

    public $packageModule;

    public $marketPlaceModule;

    public $subscriptionModule;

    protected $listeners = [
        'setCustomer',
        'resetCustomer',
        'setModule',
        'marketPlaceSelected',
        'treatmentSelected',
        'packageSelected',
        'productSelected',
        'roomSelected',
        'setPersonInCharge',
        'countGrandTotal',
        'setPayments',
        'editSalesInvoice',
        'releaseSalesInvoice',
        'redirectToPrint',
        'appointmentSelected',
        'applyDiscount',
        'clearSalesInvoiceForm',
        'clearSalesInvoiceItems',
        'clearSalesInvoiceProperties',
        'clearSalesInvoiceProperties',
        'isSalesSet',
        'setItems',
        'clearItem',
    ];

    /**
     * Mount the sales invoice by setting the sales invoice number
     * and the current date as the sales invoice date.
     */
    public function mount(): void
    {
        // Generate the sales invoice number
        $this->salesInvoiceNumber = generateSalesInvoiceNumber();

        // Set the sales invoice date as the current date
        $this->salesInvoiceDate = date('Y-m-d');

        $this->treatmentModule = ApplicationSetting::where('name', 'Application Module')
            ->where('module', 'treatment')->first()?->status == 'Y';
        $this->packageModule = ApplicationSetting::where('name', 'Application Module')
            ->where('module', 'package')->first()?->status == 'Y';
        $this->marketPlaceModule = ApplicationSetting::where('name', 'Application Module')
            ->where('module', 'marketplace')->first()?->status == 'Y';
        $this->subscriptionModule = ApplicationSetting::where('name', 'Application Module')
            ->where('module', 'subscription')->first()?->status == 'Y';

    }

    /**
     * Reset the error bag.
     */
    public function updated(): void
    {
        // Reset the error bag by calling the `resetErrorBag` method.
        $this->resetErrorBag();
    }

    /**
     * Set the selected customer.
     *
     * @param  mixed  $customer  The customer to be set.
     */
    public function setCustomer(mixed $customer): void
    {
        // Assign the $customer parameter to the class property $this->customer
        $this->customer = $customer;
    }

    /**
     * Resets the customer property to an empty array.
     */
    public function resetCustomer(): void
    {
        // Reset the customer property to an empty array
        $this->customer = [];
    }

    /**
     * Set the module for the current context.
     *
     * @param  mixed  $module  The module to set.
     */
    public function setModule(mixed $module): void
    {
        // Set the module property to the provided module
        $this->module = $module;
    }

    /**
     * Apply a discount to the current order.
     */
    public function applyDiscount(): void
    {
        $this->hasDiscount = true;
    }

    /**
     * Handle the selection of an appointment.
     *
     * @param  mixed  $appointmentId  The ID of the appointment
     */
    public function appointmentSelected(mixed $appointmentId): void
    {
        // Find the appointment by ID
        $this->appointment = Appointment::find($appointmentId);

        // Check if the appointment does not exist or if it has already been used
        $usedAppointment = UserTransaction::where('appointment_id', $appointmentId)->first();

        if (! $this->appointment || $usedAppointment) {
            // Redirect to the appointment menu
            $this->emit('redirectToAppointmentMenu');

            return;
        }

        // Emit event to select the customer associated with the appointment
        $this->emit('selectCustomer', $this->appointment->customer_id);

        // Transform the appointment items and emit event to set the items
        $this->items = $this->mapAppointmentItems(items: $this->appointment->items)->toArray();
        $this->emit('setItems', $this->items);
    }

    /**
     * Updates the sales invoice "others" data.
     *
     * This method is responsible for updating the sales invoice "others" data.
     * It performs the following actions:
     * - Checks if the value is numeric.
     *   If not, adds an error message and emits a "shouldNumericValue" event.
     * - Emits a "setSalesInvoiceOthers" event with the updated salesInvoiceOthers data as input.
     * - Calls the countGrandTotal() method.
     * - Emits a "setGrandTotal" event with the updated grandTotal data as input.
     *
     * @param  mixed  $value  The new value for the sales invoice "others" data.
     */
    public function updatedSalesInvoiceOthers(mixed $value): void
    {
        // Check if the value is numeric
        if (! is_numeric($value)) {
            // Add an error message if the value is not numeric
            $this->addError('salesInvoiceOthers', 'Sales Invoice Others must be numeric');

            // Emit a "shouldNumericValue" event
            $this->emit('shouldNumericValue');

            // Return early if the value is not numeric
            return;
        }

        // Emit a "setSalesInvoiceOthers" event with the updated salesInvoiceOthers data
        $this->emit('setSalesInvoiceOthers', $this->salesInvoiceOthers);

        // Call the countGrandTotal() method
        $this->countGrandTotal();

        // Emit a "setGrandTotal" event with the updated grandTotal data
        $this->emit('setGrandTotal', $this->grandTotal);
    }

    /**
     * Sets the sales invoice discount.
     *
     * This method updates the sales invoice discount and performs the following actions:
     *  - Emits a "setSalesInvoiceDiscount" event with the salesInvoiceDiscount as input.
     *
     * @param  mixed  $value  The value of the sales invoice discount.
     */
    public function updatedSalesInvoiceDiscount(mixed $value): void
    {
        // Check if the value is numeric
        if (! is_numeric($value)) {
            $this->addError('salesInvoiceDiscount', 'Sales Invoice Discount must be numeric');
            $this->emit('shouldNumericValue');

            $this->salesInvoiceDiscount = 0;

            return;
        }

        if ($this->salesInvoiceDiscount > 0) {
            // Emit an event to inform the frontend to apply the discount
            $this->emit('applyDiscount');
        }

        $this->salesInvoiceDiscount = floatval($value);

        // Update the sales invoice discount
        if ($this->salesInvoice) {
            $this->salesInvoice->is_discount = null;
            $this->salesInvoice->approval->status = 'pending';
            $this->salesInvoice->save();
        }

        // Emit events for the updated discount and grand total
        $this->emit('setSalesInvoiceDiscount', $this->salesInvoiceDiscount);
        $this->countGrandTotal();
        $this->emit('setGrandTotal', $this->grandTotal);
    }

    /**
     * Sets the payments for the sales invoice.
     *
     * This method sets the payments for the sales invoice using the provided array of payments.
     *
     * @param  array  $payments  The array of payments to be set.
     */
    public function setPayments(array $payments): void
    {
        $this->payments = $payments;
        $this->totalPayment = collect($this->payments)->sum('amount');
    }

    /**
     * Map the appointment items to a new format.
     *
     * This method transforms the given items into a new format.
     *
     * @param  Collection  $items  The collection of items to be transformed
     * @return Collection The transformed appointment items
     */
    protected function mapAppointmentItems(Collection $items): Collection
    {
        return $items->map(function ($item) {
            $appointmentItem = getDataByModule($item->module, $item->module_id);

            return [
                'module' => $item->module,
                'module_id' => $item->module_id,
                'code' => $appointmentItem->code,
                'name' => $appointmentItem->name,
                'doctor_id' => $item->doctor_id,
                'doctor_assistance_id' => $item->doctor_assistance_id,
                'therapist_id' => $item->therapist_id,
                'sales_id' => $item->sales_id,
                'room_id' => $item->room_id,
                'room_start' => $item->room_start,
                'room_end' => $item->room_end,
                'qty' => $item->qty,
                'discount_item' => $item->discount_item,
                'total_price' => $item->total_price,
                'transaction_status' => $item->transaction_status,
            ];
        });
    }

    /**
     * Updates the treatment and module properties.
     *
     * @param  mixed  $treatment  The new treatment value.
     */
    public function treatmentSelected(mixed $treatment): void
    {
        $this->assignValues($treatment, 'treatment', 'treatment');
    }

    /**
     * Set the selected marketplace.
     *
     * @param  mixed  $marketPlace  The selected marketplace.
     */
    public function marketPlaceSelected(mixed $marketPlace): void
    {
        $this->assignValues($marketPlace, 'marketplace', 'marketplace');
    }

    /**
     * Assigns the selected room to the current booking.
     *
     * @param  mixed  $room  The selected room to be assigned.
     */
    public function roomSelected(mixed $room): void
    {
        $this->room = $room;

        // array merge to item
        $this->item = array_merge($this->item, $room);
    }

    /**
     * Update the selected package and set the module and item property.
     *
     * @param  mixed  $package  The selected package.
     */
    public function packageSelected(mixed $package): void
    {
        $this->assignValues($package, 'package', 'package');
    }

    /**
     * Assigns the given value to the specified property, module, and item.
     *
     * @param  mixed  $value  The value to assign.
     * @param  string  $property  The name of the class property to assign.
     * @param  string  $moduleName  The name of the module.
     */
    private function assignValues(mixed $value, string $property, string $moduleName): void
    {
        $this->{$property} = $value;
        $this->module = $moduleName;
        $this->item = $value;
    }

    /**
     * This method is called when a product is selected.
     *
     * @param  mixed  $product  selected product object.
     */
    public function productSelected(mixed $product): void
    {
        $this->assignValues($product, 'product', 'product');
    }

    /**
     * Sets the person in charge of a specific task.
     *
     * @param  mixed  $personInCharge  The person in charge of the task.
     */
    public function setPersonInCharge(mixed $personInCharge): void
    {
        $this->personInCharge = $personInCharge;

        $this->item = array_merge($this->item, $this->personInCharge);
    }

    /**
     *  Set items array.
     */
    public function setItems($items): void
    {
        $this->items = $items;
    }

    /**
     * Adds an item to the list of items.
     *
     * This method performs the following steps:
     * 1. Checks if the customer, treatment, room, and person in charge have been set.
     * 2. Merges the person in charge into the item.
     * 3. Add the item to the list of items.
     * 4. Emits the changes to update the UI.
     */
    public function addItem(): void
    {
        if (! $this->validConditions()) {
            return;
        }

        if ($this->validateItem()) {
            return;
        }

        $this->item['sales_fee'] = getProductFee($this->item['module_id'], 'sales');
        $this->item['sell_price'] = countFormat($this->item['module'] == 'product' ? $this->item['normal_price'] : $this->item['price']);
        $this->item['total_sell_price'] = $this->item['qty'] * $this->item['sell_price'] - $this->item['discount_item'];
        $this->item['total_price'] = $this->item['qty'] * $this->item['price'] - $this->item['discount_item'];

        $this->items[] = $this->item;
        $this->emitChanges();
    }

    /**
     * Check if all required conditions for creating a valid sales invoice are met.
     * The conditions are as follows:
     * 1. Customer is set
     * 2. Treatment is set
     * 3. The Room is set
     *
     * @return bool Returns true if all conditions are met, otherwise false.
     */
    private function validConditions(): bool
    {
        return $this->isCustomerSet() && $this->isTreatmentSet() && $this->isSalesSet() || $this->module == 'product';
    }

    /**
     * Check if the sales module is set.
     *
     * This function checks if the sales module is set by verifying the following conditions:
     * 1. If the module is not 'subscriptions',
     * 2. If the key 'sales_id' exists in the $this->item array.
     *
     * @return bool Returns true if the sales module is set, otherwise false.
     */
    public function isSalesSet(): bool
    {
        // Check if the module is not 'subscriptions'
        if ($this->module != 'subscriptions') {
            // Check if the key sales_id exists in $this->item array
            if (! array_key_exists('sales_id', $this->item)) {
                // Emit the 'salesRequired' event and return false if sales_id is not found
                $this->emit('salesRequired');

                return false;
            }
        }

        // Return true if all conditions are met
        return true;
    }

    /**
     * Validates the item before processing.
     *
     * Checks if the item is empty.
     * If it is empty, emits an event based on the module type.
     *
     * @return bool Returns true if the item is empty, otherwise returns false.
     */
    private function validateItem(): bool
    {
        if (empty($this->item)) {
            $moduleEvents = [
                'treatment' => 'treatmentRequired',
                'marketplace' => 'marketPlaceRequired',
                'package' => 'packageRequired',
                'product' => 'productRequired',
            ];

            if (isset($moduleEvents[$this->module])) {
                $this->emit($moduleEvents[$this->module]);
            }

            return true;
        }

        return false;
    }

    /**
     * Check if the customer is set.
     *
     * 1. Check if the customer ID is set.
     * 2. Emit a 'customerRequired' event if the customer ID is not set.
     *
     * @return bool Returns true if the customer is set, otherwise returns false.
     */
    private function isCustomerSet(): bool
    {
        if (! $this->customer) {
            $this->emit('customerRequired');

            return false;
        }

        return true;
    }

    /**
     * Checks if the treatment is set
     *
     * This method checks if the "module" property is set to "treatment" and if the "treatment" property is null.
     * If the conditions are met, it emits a "treatmentRequired" event and returns false.
     * Otherwise, it returns true.
     *
     * @return bool Returns true if the treatment is set, otherwise false
     */
    private function isTreatmentSet(): bool
    {
        if ($this->module == 'treatment' && $this->treatment == null) {
            $this->emit('treatmentRequired');

            return false;
        }

        return true;
    }

    /**
     * Calculates the grand total of arrays of numbers.
     *
     * @return void The grand total of the numbers in the array.
     */
    public function countGrandTotal(): void
    {
        $this->subTotal = collect($this->items)->sum('total_price');
        $this->emit('setSubTotal', $this->subTotal);

        $this->grandTotal = $this->subTotal + $this->salesInvoiceOthers - $this->salesInvoiceDiscount;
        $this->emit('setGrandTotal', $this->grandTotal);
    }

    /**
     * Emit changes to the component
     *
     * This method emits the following events:
     *  - setItems: Emits the items array to the component
     *  - countGrandTotal: Calculates the grand total and emits it to the component
     *  - clearItem: Emits a clear item event to the component
     */
    private function emitChanges(): void
    {
        $this->emit('setItems', $this->items);
        $this->countGrandTotal();
        $this->emit('clearItem');
    }

    /**
     * Clears the item data and emits events.
     *
     * This function clears the item data by resetting various properties to their default values.
     * It also emits events to clear the treatment search, marketplace search,
     * room, sales invoice properties, and person in charge.
     */
    public function clearItem(): void
    {
        $this->emit('clearTreatmentSearch');
        $this->emit('clearMarketPlaceSearch');
        $this->emit('clearPackageSearch');
        $this->emit('clearProductSearch');
        $this->emit('clearRoomSearch');

        // Clear the person in charge
        $this->emit('clearPersonInCharge');

        // Reset module and moduleId to default values
        $this->module = 'treatment';
        $this->personInCharge = [];
        $this->item = [];
        $this->note = null;

        // Reset treatment, marketplace, package, and product to default values
        $this->treatment = [];
        $this->marketplace = [];
        $this->package = [];
        $this->product = [];
        $this->room = [];
    }

    /**
     * Retrieve the SI data as an array.
     *
     * @return array The SI data.
     */
    public function salesInvoiceData(): array
    {
        $this->grandTotal = $this->subTotal + $this->salesInvoiceOthers - $this->salesInvoiceDiscount;

        return [
            'company_id' => auth()->user()->details->branch_id,
            'branch_id' => auth()->user()->details->branch_id,
            'appointment_id' => $this->appointment?->id,
            'sales_invoice_number' => $this->salesInvoiceNumber,
            'customer_id' => $this->customer['users']['id'],
            'discount' => $this->salesInvoiceDiscount,
            'tax' => $this->salesInvoiceTax,
            'others' => $this->salesInvoiceOthers,
            'grand_total' => $this->grandTotal,
            'sales_invoice_date' => $this->salesInvoiceDate.' '.date('H:i:s'),
            'sales_invoice_status' => $this->salesInvoiceStatus,
            'created_by' => loggedInUser(),
            'note' => $this->note,
        ];
    }

    /**
     * Updates the sales invoice items.
     */
    public function createOrUpdateSalesInvoiceItems(): void
    {
        // Delete existing sales invoice items for the current sales invoice
        SalesInvoiceItem::where('sales_invoice_id', $this->salesInvoice->id)->delete();

        // Iterate over the items and associate them with the current sales invoice
        foreach ($this->items as $item) {

            if ($item['transaction_status'] == 'redemption' && $item['qty'] < 0) {
                $item['price'] = 0;
                $item['total_price'] = 0;
            }

            if ($this->salesInvoice) {
                if ($item['discount_item'] > 0) {
                    $this->salesInvoice->is_discount = null;
                    $this->salesInvoice->approval()->delete();
                    Approval::create([
                        'company_id' => auth()->user()->details->branch_id,
                        'branch_id' => auth()->user()->details->branch_id,
                        'request_by' => loggedInUser(),
                        'request_date' => now(),
                        'approve_by' => null,
                        'approve_date' => null,
                        'module' => 'sales_invoice',
                        'module_id' => $this->salesInvoice->id,
                        'status' => 'pending',
                    ]);
                    $this->salesInvoice->save();
                }
            }

            $item['sales_invoice_id'] = $this->salesInvoice->id;

            // Create a new sales invoice item
            SalesInvoiceItem::create(is_array($item) ? $item : $item->toArray());
        }
    }

    /**
     * Updates the appointment with the sales invoice number.
     */
    public function updateAppointmentSalesInvoiceNumber(): void
    {
        // Check if an appointment exists
        if ($this->appointment) {
            // Update the appointment with the sales invoice number
            $this->appointment->update([
                'sales_invoice_number' => $this->salesInvoice->sales_invoice_number,
                'reference_module' => 'sales_invoice',
                'reference_id' => $this->salesInvoice->id,
            ]);
        }
    }

    /**
     * Performs the checkout process.
     *
     * This method is responsible for executing the checkout process.
     * It performs the following actions:
     *  - Checks if the cash drawer is closed.
     *  - Checks if there are valid items and a customer set.
     *  - Checks if there is enough payment.
     *  - Creates a new sales invoice.
     *  - Checks if any item has a discount.
     *  - Creates or updates sales invoice items.
     *  - Checks if sales invoice has discount.
     *  - Creates payment data.
     *  - Processes the checkout.
     */
    public function checkout(): void
    {
        // Check if the cash drawer is closed
        if ($this->isCashDrawerClosed()) {
            return;
        }

        // Check if the customer is set
        if (! $this->customer) {
            $this->emit('customerRequired');

            return;
        }

        // Check if an items array is empty
        if (empty($this->items)) {
            $this->emit('treatmentRequired');

            return;
        }

        // Check if the total payment is less than the grand total
        if (collect($this->payments)->sum('amount') < $this->grandTotal) {
            $this->emit('needMorePayment');

            return;
        }

        DB::transaction(function () {

            $this->salesInvoiceStatus = 'valid';

            // Create a new sales invoice
            $this->createANewSalesInvoice();

            // Process the checkout
            $this->processCheckout();

        }, 5);
    }

    /**
     * Checks if the cash drawer is closed.
     *
     * This method is responsible for checking if the cash drawer is closed for the current date.
     * It performs the following actions:
     *  - Queries the CashDrawer model for a record with the current date using the whereDate() method.
     *  - If a record is found, it emits a "siClosedAlready" event and returns true.
     *  - If no record is found, it returns false.
     *
     * @return bool True if the cash drawer is closed for the current date, false otherwise.
     */
    private function isCashDrawerClosed(): bool
    {
        if (CashDrawer::whereDate('date', today())->first()) {
            $this->emit('siClosedAlready');

            return true;
        }

        return false;
    }

    /**
     * Process the checkout process.
     *
     * This function performs the checkout process by executing the following actions:
     * 1. Set the items to the sales invoice items.
     * 2. Create or update the sales invoice items.
     * 3. If an appointment is set, update the appointment sales invoice number.
     * 4. Create the history transaction.
     * 5. Update the release details.
     * 6. If a sales invoice is set, redirect to print.
     * 7. Clear the sales invoice form.
     * 8. Emit the "dataSaved" event.
     * 9. Emit the "refreshSalesInvoice" event.
     */
    private function processCheckout(): void
    {
        // Set the items to the sales invoice items
        $this->items = $this->salesInvoice->items;

        // Create or update the sales invoice items
        $this->createOrUpdateSalesInvoiceItems();

        // If an appointment is set, update the appointment sales invoice number
        if ($this->appointment) {
            $this->updateAppointmentSalesInvoiceNumber();
        }

        // Create the history transaction
        $this->createHistoryTransaction();

        // Update the release details
        $this->updateReleaseDetails();

        // If a sales invoice is set, redirect to print
        if ($this->salesInvoice) {
            if (config('app.env') === 'production') {
                $this->redirectToPrint($this->salesInvoice->id); /* Redirect browser */
            }
        }

        // Clear the sales invoice form
        $this->clearSalesInvoiceForm();

        // Emit the "refreshSalesInvoice" event
        $this->emit('refreshSalesInvoice');
    }

    /**
     * Updates the release details of the sales invoice.
     *
     * This method is responsible for updating the release details of the sales invoice.
     * It performs the following actions:
     *  - Updates the 'release_by' field of the sales invoice with the ID of the authenticated user.
     *  - Updates the 'release_date' field of the sales invoice with the current date and time.
     */
    private function updateReleaseDetails(): void
    {
        $this->salesInvoice->update([
            'release_by' => loggedInUser(),
            'release_date' => now(),
        ]);
    }

    /**
     * Redirects to the print page for a specific sales invoice.
     */
    public function redirectToPrint($id): Redirector|RedirectResponse|Application
    {
        return redirect('http://clinic.local/print/'.$id);
    }

    /**
     * Create approval data for sales invoice.
     *
     * This function deletes any existing approval records for the sales invoice and creates a new approval record.
     */
    public function createApprovalData(): void
    {
        // Delete existing approval records for the sales invoice
        Approval::where('module', 'sales_invoice')
            ->where('module_id', $this->salesInvoice->id)
            ->delete();

        // Create a new approval record for the sales invoice
        Approval::create([
            'company_id' => auth()->user()->details->branch_id,
            'branch_id' => auth()->user()->details->branch_id,
            'request_by' => loggedInUser(),
            'request_date' => now(),
            'module' => 'sales_invoice',
            'module_id' => $this->salesInvoice->id,
            'status' => 'pending',
        ]);
    }

    /**
     * Stores the sales invoice data.
     *
     * This method is responsible for storing the sales invoice data.
     * It performs the following actions:
     *  - Checks if a customer is set. If not, it emits a "customerRequired" event and returns.
     *  - Checks if the items array is empty.
     * If so, it emits a "treatmentRequired" event and returns.
     *  - Creates a new sales invoice using the SalesInvoice::create() method with the salesInvoiceData() as input.
     *  - Calls the createOrUpdateSiItems() method.
     *  - If an appointment is set, it calls the updateAppointmentSalesInvoiceNumber() method.
     *  - Clears the sales invoice form.
     *  - Emits a "dataSaved" event.
     *  - Emits a "refreshSalesInvoice" event.
     */
    public function store(): void
    {

        // Check if the customer is set
        if (empty($this->customer)) {
            $this->emit('customerRequired');

            return;
        }

        // Check if an items array is empty
        if (empty($this->items)) {
            $this->emit('treatmentRequired');

            return;
        }

        DB::transaction(function () {

            // Create a new sales invoice
            $this->createANewSalesInvoice();

            // Update appointment sales invoice number
            if ($this->appointment) {
                $this->updateAppointmentSalesInvoiceNumber();
            }

            // Clear sales invoice form
            $this->clearSalesInvoiceForm();

            $this->emit('refreshSalesInvoice');

        }, 5);

    }

    /**
     * Create Payment Data
     *
     * This method creates a new payment object and saves it to the database.
     */
    public function createPaymentData(): void
    {
        $this->salesInvoice->payments()->delete();

        foreach ($this->payments as $payment) {
            $payment['company_id'] = auth()->user()->details->branch_id;
            $payment['sales_invoice_id'] = $this->salesInvoice->id;

            SalesInvoicePayment::create($payment);
        }
    }

    /**
     * Edit Sales Invoice
     *
     * Retrieves the sales invoice by its ID, sets the necessary data,
     * and emits an event to display the sales invoice form for editing.
     *
     * @param  int  $id  The ID of the sales invoice to be edited.
     */
    public function editSalesInvoice(int $id): void
    {
        // Retrieve the sales invoice by its ID
        $this->salesInvoice = SalesInvoice::find($id);

        $this->customer = User::find($this->salesInvoice->customer_id);
        $this->emit('selectCustomer', $this->customer->id);

        // Emit an event to set the sales invoice
        $this->emit('setSalesInvoice', $this->salesInvoice);

        // Set the sales invoice tax and emit an event
        $this->salesInvoiceTax = countFormat($this->salesInvoice->tax);
        $this->emit('setSalesInvoiceTax', $this->salesInvoice->tax);

        // Set the sales invoice others and emit an event
        $this->salesInvoiceOthers = countFormat($this->salesInvoice->others);
        $this->emit('setSalesInvoiceOthers', $this->salesInvoice->others);

        // Set the sales invoice discount and emit an event
        $this->salesInvoiceDiscount = countFormat($this->salesInvoice->discount);
        $this->emit('setSalesInvoiceDiscount', $this->salesInvoice->discount);

        // Calculate the subtotal of items and emit an event
        $this->subTotal = $this->salesInvoice->items->sum('total_price');
        $this->emit('setSubTotal', $this->subTotal);

        // Set the grand total and emit an event
        $this->grandTotal = $this->salesInvoice->grand_total;
        $this->emit('setGrandTotal', $this->grandTotal);

        // Set the items array by converting the items collection to an array
        $this->items = $this->salesInvoice->items->toArray();

        // Iterate over the items array and set the code and name for each item
        foreach ($this->items as $key => $item) {
            $this->items[$key]['code'] = getDataByModule($item['module'], $item['module_id'])->code;
            $this->items[$key]['name'] = getDataByModule($item['module'], $item['module_id'])->name;
        }
        // Emit an event to set the items in an array
        $this->emit('setItems', $this->items);

        // Set the payments array and emit an event
        $this->payments = $this->salesInvoice->payments;
        $this->emit('setPayments', $this->payments);

        // Calculate the total payment and emit an event
        $this->totalPayment = $this->payments->sum('amount');
        $this->emit('setTotalPayment', $this->totalPayment);

        // Calculate the difference in payment and emit an event
        $this->diffPayment = $this->grandTotal - $this->totalPayment;
        $this->emit('setDiffPayment', $this->diffPayment);

        // Set the sales invoice number and date
        $this->salesInvoiceNumber = $this->salesInvoice->sales_invoice_number;
        $this->salesInvoiceDate = dateFormat($this->salesInvoice->sales_invoice_date);

        // Set the action form and emit an event to show the sales invoice form
        $this->actionForm = 'update';
        $this->emit('showSalesInvoiceForm');
    }

    /**
     * Updates the sales invoice data.
     *
     * This method updates the sales invoice data by performing the following actions:
     * 1. Updates the existing sales invoice using the SalesInvoice::update()
     * method with the salesInvoiceData() as input.
     * 2. Call the createOrUpdateSalesInvoiceItems() method.
     * 3. Call the updateAppointmentSalesInvoiceNumber() method.
     * 4. Call the createPaymentData() method.
     * 5. Emits a "clearSalesInvoicePaymentForm" event.
     * 6. Emits a "clearSalesInvoicePaymentForm" event.
     * 7. Clears the sales invoice form.
     * 8. Emits a "dataUpdated" event.
     * 9. Emits a "refreshSalesInvoice" event.
     */
    public function update(): void
    {
        // Step 1: Update the existing sales invoice
        $this->salesInvoice->update($this->salesInvoiceData());

        // Step 2: Create or update sales invoice items
        $this->createOrUpdateSalesInvoiceItems();

        // Step 3: Update appointment sales invoice number
        $this->updateAppointmentSalesInvoiceNumber();

        // Step 4: Create payment data
        $this->createPaymentData();

        // Step 5: Emit "clearSalesInvoicePaymentForm" event
        $this->emit('clearSalesInvoicePaymentForm');

        // Step 6: Emit "clearSalesInvoicePaymentForm" event
        $this->emit('clearSalesInvoicePaymentForm');

        // Step 7: Clear sales invoice form
        $this->clearSalesInvoiceForm();

        // Step 8: Emit "dataUpdated" event
        $this->emit('dataUpdated');

        // Step 9: Emit "refreshSalesInvoice" event
        $this->emit('refreshSalesInvoice');
    }

    /**
     * Create history transactions for each item.
     */
    public function createHistoryTransaction(): void
    {
        $userTransactionApiController = new UserTransactionApiController;

        // Iterate over each item
        foreach ($this->items->toArray() as $item) {
            $item['customer_id'] = getSalesInvoiceById($item['sales_invoice_id'])->customer_id;
            // Check the module of the item
            if ($item['module'] == 'package') {
                $item['doctor_fee'] = getPackageFee($item['module_id'], 'doctor');
                $item['doctor_assistance_fee'] = getPackageFee($item['module_id'], 'doctor_assistance');
                $item['therapist_fee'] = getPackageFee($item['module_id'], 'therapist');
                $item['sales_fee'] = getPackageFee($item['module_id'], 'sales');
                // Create a package history transaction
                $userTransactionApiController->createPackageHistoryTransaction($item);
            } elseif ($item['module'] == 'treatment') {
                $item['doctor_fee'] = getTreatmentFee($item['module_id'], 'doctor');
                $item['doctor_assistance_fee'] = getTreatmentFee($item['module_id'], 'doctor_assistance');
                $item['therapist_fee'] = getTreatmentFee($item['module_id'], 'therapist');
                $item['sales_fee'] = getTreatmentFee($item['module_id'], 'sales');

                // Count treatment or marketplace bonus
                $item['qty'] = countTreatmentBonus($item['qty']);
                // Create treatment history transaction
                $userTransactionApiController->createTreatmentHistoryTransaction($item);
            } elseif ($item['module'] == 'marketplace') {
                $item['doctor_fee'] = getMarketPlaceFee($item['module_id'], 'doctor');
                $item['doctor_assistance_fee'] = getMarketPlaceFee($item['module_id'], 'doctor_assistance');
                $item['therapist_fee'] = getMarketPlaceFee($item['module_id'], 'therapist');
                $item['sales_fee'] = getMarketPlaceFee($item['module_id'], 'sales');

                // Create marketplace history transaction
                $userTransactionApiController->createMarketPlaceHistoryTransaction($item);
            } elseif ($item['module'] == 'product') {
                $item['doctor_fee'] = getProductFee($item['module_id'], 'doctor');
                $item['doctor_assistance_fee'] = getProductFee($item['module_id'], 'doctor_assistance');
                $item['therapist_fee'] = getProductFee($item['module_id'], 'therapist');
                $item['sales_fee'] = getProductFee($item['module_id'], 'sales');

                // Create product history transaction
                $userTransactionApiController->createProductHistoryTransaction($item);
            }
        }
    }

    /**
     * Release the sales invoice.
     */
    public function releaseSalesInvoice($salesInvoiceId): void
    {
        // Find the sales invoice by ID
        $this->salesInvoice = SalesInvoice::find($salesInvoiceId);
        $this->salesInvoice->sales_invoice_status = 'valid';
        $this->salesInvoice->save();

        // Retrieve the items and payments from the sales invoice
        $this->items = $this->salesInvoice->items;
        $this->payments = $this->salesInvoice->payments;

        if (! $this->items) {
            $this->emit('treatmentRequired');

            return;
        }
        // Check if the total payment is less than the grand total
        if (floor(collect($this->payments)->sum('amount')) < floor($this->salesInvoice->grand_total)) {
            $this->emit('needMorePayment');

            return;
        }

        DB::transaction(function () {
            // Create a history transaction
            $this->createHistoryTransaction();

            // Update the release information of the sales invoice
            $this->salesInvoice->update([
                'release_by' => loggedInUser(),
                'release_date' => now(),
            ]);

            $salesInvoiceId = $this->salesInvoice->id;

            // Clear the sales invoice form
            $this->clearSalesInvoiceForm();

            // Emit events for data update and sales invoice refresh
            $this->emit('refreshSalesInvoice');

            // Emit an event to hide the sales invoice details
            $this->emit('hideSalesInvoiceDetails');

            if (config('app.env') == 'production') {
                $this->emit('redirectToPrint', $salesInvoiceId);
            }
        }, 5);
    }

    /**
     * Clear the sales invoice form by performing the following steps:
     *
     * 1. Emit clear signals.
     * 2. Reset form components.
     * 3. Initialize sales invoice details.
     */
    public function clearSalesInvoiceForm(): void
    {
        $this->payments = [];
        $this->emit('setPayments', $this->payments);

        $this->emitClearSignals();
        $this->resetFormComponents();

        $this->salesInvoiceNumber = generateSalesInvoiceNumber();
        $this->salesInvoiceDate = date('Y-m-d');
    }

    /**
     * Emits signals to clear various search fields and reset the sales invoice form table.
     *
     * This function emits multiple events to clear different search fields and reset the sales invoice form table.
     * It also emits an event to reset all errors and hide the sales invoice form.
     */
    private function emitClearSignals(): void
    {
        // Emit event to clear the person in charge
        $this->emit('clearPersonInCharge');

        // Emit event to clear customer search
        $this->emit('clearCustomerSearch');

        // Emit event to clear treatment search
        $this->emit('clearTreatmentSearch');

        // Emit event to clear marketplace search
        $this->emit('clearMarketplaceSearch');

        // Emit event to clear package search
        $this->emit('clearPackageSearch');

        // Emit event to clear product search
        $this->emit('clearProductSearch');

        // Emit event to clear sales invoice items
        $this->emit('clearSalesInvoiceItems');

        // Emit event to clear sales invoice form table
        $this->emit('clearSalesInvoiceFormTable');

        // Emit event to reset all errors
        $this->emit('resetAllError');

        // Emit event to hide a sales invoice form
        $this->emit('hideSalesInvoiceForm');
    }

    /**
     * Reset form components.
     *
     * This method resets the error bag and resets the form components to their default values.
     */
    private function resetFormComponents(): void
    {
        $this->resetErrorBag();
        $this->reset();
    }

    /**
     * Clears the sales invoice items.
     */
    public function clearSalesInvoiceItems(): void
    {
        $this->items = [];
    }

    /**
     * Render the sales invoice form view.
     *
     * @return View|Factory|Application The rendered view.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.sales-invoice-form');
    }

    public function createANewSalesInvoice(): void
    {
        $this->salesInvoice = SalesInvoice::create($this->salesInvoiceData());

        // Check if any item has a discount
        foreach ($this->items as $item) {
            if ($item['discount_item'] > 0) {
                $this->hasDiscount = true;
                $this->emit('setHasDiscount', true);

                $this->createApprovalData();

                break;
            }
        }

        // Create or update sales invoice items
        $this->createOrUpdateSalesInvoiceItems();

        // Check if sales invoice has discount
        if ($this->salesInvoice->discount) {
            $this->hasDiscount = true;
            $this->emit('setHasDiscount', true);

            $this->createApprovalData();
        }

        // Create payment data
        $this->createPaymentData();
    }
}
