<?php

namespace App\Http\Livewire;

use App\Models\MarketPlace;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class MarketPlaceTable extends Component
{
    use WithPagination;

    public $search;

    public $titlePage = 'Market Place';

    public $moduleName = 'marketplace';

    public $moduleLabel = false;

    public $priceLabel = false;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshMarketPlace' => '$refresh',
        'marketPlaceFee',
        'marketPlaceMaterialUsage',
    ];

    /**
     * Redirect to the marketplace fees page for a specific ID.
     */
    public function marketPlaceFee(int $id): void
    {
        $this->redirect(route('master.market_place_fees', ['id' => $id]));
    }

    /**
     * Redirect to the marketplace material usages page for a specific ID.
     */
    public function marketPlaceMaterialUsage($id): void
    {
        $this->redirect(route('master.market_place_material_usages', ['id' => $id]));
    }

    /**
     * Get the pagination view.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Render the livewire component.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.market-place-table', [
            'marketplaces' => MarketPlace::where('name', 'like', '%'.$this->search.'%')
                ->orWhere('code', 'like', '%'.$this->search.'%')
                ->orderBy('id', 'desc')
                ->paginate(10),
        ]);
    }
}
