<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SalesFee extends Component
{
    public $salesRp = 0;

    public $salesPercent = 0;

    public $treatmentFeeType = 'fixed';

    public $treatmentFeeValue = 0;

    protected $listeners = [
        'clearSalesFee',
    ];

    /**
     * Update the treatment fee for the doctor.
     *
     * @param  mixed  $value  The new value of the treatment fee.
     */
    public function updatedSalesRp(mixed $value): void
    {
        // Update the type of treatment fee for the doctor to be fixed.
        $this->treatmentFeeType = 'fixed';

        // Update the value of the treatment fee for the doctor.
        $this->treatmentFeeValue = floatval($value);

        $this->emit('setSalesFee', $this->treatmentFeeType, $this->treatmentFeeValue);
    }

    /**
     * Update the sales percentage value for the doctor's treatment fee.
     *
     * @param  mixed  $value  The new percentage value.
     */
    public function updatedSalesPercent(mixed $value): void
    {
        // Set the treatment fee type to 'percentage'.
        $this->treatmentFeeType = 'percentage';

        // Convert the value to float and update the treatment fee value.
        $this->treatmentFeeValue = floatval($value);

        // Emit an event to set the sales fee with the updated treatment fee type and value.
        $this->emit('setSalesFee', $this->treatmentFeeType, $this->treatmentFeeValue);
    }

    /**
     * Clears the sales fee by resetting the state and error bag.
     */
    public function clearSalesFee(): void
    {
        $this->reset();
        $this->resetErrorBag();
    }

    /**
     * Render the view for the sales fee.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.sales-fee');
    }
}
