<?php

namespace App\Http\Livewire;

use App\Models\PurchaseOrder;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class PurchaseOrderTable extends Component
{
    use WithPagination;

    public $titlePage = 'Purchase Order';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshPurchaseOrder' => '$refresh',
    ];

    /**
     * Returns the name of the pagination view.
     *
     * @return string The name of the pagination view.
     */
    public function paginationView(): string
    {
        // Return the name of the pagination view
        return 'pagination';
    }

    /**
     * Render the view for the purchase order table.
     */
    public function render(): View|Factory|Application
    {
        // Retrieve purchase orders based on search query
        $purchaseOrders = PurchaseOrder::where('purchase_order_number', 'like', '%'.$this->search.'%')
            ->orderBy('id', 'desc')
            ->paginate(10);

        // Return the view with the purchase orders data
        return view('livewire.purchase-order-table', [
            'purchase_orders' => $purchaseOrders,
        ]);
    }
}
