<?php

namespace App\Http\Livewire;

use App\Models\ApplicationSetting;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;
use Livewire\WithPagination;

class SettingModuleTable extends Component
{
    use WithPagination;

    public $titlePage = 'Module';

    public $search;

    public $applicationSetting;

    public $companyCode;

    public $branchCode;

    protected $queryString = ['search'];

    protected $listeners = [
        'refresh' => '$refresh',
        'companyCode',
        'branchCode',
        'destroy',
    ];

    /**
     * Resets the page number when the search input is updated.
     */
    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    /**
     * Update status module.
     */
    public function updateStatus(ApplicationSetting $setting): void
    {
        $status = $setting->status == 'Y' ? 'N' : 'Y';

        $setting->update(['status' => $status]);

        $this->emit('dataUpdated');
        $this->emit('refresh');
    }


    /**
     * Delete module.
     */
    public function destroy($settingId): void
    {
        $this->applicationSetting = ApplicationSetting::find($settingId);
        $this->applicationSetting->delete();

        $this->emit('dataDeleted');
        $this->emit('refresh');
    }

    /**
     * Define the View for pagination.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Perform search filtering on a Module model based on the user's company ID, search keyword, and pagination.
     */
    public function getSettingData(): array|LengthAwarePaginator
    {
        return ApplicationSetting::where(
            'company_id', auth()->user()->details->company_id
        )
            ->where(function ($query) {
                $query->where('name', 'like', '%'.$this->search.'%');
            })
            ->orWhere(function ($query) {
                $query->where('value', 'like', '%'.$this->search.'%');
            })
            ->orderBy('id', 'desc')
            ->paginate(10);
    }

    /**
     * Render the Component view with Module data.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.setting-module-table', [
            'settings' => self::getSettingData(),
        ]);
    }
}