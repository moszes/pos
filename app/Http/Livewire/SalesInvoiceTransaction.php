<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoice;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SalesInvoiceTransaction extends Component
{
    public $salesInvoice;

    protected $listeners = [
        'setSalesInvoice',
    ];

    /**
     * Set the sales invoice based on the provided sales invoice number.
     *
     * @param  string  $salesInvoiceNumber  The sales invoice number to fetch the sales invoice.
     */
    public function setSalesInvoice(string $salesInvoiceNumber): void
    {
        // Retrieve the sales invoice with the given sales invoice number
        $this->salesInvoice = SalesInvoice::where('sales_invoice_number', $salesInvoiceNumber)->first();
    }

    /**
     * Render the sales invoice transaction view.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the rendered view
        return view('livewire.sales-invoice-transaction');
    }
}
