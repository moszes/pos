<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Breadcrumb extends Component
{
    public function render()
    {
        return view('livewire.breadcrumb');
    }
}
