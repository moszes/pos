<?php

namespace App\Http\Livewire;

use App\Models\Package;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\View\View;
use Livewire\Component;

class PackageFeePage extends Component
{
    public $packageId;

    public $package;

    public $moduleLabel = 'Package Details';

    public $moduleName = 'package';

    public $redirectRoute;

    public $priceLabel = true;

    /**
     * Mounts the component and initializes the necessary data.
     *
     * This function retrieves the package ID from the request segment, initializes the fee values,
     * finds the package by ID, and updates the fee values if the package exists.
     */
    public function mount(): void
    {
        // Get the package ID from the request segment
        $this->packageId = request()->segment(count(request()->segments()));

        // Find the package by ID
        $this->package = Package::find($this->packageId);

        if (! $this->package) {
            redirect(route('master.packages'));
        }
    }

    /**
     * Render the package fee page view.
     */
    public function render(): Factory|Application|View
    {
        // Retrieve the package using the packageId property
        $package = Package::find($this->packageId);

        // Return the livewire.package-fee-page view with the package data
        return view('livewire.package-fee-page', [
            'package' => $package,
        ]);
    }
}
