<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ForgetPassword extends Component
{
    public function render()
    {
        return view('livewire.forget-password')->layout('layouts.guest');
    }
}
