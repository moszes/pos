<?php

namespace App\Http\Livewire;

use Illuminate\View\View;
use Livewire\Component;

class StockPage extends Component
{
    /**
     * Render the view for the StockPage component.
     *
     * @return View The rendered view.
     */
    public function render(): View
    {
        return view('livewire.stock-page');
    }
}
