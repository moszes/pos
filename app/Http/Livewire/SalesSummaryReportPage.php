<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SalesSummaryReportPage extends Component
{
    public $showDocument = false;

    public $salesSummaries;

    protected $listeners = [
        'showSalesSummaryReportTable',
        'hideSalesSummaryReportTable',
        'hideSalesSummaryReportTable',
    ];

    /**
     * Toggles the visibility of the document.
     */
    public function showSalesSummaryReportTable($dateFrom, $dateTo, $customer): void
    {
        $this->emit('refreshSalesSummaries', $dateFrom, $dateTo, $customer);

        // Invert the value of the showDocument property
        $this->showDocument = true;
    }

    public function hideSalesSummaryReportTable(): void
    {
        // Invert the value of the showDocument property
        $this->showDocument = false;
    }

    /**
     * Render the sales summary report page.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.sales-summary-report-page');
    }
}
