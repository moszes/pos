<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CommissionReportDetailPage extends Component
{
    public function render()
    {
        return view('livewire.commission-report-detail-page');
    }
}
