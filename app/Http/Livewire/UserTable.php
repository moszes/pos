<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class UserTable extends Component
{
    use WithPagination;

    public $titlePage = 'User';

    public $user;

    public $search;

    protected $queryString = ['search'];

    public $userDetails;

    protected $listeners = [
        'refreshUser' => '$refresh',
        'goToUser',
    ];

    /**
     *  Redirects the user to a specific route based on their role.
     */
    public function goToUser(int $id): void
    {
        $this->user = User::find($id);

        $this->redirect(route('master.employee_details', ['employeeCode' => $this->user->details->code]));

        if ($this->user->hasRole('customer')) {
            $this->redirect(route('customer.data_redemption', ['customerId' => $id]));
        }
    }

    /**
     * Filters and searches users based on specific criteria.
     */
    public function searchFilter(): array|LengthAwarePaginator
    {
        return User::whereNotIn('users.id', [1, 2])
            ->join('user_details', 'users.id', '=', 'user_details.user_id')
            ->where('user_details.company_id', auth()->user()->details->company_id)
            ->where(function (Builder $query) {
                $query->where('users.name', 'like', '%'.$this->search.'%')
                    ->orWhere('users.email', 'like', '%'.$this->search.'%')
                    ->orWhere('user_details.phone', 'like', '%'.$this->search.'%')
                    ->orWhere('user_details.code', 'like', '%'.$this->search.'%');
            })
            ->orderBy('users.id', 'desc')
            ->paginate(10);
    }

    /**
     * Returns the name of the pagination view.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Renders the user table view in the Laravel application.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.user-table', [
            'users' => self::searchFilter(),
        ]);
    }
}
