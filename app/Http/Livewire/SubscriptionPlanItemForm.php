<?php

namespace App\Http\Livewire;

use App\Models\Treatment;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SubscriptionPlanItemForm extends Component
{
    public $formTitle = 'Form subscriptionPlan';

    public $actionForm = 'store';

    public $subscriptionPlan;

    public $treatment;

    public $treatmentList = false;

    public $treatments = [];

    public $searchTreatment;

    protected $listeners = [
        'clearSubscriptionPlanItemForm',
        'destroy',
    ];

    public function updatedSearchTreatment($value): void
    {
        if ($value != '') {
            $this->treatmentList = true;
            $this->treatments = Treatment::where('name', 'like', '%'.$value.'%')
                ->orWhere('code', 'like', '%'.$value.'%')
                ->get()->toArray();
        } else {
            $this->treatmentList = false;
        }

    }

    public function selectTreatment($id): void
    {
        $this->treatmentList = false;
        $this->treatment = Treatment::find($id);
        $this->treatments = $this->subscriptionPlan->treatments->pluck('id')->toArray();

        // check if value exists on an array
        if (! in_array($this->treatment->id, $this->treatments)) {
            $this->treatments[] = $this->treatment->id;
        }

        $this->subscriptionPlan->treatments()->sync($this->treatments);

        $this->emit('selectSubscriptionPlanItem', $this->subscriptionPlan->id);
        $this->emit('refreshSubscriptionPlanItemTable');
        $this->emit('clearSubscriptionPlanItemForm');
    }

    /**
     * Delete a subscriptionPlan by its ID.
     *
     * @param  int  $id  The ID of the subscriptionPlan to delete.
     */
    public function destroy(int $id): void
    {

        DB::transaction(function () use ($id) {

            $this->treatment = Treatment::find($id);

            // Find the subscriptionPlan by its ID and delete it
            $this->subscriptionPlan->treatments()->detach($this->treatment);

        }, 5);

        $this->emit('refreshSubscriptionPlanItemTable');

        $this->emit('clearSubscriptionPlanItemForm');
    }

    /**
     * Clear the subscriptionPlan form by resetting the form data and error bag.
     */
    public function clearSubscriptionPlanItemForm(): void
    {
        $this->resetExcept(['treatments', 'subscriptionPlan']);
        $this->resetErrorBag();

        // Emit an event to hide the subscriptionPlan form
        $this->emit('hideSubscriptionPlanItemForm');
        $this->emit('refreshSubscriptionPlanItem');
    }

    public function render(): View|Factory|Application
    {
        return view('livewire.subscription-plan-item-form');
    }
}
