<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Spatie\Activitylog\Models\Activity;

class TreatmentPriceHistoryTable extends Component
{
    public $treatment;

    /**
     * Render the view for the treatment history table.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the view for the treatment history table
        return view('livewire.treatment-price-history-table', [
            'priceHistories' => Activity::where('log_name', 'Treatment Log')
                ->where('subject_type', 'App\Models\Treatment')
                ->where('subject_id', $this->treatment->id)
                ->orderBy('id', 'desc')
                ->get(),
        ]);
    }
}
