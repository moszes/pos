<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class DoctorFee extends Component
{
    public $doctorRp = 0;

    public $doctorPercent = 0;

    public $treatmentFeeType = 'fixed';

    public $treatmentFeeValue = 0;

    protected $listeners = [
        'clearDoctorFee',
    ];

    /**
     * Update the treatment fee for the doctor.
     *
     * @param  mixed  $value  The new value of the treatment fee.
     */
    public function updatedDoctorRp(mixed $value): void
    {
        // Update the type of treatment fee for the doctor to be fixed.
        $this->treatmentFeeType = 'fixed';

        // Update the value of the treatment fee for the doctor.
        $this->treatmentFeeValue = floatval($value);

        $this->emit('setDoctorFee', $this->treatmentFeeType, $this->treatmentFeeValue);
    }

    /**
     * Update the percentage value for doctor's treatment fee.
     *
     * @param  mixed  $value  The new percentage value.
     */
    public function updatedDoctorPercent(mixed $value): void
    {
        // Set the treatment fee type to 'percentage'
        $this->treatmentFeeType = 'percentage';

        // Set the treatment fee value to the new value as a float
        $this->treatmentFeeValue = floatval($value);

        // Emit an event to update the doctor's fee with the new treatment fee type and value
        $this->emit('setDoctorFee', $this->treatmentFeeType, $this->treatmentFeeValue);
    }

    /**
     * Clears the doctor fee.
     */
    public function clearDoctorFee(): void
    {
        // Reset the state of the object
        $this->reset();

        // Reset the error bag
        $this->resetErrorBag();
    }

    /**
     * Render the view for the doctor fee.
     */
    public function render(): View|Factory|Application
    {
        // Return the view for the doctor fee
        return view('livewire.doctor-fee');
    }
}
