<?php

namespace App\Http\Livewire;

use Livewire\Component;

class MasterDataLink extends Component
{
    public function render()
    {
        return view('livewire.master-data-link');
    }
}
