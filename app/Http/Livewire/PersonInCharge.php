<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Livewire\Component;

class PersonInCharge extends Component
{
    public $personInCharge = [];

    public $doctorId = '-- Choose --';

    public $doctorAssistanceId = '-- Choose --';

    public $therapistId = '-- Choose --';

    public $salesId = '-- Choose --';

    public $docOption = true;

    public $docAssOption = true;

    public $therapistOption = true;

    public $salesOption = true;

    protected $listeners = [
        'clearPersonInCharge',
        'salesRequired',
        'setModule',
    ];

    /**
     * Handle the updated event.
     */
    public function updated(string $name, mixed $value): void
    {
        $this->emit('clearRoomErrorBag');
        $this->resetErrorBag();

        $nameToDbNameMap = [
            'doctorId' => 'doctor_id',
            'doctorAssistanceId' => 'doctor_assistant_id',
            'therapistId' => 'therapist_id',
            'salesId' => 'sales_id',
        ];

        $dbName = $nameToDbNameMap[$name] ?? '';
        $this->personInCharge[$dbName] = $value == '-- Choose --' ? null : $value;

        $this->emit('setPersonInCharge', $this->personInCharge);
    }

    /**
     * Set the module options based on the given module.
     *
     * @param  string  $module  The module to set the options for.
     */
    public function setModule(string $module): void
    {
        $this->reset('docOption', 'docAssOption', 'therapistOption', 'salesOption');

        // Check if the module is either 'product' or 'package'
        if ($module == 'product' || $module == 'package') {
            // Set the options to false
            $this->docOption = false;
            $this->docAssOption = false;
            $this->therapistOption = false;
        } elseif ($module == 'subscriptions') {
            $this->docOption = false;
            $this->docAssOption = false;
            $this->therapistOption = false;
            $this->salesOption = false;
        }
    }

    /**
     * Add an error message indicating that the person in charge is required.
     *
     * This function is responsible for adding an error message to the 'salesId' field
     * indicating that the sales person is required.
     */
    public function salesRequired(): void
    {
        // Add an error message to the 'salesId' field indicating that the sales person is required
        $this->addError('salesId', 'Sales is required');
    }

    /**
     * Clears the fee user.
     */
    public function clearPersonInCharge(): void
    {
        // Reset the fee user
        $this->reset();

        // Reset the error bag
        $this->resetErrorBag();
    }

    /**
     * Render the view for the specific component.
     */
    public function render(): Application|View|Factory
    {
        return view('livewire.person-in-charge');
    }
}
