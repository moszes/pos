<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class CompanyRegister extends Component
{
    /**
     * Render the register company page.
     *
     * @return Factory|View|Application The rendered view or layout.
     */
    public function render(): Factory|View|Application
    {
        // Render the register company page view
        return view('livewire.company-register-page')
            ->layout('layouts.guest');
    }
}
