<?php

namespace App\Http\Livewire;

use App\Models\SubscriptionPlan;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;
use Livewire\WithPagination;

class SubscriptionPlanTable extends Component
{
    use WithPagination;

    public $titlePage = 'Subscription Plan';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshSubscriptionPlanTable' => '$refresh',
    ];

    /**
     * Resets the page number when the search input is updated.
     */
    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    /**
     * Define the View for pagination.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Perform search filtering on a SubscriptionPlan model based on the user's company ID, search keyword, and pagination.
     */
    public function searchFilter(): array|LengthAwarePaginator
    {
        return SubscriptionPlan::where('company_id', auth()->user()->details->company_id)
            ->where(function ($query) {
                $query->where('name', 'like', '%'.$this->search.'%')
                    ->orWhere('code', 'like', '%'.$this->search.'%')
                    ->orWhere('type', 'like', '%'.$this->search.'%');
            })
            ->orderBy('id', 'desc')
            ->paginate(10);
    }

    /**
     * Render the Component view with SubscriptionPlan data.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.subscription-plan-table', [
            'subscriptionPlans' => self::searchFilter(),
        ]);
    }
}
