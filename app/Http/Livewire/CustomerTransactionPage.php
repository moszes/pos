<?php

namespace App\Http\Livewire;

use DB;
use Illuminate\View\View;
use Livewire\Component;

class CustomerTransactionPage extends Component
{
    public function render(): View
    {
        return view('livewire.customer-transaction-page', [
            'user_transactions' => DB::table('user_transactions')->where('user_id', $this->user->id)->paginate(10),
        ]);
    }
}
