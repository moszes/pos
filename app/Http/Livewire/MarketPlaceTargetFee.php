<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class MarketPlaceTargetFee extends Component
{
    public $marketPlaceTargetFee = [];

    public $marketPlace;

    /**
     * Mount the component and initialize fee values.
     *
     * If the marketplace exists, update the fee values based on the marketplace's target fees.
     */
    public function mount(): void
    {
        // Initialize the fee values
        $this->marketPlaceTargetFee = [
            'senior_doctor' => 0, // Fee for the senior doctor
            'doctor' => 0, // Fee for the doctor
            'doctor_assistance' => 0, // Fee for the doctor's assistance
            'sales' => 0, // Fee for sales
            'therapist' => 0, // Fee for the therapist
        ];

        // If the marketplace exists, update the fee values
        if ($this->marketplace) {
            foreach ($this->marketplace->targetFees as $value) {
                // Update the fee value based on the role
                $this->marketPlaceTargetFee[$value->role] = countFormat($value->value);
            }
        }
    }

    /**
     * Update the fee values based on the property name and value.
     *
     * @param  string  $propertyName  The name of the property that was updated.
     * @param  mixed  $value  The new value for the property.
     */
    public function updated(string $propertyName, mixed $value): void
    {
        // Check if the property name is one of the fee properties
        if (in_array($propertyName, [
            'marketPlaceTargetFee.senior_doctor',
            'marketPlaceTargetFee.doctor',
            'marketPlaceTargetFee.doctor_assistance',
            'marketPlaceTargetFee.sales',
            'marketPlaceTargetFee.therapist',
        ])) {
            // Remove the 'marketPlaceTargetFee.' prefix from the property name
            $marketPlaceTargetFeeName = str_replace('marketPlaceTargetFee.', '', $propertyName);

            // Convert the value to a float and update the marketPlaceTargetFee property
            $this->marketPlaceTargetFee[$marketPlaceTargetFeeName] = floatval($value);

            $this->emit('feeUpdated', 'marketplace', 'target', $this->marketPlaceTargetFee);
        }
    }

    /**
     * Render the view for the MarketPlaceTargetFee Livewire component.
     *
     * This method returns the view for the MarketPlaceTargetFee Livewire component, which is 'livewire.marketplace-target-fee'.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the livewire.marketplace-target-fee view
        return view('livewire.market-place-target-fee');
    }
}
