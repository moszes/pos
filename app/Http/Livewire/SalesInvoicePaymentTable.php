<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SalesInvoicePaymentTable extends Component
{
    public $salesInvoicePayments = [];

    public $items = [];

    public $payments = [];

    public $diffPayment = 0;

    public $grandTotal = 0;

    public $totalPayment = 0;

    protected $listeners = [
        'setItems',
        'setPayments',
        'setTotalPayment',
        'setGrandTotal',
        'setDiffPayment',
        'clearSalesInvoicePaymentTable',
    ];

    /**
     * Update the items property with the given items.
     *
     * @param  mixed  $items  The items to be set.
     */
    public function setItems(mixed $items): void
    {
        $this->items = $items;
    }

    /**
     * Update the payment's property with the given payments.
     *
     * @param  mixed  $payments  The payments to be set.
     */
    public function setPayments(mixed $payments): void
    {
        $this->payments = $payments;
    }

    /**
     * Sets the grand total for the sales invoice.
     *
     * @param  mixed  $grandTotal  The grand total value to be set.
     */
    public function setGrandTotal(mixed $grandTotal): void
    {
        $this->grandTotal = $grandTotal;
    }

    /**
     * Sets the difference payment value.
     *
     * @param  mixed  $diffPayment  The value to set as the difference payment.
     */
    public function setDiffPayment(mixed $diffPayment): void
    {
        $this->diffPayment = $diffPayment;
    }

    /**
     * Update the total payment property with the given total payment.
     *
     * @param  mixed  $totalPayment  The total payment to be set.
     */
    public function setTotalPayment(mixed $totalPayment): void
    {
        $this->totalPayment = $totalPayment;
    }

    /**
     * Removes a sales invoice payment.
     *
     * @param  int  $arrayKey  The key of the payment to be removed from the `payments` array.
     */
    public function removeSalesInvoicePayment(int $arrayKey): void
    {
        unset($this->payments[$arrayKey]);

        $this->emit('setPayments', $this->payments);
        $this->totalPayment = collect($this->payments)->sum('amount');
        $this->emit('setDiffPayment', $this->totalPayment - $this->grandTotal);
    }

    public function clearSalesInvoicePaymentTable(): void
    {
        $this->reset();
    }

    /**
     * Renders the sales invoice payment table view.
     *
     * @return View|Factory|Application The rendered view.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.sales-invoice-payment-table');
    }
}
