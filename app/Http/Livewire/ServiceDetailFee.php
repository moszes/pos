<?php

namespace App\Http\Livewire;

use App\Models\ServiceFee;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class ServiceDetailFee extends Component
{
    public $module;

    public $roles = ServiceFee::ROLES;

    public $normalFees;

    public $discountFees;

    public $targetFees;

    /**
     * Mount the component.
     */
    public function mount($module): void
    {
        $this->normalFees = $module->normalFees;
        $this->discountFees = $module->discountFees;
        $this->targetFees = $module->targetFees;
    }

    /**
     * Render the view for the product detail fee.
     */
    public function render(): View
    {
        // Return the view for the product detail fee.
        return view('livewire.service-detail-fee');
    }
}
