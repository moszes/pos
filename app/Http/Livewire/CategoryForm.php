<?php

namespace App\Http\Livewire;

use App\Models\Category;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class CategoryForm extends Component
{
    public $formTitle = 'Form Category';

    public $actionForm = 'store';

    public $category;

    public $code;

    public $name;

    public $type;

    protected $listeners = [
        'clearCategoryForm',
        'editCategory',
        'destroy',
    ];

    protected $rules = [
        'code' => 'required|unique:categories',
        'name' => 'required',
    ];

    /**
     * Retrieves the category data.
     *
     * @return array The category data.
     */
    public function getCategoryData(): array
    {
        // Get the first company ID
        $companyId = auth()->user()->details->branch_id;

        // Get the first branch ID
        $branchId = auth()->user()->details->branch_id;

        // Determine the type value
        $type = ($this->type == '-- Choose --') ? 1 : $this->type;

        // Build and return the category data array
        return [
            'company_id' => $companyId,
            'branch_id' => $branchId,
            'code' => $this->code,
            'name' => $this->name,
            'type' => $type,
        ];
    }

    /**
     * Store a new category.
     */
    public function store(): void
    {
        // Validate the category data
        $this->validate();

        // Use a transaction to ensure data integrity
        DB::transaction(function () {
            // Create a new category
            Category::create($this->getCategoryData());

        }, 5);

        // Emit an event to clear the category form inputs
        $this->emit('clearCategoryForm');
    }

    /**
     * Edits a category by retrieving its details and preparing the form for update.
     *
     * @param  int  $id  The ID of the category to edit.
     */
    public function editCategory(int $id): void
    {
        // Retrieve the category by ID
        $this->category = Category::find($id);

        // Set the category details to form fields
        $this->code = $this->category->code;
        $this->name = $this->category->name;
        $this->type = $this->category->type;

        // Set the form action and emit an event to show the category form
        $this->actionForm = 'update';
        $this->emit('showCategoryForm');
    }

    /**
     * Update the category and perform the necessary actions.
     */
    public function update(): void
    {
        DB::transaction(function () {
            // Update the category
            $this->category->update($this->getCategoryData());

            // Emit an event to clear the category form inputs
            $this->emit('clearCategoryForm');
        }, 5);
    }

    /**
     * Delete a category by its ID.
     *
     * @param  int  $id  The ID of the category to delete.
     */
    public function destroy(int $id): void
    {
        $this->category = Category::find($id);

        DB::transaction(function () {
            $this->category->code = $this->category->code.'-deleted';
            $this->category->save();
            // Find the category by its ID and delete it
            $this->category->delete();

        }, 5);

        $this->emit('clearCategoryForm');
    }

    /**
     * Clear the category form by resetting the form data and error bag.
     */
    public function clearCategoryForm(): void
    {
        $this->reset();
        $this->resetErrorBag();

        // Emit an event to hide the category form
        $this->emit('hideCategoryForm');
        $this->emit('refreshCategory');
    }

    /**
     * Render the category form view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.category-form');
    }
}
