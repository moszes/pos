<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;
use Livewire\WithPagination;

class SubCategoryTable extends Component
{
    use WithPagination;

    public $titlePage = 'Sub Category';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshSubCategory' => '$refresh',
    ];

    /**
     * Reset the current page when the search query is updated.
     */
    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    /**
     * Return the name of the pagination view.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Fetches the subcategories based on the search term and authorized user's branch ID
     */
    public function searchFilter(): array|LengthAwarePaginator
    {
        $categories = Category::where('company_id', auth()->user()->details->company_id)
            ->pluck('id')
            ->toArray();

        return SubCategory::where(function ($query) {
            $query->where('code', 'like', '%'.$this->search.'%')
                ->orWhere('name', 'like', '%'.$this->search.'%');
        })
            ->whereIn('category_id', $categories)
            ->orderBy('id', 'desc')
            ->paginate(10);
    }

    /**
     * Render the view for the subcategory table.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.sub-category-table', [
            'subCategories' => self::searchFilter(),
        ]);
    }
}
