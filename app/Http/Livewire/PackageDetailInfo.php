<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class PackageDetailInfo extends Component
{
    public $package;

    public $moduleLabel;

    public $moduleName;

    public $redirectRoute;

    public $priceLabel = true;

    /**
     * Render the package detail info view.
     */
    public function render(): View
    {
        // Return the view for the package detail info
        return view('livewire.package-detail-info');
    }
}
