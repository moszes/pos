<?php

namespace App\Http\Livewire;

use App\Models\Company;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use LaravelIdea\Helper\App\Models\_IH_Company_C;
use Livewire\Component;

/**
 * @property Company|Company[]|_IH_Company_C|null $company
 */
class CompanyForm extends Component
{
    public $formTitle = 'Form Company';

    public $actionForm = 'store';

    public $company;

    public $companyId;

    public $code;

    public $name;

    public $email;

    public $phone;

    public $address;

    public $website;

    protected $listeners = [
        'clearCompanyForm',
        'editCompany',
        'destroy',
    ];

    protected $rules = [
        'code' => 'required|unique:companies',
        'name' => 'required',
    ];

    public function companyData(): array
    {
        return [
            'company_id' => $this->companyId == null ? 1 : $this->companyId,
            'code' => $this->code,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'website' => $this->website,
        ];
    }

    public function store(): void
    {
        $this->validate();

        Company::create($this->companyData());

        $this->clearCompanyForm();
        $this->emit('dataSaved');
        $this->emit('refreshCompany');
    }

    public function editCompany($id): void
    {
        $this->company = Company::find($id);

        $this->companyId = $this->company->company_id;
        $this->code = $this->company->code;
        $this->name = $this->company->name;
        $this->email = $this->company->email;
        $this->phone = $this->company->phone;
        $this->address = $this->company->address;
        $this->website = $this->company->website;

        $this->actionForm = 'update';
        $this->emit('showCompanyForm');
    }

    public function update(): void
    {
        $this->validate();

        $this->company->update($this->companyData());

        $this->actionForm = 'save';

        $this->clearCompanyForm();
        $this->emit('dataUpdated');
        $this->emit('refreshCompany');
    }

    public function destroy($id): void
    {
        Company::find($id)->delete();

        $this->actionForm = 'save';

        $this->clearCompanyForm();
        $this->emit('dataDeleted');
        $this->emit('refreshCompany');
    }

    public function clearCompanyForm(): void
    {
        $this->emit('hideCompanyForm');
        $this->actionForm = 'store';

        $this->reset();
    }

    public function render(): Factory|View|Application
    {
        return view('livewire.company-form');
    }
}
