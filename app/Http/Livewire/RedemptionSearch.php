<?php

namespace App\Http\Livewire;

use App\Http\Controllers\UserTransactionApiController;
use App\Models\Treatment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class RedemptionSearch extends Component
{
    public $customer = [];

    public $treatment;

    // redemption
    public $redemption = [];

    public $redemptions;

    public $redemptionSearch;

    public $redemptionLists = false;

    public $redemptionQty = 1;

    public $redemptionPrice = 0;

    public $redemptionDiscount = 0;

    public $note;

    protected $listeners = [
        'clearRedemptionSearch',
        'zeroItemsOnCart',
        'setCustomer',
        'redemptionRequired',
    ];

    /**
     * Updates the redemption search.
     *
     * This function is triggered when the value of the redemptionSearch property is updated.
     * It performs the following actions:
     * - Checks if the customer is not set.
     * - If the customer is not set, it emits an event to indicate that the customer is required.
     * - Resets the redemption search.
     * - If the customer is set, it performs the following actions:
     *   - Resets the redemption price, quantity, and discount.
     *   - Sets the redemption search flag.
     *   - Retrieves user transactions by customer ID using the UserTransactionApiController.
     */
    public function updatedRedemptionSearch(): void
    {
        // Check if the customer is not set
        if (empty($this->customer)) {
            // Emit event to indicate that a customer is required
            $this->emit('customerRequired');

            // Reset the redemption search
            $this->redemptionSearch = null;

            return;
        }

        // Reset redemption price, quantity, and discount
        $this->redemptionPrice = 0;
        $this->redemptionQty = 1;
        $this->redemptionDiscount = 0;

        // Set a redemption search flag
        $this->redemptionLists = $this->redemptionSearch != '';

        // Retrieve user transactions by customer ID
        $userTransactionApiController = new UserTransactionApiController;
        $this->redemptions = $userTransactionApiController->getUserTransactionsByCustomerId($this->customer['users']['id']);

    }

    /**
     * Set the customer for the object.
     *
     * @param  mixed  $customer  The customer to set.
     */
    public function setCustomer(mixed $customer): void
    {
        // Assign the provided customer to the class property $this->customer
        $this->customer = $customer;
    }

    /**
     * Adds an error message to indicate that there are no items in the cart.
     */
    public function zeroItemsOnCart(): void
    {
        // Add an error message to the 'redemptionSearch' field
        $this->addError('redemptionSearch', 'No Items in Cart');
    }

    /**
     * Redeem the selected item.
     *
     * @param  mixed  $ref_module_id  The reference module ID.
     * @param  string  $code  The code of the item.
     * @param  string  $name  The name of the item.
     */
    public function redeemSelected(mixed $ref_module_id, string $code, string $name): void
    {
        // Check if the reference module ID is 0
        if ($ref_module_id == 0) {
            // Emit events to indicate that a customer is required and to clear the redemption search
            $this->emit('customerRequired');
            $this->emit('clearRedemptionSearch');
            $this->emit('clearCustomerSearch');

            // Set the redemption lists to false
            $this->redemptionLists = false;

            return;
        }

        // Set the redemption search value
        $this->redemptionSearch = $code.' - '.$name;
        // Set the redemption lists to false
        $this->redemptionLists = false;

        $this->treatment = Treatment::find($ref_module_id);

        // Create the redemption array
        $this->redemption = [
            'module' => 'treatment',
            'module_id' => $ref_module_id,
            'code' => $code,
            'name' => $name,
            'qty' => $this->redemptionQty * -1,
            'price' => $this->treatment->price,
            'total_price' => $this->treatment->price * $this->redemptionQty * -1,
            'transaction_status' => 'redemption',
        ];

        // Emit the redemption-selected event with the redemption data
        $this->emit('setRedemption', $this->redemption);
    }

    /**
     * Add an error message indicating that no items are present in the cart.
     */
    public function redemptionRequired(): void
    {
        $this->addError('redemptionSearch', 'Treatment Required');
    }

    /**
     * Clears the redemption properties.
     *
     * This function resets the redemption properties except for the 'customer' property
     * and clears the error bag.
     */
    public function clearRedemptionSearch(): void
    {
        // Reset all properties of the redemption except for the 'customer' property
        $this->resetExcept('customer');

        // Clear the error bag
        $this->resetErrorBag();
    }

    /**
     * Renders the view for the redemption search.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.redemption-search');
    }
}
