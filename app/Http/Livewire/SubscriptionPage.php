<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class SubscriptionPage extends Component
{
    public $titlePage = 'Subscription';

    /**
     * Render the subscription page view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.subscription-page');
    }
}
