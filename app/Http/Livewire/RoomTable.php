<?php

namespace App\Http\Livewire;

use App\Models\Company;
use App\Models\Room;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;
use Livewire\WithPagination;

class RoomTable extends Component
{
    use WithPagination;

    public $titlePage = 'Room';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshRoom' => '$refresh',
    ];

    /**
     * Reset the page when the search input is updated.
     */
    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    /**
     * Get the pagination view.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Perform a search filter query on the rooms table.
     */
    public function searchFilter(): array|LengthAwarePaginator
    {
        return Room::where('name', 'like', '%'.$this->search.'%')
            ->where('company_id', Company::first()->id)
            ->orWhere('code', 'like', '%'.$this->search.'%')
            ->orderBy('id', 'desc')
            ->paginate(10);
    }

    /**
     * Render the view for the room table.
     */
    public function render(): Factory|Application|View
    {
        return view('livewire.room-table', [
            'rooms' => self::searchFilter(),
        ]);
    }
}
