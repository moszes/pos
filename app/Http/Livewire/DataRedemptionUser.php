<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\UserTransaction;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Query\Builder;
use Illuminate\View\View;
use Livewire\Component;

class DataRedemptionUser extends Component
{
    public $user;

    public $customerRedemptions;

    public $redemptionLists;

    public $customerId;

    protected $queryString = ['customerId'];

    public function mount(): void
    {
        $this->user = User::find($this->customerId);

        $this->customerRedemptions = DB::table('user_transactions')
            ->where('user_id', $this->customerId)
            ->whereIn('reference_module', ['package', 'seeder'])
            ->orWhere(function ($query) {
                $query->where('user_id', $this->customerId)
                    ->WhereIn('default_promo', ['B10', 'B5']);
            })
            ->orWhere(function ($query) {
                $query->where('user_id', $this->customerId)
                    ->WhereNotNull('appointment_id');
            })
            ->where(function (Builder $query) {
                $query->where('module', 'treatment');
            })
            ->whereNull('deleted_at')
            ->get();

        foreach ($this->customerRedemptions as $key => $userTreatment) {

            if (floatval($userTreatment->qty) < 1 && $userTreatment->appointment_id == null) {
                unset($userTreatment);

                continue;
            }

            if (! isset($userTreatment->redemption_left)) {
                $userTreatment->redemption_left = 0;
            }

            if ($userTreatment->module == 'marketplace') {
                $userTreatment->code = @$userTreatment->module_code;
                $userTreatment->name = @$userTreatment->module_name;
            } elseif ($userTreatment->module == 'treatment') {
                $userTreatment->code = @$userTreatment->module_code;
                $userTreatment->name = @$userTreatment->module_name;
            }

            $userTreatment->redemption_left = UserTransaction::where('user_id', $userTreatment->user_id)
                ->where('module', 'treatment')
                ->where('module_id', $userTreatment->module_id)
                ->sum('qty');

            if ($userTreatment->redemption_left < 1) {
                unset($this->customerRedemptions[$key]);
            }
        }

        $this->redemptionLists = $this->customerRedemptions->unique('code')->all();
    }

    /**
     * Render the Livewire component.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.data-redemption-user');
    }
}
