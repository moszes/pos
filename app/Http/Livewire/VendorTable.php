<?php

namespace App\Http\Livewire;

use App\Models\Vendor;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;
use Livewire\WithPagination;

class VendorTable extends Component
{
    use WithPagination;

    public $titlePage = 'Vendor';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshVendor' => '$refresh',
    ];

    /**
     * Resets the page when the search input is updated.
     */
    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    /**
     * Searches for vendors based on the search parameter.
     *
     * @return array|LengthAwarePaginator The search results as an array or a LengthAwarePaginator instance.
     */
    public function searchFilter(): array|LengthAwarePaginator
    {
        return Vendor::where('company_id', auth()->user()->details->company_id)
            ->where(function ($query) {
                $query->where('name', 'like', '%'.$this->search.'%')
                    ->orWhere('code', 'like', '%'.$this->search.'%');
            })
            ->orderBy('id', 'desc')
            ->paginate(10);
    }

    /**
     * Get the name of the pagination view.
     *
     * @return string The name of the pagination view.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Render the view for the vendor-table component.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.vendor-table', [
            'vendors' => self::searchFilter(),
        ]);
    }
}
