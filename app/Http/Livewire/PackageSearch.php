<?php

namespace App\Http\Livewire;

use App\Models\Package;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PackageSearch extends Component
{
    public $package = [];

    public $packages;

    public $packageList = false;

    public $packageSearch;

    public $packageQty = 1;

    public $packagePrice = 0;

    public $packageDiscountItem = 0;

    protected $listeners = [
        'clearPackageSearch',
        'packageRequired',
    ];

    /**
     * Handle the updated event for the specified field.
     *
     * @param  string  $name  The name of the updated field.
     */
    public function updated(string $name): void
    {
        // Emit events to clear person in charge and room search
        $this->emit('clearPersonInCharge');
        $this->emit('clearRoomSearch');

        // Reset error bag
        $this->resetErrorBag();

        // Update package information if the updated field is package related
        if ($name === 'packageQty' || $name === 'packageDiscountItem' || $name === 'packagePrice') {
            // Update package quantity
            $this->package['qty'] = $this->packageQty;

            // Update package discount item
            $this->package['discount_item'] = $this->packageDiscountItem;

            // Update package price
            $this->package['price'] = $this->packagePrice;

            // Emit event to indicate package selection
            $this->emit('packageSelected', $this->package);
        }
    }

    /**
     * Handle the updated event for the packageDiscountItem property.
     */
    public function updatedPackageDiscountItem(): void
    {
        $this->packageDiscountItem = floatval($this->packageDiscountItem);

        // Check if the package discount item is greater than 0
        if ($this->packageDiscountItem > 0) {
            // Emit the 'applyDiscount' event
            $this->emit('applyDiscount');
        }
    }

    /**
     * Updates the package search functionality.
     *
     * This method updates the package search by setting the packageList flag to true
     * and generating the upper case search value based on the packageSearch property.
     * It then queries the "packages" table for packages that match the search criteria.
     * If the package search is empty, it sets the "packages" property to null.
     */
    public function updatedPackageSearch(): void
    {
        // Set the packageList flag to true
        $this->packageList = true;

        // Initialize the packages property to null
        $this->packages = null;

        // Generate the upper case search value
        $upperCaseSearchValue = '%'.strtoupper($this->packageSearch).'%';

        // Check if the package search is not empty
        if ($this->packageSearch != '') {
            // Query the "packages" table for packages that match the search criteria
            $this->packages = Package::query()
                ->where('company_id', auth()->user()->details->branch_id)
                ->where(function ($query) use ($upperCaseSearchValue) {
                    // Add conditions to match the code or name with the upper case search value
                    $query->where('code', 'like', $upperCaseSearchValue)
                        ->orWhere('name', 'like', $upperCaseSearchValue);
                })
                ->get();
        }
    }

    /**
     * Handle the updated event for the packageQty property.
     *
     * This function checks if the packageQty value is numeric.
     * If it is not numeric,
     * it sets the packageQty value to 1, emits the 'shouldNumericValue' event,
     * and adds an error message for the packageQty field.
     */
    public function updatedPackageQty(): void
    {
        // Check if the packageQty value is numeric
        if (! is_numeric($this->packageQty)) {
            // Set the packageQty value to 1
            $this->packageQty = 1;

            // Emit the 'shouldNumericValue' event
            $this->emit('shouldNumericValue');

            // Add an error message for the packageQty field
            $this->addError('packageQty', 'The quantity must be a number.');
        }
    }

    /**
     * Handle the updated event for the packagePrice property.
     *
     * This function checks if the packagePrice value is numeric.
     * If it is not numeric,
     * it sets the packagePrice value to 0, emits the 'shouldNumericValue' event,
     * and adds an error message for the packagePrice field.
     */
    public function updatedPackagePrice(): void
    {
        // Check if the packagePrice value is numeric
        if (! is_numeric($this->packagePrice)) {
            // Set the packagePrice value to 0
            $this->packagePrice = 0;

            // Emit the 'shouldNumericValue' event
            $this->emit('shouldNumericValue');

            // Add an error message for the packagePrice field
            $this->addError('packagePrice', 'The price must be a number.');
        }
    }

    /**
     * Select a package.
     *
     * @param  mixed  $id  The ID of the package.
     */
    public function selectPackage(mixed $id): void
    {
        $this->package = Package::find($id)->toArray();

        $this->populatePackage($id);

        $this->packages = null;

        $this->package['total_price'] = $this->package['price'] * $this->package['qty'] - $this->package['discount_item'];

        $this->emit('packageSelected', $this->package);
    }

    /**
     * Populates the package details.
     *
     * This function sets the package list to false,
     * concatenates the package code, name, and price into the package search field,
     * converts the package price to a float, sets the package quantity and discount item to float values,
     * sets the package price, transaction status, module, and module ID.
     *
     * @param  mixed  $id  The ID of the package.
     */
    protected function populatePackage(mixed $id): void
    {
        // Set the package list to false
        $this->packageList = false;

        // Concatenate the package code, name, and price into the package search field
        $this->packageSearch = $this->package['code'].' - '.$this->package['name'].' - '.$this->package['price'];

        // Convert the package price to a float
        $this->packagePrice = floatval($this->package['price']);

        // Set the package quantity and discount item to float values
        $this->package['qty'] = floatval($this->packageQty);
        $this->package['discount_item'] = floatval($this->packageDiscountItem);

        // Set the package price
        $this->package['price'] = $this->packagePrice;

        // Set the transaction status, module, and module ID
        $this->package['transaction_status'] = 'buy';
        $this->package['module'] = 'package';
        $this->package['module_id'] = $id;
    }

    /**
     * Set the package as required.
     *
     * This function adds an error message to the 'packageSearch' field indicating that a package must be selected.
     */
    public function packageRequired(): void
    {
        // Add an error message to the 'packageSearch' field
        $this->addError('packageSearch', 'Please select a package.');
    }

    /**
     * Clears the package search and resets the error bag.
     */
    public function clearPackageSearch(): void
    {
        // Reset the package search
        $this->reset();

        // Clear the error bag
        $this->resetErrorBag();
    }

    /**
     * Renders the package search view.
     */
    public function render(): View|Factory|Application
    {
        // Return the package search view
        return view('livewire.package-search');
    }
}
