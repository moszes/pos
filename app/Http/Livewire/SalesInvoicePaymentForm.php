<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoice;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SalesInvoicePaymentForm extends Component
{
    public $payment = [];

    public $payments = [];

    public $paymentId = 1;

    public $paymentAmount = 0;

    public $paymentStatus = 'paid';

    public $referenceNumber;

    public $totalPayment = 0;

    public $diffPayment = 0;

    public $grandTotal = 0;

    public $items = [];

    public $salesInvoice;

    protected $listeners = [
        'setItems',
        'paymentRequired',
        'clearPayment',
        'setGrandTotal',
        'removePayment',
        'clearSalesInvoicePaymentForm',
        'setSalesInvoice',
        'resetAllError',
        'setPayments',
    ];

    /**
     * Resets the error bag.
     */
    public function updated(): void
    {
        $this->resetErrorBag();
    }

    /**
     * Set the payments for the sales invoice.
     *
     * This method receives an array of payments and updates the payments property
     * of the sales invoice object with the provided value.
     * It does not perform any
     * validation or processing on the payments' data.
     * The method simply assigns the
     * given value to the payment's property.
     *
     * @param  array  $payments  The array of payments.
     */
    public function setPayments(array $payments): void
    {
        $this->payments = $payments;
    }

    /**
     * Update the payment amount.
     *
     * This method is responsible for updating the payment amount for a sales
     * invoice. It first checks if the provided payment amount is not a decimal,
     * float, or integer value by invoking the `is_numeric()` function. If the
     * payment amount is not a numeric value, it emits an event named 'shouldNumericValue'
     * using the `emit()` method. This event can be used to trigger specific actions
     * in the application to handle the case when the payment amount is not a valid
     * numerical value.
     */
    public function updatedPaymentAmount($value): void
    {
        $this->paymentAmount = floatval($value);

        // check if not decimal, float, or int
        if (! is_numeric($this->paymentAmount)) {
            $this->paymentAmount = 0;
            // Code to handle when paymentAmount is not decimal, float, or int
            $this->emit('shouldNumericValue');
            $this->addError('paymentAmount', 'Payment amount must be numeric');
        }

        $this->paymentAmount = floatval($value);
    }

    public function setSalesInvoice($salesInvoice): void
    {
        $this->salesInvoice = SalesInvoice::find($salesInvoice['id']);
    }

    /**
     * Remove a sales invoice payment.
     *
     * This method removes a payment from the sales invoice by updating the `payments` property
     * to the provided payment data. It then calculates the new `totalPayment` by summing the
     * 'amount' values in the `payments` array using the `collect()` helper. The `diffPayment`
     * property is updated by subtracting the `grandTotal` property value from the `totalPayment`.
     * The method then emits three events to update the payment details for the invoice by calling
     * the `emit()` method with the appropriate event names and data. The 'setTotalPayment' event
     * is emitted with the new `totalPayment` value, the 'setDiffPayment' event is emitted with the
     * new `diffPayment` value, and the 'setPayments' event is emitted with the updated `payments` data.
     *
     * Note: This method assumes that the provided `payments` parameter is a valid array containing
     * payment data.
     *
     * @param  array  $payments  The new payment data for the sales invoice.
     */
    public function removePayment(array $payments): void
    {
        $this->payments = $payments;
        $this->totalPayment = collect($this->payments)->sum('amount');
        $this->diffPayment = $this->totalPayment - $this->grandTotal;

        $this->emit('setTotalPayment', $this->totalPayment);
        $this->emit('setDiffPayment', $this->diffPayment);
        $this->emit('setPayments', $this->payments);
    }

    /**
     * Add a sales invoice payment.
     *
     * This method processes the payment for a sales invoice by checking if
     * the items are empty or if the payment amount is zero. If either of
     * these conditions is met, the method returns without performing any
     * further actions. Otherwise, it proceeds to process the payment by
     * invoking the `processPayment()` method. After processing the payment,
     * the method emits events to update the payment details for the invoice
     * by calling the `emit()` method with the appropriate event names and
     * data. Finally, it clears the payment data by invoking the `clearPayment()`
     * method.
     */
    public function addSalesInvoicePayment(): void
    {
        if ($this->isItemsEmpty() || $this->isZeroPaymentAmount()) {
            return;
        }

        $this->processPayment();

        $this->totalPayment = collect($this->payments)->sum('amount');
        $this->diffPayment = $this->totalPayment - $this->grandTotal;

        $this->emit('setPayments', $this->payments);
        $this->emit('setDiffPayment', $this->diffPayment);
        $this->emit('setTotalPayment', $this->totalPayment);

        $this->clearPayment();
    }

    /**
     * Set the grand total.
     *
     * This method sets the value of the grand total property.
     *
     * @param  float|int  $grandTotal  The grand total amount.
     */
    public function setGrandTotal(float|int $grandTotal): void
    {
        $this->grandTotal = $grandTotal;
    }

    /**
     * Process the payment.
     *
     * This method appends the payment information to an array, calculates the total payment amount,
     * and calculates the difference between the payment amount and the total invoice amount *.
     */
    private function processPayment(): void
    {
        $this->buildPaymentArray();
        $this->payments[] = $this->payment;
    }

    /**
     * Check if the items array is empty.
     *
     * This method checks if the items array is empty and returns true if it is, and false otherwise.
     *
     * @return bool True if the items array is empty, false otherwise.
     */
    private function isItemsEmpty(): bool
    {
        if (empty($this->items)) {
            $this->emit('itemsRequired');

            return true;
        }

        return false;
    }

    /**
     * Checks if the payment amount is zero.
     *
     * This method compares the value of the payment amount property with zero and returns true if they are equal.
     *
     * @return bool True if the payment amount is zero, false otherwise.
     */
    private function isZeroPaymentAmount(): bool
    {
        if ($this->grandTotal !== 0 && $this->paymentAmount === 0) {
            $this->emit('paymentRequired');

            return true;
        }

        return false;
    }

    /**
     * Builds the payment array.
     *
     * This method builds an array representing a payment, using the values of the payment identifier,
     * payment amount, reference number, and payment status from the current object.
     *
     * The array structure is as follows:
     * - payment_id: The identifier of the payment.
     * - amount: The amount of the payment.
     * - reference_number: The reference number associated with the payment.
     * - status: The status of the payment.
     *
     * This method does not perform any validation or data manipulation.
     */
    private function buildPaymentArray(): void
    {
        $this->payment['payment_id'] = $this->paymentId;
        $this->payment['amount'] = $this->paymentAmount;
        $this->payment['reference_number'] = $this->referenceNumber;
        $this->payment['status'] = $this->paymentStatus;
    }

    /**
     * Set the items for the sales invoice.
     *
     * This method sets the items for the sales invoice.
     * It accepts an array of items and assigns it to the $items property.
     *
     * @param  array  $items  The array of items for the sales invoice.
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    /**
     * Adds an error for zero payment amount.
     *
     * This method adds an error to the payment amount field with a message stating that the payment amount cannot be zero.
     */
    public function paymentRequired(): void
    {
        $this->addError('paymentAmount', 'Payment amount cannot be zero');
    }

    /**
     * Clears the payment information.
     *
     * This method clears the payment data by resetting the payment array, payment amount,
     * payment ID, reference number, and payment status to their default values.
     *
     * Note: This method does not perform any type of validation or verification.
     */
    public function clearPayment(): void
    {
        $this->payment = [];
        $this->paymentAmount = 0;
        $this->paymentId = 1;
        $this->referenceNumber = null;
        $this->paymentStatus = 'paid';
    }

    /**
     * Clears the sales invoice payment form.
     *
     * This method resets the sales invoice payment form, clearing all the fields and values.
     */
    public function clearSalesInvoicePaymentForm(): void
    {
        $this->reset();
    }

    /**
     * Reset all error messages.
     *
     * This method resets all the error messages in the error bag.
     * It does
     * this by invoking the `resetErrorBag()` method.
     * This method is useful
     * when you want to clear all the error messages and start with a clean
     * slate.
     */
    public function resetAllError(): void
    {
        $this->resetErrorBag();
    }

    /**
     * Render the sales invoice payment form.
     *
     * This method returns a view or factory or application instance depending on the Laravel framework.
     * It returns a view instance of the 'livewire.sales-invoice-payment-form' view.
     *
     * @return View|Factory|Application The rendered view or factory or application instance.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.sales-invoice-payment-form');
    }
}
