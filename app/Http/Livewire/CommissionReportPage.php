<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class CommissionReportPage extends Component
{
    /**
     * Render the commission report page.
     *
     * @return Factory|View|Application The rendered page.
     */
    public function render(): Factory|View|Application
    {
        // Return the view for the commission report page
        return view('livewire.commission-report-page');
    }
}
