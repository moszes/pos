<?php

namespace App\Http\Livewire;

use App\Models\Subscription;
use App\Models\SubscriptionPlan;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SubscriptionForm extends Component
{
    public $formTitle = 'Add Subscription';

    public $actionForm = 'store';

    public $customer;

    public $code;

    public $subscription;

    public $subscriptionPlan;

    public $buyDate;

    public $activeDate;

    public $expiredDate;

    protected $listeners = [
        'editSubscription',
        'setCustomer',
        'resetCustomer',
        'setSubscriptionCode',
        'setSubscriptionPlan',
        'clearSubscriptionForm',
        'resetSubscriptionPlan',
    ];

    /**
     * Mount the component and set the initial values.
     *
     * This method is called when the component is first mounted.
     * It generates a subscription code using the generateSubscriptionCode() function.
     */
    public function mount(): void
    {
        // Generate a subscription code
        $this->code = generateSubscriptionCode();
        $this->buyDate = date('Y-m-d');
    }

    /**
     * Set the customer for the current instance.
     *
     * @param  mixed  $customer  The customer to be set
     */
    public function setCustomer(mixed $customer): void
    {
        $this->customer = $customer;
    }

    /**
     * Reset the customer property to an empty array.
     */
    public function resetCustomer(): void
    {
        // Set the customer property to an empty array
        $this->customer = [];
    }

    /**
     * Set the subscription plan based on the provided ID.
     *
     * @param  int  $subscriptionPlanId  The ID of the subscription plan.
     */
    public function setSubscriptionPlan(int $subscriptionPlanId): void
    {
        // Find the subscription plan by ID
        $this->subscriptionPlan = SubscriptionPlan::find($subscriptionPlanId);
    }

    /**
     * Reset the subscription plan to null.
     */
    public function resetSubscriptionPlan(): void
    {
        // Set the subscription plan to null
        $this->subscriptionPlan = null;
    }

    /**
     * Returns an array of subscription data.
     *
     * @return array The subscription data.
     */
    public function subscriptionData(): array
    {
        // Get the ID of the first company
        $companyId = auth()->user()->details->branch_id;

        // Get the ID of the first branch
        $branchId = auth()->user()->details->branch_id;

        // Get the ID of the customer
        $customerId = $this->customer['users']['id'];

        // Get the ID of the subscription plan
        $subscriptionPlanId = $this->subscriptionPlan->id;

        // Generate a subscription code
        $code = $this->actionForm == 'update' ? $this->code : generateSubscriptionCode();

        // Get the buy date
        $buyDate = $this->buyDate;

        // Return the subscription data as an array
        return [
            'company_id' => $companyId,
            'branch_id' => $branchId,
            'customer_id' => $customerId,
            'subscription_plan_id' => $subscriptionPlanId,
            'code' => $code,
            'buy_date' => $buyDate,
            'active_date' => $this->activeDate ?? null,
            'status' => $this->activeDate != null ? 'active' : 'inactive',
            'expired_date' => $this->expiredDate ?? null,
        ];
    }

    /**
     * Store a new subscription in the database.
     *
     * This method creates a new subscription using the data provided by the
     * `subscriptionData()` method.
     * It uses a database transaction to ensure
     * data integrity.
     * After the transaction is committed, it emits two events:
     * 'refreshSubscriptionsTable' and 'hideSubscriptionForm'.
     */
    public function store(): void
    {
        if (empty($this->customer)) {
            $this->emit('customerRequired');

            return;
        }
        if (! $this->subscriptionPlan) {
            $this->emit('subscriptionPlanRequired');

            return;
        }
        // Start a database transaction with a maximum of 5 retries
        DB::transaction(function () {
            // Create a new subscription using the subscription data
            Subscription::create($this->subscriptionData());
        }, 5);

        // Emit the 'refreshSubscriptionsTable' event to refresh the subscriptions table
        $this->emit('refreshSubscription');

        // Emit the 'hideSubscriptionForm' event to hide the subscription form
        $this->emit('clearSubscriptionPlanSearch');
        $this->emit('hideSubscriptionForm');
    }

    /**
     * Edit a subscription by its ID.
     *
     * @param  int  $id  The ID of the subscription to edit.
     */
    public function editSubscription(int $id): void
    {
        // Find the subscription by its ID
        $this->subscription = Subscription::find($id);

        // Set the code and buy date of the subscription
        $this->code = $this->subscription->code;
        $this->buyDate = date('Y-m-d', strtotime($this->subscription->buyDate));

        // Get the customer associated with the subscription
        $this->customer = getUserById($this->subscription->customer_id);

        // Set the action form to update
        $this->actionForm = 'update';

        // Emit events to select the customer and subscription plan
        $this->emit('selectCustomer', $this->customer->id);
        $this->emit('selectSubscriptionPlan', $this->subscription->subscription_plan_id);

        // Emit an event to show the subscription form
        $this->emit('showSubscriptionForm');
    }

    /**
     * Update the subscription.
     *
     * This method updates the subscription with the data provided by the `subscriptionData()` method.
     * It wraps the update operation in a database transaction to ensure data integrity.
     * After the transaction is committed, it emits the 'refreshSubscription' event to refresh the subscriptions table.
     * Finally, it emits the 'hideSubscriptionForm' event to hide the subscription form.
     */
    public function update(): void
    {
        // Start a database transaction with a maximum of 5 retries
        DB::transaction(function () {
            // Update the subscription with the subscription data
            $this->subscription->update($this->subscriptionData());
        }, 5);

        // Emit the 'refreshSubscription' event to refresh the subscriptions table
        $this->emit('refreshSubscription');

        // Emit the 'hideSubscriptionForm' event to hide the subscription form
        $this->emit('hideSubscriptionForm');
    }

    /**
     * Clear the subscription form by resetting the form data and error bag.
     */
    public function clearSubscriptionForm(): void
    {
        // Reset the form data
        $this->resetExcept(['code', 'buyDate']);

        // Reset the error bag
        $this->resetErrorBag();

        $this->emit('clearCustomerSearch');
        $this->emit('clearSubscriptionPlanSearch');
    }

    /**
     * Render the subscription form view.
     */
    public function render(): Application|Factory|View
    {
        // Return the subscription form view
        return view('livewire.subscription-form');
    }
}
