<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class ResetPassword extends Component
{
    public $password;

    public $password_confirmation;

    protected $rules = [
        'password' => 'required|confirmed',
    ];

    public function updatePassword(): void
    {
        $this->validate();

        auth()->user()?->update([
            'password' => bcrypt($this->password),
        ]);

        $this->redirect(route('dashboard'));
    }

    /**
     * Render the reset password view.
     */
    public function render(): Factory|View|Application
    {
        // Return the view for the reset password page
        return view('livewire.reset-password');
    }
}
