<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class TurnOverReportPage extends Component
{
    /**
     * Render the turn-over report page view.
     */
    public function render(): Factory|View|Application
    {
        // Return the view for the turn-over report page
        return view('livewire.turn-over-report-page');
    }
}
