<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class CommissionReportTable extends Component
{
    public $titlePage = 'Commission Report';

    public $dateFrom;

    public $dateTo;

    public function mount(): void
    {
        $this->dateFrom = date('Y-m-').'01';
        $this->dateTo = date('Y-m-d');
    }

    /**
     * Render the commission report table view.
     */
    public function render(): Factory|View|Application
    {
        // Render the commission report table view with empty commissions and employees with specific roles
        return view('livewire.commission-report-table', [
            'commissions' => collect(),
            'employees' => User::role([
                'doctor',
                'therapist',
                'sales',
            ])->get(),
        ]);
    }
}
