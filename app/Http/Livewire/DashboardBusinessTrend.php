<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoice;
use App\Models\User;
use Illuminate\View\View;
use Livewire\Component;
use Spatie\Permission\Models\Role;

class DashboardBusinessTrend extends Component
{
    public $selectedMonth;

    protected $listeners = [
        'setSelectedMonth',
    ];

    /**
     * Set the selected month.
     *
     * @param  mixed  $value  The value to set as the selected month.
     */
    public function setSelectedMonth(mixed $value): void
    {
        $this->selectedMonth = $value;
    }

    /**
     * Render the component.
     */
    public function render(): View
    {
        return view('livewire.dashboard-business-trend', [
            'salesInvoices' => SalesInvoice::where('sales_invoice_status', 'valid')
                ->whereYear('sales_invoice_date', date('Y', strtotime($this->selectedMonth)))
                ->whereMonth('sales_invoice_date', date('m', strtotime($this->selectedMonth)))
                ->count(),
            'customers' => $this->getCountCustomer(),
        ]);
    }

    public function getCountCustomer(): int
    {
        $customerCount = 0;

        if (Role::where('name', 'customer')->first()) {
            $customerCount = User::role('customer')->count();
        }

        return $customerCount;
    }
}
