<?php

namespace App\Http\Livewire;

use App\Models\Commission;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class CommissionReportDetailTable extends Component
{
    use WithPagination;

    public $titlePage = 'Commission Report Detail';

    public $dateFrom;

    public $dateTo;

    public $staff;

    /**
     * Mount the component and set the staff property based on the staff ID from the request segment.
     */
    public function mount(): void
    {
        // Get the staff ID from the last segment of the request URI
        $staffId = request()->segment(count(request()->segments()));

        // Find the user with the given staff ID and set it as the staff property
        $this->staff = User::find($staffId);

        $this->dateFrom = date('Y-m-').'01';
        $this->dateTo = date('Y-m-d');
    }

    /**
     * Get the pagination view.
     *
     * This function returns the name of the pagination view to be used.
     *
     * @return string The name of the pagination view.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Render the commission report detail table view.
     */
    public function render(): Factory|View|Application
    {
        // Render the commission report detail table view
        return view('livewire.commission-report-detail-table', [
            'staff' => $this->staff,
            'commissions' => Commission::where('user_id', $this->staff->id)
                ->whereBetween('sales_invoice_date', [$this->dateFrom, $this->dateTo])
                ->paginate(10),
        ]);
    }
}
