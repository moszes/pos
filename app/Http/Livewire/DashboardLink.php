<?php

namespace App\Http\Livewire;

use App\Models\ApplicationSetting;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class DashboardLink extends Component
{
    public $treatmentModule;

    public $packageModule;

    public $subscriptionModule;

    public $marketPlaceModule;

    public function mount(): void
    {
        $this->treatmentModule = ApplicationSetting::where('name', 'Application Module')
            ->where('module', 'treatment')
            ->first()?->status == 'Y';

        $this->packageModule = ApplicationSetting::where('name', 'Application Module')
            ->where('module', 'package')
            ->first()?->status == 'Y';

        $this->subscriptionModule = ApplicationSetting::where('name', 'Application Module')
            ->where('module', 'subscription')
            ->first()?->status == 'Y';

        $this->marketPlaceModule = ApplicationSetting::where('name', 'Application Module')
            ->where('module', 'marketplace')
            ->first()?->status == 'Y';

    }

    /**
     * Render the view for the home link.
     */
    public function render(): View|Factory|Application
    {
        // Return the view for the home link
        return view('livewire.dashboard-link');
    }
}
