<?php

namespace App\Http\Livewire;

use App\Models\Appointment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class AppointmentSearch extends Component
{
    public $appointmentSearch;

    public $appointments;

    public $appointmentLists = false;

    public $appointment;

    protected $listeners = [
        'clearAppointmentSearch',
        'selectAppointment',
        'setAppointmentSearch',
    ];

    public function updatedAppointmentSearch($value): void
    {
        $this->appointmentLists = true;

        if (! $value || $value !== '') {

            $this->appointments = Appointment::where('company_id', auth()->user()->details->branch_id)
                ->where('appointment_number', 'like', '%'.$this->appointmentSearch.'%')
                ->whereBetween('appointment_date', [date('Y-m-d').' 00:00:00', date('Y-m-d').' 23:59:59'])
                ->whereNull('sales_invoice_number')
                ->orderBy('id', 'desc')
                ->get();
        }
    }

    public function selectAppointment($id): void
    {
        $this->appointment = Appointment::find($id);

        $this->appointmentSearch = $this->appointment->appointment_number;

        $this->emit('updateAppointmentOnSalesInvoice', $this->appointment);
        $this->emit('selectCustomer', $this->appointment->customer_id);
    }

    /**
     * Clears the appointment search.
     */
    public function clearAppointmentSearch(): void
    {
        $this->resetExcept('appointmentSearch');
    }

    /**
     * Render the appointment search view.
     */
    public function render(): View|Factory|Application
    {
        // Return the appointment search view
        return view('livewire.appointment-search');
    }
}
