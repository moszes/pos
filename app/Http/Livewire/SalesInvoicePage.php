<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SalesInvoicePage extends Component
{
    /**
     * Render the sales invoice page.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.sales-invoice-page');
    }
}
