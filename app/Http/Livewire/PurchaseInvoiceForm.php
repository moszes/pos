<?php

namespace App\Http\Livewire;

use App\Models\PurchaseInvoice;
use App\Models\PurchaseInvoiceItem;
use App\Models\PurchaseOrder;
use App\Models\Vendor;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PurchaseInvoiceForm extends Component
{
    public $formTitle = 'Purchase Invoice';

    public $actionForm = 'store';

    public $vendor;

    public $tax;

    public $discount;

    public $others;

    public $note;

    public $date;

    public $purchaseOrder;

    public $purchaseInvoiceNumber;

    public $product;

    public $subTotal = 0;

    public $grandTotal = 0;

    public $purchaseInvoice;

    public $items = [];

    protected $listeners = [
        'clearPurchaseInvoiceForm',
        'setItemsFormPurchaseOrder',
        'deletePurchaseInvoice',
        'purchaseOrderSelected',
        'editPurchaseInvoice',
        'updateGrandTotal',
        'addProductItem',
        'setSubTotal',
        'setVendor',
    ];

    protected $rules = [
        'invoice_number' => 'required|unique:purchase_invoices',
    ];

    /**
     * Mount the component.
     */
    public function mount(): void
    {
        // Generate purchase invoice number
        $this->purchaseInvoiceNumber = generatePurchaseInvoiceNumber();

        // Set the current date
        $this->date = now()->format('Y-m-d');

        // Initialize an empty collection for items
        $this->items = collect();
    }

    /**
     * Update the grand total when certain fields are updated.
     *
     * @param  string  $name  The field name.
     */
    public function updated(string $name): void
    {
        // Check if the field name is 'discount', 'others', or 'tax'
        if ($name == 'discount' || $name == 'others' || $name == 'tax') {
            // Emit an event to update the grand total
            $this->emit('updateGrandTotal');
        }
    }

    /**
     * Calculate and set the subtotal of the items.
     *
     * This function sums up the total price of all the items and sets it as the subtotal.
     */
    public function setSubTotal(): void
    {
        // Calculate the subtotal by summing up the total price of all the items
        $this->subTotal = collect($this->items)->sum('total_price');
    }

    /**
     * Update the grand total.
     *
     * Resets the grand total to zero and calculates the new grand total by summing the total price of all items,
     * adding the tax and others, and subtracting the discount.
     */
    public function updateGrandTotal(): void
    {
        $this->items = collect($this->items);

        // Calculate the new grand total
        $this->grandTotal = $this->subTotal + $this->tax + $this->others - $this->discount;
    }

    /**
     * Set the items for the purchase order.
     *
     * @param  array  $items  The items to set.
     */
    public function setItemsFormPurchaseOrder(array $items): void
    {
        // Set the items property to the provided items array.
        $this->items = $items;
    }

    /**
     * Adds a purchase order item.
     *
     * @param  mixed  $item  The item to add.
     */
    public function addProductItem(mixed $item): void
    {
        $this->items = collect($this->items);
        // Add the item to the item's collection.
        $this->items->push($item);

        // Update the grand total.
        $this->updateGrandTotal();

        // Set the product property to the added item's product.
        $this->product = $item['product'];
    }

    /**
     * Set the selected purchase order.
     *
     * @param  array  $purchaseOrder  The purchase order data.
     */
    public function purchaseOrderSelected(array $purchaseOrder): void
    {
        // Set the selected purchase order
        $this->purchaseOrder = collect($purchaseOrder);
    }

    /**
     * Set the vendor for the current instance.
     *
     * @param  int  $id  The ID of the vendor.
     */
    public function setVendor(int $id): void
    {
        // Find the vendor with the given ID
        $this->vendor = Vendor::find($id);
    }

    /**
     * Remove an item from the items array.
     *
     * This function removes an item from the items array based on the array key provided.
     * It then emits two events, 'updateSubTotal' and 'updateGrandTotal', to notify the necessary
     * parts that they need to update their respective totals.
     *
     * @param  mixed  $arrayKey  The array key of the item to be removed.
     */
    public function removeItem(mixed $arrayKey): void
    {
        $this->items = collect($this->items);
        $this->items->forget($arrayKey);

        $this->emit('updateSubTotal');
        $this->emit('updateGrandTotal');
    }

    /**
     * Fetches the data for creating a purchase invoice.
     *
     * This function returns an array containing the necessary data for creating a purchase invoice in the application.
     *
     * @return array The purchase invoice data.
     */
    public function purchaseInvoiceData(): array
    {
        return [
            'company_id' => auth()->user()->details->branch_id,
            'branch_id' => auth()->user()->details->branch_id,
            'vendor_id' => @$this->vendor->id,
            'purchase_order_id' => @$this->purchaseOrder['id'],
            'user_id' => auth()->user()?->id,
            'invoice_number' => $this->purchaseInvoiceNumber,
            'discount' => countFormat($this->discount) ?? 0,
            'tax' => countFormat($this->tax) ?? 0,
            'others' => countFormat($this->others) ?? 0,
            'grand_total' => countFormat($this->grandTotal),
            'date' => $this->date ?? now()->format('Y-m-d H:i:s'),
            'approved_by' => null,
            'approved_date' => null,
            'note' => $this->note,
        ];
    }

    public function updateItems(): void
    {
        PurchaseInvoiceItem::where('purchase_invoice_id', $this->purchaseInvoice->id)->delete();

        foreach ($this->items as $purchaseInvoiceItem) {
            $purchaseInvoiceItem['purchase_invoice_id'] = $this->purchaseInvoice->id;
            $purchaseInvoiceItem['product_id'] = ! is_array($purchaseInvoiceItem['product']) ? $purchaseInvoiceItem : $purchaseInvoiceItem['product']['id'];
            $purchaseInvoiceItem['total_price'] = $purchaseInvoiceItem['price'] * $purchaseInvoiceItem['qty'] - $purchaseInvoiceItem['discount_item'];

            PurchaseInvoiceItem::create(is_array($purchaseInvoiceItem) ? $purchaseInvoiceItem : $purchaseInvoiceItem->toArray());
        }

        $this->emit('setSubtotal');
        $this->emit('setGrandTotal');

        $this->purchaseInvoice->update([
            'grand_total' => $this->grandTotal,
        ]);
    }

    public function store(): void
    {
        $this->purchaseInvoice = PurchaseInvoice::create($this->purchaseInvoiceData());

        $this->updateItems();

        $this->clearPurchaseInvoiceForm();

        $this->emit('dataSaved');
        $this->emit('refreshPurchaseInvoice');
        $this->emit('resetPurchaseOrderSearch');
    }

    public function editPurchaseInvoice($id): void
    {
        $this->purchaseInvoice = PurchaseInvoice::find($id);

        if ($this->purchaseInvoice->purchase_order_id) {
            $this->purchaseOrder = PurchaseOrder::find($this->purchaseInvoice->purchase_order_id);
            $this->emit('setPurchaseOrderNumber', $this->purchaseOrder->order_number);
        }

        if ($this->purchaseInvoice->vendor_id) {
            $this->vendor = Vendor::find($this->purchaseInvoice->vendor_id);
            $this->emit('selectVendorById', $this->vendor->id);
        }

        $this->purchaseInvoiceNumber = $this->purchaseInvoice->invoice_number;
        $this->note = $this->purchaseInvoice->note;
        $this->discount = numberFormat($this->purchaseInvoice->discount);
        $this->tax = numberFormat($this->purchaseInvoice->tax);
        $this->others = numberFormat($this->purchaseInvoice->others);
        $this->grandTotal = numberFormat($this->purchaseInvoice->grand_total);
        $this->date = dateFormat($this->purchaseInvoice->date);

        foreach ($this->purchaseInvoice->items as $purchaseInvoiceItem) {
            $product = getProductById($purchaseInvoiceItem->product_id);

            $item['purchase_invoice_id'] = $purchaseInvoiceItem->purchase_invoice_id;
            $item['product_id'] = $purchaseInvoiceItem->product_id;
            $item['itemCode'] = @$product->code;
            $item['itemName'] = @$product->name;
            $item['size'] = @$product->size;
            $item['qty'] = numberFormat($purchaseInvoiceItem->qty);
            $item['discount_item'] = numberFormat($purchaseInvoiceItem->discount_item);
            $item['price'] = numberFormat($purchaseInvoiceItem->price);
            $item['last_price'] = numberFormat($product->cogs);
            $item['total_price'] = numberFormat($purchaseInvoiceItem->total_price);
            $item['unit'] = @numberFormat($product->size).' '.@$product->unitOfMeasure->name;

            $this->items->push($item);
        }

        $this->actionForm = 'update';
        $this->emit('showPurchaseInvoiceForm');
    }

    public function update(): void
    {
        $purchaseInvoiceData = $this->purchaseInvoiceData();

        $this->purchaseInvoice->update($purchaseInvoiceData);

        PurchaseInvoiceItem::where('purchase_invoice_id', $this->purchaseInvoice->id)->delete();

        $this->updateItems();

        $this->emit('clearPurchaseInvoiceForm');
        $this->emit('dataUpdated');
        $this->emit('refreshPurchaseInvoice');
    }

    public function deletePurchaseInvoice($id): void
    {
        $this->purchaseInvoice = PurchaseInvoice::find($id);

        if (! empty($this->purchaseInvoice->items)) {
            $this->purchaseInvoice->items()->delete();
        }

        $this->purchaseInvoice->delete();

        $this->emit('clearPurchaseInvoiceForm');
        $this->emit('dataDeleted');
        $this->emit('refreshPurchaseInvoice');
    }

    public function clearPurchaseInvoiceForm(): void
    {
        $this->reset();

        $this->purchaseInvoiceNumber = generatePurchaseInvoiceNumber();
        $this->date = now()->format('Y-m-d');
        $this->items = collect();

        $this->emit('resetPurchaseOrderSearch');
        $this->emit('resetProductSearch');
        $this->emit('hidePurchaseInvoiceForm');
    }

    public function render(): Factory|View|Application
    {
        return view('livewire.purchase-invoice-form');
    }
}
