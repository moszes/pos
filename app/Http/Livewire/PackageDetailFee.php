<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class PackageDetailFee extends Component
{
    public $package;

    public function render(): View
    {
        return view('livewire.package-detail-fee');
    }
}
