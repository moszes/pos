<?php

namespace App\Http\Livewire;

use App\Mail\CompanyRegisteredMail;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Http;
use Livewire\Component;

class companyRegisterForm extends Component
{
    public $email;

    public $name;

    public $phone;

    public function updated(): void
    {
        $this->resetErrorBag();
    }

    /**
     * Create a new company.
     */
    public function createCompany(): void
    {
        try {
            $url = env('APP_ENV') == 'local' ? 'http://localhost:8001' : env('APP_URL');

            $response = Http::post($url.'/api/companies', [
                'email' => $this->email,
                'name' => $this->name,
                'phone' => $this->phone,
            ]);

            // handle the response here based upon the status code and content
            if (! $response->json()) {
                $this->addError('errorMessage', 'Silahkan isi dengan benar');
            }

            $rawData = $this->email.$this->name.$this->phone;
            $token = hash('sha256', $rawData);

            // email to customer
            \Mail::to($this->email)->send(new CompanyRegisteredMail($token));

            $this->redirect(route('company_data'));

        } catch (RequestException $exception) {
            // handle the exception if the request fails
            $this->addError('errorMessage', $exception->getMessage());
        }

    }

    /**
     * Render the register company form.
     *
     * @return Factory|View|Application The rendered view or layout.
     */
    public function render(): Factory|View|Application
    {
        // Return the view for the register company form
        return view('livewire.company-register-form');
    }
}
