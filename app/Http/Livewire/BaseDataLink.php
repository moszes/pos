<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class BaseDataLink extends Component
{
    /**
     * Renders the Livewire component.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.base-data-link');
    }
}
