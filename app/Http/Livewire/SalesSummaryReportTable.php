<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoice;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SalesSummaryReportTable extends Component
{
    public $dateFrom;

    public $dateTo;

    public $customer;

    public $salesSummaries;

    protected $listeners = [
        'setReportSummaryParams',
        'refreshSalesSummaries',
    ];

    /**
     * Refreshes the sales summaries.
     */
    public function refreshSalesSummaries($dateFrom, $dateTo, $customer): void
    {
        $this->dateTo = $dateTo;
        $this->dateFrom = $dateFrom;
        $this->customer = $customer;
        // Query sales invoices based on date range
        $this->salesSummaries = SalesInvoice::where('sales_invoices.sales_invoice_date', '>=', dateFormat($this->dateFrom))
            ->where('sales_invoices.sales_invoice_date', '<=', dateFormat($this->dateTo))
            ->whereNull('sales_invoices.deleted_at');

        // Filter sales invoices by customer if provided
        if ($customer) {
            $this->salesSummaries = $this->salesSummaries->where('sales_invoices.customer_id', '=', $this->customer['id']);
        }
        $this->salesSummaries = $this->salesSummaries->where('sales_invoices.sales_invoice_status', 'valid')
            ->get();
    }

    /**
     * Set the initial values for the dateFrom and dateTo properties.
     */
    public function mount(): void
    {
        // Set the dateFrom property to the current date and time.
        $this->dateFrom = now();

        // Set the dateTo property to the current date and time.
        $this->dateTo = now();
    }

    public function setReportSummaryParams($dateForm, $dateTo, $customer): void
    {
        $this->dateFrom = $dateForm;
        $this->dateTo = $dateTo;
        $this->customer = $customer;
    }

    /**
     * Downloads the sales summaries as an Excel file.
     */
    public function download(): void
    {
        // Generate the URL for generating the Excel file
        $url = 'generate_excel?salesSummaries=true&dateFrom='.dateFormat($this->dateFrom).'&dateTo='.dateFormat($this->dateTo);

        // Add the customer parameter to the URL if provided
        if ($this->customer) {
            $url .= '&customer='.$this->customer;
        }

        // Redirect the user to the generated Excel file
        redirect($url);
    }

    /**
     * Render the sales summary report table.
     */
    public function render(): Factory|View|Application
    {
        // Return the view with the sales summaries
        return view('livewire.sales-summary-report-table');
    }
}
