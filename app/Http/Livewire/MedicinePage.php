<?php

namespace App\Http\Livewire;

use Livewire\Component;

class MedicinePage extends Component
{
    public function render()
    {
        return view('livewire.medicine-page');
    }
}
