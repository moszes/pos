<?php

namespace App\Http\Livewire;

use App\Models\ApplicationSetting;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderItem;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PurchaseOrderItemModal extends Component
{
    public $formTitle = 'Purchase Order Details';

    public $purchaseOrder;

    public $purchaseOrderItems;

    public $approvalPurchaseOrder;

    public $totalPrice = 0;

    public $subTotal = 0;

    public $grandTotal = 0;

    protected $listeners = [
        'showPurchaseOrderItems',
        'clearPurchaseOrderItems',
    ];

    /**
     * Mounts the component.
     */
    public function mount(): void
    {
        // Get the status of the 'Approval Purchase Order' setting for the company
        $status = ApplicationSetting::where('name', 'Approval Purchase Order')
            ->where('company_id', auth()->user()->details->branch_id)
            ->first()->status;

        // Set the approvalPurchaseOrder property based on the status
        $this->approvalPurchaseOrder = $status == 'Y';
    }

    /**
     * Approves a purchase order.
     *
     * @param  int  $id  The ID of the purchase order.
     */
    public function approve(int $id): void
    {
        $this->purchaseOrder = PurchaseOrder::find($id);

        $this->purchaseOrder->update([
            'approved_date' => date('Y-m-d H:i:s'),
            'approved_by' => auth()->user()?->id,
        ]);

        $this->emit('approved');
        $this->emit('refreshPurchaseOrder');
        $this->emit('hidePurchaseOrderItems');
    }

    /**
     * Show the purchase order items.
     *
     * @param  int  $id  The ID of the purchase order.
     */
    public function showPurchaseOrderItems(int $id): void
    {
        $this->purchaseOrder = PurchaseOrder::find($id);
        $this->purchaseOrderItems = PurchaseOrderItem::where('purchase_order_id', $id)->get();

        $this->totalPrice = PurchaseOrderItem::where('purchase_order_id', $id)->sum('total_price');
    }

    /**
     * Clears the purchase order items.
     */
    public function clearPurchaseOrderItems(): void
    {
        $this->reset();
    }

    /**
     * Renders the purchase order item modal.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.purchase-order-item-modal');
    }
}
