<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;
use Livewire\WithPagination;
use Spatie\Permission\Models\Role;

class RoleTable extends Component
{
    use WithPagination;

    public $titlePage = 'Role';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshRole' => '$refresh',
    ];

    /**
     * Reset the page number when the search field is updated.
     */
    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    /**
     * Returns the view for pagination.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Apply search filter on roles and return paginated results.
     */
    public function searchFilter(): array|LengthAwarePaginator
    {
        return Role::where('name', 'like', '%'.$this->search.'%')
            ->where('name', '!=', 'godadmin')
            ->orderBy('id', 'desc')
            ->paginate(10);
    }

    /**
     * Render the view for the role table.
     */
    public function render(): View|Factory|Application
    {

        // Return the view 'livewire.role-table' with the roles' data.
        return view('livewire.role-table', [
            'roles' => self::searchFilter(),
        ]);
    }
}
