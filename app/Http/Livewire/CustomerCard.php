<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class CustomerCard extends Component
{
    public $user;

    public $customerId;

    protected $queryString = [
        'customerId',
    ];

    /**
     * Mounts the component.
     */
    public function mount(): void
    {
        $this->user = User::find($this->customerId);
    }

    /**
     * Renders the customer card view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.customer-card');
    }
}
