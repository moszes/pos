<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SalesSummaryReportForm extends Component
{
    public $dateFrom;

    public $dateTo;

    public $customer;

    public $customerSearch;

    public $customerLists;

    public $customerSearching = false;

    protected $listeners = [
        'refreshSalesSummaryReport',
        'getSalesSummaryReport',
    ];

    /**
     * Update the customer search results.
     */
    public function updatedCustomerSearch(): void
    {
        // Reset customer and customer lists if search is empty or false
        if ($this->customerSearch == '' || ! $this->customerSearch) {
            $this->customer = null;
            $this->customerLists = null;
            $this->customerSearching = false;
        } else {
            // Set customer searching flag to true
            $this->customerSearching = true;

            // Get customer lists based on search query
            $this->customerLists = User::role('customer')
                ->join('user_details', 'user_details.user_id', '=', 'users.id')
                ->where('user_details.first_name', 'like', '%'.$this->customerSearch.'%')
                ->get();
        }
    }

    /**
     * Select a customer by their ID and update the customer details.
     *
     * @param  array  $customer  The customer details.
     */
    public function selectCustomer(array $customer): void
    {
        // Find the customer by their ID
        $this->customer = User::find($customer['user_id']);

        // Reset customer searching flag and customer lists
        $this->customerSearching = false;
        $this->customerLists = null;

        // Set the customer search string using the first name and phone number
        $this->customerSearch = $customer['first_name'].' '.$customer['phone'];
    }

    /**
     * Refreshes the sales summary report by resetting the data and hiding the report table.
     */
    public function refreshSalesSummaryReport(): void
    {
        // Reset the data
        $this->reset();

        // Hide the sales summary report table
        $this->emit('hideSalesSummaryReportTable');
    }

    /**
     * Generate and display the sales summary report.
     */
    public function getSalesSummaryReport(): void
    {
        $this->emit('hideSalesSummaryReportTable');

        // Check if both dateFrom and dateTo are not empty
        if (! $this->dateFrom && ! $this->dateTo) {
            // Emit an event to notify that dateFrom and dateTo are required
            $this->emit('dateFromAndDateToRequired');

            return;
        }
        // Emit an event to show the sales summary report table
        $this->emit('showSalesSummaryReportTable', $this->dateFrom, $this->dateTo, $this->customer);
        $this->emit('setReportSummaryParams', $this->dateFrom, $this->dateTo, $this->customer);
    }

    /**
     * Render the sales summary report form.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the view for the sales summary report form
        return view('livewire.sales-summary-report-form');
    }
}
