<?php

namespace App\Http\Livewire;

use Livewire\Component;

class FaqLink extends Component
{
    public function render()
    {
        return view('livewire.faq-link');
    }
}
