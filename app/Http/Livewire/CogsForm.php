<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CogsForm extends Component
{
    public function render()
    {
        return view('livewire.cogs-form');
    }
}
