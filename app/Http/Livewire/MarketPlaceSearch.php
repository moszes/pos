<?php

namespace App\Http\Livewire;

use App\Models\MarketPlace;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class MarketPlaceSearch extends Component
{
    public $marketplace = [];

    public $marketPlaces;

    public $marketPlaceSearch;

    public $marketPlaceList = false;

    public $marketPlaceQty = 1;

    public $marketPlaceDiscountItem = 0;

    public $marketPlacePrice = 0;

    protected $listeners = [
        'clearMarketPlaceSearch',
        'marketPlaceRequired',
    ];

    /**
     * Handle the updated event for a specific property.
     *
     * This function is triggered when a property is updated.
     * It emits events to clear the person in charge and the room search,
     * resets the error bag,
     * and updates the marketplace array if the updated property is marketPlaceQty,
     * marketPlaceDiscountItem, or marketPlacePrice.
     *
     * @param  string  $name  The name of the property that was updated.
     * @param  mixed  $value  The new value of the property.
     */
    public function updated(string $name, mixed $value): void
    {
        // Emit an event to clear the person in charge
        $this->emit('clearPersonInCharge');

        // Emit an event to clear the room search
        $this->emit('clearRoomSearch');

        // Reset the error bag
        $this->resetErrorBag();

        // If the updated property is the marketPlaceDiscountItem, cast the value to float
        if ($name == 'marketPlaceDiscountItem') {
            $this->marketPlaceDiscountItem = floatval($value);
        }

        // Check if the updated property is marketPlaceQty, marketPlaceDiscountItem, or marketPlacePrice
        if ($name === 'marketPlaceQty' || $name === 'marketPlaceDiscountItem' || $name === 'marketPlacePrice') {
            // Update the marketplace array with the new values
            $this->marketplace['qty'] = $this->marketPlaceQty;
            $this->marketplace['discount_item'] = $this->marketPlaceDiscountItem;
            $this->marketplace['price'] = $this->marketPlacePrice;

            // Emit the marketPlaceSelected event with the updated marketplace array
            $this->emit('marketPlaceSelected', $this->marketplace);
        }
    }

    /**
     * Handle the updated event for the marketPlaceDiscountItem property.
     */
    public function updatedMarketPlaceDiscountItem(): void
    {
        // Check if the marketPlaceDiscountItem is not a number
        if (! is_numeric($this->marketPlaceDiscountItem)) {
            // Reset marketPlaceDiscountItem to 0
            $this->marketPlaceDiscountItem = 0;

            // Emit 'shouldNumericValue' event
            $this->emit('shouldNumericValue');

            // Add an error message
            $this->addError('marketPlaceDiscountItem', 'The discount must be a number.');
        }

        // Check if the marketPlaceDiscountItem is greater than 0
        if ($this->marketPlaceDiscountItem > 0) {
            // Emit 'applyDiscount' event
            $this->emit('applyDiscount');
        }
    }

    /**
     * Update the marketplace search and display the related marketplaces.
     *
     * This function is called whenever the marketPlaceSearch property is updated.
     * It sets the marketplace property to null and the marketPlaceList property to false.
     * If the marketPlaceSearch is not empty, it sets the marketPlaceList property to true
     * and calls the findMarkets method to find related marketplaces based on the search string.
     * The found marketplaces are then assigned to the marketPlaces property.
     */
    public function updatedMarketPlaceSearch(): void
    {
        // Reset the marketPlaces and marketPlaceList properties
        $this->marketPlaces = null;
        $this->marketPlaceList = false;

        // Check if the marketPlaceSearch is not empty
        if ($this->marketPlaceSearch !== '') {
            // Set the marketPlaceList property to true
            $this->marketPlaceList = true;

            // Find related marketplaces based on the marketPlaceSearch string
            $this->marketPlaces = MarketPlace::where('code', 'like', '%'.$this->marketPlaceSearch.'%')
                ->orWhere('name', 'like', '%'.$this->marketPlaceSearch.'%')
                ->get();
        }
    }

    /**
     * Adds an error message if a marketplace is not selected.
     */
    public function marketPlaceRequired(): void
    {
        // Check if a marketplace is not selected
        if (empty($this->marketplace)) {
            // Add an error message to the 'marketPlaceSearch' field
            $this->addError('marketPlaceSearch', 'Please select a marketplace.');
        }
    }

    /**
     * Handle the updated event for the marketPlaceQty property.
     *
     * This function checks if the marketPlaceQty is a numeric value.
     * If it's not,
     * it sets the marketPlaceQty to 1, emits an event to notify the user, and adds
     * an error message to the error bag.
     */
    public function updatedMarketPlaceQty(): void
    {
        // Check if the marketPlaceQty is a numeric value
        if (! is_numeric($this->marketPlaceQty)) {
            // Set the marketPlaceQty to 1
            $this->marketPlaceQty = 1;

            // Emit an event to notify the user
            $this->emit('shouldNumericValue');

            // Add an error message to the error bag
            $this->addError('marketPlaceQty', 'The quantity must be a number.');
        }
    }

    /**
     * Prepare the data of a marketplace based on the given ID.
     */
    private function constructMarketPlaceData(): array
    {
        // Calculate the total price
        $price = floatval($this->marketplace['price']);
        $qty = floatval($this->marketPlaceQty);
        $discount_item = floatval($this->marketPlaceDiscountItem);
        $total_price = $price * $qty - $discount_item;

        // Return the marketplace data
        return [
            'qty' => $qty,
            'price' => $price,
            'discount_item' => $discount_item,
            'total_price' => $total_price,
            'transaction_status' => 'buy',
            'module' => 'marketplace',
        ];
    }

    /**
     * Selects a marketplace based on the provided ID.
     *
     * @param  int  $id  The ID of the marketplace to select.
     */
    public function selectMarketplace(int $id): void
    {
        $this->marketplace = MarketPlace::find($id)->toArray();

        $this->marketPlaceSearch = $this->marketplace['code'].', '.$this->marketplace['name'].', '.countFormat($this->marketplace['price']);
        $this->marketPlacePrice = floatval($this->marketplace['price']);

        $this->marketplace = array_merge($this->marketplace, $this->constructMarketPlaceData());
        $this->marketplace['module_id'] = $id;

        $this->marketPlaceList = false;
        $this->marketPlaces = null;

        $this->emit('marketPlaceSelected', $this->marketplace);
        $this->emit('setModule', 'marketplace');
    }

    /**
     * Clears the marketplace search.
     *
     * Resets the search results and error bag.
     */
    public function clearMarketPlaceSearch(): void
    {
        $this->reset();
        $this->resetErrorBag();
    }

    /**
     * Render the marketplace search view.
     */
    public function render(): View|Factory|Application
    {
        // Return the marketplace search view
        return view('livewire.market-place-search');
    }
}
