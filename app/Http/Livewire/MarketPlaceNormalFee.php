<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class MarketPlaceNormalFee extends Component
{
    public $marketPlaceNormalFee = [];

    public $marketPlace;

    /**
     * Mount the component and initialize fee values.
     *
     * If the marketplace exists, update the fee values based on the marketplace's normal fees.
     */
    public function mount(): void
    {
        // Initialize the fee values
        $this->marketPlaceNormalFee = [
            'senior_doctor' => 0, // Fee for the doctor
            'doctor' => 0, // Fee for the doctor
            'doctor_assistance' => 0, // Fee for the doctor's assistance
            'sales' => 0, // Fee for sales
            'therapist' => 0, // Fee for the therapist
        ];

        // If the marketplace exists, update the fee values
        if ($this->marketplace) {
            foreach ($this->marketplace->normalFees as $value) {
                // Update the fee value based on the role
                $this->marketPlaceNormalFee[$value->role] = countFormat($value->value);
            }
        }
    }

    /**
     * Update the fee values based on the property name and value.
     *
     * @param  string  $propertyName  The name of the property that was updated.
     * @param  mixed  $value  The new value for the property.
     */
    public function updated(string $propertyName, mixed $value): void
    {
        // Check if the property name is one of the fee properties
        if (in_array($propertyName, [
            'marketPlaceNormalFee.senior_doctor',
            'marketPlaceNormalFee.doctor',
            'marketPlaceNormalFee.doctor_assistance',
            'marketPlaceNormalFee.sales',
            'marketPlaceNormalFee.therapist',
        ])) {
            // Remove the 'marketPlaceNormalFee.' prefix from the property name
            $marketPlaceNormalFeeName = str_replace('marketPlaceNormalFee.', '', $propertyName);

            // Convert the value to a float and update the marketPlaceNormalFee property
            $this->marketPlaceNormalFee[$marketPlaceNormalFeeName] = floatval($value);

            $this->emit('feeUpdated', 'marketplace', 'normal', $this->marketPlaceNormalFee);
        }
    }

    /**
     * Render the view for the MarketPlaceNormalFee Livewire component.
     *
     * This method returns the view for the MarketPlaceNormalFee Livewire component, which is 'livewire.marketplace-normal-fee'.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the livewire.marketplace-normal-fee view
        return view('livewire.market-place-normal-fee');
    }
}
