<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SubscriptionSearch extends Component
{
    public $subscriptionList;

    public function render(): View|Factory|Application
    {
        return view('livewire.subscription-search');
    }
}
