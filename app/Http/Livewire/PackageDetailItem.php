<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PackageDetailItem extends Component
{
    public $package;

    public function render(): Factory|View|Application
    {
        return view('livewire.package-detail-item');
    }
}
