<?php

namespace App\Http\Livewire;

use App\Models\ApplicationSetting;
use Illuminate\View\View;
use Livewire\Component;

class SettingBranchTable extends Component
{
    /**
     * Get branch setting
     */
    public function getBranchSetting(): array
    {
        return ApplicationSetting::where('module', 'branch')
            ->get()->toArray();
    }

    public function render(): View
    {
        return view('livewire.setting-branch-table', [
            'branch_settings' => self::getBranchSetting(),
        ]);
    }
}
