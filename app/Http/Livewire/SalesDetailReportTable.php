<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoice;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SalesDetailReportTable extends Component
{
    public $dateFrom;

    public $dateTo;

    public $customer;

    public $salesDetails;

    protected $listeners = [
        'setReportDetailParams',
        'refreshSalesDetails',
    ];

    /**
     * Refreshes the sales summaries.
     */
    public function refreshSalesDetails($dateFrom, $dateTo, $customer): void
    {
        $this->dateTo = $dateTo;
        $this->dateFrom = $dateFrom;
        $this->customer = $customer;

        // Query sales invoices based on date range
        $this->salesDetails = SalesInvoice::whereBetween('sales_invoices.sales_invoice_date', [
            $this->dateFrom.' 00:00:00', $this->dateTo.' 23:59:59',
        ]);

        // Filter sales invoices by customer if provided
        if ($customer) {
            $this->salesDetails = $this->salesDetails->where(
                'sales_invoices.customer_id',
                '=',
                $this->customer['id']
            );
        }

        $this->salesDetails = $this->salesDetails->where(
            'sales_invoices.sales_invoice_status',
            'valid'
        )
            ->get();
    }

    /**
     * Set the initial values for the dateFrom and dateTo properties.
     */
    public function mount(): void
    {
        // Set the dateFrom property to the current date and time.
        $this->dateFrom = now();

        // Set the dateTo property to the current date and time.
        $this->dateTo = now();
    }

    /**
     * Set the report detail parameters.
     *
     * @param  string  $dateFrom  The end date for the report.
     * @param  string  $dateTo  The end date for the report.
     * @param  array  $customer  The customer details for the report.
     */
    public function setReportDetailParams(string $dateFrom, string $dateTo, array $customer): void
    {
        // Set the dateFrom property to the provided start date.
        $this->dateFrom = $dateFrom;

        // Set the dateTo property to the provided end date.
        $this->dateTo = $dateTo;

        // Set the customer property to the provided customer details.
        $this->customer = $customer;
    }

    /**
     * Downloads the sales summaries as an Excel file.
     */
    public function download(): void
    {
        // Generate the URL for generating the Excel file
        $url = $this->generateExcelURL();

        // Redirect the user to the generated Excel file
        $this->redirectUser($url);
    }

    /**
     * Generates the URL for generating the Excel file.
     */
    private function generateExcelURL(): string
    {
        $url = 'generate_excel?salesDetails=true&dateFrom='.dateFormat($this->dateFrom).'&dateTo='.dateFormat($this->dateTo);

        // Add the customer parameter to the URL if provided
        if ($this->customer) {
            $url .= '&customer='.$this->customer;
        }

        return $url;
    }

    /**
     * Redirects the user to the specified URL.
     */
    private function redirectUser(string $url): void
    {
        redirect($url);
    }

    /**
     * Render the sales detail report table.
     *
     * @return Factory|View|Application The rendered view or factory object.
     */
    public function render(): Factory|View|Application
    {
        // Return the rendered view
        return view('livewire.sales-detail-report-table');
    }
}
