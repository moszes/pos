<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class ProductTargetFee extends Component
{
    public $productTargetFee = [];

    public $product;

    /**
     * Mount the component and initialize fee values.
     *
     * If the product exists, update the fee values based on the product's target fees.
     */
    public function mount(): void
    {
        // Initialize the fee values
        $this->productTargetFee = [
            'senior_doctor' => 0, // Fee for the senior doctor
            'doctor' => 0, // Fee for the doctor
            'doctor_assistance' => 0, // Fee for the doctor's assistance
            'sales' => 0, // Fee for sales
            'therapist' => 0, // Fee for the therapist
        ];

        // If the product exists, update the fee values
        if ($this->product) {
            foreach ($this->product->targetFees as $value) {
                // Update the fee value based on the role
                $this->productTargetFee[$value->role] = countFormat($value->value);
            }
        }
    }

    /**
     * Update the fee values based on the property name and value.
     *
     * @param  string  $propertyName  The name of the property that was updated.
     * @param  mixed  $value  The new value for the property.
     */
    public function updated(string $propertyName, mixed $value): void
    {
        // Check if the property name is one of the fee properties
        if (in_array($propertyName, [
            'productTargetFee.senior_doctor',
            'productTargetFee.doctor',
            'productTargetFee.doctor_assistance',
            'productTargetFee.sales',
            'productTargetFee.therapist',
        ])) {
            // Remove the 'productTargetFee.' prefix from the property name
            $productTargetFeeName = str_replace('productTargetFee.', '', $propertyName);

            // Convert the value to a float and update the productTargetFee property
            $this->productTargetFee[$productTargetFeeName] = floatval($value);

            $this->emit('feeUpdated', 'product', 'target', $this->productTargetFee);
        }
    }

    /**
     * Render the view for the ProductTargetFee Livewire component.
     *
     * This method returns the view for the ProductTargetFee Livewire component, which is 'livewire.product-target-fee'.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the livewire.product-target-fee view
        return view('livewire.product-target-fee');
    }
}
