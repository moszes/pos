<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class TreatmentDiscountFee extends Component
{
    public $treatmentDiscountFee = [];

    public $treatment;

    /**
     * Mount the component and initialize fee values.
     *
     * If the treatment exists, update the fee values based on the treatment's discount fees.
     */
    public function mount(): void
    {
        // Initialize the fee values
        $this->treatmentDiscountFee = [
            'senior_doctor' => 0, // Fee for the doctor
            'doctor' => 0, // Fee for the doctor
            'doctor_assistance' => 0, // Fee for the doctor's assistance
            'sales' => 0, // Fee for sales
            'therapist' => 0, // Fee for the therapist
        ];

        // If the treatment exists, update the fee values
        if ($this->treatment) {
            foreach ($this->treatment->discountFees as $value) {
                // Update the fee value based on the role
                $this->treatmentDiscountFee[$value->role] = countFormat($value->value);
            }
        }
    }

    /**
     * Update the fee values based on the property name and value.
     *
     * @param  string  $propertyName  The name of the property that was updated.
     * @param  mixed  $value  The new value for the property.
     */
    public function updated(string $propertyName, mixed $value): void
    {
        // Check if the property name is one of the fee properties
        if (in_array($propertyName, [
            'treatmentDiscountFee.senior_doctor',
            'treatmentDiscountFee.doctor',
            'treatmentDiscountFee.doctor_assistance',
            'treatmentDiscountFee.sales',
            'treatmentDiscountFee.therapist',
        ])) {
            // Remove the 'treatmentDiscountFee.' prefix from the property name
            $treatmentDiscountFeeName = str_replace('treatmentDiscountFee.', '', $propertyName);

            // Convert the value to a float and update the treatmentDiscountFee property
            $this->treatmentDiscountFee[$treatmentDiscountFeeName] = floatval($value);

            $this->emit('feeUpdated', 'treatment', 'discount', $this->treatmentDiscountFee);
        }
    }

    /**
     * Render the view for the TreatmentDiscountFee Livewire component.
     *
     * This method returns the view for the TreatmentDiscountFee Livewire component, which is 'livewire.treatment-discount-fee'.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the livewire.treatment-discount-fee view
        return view('livewire.treatment-discount-fee');
    }
}
