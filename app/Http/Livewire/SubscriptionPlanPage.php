<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SubscriptionPlanPage extends Component
{
    public function render()
    {
        return view('livewire.subscription-plan-page');
    }
}
