<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class TreatmentTargetFee extends Component
{
    public $treatmentTargetFee = [];

    public $treatment;

    /**
     * Mount the component and initialize fee values.
     *
     * If the treatment exists, update the fee values based on the treatment's target fees.
     */
    public function mount(): void
    {
        // Initialize the fee values
        $this->treatmentTargetFee = [
            'senior_doctor' => 0, // Fee for the senior doctor
            'doctor' => 0, // Fee for the doctor
            'doctor_assistance' => 0, // Fee for the doctor's assistance
            'sales' => 0, // Fee for sales
            'therapist' => 0, // Fee for the therapist
        ];

        // If the treatment exists, update the fee values
        if ($this->treatment) {
            foreach ($this->treatment->targetFees as $value) {
                // Update the fee value based on the role
                $this->treatmentTargetFee[$value->role] = countFormat($value->value);
            }
        }
    }

    /**
     * Update the fee values based on the property name and value.
     *
     * @param  string  $propertyName  The name of the property that was updated.
     * @param  mixed  $value  The new value for the property.
     */
    public function updated(string $propertyName, mixed $value): void
    {
        // Check if the property name is one of the fee properties
        if (in_array($propertyName, [
            'treatmentTargetFee.senior_doctor',
            'treatmentTargetFee.doctor',
            'treatmentTargetFee.doctor_assistance',
            'treatmentTargetFee.sales',
            'treatmentTargetFee.therapist',
        ])) {
            // Remove the 'treatmentTargetFee.' prefix from the property name
            $treatmentTargetFeeName = str_replace('treatmentTargetFee.', '', $propertyName);

            // Convert the value to a float and update the treatmentTargetFee property
            $this->treatmentTargetFee[$treatmentTargetFeeName] = floatval($value);

            $this->emit('feeUpdated', 'treatment', 'target', $this->treatmentTargetFee);
        }
    }

    /**
     * Render the view for the TreatmentTargetFee Livewire component.
     *
     * This method returns the view for the TreatmentTargetFee Livewire component, which is 'livewire.treatment-target-fee'.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the livewire.treatment-target-fee view
        return view('livewire.treatment-target-fee');
    }
}
