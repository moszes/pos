<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class ProductNormalFee extends Component
{
    public $productNormalFee = [];

    public $product;

    /**
     * Mount the component and initialize fee values.
     *
     * If the product exists, update the fee values based on the product's normal fees.
     */
    public function mount(): void
    {
        // Initialize the fee values
        $this->productNormalFee = [
            'senior_doctor' => 0, // Fee for the doctor
            'doctor' => 0, // Fee for the doctor
            'doctor_assistance' => 0, // Fee for the doctor's assistance
            'sales' => 0, // Fee for sales
            'therapist' => 0, // Fee for the therapist
        ];

        // If the product exists, update the fee values
        if ($this->product) {
            foreach ($this->product->normalFees as $value) {
                // Update the fee value based on the role
                $this->productNormalFee[$value->role] = countFormat($value->value);
            }
        }
    }

    /**
     * Update the fee values based on the property name and value.
     *
     * @param  string  $propertyName  The name of the property that was updated.
     * @param  mixed  $value  The new value for the property.
     */
    public function updated(string $propertyName, mixed $value): void
    {
        // Check if the property name is one of the fee properties
        if (in_array($propertyName, [
            'productNormalFee.senior_doctor',
            'productNormalFee.doctor',
            'productNormalFee.doctor_assistance',
            'productNormalFee.sales',
            'productNormalFee.therapist',
        ])) {
            // Remove the 'productNormalFee.' prefix from the property name
            $productNormalFeeName = str_replace('productNormalFee.', '', $propertyName);

            // Convert the value to a float and update the productNormalFee property
            $this->productNormalFee[$productNormalFeeName] = floatval($value);

            $this->emit('feeUpdated', 'product', 'normal', $this->productNormalFee);
        }
    }

    /**
     * Render the view for the ProductNormalFee Livewire component.
     *
     * This method returns the view for the ProductNormalFee Livewire component, which is 'livewire.product-normal-fee'.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the livewire.product-normal-fee view
        return view('livewire.product-normal-fee');
    }
}
