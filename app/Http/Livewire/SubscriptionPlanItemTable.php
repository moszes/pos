<?php

namespace App\Http\Livewire;

use App\Models\SubscriptionPlan;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Livewire\WithPagination;
use Str;

class SubscriptionPlanItemTable extends Component
{
    use WithPagination;

    public $titlePage = 'Subscription Plan Item';

    public $search;

    public $subscriptionPlan;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshSubscriptionPlanItemTable' => '$refresh',
        'selectSubscriptionPlanItem',
    ];

    public function mount(): void
    {
        $this->subscriptionPlan = SubscriptionPlan::find(Str::afterLast(request()->path(), '/'));
    }

    public function paginationView(): string
    {
        return 'pagination';
    }

    public function selectSubscriptionPlanItem($id): void
    {
        $this->subscriptionPlan = SubscriptionPlan::find($id);
    }

    public function render(): View|Factory|Application
    {
        return view('livewire.subscription-plan-item-table', [
            'subscription_plan_items' => $this->subscriptionPlan->treatments()?->paginate(10),
        ]);
    }
}
