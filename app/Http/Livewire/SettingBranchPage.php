<?php

namespace App\Http\Livewire;

use Illuminate\View\View;
use Livewire\Component;

class SettingBranchPage extends Component
{
    /**
     * Show the branch page.
     */
    public function render(): View
    {
        return view('livewire.setting-branch-page');
    }
}
