<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class TreatmentDetailInfo extends Component
{
    public $treatment;

    public $moduleLabel;

    public $moduleName;

    public $redirectRoute;

    public $priceLabel = true;

    /**
     * Render the treatment detail info view.
     */
    public function render(): View
    {
        // Return the view for the treatment detail info
        return view('livewire.treatment-detail-info');
    }
}
