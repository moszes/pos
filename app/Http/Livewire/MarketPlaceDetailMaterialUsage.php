<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class MarketPlaceDetailMaterialUsage extends Component
{
    public $marketplace;

    /**
     * Render the view for the MarketPlaceDetailMaterialUsage component.
     *
     * @return View The rendered view.
     */
    public function render(): View
    {
        return view('livewire.market-place-detail-material-usage');
    }
}
