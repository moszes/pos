<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PromotionPage extends Component
{
    public function render()
    {
        return view('livewire.promotion-page');
    }
}
