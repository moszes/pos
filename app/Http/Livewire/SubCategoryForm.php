<?php

namespace App\Http\Livewire;

use App\Models\SubCategory;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SubCategoryForm extends Component
{
    public $formTitle = 'Form Sub Category';

    public $actionForm = 'store';

    public $subCategory;

    public $categoryId;

    public $code;

    public $name;

    protected $listeners = [
        'clearSubCategoryForm',
        'editSubCategory',
        'destroy',
    ];

    protected $rules = [
        'code' => 'required|unique:sub_categories',
        'name' => 'required',
    ];

    /**
     * Returns an array of subcategory data.
     */
    public function getSubCategoryData(): array
    {
        // Set the default category ID to 1 if the selected category is '-- Choose --' or null
        $categoryId = ($this->categoryId == '-- Choose --' || $this->categoryId == null) ? 1 : $this->categoryId;

        // Return the subcategory data as an array
        return [
            'category_id' => $categoryId,
            'code' => $this->code,
            'name' => $this->name,
        ];
    }

    /**
     * Store a new subcategory.
     *
     * This function creates a new subcategory record in the database.
     * It validates the input data, starts a database transaction,
     * creates the subcategory record, and emits an event to clear the subcategory form.
     */
    public function store(): void
    {
        // Validate the input data
        $this->validate();

        // Start a database transaction
        DB::transaction(function () {
            // Create a new subcategory record
            SubCategory::create($this->getSubCategoryData());
        }, 5);

        // Emit an event to clear the subcategory form
        $this->emit('clearSubCategoryForm');
    }

    /**
     * Edit a subcategory.
     *
     * @param  mixed  $id  The ID of the subcategory to edit.
     */
    public function editSubCategory(mixed $id): void
    {
        // Find the subcategory by ID
        $this->subCategory = SubCategory::find($id);

        // Set the category ID, code, and name
        $this->categoryId = $this->subCategory->category_id;
        $this->code = $this->subCategory->code;
        $this->name = $this->subCategory->name;

        // Set the action form and emit an event to show the subcategory form
        $this->actionForm = 'update';
        $this->emit('showSubCategoryForm');
    }

    /**
     * Update the subcategory.
     *
     * This method updates the subcategory using the provided data.
     * It also emits an event to clear the subcategory form.
     */
    public function update(): void
    {
        // Start a database transaction
        DB::transaction(function () {
            // Update the subcategory with the new data
            $this->subCategory->update($this->getSubCategoryData());

            // Emit an event to clear the subcategory form
            $this->emit('clearSubCategoryForm');
        }, 5);
    }

    /**
     * Delete a subcategory by ID.
     *
     * @param  int  $id  The ID of the subcategory to delete.
     */
    public function destroy(int $id): void
    {
        // Find the subcategory by ID
        $this->subCategory = SubCategory::find($id);

        // Use a database transaction to ensure atomicity and consistency
        DB::transaction(function () {
            $this->subCategory->code = $this->subCategory->code.'-deleted';
            $this->subCategory->save();
            // Delete the subcategory
            $this->subCategory->delete();

        }, 5);

        // Emit an event to clear the subcategory form
        $this->emit('clearSubCategoryForm');
    }

    /**
     * Clear the subcategory form.
     *
     * Reset the form values and error bag, and emit events to refresh and hide the subcategory form.
     */
    public function clearSubCategoryForm(): void
    {
        $this->reset();
        $this->resetErrorBag();

        $this->emit('refreshSubCategory');
        $this->emit('hideSubCategoryForm');
    }

    /**
     * Render the view for the subcategory form.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.sub-category-form');
    }
}
