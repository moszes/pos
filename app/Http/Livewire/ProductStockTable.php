<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ProductStockTable extends Component
{
    public $product;

    public function render()
    {
        return view('livewire.product-stock-table');
    }
}
