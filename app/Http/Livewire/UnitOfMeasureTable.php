<?php

namespace App\Http\Livewire;

use App\Models\UnitOfMeasure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;
use Livewire\WithPagination;

class UnitOfMeasureTable extends Component
{
    use WithPagination;

    public $search;

    public $titlePage = 'Unit Of Measure';

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshUnitOfMeasure' => '$refresh',
    ];

    /**
     * Returns the name of the pagination view.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Filters the search results for unit of measure.
     */
    public function searchFilter(): LengthAwarePaginator|array
    {
        return UnitOfMeasure::where('company_id', auth()->user()->details->company_id)
            ->where(function ($query) {
                $query->where('name', 'like', '%'.$this->search.'%')
                    ->orWhere('code', 'like', '%'.$this->search.'%');
            })
            ->orderBy('id', 'desc')
            ->paginate(10);
    }

    /**
     * Renders the view for the unit of measure table.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.unit-of-measure-table', [
            'unit_of_measures' => self::searchFilter(),
        ]);
    }
}
