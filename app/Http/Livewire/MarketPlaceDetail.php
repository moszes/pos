<?php

namespace App\Http\Livewire;

use App\Models\MarketPlace;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class MarketPlaceDetail extends Component
{
    public $formTitle = 'Market Place Details';

    public $marketplace;

    public $marketPlaceMaterialUsages = [];

    protected $listeners = [
        'showMarketPlaceDetail',
        'clearMarketPlaceDetails',
    ];

    /**
     * Show marketplace details based on the given ID.
     */
    public function showMarketPlaceDetail(int $id): void
    {
        // Find and set the marketplace item based on the given ID
        $this->marketplace = MarketPlace::find($id);
    }

    /**
     * Clear the marketplace details.
     */
    public function clearMarketPlaceDetails(): void
    {
        // Reset the Livewire component
        $this->reset();

        // Reset the Livewire error bag
        $this->resetErrorBag();
    }

    /**
     * Render the view for the marketplace detail.
     */
    public function render(): View
    {
        // Return the rendered view for the marketplace detail
        return view('livewire.market-place-detail');
    }
}
