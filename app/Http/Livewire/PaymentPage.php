<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class PaymentPage extends Component
{
    /**
     * Render the payment page.
     *
     * This method renders the payment page view using the Laravel view helper
     * function.
     * The rendered view can be a Factory, View, or Application instance.
     *
     * @return Factory|View|Application The rendered payment page view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.payment-page');
    }
}
