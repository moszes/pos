<?php

namespace App\Http\Livewire;

use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Spatie\Permission\Models\Role;

class RoleForm extends Component
{
    public $formTitle = 'Form Role';

    public $actionForm = 'store';

    public $role;

    public $name;

    protected $listeners = [
        'clearRoleForm',
        'editRole',
        'destroy',
    ];

    protected $rules = [
        'name' => 'required',
    ];

    /**
     * Get the role data.
     *
     * @return array The role data.
     */
    public function getRoleData(): array
    {
        return [
            'name' => $this->name,
        ];
    }

    /**
     * Store a new role.
     */
    public function store(): void
    {
        // Validate the request data
        $this->validate();

        // Create the role within a database transaction
        DB::transaction(function () {
            Role::create($this->getRoleData());
        }, 5);

        // Emit an event to clear the role form
        $this->emit('clearRoleForm');
    }

    /**
     * Edit a role.
     *
     * @param  int  $id  The ID of the role to edit.
     */
    public function editRole(int $id): void
    {
        // Find the role by ID
        $this->role = Role::find($id);

        // Set the name property to the name of the role
        $this->name = $this->role->name;

        // Set the actionForm property to 'update'
        $this->actionForm = 'update';

        // Emit the 'showRoleForm' event
        $this->emit('showRoleForm');
    }

    /**
     * Update the role in the database.
     */
    public function update(): void
    {
        // Validate the input
        $this->validate();

        // Start a database transaction
        DB::transaction(function () {
            // Update the role with the new name
            $this->role->update([
                'name' => $this->name,
            ]);
        }, 5);

        // Emit an event to clear the role form
        $this->emit('clearRoleForm');
    }

    /**
     * Delete a role by its ID.
     *
     * @param  int  $id  The ID of the role to delete.
     */
    public function destroy(int $id): void
    {
        // Find the role by its ID
        $this->role = Role::find($id);

        // Start a database transaction
        DB::transaction(function () {
            // Delete the role
            $this->role->delete();
        }, 5);

        // Emit an event to clear the role form
        $this->emit('clearRoleForm');
    }

    /**
     * Clear the role form by resetting the form data and error bag,
     * and emit events to refresh and hide the role form.
     */
    public function clearRoleForm(): void
    {
        $this->reset(); // Reset the form data
        $this->resetErrorBag(); // Reset the error bag

        $this->emit('refreshRole'); // Emit event to refresh the role form
        $this->emit('hideRoleForm'); // Emit event to hide the role form
    }

    /**
     * Render the form for creating a new role.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.role-form');
    }
}
