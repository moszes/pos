<?php

namespace App\Http\Livewire;

use App\Models\ApplicationSetting;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderItem;
use App\Models\Vendor;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PurchaseOrderForm extends Component
{
    public $formTitle = 'Purchase Order Form';

    public $actionForm = 'store';

    public $userId;

    public $companyId;

    public $branchId;

    public $vendor;

    public $product;

    public $purchaseOrderNumber;

    public $date;

    public $discount = 0;

    public $tax = 0;

    public $others = 0;

    public $subTotal = 0;

    public $grandTotal = 0;

    public $note;

    public $items;

    public $purchaseOrder;

    public $approvalPurchaseOrder;

    protected $listeners = [
        'clearPurchaseOrderForm',
        'clearPurchaseOrderItem',
        'editPurchaseOrder',
        'setVendor',
        'addProductItem',
        'setSubTotal',
        'destroy',
    ];

    protected $rules = [
        'order_number' => 'required|unique:purchase_orders',
    ];

    /**
     * Mounts the component.
     */
    public function mount(): void
    {
        // Load the default field
        $this->loadDefaultField();
    }

    /**
     * Update the grand total when specific properties are updated.
     *
     * @param  string  $name  The name of the property being updated.
     */
    public function updated(string $name): void
    {
        // Update the grand total only if the updated property is 'discount', 'others', or 'tax'
        if ($name === 'discount' || $name === 'others' || $name === 'tax') {
            // Calculate the new grand total based on the sum of item total prices and the updated properties
            $this->grandTotal = $this->items->sum('total_price') + $this->tax + $this->others - $this->discount;
        }
    }

    /**
     * Load default field values for the purchase order.
     */
    public function loadDefaultField(): void
    {
        // Generate a purchase order number
        $this->purchaseOrderNumber = generatePurchaseOrderNumber();

        // Set the current date
        $this->date = now()->format('Y-m-d');

        // Create an empty collection for the items
        $this->items = collect();

        // Check if the approval purchase order setting is enabled
        $this->approvalPurchaseOrder = ApplicationSetting::where('name', 'Approval Purchase Order')->first()->status == 'Y';
    }

    /**
     * Sets the vendor for the object.
     *
     * @param  int  $vendorId  The ID of the vendor.
     */
    public function setVendor(int $vendorId): void
    {
        // Find the vendor with the given ID
        $this->vendor = Vendor::find($vendorId);
    }

    /**
     * Adds a purchase order item.
     *
     * @param  mixed  $item  The item to add.
     */
    public function addProductItem($item): void
    {
        // Add the item to the items collection.
        $this->items->push($item);

        // Calculate the new grand total.
        $this->grandTotal = $this->items->sum('total_price') + $this->tax + $this->others - $this->discount;
    }

    /**
     * Removes a purchase order item from the list.
     *
     * @param  int  $arrayKey  The key of the item to be removed.
     */
    public function removePurchaseOrderItem(int $arrayKey): void
    {
        // Remove the item from the list
        $this->items->forget($arrayKey);

        // Reset the grand total
        $this->grandTotal = 0;

        // Recalculate the grand total
        foreach ($this->items as $purchaseOrder) {
            $this->grandTotal += $purchaseOrder['total_price'];
        }
    }

    /**
     * Returns an array containing the purchase order data.
     */
    public function purchaseOrderData(): array
    {
        return [
            'company_id' => $this->companyId,
            'branch_id' => $this->branchId,
            'vendor_id' => @$this->vendor->id,
            'user_id' => $this->userId,
            'order_number' => $this->purchaseOrderNumber,
            'discount' => countFormat($this->discount ?? 0),
            'tax' => countFormat($this->tax ?? 0),
            'others' => countFormat($this->others ?? 0),
            'grand_total' => countFormat($this->grandTotal ?? 0),
            'date' => $this->date ?? now()->format('Y-m-d H:i:s'),
            'approved_by' => $this->approvalPurchaseOrder ? null : 'system',
            'approved_date' => $this->approvalPurchaseOrder ? null : date('Y-m-d H:i:s'),
            'note' => $this->note,
        ];
    }

    /**
     * Store the purchase order and its items.
     */
    public function store(): void
    {
        // Create the purchase order
        $this->purchaseOrder = PurchaseOrder::create($this->purchaseOrderData());

        // Create the purchase order items
        foreach ($this->items as $item) {
            // Set the purchase order id
            $item['purchase_order_id'] = $this->purchaseOrder->id;

            // Set the product id
            $item['product_id'] = $item['product']['id'];

            // Create the purchase order item
            PurchaseOrderItem::create($item);
        }

        // Update the grand total of the purchase order
        $this->updateGrandTotal($this->purchaseOrder);

        // Clear the purchase order form
        $this->clearPurchaseOrderForm();

        // Emit events
        $this->emit('dataSaved');
        $this->emit('refreshPurchaseOrder');
    }

    /**
     * Set the subtotal for the items.
     */
    public function setSubTotal(): void
    {
        // Calculate the sum of the total prices for all items
        $totalPriceSum = $this->items->sum('total_price');

        // Format the sum using the countFormat function
        $formattedTotalPriceSum = countFormat($totalPriceSum);

        // Set the formatted sum as the subtotal
        $this->subTotal = $formattedTotalPriceSum;
    }

    /**
     * Updates the grand total of a purchase order
     *
     * @param  PurchaseOrder  $purchaseOrder  The purchase order to update
     */
    public function updateGrandTotal(PurchaseOrder $purchaseOrder): void
    {
        // Calculate the new grand total
        $grandTotal = $this->items->sum('total_price') + $this->tax + $this->others - $this->discount;

        // Update the purchase order with the new grand total
        $purchaseOrder->update([
            'grand_total' => $grandTotal,
        ]);
    }

    /**
     * Edit a purchase order.
     *
     * @param  int  $id  - The ID of the purchase order to edit.
     */
    public function editPurchaseOrder(int $id): void
    {
        // Retrieve the purchase order
        $this->purchaseOrder = PurchaseOrder::find($id);

        // Set properties based on the purchase order
        $this->companyId = $this->purchaseOrder->company_id;
        $this->branchId = $this->purchaseOrder->branch_id;

        $this->emit('selectVendorById', $this->purchaseOrder->vendor_id);

        $this->purchaseOrderNumber = $this->purchaseOrder->order_number;
        $this->note = $this->purchaseOrder->note;
        $this->date = dateFormat($this->purchaseOrder->date);

        $this->discount = numberFormat($this->purchaseOrder->discount);
        $this->tax = numberFormat($this->purchaseOrder->tax);
        $this->others = numberFormat($this->purchaseOrder->others);
        $this->grandTotal = numberFormat($this->purchaseOrder->grand_total);

        // Retrieve the purchase order items
        $items = PurchaseOrderItem::where('purchase_order_id', $id)->get();

        foreach ($items as $purchaseOrderItem) {
            // Create an array to store the item details
            $item = [];

            // Set the item details based on the purchase order item
            $item['purchase_order_id'] = $this->purchaseOrder->id;
            $item['id'] = $purchaseOrderItem->product_id;
            $item['product_id'] = $purchaseOrderItem->product_id;
            $item['itemCode'] = @getProductById($purchaseOrderItem['product_id'])->code;
            $item['itemName'] = @getProductById($purchaseOrderItem['product_id'])->name;
            $item['unit'] = @getProductById($purchaseOrderItem['product_id'])->size.' '.@getProductById($purchaseOrderItem['product_id'])->unit;
            $item['qty'] = @countFormat($purchaseOrderItem->qty);
            $item['price'] = @countFormat($purchaseOrderItem->price);
            $item['last_price'] = getLastPriceByProductId($purchaseOrderItem['product_id']);
            $item['discount_item'] = @countFormat($purchaseOrderItem->discount_item);
            $item['total_price'] = @countFormat($purchaseOrderItem->total_price);
            $item['unit_of_measure_id'] = @getProductById($purchaseOrderItem['product_id'])->unit_of_measure_id;

            // Add the item to the items collection
            $this->items->push($item);
        }

        // Set the action form
        $this->actionForm = 'update';

        // Emit an event to show the purchase order form
        $this->emit('showPurchaseOrderForm');
    }

    /**
     * Update the purchase order and its items.
     */
    public function update(): void
    {
        // Delete all existing purchase order items for this purchase order
        PurchaseOrderItem::where('purchase_order_id', $this->purchaseOrder->id)->delete();

        // Reset the grand total
        $this->grandTotal = 0;

        // Iterate over each item and create a new purchase order item
        foreach ($this->items as $purchaseOrderItem) {
            // Set the purchase order ID
            $purchaseOrderItem['purchase_order_id'] = $this->purchaseOrder->id;
            // Set the product ID
            $purchaseOrderItem['product_id'] = $purchaseOrderItem['id'];
            // Format the price, quantity, and discount item
            $purchaseOrderItem['price'] = countFormat($purchaseOrderItem['price'] ?? 0);
            $purchaseOrderItem['qty'] = countFormat($purchaseOrderItem['qty'] ?? 0);
            $purchaseOrderItem['discount_item'] = countFormat($purchaseOrderItem['discount_item'] ?? 0);
            // Calculate and format the total price
            $purchaseOrderItem['total_price'] = countFormat($purchaseOrderItem['price'] * $purchaseOrderItem['qty'] - $purchaseOrderItem['discount_item']);

            // Create the new purchase order item
            PurchaseOrderItem::create($purchaseOrderItem);
        }

        // Calculate the new grand total
        $this->grandTotal = $this->items->sum('total_price');

        // Update the purchase order with the new data
        $this->purchaseOrder->update($this->purchaseOrderData());

        // Clear the purchase order form
        $this->clearPurchaseOrderForm();

        // Emit events to notify other components of the data update
        $this->emit('dataUpdated');
        $this->emit('refreshPurchaseOrder');
    }

    /**
     * Destroy a purchase order and its related items.
     *
     * @param  int  $id  The ID of the purchase order to destroy.
     */
    public function destroy(int $id): void
    {
        // Delete the purchase order
        PurchaseOrder::find($id)->delete();

        // Delete the items associated with the purchase order
        PurchaseOrderItem::where('purchase_order_id', $id)->delete();

        // Clear the purchase order form
        $this->clearPurchaseOrderForm();

        // Emit an event to indicate that data has been deleted
        $this->emit('dataDeleted');

        // Emit an event to refresh the purchase order
        $this->emit('refreshPurchaseOrder');
    }

    /**
     * Clears the purchase order form.
     */
    public function clearPurchaseOrderForm(): void
    {
        // Reset the form
        $this->reset();

        // Load default field values
        $this->loadDefaultField();
        // Hide the purchase order form
        $this->emit('clearVendorSearch');
        $this->emit('hidePurchaseOrderForm');
    }

    /**
     * Renders the purchase order form view.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the rendered purchase order form view
        return view('livewire.purchase-order-form');
    }
}
