<?php

namespace App\Http\Livewire;

use App\Models\Treatment;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class TreatmentMaterialUsagePage extends Component
{
    public $treatmentId;

    public $treatment;

    public $moduleLabel = 'Item Details';

    public $moduleName;

    public $redirectRoute;

    public $priceLabel = true;

    /**
     * Mount the treatment ID from the request segment
     */
    public function mount(): void
    {
        // Set the treatment ID from the last segment of the request URL
        $this->treatmentId = request()->segment(count(request()->segments()));

        // Find the treatment by ID
        $this->treatment = Treatment::find($this->treatmentId);
        $this->redirectRoute = route('master.treatments', ['search' => $this->treatment->code]);

        if (! $this->treatment) {
            redirect($this->redirectRoute);
        }
    }

    /**
     * Render the treatment material usage page view.
     */
    public function render(): View
    {
        return view('livewire.treatment-material-usage-page', [
            'treatment' => Treatment::find($this->treatmentId),
        ]);
    }
}
