<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoice;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class DashboardTodaySales extends Component
{
    /**
     * Render the component.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.dashboard-today-sales', [
            'todaySales' => SalesInvoice::where('sales_invoice_status', 'valid')
                ->whereDate('sales_invoice_date', date('Y-m-d'))->sum('grand_total'),
        ]);
    }
}
