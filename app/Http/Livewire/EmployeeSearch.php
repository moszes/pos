<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class EmployeeSearch extends Component
{
    public $doctorId = '-- Choose --';

    public $doctorAssistanceId = '-- Choose --';

    public $therapistId = '-- Choose --';

    public $staff = [];

    protected $listeners = [
        'clearEmployeeSearch',
        'picRequired',
    ];

    /**
     * Update the value of a staff member and emit an event with the updated staff data.
     *
     * @param  string  $name  The name of the staff member.
     * @param  mixed  $value  The new value for the staff member.
     */
    public function updated(string $name, mixed $value): void
    {
        // Update the value of the staff member
        $this->staff[$name] = $value;

        // Emit an event with the updated staff data
        $this->emit('setEmployee', $this->staff);
        $this->resetErrorBag();
    }

    /**
     * Adds an error message to the 'doctorId' field indicating that a PIC is required.
     */
    public function picRequired(): void
    {
        // Add an error message to the 'doctorId' field
        $this->addError('doctorId', 'PIC Required');
    }

    /**
     * Clears the staff data by resetting the object.
     */
    public function clearEmployeeSearch(): void
    {
        $this->reset();
    }

    /**
     * Render the view for the staff search component.
     */
    public function render(): View
    {
        // Return the view for the staff search component
        return view('livewire.employee-search');
    }
}
