<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class ProductDetail extends Component
{
    public $formTitle = 'Product Details';

    public $product;

    protected $listeners = [
        'showProductDetails',
        'clearProductDetail',
    ];

    /**
     * Show product details based on the provided ID.
     *
     * @param  int  $id  The ID of the product to display
     */
    public function showProductDetails(int $id): void
    {
        $this->product = Product::find($id);
    }

    /**
     * Clears the product detail by resetting the properties and error bag.
     */
    public function clearProductDetail(): void
    {
        // Reset all properties to their default values
        $this->reset();

        // Reset the error bag to remove any errors
        $this->resetErrorBag();
    }

    /**
     * Render the product detail view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.product-detail');
    }
}
