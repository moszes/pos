<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SalesReportLink extends Component
{
    public function render()
    {
        return view('livewire.sales-report-link');
    }
}
