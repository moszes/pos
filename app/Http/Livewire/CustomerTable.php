<?php

namespace App\Http\Livewire;

use App\Models\User;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Query\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class CustomerTable extends Component
{
    use WithPagination;

    public $titlePage = 'Customer';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshCustomer' => '$refresh',
    ];

    /**
     * Reset the page number when the search parameter is updated.
     */
    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    /**
     * Redirects the user to the downloadCustomers route to generate Excel file for customer data download.
     */
    public function downloadCustomerData(): void
    {
        $this->redirect(route('generate_excel', ['customers' => 'downloadCustomers']));
    }

    /**
     * The attributes that are mass assignable.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Perform a search filter and return a paginated result.
     */
    public function searchFilter(): LengthAwarePaginator
    {
        $customerData = User::role('customer')->pluck('id')->toArray();

        return DB::table('users')
            ->join('user_details', 'user_details.user_id', '=', 'users.id')
            ->whereIn('users.id', $customerData)
            ->where('user_details.company_id', auth()->user()->details->company_id)
            ->where(function (Builder $query) {
                $query->where('users.name', 'like', '%'.$this->search.'%')
                    ->orWhere('user_details.code', 'like', '%'.$this->search.'%')
                    ->orWhere('user_details.phone', 'like', '%'.$this->search.'%')
                    ->orWhere('user_details.first_name', 'like', '%'.$this->search.'%');
            })
            ->orderBy('users.id', 'desc')
            ->paginate(10);
    }

    /**
     * The attributes that are mass assignable.
     */
    public function render(): View|Factory|Application
    {

        return view('livewire.customer-table', [
            'customers' => $this->searchFilter(),
        ]);
    }
}
