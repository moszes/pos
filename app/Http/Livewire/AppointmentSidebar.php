<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class AppointmentSidebar extends Component
{
    public $appointmentDate;

    public $customer = [];

    public $note;

    public $item = [];

    public $room = [];

    public $redemption = [];

    public $staff = [];

    public $newCustomer = false;

    protected $listeners = [
        'setEmployee',
        'roomSelected',
        'setRedemption',
        'clearAppointmentSidebar',
        'setCustomer',
    ];

    /**
     * Mount the function.
     */
    public function mount(): void
    {
        $this->appointmentDate = now()->format('Y-m-d');
    }

    /**
     * Sets the selected staff.
     *
     * @param  mixed  $staff  The staff to set.
     */
    public function setEmployee(mixed $staff): void
    {
        // Assign the $staff parameter to the class property $this->staff
        $this->staff = $staff;
    }

    /**
     * Sets the selected redemption.
     *
     * @param  mixed  $redemption  The redemption to set.
     */
    public function setRedemption(mixed $redemption): void
    {
        $this->redemption = $redemption;
    }

    /**
     * Set the selected customer.
     *
     * @param  mixed  $customer  The customer to be set.
     */
    public function setCustomer(mixed $customer): void
    {
        // Assign the $customer parameter to the class property $this->customer
        $this->customer = $customer;
    }

    /**
     * Sets the selected room.
     *
     * @param  mixed  $room  The room to be selected.
     */
    public function roomSelected(mixed $room): void
    {
        $this->room = $room;
    }

    /**
     * Create an appointment.
     *
     * Sets the necessary information for the appointment in the items array.
     * Emits the 'itemAdded' event with the items array as the payload
     * and emits the 'cartOpen' event.
     */
    public function addAppointmentItem(): void
    {
        // Check if the customer is not set
        if (empty($this->customer)) {
            // Emit event to indicate that a customer is required
            $this->emit('customerRequired');

            return;
        }

        if (empty($this->redemption)) {

            // Emit event to indicate that form is expired
            $this->emit('redemptionRequired');

            return;
        }

        // Set the necessary information for the appointment
        $this->item = [
            'appointment_date' => $this->appointmentDate,
            'module' => $this->redemption['module'],
            'module_id' => $this->redemption['module_id'],
            'code' => $this->redemption['code'],
            'name' => $this->redemption['name'],
            'transaction_status' => $this->redemption['transaction_status'],
            'room_id' => @$this->room['room_id'],
            'room_start' => @$this->room['room_start'],
            'room_end' => @$this->room['room_end'],
            'qty' => $this->redemption['qty'],
            'price' => $this->redemption['price'],
            'total_price' => $this->redemption['total_price'],
            'redemption_price' => @$this->redemption['redemption_price'],
            'total_redemption_price' => @$this->redemption['total_redemption_price'],
            'doctor_id' => @$this->staff['doctorId'],
            'doctor_assistance_id' => @$this->staff['doctorAssistanceId'],
            'therapist_id' => @$this->staff['therapistId'],
        ];

        // Emit the 'itemAdded' event with the items array as the payload
        $this->emit('itemAdded', $this->item);

        // Emit the 'cartOpen' event
        $this->emit('cartOpen', null, $this->customer['users']['id']);

        // Reset the item array
        $this->item = [];
    }

    /**
     * Clears all items in the class.
     */
    public function clearAppointmentSidebar(): void
    {
        // Set the items array to an empty array.
        $this->item = [];
    }

    /**
     * Renders the view for the appointment sidebar.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.appointment-sidebar');
    }
}
