<?php

namespace App\Http\Livewire;

use App\Models\SubscriptionPlan;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class SubscriptionPlanForm extends Component
{
    public $formTitle = 'Add Subscription Plan';

    public $actionForm = 'store';

    public $customer;

    public $subscriptionPlan;

    public $code;

    public $name;

    public $price;

    protected $listeners = [
        'clearSubscriptionPlanForm',
        'edit',
        'setSubscriptionPlan',
        'destroy',
    ];

    /**
     * Set the subscription plan for the user based on the provided subscription plan ID.
     *
     * @param  int  $subscriptionPlanId  The ID of the subscription plan to set
     */
    public function setSubscriptionPlan(int $subscriptionPlanId): void
    {
        $this->subscriptionPlan = SubscriptionPlan::find($subscriptionPlanId);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(): void
    {
        // Validate the request data
        $this->validate([
            'code' => 'required',
            'name' => 'required',
            'price' => 'required',
        ]);

        // Create a new subscription plan
        SubscriptionPlan::create([
            'company_id' => 1,
            'code' => $this->code,
            'name' => $this->name,
            'price' => $this->price,
        ]);

        // Emit event to refresh the subscription plan table
        $this->emit('refreshSubscriptionPlanTable');

        // Emit event to hide the subscription plan form
        $this->emit('hideSubscriptionPlanForm');
    }

    /**
     * Edit a subscription plan
     *
     * @param  int  $id  The ID of the subscription plan to edit
     */
    public function edit(int $id): void
    {
        // Find the subscription plan by ID
        $this->subscriptionPlan = SubscriptionPlan::find($id);

        // Set form title and action for the form
        $this->formTitle = 'Edit Subscription Plan';
        $this->actionForm = 'update';

        // Set variables from the subscription plan
        $this->code = $this->subscriptionPlan->code;
        $this->name = $this->subscriptionPlan->name;
        $this->price = $this->subscriptionPlan->price;

        // Emit event to show the subscription plan form
        $this->emit('showSubscriptionPlanForm');
    }

    /**
     * Update the subscription plan with the provided code, name, and price
     */
    public function update(): void
    {
        // Validate the request data
        $this->validate([
            'code' => 'required',
            'name' => 'required',
            'price' => 'required',
        ]);

        // Update the subscription plan with the new values
        $this->subscriptionPlan->update([
            'code' => $this->code,
            'name' => $this->name,
            'price' => $this->price,
        ]);

        // Emit events to refresh the subscription plan table and hide the subscription plan form
        $this->emit('refreshSubscriptionPlanTable');
        $this->emit('hideSubscriptionPlanForm');
    }

    /**
     * Delete the subscription plan and emit events to refresh the subscription plan table and hide the subscription plan form.
     */
    public function destroy($id): void
    {
        $this->subscriptionPlan = SubscriptionPlan::find($id);
        $this->subscriptionPlan->code = $this->subscriptionPlan->code.'-deleted';
        $this->subscriptionPlan->save();

        $this->subscriptionPlan->delete();
        $this->emit('refreshSubscriptionPlanTable');
        $this->emit('hideSubscriptionPlanForm');
    }

    public function clearSubscriptionPlanForm(): void
    {
        $this->reset();
        $this->resetErrorBag();
    }

    /**
     * Render the subscription plan form view.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.subscription-plan-form');
    }
}
