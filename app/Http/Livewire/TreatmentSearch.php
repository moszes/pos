<?php

namespace App\Http\Livewire;

use App\Models\Treatment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;

class TreatmentSearch extends Component
{
    public $treatment = [];

    public $treatments;

    public $treatmentList = false;

    public $treatmentSearch;

    public $treatmentPrice = 0;

    public $treatmentQty = 1;

    public $treatmentDiscountItem = 0;

    protected $listeners = [
        'clearTreatmentSearch',
        'resetAllError',
        'treatmentRequired',
    ];

    /**
     * Reset the error bag.
     *
     * This function is called when the component is updated and it resets the error bag.
     */
    public function updated(): void
    {
        // Reset the error bag
        $this->resetErrorBag();
    }

    /**
     * Update the treatment search and emit events to clear related fields.
     *
     * This function is triggered when the treatment search input is updated.
     * It emits events to clear the person in charge and room search fields,
     * resets the error bag, and queries the treatments from the database based on the search input.
     * If the search input is empty, it enables the treatment list view and clears the treatments.
     */
    public function updatedTreatmentSearch(): void
    {
        // Emit events to clear the person in charge and room search fields
        $this->emit('clearPersonInCharge');
        $this->emit('clearRoomSearch');

        // Reset the error bag
        $this->resetErrorBag();

        if ($this->treatmentSearch !== '') {
            // Enable the treatment list view
            $this->treatmentList = true;

            // Query the treatments from the database based on the search input
            $this->treatments = Treatment::where('company_id', auth()->user()->details->branch_id)
                ->where(function (Builder $query) {
                    $query->where('code', 'like', '%'.strtoupper($this->treatmentSearch).'%')
                        ->orWhere('name', 'like', '%'.strtoupper($this->treatmentSearch).'%');
                })
                ->get();
        } else {
            // Enable the treatment list view and clear the treatments
            $this->treatmentList = true;
            $this->treatments = [];
        }
    }

    /**
     * Updates the quantity of the selected treatment and emits an event with the updated treatment data.
     *
     * This method is triggered when the quantity of the selected treatment changes.
     * It first checks if the new quantity is a numeric value, and if not, it sets the quantity to 1,
     * emits an event to notify the user about the invalid input and adds an error message to the treatmentQty field.
     *
     * After validating the quantity, it updates the 'qty' property of the selected treatment with the new value
     * converted to a float and emits an event to notify any listeners about the updated treatment data.
     */
    public function updatedTreatmentQty(): void
    {
        if (! is_numeric($this->treatmentQty)) {
            $this->treatmentQty = 1;
            $this->emit('shouldNumericValue');
            $this->addError('treatmentQty', 'The quantity must be a number.');
        }

        $this->treatment['qty'] = floatval($this->treatmentQty);
        $this->emit('treatmentSelected', $this->treatment);
    }

    /**
     * Update the discount item for the selected treatment and emit an event with the updated treatment data.
     */
    public function updatedTreatmentDiscountItem(): void
    {
        $this->treatmentDiscountItem = floatval($this->treatmentDiscountItem);

        if (! is_numeric($this->treatmentDiscountItem)) {
            $this->treatmentDiscountItem = 0;
            $this->emit('shouldNumericValue');
            $this->addError('treatmentDiscountItem', 'The discount must be a number.');
        }

        if ($this->treatmentDiscountItem > 0) {
            $this->treatment['discount_item'] = floatval($this->treatmentDiscountItem);
            $this->emit('treatmentSelected', $this->treatment);
            $this->emit('applyDiscount');
        }

    }

    /**
     * Updates the selected treatment and emits an event with the updated treatment data.
     *
     * @param  int  $id  The ID of the treatment.
     */
    public function selectTreatment(int $id): void
    {
        $treatmentData = Treatment::find($id)->toArray();
        $this->populateTreatmentData($treatmentData, $id);
        $this->treatmentList = false;
        $this->treatments = null;
        $this->emit('treatmentSelected', $this->treatment);
    }

    /**
     * Updates the treatment data with the provided information.
     *
     * @param  array  $treatmentData  An associative array containing the treatment data.
     * @param  int  $id  The ID of the treatment.
     */
    private function populateTreatmentData(array $treatmentData, int $id): void
    {
        $this->treatment = $treatmentData;
        $this->treatmentSearch = $this->treatment['code'].', '.$this->treatment['name'].', '.countFormat($this->treatment['price']);

        $this->treatmentPrice = floatval($this->treatment['price']);

        $this->treatment['qty'] = floatval($this->treatmentQty);
        $this->treatment['price'] = $this->treatmentPrice;
        $this->treatment['discount_item'] = floatval($this->treatmentDiscountItem);
        $this->treatment['transaction_status'] = 'buy';
        $this->treatment['module'] = 'treatment';
        $this->treatment['module_id'] = $id;
    }

    /**
     * Marks the treatment as required.
     */
    public function treatmentRequired(): void
    {
        // Adds an error message for the 'treatmentSearch' field
        $this->addError('treatmentSearch', 'Please select a treatment.');
        $this->emit('clearRoomSearch');
        $this->emit('clearPersonInCharge');
    }

    /**
     * Clears the SI item.
     */
    public function clearTreatmentSearch(): void
    {
        $this->reset();
    }

    /**
     * Resets all errors in the error bag.
     */
    public function resetAllError(): void
    {
        $this->resetErrorBag();
    }

    /**
     * Render the view for the treatment search.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the view for the treatment search
        return view('livewire.treatment-search');
    }
}
