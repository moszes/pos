<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PurchaseReportPage extends Component
{
    public function render()
    {
        return view('livewire.purchase-report-page');
    }
}
