<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class MarketPlacePage extends Component
{
    /**
     * Render the market-place-page view.
     */
    public function render(): View
    {
        // Return the livewire.market-place-page view.
        return view('livewire.market-place-page');
    }
}
