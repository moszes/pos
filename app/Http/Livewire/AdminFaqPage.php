<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AdminFaqPage extends Component
{
    public function render()
    {
        return view('livewire.admin-faq-page');
    }
}
