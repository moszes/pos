<?php

namespace App\Http\Livewire;

use App\Models\Package;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PackageDetail extends Component
{
    public $formTitle = 'Package Details';

    public $package;

    public $grandTotal;

    protected $listeners = [
        'showPackageDetail',
        'clearItems',
    ];

    /**
     * Show package items.
     *
     * @param  int  $id  The package ID
     */
    public function showPackageDetail(int $id): void
    {
        $this->package = Package::find($id);
    }

    /**
     * Clears all items in the current context.
     */
    public function clearItems(): void
    {
        // Resets the current context
        $this->reset();
    }

    /**
     * Render the package details modal view.
     */
    public function render(): View
    {
        return view('livewire.package-detail');
    }
}
