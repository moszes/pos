<?php

namespace App\Http\Livewire;

use App\Models\Vendor;
use App\Models\VendorPayment;
use App\Models\VendorTax;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class VendorForm extends Component
{
    public $formTitle = 'Form Vendor';

    public $actionForm = 'store';

    public $companyId;

    public $vendor;

    public $code;

    public $name;

    public $phone;

    public $address;

    public $contactPerson;

    public $company;

    public $npwp;

    public $npwpName;

    public $taxAddress;

    public $period;

    public $bank;

    public $payment_name;

    public $payment_number;

    protected $listeners = [
        'clearVendorForm',
        'editVendor',
        'destroy',
    ];

    protected $rules = [
        'code' => 'required|unique:vendors',
        'name' => 'required',
    ];

    /**
     * Retrieve vendor data as an array.
     *
     * @return array The vendor data.
     */
    public function vendorData(): array
    {
        return [
            // Retrieve the first company ID.
            'company_id' => auth()->user()->details->branch_id,

            // Get the vendor code.
            'code' => $this->code,

            // Get the vendor name.
            'name' => $this->name,

            // Get the vendor phone number.
            'phone' => $this->phone,

            // Get the vendor address.
            'address' => $this->address,

            // Get the vendor contact person.
            'contact_person' => $this->contactPerson,
        ];
    }

    /**
     * Returns an array with vendor tax data.
     *
     * @return array An array containing the vendor tax data.
     */
    public function getVendorTaxData(): array
    {
        // Create an array with the vendor tax data
        return [
            'vendor_id' => $this->vendor->id,
            'company' => $this->company,
            'npwp' => $this->npwp,
            'npwp_name' => $this->npwpName,
            'tax_address' => $this->taxAddress,
        ];
    }

    /**
     * Get the vendor payment data.
     *
     * @return array The vendor payment data.
     */
    public function vendorPaymentData(): array
    {
        // Return an array with the vendor payment data
        return [
            'vendor_id' => $this->vendor->id,
            'period' => $this->period ? date('Y-m-d', strtotime($this->period)) : date('Y-m-d'),
            'bank' => $this->bank,
            'payment_name' => $this->payment_name,
            'payment_number' => $this->payment_number,
        ];
    }

    /**
     * Store the vendor data.
     */
    public function store(): void
    {
        // Validate the vendor data
        $this->validate();

        // Use a database transaction to ensure atomicity
        DB::transaction(function () {
            // Create a new vendor record
            $this->vendor = Vendor::create($this->vendorData());

            // Create a new vendor tax record
            VendorTax::create($this->getVendorTaxData());

            // Create a new vendor payment record
            VendorPayment::create($this->vendorPaymentData());
        }, 5);

        // Emit an event to clear the vendor form
        $this->emit('clearVendorForm');
    }

    /**
     * Edit a vendor by ID.
     *
     * @param  int  $id  The ID of the vendor to edit.
     */
    public function editVendor(int $id): void
    {
        // Retrieve the vendor by ID
        $this->vendor = Vendor::find($id);

        // Assign the vendor properties to the corresponding class properties
        $this->code = $this->vendor->code;
        $this->name = $this->vendor->name;
        $this->phone = $this->vendor->phone;
        $this->address = $this->vendor->address;
        $this->contactPerson = $this->vendor->contact_person;

        // Assign the vendor's tax properties to the corresponding class properties
        $this->company = $this->vendor->tax->company;
        $this->npwp = $this->vendor->tax->npwp;
        $this->npwpName = $this->vendor->tax->npwp_name;
        $this->taxAddress = $this->vendor->tax->tax_address;

        // Assign the vendor's payment properties to the corresponding class properties
        $this->period = date('Y-m-d', strtotime($this->vendor->payment->period));
        $this->bank = $this->vendor->payment->bank;
        $this->payment_name = $this->vendor->payment->payment_name;
        $this->payment_number = $this->vendor->payment->payment_number;

        // Set the action form to 'update' and emit the 'showVendorForm' event
        $this->actionForm = 'update';
        $this->emit('showVendorForm');
    }

    /**
     * Update the vendor data and related models.
     */
    public function update(): void
    {
        // Start a database transaction
        DB::transaction(function () {
            // Update the vendor data
            $this->vendor->update($this->vendorData());

            // Update the vendor tax data
            $this->vendor->tax->update($this->getVendorTaxData());

            // Update the vendor payment data
            $this->vendor->payment->update($this->vendorPaymentData());
        }, 5);

        // Emit an event to clear the vendor form
        $this->emit('clearVendorForm');
    }

    /**
     * Delete a vendor by ID.
     *
     * @param  int  $id  The ID of the vendor to delete.
     */
    public function destroy(int $id): void
    {
        // Find the vendor by ID
        $this->vendor = Vendor::find($id);

        // Start a database transaction
        DB::transaction(function () {
            // Append '_deleted_' followed by the current timestamp to the vendor's code
            $this->vendor->code = $this->vendor->code.'-deleted';
            $this->vendor->save();

            // Delete the vendor
            $this->vendor->delete();

            // Delete associated vendor tax records
            VendorTax::where('vendor_id', $this->vendor->id)->delete();

            // Delete associated vendor payment records
            VendorPayment::where('vendor_id', $this->vendor->id)->delete();
        }, 5);

        // Emit an event to clear the vendor form
        $this->emit('clearVendorForm');
    }

    /**
     * Clear the vendor form by resetting the form fields and error bag,
     * then emit events to hide the form and refresh the vendor data.
     */
    public function clearVendorForm(): void
    {
        $this->reset();
        $this->resetErrorBag();

        $this->emit('hideVendorForm');
        $this->emit('refreshVendor');
    }

    public function render(): View|Factory|Application
    {
        return view('livewire.vendor-form');
    }
}
