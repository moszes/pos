<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class TreatmentNormalFee extends Component
{
    public $treatmentNormalFee = [];

    public $treatment;

    /**
     * Mount the component and initialize fee values.
     *
     * If the treatment exists, update the fee values based on the treatment's normal fees.
     */
    public function mount(): void
    {
        // Initialize the fee values
        $this->treatmentNormalFee = [
            'senior_doctor' => 0, // Fee for the doctor
            'doctor' => 0, // Fee for the doctor
            'doctor_assistance' => 0, // Fee for the doctor's assistance
            'sales' => 0, // Fee for sales
            'therapist' => 0, // Fee for the therapist
        ];

        // If the treatment exists, update the fee values
        if ($this->treatment) {
            foreach ($this->treatment->normalFees as $value) {
                // Update the fee value based on the role
                $this->treatmentNormalFee[$value->role] = countFormat($value->value);
            }
        }
    }

    /**
     * Update the fee values based on the property name and value.
     *
     * @param  string  $propertyName  The name of the property that was updated.
     * @param  mixed  $value  The new value for the property.
     */
    public function updated(string $propertyName, mixed $value): void
    {
        // Check if the property name is one of the fee properties
        if (in_array($propertyName, [
            'treatmentNormalFee.senior_doctor',
            'treatmentNormalFee.doctor',
            'treatmentNormalFee.doctor_assistance',
            'treatmentNormalFee.sales',
            'treatmentNormalFee.therapist',
        ])) {
            // Remove the 'treatmentNormalFee.' prefix from the property name
            $treatmentNormalFeeName = str_replace('treatmentNormalFee.', '', $propertyName);

            // Convert the value to a float and update the treatmentNormalFee property
            $this->treatmentNormalFee[$treatmentNormalFeeName] = floatval($value);

            $this->emit('feeUpdated', 'treatment', 'normal', $this->treatmentNormalFee);
        }
    }

    /**
     * Render the view for the TreatmentNormalFee Livewire component.
     *
     * This method returns the view for the TreatmentNormalFee Livewire component, which is 'livewire.treatment-normal-fee'.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the livewire.treatment-normal-fee view
        return view('livewire.treatment-normal-fee');
    }
}
