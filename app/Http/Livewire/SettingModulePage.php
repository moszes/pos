<?php

namespace App\Http\Livewire;

use Illuminate\View\View;
use Livewire\Component;

class SettingModulePage extends Component
{
    public $companyCode;

    public $branchCode;

    protected $queryString = [
        'companyCode',
        'branchCode',
    ];

    public function mount(): void
    {
        $this->companyCode = session('company_code');
        $this->branchCode = session('branch_code');
    }

    /**
     * Renders the 'livewire.setting-module-page' view and returns it as a View object.
     *
     * @return View The rendered view.
     */
    public function render(): View
    {
        return view('livewire.setting-module-page');
    }
}
