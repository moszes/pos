<?php

namespace App\Http\Livewire;

use App\Models\MarketPlace;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class MarketPlaceMaterialUsagePage extends Component
{
    public $marketplaceId;

    public $marketplace;

    public $moduleLabel = 'Item Details';

    public $moduleName = 'marketplace';

    public $redirectRoute;

    public $priceLabel = true;

    /**
     * Mount the component.
     */
    public function mount(): void
    {
        $this->marketplaceId = request()->segment(count(request()->segments()));

        // Find the marketplace by ID
        $this->marketplace = MarketPlace::find($this->marketplaceId);
        $this->redirectRoute = route('master.market_places', ['search' => $this->marketplace->code]);

        if (! $this->marketplace) {
            redirect($this->redirectRoute);
        }
    }

    /**
     * Render the component.
     */
    public function render(): View
    {
        return view('livewire.market-place-material-usage-page', [
            'marketplace' => MarketPlace::find($this->marketplaceId),
        ]);
    }
}
