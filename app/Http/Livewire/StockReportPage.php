<?php

namespace App\Http\Livewire;

use Livewire\Component;

class StockReportPage extends Component
{
    public function render()
    {
        return view('livewire.stock-report-page');
    }
}
