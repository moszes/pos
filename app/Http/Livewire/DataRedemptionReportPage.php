<?php

namespace App\Http\Livewire;

use Livewire\Component;

class DataRedemptionReportPage extends Component
{
    public $customer;

    public function render()
    {
        return view('livewire.user-transaction-report-page');
    }
}
