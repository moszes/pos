<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class RolePage extends Component
{
    /**
     * Renders the role-page view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.role-page');
    }
}
