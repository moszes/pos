<?php

namespace App\Http\Livewire;

use App\Http\Controllers\UserController;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithFileUploads;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Storage;

class AvatarFileUpload extends Component
{
    use WithFileUploads;

    public $user;

    public $avatar;

    public $avatarImage;

    public $userCode;

    protected $queryString = ['userCode'];

    /**
     * Mount the component.
     */
    public function mount(): void
    {
        $this->user = UserController::findUserByCode($this->userCode)->user;

        $this->avatar = UserController::avatarImage($this->user);
    }

    /**
     * Update the cover image.
     *
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function updatedAvatarImage(): void
    {
        $this->validate([
            'avatarImage' => 'image', // Validate that the upload is an image
        ]);

        DB::transaction(function () {
            if ($this->avatarImage && $this->avatarImage->isValid()) {

                $imageUrl = $this->avatarImage->store('photos', 'gcs');

                $media = $this->user->addMediaFromUrl(Storage::url($imageUrl))->toMediaCollection('users');

                $this->user->details->avatar_file_name = $media->getUrl('thumb-cropped');
                $this->user->details->save();

                UserController::avatarImage($this->user);

                $this->avatar = $this->user->details->avatar_file_name;
                $this->emit('refreshAvatarImage');
            }
        }, 5);
    }

    /**
     * Render the view for the avatar file upload component.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.avatar-file-upload');
    }
}
