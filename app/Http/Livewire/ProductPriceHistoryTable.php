<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Spatie\Activitylog\Models\Activity;

class ProductPriceHistoryTable extends Component
{
    public $product;

    /**
     * Render the view for the Livewire component.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {

        // Render the 'livewire.product-price-history-table' view
        return view('livewire.product-price-history-table', [
            'priceHistories' => Activity::where('log_name', 'Product Log')
                ->where('subject_type', 'App\Models\Product')
                ->where('subject_id', $this->product->id)
                ->orderBy('id', 'desc')
                ->limit(5)
                ->get(),
        ]);
    }
}
