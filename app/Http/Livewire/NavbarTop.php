<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class NavbarTop extends Component
{
    /**
     * Renders the navbar-top view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.navbar-top');
    }
}
