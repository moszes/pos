<?php

namespace App\Http\Livewire;

use App\Models\ApplicationSetting;
use App\Models\Branch;
use App\Models\Company;
use Illuminate\View\View;
use Livewire\Component;

class SettingModuleForm extends Component
{
    public $formTitle = 'Setting Module';

    public $actionForm = 'store';

    public $company;

    public $branch;

    public $companyCode;

    public $branchCode;

    public $name;

    public $status;

    public $module;

    public $value;

    public $redirectUrl;

    public $applicationSetting;

    protected $queryString = [
        'companyCode',
        'branchCode',
    ];

    protected $listeners = [
        'editSetting',
    ];

    /**
     * Mounts the component.
     */
    public function mount(): void
    {
        $this->companyCode = session('company_code');
        $this->branchCode = session('branch_code');

        $this->company = Company::where(
            'code', $this->companyCode
        )->first();

        $this->company = Company::find($this->company->id);

        $this->branch = Branch::where(
            'code', $this->branchCode
        )->first();
    }

    /**
     * Returns the data of the application setting.
     */
    public function applicationSettingData(): array
    {
        return [
            'company_id' => $this->company->id,
            'company_code' => $this->companyCode,
            'name' => $this->name,
            'status' => $this->status,
            'module' => $this->module,
            'value' => $this->value,
            'redirect_url' => $this->redirectUrl,
        ];
    }

    public function store(): void
    {
        $this->validate([
            'name' => 'required|string|unique:application_settings',
            'value' => 'required|string',
        ]);

        ApplicationSetting::create($this->applicationSettingData());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function editSetting($settingId): void
    {
        $this->actionForm = 'update';

        $this->formTitle = 'Edit Setting';

        $this->applicationSetting = ApplicationSetting::find($settingId);
        $this->name = $this->applicationSetting->name;
        $this->status = $this->applicationSetting->status;
        $this->module = $this->applicationSetting->module;
        $this->value = $this->applicationSetting->value;
        $this->redirectUrl = $this->applicationSetting->redirect_url;

        $this->emit('showSettingModuleForm');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(): void
    {
        $this->validate([
            'name' => 'required',
            'value' => 'required',
        ]);

        $this->applicationSetting->update($this->applicationSettingData());

        $this->emit('hideSettingModuleForm');
        $this->emit('refresh');
    }

    /**
     * Renders the view for the module form.
     *
     * @return View The rendered view.
     */
    public function render(): View
    {
        return view('livewire.setting-module-form');
    }
}
