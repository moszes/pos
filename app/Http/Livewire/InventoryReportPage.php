<?php

namespace App\Http\Livewire;

use Livewire\Component;

class InventoryReportPage extends Component
{
    public function render()
    {
        return view('livewire.inventory-report-page');
    }
}
