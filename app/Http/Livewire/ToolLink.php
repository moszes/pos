<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class ToolLink extends Component
{
    /**
     * Render the view for the ToolLink component.
     */
    public function render(): Factory|View|Application
    {
        // Return the view for the ToolLink component
        return view('livewire.tool-link');
    }
}
