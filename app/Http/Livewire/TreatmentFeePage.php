<?php

namespace App\Http\Livewire;

use App\Models\Treatment;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\View\View;
use Livewire\Component;

class TreatmentFeePage extends Component
{
    public $treatmentId;

    public $treatment;

    public $moduleLabel = 'Treatment Details';

    public $moduleName = 'treatment';

    public $redirectRoute;

    public $priceLabel = true;

    /**
     * Mounts the component and initializes the necessary data.
     *
     * This function retrieves the treatment ID from the request segment, initializes the fee values,
     * finds the treatment by ID, and updates the fee values if the treatment exists.
     */
    public function mount(): void
    {
        // Get the treatment ID from the request segment
        $this->treatmentId = request()->segment(count(request()->segments()));

        // Find the treatment by ID
        $this->treatment = Treatment::find($this->treatmentId);

        $this->redirectRoute = route('master.treatments', ['search' => $this->treatment->code]);

        if (! $this->treatment) {
            redirect($this->redirectRoute);
        }
    }

    /**
     * Render the treatment fee page view.
     */
    public function render(): Factory|Application|View
    {
        // Retrieve the treatment using the treatmentId property
        $treatment = Treatment::find($this->treatmentId);

        // Return the livewire.treatment-fee-page view with the treatment data
        return view('livewire.treatment-fee-page', [
            'treatment' => $treatment,
        ]);
    }
}
