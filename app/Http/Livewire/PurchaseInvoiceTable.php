<?php

namespace App\Http\Livewire;

use App\Models\PurchaseInvoice;
use Livewire\Component;
use Livewire\WithPagination;

class PurchaseInvoiceTable extends Component
{
    use WithPagination;

    public $titlePage = 'Purchase Invoice';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshPurchaseInvoice' => '$refresh',
    ];

    public function paginationView()
    {
        return 'pagination';
    }

    public function render()
    {
        return view('livewire.purchase-invoice-table', [
            'purchase_invoices' => PurchaseInvoice::where('invoice_number', 'like', '%'.$this->search.'%')
                ->orderBy('id', 'desc')
                ->paginate(10),
        ]);
    }
}
