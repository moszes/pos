<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class ServiceLink extends Component
{
    /**
     * Render the component.
     */
    public function render(): View
    {
        return view('livewire.service-link');
    }
}
