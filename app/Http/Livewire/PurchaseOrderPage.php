<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PurchaseOrderPage extends Component
{
    public function render()
    {
        return view('livewire.purchase-order-page');
    }
}
