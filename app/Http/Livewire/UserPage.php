<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class UserPage extends Component
{
    /**
     * Render the user page view.
     */
    public function render(): View
    {
        // Return the user page view
        return view('livewire.user-page');
    }
}
