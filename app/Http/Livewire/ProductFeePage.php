<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\View\View;
use Livewire\Component;

class ProductFeePage extends Component
{
    public $productId;

    public $product;

    public $moduleName = 'product';

    public $moduleLabel = 'Product Details';

    public $priceLabel = true;

    public $redirectRoute;

    /**
     * Mounts the component and initializes the necessary data.
     *
     * This function retrieves the product ID from the request segment, initializes the fee values,
     * finds the product by ID, and updates the fee values if the product exists.
     */
    public function mount(): void
    {
        // Get the product ID from the request segment
        $this->productId = request()->segment(count(request()->segments()));

        // Find the product by ID
        $this->product = Product::find($this->productId);
        $this->redirectRoute = route('master.products', ['search' => $this->product->code]);

        $this->moduleName = 'product';

        if (! $this->product) {
            redirect($this->redirectRoute);
        }
    }

    /**
     * Render the product fee page view.
     */
    public function render(): Factory|Application|View
    {
        // Return the livewire.product-fee-page view with the product data
        return view('livewire.product-fee-page', [
            'product' => Product::find($this->productId),
        ]);
    }
}
