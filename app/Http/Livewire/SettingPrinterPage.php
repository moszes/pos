<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Livewire\Component;

class SettingPrinterPage extends Component
{
    /**
     * Renders the setting-printer-page view.
     */
    public function render(): View|Application|Factory
    {
        return view('livewire.setting-printer-page');
    }
}
