<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PackageTargetFee extends Component
{
    public $packageTargetFee = [];

    public $package;

    /**
     * Mount the component and initialize fee values.
     *
     * If the package exists, update the fee values based on the package's target fees.
     */
    public function mount(): void
    {
        // Initialize the fee values
        $this->packageTargetFee = [
            'senior_doctor' => 0, // Fee for the senior doctor
            'doctor' => 0, // Fee for the doctor
            'doctor_assistance' => 0, // Fee for the doctor's assistance
            'sales' => 0, // Fee for sales
            'therapist' => 0, // Fee for the therapist
        ];

        // If the package exists, update the fee values
        if ($this->package) {
            foreach ($this->package->targetFees as $value) {
                // Update the fee value based on the role
                $this->packageTargetFee[$value->role] = countFormat($value->value);
            }
        }
    }

    /**
     * Update the fee values based on the property name and value.
     *
     * @param  string  $propertyName  The name of the property that was updated.
     * @param  mixed  $value  The new value for the property.
     */
    public function updated(string $propertyName, mixed $value): void
    {
        // Check if the property name is one of the fee properties
        if (in_array($propertyName, [
            'packageTargetFee.senior_doctor',
            'packageTargetFee.doctor',
            'packageTargetFee.doctor_assistance',
            'packageTargetFee.sales',
            'packageTargetFee.therapist',
        ])) {
            // Remove the 'packageTargetFee.' prefix from the property name
            $packageTargetFeeName = str_replace('packageTargetFee.', '', $propertyName);

            // Convert the value to a float and update the packageTargetFee property
            $this->packageTargetFee[$packageTargetFeeName] = floatval($value);

            $this->emit('feeUpdated', 'package', 'target', $this->packageTargetFee);
        }
    }

    /**
     * Render the view for the PackageTargetFee Livewire component.
     *
     * This method returns the view for the PackageTargetFee Livewire component, which is 'livewire.package-target-fee'.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the livewire.package-target-fee view
        return view('livewire.package-target-fee');
    }
}
