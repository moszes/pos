<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class ItemDetail extends Component
{
    public $item;

    public $redirectRoute;

    public $moduleName;

    public $priceLabel;

    public $moduleLabel;

    public $search;

    /**
     * Renders the view for the item fee header.
     */
    public function render(): View
    {
        return view('livewire.item-detail');
    }
}
