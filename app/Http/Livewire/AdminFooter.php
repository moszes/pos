<?php

namespace App\Http\Livewire;

use App\Models\Company;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class AdminFooter extends Component
{
    public $company;

    public $gitTag;

    public function mount(): void
    {
        $this->company = Company::first()->name;
        $this->gitTag = 'v'.json_decode(file_get_contents(base_path('package.json')))->version;
    }

    public function render(): Factory|View|Application
    {
        return view('livewire.admin-footer');
    }
}
