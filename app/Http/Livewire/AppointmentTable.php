<?php

namespace App\Http\Livewire;

use App\Models\Appointment;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class AppointmentTable extends Component
{
    use WithPagination;

    public $titlePage = 'Appointment';

    public $search;

    public $appointment;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshAppointment' => '$refresh',
        'editAppointment',
        'destroy',
    ];

    /**
     * Load an input form with value from table appointment
     */
    public function editAppointment($id): void
    {
        $appointment = Appointment::find($id);
        $this->emit('selectCustomer', $appointment->customer_id);
        $this->emit('cartOpen', $id);
        $this->emit('setActionButton', 'update');
    }

    /**
     * Returns the view for pagination.
     *
     * @return string The name of the pagination view.
     */
    public function paginationView(): string
    {
        // Return the name of the pagination view.
        return 'pagination';
    }

    /**
     * Delete an appointment by ID.
     *
     * @param  int  $id  The ID of the appointment to delete.
     */
    public function destroy(int $id): void
    {
        // Find the appointment by ID
        $this->appointment = Appointment::find($id);

        // Use a database transaction to delete the appointment
        DB::transaction(function () {
            $this->appointment->delete();
        }, 5);
    }

    /**
     * Render the view for the appointment table with filtered appointments.
     */
    public function render(): Factory|View|Application
    {
        // Retrieve appointments based on search keyword, company ID, today's date, and specific conditions
        $appointments = Appointment::where('appointment_number', 'like', '%'.$this->search.'%')
            ->where('company_id', auth()->user()->details->branch_id)
            ->whereDate('appointment_date', today())
            ->whereNull('reference_module')
            ->orderBy('id', 'desc')
            ->paginate(10);

        // Return the view with the appointments data
        return view('livewire.appointment-table', [
            'appointments' => $appointments,
        ]);
    }
}
