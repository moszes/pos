<?php

namespace App\Http\Livewire;

use Hash;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class UserDetailForm extends Component
{
    public $user;

    public $email;

    public $username;

    public $firstName;

    public $lastName;

    public $phone;

    public $address;

    public $instagram;

    public $dateOfBirth;

    public $placeOfBirth;

    public $companyName;

    public $companyWebsite;

    public $password;

    public $newPassword;

    public $newPasswordConfirmation;

    protected $listeners = [
        'clearPassword',
    ];

    /**
     * Initialize the component.
     *
     * This method is called when the component is being initialized.
     * It retrieves the user's profile data from the database and populates the component's properties.
     */
    public function mount(): void
    {
        $this->username = $this->user?->name;
        $this->firstName = $this->user?->details->first_name;
        $this->lastName = $this->user?->details->last_name;
        $this->email = $this->user?->details->email;
        $this->instagram = $this->user?->details->instagram;
        $this->dateOfBirth = $this->user?->details->date_of_birth;
        $this->placeOfBirth = $this->user?->details->place_of_birth;
        $this->phone = $this->user?->details->phone;
        $this->address = $this->user?->details->address;
        $this->companyName = $this->user?->details->company->name;
        $this->companyWebsite = $this->user?->details->company->website;
    }

    /**
     * Update the user profile.
     */
    public function updateProfile(): void
    {
        $this->user->update([
            'name' => $this->username,
            'email' => $this->email,
        ]);

        $this->user->details->update([
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'phone' => $this->phone,
            'address' => $this->address,
            'instagram' => $this->instagram,
            'email' => $this->email,
            'date_of_birth' => $this->dateOfBirth,
            'place_of_birth' => $this->placeOfBirth,
        ]);

        if ($this->password != '' && $this->newPassword != '' && $this->newPasswordConfirmation != '') {

            if (! $this->checkPassword()) {
                $this->emit('wrongPassword');
                $this->clearPassword();

                return;
            } else {

                if (! $this->passwordConfirm()) {
                    $this->emit('newPasswordNotMatched');
                    $this->clearPassword();

                    return;
                } else {
                    $this->user->password = bcrypt($this->newPassword);
                    $this->user->save();
                    auth()->logout();

                    redirect()->route('landingPage');
                }
            }
        }

        $this->emit('clearPassword');
        $this->emit('dataUpdated');
    }

    /**
     * Check if the provided password matches the user's stored password.
     */
    public function checkPassword(): bool
    {
        return Hash::check($this->password, $this->user->password);
    }

    /**
     * Checks if the new password matches the new password confirmation.
     *
     * @return bool Returns true if the new password matches the new password confirmation.
     */
    public function passwordConfirm(): bool
    {
        return $this->newPassword == $this->newPasswordConfirmation;
    }

    /**
     * Clears the password fields.
     */
    public function clearPassword(): void
    {
        $this->password = '';
        $this->newPassword = '';
        $this->newPasswordConfirmation = '';
    }

    /**
     * Renders the user detail form view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.user-detail-form');
    }
}
