<?php

namespace App\Http\Livewire;

use Livewire\Component;

class StockForm extends Component
{
    public function render()
    {
        return view('livewire.stock-form');
    }
}
