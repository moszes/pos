<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\UserDetail;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class CustomerForm extends Component
{
    public $formTitle = 'Form Customer';

    public $actionForm = 'store';

    public $user;

    public $userName;

    public $email;

    public $firstName;

    public $lastName;

    public $instagram;

    public $dateOfBirth;

    public $placeOfBirth;

    public $phone;

    public $address;

    protected $listeners = [
        'clearCustomerForm',
        'editCustomer',
        'destroy',
    ];

    protected $rules = [
        'userName' => 'required',
        'phone' => 'required|unique:user_details',
    ];

    /**
     * Store a new user and user detail.
     */
    public function store(): void
    {
        // Validate the request data
        $this->validate();

        // Use a database transaction to ensure data consistency
        DB::transaction(function () {
            // Create a new user record
            $this->user = User::create([
                'name' => $this->userName,
                'email' => $this->email ?? generateCustomerEmail(),
                'password' => bcrypt('ministry'),
            ]);

            // Assign the 'customer' role to the user
            $this->user->assignRole('customer');

            // Create a new user detail record
            UserDetail::create([
                'user_id' => $this->user->id,
                'company_id' => auth()->user()->details->branch_id,
                'branch_id' => auth()->user()->details->branch_id,
                'code' => generateUserCode(),
                'email' => $this->email,
                'first_name' => $this->firstName,
                'instagram' => $this->instagram,
                'date_of_birth' => $this->dateOfBirth,
                'place_of_birth' => $this->placeOfBirth,
                'last_name' => $this->lastName,
                'phone' => $this->phone,
                'address' => $this->address,
            ]);
        }, 5);

        // Emit an event to clear the customer form
        $this->emit('clearCustomerForm');
    }

    /**
     * Edit a customer by their ID.
     *
     * @param  int  $id  The ID of the customer to edit.
     */
    public function editCustomer(int $id): void
    {
        // Find the user by ID
        $this->user = User::find($id);

        // Set the username
        $this->userName = $this->user->name;

        // Set the user details
        $this->firstName = $this->user->details->first_name;
        $this->lastName = $this->user->details->last_name;
        $this->email = $this->user->email;
        $this->phone = $this->user->details->phone;
        $this->address = $this->user->details->address;

        // Set the action form
        $this->actionForm = 'update';

        // Emit an event to show the customer form
        $this->emit('showCustomerForm');
    }

    /**
     * Update the user and user details in the database.
     */
    public function update(): void
    {
        // Start a database transaction
        DB::transaction(function () {
            // Update the user's name and email
            $this->user->update([
                'name' => $this->userName,
                'email' => $this->email,
            ]);

            // Update the user's details
            $this->user->details->update([
                'first_name' => $this->firstName,
                'last_name' => $this->lastName,
                'phone' => $this->phone,
                'email' => $this->email,
                'address' => $this->address,
            ]);
        }, 5);

        // Emit an event to clear the customer form
        $this->emit('clearCustomerForm');
    }

    /**
     * Destroy a user by their ID.
     *
     * @param  int  $id  The ID of the user to destroy.
     */
    public function destroy(int $id): void
    {
        // Find the user by their ID
        $this->user = User::find($id);

        // Start a database transaction
        DB::transaction(function () use ($id) {
            // Delete the user
            $this->user->delete();

            // Find the user details by the user ID
            $userDetails = UserDetail::where('user_id', $id)->first();

            // Update the phone number of the user details
            $userDetails->update([
                'phone' => 'deleted-'.$userDetails->phone,
                'code' => 'deleted-'.$userDetails->code,
                'email' => 'deleted-'.$userDetails->email,
            ]);

            // Delete the user details
            $userDetails->delete();
        }, 5);

        // Emit an event to clear the customer form
        $this->emit('clearCustomerForm');
    }

    /**
     * Clears the customer form by resetting the form data and error bag
     * and emitting events to refresh the customer data and hide the customer form.
     */
    public function clearCustomerForm(): void
    {
        $this->reset(); // Reset the form data
        $this->resetErrorBag(); // Reset the error bag

        $this->emit('refreshCustomer'); // Emit event to refresh the customer data
        $this->emit('hideCustomerForm'); // Emit event to hide the customer form
    }

    /**
     * Render the customer form view.
     */
    public function render(): View|Factory|Application
    {
        // Return the customer form view
        return view('livewire.customer-form');
    }
}
