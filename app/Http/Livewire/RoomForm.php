<?php

namespace App\Http\Livewire;

use App\Models\Room;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class RoomForm extends Component
{
    public $formTitle = 'Form Room';

    public $actionForm = 'store';

    public $room;

    public $code;

    public $name;

    protected $listeners = [
        'clearRoomForm',
        'editRoom',
        'destroy',
    ];

    protected $rules = [
        'code' => 'required|unique:rooms',
        'name' => 'required',
    ];

    /**
     * Retrieves the room data.
     *
     * @return array The room data.
     */
    public function roomData(): array
    {
        return [
            // Retrieve the company ID from the first company
            'company_id' => auth()->user()->details->branch_id,
            // Retrieve the branch ID from the first branch
            'branch_id' => auth()->user()->details->branch_id,
            // Set the room code
            'code' => $this->code,
            // Set the room name
            'name' => $this->name,
        ];
    }

    /**
     * Store a new room.
     */
    public function store(): void
    {
        // Validate the data
        $this->validate();

        // Start a database transaction
        DB::transaction(function () {
            // Create a new room with the provided data
            Room::create($this->roomData());
        }, 5);

        // Emit an event to clear the room form
        $this->emit('clearRoomForm');
    }

    /**
     * Edit a room.
     *
     * @param  int  $id  The ID of the room to be edited.
     */
    public function editRoom(int $id): void
    {
        // Find the room with the given ID
        $this->room = Room::find($id);

        // Set the code and name variables to the room's code and name
        $this->code = $this->room->code;
        $this->name = $this->room->name;

        // Set the actionForm variable to 'update'
        $this->actionForm = 'update';

        // Emit the 'showRoomForm' event
        $this->emit('showRoomForm');
    }

    public function update(): void
    {
        DB::transaction(function () {
            $this->room->update($this->roomData());
        }, 5);

        $this->emit('clearRoomForm');
    }

    /**
     * Delete a room by ID.
     *
     * @param  int  $id  The ID of the room to delete.
     */
    public function destroy(int $id): void
    {
        // Find the room by ID
        $this->room = Room::find($id);

        // Use a database transaction to ensure atomicity
        DB::transaction(function () {
            // Append '_deleted' and the current timestamp to the room code
            $this->room->code = $this->room->code.'_deleted'.time();

            // Save the updated room code
            $this->room->save();

            // Delete the room
            $this->room->delete();
        }, 5);

        // Emit an event to clear the room form
        $this->emit('clearRoomForm');
    }

    /**
     * Clears the room form by resetting the form data and error bag,
     * and emits events to refresh the room and hide the room form.
     */
    public function clearRoomForm(): void
    {
        $this->reset(); // Reset the form data
        $this->resetErrorBag(); // Reset the error bag

        $this->emit('refreshRoom'); // Emit 'refreshRoom' event
        $this->emit('hideRoomForm'); // Emit 'hideRoomForm' event
    }

    public function render(): View|Factory|Application
    {
        return view('livewire.room-form');
    }
}
