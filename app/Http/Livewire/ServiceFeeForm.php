<?php

namespace App\Http\Livewire;

use App\Models\Product;
use App\Models\ServiceFee;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class ServiceFeeForm extends Component
{
    public Product $product;

    public $roles = ServiceFee::ROLES;

    public $modules = ServiceFee::MODULES;

    public $moduleName;

    public $item;

    public $normalFees;

    public $normalFee;

    public $discountFees;

    public $discountFee;

    public $targetFees;

    public $targetFee;

    public $fees = [];

    public $listeners = [
        'feeUpdated',
    ];

    /**
     * Mount the component.
S     */
    public function mount(): void
    {
        $this->checkIfProductHasFee($this->moduleName, $this->item);

        $this->normalFees = $this->item->normalFees;
        $this->discountFees = $this->item->discountFees;
        $this->targetFees = $this->item->targetFees;

        foreach ($this->roles as $role) {
            $this->normalFee[$this->moduleName][$role]['fee_symbol'] = $this->item->normalFees->where('role', $role)->first()->fee_symbol;
            $this->normalFee[$this->moduleName][$role]['value'] = (float) $this->item->normalFees->where('role', $role)->first()->value;
            $this->discountFee[$this->moduleName][$role]['fee_symbol'] = $this->item->discountFees->where('role', $role)->first()->fee_symbol;
            $this->discountFee[$this->moduleName][$role]['value'] = (float) $this->item->discountFees->where('role', $role)->first()->value;
            $this->targetFee[$this->moduleName][$role]['fee_symbol'] = $this->item->targetFees->where('role', $role)->first()->fee_symbol;
            $this->targetFee[$this->moduleName][$role]['value'] = (float) $this->item->targetFees->where('role', $role)->first()->value;
        }

    }

    /**
     * Update the product's normal fee in the database.
     */
    public function updatedNormalFee($value, $key): void
    {
        $role = explode('.', $key)[1];
        $field = explode('.', $key)[2];

        $fee = $this->item->normalFees->where('role', $role)->first();

        $fee->update([
            $field => $value,
        ]);

        foreach ($this->modules as $module) {
            $this->normalFee[$module][$role][$field] = $field == 'fee_symbol' ? $value : (float) $value;
        }

        $this->emit('dataSaved');
    }

    /**
     * Update the product's discount fee in the database.
     */
    public function updatedDiscountFee($value, $key): void
    {
        $role = explode('.', $key)[1];
        $field = explode('.', $key)[2];

        $fee = $this->item->discountFees->where('role', $role)->first();
        $fee->update([
            $field => $value,
        ]);

        foreach ($this->modules as $module) {
            $this->discountFee[$module][$role][$field] = $field == 'fee_symbol' ? $value : (float) $value;
        }

        $this->emit('dataSaved');
    }

    /**
     * Update the product's target fee in the database.
     */
    public function updatedTargetFee($value, $key): void
    {
        $role = explode('.', $key)[1];
        $field = explode('.', $key)[2];

        $fee = $this->item->targetFees->where('role', $role)->first();
        $fee->update([
            $field => $value,
        ]);

        foreach ($this->modules as $module) {
            $this->targetFee[$module][$role][$field] = $field == 'fee_symbol' ? $value : (float) $value;
        }

        $this->emit('dataSaved');
    }

    /**
     * Check if a product has service fees in the database.
     */
    public function checkIfProductHasFee($moduleName, $item): void
    {
        $serviceFee = ServiceFee::where('module', $moduleName)->where('module_id', $item->id)->first();
        if (! $serviceFee) {
            foreach (ServiceFee::ROLES as $role) {
                foreach (ServiceFee::FEE_TYPE as $type) {
                    ServiceFee::create([
                        'module' => $moduleName,
                        'module_id' => $item->id,
                        'role' => $role,
                        'value' => 0,
                        'fee_type' => $type,
                        'fee_symbol' => 'fixed',
                    ]);
                }
            }
        }
    }

    /**
     * Update the fees for a specific module and type.
     *
     * @param  string  $module  The module for which the fees are being updated.
     * @param  string  $type  The type of fee being updated.
     * @param  mixed  $fees  The new value for the fee.
     */
    public function feeUpdated(string $module, string $type, mixed $fees): void
    {
        // add the fee to the fees array
        $this->fees[$module][$type] = $fees;
    }

    /**
     * Store the product fees in the database.
     */
    public function store(): void
    {
        // Iterate over each fee in the fee array
        foreach ($this->fees as $module => $fees) {
            // Start a database transaction
            DB::transaction(function () use ($module, $fees) {

                foreach ($fees as $type => $fee) {
                    foreach ($fee as $role => $value) {

                        // Delete the commission fee from the database
                        deleteCommissionFee($module, $this->product->id, $type, $role);

                        // Get the fee symbol based on the role
                        $feeSymbol = in_array($fee, ['doctor_assistance', 'therapist']) ? 'fixed' : 'percentage';

                        // Update or create a product fee record in the database
                        ServiceFee::create([
                            'module' => $module,
                            'module_id' => $this->product->id,
                            'role' => $role,
                            'value' => $value,
                            'fee_type' => $type,
                            'fee_symbol' => $feeSymbol,
                        ]);
                    }
                }
            }, 5);
        }

        // Redirect to the master products page
        $this->emit('productFeeUpdated');
    }

    public function setDiscountFees($discountFees): static
    {
        $this->discountFees = $discountFees;

        return $this;
    }

    /**
     * Render the view for the Service Fee Detail Livewire component.
     */
    public function render(): Factory|View|Application
    {
        // Return the livewire.product-fee-detail view
        return view('livewire.service-fee-form');
    }
}
