<?php

namespace App\Http\Livewire;

use App\Models\Company;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithFileUploads;

class SettingCompanyPage extends Component
{
    use WithFileUploads;

    public $company;

    public $companyName;

    public $companyEmail;

    public $companyPhone;

    public $companyAddress;

    public $companyWebsite;

    public $companyLogo;

    public $logoPath;

    /**
     * Mount the component.
     */
    public function mount(): void
    {
        $this->company = Company::first();
        $this->companyName = auth()->user()?->details->company->name;
        $this->companyWebsite = auth()->user()?->details->company->website;
        $this->companyPhone = auth()->user()?->details->company->phone;
        $this->companyAddress = auth()->user()?->details->company->address;
        $this->companyEmail = auth()->user()?->details->company->email;
    }

    /**
     * Update the company details.
     *
     * If the company logo is provided, validate the file type and size.
     * Store the logo in the "photos" directory with a unique name based on the company's name and current environment.
     *
     * Update the company record in the database with the provided details.
     *
     * Emit the "companyUpdated" and "dataSaved" events.
     */
    public function updateCompany(): void
    {
        if ($this->companyLogo) {
            $this->validate([
                'companyLogo' => 'mimes:jpg,bmp,png|max:5024', // 5MB Max
            ]);

            $filename = strtolower(str_replace(' ', '_', $this->companyName)).'.'.$this->companyLogo->getClientOriginalExtension();

            $this->logoPath = $this->companyLogo->storePubliclyAs('photos', 'logo_'.$filename, 'public');

            $this->emit('refreshLogoImage', $this->logoPath);
        }

        $this->company->update([
            'name' => $this->companyName,
            'email' => $this->companyEmail,
            'address' => $this->companyAddress,
            'phone' => $this->companyPhone,
            'website' => $this->companyWebsite,
            'logo_path' => $this->logoPath,
        ]);

        $this->emit('dataSaved');
    }

    /**
     * Render the component's view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.setting-company-page');
    }
}
