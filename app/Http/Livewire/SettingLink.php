<?php

namespace App\Http\Livewire;

use Illuminate\View\View;
use Livewire\Component;

class SettingLink extends Component
{
    /**
     * Render the component.
     */
    public function render(): View
    {
        return view('livewire.setting-link');
    }
}
