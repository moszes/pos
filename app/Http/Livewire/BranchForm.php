<?php

namespace App\Http\Livewire;

use App\Models\Branch;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class BranchForm extends Component
{
    public $formTitle = 'Form Branch';

    public $actionForm = 'store';

    public $branch;

    public $code;

    public $name;

    protected $listeners = [
        'clearBranchForm',
        'editBranch',
        'destroy',
    ];

    protected $rules = [
        'code' => 'required|unique:categories',
        'name' => 'required',
    ];

    /**
     * Retrieves the branch data.
     *
     * @return array The branch data.
     */
    public function getBranchData(): array
    {
        // Get the first company ID
        $companyId = auth()->user()->details->company_id;

        // Get the first branch ID
        $branchId = auth()->user()->details->branch_id;

        // Build and return the branch data array
        return [
            'company_id' => $companyId,
            'branch_id' => $branchId,
            'code' => $this->code,
            'name' => $this->name,
        ];
    }

    /**
     * Store a new branch.
     */
    public function store(): void
    {
        // Validate the branch data
        $this->validate();

        // Use a transaction to ensure data integrity
        DB::transaction(function () {
            // Create a new branch
            Branch::create($this->getBranchData());

        }, 5);

        // Emit an event to clear the branch form inputs
        $this->emit('clearBranchForm');
    }

    /**
     * Edits a branch by retrieving its details and preparing the form for update.
     *
     * @param  int  $id  The ID of the branch to edit.
     */
    public function editBranch(int $id): void
    {
        // Retrieve the branch by ID
        $this->branch = Branch::find($id);

        // Set the branch details to form fields
        $this->code = $this->branch->code;
        $this->name = $this->branch->name;

        // Set the form action and emit an event to show the branch form
        $this->actionForm = 'update';
        $this->emit('showBranchForm');
    }

    /**
     * Update the branch and perform the necessary actions.
     */
    public function update(): void
    {
        DB::transaction(function () {
            // Update the branch
            $this->branch->update($this->getBranchData());

            // Emit an event to clear the branch form inputs
            $this->emit('clearBranchForm');
        }, 5);
    }

    /**
     * Delete a branch by its ID.
     *
     * @param  int  $id  The ID of the branch to delete.
     */
    public function destroy(int $id): void
    {
        $this->branch = Branch::find($id);

        DB::transaction(function () {
            $this->branch->code = $this->branch->code.'-deleted';
            $this->branch->save();
            // Find the branch by its ID and delete it
            $this->branch->delete();

        }, 5);

        $this->emit('clearBranchForm');
    }

    /**
     * Clear the branch form by resetting the form data and error bag.
     */
    public function clearBranchForm(): void
    {
        $this->reset();
        $this->resetErrorBag();

        // Emit an event to hide the branch form
        $this->emit('hideBranchForm');
        $this->emit('refreshBranch');
    }

    /**
     * Render the branch form view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.branch-form');
    }
}
