<?php

namespace App\Http\Livewire;

use App\Models\MarketPlace;
use DB;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Livewire\Component;

class MarketPlaceForm extends Component
{
    public $formTitle = 'MarketPlace Form';

    public $actionForm = 'store';

    public $categoryId;

    public $subCategoryId;

    public $type = 'treatment';

    public $code;

    public $name;

    public $price = 0;

    public $marketplace;

    protected $listeners = [
        'categorySelected',
        'subCategorySelected',
        'resetSubCategoryId',
        'clearMarketPlaceForm',
        'editMarketPlace',
        'destroy',
    ];

    protected $rules = [
        'code' => 'required|unique:market_places',
        'name' => 'required',
    ];

    /**
     * Update the price value.
     *
     * @param  mixed  $value  The new price value.
     */
    public function updatedPrice(mixed $value): void
    {
        // Convert the value to a float and assign it to the price property.
        $this->price = floatval($value);
    }

    /**
     * Sets the selected category ID.
     *
     * @param  mixed  $categoryId  The ID of the selected category.
     */
    public function categorySelected(mixed $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * Sets the selected subCategory ID.
     *
     * @param  mixed  $subCategoryId  The ID of the selected category.
     */
    public function subCategorySelected(mixed $subCategoryId): void
    {
        $this->subCategoryId = $subCategoryId;
    }

    /**
     * Reset the subCategoryId property.
     */
    public function resetSubCategoryId(): void
    {
        $this->reset('subCategoryId');
    }

    /**
     * Generate an array of marketplace data.
     *
     * @return array The generated marketplace data.
     */
    public function marketPlaceData(): array
    {
        // Get the ID of the first company
        $companyId = auth()->user()->details->company_id;

        // Get the ID of the first branch
        $branchId = auth()->user()->details->branch_id;

        // Generate the marketplace data array
        return [
            'company_id' => $companyId,
            'branch_id' => $branchId,
            'category_id' => $this->categoryId === '-- Choose --' ? null : $this->categoryId,
            'sub_category_id' => $this->subCategoryId === '-- Choose --' ? null : $this->subCategoryId,
            'code' => $this->code,
            'name' => $this->name,
            'price' => $this->price ?? 0,
        ];
    }

    /**
     * Store the marketplace data in the database and emit an event to clear the marketplace form.
     */
    public function store(): void
    {
        $this->validate();

        DB::transaction(function () {
            $this->marketplace = MarketPlace::create($this->marketPlaceData());

            generateCommissionFee($this->marketplace, 'marketplace');
        }, 5);

        $this->emit('clearMarketPlaceForm');
    }

    /**
     * Edit a marketplace item.
     *
     * This function retrieves the marketplace item with the given ID and sets the
     * corresponding properties.
     * It also emits events to show the marketplace form
     * and set the category type.
     *
     * @param  int  $id  The ID of the marketplace item to edit.
     */
    public function editMarketPlace(int $id): void
    {
        // Emit events to show the marketplace form and set the category type.
        $this->emit('showMarketPlaceForm');
        $this->emit('setCategoryType', 'treatment');

        // Retrieve the marketplace item with the given ID.
        $this->marketplace = MarketPlace::find($id);

        // Set the category and subcategory IDs.
        $this->categoryId = $this->marketplace->category_id;
        $this->subCategoryId = $this->marketplace->sub_category_id;

        // Emit events to set the category and subcategory IDs.
        $this->emit('setCategoryId', $this->marketplace->category_id);
        $this->emit('setSubCategoryId', $this->marketplace->sub_category_id);

        // Set the code, name, and price of the marketplace item.
        $this->code = $this->marketplace->code;
        $this->name = $this->marketplace->name;
        $this->price = countFormat($this->marketplace->price);

        // Set the action form to 'update'.
        $this->actionForm = 'update';
    }

    /**
     * Update the marketplace data.
     *
     * This function updates the marketplace data in a transactional manner.
     * It then clears the marketplace form and emits an event to refresh the marketplace.
     */
    public function update(): void
    {
        // Start a database transaction
        DB::transaction(function () {
            // Update the marketplace data
            $this->marketplace->update($this->marketPlaceData());
        }, 5);

        // Clear the marketplace form
        $this->emit('clearMarketPlaceForm');

        // Emit an event to refresh the marketplace
        $this->emit('refreshMarketPlace');
    }

    /**
     * Deletes a marketplace item and its related fees and material usages.
     *
     * @param  int  $id  The ID of the marketplace item to be deleted.
     */
    public function destroy(int $id): void
    {
        // Find the marketplace item by ID
        $this->marketplace = MarketPlace::find($id);

        // Use a database transaction to ensure data consistency
        DB::transaction(function () {
            $this->marketplace->code = $this->marketplace->code.'-deleted';
            $this->marketplace->save();

            // Delete the marketplace item
            $this->marketplace->delete();

            // Delete the related fees
            $this->marketplace->normalFees()->delete();
            $this->marketplace->targetFees()->delete();
            $this->marketplace->discountFees()->delete();

            // Delete the related material usages
            $this->marketplace->material_usages()->delete();
        }, 5);

        // Emit an event to clear the marketplace form
        $this->emit('clearMarketPlaceForm');
    }

    /**
     * Clears the marketplace form.
     */
    public function clearMarketPlaceForm(): void
    {
        // Reset the component state
        $this->reset();
        $this->resetErrorBag();

        // Emit events to clear category, subcategory, and marketplace fee options
        $this->emit('clearCategoryOption');
        $this->emit('clearSubCategoryOption');

        // Emit events to hide the marketplace form and refresh the marketplace
        $this->emit('hideMarketPlaceForm');
        $this->emit('refreshMarketPlace');
    }

    /**
     * Render the view for the marketplace form.
     *
     * @return View|Application|Factory The rendered view.
     */
    public function render(): View|Application|Factory
    {
        // Return the rendered view for the marketplace form.
        return view('livewire.market-place-form');
    }
}
