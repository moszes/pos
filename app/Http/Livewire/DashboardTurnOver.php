<?php

namespace App\Http\Livewire;

use App\Models\SalesInvoice;
use App\Models\TurnOver;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class DashboardTurnOver extends Component
{
    public $selectedMonth;

    public $turnOverAmount;

    public $monthLabel;

    public $monthlyTargetPercentage;

    protected $listeners = [
        'setSelectedMonth',
    ];

    protected $queryString = [
        'selectedMonth',
    ];

    /**
     * Mount the component.
     *
     * This method is called when the component is being mounted to the DOM.
     * It sets the initial values for the `turnOverAmount` and `monthlyTargetPercentage` properties.
     */
    public function mount(): void
    {
        $this->monthLabel = date('F Y', strtotime($this->selectedMonth));
        $this->monthlyTargetPercentage = $this->getMonthlyPercentage();
        $this->turnOverAmount = $this->getTurnOverAmount();
    }

    /**
     * Set the selected month.
     *
     * @param  string  $value  The value of the selected month.
     */
    public function setSelectedMonth(string $value): void
    {
        $this->selectedMonth = $value;
        $this->turnOverAmount = $this->getTurnOverAmount();
        $this->monthLabel = date('F Y', strtotime($this->selectedMonth));
        $this->monthlyTargetPercentage = $this->getMonthlyPercentage();
    }

    /**
     * Get the turnover amount for the selected month.
     *
     * @return float The turnover amount.
     */
    public function getTurnOverAmount(): float
    {
        $turnoverAmount = TurnOver::whereYear('month', date('Y', strtotime($this->selectedMonth)))
            ->whereMonth('month', date('m', strtotime($this->selectedMonth)))
            ->first();

        return (float) ! $turnoverAmount ? 500000000 : $turnoverAmount->amount;
    }

    /**
     * Get the total monthly sales.
     */
    public function getMonthlySales(): float
    {
        return (float) SalesInvoice::where('sales_invoice_status', 'valid')
            ->whereYear('sales_invoice_date', date('Y', strtotime($this->selectedMonth)))
            ->whereMonth('sales_invoice_date', date('m', strtotime($this->selectedMonth)))
            ->sum('grand_total');
    }

    /**
     * Get the monthly target percentage.
     *
     * @return float The monthly target percentage.
     */
    public function getMonthlyPercentage(): float
    {
        return floor($this->getMonthlySales() / $this->getTurnOverAmount() * 100);
    }

    public function render(): Factory|View|Application
    {
        return view('livewire.dashboard-turn-over', [
            'turnOverAmount' => $this->turnOverAmount,
            'monthlyTargetPercentage' => $this->monthlyTargetPercentage,
            'monthLabel' => date('F Y', strtotime($this->selectedMonth)),
        ]);
    }
}
