<?php

namespace App\Http\Livewire;

use App\Models\TurnOver;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class TurnOverForm extends Component
{
    public $formTitle = 'Form TurnOver';

    public $actionForm = 'store';

    public $turnOver;

    public $month;

    public $amount;

    protected $listeners = [
        'clearTurnOverForm',
        'editTurnOver',
        'destroy',
    ];

    protected $rules = [
        'month' => 'required|unique:categories',
        'amount' => 'required',
    ];

    /**
     * Retrieves the turnOver data.
     *
     * @return array The turnOver data.
     */
    public function getTurnOverData(): array
    {
        // Get the first company ID
        $companyId = auth()->user()->details->branch_id;

        // Get the first branch ID
        $branchId = auth()->user()->details->branch_id;

        // Build and return the turnOver data array
        return [
            'company_id' => $companyId,
            'branch_id' => $branchId,
            'month' => $this->month,
            'amount' => $this->amount,
        ];
    }

    /**
     * Store a new turnOver.
     */
    public function store(): void
    {
        // Validate the turnOver data
        $this->validate();

        // Use a transaction to ensure data integrity
        DB::transaction(function () {
            // Create a new turnOver
            TurnOver::create($this->getTurnOverData());

        }, 5);

        // Emit an event to clear the turnOver form inputs
        $this->emit('clearTurnOverForm');
    }

    /**
     * Edits a turnOver by retrieving its details and preparing the form for update.
     *
     * @param  int  $id  The ID of the turnOver to edit.
     */
    public function editTurnOver(int $id): void
    {
        // Retrieve the turnOver by ID
        $this->turnOver = TurnOver::find($id);

        // Set the turnOver details to form fields
        $this->month = $this->turnOver->month;
        $this->amount = $this->turnOver->amount;

        // Set the form action and emit an event to show the turnOver form
        $this->actionForm = 'update';
        $this->emit('showTurnOverForm');
    }

    /**
     * Update the turnOver and perform the necessary actions.
     */
    public function update(): void
    {
        DB::transaction(function () {
            // Update the turnOver
            $this->turnOver->update($this->getTurnOverData());

            // Emit an event to clear the turnOver form inputs
            $this->emit('clearTurnOverForm');
        }, 5);
    }

    /**
     * Delete a turnOver by its ID.
     *
     * @param  int  $id  The ID of the turnOver to delete.
     */
    public function destroy(int $id): void
    {
        $this->turnOver = TurnOver::find($id);

        DB::transaction(function () {
            $this->turnOver->month = $this->turnOver->month.'-deleted';
            $this->turnOver->save();
            // Find the turnOver by its ID and delete it
            $this->turnOver->delete();

        }, 5);

        $this->emit('clearTurnOverForm');
    }

    /**
     * Clear the turnOver form by resetting the form data and error bag.
     */
    public function clearTurnOverForm(): void
    {
        $this->reset();
        $this->resetErrorBag();

        // Emit an event to hide the turnOver form
        $this->emit('hideTurnOverForm');
        $this->emit('refreshTurnOver');
    }

    /**
     * Render the turnOver form view.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.turn-over-form');
    }
}
