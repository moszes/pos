<?php

namespace App\Http\Livewire;

use App\Models\Stock;
use Livewire\Component;
use Livewire\WithPagination;

class StockReportTable extends Component
{
    use WithPagination;

    public $titlePage = 'Stock Products';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshStock' => '$refresh',
    ];

    public function paginationView()
    {
        return 'pagination';
    }

    public function render()
    {
        return view('livewire.stock-report-table', [
            'stocks' => Stock::where('module', 'like', '%'.$this->search.'%')
                ->orderBy('id', 'desc')
                ->paginate(10),
        ]);
    }
}
