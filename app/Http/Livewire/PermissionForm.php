<?php

namespace App\Http\Livewire;

use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Spatie\Permission\Models\Permission;

class PermissionForm extends Component
{
    public $formTitle = 'Form Permission';

    public $actionForm = 'store';

    public $permission;

    public $name;

    protected $listeners = [
        'clearPermissionForm',
        'editPermission',
        'destroy',
    ];

    protected $rules = [
        'name' => 'required|unique:permissions',
    ];

    /**
     * Retrieve the permission data.
     *
     * @return array The permission data.
     */
    public function permissionData(): array
    {
        return [
            'name' => $this->name,
        ];
    }

    public function store(): void
    {
        $this->validate();

        DB::transaction(function () {
            Permission::create($this->permissionData());
        }, 5);

        $this->emit('clearPermissionForm');
    }

    /**
     * Edit permission by id.
     *
     * @param  int  $id  The id of the permission to edit.
     */
    public function editPermission(int $id): void
    {
        // Find the permission by id
        $this->permission = Permission::find($id);

        // Set the name property to the name of the permission
        $this->name = $this->permission->name;

        // Set the actionForm property to 'update'
        $this->actionForm = 'update';

        // Emit the 'showPermissionForm' event
        $this->emit('showPermissionForm');
    }

    /**
     * Update the permission.
     */
    public function update(): void
    {
        // Start a database transaction
        DB::transaction(function () {
            // Update the permission using the permission data
            $this->permission->update($this->permissionData());
        }, 5);

        // Emit an event to clear the permission form
        $this->emit('clearPermissionForm');
    }

    /**
     * Delete a permission by its ID
     *
     * @param  int  $id  The ID of the permission to delete
     */
    public function destroy(int $id): void
    {
        // Find the permission by its ID
        $this->permission = Permission::find($id);

        // Delete the permission within a database transaction
        DB::transaction(function () {
            $this->permission->delete();
        }, 5);

        // Emit an event to clear the permission form
        $this->emit('clearPermissionForm');
    }

    /**
     * Clears the permission form.
     */
    public function clearPermissionForm(): void
    {
        // Reset the component state
        $this->reset();

        // Reset any error messages
        $this->resetErrorBag();

        // Emit an event to refresh the permission data
        $this->emit('refreshPermission');

        // Emit an event to hide the permission form
        $this->emit('hidePermissionForm');
    }

    /**
     * Render the permission form view.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.permission-form');
    }
}
