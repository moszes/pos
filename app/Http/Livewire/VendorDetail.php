<?php

namespace App\Http\Livewire;

use App\Models\Vendor;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;

class VendorDetail extends Component
{
    public $formTitle = 'Vendor Details';

    public $vendor;

    protected $listeners = [
        'showVendorDetails',
        'clearVendorDetail',
    ];

    /**
     * Show vendor details by ID.
     *
     * @param  int  $id  The ID of the vendor
     */
    public function showVendorDetails(int $id): void
    {
        $this->vendor = Vendor::find($id);
    }

    /**
     * Clear the vendor details by resetting the model and error bag.
     */
    public function clearVendorDetail(): void
    {
        $this->reset();
        $this->resetErrorBag();
    }

    /**
     * A description of the entire PHP function.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.vendor-detail');
    }
}
