<?php

namespace App\Http\Livewire;

use App\Models\UserTransaction;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class UserTransactionReportTable extends Component
{
    use WithPagination;

    public $titlePage = 'User Transaction';

    public $customer;

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshUserTransaction' => '$refresh',
    ];

    /**
     * Mount the customer to the class property.
     *
     * @param  mixed  $customer  The customer to be mounted.
     */
    public function mount(mixed $customer): void
    {
        // Assign the $customer parameter to the class property $this->customer
        $this->customer = $customer;
    }

    /**
     * Get the pagination view.
     *
     * This function returns the name of the pagination view to be used.
     *
     * @return string The name of the pagination view.
     */
    public function paginationView(): string
    {
        // The pagination view is named 'pagination'.
        return 'pagination';
    }

    /**
     * Render the user transaction report table view.
     */
    public function render(): View|Factory|Application
    {
        // Check if a customer is set
        if ($this->customer) {
            // Fetch user transactions for the customer
            $user_transactions = UserTransaction::join('users', 'users.id', '=', 'user_transactions.user_id')
                ->where('users.id', 'like', '%'.$this->customer->id.'%')
                ->whereNull('user_transactions.used_date')
                ->orderBy('user_transactions.id', 'desc')
                ->paginate(10);
        } else {
            // Fetch all user transactions
            $user_transactions = UserTransaction::orderBy('id', 'desc')
                ->paginate(10);
        }

        // Return the view with user transactions data
        return view('livewire.user-transaction-report-table', [
            'user_transactions' => $user_transactions,
        ]);
    }
}
