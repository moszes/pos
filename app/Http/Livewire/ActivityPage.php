<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use Spatie\Activitylog\Models\Activity;

class ActivityPage extends Component
{
    use WithPagination;

    public $search;

    protected $queryString = ['search'];

    public function paginationView()
    {
        return 'pagination';
    }

    public function render()
    {
        return view('livewire.activity-page', [
            'activities' => Activity::orderBy('id', 'desc')->paginate(10),
        ]);
    }
}
