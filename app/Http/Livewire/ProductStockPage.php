<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Livewire\Component;

class ProductStockPage extends Component
{
    public $product;

    public function mount($id)
    {
        $this->product = Product::find($id);
    }

    public function render()
    {
        return view('livewire.product-stock-page', [
            'product' => $this->product,
        ]);
    }
}
