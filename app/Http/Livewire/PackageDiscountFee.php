<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PackageDiscountFee extends Component
{
    public $packageDiscountFee = [];

    public $package;

    /**
     * Mount the component and initialize fee values.
     *
     * If the package exists, update the fee values based on the package's discount fees.
     */
    public function mount(): void
    {
        // Initialize the fee values
        $this->packageDiscountFee = [
            'senior_doctor' => 0, // Fee for the doctor
            'doctor' => 0, // Fee for the doctor
            'doctor_assistance' => 0, // Fee for the doctor's assistance
            'sales' => 0, // Fee for sales
            'therapist' => 0, // Fee for the therapist
        ];

        // If the package exists, update the fee values
        if ($this->package) {
            foreach ($this->package->discountFees as $value) {
                // Update the fee value based on the role
                $this->packageDiscountFee[$value->role] = countFormat($value->value);
            }
        }
    }

    /**
     * Update the fee values based on the property name and value.
     *
     * @param  string  $propertyName  The name of the property that was updated.
     * @param  mixed  $value  The new value for the property.
     */
    public function updated(string $propertyName, mixed $value): void
    {
        // Check if the property name is one of the fee properties
        if (in_array($propertyName, [
            'packageDiscountFee.senior_doctor',
            'packageDiscountFee.doctor',
            'packageDiscountFee.doctor_assistance',
            'packageDiscountFee.sales',
            'packageDiscountFee.therapist',
        ])) {
            // Remove the 'packageDiscountFee.' prefix from the property name
            $packageDiscountFeeName = str_replace('packageDiscountFee.', '', $propertyName);

            // Convert the value to a float and update the packageDiscountFee property
            $this->packageDiscountFee[$packageDiscountFeeName] = floatval($value);

            $this->emit('feeUpdated', 'package', 'discount', $this->packageDiscountFee);
        }
    }

    /**
     * Render the view for the PackageDiscountFee Livewire component.
     *
     * This method returns the view for the PackageDiscountFee Livewire component, which is 'livewire.package-discount-fee'.
     *
     * @return Factory|View|Application The rendered view.
     */
    public function render(): Factory|View|Application
    {
        // Return the livewire.package-discount-fee view
        return view('livewire.package-discount-fee');
    }
}
