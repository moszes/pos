<?php

namespace App\Http\Livewire;

use App\Models\User;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Query\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;
use Livewire\WithPagination;

class EmployeeTable extends Component
{
    use WithPagination;

    public $titlePage = 'Employee';

    public $search;

    protected $queryString = ['search'];

    protected $listeners = [
        'refreshEmployee' => '$refresh',
    ];

    /**
     * Reset the page after the search is updated.
     */
    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    /**
     * The attributes that are mass assignable.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Performs a search filter on the users.
     */
    public function searchFilter(): LengthAwarePaginator
    {
        $employeeData = User::role([
            'admin',
            'cashier',
        ])->pluck('id')
            ->toArray();

        return DB::table('users')
            ->join('user_details', 'user_details.user_id', '=', 'users.id')
            ->whereIn('users.id', $employeeData)
            ->where('user_details.company_id', auth()->user()->details->company_id)
            ->where(function (Builder $query) {
                $query->where('users.name', 'like', '%'.$this->search.'%')
                    ->orWhere('user_details.phone', 'like', '%'.$this->search.'%')
                    ->orWhere('user_details.first_name', 'like', '%'.$this->search.'%');
            })
            ->orderBy('users.id', 'desc')
            ->paginate(10);
    }

    /**
     * The attributes that are mass assignable.
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.employee-table', [
            'employees' => self::searchFilter(),
        ]);
    }
}
