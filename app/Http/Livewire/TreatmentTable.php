<?php

namespace App\Http\Livewire;

use App\Models\SubscriptionPlan;
use App\Models\Treatment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;
use Livewire\WithPagination;

class TreatmentTable extends Component
{
    use WithPagination;

    public $search;

    public $titlePage = 'Treatment';

    public $moduleName = 'treatment';

    public $moduleLabel = false;

    public $priceLabel = false;

    public $subscriptionPlanOption = 'all';

    public $subscriptionPlans;

    public $subscriptionPlan;

    protected $queryString = [
        'search',
        'subscriptionPlanOption',
    ];

    protected $listeners = [
        'refreshTreatment' => '$refresh',
        'treatmentFee',
        'treatmentMaterialUsage',
        'setSubscriptionPlan',
    ];

    /**
     * Reset the page when the "search" property is updated.
     */
    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    /**
     * Set the subscription plan option.
     */
    public function setSubscriptionPlan(mixed $code): void
    {
        $this->subscriptionPlanOption = $code;
    }

    /**
     * Redirects to the treatment fees page for a specific ID.
     */
    public function treatmentFee(int $id): void
    {
        // Generate the route for the treatment fees page with the given ID
        $route = route('master.treatment_fees', ['id' => $id]);

        // Redirect to the generated route
        $this->redirect($route);
    }

    /**
     * Redirects to the route for viewing treatment material usages.
     */
    public function treatmentMaterialUsage(int $id): void
    {
        $this->redirect(route('master.treatment_material_usages', ['id' => $id]));
    }

    /**
     * Returns the view name for the pagination.
     */
    public function paginationView(): string
    {
        return 'pagination';
    }

    /**
     * Filter treatments based on search criteria.
     */
    public function searchFilter(): array|LengthAwarePaginator
    {
        // Retrieve treatments that match the search criteria and order them by updated_at in descending order
        $treatments = Treatment::where(function ($query) {
            $query->where('name', 'like', '%'.$this->search.'%')
                ->orWhere('code', 'like', '%'.$this->search.'%');
        })
            ->orderBy('updated_at', 'desc');

        if ($this->subscriptionPlanOption != 'all') {
            $this->subscriptionPlan = SubscriptionPlan::where('code', $this->subscriptionPlanOption)->first();
            $treatments = $treatments->where('subscription_plan', $this->subscriptionPlan->code);
        }

        return $treatments->paginate(10);
    }

    /**
     * Render the view for the service table.
     */
    public function render(): View|Factory|Application
    {
        // Render the view with the treatments data
        return view('livewire.treatment-table', [
            'treatments' => self::searchFilter(),
            'subscriptionPlanOption' => 'all',
        ]);
    }
}
