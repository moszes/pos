<?php

namespace App\Http\Livewire;

use App\Models\UserDetail;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Livewire\Component;

class DashboardCustomerBirthday extends Component
{
    /**
     * Render the component.
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.dashboard-customer-birthday', [
            'customersBirthOfDay' => UserDetail::whereDate('date_of_birth', Carbon::today())->get(),
        ]);
    }
}
