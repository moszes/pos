<?php

namespace App\Http\Livewire;

use App\Models\Appointment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class AppointmentDetail extends Component
{
    public $formTitle = 'Appointment Details';

    public $appointment;

    public $grandTotal;

    protected $listeners = [
        'showAppointmentItems',
        'clearAppointmentItems',
    ];

    /**
     * This function retrieves the appointment and its grand total by ID.
     *
     * @param  int  $id  The ID of the appointment.
     */
    public function showAppointmentItems(int $id): void
    {
        // Find the appointment by ID
        $this->appointment = Appointment::find($id);

        // Retrieve the grand total of the appointment
        $this->grandTotal = $this->appointment->grand_total;
    }

    /**
     * Redirects the user to the transaction sales invoices page with the appointment ID.
     */
    public function appointmentPayment($appointmentId): void
    {
        // Redirect the user to the transaction sales invoices page with the appointment ID
        redirect()->route('transaction.sales_invoices', ['appointmentId' => $appointmentId]);
    }

    /**
     * Clears the appointment data by setting the appointment property to null.
     */
    public function clearAppointmentItems(): void
    {
        // Set the appointment property to null to clear the appointment data
        $this->appointment = null;
    }

    public function render(): Factory|View|Application
    {
        return view('livewire.appointment-detail');
    }
}
