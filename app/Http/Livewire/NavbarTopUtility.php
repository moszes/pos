<?php

namespace App\Http\Livewire;

use App\Http\Controllers\UserController;
use App\Models\Branch;
use App\Models\Company;
use Illuminate\View\View;
use Livewire\Component;

class NavbarTopUtility extends Component
{
    public $user;

    public $userDetail;

    public $avatar;

    public $companyCode;

    public $company;

    public $branchCode;

    public $branch;

    public $branches = [];

    protected $listeners = [
        'refresh' => '$refresh',
        'refreshAvatarImage',
    ];

    protected $queryString = [
        'companyCode',
        'branchCode',
    ];

    /**
     * Sets the avatar image of the user.
     */
    public function mount(): void
    {
        $this->user = session('user');
        $this->userDetail = $this->user->details;

        $this->avatar = UserController::avatarImage($this->user);

        $this->branchCode = session('branch_code');
        $this->branchCode ??= $this->user->details->branch_code;
        $this->branch = Branch::where('code', $this->branchCode)->first();

        $this->companyCode = session('company_code');
        $this->companyCode ??= $this->user->details->company_code;
        $this->company = Company::where('code', $this->companyCode)->first();

        if ($this->user->hasRole('godadmin')) {
            $this->branches = Branch::where(
                'company_id', $this->user->details->company_id
            )->get();
        }
    }

    /**
     * Selects a branch for the user.
     *
     * @param  int  $branchId  ID of the branch to select.
     */
    public function selectBranch(int $branchId): void
    {
        $this->branch = Branch::where('id', $branchId)->first();
        $this->userDetail->branch_id = $branchId;
        $this->userDetail->branch_code = $this->branch->code;
        $this->userDetail->save();

        $this->branchCode = $this->branch->code;

        session([
            'user', auth()->user(),
            'company_code', $this->companyCode,
            'branch_code', $this->branchCode,
        ]);

        $this->emit('updateBranch', $this->branchCode);
        $this->emit('refresh');
    }

    /**
     * Refreshes the avatar image for the user.
     */
    public function refreshAvatarImage(): void
    {
        $this->avatar = $this->userDetail->avatar_file_name;
    }

    public function render(): View
    {
        return view('livewire.navbar-top-utility');
    }
}
