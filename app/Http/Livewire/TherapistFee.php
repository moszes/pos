<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class TherapistFee extends Component
{
    public $therapistRp = 0;

    public $therapistPercent = 0;

    public $treatmentFeeType = 'fixed';

    public $treatmentFeeValue = 0;

    protected $listeners = [
        'clearTherapistFee',
    ];

    /**
     * Update the treatment fee for the doctor.
     *
     * @param  mixed  $value  The new value of the treatment fee.
     */
    public function updatedTherapistRp(mixed $value): void
    {
        // Update the type of treatment fee for the doctor to be fixed.
        $this->treatmentFeeType = 'fixed';

        // Update the value of the treatment fee for the doctor.
        $this->treatmentFeeValue = floatval($value);

        $this->emit('setTherapistFee', $this->treatmentFeeType, $this->treatmentFeeValue);
    }

    /**
     * Updates the percentage value for the therapist's treatment fee.
     *
     * @param  mixed  $value  The new percentage value.
     */
    public function updatedTherapistPercent(mixed $value): void
    {
        // Update the type of treatment fee for the therapist to be fixed.
        $this->treatmentFeeType = 'percentage';

        // Update the value of the treatment fee for the therapist.
        $this->treatmentFeeValue = floatval($value);

        // Emit an event to set the therapist fee with the updated type and value.
        $this->emit('setTherapistFee', $this->treatmentFeeType, $this->treatmentFeeValue);
    }

    /**
     * Clears the therapist fee by resetting the object and error bag.
     */
    public function clearTherapistFee(): void
    {
        $this->reset();
        $this->resetErrorBag();
    }

    /**
     * Render the therapist fee view.
     */
    public function render(): View|Factory|Application
    {
        // Return the therapist fee view
        return view('livewire.therapist-fee');
    }
}
