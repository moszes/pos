<?php

namespace App\Console\Commands;

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Console\Command;

class FixEmptyUserDetail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:fix-empty-user-detail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {

        // Retrieve all users
        $users = User::all();

        // Initialize Faker
        $faker = app(Faker::class);

        foreach ($users as $user) {

            // Use firstOrCreate() which will attempt to locate a database record using the given column / value pairs.
            // If the model cannot be found in the database, a record will be inserted with the attributes resulting from merging the first array argument with the optional second array argument
            $user->details()->firstOrCreate(
                [
                    'user_id' => $user->id,
                ], // columns / values to search records
                [ // Default values for create
                    'company_id' => 1,
                    'branch_id' => 1,
                    'user_id' => $user->id,
                    'code' => generateUserCode(),
                    'phone' => $faker->phoneNumber,
                    // Add more fields as per UserDetails table column
                ]
            );

            $this->info('User details have been updated for the user with ID: '.$user->id);
        }

    }
}
