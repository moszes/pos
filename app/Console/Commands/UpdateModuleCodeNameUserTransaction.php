<?php

namespace App\Console\Commands;

use App\Models\UserTransaction;
use Illuminate\Console\Command;

class UpdateModuleCodeNameUserTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-module-code-name-user-transaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     * This function retrieves user transactions, updates module codes and names,
     * and saves the changes while providing informative logs.
     */
    public function handle(): void
    {
        // Retrieve all user transactions
        $userTransactions = UserTransaction::all();

        foreach ($userTransactions as $userTransaction) {
            // Check if module and module_id are set
            if (! $userTransaction->module || ! $userTransaction->module_id) {
                continue; // Skip if module or module_id is missing
            }

            // Update module code and name based on module and module_id
            $userTransaction->module_code = getDataByModule($userTransaction->module, $userTransaction->module_id)?->code;
            $userTransaction->module_name = getDataByModule($userTransaction->module, $userTransaction->module_id)?->name;

            // Check if reference_module is set and not 'seeder'
            if ($userTransaction->reference_module != null && $userTransaction->reference_module != 'seeder') {
                // Update reference code and name based on reference_module and reference_id
                $userTransaction->reference_code = getDataByModule($userTransaction->reference_module, $userTransaction->reference_id)?->code;
                $userTransaction->reference_name = getDataByModule($userTransaction->reference_module, $userTransaction->reference_id)?->name;

                // Log reference code and name
                $this->info('reference code '.$userTransaction->reference_code.' reference name '.$userTransaction->reference_name);
            }

            // Save the updated user transaction
            $userTransaction->save();

            // Log the update action
            $this->info('Update module code and name for user transaction '.$userTransaction->id);
        }
    }
}
