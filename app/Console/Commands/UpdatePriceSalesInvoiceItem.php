<?php

namespace App\Console\Commands;

use App\Models\SalesInvoiceItem;
use Illuminate\Console\Command;

class UpdatePriceSalesInvoiceItem extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-price-sales-invoice-item';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update price sales invoice item base on master data';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $salesInvoiceItems = SalesInvoiceItem::all();

        foreach ($salesInvoiceItems as $salesInvoiceItem) {
            $salesInvoice = getSalesInvoiceById($salesInvoiceItem->sales_invoice_id);

            if (! $salesInvoice) {
                continue;
            }

            if ($salesInvoiceItem->module == 'treatment') {

                $this->info('Sales Invoice number = '.$salesInvoice->sales_invoice_number);
                $this->info('Sales Invoice ID = '.$salesInvoiceItem->id);

                $salesInvoiceItem->price = $salesInvoiceItem->treatment->price;
                $salesInvoiceItem->total_price = $salesInvoiceItem->treatment->price * $salesInvoiceItem->qty;
                $salesInvoiceItem->sell_price = $salesInvoiceItem->treatment->price;
                $salesInvoiceItem->total_sell_price = $salesInvoiceItem->sell_price * $salesInvoiceItem->qty;
                $salesInvoiceItem->save();

                $this->info('update price sales invoice item base on master data SalesInvoice = '.$salesInvoice->sales_invoice_number);
            }
        }
    }
}
