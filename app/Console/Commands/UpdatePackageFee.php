<?php

namespace App\Console\Commands;

use App\Models\Package;
use Illuminate\Console\Command;

class UpdatePackageFee extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-package-fee';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'fill the package fee';

    /**
     * Execute the console command.
     *
     * This function retrieves all packages from the database and checks if each package has a fee for each role.
     * If a package does not have a fee for a role, a new fee is created for that role with a value of 0 and a type based on the role.
     * A message is output to the console indicating that a fee has been added for the role and package.
     */
    public function handle(): void
    {
        // Get all packages
        $packages = Package::all(); // Retrieve all packages from the database

        // Define the roles for which fees need to be checked and created
        $roles = ['doctor', 'doctor_assistance', 'sales', 'therapist'];

        // Iterate over each package
        foreach ($packages as $package) {
            // Iterate over each role
            foreach ($roles as $role) {
                // Check if the package has a fee for the current role
                if (! $package->fees->where('role', $role)->first()) {
                    // If not, create a new fee
                    $package->fees()->create([
                        'role' => $role, // Set the role of the fee
                        'value' => 0, // Set the value of the fee to 0
                        'type' => $role == 'doctor' ? 'percentage' : 'fixed', // Set the type of the fee based on the role
                    ]);

                    // Output a message to the console indicating that a fee has been added for the role and package
                    $this->info('Added fee for role '.$role.' for package '.$package->name);
                }
            }
        }
    }
}
