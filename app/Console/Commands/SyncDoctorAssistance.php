<?php

namespace App\Console\Commands;

use App\Models\Appointment;
use App\Models\SalesInvoice;
use Illuminate\Console\Command;

class SyncDoctorAssistance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sync-doctor-assistance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'sync doctor assistance';

    /**
     * Execute the console command.
     *
     * This command synchronizes the doctor assistance between appointments and sales invoices.
     * It iterates over all appointments, checks if they have a sales invoice number, and if so,
     * it retrieves the corresponding sales invoice.
     * If the sales invoice exists, it checks if the
     * appointment has any items with a doctor assistant ID. If so, it updates the corresponding
     * sales invoice items with the doctor assistant ID.
     */
    public function handle(): void
    {
        // Retrieve all appointments
        $appointments = Appointment::all();

        // Iterate over each appointment
        foreach ($appointments as $appointment) {
            // Check if the appointment has a sales invoice number
            if ($appointment->sales_invoice_number) {
                // Output the appointment ID
                echo 'appointment'.$appointment->id.PHP_EOL;

                // Check if the appointment has any items
                if ($appointment->items) {
                    // Iterate over each appointment item
                    foreach ($appointment->items as $appointmentItem) {
                        // Check if the appointment item has a doctor assistance ID
                        if ($appointmentItem->doctor_assistance_id) {
                            // Retrieve the corresponding sales invoice
                            $salesInvoices = SalesInvoice::where('sales_invoice_number', $appointment->sales_invoice_number)
                                ->whereNull('deleted_at')->first();

                            // Output the appointment sales invoice number
                            echo 'appointment sales invoice number '.$appointment->sales_invoice_number.PHP_EOL;

                            // Output the sales invoice
                            echo 'sales invoice '.$salesInvoices.PHP_EOL;

                            // Check if the sales invoice exists
                            if (! $salesInvoices) {
                                continue;
                            }

                            // Sync appointment items to sales invoice items
                            if ($salesInvoices->items) {
                                // Iterate over each sales invoice item
                                $salesInvoices->items()->each(function ($salesInvoiceItem) use ($appointmentItem) {
                                    // Check if the sales invoice item has the same module and module ID as the appointment item
                                    if ($salesInvoiceItem->module == $appointmentItem->module && $salesInvoiceItem->module_id == $appointmentItem->module_id) {
                                        // Update the sales invoice item with the doctor assistance ID
                                        $salesInvoiceItem->update([
                                            'doctor_assistance_id' => $appointmentItem->doctor_assistance_id,
                                        ]);
                                    }
                                });
                            }
                        }
                    }
                }
            }
        }
    }
}
