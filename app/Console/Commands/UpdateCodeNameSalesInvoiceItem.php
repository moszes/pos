<?php

namespace App\Console\Commands;

use App\Models\SalesInvoiceItem;
use Illuminate\Console\Command;

class UpdateCodeNameSalesInvoiceItem extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-code-name-sales-invoice-item';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $salesInvoiceItems = SalesInvoiceItem::whereNull('code')
            ->orWhereNull('name')
            ->withTrashed()
            ->get();
        foreach ($salesInvoiceItems as $salesInvoiceItem) {
            $salesInvoiceItem->update([
                'code' => getDataByModule($salesInvoiceItem->module, $salesInvoiceItem->module_id)?->code,
                'name' => getDataByModule($salesInvoiceItem->module, $salesInvoiceItem->module_id)?->name,
            ]);

            // print
            $this->info($salesInvoiceItem->code.' - '.$salesInvoiceItem->name);
        }
    }
}
