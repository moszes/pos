<?php

namespace App\Console\Commands;

use App\Models\MarketPlace;
use App\Models\Package;
use App\Models\Product;
use App\Models\Treatment;
use Illuminate\Console\Command;

class UpdateServiceFee extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-service-fee';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        // Seed product fee
        $products = Product::all();

        // Process products
        foreach ($products as $product) {
            generateCommissionFee($product, 'product');
            $this->info('Product '.$product->name.' has been updated');
        }

        // Retrieve and process treatments
        $treatments = Treatment::all();
        foreach ($treatments as $treatment) {
            generateCommissionFee($treatment, 'treatment');
            $this->info('Treatment '.$treatment->name.' has been updated');
        }

        // Fetch and update packages
        $packages = Package::all();
        foreach ($packages as $package) {
            generateCommissionFee($package, 'package');
            $this->info('Package '.$package->name.' has been updated');
        }

        // Handle marketplace items
        $marketplaces = MarketPlace::all();
        foreach ($marketplaces as $marketplace) {
            generateCommissionFee($marketplace, 'marketplace');
            $this->info('Marketplace '.$marketplace->name.' has been updated');
        }
    }
}
