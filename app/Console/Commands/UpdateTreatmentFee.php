<?php

namespace App\Console\Commands;

use App\Models\Treatment;
use Illuminate\Console\Command;

class UpdateTreatmentFee extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-treatment-fee';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'fill treatment fee';

    /**
     * Execute the console command.
     *
     * This function retrieves all treatments from the database and checks if each treatment has a fee for each role.
     * If a treatment does not have a fee for a role, a new fee is created for that role with a value of 0 and a type based on the role.
     * A message is output to the console indicating that a fee has been added for the role and treatment.
     */
    public function handle(): void
    {
        // Get all treatments
        $treatments = Treatment::all(); // Retrieve all treatments from the database

        // Define the roles for which fees need to be checked and created
        $roles = ['doctor', 'doctor_assistance', 'sales', 'therapist'];

        // Iterate over each treatment
        foreach ($treatments as $treatment) {
            // Iterate over each role
            foreach ($roles as $role) {
                // Check if the treatment has a fee for the current role
                if (! $treatment->fees->where('role', $role)->first()) {
                    // If not, create a new fee
                    $treatment->fees()->create([
                        'role' => $role, // Set the role of the fee
                        'value' => 0, // Set the value of the fee to 0
                        'type' => $role == 'doctor' ? 'percentage' : 'fixed', // Set the type of the fee based on the role
                    ]);

                    // Output a message to the console indicating that a fee has been added for the role and treatment
                    $this->info('Added fee for role '.$role.' for treatment '.$treatment->name);
                }
            }
        }
    }
}
