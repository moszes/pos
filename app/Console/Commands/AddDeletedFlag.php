<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AddDeletedFlag extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:add-deleted-flag {model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $model = $this->argument('model');
        $model = "App\Models\\$model";

        $models = $model::withTrashed()->get();

        foreach ($models as $data) {
            if ($data->deleted_at) {
                $data->code = $data->code.'-deleted';
                $data->save();

                $this->info($data->code.' - '.$data->name.' deleted');
            }
        }
    }
}
