<?php

namespace App\Console\Commands;

use App\Models\AppointmentItem;
use Illuminate\Console\Command;

class UpdateCodeNameAppointmentItem extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-code-name-appointment-item';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * This function updates the code and name of all appointment items by fetching the data from the module.
     * It then saves the changes and logs the update for each item.
     */
    public function handle(): void
    {
        // Get all appointment items
        $appointmentItems = AppointmentItem::all();

        // Loop through each appointment item
        foreach ($appointmentItems as $appointmentItem) {

            // Fetch the code and name from the module
            $appointmentItem->code = getDataByModule($appointmentItem->module, $appointmentItem->module_id)?->code;
            $appointmentItem->name = getDataByModule($appointmentItem->module, $appointmentItem->module_id)?->name;

            // Save the changes
            $appointmentItem->save();

            // Log the update for the item
            $this->info('Update code and name for appointment item '.$appointmentItem->id);
        }
    }
}
