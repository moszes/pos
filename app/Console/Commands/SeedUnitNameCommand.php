<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\Treatment;
use App\Models\UnitOfMeasure;
use Illuminate\Console\Command;

class SeedUnitNameCommand extends Command
{
    protected $signature = 'seed:unit-name';

    protected $description = 'Seed Unit name on treatment material usage';

    public function handle(): void
    {
        $treatments = Treatment::all();

        foreach ($treatments as $treatment) {
            foreach ($treatment->material_usages as $material_usage) {
                $this->info('material usage: '.$material_usage);
                $product = Product::find($material_usage->product_id);
                $this->info('product: '.$product);

                if ($product->unit_of_measure_id) {
                    $unit = UnitOfMeasure::find($product->unit_of_measure_id);

                    if ($unit) {
                        $material_usage->unit_name = $unit->name;
                        $material_usage->save();

                        $this->info('seed: '.$material_usage);
                    }

                }

            }
        }
    }
}
