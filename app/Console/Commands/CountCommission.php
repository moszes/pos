<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CountCommission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:count-commission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Count Commission base on sales invoice items';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        countCommission();
    }
}
