<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class SyncEmailUserCommand extends Command
{
    protected $signature = 'sync:email-user';

    protected $description = 'Command description';

    public function handle(): void
    {
        $users = User::all();

        foreach ($users as $user) {
            $user->details->email = $user->email;
            $user->save();

            $this->info('User Updated '.$user->email);
        }
    }
}
