<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Opcodes\LogViewer\Facades\LogViewer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Schema::defaultStringLength(191);

        LogViewer::auth(function ($request) {
            if (env('APP_ENV') === 'production') {
                return $request->user()
                    && $request->user()->email == 'godadmin@ministrybeauty.com';
            }

            return true;
        });
    }
}
