<?php

namespace App\Listeners;

use App\Events\UserEvent;
use App\Models\LoginAttempt;

class SendUserEventHandling
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(UserEvent $event): void
    {
        LoginAttempt::create([
            'company_id' => auth()->user()->details?->company_id,
            'user_id' => $event->user->id,
            'ip_address' => request()->ip(),
        ]);
    }
}
