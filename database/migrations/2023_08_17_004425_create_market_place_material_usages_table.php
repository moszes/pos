<?php

use App\Models\MarketPlace;
use App\Models\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('market_place_material_usages', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(MarketPlace::class)
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignIdFor(Product::class)
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->decimal('normal_price', 16)->default(0);
            $table->decimal('agent_price', 16)->default(0);
            $table->decimal('qty');
            $table->decimal('size', 16)->default(0);
            $table->decimal('usage', 16)->default(0);
            $table->decimal('cost', 16)->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('market_place_material_usages');
    }
};
