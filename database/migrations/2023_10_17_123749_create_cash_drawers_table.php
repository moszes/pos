<?php

use App\Models\Branch;
use App\Models\Company;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cash_drawers', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Company::class)
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignIdFor(Branch::class)
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignIdFor(User::class)
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('code')->nullable();
            $table->longText('sales_invoice')->nullable();
            $table->decimal('cash', 16)->default(0);
            $table->decimal('credit_debit', 16)->default(0);
            $table->decimal('custom', 16)->default(0);
            $table->decimal('prepaid', 16)->default(0);
            $table->decimal('qrcode', 16)->default(0);
            $table->decimal('voucher', 16)->default(0);
            $table->decimal('grand_total', 16)->default(0);
            $table->datetime('date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cash_drawers');
    }
};
