<?php

use App\Models\Branch;
use App\Models\Company;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Company::class)
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignIdFor(Branch::class)
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('sales_invoice_number')->nullable();
            $table->unsignedBigInteger('customer_id');
            $table->string('appointment_number')->unique();
            $table->datetime('appointment_date');
            $table->decimal('tax', 16)->default(0);
            $table->decimal('others', 16)->default(0);
            $table->decimal('discount', 16)->default(0);
            $table->decimal('grand_total', 16)->default(0);
            $table->string('reference_module')->nullable();
            $table->string('reference_id')->nullable();
            $table->longText('note')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('appointments');
    }
};
