<?php

use App\Models\ServiceFee;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('service_fees', function (Blueprint $table) {
            $table->id();
            $table->string('module')->nullable();
            $table->unsignedInteger('module_id')->nullable();
            $table->enum('role', ServiceFee::ROLES)->default('therapist');
            $table->decimal('value', 16)->default(0);
            $table->enum('fee_type', ServiceFee::FEE_TYPE)->default('normal');
            $table->enum('fee_symbol', ServiceFee::FEE_SYMBOL)->default('fixed');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('service_fees');
    }
};
