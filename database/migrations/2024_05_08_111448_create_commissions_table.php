<?php

use App\Models\SalesInvoiceItem;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('commissions', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(SalesInvoiceItem::class)
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignIdFor(User::class)
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->datetime('sales_invoice_date');
            $table->string('type');
            $table->string('code');
            $table->string('name');
            $table->string('role');
            $table->decimal('normal_price', 16)->default(0);
            $table->string('sell_price', 16)->default(0);
            $table->decimal('fee', 16)->default(0);
            $table->decimal('fee_release', 16)->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('commissions');
    }
};
