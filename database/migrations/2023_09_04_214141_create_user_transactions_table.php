<?php

use App\Models\Branch;
use App\Models\Company;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Company::class)
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignIdFor(Branch::class)
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignIdFor(User::class)
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->unsignedBigInteger('sales_invoice_id')->nullable();
            $table->unsignedBigInteger('appointment_id')->nullable();
            $table->unsignedBigInteger('user_transaction_id')->nullable();
            $table->enum('default_promo', ['B5', 'B10', 'B1'])->default('B1');
            $table->enum('transaction_status', ['buy', 'redemption'])->nullable();
            $table->string('module');
            $table->unsignedBigInteger('module_id')->nullable();
            $table->string('module_code');
            $table->string('module_name');
            $table->string('reference_module')->nullable();
            $table->unsignedBigInteger('reference_id')->nullable();
            $table->string('reference_code')->nullable();
            $table->string('reference_name')->nullable();
            $table->unsignedBigInteger('doctor_id')->nullable();
            $table->unsignedBigInteger('doctor_assistance_id')->nullable();
            $table->unsignedBigInteger('therapist_id')->nullable();
            $table->unsignedBigInteger('sales_id')->nullable();
            $table->decimal('doctor_fee', 16)->default(0);
            $table->decimal('doctor_assistance_fee', 16)->default(0);
            $table->decimal('therapist_fee', 16)->default(0);
            $table->decimal('sales_fee', 16)->default(0);
            $table->decimal('qty', 16)->default(0);
            $table->decimal('discount_item', 16)->default(0);
            $table->decimal('item_price', 16)->default(0);
            $table->decimal('total_price', 16)->default(0);
            $table->decimal('sell_price', 16)->default(0);
            $table->decimal('total_sell_price', 16)->default(0);
            $table->decimal('redemption_price', 16)->default(0);
            $table->decimal('total_redemption_price', 16)->default(0);
            $table->longText('note')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_transactions');
    }
};
