<?php

use App\Models\Appointment;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('appointment_items', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Appointment::class)
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('module')->nullable();
            $table->string('module_id')->nullable();
            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->unsignedBigInteger('user_transaction_id')->nullable();
            $table->unsignedBigInteger('doctor_id')->nullable();
            $table->unsignedBigInteger('doctor_assistance_id')->nullable();
            $table->unsignedBigInteger('therapist_id')->nullable();
            $table->unsignedBigInteger('sales_id')->nullable();
            $table->unsignedBigInteger('room_id')->nullable();
            $table->datetime('room_start')->nullable();
            $table->datetime('room_end')->nullable();
            $table->decimal('qty', 16)->default(0);
            $table->decimal('discount_item', 16)->default(0);
            $table->decimal('redemption_price', 16)->default(0);
            $table->decimal('total_redemption_price', 16)->default(0);
            $table->decimal('price', 16)->default(0);
            $table->decimal('total_price', 16)->default(0);
            $table->enum('transaction_status', ['buy', 'redemption'])->default('buy');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('appointment_items');
    }
};
