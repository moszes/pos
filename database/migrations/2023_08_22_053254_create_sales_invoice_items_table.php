<?php

use App\Models\SalesInvoice;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sales_invoice_items', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(SalesInvoice::class)
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('module')->nullable();
            $table->string('module_id')->nullable();
            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->unsignedBigInteger('doctor_id')->nullable();
            $table->unsignedBigInteger('doctor_assistance_id')->nullable();
            $table->unsignedBigInteger('therapist_id')->nullable();
            $table->unsignedBigInteger('sales_id')->nullable();
            $table->unsignedBigInteger('room_id')->nullable();
            $table->decimal('doctor_fee', 16)->default(0);
            $table->decimal('doctor_assistance_fee', 16)->default(0);
            $table->decimal('therapist_fee', 16)->default(0);
            $table->decimal('sales_fee', 16)->default(0);
            $table->datetime('room_start')->nullable();
            $table->datetime('room_end')->nullable();
            $table->decimal('qty', 16)->default(0);
            $table->decimal('discount_item', 16)->default(0);
            $table->decimal('price', 16)->default(0);
            $table->decimal('total_price', 16)->default(0);
            $table->decimal('sell_price', 16)->default(0);
            $table->decimal('total_sell_price', 16)->default(0);
            $table->enum('transaction_status', ['buy', 'redemption'])->default('buy');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sales_invoice_items');
    }
};
