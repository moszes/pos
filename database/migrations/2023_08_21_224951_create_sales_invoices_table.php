<?php

use App\Models\Appointment;
use App\Models\Branch;
use App\Models\Company;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sales_invoices', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Company::class)
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignIdFor(Branch::class)
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignIdFor(Appointment::class)
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('customer_id');
            $table->string('sales_invoice_number')->unique();
            $table->string('created_by');
            $table->datetime('sales_invoice_date');
            $table->string('release_by')->nullable();
            $table->datetime('release_date')->nullable();
            $table->enum('sales_invoice_status', ['valid', 'invalid'])->default('invalid');
            $table->datetime('is_discount')->nullable();
            $table->decimal('discount', 16)->default(0);
            $table->decimal('tax', 16)->default(0);
            $table->decimal('others', 16)->default(0);
            $table->decimal('grand_total', 16)->default(0);
            $table->decimal('total_payment', 16)->default(0);
            $table->longText('note')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sales_invoices');
    }
};
