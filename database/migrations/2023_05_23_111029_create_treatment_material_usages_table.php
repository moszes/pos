<?php

use App\Models\Product;
use App\Models\Treatment;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('treatment_material_usages', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Treatment::class)
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignIdFor(Product::class)
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->decimal('normal_price', 16)->default(0);
            $table->decimal('agent_price', 16)->default(0);
            $table->decimal('qty', 16)->default(0);
            $table->decimal('usage', 16)->default(0);
            $table->decimal('size', 16)->default(0);
            $table->string('unit_name')->nullable();
            $table->decimal('cost', 16)->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('treatment_material_usages');
    }
};
