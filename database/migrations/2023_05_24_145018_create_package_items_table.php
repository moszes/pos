<?php

use App\Models\Package;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('package_items', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Package::class)
                ->nullable()
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('module');
            $table->unsignedBigInteger('module_id')->nullable();
            $table->string('code');
            $table->string('name')->nullable();
            $table->decimal('qty', 16)->default(0);
            $table->decimal('package_item_price', 16)->default(0);
            $table->decimal('normal_item_price', 16)->default(0);
            $table->decimal('total_package_price', 16)->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('package_items');
    }
};
