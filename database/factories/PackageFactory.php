<?php

namespace Database\Factories;

use App\Models\Branch;
use App\Models\Company;
use App\Models\Package;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Package>
 */
class PackageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'company_id' => Company::first()->id,
            'branch_id' => Branch::first()->id,
            'code' => $this->faker->unique()->word(),
            'name' => $this->faker->sentence(),
            'price' => $this->faker->numberBetween(1000, 100000),
        ];
    }
}
