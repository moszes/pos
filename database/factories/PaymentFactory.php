<?php

namespace Database\Factories;

use App\Models\Company;
use App\Models\Payment;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Payment>
 */
class PaymentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'company_id' => Company::first()->id,
            'code' => 'PAY'.str_pad(Payment::withTrashed()->count() + 1, 10, '0', STR_PAD_LEFT),
            'name' => fake()->name(),
        ];
    }
}
