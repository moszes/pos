<?php

namespace Database\Factories;

use App\Models\Branch;
use App\Models\Category;
use App\Models\Company;
use App\Models\SubCategory;
use App\Models\Treatment;
use App\Models\TreatmentMaterialUsage;
use Illuminate\Database\Eloquent\Factories\Factory;

class TreatmentFactory extends Factory
{
    protected $model = Treatment::class;

    /**
     * Configures the factory to create a Treatment and its related TreatmentMaterialUsage and generate a commission fee.
     */
    public function configure(): self
    {
        return $this->afterCreating(function (Treatment $treatment) {
            TreatmentMaterialUsage::factory()->create([
                'treatment_id' => $treatment->id,
            ]);

            generateCommissionFee($treatment, 'treatment');
        });
    }

    /**
     * Generates an array of data for creating a new record in the database.
     *
     * @return array The generated data array.
     */
    public function definition(): array
    {
        return [
            'company_id' => Company::first()->id,
            'branch_id' => Branch::first()->id,
            'code' => $this->faker->unique()->word(),
            'name' => $this->faker->name(),
            'price' => $this->faker->randomFloat(),

            'category_id' => Category::first()->id,
            'sub_category_id' => SubCategory::first()->id,
        ];
    }
}
