<?php

namespace Database\Factories;

use App\Models\VendorTax;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class VendorTaxFactory extends Factory
{
    protected $model = VendorTax::class;

    public function definition(): array
    {
        return [
            'vendor_id' => getFirstVendor()->id,
            'npwp' => $this->faker->numerify('##########'),
            'npwp_name' => 'PT DUNIA KECANTIKAN INDONESIA',
            'tax_address' => $this->faker->address(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
