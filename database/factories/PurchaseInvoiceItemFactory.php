<?php

namespace Database\Factories;

use App\Models\PurchaseInvoiceItem;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class PurchaseInvoiceItemFactory extends Factory
{
    protected $model = PurchaseInvoiceItem::class;

    public function definition(): array
    {
        return [
            'purchase_invoice_id' => getFirstPurchaseInvoice()->id,
            'product_id' => getFirstProduct()->id,
            'qty' => $this->faker->randomFloat(),
            'price' => $this->faker->randomFloat(),
            'discount_item' => $this->faker->randomFloat(),
            'total_price' => $this->faker->randomFloat(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
