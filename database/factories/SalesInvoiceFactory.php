<?php

namespace Database\Factories;

use App\Models\Appointment;
use App\Models\Branch;
use App\Models\Company;
use App\Models\SalesInvoice;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<SalesInvoice>
 */
class SalesInvoiceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        // base on SalesInvoice Model creates factory
        return [
            'company_id' => function () {
                return Company::factory()->create()->id;
            },
            'branch_id' => function () {
                return Branch::factory()->create()->id;
            },
            'sales_invoice_number' => generateSalesInvoiceNumber(),
            'sales_invoice_date' => $this->faker->date(),
            'appointment_id' => function () {
                return Appointment::factory()->create()->id;
            },
            'customer_id' => function () {
                return User::factory()->create()->assignRole('customer')->id;
            },
            'created_by' => function () {
                return User::factory()->create()->id;
            },
            'created_at' => $this->faker->dateTimeBetween('-1 year', 'now'),
            'updated_at' => $this->faker->dateTimeBetween('-1 year', 'now'),
        ];
    }
}
