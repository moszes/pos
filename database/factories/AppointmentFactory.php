<?php

namespace Database\Factories;

use App\Models\Appointment;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class AppointmentFactory extends Factory
{
    protected $model = Appointment::class;

    public function definition(): array
    {
        return [
            'company_id' => auth()->user()->details->branch_id,
            'branch_id' => auth()->user()->details->branch_id,
            'customer_id' => getFirstCustomer()->id,
            'appointment_number' => generateAppointmentNumber(),
            'appointment_date' => Carbon::now(),
            'tax' => $this->faker->randomFloat(),
            'others' => $this->faker->randomFloat(),
            'discount' => $this->faker->randomFloat(),
            'grand_total' => $this->faker->randomFloat(),
            'reference_module' => $this->faker->word(),
            'reference_id' => $this->faker->word(),
            'note' => $this->faker->word(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
