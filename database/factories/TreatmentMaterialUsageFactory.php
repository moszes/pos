<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Treatment;
use App\Models\TreatmentMaterialUsage;
use App\Models\UnitOfMeasure;
use Illuminate\Database\Eloquent\Factories\Factory;

class TreatmentMaterialUsageFactory extends Factory
{
    protected $model = TreatmentMaterialUsage::class;

    public function definition(): array
    {

        return [
            'treatment_id' => Treatment::first()->id,
            'product_id' => Product::first()->id,
            'normal_price' => $this->faker->numberBetween(1, 100),
            'agent_price' => $this->faker->numberBetween(1, 100),
            'qty' => $this->faker->numberBetween(1, 100),
            'size' => $this->faker->numberBetween(1, 100),
            'unit_name' => UnitOfMeasure::first()->name,
            'usage' => $this->faker->numberBetween(1, 100),
            'cost' => $this->faker->numberBetween(1, 100),
        ];
    }
}
