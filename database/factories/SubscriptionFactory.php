<?php

namespace Database\Factories;

use App\Models\Subscription;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class SubscriptionFactory extends Factory
{
    protected $model = Subscription::class;

    public function definition(): array
    {
        return [
            'company_id' => $this->faker->randomNumber(),
            'branch_id' => $this->faker->randomNumber(),
            'subscription_plan_id' => $this->faker->randomNumber(),
            'customer_id' => $this->faker->word(),
            'code' => $this->faker->word(),
            'buy_date' => Carbon::now(),
            'active_date' => Carbon::now(),
            'expired_date' => Carbon::now(),
            'status' => $this->faker->word(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
