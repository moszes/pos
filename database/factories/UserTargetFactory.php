<?php

namespace Database\Factories;

use App\Models\Branch;
use App\Models\Company;
use App\Models\User;
use App\Models\UserTarget;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserTargetFactory extends Factory
{
    protected $model = UserTarget::class;

    public function definition(): array
    {
        return [
            'company_id' => Company::first()->id,
            'branch_id' => Branch::first()->id,
            'user_id' => User::first()->id,
            'code' => $this->faker->unique()->numerify('########'),
            'name' => $this->faker->word,
            'module' => $this->faker->randomElement(['products', 'treatment', 'package', 'marketplace']),
            'module_id' => 1,
            'amount' => $this->faker->randomFloat(2, 0, 1000),
        ];
    }
}
