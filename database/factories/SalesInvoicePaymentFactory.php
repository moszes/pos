<?php

namespace Database\Factories;

use App\Models\SalesInvoice;
use App\Models\SalesInvoicePayment;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<SalesInvoicePayment>
 */
class SalesInvoicePaymentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'sales_invoice_id' => SalesInvoice::factory(),
            'payment_id' => firstPayment()->id,
            'amount' => 1,
            'reference_number' => fake()->text(10),
            'status' => 'paid',
        ];
    }
}
