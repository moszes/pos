<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\MarketPlaceMaterialUsage>
 */
class MarketPlaceItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $price = rand(100000, 1000000);
        $dose = rand(1, 10);

        return [

            'price' => $price,
            'dose' => $dose,
            'total_cost' => $price * $dose,
        ];
    }
}
