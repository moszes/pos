<?php

namespace Database\Factories;

use App\Models\Approval;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Approval>
 */
class ApprovalFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'company_id' => auth()->user()->details->branch_id,
            'branch_id' => auth()->user()->details->branch_id,
            'request_by' => User::factory()->create()->assignRole('admin')->id,
            'request_date' => fake()->date(),
            'approve_by' => User::factory()->create()->assignRole('manager')->id,
            'approve_date' => fake()->date(),
            'module' => 'sales_invoices',
            'module_id' => getFirstSalesInvoice()->id,
            'status' => 'pending',
            'note' => fake()->sentence(),
        ];
    }
}
