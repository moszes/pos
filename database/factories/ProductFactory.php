<?php

namespace Database\Factories;

use App\Models\Branch;
use App\Models\Category;
use App\Models\Company;
use App\Models\Product;
use App\Models\SubCategory;
use App\Models\UnitOfMeasure;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class ProductFactory extends Factory
{
    protected $model = Product::class;

    /**
     * Configure the ProductFactory.
     */
    public function configure(): ProductFactory
    {
        return $this->afterCreating(function (Product $product) {
            generateCommissionFee($product, 'product');
        });
    }

    /**
     * Returns an array containing the definition of a data object.
     */
    public function definition(): array
    {
        return [
            // unit of measure us from UnitOfMeasure Model
            'unit_of_measure_id' => UnitOfMeasure::first()->id,
            'code' => $this->faker->unique()->word(),
            'name' => $this->faker->name(),
            'size' => $this->faker->randomFloat(),
            'cost' => $this->faker->randomFloat(),
            'agent_price' => $this->faker->randomFloat(),
            'normal_price' => $this->faker->randomFloat(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'company_id' => Company::first()->id,
            'branch_id' => Branch::first()->id,
            'category_id' => Category::first()->id,
            'sub_category_id' => SubCategory::first()->id,
        ];
    }
}
