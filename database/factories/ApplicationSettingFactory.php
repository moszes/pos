<?php

namespace Database\Factories;

use App\Models\ApplicationSetting;
use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class ApplicationSettingFactory extends Factory
{
    protected $model = ApplicationSetting::class;

    public function definition(): array
    {
        return [
            'company_id' => Company::first()->id,
            'name' => $this->faker->name(),
            'module' => $this->faker->word(),
            'status' => 'Y',
            'value' => $this->faker->word(),
            'redirect_url' => $this->faker->url(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
