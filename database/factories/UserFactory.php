<?php

namespace Database\Factories;

use App\Models\Branch;
use App\Models\Company;
use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory<User>
 */
class UserFactory extends Factory
{
    /**
     * Configure the user after creation.
     */
    public function configure(): Factory|UserFactory
    {
        return $this->afterCreating(function (User $user) {
            UserDetail::create([
                'user_id' => $user->id,
                'company_id' => Company::first()->id,
                'branch_id' => Branch::first()->id,
                'code' => generateUserCode(),
                'first_name' => $this->faker->firstName,
                'last_name' => $this->faker->lastName,
                'email' => $this->faker->safeEmail,
                'phone' => $this->faker->phoneNumber,
                'address' => $this->faker->address,
                'cover_image_file_name' => null,
                'avatar_file_name' => null,
                'instagram' => $this->faker->userName,
                'date_of_birth' => $this->faker->date,
                'place_of_birth' => $this->faker->city,
            ]);
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'email' => fake()->safeEmail(),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     */
    public function unverified(): static
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
        ]);
    }
}
