<?php

namespace Database\Factories;

use App\Models\Appointment;
use App\Models\Branch;
use App\Models\Company;
use App\Models\Product;
use App\Models\SalesInvoice;
use App\Models\User;
use App\Models\UserTransaction;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class UserTransactionFactory extends Factory
{
    protected $model = UserTransaction::class;

    /**
     * Generates an array with predefined values for creating a new record.
     *
     * @return array The generated array.
     */
    public function definition(): array
    {
        // Define an array of options.
        $options = ['service', 'package', 'marketplace', 'product'];

        // Pick a random key from $options.
        $randomKey = array_rand($options);

        // Get the value at the random key.
        $randomValue = $options[$randomKey];

        // Generate a random price.
        $price = $this->faker->randomFloat();

        // Generate and return the array.
        return [
            'company_id' => Company::factory()->create()->id,
            'branch_id' => Branch::factory()->create()->id,
            'user_id' => User::factory()->create()->id,
            'appointment_id' => Appointment::factory()->create()->id,
            'sales_invoice_id' => SalesInvoice::factory()->create()->id,
            'module' => 'product',
            'module_id' => Product::factory()->create()->id,
            'module_code' => Product::factory()->create()->code,
            'module_name' => Product::factory()->create()->name,
            'reference_module' => $randomValue,
            'reference_id' => 1,
            'default_promo' => getRandomDefaultPromo(),
            'qty' => $price,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
