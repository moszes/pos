<?php

namespace Database\Factories;

use App\Models\Company;
use App\Models\SubscriptionPlan;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubscriptionPlanFactory extends Factory
{
    protected $model = SubscriptionPlan::class;

    public function definition(): array
    {
        return [
            'company_id' => Company::first()->id,
            'code' => $this->faker->word(),
            'name' => $this->faker->name(),
            'price' => $this->faker->word(),
        ];
    }
}
