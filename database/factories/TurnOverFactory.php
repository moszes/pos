<?php

namespace Database\Factories;

use App\Models\TurnOver;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class TurnOverFactory extends Factory
{
    protected $model = TurnOver::class;

    public function definition(): array
    {
        return [
            'company_id' => $this->faker->randomNumber(),
            'branch_id' => $this->faker->randomNumber(),
            'month' => Carbon::now(),
            'amount' => $this->faker->randomFloat(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(), ];
    }
}
