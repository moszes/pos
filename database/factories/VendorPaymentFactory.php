<?php

namespace Database\Factories;

use App\Models\VendorPayment;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class VendorPaymentFactory extends Factory
{
    protected $model = VendorPayment::class;

    public function definition(): array
    {
        return [
            'vendor_id' => getFirstVendor()->id,
            'period' => Carbon::now(),
            'bank' => 'BCA',
            'payment_name' => 'Asep',
            'payment_number' => '1234567890',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
