<?php

namespace Database\Factories;

use App\Models\Branch;
use App\Models\Category;
use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class CategoryFactory extends Factory
{
    protected $model = Category::class;

    public function definition(): array
    {
        $company = Company::factory()->create();

        $branch = Branch::factory()->create(['company_id' => $company->id]);

        return [
            'branch_id' => $branch->id,
            'code' => $this->faker->unique()->word(),
            'type' => getCategoryType()[rand(0, 4)],
            'name' => $this->faker->name(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'company_id' => $company->id,
        ];
    }
}
