<?php

namespace Database\Factories;

use App\Models\MarketPlaceMaterialUsage;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class MarketPlaceMaterialUsageFactory extends Factory
{
    protected $model = MarketPlaceMaterialUsage::class;

    /**
     * Define the attributes of the model.
     */
    public function definition(): array
    {
        return [
            'market_place_id' => $this->faker->randomNumber(),
            'normal_price' => $this->faker->randomFloat(),
            'agent_price' => $this->faker->randomFloat(),
            'qty' => $this->faker->randomNumber(2),
            'size' => $this->faker->randomFloat(),
            'usage' => $this->faker->randomFloat(),
            'cost' => $this->faker->randomFloat(),

            'product_id' => Product::factory(),
        ];
    }
}
