<?php

namespace Database\Factories;

use App\Models\Branch;
use App\Models\Company;
use App\Models\Room;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class RoomFactory extends Factory
{
    protected $model = Room::class;

    public function definition(): array
    {
        return [
            'company_id' => Company::first()->id,
            'branch_id' => Branch::first()->id,
            'code' => $this->faker->ean8(),
            'name' => 'VIP In Room ',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
