<?php

namespace Database\Factories;

use App\Models\Company;
use App\Models\Vendor;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class VendorFactory extends Factory
{
    protected $model = Vendor::class;

    public function definition(): array
    {
        return [
            'code' => 'V-'.$this->faker->unique()->numberBetween(1000, 9999),
            'name' => 'PT DUNIA KECANTIKAN INDONESIA',
            'address' => $this->faker->address(),
            'phone' => $this->faker->phoneNumber(),
            'contact_person' => 'John Doe',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'company_id' => Company::first()->id,
        ];
    }
}
