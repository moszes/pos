<?php

namespace Database\Factories;

use App\Models\PurchaseOrder;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class PurchaseOrderFactory extends Factory
{
    protected $model = PurchaseOrder::class;

    public function definition(): array
    {
        return [
            'purchase_order_number' => generatePurchaseOrderNumber(),
            'discount' => $this->faker->randomFloat(),
            'tax' => $this->faker->randomFloat(),
            'others' => $this->faker->randomFloat(),
            'date' => Carbon::now(),
            'approved_by' => $this->faker->word(),
            'approved_date' => Carbon::now(),
            'grand_total' => $this->faker->randomFloat(),
            'note' => $this->faker->word(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'branch_id' => auth()->user()->details->branch_id,
            'vendor_id' => getFirstVendor()->id,
            'user_id' => getFirstUser()->id,
        ];
    }
}
