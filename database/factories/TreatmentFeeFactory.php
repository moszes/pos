<?php

namespace Database\Factories;

use App\Models\TreatmentFee;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class TreatmentFeeFactory extends Factory
{
    protected $model = TreatmentFee::class;

    public function definition(): array
    {
        return [
            'treatment_id' => getFirstTreatment()->id,
            'role' => $this->faker->randomElement(['doctor', 'doctor_assistance', 'sales', 'therapist']),
            'value' => $this->faker->randomFloat(),
            'type' => $this->faker->randomElement(['fixed', 'percentage']),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
