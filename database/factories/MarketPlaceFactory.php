<?php

namespace Database\Factories;

use App\Models\Branch;
use App\Models\Company;
use App\Models\MarketPlace;
use App\Models\MarketPlaceMaterialUsage;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class MarketPlaceFactory extends Factory
{
    protected $model = MarketPlace::class;

    /**
     * Configure the application.
     */
    public function configure(): MarketPlaceFactory
    {
        return $this->afterCreating(function (MarketPlace $marketPlace) {
            $marketPlace->material_usages()->save(MarketplaceMaterialUsage::factory()->make());

            generateCommissionFee($marketPlace, 'marketplace');
        });
    }

    /**
     * Returns an array of attributes for a model definition.
     */
    public function definition(): array
    {
        return [
            'company_id' => Company::first()->id,
            'branch_id' => Branch::first()->id,
            'category_id' => 3,
            'sub_category_id' => 3,
            'code' => $this->faker->unique()->word(),
            'name' => $this->faker->name(),
            'price' => $this->faker->randomFloat(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
