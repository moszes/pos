<?php

namespace Database\Factories;

use App\Models\Stock;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class StockFactory extends Factory
{
    protected $model = Stock::class;

    public function definition(): array
    {
        return [
            'company_id' => $this->faker->randomNumber(),
            'branch_id' => $this->faker->randomNumber(),
            'warehouse_id' => $this->faker->randomNumber(),
            'good_receive_id' => $this->faker->randomNumber(),
            'appointment_id' => $this->faker->randomNumber(),
            'sales_invoice_id' => $this->faker->randomNumber(),
            'user_id' => $this->faker->randomNumber(),
            'module' => $this->faker->word(),
            'module_id' => $this->faker->randomNumber(),
            'stock_qty' => $this->faker->word(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
