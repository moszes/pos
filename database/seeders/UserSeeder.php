<?php

namespace Database\Seeders;

use App\Models\Branch;
use App\Models\Company;
use App\Models\User;
use App\Models\UserDetail;
use Arr;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = [
            [
                'name' => 'admin',
                'first_name' => fake()->firstName,
                'last_name' => fake()->lastName,
                'phone' => fake()->phoneNumber,
                'address' => fake()->address,
                'email' => 'admin@test.com',
                'password' => bcrypt('project'),
                'role' => 'godadmin',
            ],
            [
                'name' => 'manager',
                'first_name' => 'manager',
                'last_name' => 'role',
                'phone' => fake()->phoneNumber,
                'address' => fake()->address,
                'email' => 'merry@test.com',
                'password' => bcrypt('ministry'),
                'role' => 'manager',
            ],
            [
                'name' => 'doctor',
                'first_name' => 'doctor',
                'last_name' => 'role',
                'phone' => fake()->phoneNumber,
                'address' => fake()->address,
                'email' => 'tenny@test.com',
                'password' => bcrypt('ministry'),
                'role' => 'doctor',
            ],
            [
                'name' => 'sales',
                'first_name' => 'sales',
                'last_name' => 'ministry',
                'phone' => fake()->phoneNumber,
                'address' => fake()->address,
                'email' => 'sales@test.com',
                'password' => bcrypt('ministry'),
                'role' => 'sales',
            ],
            [
                'name' => 'therapist',
                'first_name' => 'therapist',
                'last_name' => 'ministry',
                'phone' => fake()->phoneNumber,
                'address' => fake()->address,
                'email' => 'therapist@test.com',
                'password' => bcrypt('ministry'),
                'role' => 'therapist',
            ],
            [
                'name' => 'kiki',
                'first_name' => 'kiki',
                'last_name' => 'ministry',
                'phone' => fake()->phoneNumber,
                'address' => fake()->address,
                'email' => 'kiki@customer.com',
                'password' => bcrypt('ministry'),
                'role' => 'customer',
            ],
        ];

        foreach ($users as $user) {
            $dataUser = Arr::only($user, ['name', 'email', 'password']);
            $dataUserDetail = Arr::only($user, ['first_name', 'last_name', 'phone', 'address', 'email']);
            $newUser = User::create($dataUser);

            $dataUserDetail['company_id'] = Company::first()->id;
            $dataUserDetail['code'] = generateUserCode();
            $dataUserDetail['branch_id'] = Branch::first()->id;
            $dataUserDetail['user_id'] = $newUser->id;

            UserDetail::create($dataUserDetail);
            $newUser->assignRole($user['role']);
        }

        auth()->loginUsingId(1);
    }
}
