<?php

namespace Database\Seeders;

use App\Models\SubCategory;
use Illuminate\Database\Seeder;

class SubCategorySeeder extends Seeder
{
    /**
     * Inserts subcategories into the database.
     */
    public function run(): void
    {
        $subCategories = [
            [
                'category_id' => 1,
                'code' => '01',
                'name' => 'treatment sub category',
            ],
            [
                'category_id' => 2,
                'code' => '02',
                'name' => 'product sub category',
            ],
            [
                'category_id' => 3,
                'code' => '03',
                'name' => 'marketplace sub category',
            ],
            [
                'category_id' => 4,
                'code' => '04',
                'name' => 'package sub category',
            ],
            [
                'category_id' => 5,
                'code' => '05',
                'name' => 'subscription sub category',
            ],
        ];

        // Process the subcategories and create records in the database
        foreach ($subCategories as $subCategory) {
            SubCategory::create([
                'category_id' => $subCategory['category_id'],
                'code' => $subCategory['code'],
                'name' => $subCategory['name'],
            ]);
        }
    }
}
