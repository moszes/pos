<?php

namespace Database\Seeders;

use App\Models\SubscriptionPlan;
use App\Models\Treatment;
use Illuminate\Database\Seeder;

class TreatmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Treatment::factory()->create();

        // with subscription plan
        Treatment::factory()->create([
            'subscription_plan' => SubscriptionPlan::first()->code,
        ]);
    }
}
