<?php

namespace Database\Seeders;

use App\Models\Payment;
use App\Models\Product;
use App\Models\SalesInvoice;
use App\Models\SalesInvoicePayment;
use App\Models\UserTransaction;
use Illuminate\Database\Seeder;

class SalesInvoiceSeeder extends Seeder
{
    public $dataSetItem;

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $salesInvoice = SalesInvoice::factory()->create();

        $product = Product::first();

        // create sales Invoice Items
        $salesInvoice->items()->create([
            'sales_invoice_id' => $salesInvoice->id,
            'module' => 'product',
            'module_id' => $product->id,
            'code' => $product->code,
            'name' => $product->name,
            'doctor_id' => getFirstDoctor()->id,
            'doctor_assistance_id' => getFirstTherapist()->id,
            'therapist_id' => getFirstTherapist()->id,
            'sales_id' => getFirstSales()->id,
            'qty' => 1,
            'price' => 100000,
            'discount_item' => 0,
            'total_price' => 100000,
        ]);

        // create sales invoice payments
        SalesInvoicePayment::factory()->create([
            'sales_invoice_id' => $salesInvoice->id,
            'payment_id' => Payment::factory()->create()->id,
            'amount' => 100000,
            'reference_number' => 'INV-001',
            'status' => 'paid',
        ]);

        // create user transactions
        UserTransaction::create([
            'company_id' => $salesInvoice->company_id,
            'branch_id' => $salesInvoice->branch_id,
            'user_id' => getFirstUser()->id,
            'appointment_id' => null,
            'sales_invoice_id' => $salesInvoice->id,
            'default_promo' => getRandomDefaultPromo(),
            'module' => 'product',
            'module_id' => $product->id,
            'module_code' => $product->code,
            'module_name' => $product->name,
            'reference_module' => null,
            'reference_id' => null,
            'qty' => 1,
        ]);
    }
}
