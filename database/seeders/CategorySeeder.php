<?php

namespace Database\Seeders;

use App\Models\Branch;
use App\Models\Category;
use App\Models\Company;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the function to create treatment categories in the database.
     */
    public function run(): void
    {
        // Define the treatment categories
        $categories = [
            [
                'code' => '01',
                'name' => 'product category',
                'type' => 'product',
            ],
            [
                'code' => '02',
                'name' => 'treatment category',
                'type' => 'treatment',
            ],
            [
                'code' => '03',
                'name' => 'marketplace category',
                'type' => 'marketplace',
            ],
            [
                'code' => '04',
                'name' => 'package category',
                'type' => 'package',
            ],
            [
                'code' => '05',
                'name' => 'subscription category',
                'type' => 'subscription',
            ],
        ];

        // Process each category and create records in the database
        foreach ($categories as $category) {
            Category::create([
                'company_id' => Company::first()->id,
                'branch_id' => Branch::first()->id,
                'code' => $category['code'],
                'name' => $category['name'],
                'type' => $category['type'],
            ]);
        }
    }
}
