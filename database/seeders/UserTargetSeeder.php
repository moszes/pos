<?php

namespace Database\Seeders;

use App\Models\UserTarget;
use Illuminate\Database\Seeder;

class UserTargetSeeder extends Seeder
{
    public function run(): void
    {
        UserTarget::factory()->create();
    }
}
