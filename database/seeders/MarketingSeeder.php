<?php

namespace Database\Seeders;

use App\Models\Branch;
use App\Models\Company;
use App\Models\Country;
use App\Models\District;
use App\Models\SubDistrict;
use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Database\Seeder;

class MarketingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $marketing = 'marketing@ministrybeauty.com';

        $dataSet = [
            'name' => 'marketing',
            'password' => bcrypt('ministry'),
            'email' => $marketing,
        ];

        $user = User::create($dataSet);
        $user->assignRole('marketing');

        UserDetail::create([
            'user_id' => $user->id,
            'company_id' => Company::first()->id,
            'branch_id' => Branch::first()->id,
            'country_id' => Country::first()->id,
            'district_id' => District::first()->id,
            'sub_district_id' => SubDistrict::first()->id,
            'code' => generateUserCode(),
            'email' => $marketing,
            'first_name' => 'marketing',
            'last_name' => 'Ministry',
            'phone' => fake()->phoneNumber,
            'address' => fake()->address,
        ]);
    }
}
