<?php

namespace Database\Seeders;

use App\Models\UnitOfMeasure;
use Illuminate\Database\Seeder;

class UnitOfMeasureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $units = [
            [
                'code' => 'unts',
                'name' => 'Units',
            ],
            [
                'code' => 'ml',
                'name' => 'Milli Liter',
            ],
            [
                'code' => 'cm',
                'name' => 'Centi Meter',
            ],
            [
                'code' => 'pcs',
                'name' => 'Pieces',
            ],
        ];

        foreach ($units as $unit) {
            $unit['company_id'] = 1;
            UnitOfMeasure::create($unit);
        }
    }
}
