<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Payment;
use Illuminate\Database\Seeder;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $payments = [
            [
                'company_id' => Company::first()->id,
                'name' => 'Cash',
                'status' => 'Y',
            ],
            [
                'company_id' => Company::first()->id,
                'name' => 'Credit/Debit',
                'status' => 'Y',
            ],
            [
                'company_id' => Company::first()->id,
                'name' => 'Custom',
                'status' => 'Y',
            ],
            [
                'company_id' => Company::first()->id,
                'name' => 'Prepaid',
                'status' => 'Y',
            ],
            [
                'company_id' => Company::first()->id,
                'name' => 'QR code',
                'status' => 'Y',
            ],
            [
                'company_id' => Company::first()->id,
                'name' => 'Membership',
                'status' => 'Y',
            ],
            [
                'company_id' => Company::first()->id,
                'name' => 'Check',
                'status' => 'Y',
            ],
        ];

        foreach ($payments as $key => $payment) {
            $payment['code'] = 'PAY'.str_pad(Payment::withTrashed()->count() + 1, 10, '0', STR_PAD_LEFT);

            Payment::create($payment);
        }
    }
}
