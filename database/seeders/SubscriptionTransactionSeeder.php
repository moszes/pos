<?php

namespace Database\Seeders;

use App\Models\SubscriptionPlan;
use App\Models\Treatment;
use Illuminate\Database\Seeder;

class SubscriptionTransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $subscriptionPlan = SubscriptionPlan::all();
        $treatment = Treatment::first();
        $treatment->subscriptionPlans()->sync($subscriptionPlan);

        $secondTreatment = Treatment::find(2);

        $secondTreatment->subscriptionPlans()->sync($subscriptionPlan);
    }
}
