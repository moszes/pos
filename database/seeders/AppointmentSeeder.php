<?php

namespace Database\Seeders;

use App\Http\Controllers\UserTransactionApiController;
use App\Models\Appointment;
use App\Models\AppointmentItem;
use App\Models\Branch;
use App\Models\Company;
use App\Models\Package;
use App\Models\Room;
use App\Models\SalesInvoice;
use App\Models\SalesInvoiceItem;
use App\Models\User;
use App\Models\UserTransaction;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AppointmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // select customer
        $customer = User::role('customer')->first();

        // select cashier
        $sales = User::role('sales')->first();

        // create sales invoice for customer
        $salesInvoice = SalesInvoice::create([
            'company_id' => Company::first()->id,
            'customer_id' => $customer->id,
            'sales_invoice_number' => generateSalesInvoiceNumber(),
            'created_by' => $sales->id,
            'sales_invoice_date' => Carbon::now(),
            'tax' => 0.0,
            'others' => 0.0,
            'discount' => 0.0,
            'grand_total' => 0.0,
            'note' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // get package
        $package = Package::first();

        // create sales invoice item
        SalesInvoiceItem::create([
            'sales_invoice_id' => $salesInvoice->id,
            'module' => 'package',
            'module_id' => $package->id,
            'qty' => 1,
            'price' => rand(1000, 10000),
            'discount_item' => 0.0,
            'total_price' => 0.0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // release sales invoice
        $salesInvoice->update([
            'release_by' => $sales->id,
            'release_date' => Carbon::now(),
            'sales_invoice_status' => 'valid',
        ]);

        // create user transaction base on sales invoice
        UserTransaction::create([
            'company_id' => Company::first()->id,
            'branch_id' => Branch::first()->id,
            'user_id' => $customer->id,
            'sales_invoice_id' => $salesInvoice->id,
            'appointment_id' => null,
            'default_promo' => getRandomDefaultPromo(),
            'transaction_status' => 'buy',
            'module' => 'package',
            'module_id' => $package->id,
            'module_code' => $package->code,
            'module_name' => $package->name,
            'reference_module' => null,
            'reference_id' => null,
            'reference_code' => null,
            'reference_name' => null,
            'qty' => 1,
            'note' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // generate user transaction base on package item
        foreach ($package->items as $item) {

            // only for treatment
            if ($item->module == 'treatment') {
                UserTransaction::create([
                    'company_id' => Company::first()->id,
                    'branch_id' => Branch::first()->id,
                    'user_id' => $customer->id,
                    'sales_invoice_id' => $salesInvoice->id,
                    'appointment_id' => null,
                    'default_promo' => getRandomDefaultPromo(),
                    'transaction_status' => 'buy',
                    'module' => 'treatment',
                    'module_id' => $item->id,
                    'module_code' => $item->code,
                    'module_name' => $item->name,
                    'reference_module' => 'package',
                    'reference_id' => $package->id,
                    'reference_code' => $package->code,
                    'reference_name' => $package->name,
                    'qty' => 1,
                    'note' => '',
                ]);
            }
        }

        $customerTreatment = new UserTransactionApiController;

        $dataRedemption = $customerTreatment->getUserTransactionsByCustomerId($customer->id);

        // create appointment
        Appointment::create([
            'company_id' => Company::first()->id,
            'branch_id' => Branch::first()->id,
            'customer_id' => $customer->id,
            'appointment_number' => generateAppointmentNumber(),
            'appointment_date' => Carbon::now(),
            'tax' => 0.0,
            'others' => 0.0,
            'discount' => 0.0,
            'grand_total' => 0.0,
            'note' => '',
        ]);

        // create appointment item
        foreach ($dataRedemption as $item) {
            $price = getDataByModule($item->module, $item->module_id)->price;
            AppointmentItem::create([
                'appointment_id' => 1,
                'module' => $item->module,
                'module_id' => $item->module_id,
                'code' => $item->code,
                'name' => $item->name,
                'doctor_id' => User::role('doctor')->first()->id,
                'doctor_assistance_id' => User::role('therapist')->first()->id,
                'therapist_id' => User::role('therapist')->first()->id,
                'sales_id' => User::role('sales')->first()->id,
                'room_id' => Room::first()->id,
                'room_start' => Carbon::now(),
                'room_end' => Carbon::now(),
                'qty' => $item->qty,
                'price' => $price,
                'discount_item' => 0,
                'total_price' => $item->qty * $price,
            ]);
        }
    }
}
