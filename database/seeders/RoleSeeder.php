<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $roles = [
            [
                'name' => 'godadmin',
            ],
            [
                'name' => 'business owner',
            ],
            [
                'name' => 'manager',
            ],
            [
                'name' => 'admin',
            ],
            [
                'name' => 'senior doctor',
            ],
            [
                'name' => 'doctor',
            ],
            [
                'name' => 'therapist',
            ],
            [
                'name' => 'sales',
            ],
            [
                'name' => 'supervisor',
            ],
            [
                'name' => 'customer',
            ],
        ];

        foreach ($roles as $role) {
            Role::firstOrCreate($role);
        }
    }
}
