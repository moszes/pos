<?php

namespace Database\Seeders;

use App\Models\Vendor;
use App\Models\VendorPayment;
use App\Models\VendorTax;
use Illuminate\Database\Seeder;

class VendorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Vendor::factory()->create();
        VendorPayment::factory()->create();
        VendorTax::factory()->create();
    }
}
