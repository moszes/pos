<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $permissions = [
            [
                'name' => 'see master data menu',
            ],
            [
                'name' => 'see region group link',
            ],
            [
                'name' => 'see base data group link',
            ],
            [
                'name' => 'see user group link',
            ],
            [
                'name' => 'see product group link',
            ],
            [
                'name' => 'see service group link',
            ],
            [
                'name' => 'see employees link',
            ],
            [
                'name' => 'see working time group link',
            ],
            [
                'name' => 'see finance group link',
            ],
            [
                'name' => 'see transaction data menu',
            ],
            [
                'name' => 'see purchase order link',
            ],
            [
                'name' => 'see purchase invoice link',
            ],
            [
                'name' => 'see good receive link',
            ],
            [
                'name' => 'see stock link',
            ],
            [
                'name' => 'see cogs link',
            ],
            [
                'name' => 'see sales invoice link',
            ],
            [
                'name' => 'see appointment link',
            ],
            [
                'name' => 'see deposit link',
            ],
            [
                'name' => 'see schedule menu',
            ],
            [
                'name' => 'see schedule product link',
            ],
            [
                'name' => 'see schedule service link',
            ],
            [
                'name' => 'see schedule package link',
            ],
            [
                'name' => 'see schedule promo link',
            ],
            [
                'name' => 'see report menu',
            ],
            [
                'name' => 'see activities link',
            ],
            [
                'name' => 'see sales report link',
            ],
            [
                'name' => 'see purchase report link',
            ],
            [
                'name' => 'see faq menu',
            ],
        ];

        foreach ($permissions as $permission) {
            Permission::create($permission);
        }
    }
}
