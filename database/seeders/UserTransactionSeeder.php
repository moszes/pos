<?php

namespace Database\Seeders;

use App\Models\UserTransaction;
use Illuminate\Database\Seeder;

class UserTransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        UserTransaction::factory()->create();
    }
}
