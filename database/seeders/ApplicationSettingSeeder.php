<?php

namespace Database\Seeders;

use App\Models\ApplicationSetting;
use App\Models\Company;
use Illuminate\Database\Seeder;

class ApplicationSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ApplicationSetting::factory()->create();
        $modules = [
            'treatment',
            'subscription',
            'package',
            'marketplace',
        ];
        foreach ($modules as $module) {
            ApplicationSetting::create([
                'company_id' => Company::first()->id,
                'name' => 'Application Module',
                'status' => 'N',
                'module' => $module,
            ]);
        }
    }
}
