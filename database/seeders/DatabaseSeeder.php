<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            RoleSeeder::class,
            PermissionSeeder::class,
            CompanySeeder::class,
            BranchSeeder::class,
            UserSeeder::class,
            ApplicationSettingSeeder::class,
            UserTargetSeeder::class,
            VendorSeeder::class,
            CategorySeeder::class,
            SubCategorySeeder::class,
            UnitOfMeasureSeeder::class,
            PaymentSeeder::class,
            ProductSeeder::class,
            RoomSeeder::class,
            SubscriptionPlanSeeder::class,
            TreatmentSeeder::class,
            MarketPlaceSeeder::class,
            PackageSeeder::class,
            //            PurchaseOrderSeeder::class,
            //            PurchaseInvoiceSeeder::class,
            //            GiftCardSeeder::class,
            //            ConsumableSeeder::class,
            //            AppointmentSeeder::class,
            //            SalesInvoiceSeeder::class,
            //            UserTransactionSeeder::class,
            //            ApprovalSeeder::class,
            //            TurnOverSeeder::class,
            //            SubscriptionSeeder::class,
            //            SubscriptionTransactionSeeder::class,
            //            CommissionSeeder::class,
        ]);
    }
}
