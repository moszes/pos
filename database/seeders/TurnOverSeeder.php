<?php

namespace Database\Seeders;

use App\Models\TurnOver;
use Illuminate\Database\Seeder;

class TurnOverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TurnOver::create([
            'company_id' => auth()->user()->details->branch_id,
            'branch_id' => auth()->user()->details->branch_id,
            'month' => '2022-03-01',
            'amount' => 500000000,
        ]);
    }
}
