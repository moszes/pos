<?php

namespace Database\Seeders;

use App\Models\MarketPlace;
use Illuminate\Database\Seeder;

class MarketPlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        MarketPlace::factory()->create();
    }
}
