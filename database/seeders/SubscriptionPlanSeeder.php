<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\SubscriptionPlan;
use Illuminate\Database\Seeder;

class SubscriptionPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        SubscriptionPlan::create([
            'company_id' => Company::first()->id,
            'code' => 'all_you_can_treat',
            'name' => 'All You Can Treat',
            'price' => 100000,
        ]);

        SubscriptionPlan::create([
            'company_id' => Company::first()->id,
            'code' => 'beauty_forever',
            'name' => 'Beauty Forever',
            'price' => 100000,
        ]);
    }
}
