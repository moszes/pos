<?php

use App\Http\Livewire\MaterialPage;
use Illuminate\Support\Facades\Route;

Route::get('material', MaterialPage::class)->name('material');
