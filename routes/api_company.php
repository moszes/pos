<?php

use App\Http\Controllers\CompanyController;
use Illuminate\Support\Facades\Route;

Route::middleware([])->group(function () {
    Route::post('companies', [CompanyController::class, 'store'])->name('api.companies.store');
    Route::get('companies', [CompanyController::class, 'index'])->name('api.companies')->middleware('auth:sanctum');
    Route::patch('companies/{id}', [CompanyController::class, 'update'])->name('api.companies.update')->middleware('auth:sanctum');
    Route::delete('companies/{id}', [CompanyController::class, 'destroy'])->name('api.companies.destroy')->middleware('auth:sanctum');

});
