<?php

use App\Http\Livewire\InventoryReportPage;
use Illuminate\Support\Facades\Route;

Route::get('inventoryReport', InventoryReportPage::class)->name('inventoryReport');
