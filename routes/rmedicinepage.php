<?php

use App\Http\Livewire\MedicinePage;
use Illuminate\Support\Facades\Route;

Route::get('medicine', MedicinePage::class)->name('medicine');
