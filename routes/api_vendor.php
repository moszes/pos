<?php

use App\Http\Controllers\ApiVendorController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->group(function () {

    Route::get('vendors', [ApiVendorController::class, 'vendors'])->name('api.vendors');

});
