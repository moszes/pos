<?php

use App\Http\Controllers\GenerateExcel;
use App\Http\Controllers\ReceiptController;
use App\Mail\SendEmailTestMail;
use Illuminate\Support\Facades\Route;

Route::get('logout', function () {
    auth()->logout();

    return redirect('login');
})->name('logout');

Route::get('phpinfo', function () {
    phpinfo();
});

Route::get('print/{siId}', [ReceiptController::class, 'receipt'])->name('print');

Route::get('printCloseCashDrawer', [ReceiptController::class, 'printCloseCashDrawer'])->name('printCloseCashDrawer');

Route::get('generate_excel', [GenerateExcel::class, 'download'])->name('generate_excel');

Route::get('getApplicationVersion', function () {
    return getApplicationVersion();
});

Route::get('/test/bucket/upload', function () {
    Storage::put('text1.txt', 'test');

    return Storage::allFiles();
})->name('test.bucket.upload');

Route::get('/test/bucket/download', function () {
    return Storage::download('text.txt');
})->name('test.bucket.download');

Route::get('sendEmailTest', function () {
    $details = [
        'title' => 'Mail from Ministry',
        'body' => 'This is for testing email using smtp',
    ];

    Mail::to('creative@programinglive.com')->send(new SendEmailTestMail($details));

    return 'Email has been sent!';
})->name('sendEmailTest');
