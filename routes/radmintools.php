<?php

use App\Http\Livewire\AdjustRedemptionPage;
use App\Http\Livewire\AdminFaqPage;
use App\Http\Livewire\ApplicationFaqPage;
use App\Http\Livewire\SalesInvoiceToolPage;
use Illuminate\Support\Facades\Route;

Route::prefix('faq')->group(function () {
    Route::get('application', ApplicationFaqPage::class)->name('faq.application');
    Route::get('module', AdminFaqPage::class)->name('faq.module');
});

Route::prefix('tools')->group(function () {
    Route::get('adjustRedemption', AdjustRedemptionPage::class)->name('tools.adjustRedemption');
    Route::get('salesInvoiceTool', SalesInvoiceToolPage::class)->name('tools.salesInvoiceTool');
});
