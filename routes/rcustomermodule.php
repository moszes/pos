<?php

use App\Http\Livewire\DataRedemptionPage;
use Illuminate\Support\Facades\Route;

Route::name('customer.')->prefix('customer')->group(function () {
    Route::get('data_redemption', DataRedemptionPage::class)->name('data_redemption');
});
