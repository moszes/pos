<?php

use App\Http\Controllers\ApiBaseDataController;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth:sanctum'])->group(function () {

    Route::get('/categories', [ApiBaseDataController::class, 'categories'])
        ->name('api.categories');

    Route::get('/subCategories', [ApiBaseDataController::class, 'subCategories'])
        ->name('api.subCategories');

    Route::get('/unit_of_measures', [ApiBaseDataController::class, 'unit_of_measures'])
        ->name('api.unit_of_measures');

});
