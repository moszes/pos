<?php

use App\Http\Livewire\PackagePage;
use Illuminate\Support\Facades\Route;

Route::get('package', PackagePage::class)->name('package');
