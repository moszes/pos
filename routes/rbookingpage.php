<?php

use App\Http\Livewire\BookingPage;
use Illuminate\Support\Facades\Route;

Route::get('booking', BookingPage::class)->name('booking');
