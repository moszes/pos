<?php

use App\Http\Livewire\SettingBranchPage;
use App\Http\Livewire\SettingCompanyPage;
use App\Http\Livewire\SettingModulePage;
use App\Http\Livewire\SettingPrinterPage;
use Illuminate\Support\Facades\Route;

Route::prefix('settings')->group(function () {
    Route::get('company', SettingCompanyPage::class)->name('settings.company');
    Route::get('branch', SettingBranchPage::class)->name('settings.branch');
    Route::get('printer', SettingPrinterPage::class)->name('settings.printer');
    Route::get('module', SettingModulePage::class)->name('settings.module');
});
