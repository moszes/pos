<?php

use App\Http\Livewire\BreakdownCostPage;
use Illuminate\Support\Facades\Route;

Route::get('breakdownCost', BreakdownCostPage::class)->name('breakdownCost');
