<?php

use App\Http\Livewire\ManageCompanyPage;
use App\Http\Livewire\ManageUserTransactionPage;
use Illuminate\Support\Facades\Route;

Route::get('manage/companyPage', ManageCompanyPage::class)->name('manage.companyPage');
Route::get('manage/userTransaction', ManageUserTransactionPage::class)->name('manage.userTransaction');
