<?php

use App\Http\Controllers\ApiPackageController;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth:sanctum'])->group(function () {

    Route::get('/packages', [ApiPackageController::class, 'packages'])
        ->name('api.packages');

});
