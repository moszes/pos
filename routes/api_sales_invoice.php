<?php

use App\Http\Controllers\ApiSalesInvoiceController;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth:sanctum'])->group(function () {

    Route::get('sales_invoices', [ApiSalesInvoiceController::class, 'sales_invoices'])
        ->name('api.sales_invoices');
    Route::get('getSalesInvoiceByInvoiceNumber/{salesInvoiceNumber}', [ApiSalesInvoiceController::class, 'getSalesInvoiceByInvoiceNumber'])
        ->name('api.getSalesInvoiceByInvoiceNumber');
    Route::get('getSalesInvoiceById/{salesInvoiceId}', [ApiSalesInvoiceController::class, 'getSalesInvoiceById'])
        ->name('api.getSalesInvoiceById');

});

Route::get('getSelectedMonthData/{selectedMonth}', [ApiSalesInvoiceController::class, 'getSelectedMonthData'])
    ->name('api.getSelectedMonthData');
Route::get('getBeforeSelectedMonthData/{selectedMonth}', [ApiSalesInvoiceController::class, 'getBeforeSelectedMonthData'])
    ->name('api.getBeforeSelectedMonthData');
