<?php

use App\Http\Livewire\CustomerPage;
use App\Http\Livewire\EmployeePage;
use App\Http\Livewire\UserProfilePage;
use Illuminate\Support\Facades\Route;

Route::name('master.')->prefix('master')->group(function () {
    Route::get('customers', CustomerPage::class)->name('customers');
    Route::get('employees', EmployeePage::class)->name('employees');
    Route::get('employee_details', UserProfilePage::class)->name('employee_details');
});

Route::get('/users/profile', UserProfilePage::class)->name('user.profile');
