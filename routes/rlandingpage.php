<?php

use App\Http\Livewire\CompanyDataPage;
use App\Http\Livewire\CompanyRegister;
use App\Http\Livewire\ForgetPassword;
use App\Http\Livewire\LandingPage;
use App\Http\Livewire\LoginPage;
use Illuminate\Support\Facades\Route;

Route::get('/', LandingPage::class)->name('landingPage');
Route::get('/login', LoginPage::class)->name('login');
Route::get('/forget_password', ForgetPassword::class)->name('forget_password');

// Register Company
Route::get('company_register', CompanyRegister::class)->name('company_register');
Route::get('company_data', CompanyDataPage::class)->name('company_data');
