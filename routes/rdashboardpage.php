<?php

use App\Http\Livewire\DashboardPage;
use Illuminate\Support\Facades\Route;

Route::get('dashboard', DashboardPage::class)->name('dashboard');
