<?php

use App\Http\Livewire\BranchPage;
use App\Http\Livewire\CompanyPage;
use App\Http\Livewire\TurnOverPage;
use Illuminate\Support\Facades\Route;

Route::name('master.')->prefix('master')->group(function () {
    Route::get('companies', CompanyPage::class)->name('companies');
    Route::get('branches', BranchPage::class)->name('branches');
    Route::get('turnovers', TurnOverPage::class)->name('turnovers');
});
