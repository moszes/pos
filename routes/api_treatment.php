<?php

use App\Http\Controllers\ApiTreatmentController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->group(function () {

    Route::get('/treatments', [ApiTreatmentController::class, 'treatments'])->name('api.treatments');

});
