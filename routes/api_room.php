<?php

use App\Http\Controllers\ApiRoomController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->group(function () {

    Route::get('/rooms', [ApiRoomController::class, 'rooms'])->name('api.rooms');

});
