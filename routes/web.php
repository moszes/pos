<?php

use Illuminate\Support\Facades\Route;

Route::middleware('guest')->group(function () {
    include 'rlandingpage.php';
});

Route::middleware(['auth'])->group(function () {
    include 'rpagemanagement.php';
    include 'rdashboardpage.php';
    include 'rusermodule.php';
    include 'rcustomermodule.php';
    include 'rmasterdata.php';
    include 'rtransaction.php';
    include 'rreport.php';
    include 'rorganisation.php';
    include 'rsettings.php';
    include 'radmintools.php';
});

include 'tool.php';
