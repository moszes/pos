<?php

use App\Http\Controllers\ApiPurchaseInvoiceController;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth:sanctum'])->group(function () {

    Route::get('purchase_invoices', [ApiPurchaseInvoiceController::class, 'purchase_invoices'])
        ->name('api.purchase_invoices');

});
