<?php

use App\Http\Livewire\ActivityPage;
use App\Http\Livewire\CogsReportPage;
use App\Http\Livewire\CommissionReportDetailPage;
use App\Http\Livewire\CommissionReportPage;
use App\Http\Livewire\DataRedemptionReportPage;
use App\Http\Livewire\PurchaseReportPage;
use App\Http\Livewire\SalesDetailReportPage;
use App\Http\Livewire\SalesReportPage;
use App\Http\Livewire\SalesSubscriptionReportPage;
use App\Http\Livewire\SalesSummaryReportPage;
use App\Http\Livewire\StockReportPage;
use App\Http\Livewire\TodaySalesReportPage;
use App\Http\Livewire\TurnOverReportPage;
use Illuminate\Support\Facades\Route;

Route::name('report.')->prefix('report')->group(callback: function () {
    Route::get('today_sales_report', TodaySalesReportPage::class)->name('today_sales_report');
    Route::get('salesReport', SalesReportPage::class)->name('salesReport');
    Route::get('salesSummaryReport', SalesSummaryReportPage::class)->name('salesSummaryReport');
    Route::get('salesDetailReport', SalesDetailReportPage::class)->name('salesDetailReport');
    Route::get('salesSubscriptionReport', SalesSubscriptionReportPage::class)->name('salesSubscriptionReport');
    Route::get('commissionReport', CommissionReportPage::class)->name('commissionReport');
    Route::get('commissionReportDetail/{employee_id}', CommissionReportDetailPage::class)->name('commissionReportDetail');
    Route::get('turnOverReport', TurnOverReportPage::class)->name('turnOverReport');
    Route::get('purchaseReport', PurchaseReportPage::class)->name('purchaseReport');
    Route::get('stockReport', StockReportPage::class)->name('stockReport');
    Route::get('data_redemption', DataRedemptionReportPage::class)->name('data_redemption');
    Route::get('cogsReport', CogsReportPage::class)->name('cogsReport');
    Route::get('activities', ActivityPage::class)->name('activities');
});
