<?php

use App\Http\Controllers\ApiProductController;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth:sanctum'])->group(function () {

    Route::get('/products', [ApiProductController::class, 'products'])
        ->name('api.products');

});
