<?php

use App\Http\Controllers\ApiPurchaseOrderController;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth:sanctum'])->group(function () {

    Route::get('purchase_orders', [ApiPurchaseOrderController::class, 'purchase_orders'])
        ->name('api.purchase_orders');

});
