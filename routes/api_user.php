<?php

use App\Http\Controllers\ApiUserController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->group(function () {

    Route::get('/user', [ApiUserController::class, 'user'])->name('api.users');

});

Route::post('generateToken', [ApiUserController::class, 'generateToken'])->name('api.generateToken');
