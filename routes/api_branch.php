<?php

use App\Http\Controllers\ApiBranchController;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth:sanctum'])->group(function () {

    Route::get('branches', [ApiBranchController::class, 'branches'])->name('api.branches');
    Route::post('branches', [ApiBranchController::class, 'store'])->name('api.branches.store');
    Route::patch('branches/{branch}', [ApiBranchController::class, 'update'])->name('api.branches.update');
    Route::delete('branches/{branch}', [ApiBranchController::class, 'destroy'])->name('api.branches.destroy');

});
