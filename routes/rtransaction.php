<?php

use App\Http\Livewire\AppointmentPage;
use App\Http\Livewire\DepositPage;
use App\Http\Livewire\ProductStockPage;
use App\Http\Livewire\PurchaseInvoicePage;
use App\Http\Livewire\PurchaseOrderPage;
use App\Http\Livewire\SalesInvoicePage;
use App\Http\Livewire\StockPage;
use App\Http\Livewire\SubscriptionPage;
use Illuminate\Support\Facades\Route;

Route::name('transaction.')->prefix('transaction')->group(function () {
    Route::get('purchase_orders', PurchaseOrderPage::class)->name('purchase_orders');
    Route::get('purchase_invoices', PurchaseInvoicePage::class)->name('purchase_invoices');
    Route::get('product_stocks/{id}', ProductStockPage::class)->name('product_stocks');
    Route::get('sales_invoices', SalesInvoicePage::class)->name('sales_invoices');
    Route::get('subscriptions', SubscriptionPage::class)->name('subscriptions');
    Route::get('appointments', AppointmentPage::class)->name('appointments');
    Route::get('stocks', StockPage::class)->name('stocks');
    Route::get('deposits', DepositPage::class)->name('deposits');
});
