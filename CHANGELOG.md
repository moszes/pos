# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [3.4.2](https://gitlab.com/beautyworld_repository/clinic/compare/v3.4.1...v3.4.2) (2024-08-03)


### Features

* **branch:** add search filtering and pagination ([188f123](https://gitlab.com/beautyworld_repository/clinic/commit/188f123aef2cea67cc154a5457202d2d8782bdce))
* **livewire:** add functionality to clear marketplace form and emit events ([e13b900](https://gitlab.com/beautyworld_repository/clinic/commit/e13b90019c7d243755ef4414283b6e610b5d6438))
* **livewire:** remove Date column in sales-invoice-table ([12e4ee6](https://gitlab.com/beautyworld_repository/clinic/commit/12e4ee6c7acfc69ba94f1b9dd0ef16c0fa0de6ca))
* **tests:** add unit tests for TreatmentForm class ([e0bd7e3](https://gitlab.com/beautyworld_repository/clinic/commit/e0bd7e3badfff9334fcfe1af747b398b1e490b3b))
* **treatment:** add logic to mark treatment as deleted ([762375d](https://gitlab.com/beautyworld_repository/clinic/commit/762375dd6ef2db557dd408104431a594d9b793af))
* **turnover:** refactor turn over ([57f14a3](https://gitlab.com/beautyworld_repository/clinic/commit/57f14a3fd3fac9f635cb6c93982e425c4f34ad2c))


### Bug Fixes

* corrected variable name in MarketPlaceForm.php ([b64b290](https://gitlab.com/beautyworld_repository/clinic/commit/b64b29093cdcd20c9e600118a3d7e537260d00db))

### [3.4.1](https://gitlab.com/beautyworld_repository/clinic/compare/v3.3.9...v3.4.1) (2024-07-30)


### Features

* command to add deleted flag on model ([5bdca59](https://gitlab.com/beautyworld_repository/clinic/commit/5bdca598334039dca728b036f0f9651d7223023b))
* **product:** add tests for product form in Livewire component ([2812559](https://gitlab.com/beautyworld_repository/clinic/commit/28125595d4a30ed41036eab843ff48d1140012ac))
* **web:** update treatment form to delete specific fees and material usages ([34eabfa](https://gitlab.com/beautyworld_repository/clinic/commit/34eabfa54d6d532ec0d9ab3ad64642e3e90b3e1e))

### [3.3.9](https://gitlab.com/beautyworld_repository/clinic/compare/v3.3.8...v3.3.9) (2024-07-25)


### Features

* **livewire-components:** add additional properties and refactor detail views ([8cc92ca](https://gitlab.com/beautyworld_repository/clinic/commit/8cc92ca8171b8835a2de972ab5deebdbf09e999a))
* **livewire-components:** add item property to ProductFeePage and ServiceFeeForm ([d1c0599](https://gitlab.com/beautyworld_repository/clinic/commit/d1c0599033b506f66dd99064f1f718c6c05b051b))
* **livewire-components:** add moduleLabel and modify text formatting ([aa2ae32](https://gitlab.com/beautyworld_repository/clinic/commit/aa2ae322428d3191f54540b0249826d3618c9c29))
* **livewire-market-place:** refactor market place data management ([cf39adb](https://gitlab.com/beautyworld_repository/clinic/commit/cf39adb889e496cd6dcb6731b15bfac918900e15))
* **livewire:** create service-link component and refactor routes ([15daeef](https://gitlab.com/beautyworld_repository/clinic/commit/15daeef09552b5ecaf0e526c132cca2c611cd895))
* **market-place:** refactor code and update views ([8608fd7](https://gitlab.com/beautyworld_repository/clinic/commit/8608fd7099320a08d0717393a41afc25af351e7b))
* refactor marketPlace to marketplace ([3228283](https://gitlab.com/beautyworld_repository/clinic/commit/32282836e2e698a9eb9258efcaa01d7762ace0b2))
* **tests-http-livewire:** add TreatmentDetailMaterialUsage tests and update render method ([039d5fa](https://gitlab.com/beautyworld_repository/clinic/commit/039d5fa4b9fa173ba9aaa118a5ab1f883760e3db))
* **tests:** add Customer related tests ([bb1b969](https://gitlab.com/beautyworld_repository/clinic/commit/bb1b969fa48df9575c602988e5685e97b036df2c))
* **treatment-table-test:** add new tests for treatment table functionality ([db01d7c](https://gitlab.com/beautyworld_repository/clinic/commit/db01d7c9023974720aab69d04181b2302b159809))


### Bug Fixes

* fix incorrect model reference in MarketPlaceMaterialUsagePage ([b8476bb](https://gitlab.com/beautyworld_repository/clinic/commit/b8476bbf635d051525c6ccae7590934ac151c24b))
* update CompanyDataPageTest and CompanyRegistrationTest ([c4aba3d](https://gitlab.com/beautyworld_repository/clinic/commit/c4aba3dde742bb727ffae2e99ee09799022aafe5))
* update DashboardPageTest to use existing user ([130b23b](https://gitlab.com/beautyworld_repository/clinic/commit/130b23b42a32cb97c9000ee9c0ee0cf683247355))

### [3.3.8](https://gitlab.com/beautyworld_repository/clinic/compare/v3.3.7...v3.3.8) (2024-07-23)


### Features

* **item-fee-header:** refactor code to make item-fee-header more generic ([480de4e](https://gitlab.com/beautyworld_repository/clinic/commit/480de4ef3bbbf65e0f433acf450f5e734333c3db))
* **service-fee-form:** delete product-fee-detail, create service-fee-form, update serviceFee model ([5204e83](https://gitlab.com/beautyworld_repository/clinic/commit/5204e831e059869cdb412221f62c910b115c2166))

### [3.3.7](https://gitlab.com/beautyworld_repository/clinic/compare/v3.3.6...v3.3.7) (2024-07-22)


### Features

* **console-commands:** add seed unit name command ([5e9d430](https://gitlab.com/beautyworld_repository/clinic/commit/5e9d430351aeeaad1f62fe3b578c3a098dd7f3f5))
* **livewire-component:** implement subscription plan in treatment-form ([2777159](https://gitlab.com/beautyworld_repository/clinic/commit/277715914d14ca564980ad85df52f3a1ffa52611))
* **livewire-product:** refactor components and simplify tests ([05334b4](https://gitlab.com/beautyworld_repository/clinic/commit/05334b4e070259e11dc8baae3cdf06008d09acf4))
* **livewire:** implement item-detail component ([a76396f](https://gitlab.com/beautyworld_repository/clinic/commit/a76396fb163ece38aa62a9752aca1b59aca10794))
* **product-fee-detail:** add check-product-has-fee method ([fb43670](https://gitlab.com/beautyworld_repository/clinic/commit/fb436700440125e038d9bf519b2e31bfae08dfab))
* **product-fee-detail:** update functionalities and layout design ([d6289f3](https://gitlab.com/beautyworld_repository/clinic/commit/d6289f375efbb01d249186cba5d555a0576ed1f9))
* **subscription-plan-treatment:** revise subscription plan and treatment related files ([4cbbff9](https://gitlab.com/beautyworld_repository/clinic/commit/4cbbff9192215fce00b316cf0dd3b212a8df5d3c))
* **subscription-plan:** update seeder and add search filter ([000e8a6](https://gitlab.com/beautyworld_repository/clinic/commit/000e8a605ce39c1c2d05f27a105f8d370c265205))
* **treatment-material-usage:** add unit-name field ([74c5eaa](https://gitlab.com/beautyworld_repository/clinic/commit/74c5eaaf75a42ba8cd2ca93e85fec489d7ef2fc2))
* **unit-of-measure-form:** add check for unit usage before deletion ([cb7ab3f](https://gitlab.com/beautyworld_repository/clinic/commit/cb7ab3ffab8ab433476d7e3d47bec80b8b49a7b6))

### [3.3.6](https://gitlab.com/beautyworld_repository/clinic/compare/v3.3.5...v3.3.6) (2024-07-20)


### Features

* **livewire-components:** update element styling and image source ([b5e4913](https://gitlab.com/beautyworld_repository/clinic/commit/b5e49133a8388fa8e7b2c9f15c1a9bed5cd789de))
* **product:** refactor product details and fees components ([076873f](https://gitlab.com/beautyworld_repository/clinic/commit/076873f287fb1cabc8b3376c84fc8fa2bbff453d))

### [3.3.5](https://gitlab.com/beautyworld_repository/clinic/compare/v3.3.4...v3.3.5) (2024-07-20)


### Features

* **product-table:** add search functionality and UI updates ([f315929](https://gitlab.com/beautyworld_repository/clinic/commit/f31592972934a07294e20e73fd4c8105e9d60320))

### [3.3.4](https://gitlab.com/beautyworld_repository/clinic/compare/v3.3.3...v3.3.4) (2024-07-20)


### Features

* **customer-form:** modify UI layout, enhance search functionality and add unit tests ([12057d3](https://gitlab.com/beautyworld_repository/clinic/commit/12057d3bbea471143d3ce95fd04dd7585791f03a))
* **employee-table:** update employee search and table view ([31c5788](https://gitlab.com/beautyworld_repository/clinic/commit/31c57883c52fab244b4f0237d7a8650662e882c4))
* **livewire:** refactor role-page and role-table components, and add tests ([6c2c42b](https://gitlab.com/beautyworld_repository/clinic/commit/6c2c42be94764b6ba79485962127fedf3568f6ef))
* **permission-table:** refactor permission page and table components ([96b39e4](https://gitlab.com/beautyworld_repository/clinic/commit/96b39e49d258e7b9dcee247e87044d3a11643133))

### [3.3.3](https://gitlab.com/beautyworld_repository/clinic/compare/v3.3.2...v3.3.3) (2024-07-19)


### Features

* **category-table:** add reset page function on search update ([42d09ca](https://gitlab.com/beautyworld_repository/clinic/commit/42d09ca83391c9a8ce287342a0f71499d40475ce))
* **category-table:** add search filter functionality and test cases ([53e8c97](https://gitlab.com/beautyworld_repository/clinic/commit/53e8c973b8a7a2c93d08d21756ccaae9c103a944))
* **livewire-treatment:** update function parameters and reorganize code ([55bade7](https://gitlab.com/beautyworld_repository/clinic/commit/55bade7b9f1669824e953f7a16a8c4c0b81b8127))
* **payment-components:** refactor layout and add tests ([c5c203c](https://gitlab.com/beautyworld_repository/clinic/commit/c5c203cf279ffce77c9cd46cecde4179d4ee758b))
* **permission:** add tests and refactor permission components ([6f1de59](https://gitlab.com/beautyworld_repository/clinic/commit/6f1de599b002154f0d934cd4a7f1795f28b4f7e6))
* **product-page:** refactor product table and test ([1a70432](https://gitlab.com/beautyworld_repository/clinic/commit/1a704324809389c981f0ef62c02c6f704855f31d))
* remove subscription-plan-controller from treatment-page and revise treatment-table ([fae78f1](https://gitlab.com/beautyworld_repository/clinic/commit/fae78f15871da3d7aaff9d71bd3f3c6cb3967870))
* **role-page:** enhance UI and add tests ([86e8702](https://gitlab.com/beautyworld_repository/clinic/commit/86e8702d7f9a277da7edc6f812b305a3a778509e))
* **room-table:** overhaul UI and add search functionality with tests ([2b6cd86](https://gitlab.com/beautyworld_repository/clinic/commit/2b6cd8625cd213431ecda29aedc096ced7c78edf))
* **sub-category:** refactor tests and UI components ([4b947a3](https://gitlab.com/beautyworld_repository/clinic/commit/4b947a3495b799e89336f3e10ecaf77e9ff68549))
* **tests-http-livewire:** add subCategoryTable test and update roomTable test ([375b55a](https://gitlab.com/beautyworld_repository/clinic/commit/375b55a2a680a27576dabd3d66d75b303122242f))
* **tests-http-livewire:** implement vendor table tests and update views ([b3a1897](https://gitlab.com/beautyworld_repository/clinic/commit/b3a18976d2424242c4d4c09e0fcb781e2ff3bc80))
* **treatment-page:** implement treatment page improvements ([8a80dd6](https://gitlab.com/beautyworld_repository/clinic/commit/8a80dd6db15f68294d46db4729bef516c249f4d0))
* **unit-of-measure:** refactor code and add test ([0c02f75](https://gitlab.com/beautyworld_repository/clinic/commit/0c02f757fd4de41391e1801000078dffcb0ad639))
* **user-table:** enhance user search, update UI and add tests ([b59c3ee](https://gitlab.com/beautyworld_repository/clinic/commit/b59c3ee4bf71c8d9feb5feff25bafecf2b5e6061))

### [3.3.2](https://gitlab.com/beautyworld_repository/clinic/compare/v3.3.1...v3.3.2) (2024-07-18)

### [3.3.1](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.19...v3.3.1) (2024-07-18)


### Features

* **room-page:** update UI and add unit tests ([763d032](https://gitlab.com/beautyworld_repository/clinic/commit/763d032bd29f22618bdb7d476eb9108ac5d3bbed))
* **unit-of-measure:** enhance UI and add tests ([3303186](https://gitlab.com/beautyworld_repository/clinic/commit/33031862f2501c1b8aeee64f996d2b082deb7bcd))

### [3.2.19](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.18...v3.2.19) (2024-07-18)


### Features

* **employee-page:** add breadcrumb, revamp structure and add tests ([5d8bdde](https://gitlab.com/beautyworld_repository/clinic/commit/5d8bdde7eb0f98c016d6982fa9a2378ba224705b))
* **livewire-components:** delete CustomerFilter, modify layouts ([1d83668](https://gitlab.com/beautyworld_repository/clinic/commit/1d83668b45281e1f5467ebeabde7c56d35d7a6bf))
* **livewire:** refactor category and employee pages ([9e96431](https://gitlab.com/beautyworld_repository/clinic/commit/9e964313430e1626b7466ff21f257154ca46ee8d))
* **tests-feature:** add CustomerPageTest and rename CategoryTest to CategoryPageTest ([b32d008](https://gitlab.com/beautyworld_repository/clinic/commit/b32d00804f816a5c1cdc06b3893952fb46e79abb))
* **tests-feature:** add SubscriptionPage test ([287e64c](https://gitlab.com/beautyworld_repository/clinic/commit/287e64ca71026620a56f1ddefcef03f1e13b0d60))
* **tests-feature:** update tests and title for sales report page ([9882a5f](https://gitlab.com/beautyworld_repository/clinic/commit/9882a5fb8c5b0e4533f2c0b5b99a362ae9994621))


### Bug Fixes

* **livewire-subscription-search:** update subscription selection logic ([d1bfff1](https://gitlab.com/beautyworld_repository/clinic/commit/d1bfff1ff0ea0c991040dbe7280158ea3e64b4df))

### [3.2.18](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.17...v3.2.18) (2024-07-17)


### Features

* **database-factories-and-views:** update UserDetailFactory and add payment link ([a6d1c31](https://gitlab.com/beautyworld_repository/clinic/commit/a6d1c313971ad785b95d341b6bcdbd74b017bf42))
* **livewire-components:** refactor UI and add breadcrumb component ([4f62c63](https://gitlab.com/beautyworld_repository/clinic/commit/4f62c6376b253c814d8436e923fd859767eab095))
* **livewire-components:** update layout and add scripts to payment-page and related components ([8dfbfe4](https://gitlab.com/beautyworld_repository/clinic/commit/8dfbfe4363b678b663e3dd8e29b1a1170f976249))
* **payment-module:** refactor components and update tests ([50b4611](https://gitlab.com/beautyworld_repository/clinic/commit/50b4611f8b9046635e3459046949ef111d60f422))
* **README:** add conventional commits section ([ca7cc4a](https://gitlab.com/beautyworld_repository/clinic/commit/ca7cc4a3f7a7a6e8746db4387f93f99211c2821e))
* **setting:** remove purchase-order and add printer setting ([a0af1a9](https://gitlab.com/beautyworld_repository/clinic/commit/a0af1a92561f4598585395b1aedae2743458a44f))

### [3.2.17](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.16...v3.2.17) (2024-07-17)


### Features

* **customer-transaction-table:** add customer ID and modify transaction retrieval ([f28baa4](https://gitlab.com/beautyworld_repository/clinic/commit/f28baa4a3f613c54bf0a106d1f496be0399a397c))
* **livewire-components:** update user data handling and table view ([3b4a7a7](https://gitlab.com/beautyworld_repository/clinic/commit/3b4a7a7a7638cee3995d0e26ee8d9ee99b755776))
* **livewire-navbar-vertical:** add test and update render method ([e6091a4](https://gitlab.com/beautyworld_repository/clinic/commit/e6091a467a9856c9abc9287b85e2707d0b3bb05b))
* **payment-livewire-test:** update return type, add test file, remove commented seeders ([8cd1bb9](https://gitlab.com/beautyworld_repository/clinic/commit/8cd1bb99e012a29aed41cbe0bc0849ecdcd5f6b1))
* **tests-http-livewire:** add MasterDataLink component tests ([35789de](https://gitlab.com/beautyworld_repository/clinic/commit/35789de419030bca79544f8cc562c294986744e5))
* **tests-http-livewire:** added new livewire component tests and updated navbar-vertical view ([3495522](https://gitlab.com/beautyworld_repository/clinic/commit/3495522ebaede9e6b871ca667b2d40c189ecfab6))
* **user-detail-form-data-redemption-page:** add new fields and user parameter ([52ee4d8](https://gitlab.com/beautyworld_repository/clinic/commit/52ee4d888fd4e4200cd9aa336e0c195592a8ff43))

### [3.2.16](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.15...v3.2.16) (2024-07-16)


### Features

* **data-redemption:** split redemption profile data and redemption user data ([2904893](https://gitlab.com/beautyworld_repository/clinic/commit/29048939abe8f325a9de17411b07a6a4aeb89b8d))
* **user-table:** refactor user-table and remove user-details-modal ([38996de](https://gitlab.com/beautyworld_repository/clinic/commit/38996de0119da64b8502e48220db80678a42d89e))

### [3.2.15](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.14...v3.2.15) (2024-07-16)


### Features

* **backup:** Configure backup settings ([b6af51d](https://gitlab.com/beautyworld_repository/clinic/commit/b6af51d6e206213e3e1c334a625d3df56322415d))
* **dashboard-turnover-chart:** initialize selectedMonth variable ([1d9a04d](https://gitlab.com/beautyworld_repository/clinic/commit/1d9a04d105e4a130f05feada96f0194275d77fc1))
* **DashboardPage:** add mount function for selectedMonth ([8de06fe](https://gitlab.com/beautyworld_repository/clinic/commit/8de06fe393efbe19785c51685c90ca73c60c5611))
* **database-model-controller-seeder:** add employee-targets infrastructure ([3a052cb](https://gitlab.com/beautyworld_repository/clinic/commit/3a052cb19c6714b55c5fc0ffe2366e903caa5529))
* **kernel-dashboard-customer-birthday:** add return types and update birthday wishes layout ([ab5c638](https://gitlab.com/beautyworld_repository/clinic/commit/ab5c638e50ec3b4c04bae39f6b39d15dfea452ef))
* **livewire-component:** separate data redemption and profile tabs functionality ([71c7592](https://gitlab.com/beautyworld_repository/clinic/commit/71c7592343f782da775009083a1b9295ea9388af))
* **livewire-components:** implement various improvements ([5c0120a](https://gitlab.com/beautyworld_repository/clinic/commit/5c0120a06d930182132d8e406a5ddc5929527f54))
* **livewire-components:** refactor customer to user and improve error handling ([cc17cd9](https://gitlab.com/beautyworld_repository/clinic/commit/cc17cd921a6076138e45a1cf6ac8d07437ad517a))
* **livewire-components:** update customer-avatar and customer-card components ([8994530](https://gitlab.com/beautyworld_repository/clinic/commit/89945308805c830237e3acd2bea3094032985c33))
* **livewire-data-redemption:** extend user profile details and transaction history ([041552c](https://gitlab.com/beautyworld_repository/clinic/commit/041552c4ce3e33db13356dc01abdd6641bccb643))
* **login-page:** refactor login function and add corresponding tests ([6665686](https://gitlab.com/beautyworld_repository/clinic/commit/6665686140ab9d6aa62f9c8e5f48b34d42d74cd6))
* **tests-feature:** add CompanyDataPageTest ([fd010f9](https://gitlab.com/beautyworld_repository/clinic/commit/fd010f9f85fe3bfe1a3ba973c13e7c7ce2976ecc))

### [3.2.14](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.13...v3.2.14) (2024-07-12)


### Features

* **dashboard-turn-over:** add month label and refactor properties refreshing ([69920b0](https://gitlab.com/beautyworld_repository/clinic/commit/69920b09018db92025a28949cec228bd6aa63893))
* **DashboardTurnOver-Chart:** update selected month value and adjust methods ([6d28054](https://gitlab.com/beautyworld_repository/clinic/commit/6d2805488ef07f86aef6b80cce0af69c5f97f93e))
* **livewire-dashboard-turnover:** add mount method and update setSelectedMonth ([d27b76f](https://gitlab.com/beautyworld_repository/clinic/commit/d27b76f18fed51db1b55e20939afe3d67afb541b))

### [3.2.13](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.12...v3.2.13) (2024-07-12)


### Features

* **app-http-livewire-dashboard-page:** remove unused properties, methods, and refactor code ([eee310e](https://gitlab.com/beautyworld_repository/clinic/commit/eee310eb13c2cf866e91039880635437a5e99340))

### [3.1.13](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.12...v3.1.13) (2024-07-12)


### Features

* **app-http-livewire-dashboard-page:** remove unused properties, methods, and refactor code ([eee310e](https://gitlab.com/beautyworld_repository/clinic/commit/eee310eb13c2cf866e91039880635437a5e99340))

### [3.2.12](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.11...v3.2.12) (2024-07-11)


### Features

* **dashboard-trend:** refactor date variable and sales-data calculation ([e51d6eb](https://gitlab.com/beautyworld_repository/clinic/commit/e51d6ebab0347c02617073b13ef80f3e02605284))

### [3.2.11](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.10...v3.2.11) (2024-07-11)

### [3.2.10](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.9...v3.2.10) (2024-07-11)


### Features

* **dashboard-page:** extract monthly sales to a livewire component ([a13b806](https://gitlab.com/beautyworld_repository/clinic/commit/a13b806bbae471968eeda7fe6256d0dee00f4e62))
* **dashboard:** overhaul live data and charts, update database schema ([0917190](https://gitlab.com/beautyworld_repository/clinic/commit/0917190809bc076b5c5503fa2854bb0971d8854d))
* **DashboardTurnOver:** add new livewire component and update dashboard-page ([b61ae95](https://gitlab.com/beautyworld_repository/clinic/commit/b61ae95b6968b8335484fe63828e80ec9fc5517f))
* **database-seeders:** enable payment-seeder and update related models and tests ([5470bf6](https://gitlab.com/beautyworld_repository/clinic/commit/5470bf6a72d7fe122cc9e8e28f9ba8244c17452d))
* **database-tests:** modify database seeders and add new product tests ([f71b83d](https://gitlab.com/beautyworld_repository/clinic/commit/f71b83d138d3ccf63a83991c8406d138f2448416))
* **livewire-dashboard:** add today sales view and tests ([105c3a1](https://gitlab.com/beautyworld_repository/clinic/commit/105c3a1cd9034a0780c4fdc15145ba74e031c1c6))
* **tests-feature:** add dashboard widget test ([e5387b1](https://gitlab.com/beautyworld_repository/clinic/commit/e5387b1c89618212010cb941fd4b1b81223bc74e))
* **tests:** add new test files and update seeders ([5112f82](https://gitlab.com/beautyworld_repository/clinic/commit/5112f8204b362bff6b9b1476defa7d98d75548a4))
* **vendor-test-factory-page-table:** update code and add new test cases ([24b0cca](https://gitlab.com/beautyworld_repository/clinic/commit/24b0cca651951b612ba78a040f59405530499cb9))
* **VendorTable:** add pagination view method ([b42ba6e](https://gitlab.com/beautyworld_repository/clinic/commit/b42ba6ec6599a21a1163f3b56b8eefeeb45dce81))

### [3.2.9](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.8...v3.2.9) (2024-07-10)


### Features

* **app-http-livewire:** update various components and methods ([e7beeab](https://gitlab.com/beautyworld_repository/clinic/commit/e7beeab6ca4d7da10e0d3ecbf832d0a3cf1078bf))
* **customer-profile:** implement upload and display customer avatar ([7a9d677](https://gitlab.com/beautyworld_repository/clinic/commit/7a9d677ccd52f33dd21a6bb6350fb90a9b0bc7cf))
* **livewire-customer-avatar:** integrate updating customer avatar functionality ([ee9a2da](https://gitlab.com/beautyworld_repository/clinic/commit/ee9a2da70b5af3777c4d1e26487021fa3475b3f3))
* **livewire-customer:** modify data-redemption route and add new customer tests ([6cb1b64](https://gitlab.com/beautyworld_repository/clinic/commit/6cb1b643b90065209909ad6af6cf27130f7a08db))

### [3.2.8](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.7...v3.2.8) (2024-07-10)


### Features

* **livewire-avatar-file-upload:** add image upload and display feature ([4186e12](https://gitlab.com/beautyworld_repository/clinic/commit/4186e12cbf15825e1a56bec463a24d44fefa0951))
* **UserController:** extract image URL methods and listeners update ([70533cb](https://gitlab.com/beautyworld_repository/clinic/commit/70533cba44997c53fb7e406f706388a27adc1f7f))

### [3.2.7](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.6...v3.2.7) (2024-07-10)


### Features

* **database-migrations-config-composer:** add media library support ([fa5539a](https://gitlab.com/beautyworld_repository/clinic/commit/fa5539afca628028623382589ba68a317cf4c9e9))
* **tests-feature:** add avatar file upload test and update user profile test ([1187d17](https://gitlab.com/beautyworld_repository/clinic/commit/1187d177cec58db8e7e3620210fc252e43699cd7))
* **user-profile-updates:** add cover-image and avatar-file functionality ([ad65504](https://gitlab.com/beautyworld_repository/clinic/commit/ad65504005bd292640cad24936a78638cdf35e7d))
* **user-target:** add user target model and migration ([457e509](https://gitlab.com/beautyworld_repository/clinic/commit/457e50969111d9696f246f74a83d0f48209b06b1))

### [3.2.6](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.5...v3.2.6) (2024-07-09)


### Features

* **employee-detail:** add image upload validation ([747c6af](https://gitlab.com/beautyworld_repository/clinic/commit/747c6af794bbbcdfe055284ae034d4ce9b43b4ae))
* **user-profile:** refactor employee page to uniform user profile and add related tests ([bb7e22c](https://gitlab.com/beautyworld_repository/clinic/commit/bb7e22c3a1ad5dc6d50430b93ad95a6cdf7c1404))

### [3.2.5](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.4...v3.2.5) (2024-07-09)


### Features

* **console-commands:** add sync-email-user command ([242e968](https://gitlab.com/beautyworld_repository/clinic/commit/242e968106a6c47fa82067f6abd02f16d41a5343))
* **employee-details:** add dynamic user data and links to tables ([ea75deb](https://gitlab.com/beautyworld_repository/clinic/commit/ea75debba0e7289cacfba43acb0245246e3b687b))
* **livewire-components:** update user and employee details handling ([4d9461a](https://gitlab.com/beautyworld_repository/clinic/commit/4d9461a7bf3653f2fdee16a8415890b581a104da))
* **tests-feature:** update DashboardPageTest function format and add new test ([0d87918](https://gitlab.com/beautyworld_repository/clinic/commit/0d8791879ebdfa513984b685eca9b9ae6cac79a3))
* **today-sales-report-page:** add titlePage property and adapt tests ([bec4762](https://gitlab.com/beautyworld_repository/clinic/commit/bec4762872b1e59f62fa2f52019ecf37875f2956))

### [3.2.4](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.3...v3.2.4) (2024-07-09)

### [3.2.3](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.2...v3.2.3) (2024-07-09)


### Features

* **database-model-test-seeder:** update user details and add test ([07f2db7](https://gitlab.com/beautyworld_repository/clinic/commit/07f2db7cb5b4caa2529cd433ae022a1f07f65e9a))
* **emails:** add functionality to send test emails ([450698f](https://gitlab.com/beautyworld_repository/clinic/commit/450698f1c7201fe7acfa0037bd62f59990a7bcfd))
* **employee-detail-page:** enhance UI and add new components ([e043c44](https://gitlab.com/beautyworld_repository/clinic/commit/e043c449e5ae6783a4345ebb83b7d0ad3dda9303))
* **livewire-components:** extract base-data-link to standalone livewire component ([5b86d38](https://gitlab.com/beautyworld_repository/clinic/commit/5b86d3881f89669b1579a2ac9e9d4efde6b29030))
* **livewire-dashboard-link:** add employee link for authorized roles ([34903a2](https://gitlab.com/beautyworld_repository/clinic/commit/34903a2d8465e65328afe5955dc6353d73f5ef35))
* **livewire-view:** rename today-sales to today-sales-report ([58efc4a](https://gitlab.com/beautyworld_repository/clinic/commit/58efc4a2424aae54ed83427c77978d1639658ee5))
* **livewire:** replace region menu with livewire component ([e66332e](https://gitlab.com/beautyworld_repository/clinic/commit/e66332ecbf8caf87c9ab13251866d4ebcc50f4b4))
* **routes-permissions-views-tests:** update several files for deposits and permissions ([4387057](https://gitlab.com/beautyworld_repository/clinic/commit/43870571804bff61148299cad4866bcb17489766))
* **routes-tests-views:** rename routes and update tests ([001fc2e](https://gitlab.com/beautyworld_repository/clinic/commit/001fc2ea97c1f5c471b0328af318401d1e7482f7))

### [3.2.2](https://gitlab.com/beautyworld_repository/clinic/compare/v3.2.1...v3.2.2) (2024-07-08)


### Features

* **google-cloud-config:** add google-cloud-storage configuration and routes ([4f598da](https://gitlab.com/beautyworld_repository/clinic/commit/4f598da6df08387bc9cb1a8660067953cb673293))
* **dashboard-link:** add today sales report link to sidebar navigation ([3c99a91](https://gitlab.com/beautyworld_repository/clinic/commit/3c99a9131f2f39cfebd1ec26bfb07a96340c44c3))
* **livewire-employee-detail:** add new EmployeeDetailPage component and route ([3437f8c](https://gitlab.com/beautyworld_repository/clinic/commit/3437f8cf049f3a343621a861f2cc5d0e599ce19e))
* **livewire-stake-holder:** remove hardcoded stakeholderlink & enhance tests ([12a8878](https://gitlab.com/beautyworld_repository/clinic/commit/12a88788c2c4e6762baa6cf36be4f2e71a58dde5))
* **routes-tool:** add upload and download routes ([027f044](https://gitlab.com/beautyworld_repository/clinic/commit/027f0447e85e7a8a38fbaf1e32d8d3295535b51e))
* **subscription-plan-controller-treatment-table:** add subscription plan options to treatment table ([21dcdff](https://gitlab.com/beautyworld_repository/clinic/commit/21dcdff48ba76e5bd294843226f40cf59ccda7c7))

### [3.2.1](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.16...v3.2.1) (2024-07-03)


### Features

* **avatar-config:** add laravolt avatar and configure its properties ([9a0bf86](https://gitlab.com/beautyworld_repository/clinic/commit/9a0bf86c9a4161390993084f835941716c216a27))
* **console-commands:** add FixEmptyUserDetail and update User model ([7f20ac3](https://gitlab.com/beautyworld_repository/clinic/commit/7f20ac33c2f3503bc28740dd9e1c34a651065c4c))
* **customer-form:** add instagram, date-of-birth and place-of-birth fields ([c5c1447](https://gitlab.com/beautyworld_repository/clinic/commit/c5c1447116719d328d2009ca8b900a73e0cb394b))
* **database-migrations-dashboard-page:** add instagram, ([95b6da9](https://gitlab.com/beautyworld_repository/clinic/commit/95b6da9987e96e50aeec2714ddb6cc2eac6e7751))

### [3.1.16](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.15...v3.1.16) (2024-07-03)


### Features

* **company-controller-tests:** rename CompanyApiController, add new tests and methods ([13e7e90](https://gitlab.com/beautyworld_repository/clinic/commit/13e7e909ee02f478258b8e4ab309287f27d4202c))
* **Company-Model-Registration:** add token field and registration email notification ([20ce262](https://gitlab.com/beautyworld_repository/clinic/commit/20ce26239447b6a29b84bbd3f32e2296c085bea2))
* **company-register:** update form, refactor API request handling ([b1c5db3](https://gitlab.com/beautyworld_repository/clinic/commit/b1c5db3e1adca6eed5ab51c81e313784fa42dabc))
* **livewire-company:** improved layout, mail methods and added company-detail-form component ([2ead6fd](https://gitlab.com/beautyworld_repository/clinic/commit/2ead6fd65c22f09da2ba4d36aa74af7cc622802a))
* **tests-feature:** add tests, new CompanyDataPage, revise companyRegisterForm ([7415101](https://gitlab.com/beautyworld_repository/clinic/commit/74151018af21596d4e3031b0fd6401f6d0b050b7))
* **tests:** update and add new landing page tests ([d70b074](https://gitlab.com/beautyworld_repository/clinic/commit/d70b074eac0a0c05bc519528be39f41685672a08))
* **unit-of-measure:** use unit-of-measure relationship in product model ([f24bf19](https://gitlab.com/beautyworld_repository/clinic/commit/f24bf194c38258c946ca26ce700531fc0a94a7b2))

### [3.1.15](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.14...v3.1.15) (2024-07-01)


### Features

* **authentication:** replace first company and branch with authenticated user's company and branch ([ff81985](https://gitlab.com/beautyworld_repository/clinic/commit/ff81985b3578cc255ee8a769041633efd69ef5b1))
* **database-factories:** add new factories and update user role in tool-link view ([1113aa5](https://gitlab.com/beautyworld_repository/clinic/commit/1113aa511cca6a532f832c4c319ccda3eeb45d70))
* **helpers:** remove helper files and their usages ([8c529d0](https://gitlab.com/beautyworld_repository/clinic/commit/8c529d02b21bfeda018ce4a869fee6a35cdb0c19))
* **livewire-reset-password:** add forget password view and update reset password logic ([78196fc](https://gitlab.com/beautyworld_repository/clinic/commit/78196fce1b49ce5ed59a161a63572c94be5d1556))
* **livewire-views:** changed staff to employee and updated sales helper ([0863ea6](https://gitlab.com/beautyworld_repository/clinic/commit/0863ea649bdd6ac5ef38536ca38fdc838309847a))
* **livewire:** rename Staff to Employee, update user roles ([151d9c5](https://gitlab.com/beautyworld_repository/clinic/commit/151d9c55c3415fcf216a943f5a8efd9d699568d9))

### [3.1.14](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.13...v3.1.14) (2024-06-30)


### Features

* **sales-invoice:** rename itemLists to items ([497f53c](https://gitlab.com/beautyworld_repository/clinic/commit/497f53ce61f49b228045c21454c2d10a1988258f))
* **sales-invoice:** replace items with itemLists in various files ([e0fc0c0](https://gitlab.com/beautyworld_repository/clinic/commit/e0fc0c0f5b2a7c5f609ce9b1977c6819e2d736a7))

### [3.1.13](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.12...v3.1.13) (2024-06-29)


### Features

* Add 'senior doctor' role and refactor test files ([7eafb24](https://gitlab.com/beautyworld_repository/clinic/commit/7eafb24c9d6b6e9d8e56c31ac450ddd0e52bc63b))
* Add error-page and null coalesce auth checks ([77f9323](https://gitlab.com/beautyworld_repository/clinic/commit/77f93234c7024b59554cecc36a2c0685a17d7308))
* **ApiUserController-CompanyRegistrationTest-AdjustRedemptionForm:** update authentication and test process, handle null auth user ([766c90d](https://gitlab.com/beautyworld_repository/clinic/commit/766c90df85aa70ea25882b79c3219f92d3e59962))
* **commission-model:** remove HasFactory, add BelongsTo relation and fee_release column ([bede76c](https://gitlab.com/beautyworld_repository/clinic/commit/bede76c21efa9f5023a86db79d7a350c475a4428))
* **consumable-form:** refactor code and add inline comments ([2063b8b](https://gitlab.com/beautyworld_repository/clinic/commit/2063b8b14207539ff3292f337ffed455dbb86714))
* **database-migrations:** modify employee targets table structure ([aa8fe0a](https://gitlab.com/beautyworld_repository/clinic/commit/aa8fe0ab010949c92e475f08719db0c293e7b025))
* **database:** add seeders and factories for models ([9171f25](https://gitlab.com/beautyworld_repository/clinic/commit/9171f25fe899bfc68523689dc763d261da6b9c5c))
* **employee-target:** add employee target module and related changes ([9dd171f](https://gitlab.com/beautyworld_repository/clinic/commit/9dd171f1dc161013a5660f8cd8f191c99560e8cf))
* **employee-target:** update models, factories, and views ([dfdd720](https://gitlab.com/beautyworld_repository/clinic/commit/dfdd72060cc8458c3e4ea6e8c61acb64760ee176))
* **employee-target:** update models, factories, and views ([742fc4d](https://gitlab.com/beautyworld_repository/clinic/commit/742fc4d0a9b1086197c0072375f5ffef3f3e1bac))
* **errors-view:** add new 401 error page ([b1ab950](https://gitlab.com/beautyworld_repository/clinic/commit/b1ab950a3f033b5c4d47cd699cd70900c5f80687))
* **livewire-components:** add returning types, improve queries and enhance documentation ([6081cf3](https://gitlab.com/beautyworld_repository/clinic/commit/6081cf3b297b3e02e3b9cb98da2db146703384dc))
* **livewire-components:** add returning types, improve queries and enhance documentation ([4689993](https://gitlab.com/beautyworld_repository/clinic/commit/4689993d1962fa95ecb7ebebcbc37db7f21964ae))
* **permissions:** add null safe operator to user permissions checks ([253f36e](https://gitlab.com/beautyworld_repository/clinic/commit/253f36ebf0d42ca9f9c82ffd25d5b9c9fec9f1fa))
* **subscription-table:** update company_id source, remove good_receive from stock model and view ([a9279f0](https://gitlab.com/beautyworld_repository/clinic/commit/a9279f01c7c0301fc844fe117c756c3a1ebe6d1a))
* **tests:** refactor tests, rename and enhance helpers and controllers ([2c475e4](https://gitlab.com/beautyworld_repository/clinic/commit/2c475e48a9f19e3a9032c0760958b19193d0881a))
* **tests:** update existing tests, add new test for company registration ([5894301](https://gitlab.com/beautyworld_repository/clinic/commit/5894301cddece3faa661a3c58fec3287600e8a7f))
* Update database seeder and factories to use random default promo ([0e7e087](https://gitlab.com/beautyworld_repository/clinic/commit/0e7e087c6835387669cde38422ab0933b64c3264))
* **user-detail-form:** add docstrings and refactor function signatures ([412e009](https://gitlab.com/beautyworld_repository/clinic/commit/412e00907876e224521bfb9e91615bbba368363d))

### [3.1.12](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.11...v3.1.12) (2024-06-25)


### Features

* **staff-management:** add staff table and page components ([59b52a4](https://gitlab.com/beautyworld_repository/clinic/commit/59b52a474cd21043cbfdaa348d594b516c4f003a))


### Bug Fixes

* **sales-invoice-detail:** fix download excel not show doctor name ([0ebfdfd](https://gitlab.com/beautyworld_repository/clinic/commit/0ebfdfde04d2d2e17c75b309d9f0c5e5b445e1fe))

### [3.1.11](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.10...v3.1.11) (2024-06-20)


### Bug Fixes

* **makerplace product treatment fee:** fix can't update multiple fee ([14a2ed4](https://gitlab.com/beautyworld_repository/clinic/commit/14a2ed437669fe99b91e4d1e841c3d5ab6e3f582))
* **marketplace-fee:** fix new layout for marketplace fee ([0b208a2](https://gitlab.com/beautyworld_repository/clinic/commit/0b208a2c254c61fc5edb4426a968947b7d2ca727))
* **package-fee:** apply new layout package fee ([9947ea5](https://gitlab.com/beautyworld_repository/clinic/commit/9947ea5dc2b4fd61a14c89d83d3e06cb31206128))

### [3.1.10](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.9...v3.1.10) (2024-06-20)


### Bug Fixes

* **adjust-redemption-form:** add reference to seeder ([2bc0e53](https://gitlab.com/beautyworld_repository/clinic/commit/2bc0e53b07efb5f40934244113e7b79920cdf9f6))
* **app-command:** seed code and name on appointment items ([60bf048](https://gitlab.com/beautyworld_repository/clinic/commit/60bf0489c0b01a77838c0bc81d04237c6499b672))
* **app-command:** seed default service fee ([9e62b23](https://gitlab.com/beautyworld_repository/clinic/commit/9e62b23264b20c31fd7917e2a53b72f96243949d))
* **app-command:** seed module code, module name, reference code and reference name ([7ee03c8](https://gitlab.com/beautyworld_repository/clinic/commit/7ee03c825dd1285032782d2fbb2066980ab6cfdc))
* **app-command:** update code name sales invoice item ([b590386](https://gitlab.com/beautyworld_repository/clinic/commit/b5903868af7250cc116ada938f4e1daab3ba4a90))
* **maketplace-form:** add generate commission on store method ([0603e1a](https://gitlab.com/beautyworld_repository/clinic/commit/0603e1a4742013ec7ec1a3a71d641074c4a56122))
* **marketplace:** refactor marketplace detail ([114461c](https://gitlab.com/beautyworld_repository/clinic/commit/114461c2e4541f1046fb998b5b9d0bef52dc30c5))
* **package:** fix package fee ([a7c914b](https://gitlab.com/beautyworld_repository/clinic/commit/a7c914ba48ef61d5c75c5174be856976b5aaab66))
* **packate-table:** fix broken link package item ([407bc4d](https://gitlab.com/beautyworld_repository/clinic/commit/407bc4d0bf5c7f2f7c5a62edba3947cf138ffb49))
* **product-detail:** fix product history table ([7a8a465](https://gitlab.com/beautyworld_repository/clinic/commit/7a8a465a8f8edca85cfa363bdb11e6696f15f0dd))
* **product-detail:** fix product history table ([888a1ca](https://gitlab.com/beautyworld_repository/clinic/commit/888a1ca0d00d76bdeb2f50efc34bc656450627db))
* **product-detail:** fix product history table because we use customer_price before ([31b517c](https://gitlab.com/beautyworld_repository/clinic/commit/31b517cf3ed83a88b2e824d9f02bd7878fb5fab6))
* **product-fee:** delete product fee then write new product fee ([67aa924](https://gitlab.com/beautyworld_repository/clinic/commit/67aa9247f72c0ee26de92f12503da7a9009efd74))
* **product-fee:** now we set the fee page into one trigger ([9e3b38b](https://gitlab.com/beautyworld_repository/clinic/commit/9e3b38b906f42bdbec1c4c0a2bba253fa79a3464))
* **product-form:** prevent delete product if product been sold ([1e42274](https://gitlab.com/beautyworld_repository/clinic/commit/1e4227406489f4bec09a381ccde5ea8f0b2d54df))
* **product-target-fee:** fix product target fee ([bc40202](https://gitlab.com/beautyworld_repository/clinic/commit/bc40202701d1ed0543fd65de0175476e661406b6))
* **product-target-fee:** fix product target fee ([5f9ae75](https://gitlab.com/beautyworld_repository/clinic/commit/5f9ae75b9fd262dd53faf3a14b881621c9542329))
* **sales-detail-report:** fix bug price ([a465534](https://gitlab.com/beautyworld_repository/clinic/commit/a46553455f66789276e513de4e78c6b7fcfea217))
* **sales-detail-report:** fix bug price ([f6b55d3](https://gitlab.com/beautyworld_repository/clinic/commit/f6b55d3402da01bdb8110b0dcf63c17e4298d443))
* **sales-detail-report:** fix bug price ([99ff2e2](https://gitlab.com/beautyworld_repository/clinic/commit/99ff2e2c3bf64bac26f8d3eec319c0e1cf4ca246))
* **sales-detail-report:** fix bug price ([fbfc32e](https://gitlab.com/beautyworld_repository/clinic/commit/fbfc32e1b2cc08fe68dab23eac140e6d8c023885))
* **sales-detail-report:** fix bug price ([707bb8a](https://gitlab.com/beautyworld_repository/clinic/commit/707bb8a80e96580ad74ff25d6582e767cbe8b35b))
* **sales-detail-report:** fix bug price ([826f807](https://gitlab.com/beautyworld_repository/clinic/commit/826f80706076eab96b59b2eddbbca6cea259e777))
* **sales-detail-report:** fix error normal price not found ([9d6c694](https://gitlab.com/beautyworld_repository/clinic/commit/9d6c694bc6d4556fe41f4f3ee02463b6a2a94095))
* **service-details:** fix display decimal on service detail ([654040e](https://gitlab.com/beautyworld_repository/clinic/commit/654040e59aeacf3b18e87f3403318151ddfd6275))
* **treament-fee marketplace-fee:** update treatment and market place fee ([89ae118](https://gitlab.com/beautyworld_repository/clinic/commit/89ae11855eade08b83e524cedeae8b4ae7a323e5))
* **treatment-detail:** fix category and subCategory now show ([d205f6e](https://gitlab.com/beautyworld_repository/clinic/commit/d205f6eb6b9ae6a6f077cdaedfac807238da0295))
* **treatment:** prevent delete treatment if treatment been sold ([2550340](https://gitlab.com/beautyworld_repository/clinic/commit/2550340e1ab7dac4c5a667cbaff55edcb5b8f972))

### [3.1.9](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.8...v3.1.9) (2024-06-10)


### Bug Fixes

* **format-helpers:** allow countFormat to decimal value ([5b28678](https://gitlab.com/beautyworld_repository/clinic/commit/5b286786b9a29bde57ffbf62a68f4092562c30b3))
* **sales-detail-report:** fix bug when null customer parse ([6a61694](https://gitlab.com/beautyworld_repository/clinic/commit/6a616945598289b246bc96fe543848e6c6d673b6))

### [3.1.8](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.7...v3.1.8) (2024-06-10)


### Bug Fixes

* **all-you-can-treat-fee:** apply senior doctor to store method ([828db4a](https://gitlab.com/beautyworld_repository/clinic/commit/828db4a5825beabaa98657d4ab04e5ea63943109))
* **generate-excel:** fix empty data row when download sales invoice report ([9d67219](https://gitlab.com/beautyworld_repository/clinic/commit/9d67219fc713775bee915fe38b73919061fe5f56))
* **market-place-fee:** add senior doctor to fee form ([a3e4cc9](https://gitlab.com/beautyworld_repository/clinic/commit/a3e4cc9ce8bec86d6750f4c64ac27e23f7d88c86))
* **product-detail:** add fee symbol following the description ([f5fe253](https://gitlab.com/beautyworld_repository/clinic/commit/f5fe253742b55ee588128740bbcae813d0a2deda))
* **product-detail:** refactor product detail item to component ([a8527df](https://gitlab.com/beautyworld_repository/clinic/commit/a8527dfbf7dbd0e88f69462d565ab70dc5876e96))
* **product-form:** fix error edit data when code used already ([c655e11](https://gitlab.com/beautyworld_repository/clinic/commit/c655e112ccff752491b54aac80d933cebfbd35df))
* **sales-detail-report:** fix total price wrong compute ([f7837c9](https://gitlab.com/beautyworld_repository/clinic/commit/f7837c9c570482bf0519b3415d9b0e941083d968))
* **treatment-detail:** refactor treatment detail item to component ([2774302](https://gitlab.com/beautyworld_repository/clinic/commit/277430296eddba4a83606a76dc1884f8cca1b16b))
* **treatment-fee:** add senior doctor to fee form ([b8e51af](https://gitlab.com/beautyworld_repository/clinic/commit/b8e51af3404bd04c5d59f2b670f153e02776a5b9))

### [3.1.7](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.6...v3.1.7) (2024-06-07)


### Bug Fixes

* **all-you-can-treat-fee:** apply new rule to commission ([f0c3c68](https://gitlab.com/beautyworld_repository/clinic/commit/f0c3c684ce3a54ae321853f2198d0f244dc5aaea))
* **all-you-can-treat-fee:** fix store data for missing module name ([ae64602](https://gitlab.com/beautyworld_repository/clinic/commit/ae646022c663e4a025c872c25e4e9cbbdf0da23b))
* **product-fee:** add senior doctor to fee form ([2fccb0c](https://gitlab.com/beautyworld_repository/clinic/commit/2fccb0c7dde21715e231774bcb174c5fa673a0bb))
* **product-fee:** apply new rule to commission ([ea3764a](https://gitlab.com/beautyworld_repository/clinic/commit/ea3764a8988a401cdbc6ad8f84b93d1777b8224e))
* **product-fee:** fix store data for missing module name ([71b8aba](https://gitlab.com/beautyworld_repository/clinic/commit/71b8aba1d0164469f5ab1a056d55101d41c2d69d))
* **sales-detail-report:** use time for get sales detail report data ([8f731a9](https://gitlab.com/beautyworld_repository/clinic/commit/8f731a9ea349f203d9084dffdc8382ba86a30df9))
* **sales-invoice-form:** ignore payment when store data ([50c42f6](https://gitlab.com/beautyworld_repository/clinic/commit/50c42f65e61de4cf140c075b13b5745a978cf7ee))
* **staff-search:** add senior doctor to doctor options ([8dad028](https://gitlab.com/beautyworld_repository/clinic/commit/8dad02808742d2bd81d8228ca2dd858264e73293))
* **staff-search:** add senior doctor to doctor options ([892e791](https://gitlab.com/beautyworld_repository/clinic/commit/892e7912584ee0cbeb9fa9c13b0492b5135eebb9))
* **user-form:** fix clear user form ([af725bf](https://gitlab.com/beautyworld_repository/clinic/commit/af725bf57d084cfdbf9f45107f8caf8262514b5d))

### [3.1.6](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.5...v3.1.6) (2024-06-04)


### Features

* **service-fees:** use service-fees to make fee dynamic ([da06018](https://gitlab.com/beautyworld_repository/clinic/commit/da060182741934f3a6af3d79dbb21def227d8ee2))


### Bug Fixes

* **cash-drawer-form:** fix auto float val on payment properti ([7a9c827](https://gitlab.com/beautyworld_repository/clinic/commit/7a9c82789798baa606fe2efa1bf5a3b59118f25a))
* **cash-drawer-form:** fix auto float val on payment property ([f05693c](https://gitlab.com/beautyworld_repository/clinic/commit/f05693c61cf5a111b37d45e9cc3014f2ac2f45dc))
* **cash-drawer-form:** optimize function ([9ed0268](https://gitlab.com/beautyworld_repository/clinic/commit/9ed02683d8b00c5b5460cf43e22516fe043fdf59))
* **customer-helpers:** masking phone number on development environment ([680eae9](https://gitlab.com/beautyworld_repository/clinic/commit/680eae9c76895f441b3c5bc19082cde522fda602))
* **dashboard-page:** make dashboard chart working but not completed yet ([42b35fe](https://gitlab.com/beautyworld_repository/clinic/commit/42b35fe891927660fafbf532710725294bbcd5ac))
* **dashboard-page:** make dashboard chart working but not completed yet ([329fc93](https://gitlab.com/beautyworld_repository/clinic/commit/329fc934338c8c90474460056d74debeb8fbd560))
* **product-normal-fee:** fixed store function ([6ad4eba](https://gitlab.com/beautyworld_repository/clinic/commit/6ad4eba8faab9cfc0eeb4e313a806be0563631ff))
* **product-normal-fee:** fixed store function ([18bb3d6](https://gitlab.com/beautyworld_repository/clinic/commit/18bb3d6780a35e220e3b4c963f997b136c640d70))
* **sales-invoice-forms:** fix null id because emit clear ([a8e2f33](https://gitlab.com/beautyworld_repository/clinic/commit/a8e2f33120a69f6097c458e9eeb860aa2e454ad0))
* **sales-invoice-items:** set price and total price zero when redeem treatment ([e658c72](https://gitlab.com/beautyworld_repository/clinic/commit/e658c724543d7407447ff5f66b63b28b3974558d))
* **turn-over-report-table:** add filter by hours ([b789581](https://gitlab.com/beautyworld_repository/clinic/commit/b789581f8924f6f05c499dc10f923706e11d8e73))
* **turn-over-report-table:** skip turn over with zero value, add footer to display summary ([e0f1cf9](https://gitlab.com/beautyworld_repository/clinic/commit/e0f1cf96f1fb6c1633e4dceff196926ce314d1c5))

### [3.1.5](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.4...v3.1.5) (2024-05-31)


### Bug Fixes

* **customer-search:** remove swal alert when customer empty ([3e50792](https://gitlab.com/beautyworld_repository/clinic/commit/3e50792ad551729be278fb1b0154882183ac7cb4))
* **product-helpers:** allow null and set to zero ([ea1720b](https://gitlab.com/beautyworld_repository/clinic/commit/ea1720b4d5d8503bcd1bc035f0e888eb5dda5261))
* **product-helpers:** allow null and set to zero ([6028990](https://gitlab.com/beautyworld_repository/clinic/commit/6028990b1b9724db3397cb228d529389d181058c))

### [3.1.4](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.3...v3.1.4) (2024-05-27)


### Features

* **market-place:** create command for update market place fee ([c44a097](https://gitlab.com/beautyworld_repository/clinic/commit/c44a0970c8ff1ce7641ade35603afaf90bf55530))
* **package:** add command to fill package fee and extract package history transaction ([f5c79d4](https://gitlab.com/beautyworld_repository/clinic/commit/f5c79d4bae70385d6ccc844bff480d08ddad2fec))
* **product-fee:** seed fee data to product ([b682af9](https://gitlab.com/beautyworld_repository/clinic/commit/b682af961614547ade9233fb39f55245f838c3d9))
* **treatment-fee:** add command to fill treatment fee ([6b30377](https://gitlab.com/beautyworld_repository/clinic/commit/6b30377d624d65d9b177f34eca3fef891994f2bc))


### Bug Fixes

* **application:** rename fee to fees on chain model ([8c30be3](https://gitlab.com/beautyworld_repository/clinic/commit/8c30be3e94f3f876fe5345c3d25925f9e8bf07c0))
* **commission:** change filter do dateFrom - dateTo ([e3c2926](https://gitlab.com/beautyworld_repository/clinic/commit/e3c2926e4b795fa44bf5d5872fc35520a6614287))
* **commission:** change filter do dateFrom - dateTo ([61513f8](https://gitlab.com/beautyworld_repository/clinic/commit/61513f85f7860ed651d6e0fe6dcfaae172e378a4))
* **commission:** change filter do dateFrom - dateTo ([39ff1c9](https://gitlab.com/beautyworld_repository/clinic/commit/39ff1c901b36171acd84b5c8458d25c5e2466cb6))
* **person-in-charge:** prevent sell service is sales is empty ([c8d36fc](https://gitlab.com/beautyworld_repository/clinic/commit/c8d36fcad6834c652251a061d7eeaa1833a8a830))
* **person-in-charge:** revert marketplace option to choose pic ([e63f148](https://gitlab.com/beautyworld_repository/clinic/commit/e63f148533f594efc1ac8920c9113d87146222ed))
* **product:** fix data stream for product on sales invoice ([f7dbc7d](https://gitlab.com/beautyworld_repository/clinic/commit/f7dbc7d2b4d142d68b2419df3fd999b4b0f3d6d1))
* **receipt:** adjust layout ([a218854](https://gitlab.com/beautyworld_repository/clinic/commit/a218854bf36ad2d114c291f23d1fb19c21ec8444))
* **sales-invoice-form:** prevent add item if no sales selected ([bb330e4](https://gitlab.com/beautyworld_repository/clinic/commit/bb330e44991273db1ec2e6516ec6e85d44013c02))
* **sales-invoice-form:** prevent add item if no sales selected ([fa4fc98](https://gitlab.com/beautyworld_repository/clinic/commit/fa4fc9865afd03edfcb49116d92b5666a50495f1))
* **subscription-form:** add expired date ([4db047b](https://gitlab.com/beautyworld_repository/clinic/commit/4db047b6418b03d2b3296037eb83b060a6e33bfd))
* **subscription-form:** except code and buy date when load modal ([e8a2438](https://gitlab.com/beautyworld_repository/clinic/commit/e8a2438c436d6bb171add79bb21932d20975fca1))

### [3.1.3](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.2...v3.1.3) (2024-05-24)


### ⚠ BREAKING CHANGES

* **package-items-table:** - sales invoice
  - appointment
  - report

### Bug Fixes

* **data-redemption:** fix return null getSellPrice ([bb09837](https://gitlab.com/beautyworld_repository/clinic/commit/bb0983733f5793e6752d175883bec57fca09b5e3))
* **package-detail:** fix package item price ([3283710](https://gitlab.com/beautyworld_repository/clinic/commit/3283710aa644b901fa1563ba8d7fb66b647e0552))
* **package-item-table:** allow package item sell_price to be edited ([1464049](https://gitlab.com/beautyworld_repository/clinic/commit/146404921dc0f232b241ce694ceaf349a01f8104))
* **package-item-table:** allow package item sell_price to be edited ([683bf0d](https://gitlab.com/beautyworld_repository/clinic/commit/683bf0dd2519a5eaab5d1401fb8d85f0b763dbd1))
* **package-item-table:** refactor price name using last request from finance ([2b54232](https://gitlab.com/beautyworld_repository/clinic/commit/2b5423251f8498491922615d192f6b5aa4f21e26))
* **package-item:** fix price ([4b01a37](https://gitlab.com/beautyworld_repository/clinic/commit/4b01a37ae04e52840b6f96eb02465e85d9165959))
* **package-items-table:** refactoring package items ([0769619](https://gitlab.com/beautyworld_repository/clinic/commit/07696192a18a62e428705be15cc26d31bf2d72c6))
* **package:** remove category ([d42cd7b](https://gitlab.com/beautyworld_repository/clinic/commit/d42cd7b0ccb82cd6b671c786fa6caa32d2ad3169))
* **package:** remove category ([d880a5f](https://gitlab.com/beautyworld_repository/clinic/commit/d880a5fb96f718e6966e289ef40c7a59537e8cb5))
* **package:** remove category ([8a31882](https://gitlab.com/beautyworld_repository/clinic/commit/8a318820e9693d65026fb3904eb0ac09ad2b8b89))
* **subscription-form:** fix crud subscription form and add sales to see menu same the admin can see ([af80a86](https://gitlab.com/beautyworld_repository/clinic/commit/af80a869789770e0d64c13e56b3b4f3d19b55b83))
* **subscriptions:** fix crud ([fe64c36](https://gitlab.com/beautyworld_repository/clinic/commit/fe64c369f87ff926839eb62c2e991d21431db6d2))
* **subscriptions:** fix store method ([c8fe3d6](https://gitlab.com/beautyworld_repository/clinic/commit/c8fe3d6f38203afbc627ebb14b057a50953f7791))

### [3.1.2](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.1...v3.1.2) (2024-05-16)


### Features

* **sales-invoice-tool:** add commissions widget ([eb80573](https://gitlab.com/beautyworld_repository/clinic/commit/eb805733b8cd1cdd8552b7beb5fac2df4cff44c5))


### Bug Fixes

* **log-viewer:** add auth for see log viewer ([fe7b5af](https://gitlab.com/beautyworld_repository/clinic/commit/fe7b5af50dacae9897e88e9702cb30b9f290019e))
* **receipt-controller:** tidy up and set if env is local then redirect url to http://localhost:8000 ([8334286](https://gitlab.com/beautyworld_repository/clinic/commit/83342869c5acdc6440a8a01be3617187184efad7))
* **sales-invoices-tool:** fix searh by invoice number change method set to select ([8ed95a5](https://gitlab.com/beautyworld_repository/clinic/commit/8ed95a569544df23c70db83e3b8fbf6c62738bba))
* **sales-invoices-tool:** fix widget ([2c55a68](https://gitlab.com/beautyworld_repository/clinic/commit/2c55a68dadccd1457613c7f8c37f525d530f8081))
* **sales-invoices-tool:** tidy up ([9f9d261](https://gitlab.com/beautyworld_repository/clinic/commit/9f9d26136bfbb6519df1b487ebb173c630758960))
* **sales-invoices-tool:** tidy up ([f5f5f2a](https://gitlab.com/beautyworld_repository/clinic/commit/f5f5f2a8fedaf468b6007343f6dd9b8d98585e49))
* **sales-invoices-tool:** tidy up and initiate sales commission ([8e3c473](https://gitlab.com/beautyworld_repository/clinic/commit/8e3c47327f5a63714b7909376135537671f8945a))
* **turn-over:** add marketplace invoice on turn over table ([57d92ac](https://gitlab.com/beautyworld_repository/clinic/commit/57d92aca09d59b260ec1e694b68ef5da70b94e8d))

### [3.1.1](https://gitlab.com/beautyworld_repository/clinic/compare/v3.1.0...v3.1.1) (2024-05-14)


### Features

* **command-sync-doctor-assistance:** add command to sync doctor_assistance ([11c0037](https://gitlab.com/beautyworld_repository/clinic/commit/11c0037f739583cf54910605c140f872daef6302))
* **command-sync-doctor-assistance:** add command to sync doctor_assistance ([349aeb3](https://gitlab.com/beautyworld_repository/clinic/commit/349aeb3c53c89fa7c0ef8e3928455628a15b8acc))
* **commission-page:** apply commission by role ([b24058d](https://gitlab.com/beautyworld_repository/clinic/commit/b24058d6a46d424d1948876dad5fe40763cfbec2))
* **commission-table:** add new feature commission ([5207094](https://gitlab.com/beautyworld_repository/clinic/commit/5207094af65fb81bf80f1f0537fd03de83af5259))
* **commission-table:** add Sales Invoice Date on Commissions Table ([88422ae](https://gitlab.com/beautyworld_repository/clinic/commit/88422aea63be6efc87731379fe47e23411c6549d))
* **commission-table:** add Sales Invoice Date on Commissions Table ([a38e9e2](https://gitlab.com/beautyworld_repository/clinic/commit/a38e9e2b0cd011b0471c7f831dc61bcb295020f9))
* **turn-over-report:** add loading state ([5e7931e](https://gitlab.com/beautyworld_repository/clinic/commit/5e7931e941486b0adf42e1823e1e89067588a4e5))
* **turn-over-report:** fix autoload month label ([a9b2a15](https://gitlab.com/beautyworld_repository/clinic/commit/a9b2a15542774d2ddaae40f5774891d5db8bead4))


### Bug Fixes

* **appointment-table:** add destroy function ([ffa454d](https://gitlab.com/beautyworld_repository/clinic/commit/ffa454d0c33f85ee0bd7a62d29ffda97e0109928))
* **sales-incoice-form:** fix doctor assistance not recorded on database ([39cf736](https://gitlab.com/beautyworld_repository/clinic/commit/39cf7362f49e3f24efc113d9445623a8009c16cf))
* **sales-invoice-tool:** refactor how to load data sales invoices ([38d4cc6](https://gitlab.com/beautyworld_repository/clinic/commit/38d4cc67a2b2e0961867e6b6e0cc0aca61cebd94))
* **sales-invoice-tool:** refactor how to load data sales invoices ([c34becd](https://gitlab.com/beautyworld_repository/clinic/commit/c34becd2e32ecdcc680675036f136999d48db619))
* **sales-invoice-tool:** refactor how to load data sales invoices ([4aaa0d4](https://gitlab.com/beautyworld_repository/clinic/commit/4aaa0d4641ceb9e9d4b225289eba2ebfe6b1b42c))
* **sales-invoices:** allow null for get sales invoice params ([45fb2ef](https://gitlab.com/beautyworld_repository/clinic/commit/45fb2efe2410770ba3dbb776563bab636a7e25d8))
* **treatment-details:** add grand total cost ([063b98e](https://gitlab.com/beautyworld_repository/clinic/commit/063b98eec6cf7e0e3c27c250850435f05d1139f2))
* **treatment-details:** add grand total cost ([943288c](https://gitlab.com/beautyworld_repository/clinic/commit/943288c4860b7360dfd9cb6edd2ac0eab3621ab9))

## [3.1.0](https://gitlab.com/beautyworld_repository/clinic/compare/v3.0.9...v3.1.0) (2024-05-06)


### Features

* **turn-over-report:** fix invoice by module and payment ([ece040f](https://gitlab.com/beautyworld_repository/clinic/commit/ece040f37ba900f2268d6b1375357fda8e8c4bdd))


### Bug Fixes

* **application:** fix delete button using swal alert ([49f4beb](https://gitlab.com/beautyworld_repository/clinic/commit/49f4beb70f37991d376d65ec9e773727342f934c))
* **application:** fix delete button using swal alert and fix turn over crud ([e5fbe13](https://gitlab.com/beautyworld_repository/clinic/commit/e5fbe13c32120b390068d40b2467b8f83e7dccb5))
* **appointment:** fix replace appointment number ([569ca7b](https://gitlab.com/beautyworld_repository/clinic/commit/569ca7b36bde7469344c559c129a5181e9fa001a))
* **user-form:** fix edit button when role is not defined ([8b594ff](https://gitlab.com/beautyworld_repository/clinic/commit/8b594ff67ed93180636506a9e6ca727a10f87cb2))
* **vendor-detail:** fix unable load vendor detail ([94f0540](https://gitlab.com/beautyworld_repository/clinic/commit/94f054009670fdc1d6577f954df7cab7319dbc7e))

### [3.0.9](https://gitlab.com/beautyworld_repository/clinic/compare/v3.0.8...v3.0.9) (2024-05-03)


### Bug Fixes

* **appointment:** fix double appointment when edit data ([6d6b019](https://gitlab.com/beautyworld_repository/clinic/commit/6d6b019388ec079615a63cb64b8f8c7460461e36))
* **layouat:** fix sweet alert 2 ([a6acaf7](https://gitlab.com/beautyworld_repository/clinic/commit/a6acaf74088cfad1057c83ce528edcb4f1c8feaf))

### [3.0.8](https://gitlab.com/beautyworld_repository/clinic/compare/v3.0.7...v3.0.8) (2024-05-02)


### Bug Fixes

* **assets:** import swal via npm ([6794428](https://gitlab.com/beautyworld_repository/clinic/commit/6794428f272ff4617c944cd6d26261e954fd3a81))
* **footer:** add footer and show the last version ([458971b](https://gitlab.com/beautyworld_repository/clinic/commit/458971bebd4bb5e09122e20b0302552a38cd1a02))
* **footer:** add footer and show the last version ([3138747](https://gitlab.com/beautyworld_repository/clinic/commit/313874781a7a94d94e95ccd3460c76d849b6fc80))
* **footer:** add footer and show the last version ([c746a90](https://gitlab.com/beautyworld_repository/clinic/commit/c746a90cc2cdb8f32bd7127efdf3ab59fc51a854))
* **footer:** add footer and show the last version ([b5237c4](https://gitlab.com/beautyworld_repository/clinic/commit/b5237c428c62aab4b93047557b05a162dc05bf23))
* **footer:** add footer and show the last version ([ca79d25](https://gitlab.com/beautyworld_repository/clinic/commit/ca79d255bad346752f346fb5ad9d54b422d0552f))
* **footer:** add footer and show the last version ([63c99ee](https://gitlab.com/beautyworld_repository/clinic/commit/63c99eeb65cbc5ceddd18d8bacfa16236b26f9dd))

### [3.0.7](https://gitlab.com/beautyworld_repository/clinic/compare/v3.0.6...v3.0.7) (2024-05-02)


### Bug Fixes

* **footer:** add footer and show the last version ([ac315a6](https://gitlab.com/beautyworld_repository/clinic/commit/ac315a62e45ee2b6e68a1e04dc75177f30b16660))
* **marketplace:** fix typo marketplace table ([875d47e](https://gitlab.com/beautyworld_repository/clinic/commit/875d47e7f8aabf92272a2e0a65120671a3320b8f))
* **marketplace:** set Price to float value ([0dd5878](https://gitlab.com/beautyworld_repository/clinic/commit/0dd58788203de8ab458520d4ce8a5a1ba882257d))
* **product-form:** fix load cost on edit state ([9815a52](https://gitlab.com/beautyworld_repository/clinic/commit/9815a524ffe90994ed31510197c304cc12d7c004))
* **subscription-plan-table:** rename route with camel case ([35dc40c](https://gitlab.com/beautyworld_repository/clinic/commit/35dc40c8c511c3a1f0823d5f1837e4c7846c301b))
* **treatment:** inject agent price ([9bacb85](https://gitlab.com/beautyworld_repository/clinic/commit/9bacb856d5e5a4ce5911a5586c8541a9f0218d8a))
* **unit-of-measure:** fix bug on update function ([864d0c4](https://gitlab.com/beautyworld_repository/clinic/commit/864d0c4bd093c50936d526b3b5de9768917ca240))

### [3.0.6](https://gitlab.com/beautyworld_repository/clinic/compare/v3.0.5...v3.0.6) (2024-04-27)


### Bug Fixes

* **data-redemption:** add sell price and usage price ([55106cf](https://gitlab.com/beautyworld_repository/clinic/commit/55106cf18b3d5d70917fd9e563f09ff67af25f67))
* **data-redemption:** add sell price and usage price ([080d611](https://gitlab.com/beautyworld_repository/clinic/commit/080d611d723745f92d41806c1e6d39d9a477ed41))
* **data-redemption:** add sell price and usage price ([38fefe4](https://gitlab.com/beautyworld_repository/clinic/commit/38fefe47efab8540fcb2b8e6e10399010f0d576a))
* **data-redemption:** fix not show package ([7e68f55](https://gitlab.com/beautyworld_repository/clinic/commit/7e68f55c3fa9beb548ff9714bf15c2f3fbd23516))
* **product:** remove cogs field ([a24f3b7](https://gitlab.com/beautyworld_repository/clinic/commit/a24f3b7a377110e42354fe64359b2691cefc3490))
* **product:** remove cogs field ([82682cf](https://gitlab.com/beautyworld_repository/clinic/commit/82682cf533ae0e3e8da70f63c298f5431710dd89))
* **product:** remove cogs field ([b6578d5](https://gitlab.com/beautyworld_repository/clinic/commit/b6578d54578ea3497d4740e68f4e8d3c7422266b))

### [3.0.5](https://gitlab.com/beautyworld_repository/clinic/compare/v3.0.4...v3.0.5) (2024-04-25)


### Bug Fixes

* **package:** remove category and sub category ([b65c84d](https://gitlab.com/beautyworld_repository/clinic/commit/b65c84de36a886d786230d3f5b9f1c03b78b5214))

### [3.0.4](https://gitlab.com/beautyworld_repository/clinic/compare/v3.0.3...v3.0.4) (2024-04-25)


### Features

* **treatment:** add all you can treatment ([f5efe2c](https://gitlab.com/beautyworld_repository/clinic/commit/f5efe2cd6ab5c27932e18b5afa6bb9bf3bae962a))
* **treatment:** add all you can treatment ([a7e5e48](https://gitlab.com/beautyworld_repository/clinic/commit/a7e5e48a57c7a3822b44f4a92db8fc0fe6130944))

### [3.0.3](https://gitlab.com/beautyworld_repository/clinic/compare/v2.3.2...v3.0.3) (2024-04-22)


### ⚠ BREAKING CHANGES

* **treatment:** all the module with services
* **subscriptions:** sales invoice
* **subscriptions:** sales invoice
* **package-form:** - sales invoice
* **treatment-fee-form:** - Sales Invoice
      - Package
      - Appointment
* **treatment-form:** - Sales Invoice
    - Package
    - Appointment
* **treatment-feature:** - Sales Invoice
        - Package
        - Appointment
* **service-item:** - Sales Invoice
      - Package
      - Appointment
* **treatment-feature:** - Sales Invoice
    - Package
    - Appointment
* **services-feature:** - Sales Invoice
  - Package
  - Appointment

### Features

* **commission-report-page:** add new layout for commision report page ([b03c115](https://gitlab.com/beautyworld_repository/clinic/commit/b03c1155b14a0637c671c30fc6e54f75b696a82d))
* **commission-report-page:** add new layout for commision report page ([09ec1f2](https://gitlab.com/beautyworld_repository/clinic/commit/09ec1f28f5341b5fee2bb6f0b1278b6a3e40767d))
* **commission-report-page:** add new layout for commision report page ([9c4b77b](https://gitlab.com/beautyworld_repository/clinic/commit/9c4b77b93e5fa0e3209d3ed33234614df0dc5fe8))
* **commission-report-page:** add new layout for commision report page ([ff39adc](https://gitlab.com/beautyworld_repository/clinic/commit/ff39adc407aedbb7633934a0d8e545ceecdd56bf))
* **commission-report-page:** add new layout for commision report page ([94fb9e6](https://gitlab.com/beautyworld_repository/clinic/commit/94fb9e6958e0e513f2c32143568a749121f3eafd))
* **commission-report-page:** add new layout for commision report page ([0427136](https://gitlab.com/beautyworld_repository/clinic/commit/042713696d80516bf0d0e00db29beb7aa9dc91f1))
* **commission-report-page:** add new layout for commision report page ([ea82b45](https://gitlab.com/beautyworld_repository/clinic/commit/ea82b459e1c00600e8af468f0c198f977a0f6582))
* **commission-report-page:** add new layout for commision report page ([ddccb4e](https://gitlab.com/beautyworld_repository/clinic/commit/ddccb4e342514f4a4f328056877287784c24b2cd))
* **marketplace:** initiate marketplace fee and material usage page ([c3ef699](https://gitlab.com/beautyworld_repository/clinic/commit/c3ef6998fd69414c2d6c3f69ee822192b37384aa))
* **product-fee:** add product fee page ([803acf5](https://gitlab.com/beautyworld_repository/clinic/commit/803acf5665774bf788d6cafae3ed7c95bfd294c9))
* **sales-invoice-api:** add sales invoice api ([80efd1c](https://gitlab.com/beautyworld_repository/clinic/commit/80efd1c728a0bbaae3e3460fe6a1dc52d3233273))
* **sales-invoice-api:** add sales invoice api ([85ca1de](https://gitlab.com/beautyworld_repository/clinic/commit/85ca1dea3580655675e56b22617ba572e43d186c))
* **subscription:** add seeder for subscription transaction ([c9079c5](https://gitlab.com/beautyworld_repository/clinic/commit/c9079c51e78ae4cb51821a946c677591ce0d118d))
* **subscription:** add seeder for subscription transaction ([139286c](https://gitlab.com/beautyworld_repository/clinic/commit/139286c378b8f9cd5bfa21d44d65f4a058d54fbd))
* **subscription:** add subscription seeder ([eff4ffd](https://gitlab.com/beautyworld_repository/clinic/commit/eff4ffdab18508f7391956c09e7dae4e8917029f))
* **subscription:** initiate subscriptions transaction ([82c9bef](https://gitlab.com/beautyworld_repository/clinic/commit/82c9bef137be0519fe6e67ec3df382d5593926ce))
* **subscriptions:** add new feature call subscriptions ([9f20a7a](https://gitlab.com/beautyworld_repository/clinic/commit/9f20a7a6e6712b3304126d17713056d9279a99ff))
* **subscriptions:** add new subscriptions on sales invoice form ([3a04c15](https://gitlab.com/beautyworld_repository/clinic/commit/3a04c1588a64bd350e9d9b5121af8aa6d081a285))
* **treatment:** add treatment fee and treatment material usages ([c30b8b1](https://gitlab.com/beautyworld_repository/clinic/commit/c30b8b1df502ac146045ecfd003b59b8179cbe04))
* **turn over report:** update turn over report ([d6c2aad](https://gitlab.com/beautyworld_repository/clinic/commit/d6c2aad1a365bef666202bcf2b96373ee47a590f))
* **turn-over:** add master turn over ([b8a771a](https://gitlab.com/beautyworld_repository/clinic/commit/b8a771af0b5a3036c2fd4ff469c5f634b33b5d7b))
* **turn-over:** add master turn over ([1cf8bad](https://gitlab.com/beautyworld_repository/clinic/commit/1cf8bad5bd5626f34ac72ce8c5718dab092101c8))


### Bug Fixes

* **application:** change service to treatment word ([5a3172a](https://gitlab.com/beautyworld_repository/clinic/commit/5a3172aefc21b3e6aaee7fed43c9b810a29e4e37))
* **application:** change service to treatment word ([90c8288](https://gitlab.com/beautyworld_repository/clinic/commit/90c828833808dff54a086e957e9641d79ffc3d99))
* **appointment-form:** fix appointment form edit not save PIC ([a46d30c](https://gitlab.com/beautyworld_repository/clinic/commit/a46d30c551c40472bd41361ffb17f629698cfefc))
* **appointment-form:** fix appointment form edit not save PIC ([013c66a](https://gitlab.com/beautyworld_repository/clinic/commit/013c66a1da07cdbcafbfff5a78275746354af275))
* **appointment-form:** use livewire customer-search on appointment-form ([3a67739](https://gitlab.com/beautyworld_repository/clinic/commit/3a67739414fa259323680647bc7b599fbdff1d04))
* **appointment-redemption:** fix bug redemption ([f1d7229](https://gitlab.com/beautyworld_repository/clinic/commit/f1d72292a5c57ac76efc345fc09ce122a08a1c04))
* **appointment-redemption:** fix bug redemption ([f810154](https://gitlab.com/beautyworld_repository/clinic/commit/f810154e73bf7301f8a01c657f4c2723ae1cf3f1))
* **appointment:** fix bug add appointment ([e91da23](https://gitlab.com/beautyworld_repository/clinic/commit/e91da230686e17d36cadfd1091cfce5f13cb8d8b))
* **appointment:** fix bug delete appointment ([4eac02e](https://gitlab.com/beautyworld_repository/clinic/commit/4eac02efa54603e7a5f5dd984f339d09716088f3))
* **appointment:** fix empty data redemption and appointment flow ([0089fd2](https://gitlab.com/beautyworld_repository/clinic/commit/0089fd20d3a1bd659a3e7eb70c28119dabc54074))
* **appointment:** fix failed load appointment item modal ([b32971b](https://gitlab.com/beautyworld_repository/clinic/commit/b32971bd30187b4e15b1d0e6f5a3a52809a56325))
* **appointment:** fix failed writing sales invoice number on appointment table ([d1c5cc8](https://gitlab.com/beautyworld_repository/clinic/commit/d1c5cc82375921ff8d84715a01be9dc2ca41e086))
* **category-subcategory:** fix category and subcategory seeder ([ec85353](https://gitlab.com/beautyworld_repository/clinic/commit/ec8535396a6f341c7019bde35b3a01a87db61ade))
* **category:** fix autoload category on treatment form ([e74b104](https://gitlab.com/beautyworld_repository/clinic/commit/e74b104c484f26b45b7a2952581d5f2a377879e0))
* **category:** fix autoload category on treatment form ([26f1eec](https://gitlab.com/beautyworld_repository/clinic/commit/26f1eec7bfff789ce114fc930d18ae711d78f76f))
* **category:** for now bypass category option ([5ce09b0](https://gitlab.com/beautyworld_repository/clinic/commit/5ce09b0f049edb060d982204c346dd2b51c1c00f))
* **category:** for now bypass category option ([e2eb916](https://gitlab.com/beautyworld_repository/clinic/commit/e2eb916a4fb7632ce1ce5aa34c8daddedd22ed2e))
* **changelog:** update change log ([f0172e8](https://gitlab.com/beautyworld_repository/clinic/commit/f0172e8edac44c0ba091d3979695cc89ee112f16))
* **changelog:** update change log ([f772085](https://gitlab.com/beautyworld_repository/clinic/commit/f772085ac64d0d7b613f4d8bae9bec4636aa125c))
* **changelog:** update change log ([3138cd1](https://gitlab.com/beautyworld_repository/clinic/commit/3138cd187ce2fd3b39adce0d9ed6f7e9178f7b7b))
* **command:** add import product ([6d7daa8](https://gitlab.com/beautyworld_repository/clinic/commit/6d7daa81d2c7c934accde2b9bde7da9e8ee19fce))
* **command:** add import product ([98a313d](https://gitlab.com/beautyworld_repository/clinic/commit/98a313d23d5f5ff2b346fb9da7608938e5d59570))
* **command:** add import product ([d7e12e2](https://gitlab.com/beautyworld_repository/clinic/commit/d7e12e2d1d354fd7c9f4a99f9e582f98e13aad30))
* **command:** add import product ([3b1f298](https://gitlab.com/beautyworld_repository/clinic/commit/3b1f2985dc725730bd8b40417c99403db34c4590))
* **customer-form:** fix error save customer ([1766939](https://gitlab.com/beautyworld_repository/clinic/commit/17669395c0da71323fb5550fadaa4ed0b4b0d428))
* **customer-form:** fix error save customer ([e66ce97](https://gitlab.com/beautyworld_repository/clinic/commit/e66ce972230c6a94f6d5909f8c31bfbd0b0df4c4))
* **customer-form:** fix error save customer ([1eb3b8a](https://gitlab.com/beautyworld_repository/clinic/commit/1eb3b8a77586b0e015ae3d80fa3f473394a9fea6))
* **data-redemption:** fix error redemption left ([83f853b](https://gitlab.com/beautyworld_repository/clinic/commit/83f853bc97c8bf1bb69acdec03e8440ad721de1d))
* **data-redemption:** fix error redemption left ([fd0f3a5](https://gitlab.com/beautyworld_repository/clinic/commit/fd0f3a5275c7f67fa911223e04c7187ca80c29ae))
* **helpers:** remove unnecessary role ([be6e14a](https://gitlab.com/beautyworld_repository/clinic/commit/be6e14a12b2d7656f1ee5f9df21241f7f77711b0))
* **marketplace-fee:** fix marketplace fee page ([5fae42b](https://gitlab.com/beautyworld_repository/clinic/commit/5fae42b28bc8eacf26db6fab8c02c001bd361798))
* **marketplace-fee:** fix marketplace fee page ([057d058](https://gitlab.com/beautyworld_repository/clinic/commit/057d0582be3f73b137008a8a6a797e8320a62ea1))
* **marketplace-treatment:** fix delete button on marketplace and treatment ([95228c1](https://gitlab.com/beautyworld_repository/clinic/commit/95228c1ffe4a5b4b79ccad2e0500e04d3c62e337))
* **marketplace-treatment:** fix error fee after change fee method on model ([26cbb99](https://gitlab.com/beautyworld_repository/clinic/commit/26cbb99a99ee012b15bbb8a7acadf61af644f626))
* **marketplace-treatment:** fix error treatment and marketplace fee ([4235cab](https://gitlab.com/beautyworld_repository/clinic/commit/4235cab381f9114964c26fb5ff7c0774503616f4))
* **marketplace-treatment:** fix treatment and marketplace fee after change the method name ([7414999](https://gitlab.com/beautyworld_repository/clinic/commit/74149991cc77b2a7a57c6cff84954d379a83231f))
* **marketplace-treatment:** fix treatment and marketplace fee after change the method name ([d254362](https://gitlab.com/beautyworld_repository/clinic/commit/d2543623ea2dde3e99513dd0d0173a5e34486580))
* **marketplace:** update marketplace follow the excel ([98b7748](https://gitlab.com/beautyworld_repository/clinic/commit/98b774868017c9845e69358b7a8aa1505ff5a324))
* **marketplace:** update marketplace follow the excel ([9573e45](https://gitlab.com/beautyworld_repository/clinic/commit/9573e45100006c58f11bafd745efee613bedcde8))
* **package-details:** fix table row package data ([53f5170](https://gitlab.com/beautyworld_repository/clinic/commit/53f5170a2bead2fbca753dc1f2bc34fa1bdbd727))
* **package-fee:** fix package fee store ([c6c1242](https://gitlab.com/beautyworld_repository/clinic/commit/c6c124263221077e12ddb4558497aabb64e66941))
* **package-form:** fix autoload package form ([44e0343](https://gitlab.com/beautyworld_repository/clinic/commit/44e03431403198f1e582a1eacccca335330246a5))
* **package-form:** fix crud package ([17fa968](https://gitlab.com/beautyworld_repository/clinic/commit/17fa9680a66e5cda7474f00ae0d72a635d950c9a))
* **package-form:** fix crud package ([e6a715f](https://gitlab.com/beautyworld_repository/clinic/commit/e6a715f9c30db549d094c642d02c685c9d73d7d0))
* **package-item:** fix but item qty ([5a9ac3c](https://gitlab.com/beautyworld_repository/clinic/commit/5a9ac3c34f5fbaa57e4dfa9419d82c135e27c4fb))
* **package-item:** fix but item qty ([ecaadc5](https://gitlab.com/beautyworld_repository/clinic/commit/ecaadc5e32eae6507a441295ecdf6e041ca0c0df))
* **product and treatment:** fix category option allow null value ([01b0f2e](https://gitlab.com/beautyworld_repository/clinic/commit/01b0f2ecfa658978ee1861bbcdfb924e47e9bfbb))
* **product-detail:** fix layout fee ([d317cda](https://gitlab.com/beautyworld_repository/clinic/commit/d317cda681cf9b40a1a16e6d4af4479beec55a14))
* **product-detail:** fix layout fee ([5acb843](https://gitlab.com/beautyworld_repository/clinic/commit/5acb843c5d4f871d648efa77c38027752d94163a))
* **product-fee-page:** fix agent price and normal price ([ea36e2d](https://gitlab.com/beautyworld_repository/clinic/commit/ea36e2d587c7a4dbc0301b58bf58263b43ea0b1e))
* **product-fee-page:** set fee on price format ([498fac6](https://gitlab.com/beautyworld_repository/clinic/commit/498fac64aa55d7271fc037589296e7a33dcbaee8))
* **product-fee-page:** set fee on price format ([2c4e865](https://gitlab.com/beautyworld_repository/clinic/commit/2c4e8657ef6cba1a2392edaa0c06e038cf8cca2f))
* **product-treatment:** fix category option allow null value ([17256ba](https://gitlab.com/beautyworld_repository/clinic/commit/17256ba71a83f8362901b58fef8729a1298a9895))
* **product-treatment:** fix category option allow null value ([7767729](https://gitlab.com/beautyworld_repository/clinic/commit/7767729a7f1403e60f9b215ce0d5ee5efeec4d35))
* **product:** refactor product ([2e76885](https://gitlab.com/beautyworld_repository/clinic/commit/2e768854a9be64593339344ae5481ac8993ec033))
* **receipt:** fix wrong date on receipt ([7fb95b3](https://gitlab.com/beautyworld_repository/clinic/commit/7fb95b34dd2016c1ea533074a3149beb84a2efb9))
* **receipt:** fix wrong date on receipt ([a26474c](https://gitlab.com/beautyworld_repository/clinic/commit/a26474c2de00eeb0954b4507510d42af50c0dbfc))
* **receipt:** remove tax ([e8d1868](https://gitlab.com/beautyworld_repository/clinic/commit/e8d1868d76463621a141f6c66942b68572c94190))
* **redemption-search:** change service to treatment word ([3f435de](https://gitlab.com/beautyworld_repository/clinic/commit/3f435de5c07c4d3aa09fe92e560cad9969d16ac0))
* **sales-detail-report:** fix not counted product price ([407eb4b](https://gitlab.com/beautyworld_repository/clinic/commit/407eb4b5d0790538e41a2bcedf8024edb917b637))
* **sales-detail-report:** fix not counted product price ([97b1aac](https://gitlab.com/beautyworld_repository/clinic/commit/97b1aac68b5169d3602865ed9118f002185f90c5))
* **sales-detail-report:** fix wrong column ([0173ce6](https://gitlab.com/beautyworld_repository/clinic/commit/0173ce6de33fecfdbf734a59f059672a2cd08505))
* **sales-invoice-form:** hide subscriptions tabs ([bc88fb0](https://gitlab.com/beautyworld_repository/clinic/commit/bc88fb07997eb375d65211dff745ab05ef75e17c))
* **sales-invoice-table:** add print button on sales invoice table ([be9b894](https://gitlab.com/beautyworld_repository/clinic/commit/be9b894a778258b18b41b19e41ad72908d967830))
* **sales-invoice:** fix error sales invoice page ([8935efb](https://gitlab.com/beautyworld_repository/clinic/commit/8935efb8e0179706d45f977d5ac3972866efcf9a))
* **sales-invoice:** fix error sales invoice page ([60fbe6f](https://gitlab.com/beautyworld_repository/clinic/commit/60fbe6fca9f48b1c7ae49cc3f0f5d8934b23c07b))
* **sales-invoice:** fix error sales invoice page ([a78f46a](https://gitlab.com/beautyworld_repository/clinic/commit/a78f46a0bab800b3df106d3a1e7e55d3bface402))
* **sales-invoice:** fix sales invoice approval ([2e1dc31](https://gitlab.com/beautyworld_repository/clinic/commit/2e1dc3182d0fbca2dcf0b082eb58ede4b50a79b3))
* **sales-invoice:** remove tax ([e6b5098](https://gitlab.com/beautyworld_repository/clinic/commit/e6b5098a8240d9901261444f60b51a2c9cf2e7a6))
* **seeder:** fix category seeder and service seeder ([c78969b](https://gitlab.com/beautyworld_repository/clinic/commit/c78969b1a8a41e55ae20b57fa18352c3b0bf0cb1))
* **subscription-plan-item:** fix crud ([5208cf3](https://gitlab.com/beautyworld_repository/clinic/commit/5208cf329363876dc2cce837d3b622fbae4d9a2b))
* **subscription-plan:** fix hide subscription form on crud ([ac2dd4c](https://gitlab.com/beautyworld_repository/clinic/commit/ac2dd4cc86c30ca5a2d375a89d694cea879fa72e))
* **treatment-detail:** set treatment fee into price format ([17191c1](https://gitlab.com/beautyworld_repository/clinic/commit/17191c1a3980a8d973f66d0228625c7189026a60))
* **treatment-feature:** fix insert treatment fee ([e532d8a](https://gitlab.com/beautyworld_repository/clinic/commit/e532d8aa3c6a6b5d94e2c70cf0233d83858119b7))
* **treatment-feature:** show  swal alert when admin try to delete treatment ([9340b92](https://gitlab.com/beautyworld_repository/clinic/commit/9340b9291a5eb43f188bcc176cf077db25d10908))
* **treatment-fee-page:** fix layout fee treatment ([877e3f6](https://gitlab.com/beautyworld_repository/clinic/commit/877e3f6e939a4d8d220c3df7960a2eb9193d4e15))
* **treatment-fee-page:** fix layout fee treatment ([81c10b3](https://gitlab.com/beautyworld_repository/clinic/commit/81c10b35d9f9b25172ed5a8fde19ac5e0819abfa))
* **treatment-fee-page:** layout treatment fee header ([0aa62cb](https://gitlab.com/beautyworld_repository/clinic/commit/0aa62cb095fc55202ed7cbb1f6a4798ca91e7d96))
* **treatment-fee-page:** store treatment fee data ([809f998](https://gitlab.com/beautyworld_repository/clinic/commit/809f998202a187edb04c9e71c68c412e514d6e0d))
* **treatment-fee:** fix auto load fee ([e298d99](https://gitlab.com/beautyworld_repository/clinic/commit/e298d99350ead2999b5a0e67777dfcb7ce983d2a))
* **treatment-fee:** fix auto load fee ([9f53c65](https://gitlab.com/beautyworld_repository/clinic/commit/9f53c651e485cab74593f5a1055fbc7f0dd5c7e8))
* **treatment-fee:** fix auto load fee ([1f549f8](https://gitlab.com/beautyworld_repository/clinic/commit/1f549f8ace878d1c56c9c93242e21579def5dc20))
* **treatment-fee:** fix autoload treatment fee detail ([e738140](https://gitlab.com/beautyworld_repository/clinic/commit/e738140cb7fef45b2eb742d4375cbb2ad17b3f66))
* **treatment-form:** adjust treatment fee form display using tabs ([306a641](https://gitlab.com/beautyworld_repository/clinic/commit/306a641b81c2e2925efccdb4c02570c0decc6fc4))
* **treatment-form:** fix store data ([4bf0b50](https://gitlab.com/beautyworld_repository/clinic/commit/4bf0b5039fa70e9a3b634376abdda0e3f7e6e59d))
* **treatment-form:** fix update data on edit state ([32d06c2](https://gitlab.com/beautyworld_repository/clinic/commit/32d06c2b6ae8cafc9e378d6c42b2a6444ec45495))
* **treatment-form:** fix update data on edit state ([1d17f7f](https://gitlab.com/beautyworld_repository/clinic/commit/1d17f7fcdaf282cd37773636f2a296b3c4e41ff6))
* **treatment-form:** set fee value ([ffe0b1b](https://gitlab.com/beautyworld_repository/clinic/commit/ffe0b1bc50ec60d927d91407279028a4711c9096))
* **treatment-material-usage:** fix autoload treatment material usage detail ([6b80056](https://gitlab.com/beautyworld_repository/clinic/commit/6b800565a01d7de4367ed673199b05a148fdde8c))
* **treatment-material-usage:** fix search product ([669d515](https://gitlab.com/beautyworld_repository/clinic/commit/669d515db5d0821cbcf5481f0239ac2cb5d0542c))
* **treatment-material-usage:** fix treatment material usages table ([1ddfb17](https://gitlab.com/beautyworld_repository/clinic/commit/1ddfb17dcd055b4b085c0c5dc52d9f8d600fb545))
* **treatment-material-usage:** remove qty from treatment material usage ([c2de0f1](https://gitlab.com/beautyworld_repository/clinic/commit/c2de0f1004e0befeb7e40f01cb75a0a7155c78fb))
* **treatment-material-usage:** set treatment price into price format ([cc5113e](https://gitlab.com/beautyworld_repository/clinic/commit/cc5113ee4c7d8bc1a817345549f09cfbac8d1cf6))
* **treatment-material-usage:** show treatment by id ([f1ece6f](https://gitlab.com/beautyworld_repository/clinic/commit/f1ece6f1a851b92e91ae7ddef38b781e14b55cba))
* **treatment:** adjust treatment table field ([50e1e8e](https://gitlab.com/beautyworld_repository/clinic/commit/50e1e8e3fc7da9fdc399fe5057c995964477be57))
* **treatment:** fix add treatment material usage ([31bfeb5](https://gitlab.com/beautyworld_repository/clinic/commit/31bfeb545b552b016d4455be54c126cfe2141158))
* **treatment:** fix autoload category on market place ([a51258e](https://gitlab.com/beautyworld_repository/clinic/commit/a51258ee0b34c4eb3eaad57ec58de8dd64d1d891))
* **treatment:** fix not showing price on product search ([418128f](https://gitlab.com/beautyworld_repository/clinic/commit/418128f1d46b5a22cf5a84372c6fbc491f2f3ad8))
* **treatment:** fix redirect to master treatments after save data ([6f973f6](https://gitlab.com/beautyworld_repository/clinic/commit/6f973f6bf13c6e617466317ee30062402a914b13))
* **treatment:** fix treatment seeder ([5677960](https://gitlab.com/beautyworld_repository/clinic/commit/5677960ae2b2936576125e8dbea97f67d42cd2df))
* **turn-over:** fix delete function ([0b5b7e7](https://gitlab.com/beautyworld_repository/clinic/commit/0b5b7e7c22cc5d967ee9a08779b85fbd35ae2b23))
* **turn-over:** fix delete function ([af7477d](https://gitlab.com/beautyworld_repository/clinic/commit/af7477de81e9e11e04280f3e97006dd8bcc2333d))
* **user-detail:** set unique code on user detail ([562e6d2](https://gitlab.com/beautyworld_repository/clinic/commit/562e6d206b5635fe9725fd810901e5dc9af2f9d0))
* **user-seeder:** add melinda and bu selvi to user seeder ([5d3a704](https://gitlab.com/beautyworld_repository/clinic/commit/5d3a704bf8f43d41d3568978aea4be144390f287))
* **user-seeder:** add melinda and bu selvi to user seeder ([d391608](https://gitlab.com/beautyworld_repository/clinic/commit/d391608b391d3f240c1c13e4d16e3fa284ce8ea0))


* **package-form:** update package-form ([6feb4a3](https://gitlab.com/beautyworld_repository/clinic/commit/6feb4a3c38cd6eef42fef802b2aaa029487f789c))
* **service-item:** rename to treatment_material_usages ([a57fcfa](https://gitlab.com/beautyworld_repository/clinic/commit/a57fcfab240fa692b8cf7c2aa426c89e5c6b7f84))
* **services-feature:** change service to treatment ([3e9ae2b](https://gitlab.com/beautyworld_repository/clinic/commit/3e9ae2b32424a60d70291d4dc0946fce32bd1448))
* **treatment-feature:** refactor ([9d13d56](https://gitlab.com/beautyworld_repository/clinic/commit/9d13d56bef1e5af7218325057b64e19e393c9831))
* **treatment-fee-form:** clear fee value ([04bd04b](https://gitlab.com/beautyworld_repository/clinic/commit/04bd04b9984e43e62d0428b10ab953d6fbf024e6))
* **treatment-form:** use compoenent for material usage, add sample event for Rp agains Percents ([7f5a6e4](https://gitlab.com/beautyworld_repository/clinic/commit/7f5a6e41a496f6a250e997ebafff3bedd62b2c84))
* **treatment:** refactor treatment ([c42a61f](https://gitlab.com/beautyworld_repository/clinic/commit/c42a61fabf01e0daf022e2ec7ebed69571e86b81))

### [3.0.2](https://gitlab.com/beautyworld_repository/clinic/compare/v3.0.1...v3.0.2) (2024-04-18)


### Bug Fixes

* **appointment:** fix empty data redemption and appointment flow ([0089fd2](https://gitlab.com/beautyworld_repository/clinic/commit/0089fd20d3a1bd659a3e7eb70c28119dabc54074))
* **appointment:** fix failed load appointment item modal ([b32971b](https://gitlab.com/beautyworld_repository/clinic/commit/b32971bd30187b4e15b1d0e6f5a3a52809a56325))
* **appointment:** fix failed writing sales invoice number on appointment table ([d1c5cc8](https://gitlab.com/beautyworld_repository/clinic/commit/d1c5cc82375921ff8d84715a01be9dc2ca41e086))
* **sales-invoice-form:** hide subscriptions tabs ([bc88fb0](https://gitlab.com/beautyworld_repository/clinic/commit/bc88fb07997eb375d65211dff745ab05ef75e17c))

### [3.0.1](https://gitlab.com/beautyworld_repository/clinic/compare/v3.0.0...v3.0.1) (2024-04-17)


### Bug Fixes

* **application:** change service to treatment word ([5a3172a](https://gitlab.com/beautyworld_repository/clinic/commit/5a3172aefc21b3e6aaee7fed43c9b810a29e4e37))
* **application:** change service to treatment word ([90c8288](https://gitlab.com/beautyworld_repository/clinic/commit/90c828833808dff54a086e957e9641d79ffc3d99))
* **category:** fix autoload category on treatment form ([e74b104](https://gitlab.com/beautyworld_repository/clinic/commit/e74b104c484f26b45b7a2952581d5f2a377879e0))
* **category:** fix autoload category on treatment form ([26f1eec](https://gitlab.com/beautyworld_repository/clinic/commit/26f1eec7bfff789ce114fc930d18ae711d78f76f))
* **category:** for now bypass category option ([5ce09b0](https://gitlab.com/beautyworld_repository/clinic/commit/5ce09b0f049edb060d982204c346dd2b51c1c00f))
* **category:** for now bypass category option ([e2eb916](https://gitlab.com/beautyworld_repository/clinic/commit/e2eb916a4fb7632ce1ce5aa34c8daddedd22ed2e))
* **product-fee-page:** fix agent price and normal price ([ea36e2d](https://gitlab.com/beautyworld_repository/clinic/commit/ea36e2d587c7a4dbc0301b58bf58263b43ea0b1e))
* **product-fee-page:** set fee on price format ([498fac6](https://gitlab.com/beautyworld_repository/clinic/commit/498fac64aa55d7271fc037589296e7a33dcbaee8))
* **product-fee-page:** set fee on price format ([2c4e865](https://gitlab.com/beautyworld_repository/clinic/commit/2c4e8657ef6cba1a2392edaa0c06e038cf8cca2f))
* **treatment-detail:** set treatment fee into price format ([17191c1](https://gitlab.com/beautyworld_repository/clinic/commit/17191c1a3980a8d973f66d0228625c7189026a60))
* **treatment-material-usage:** remove qty from treatment material usage ([c2de0f1](https://gitlab.com/beautyworld_repository/clinic/commit/c2de0f1004e0befeb7e40f01cb75a0a7155c78fb))
* **treatment-material-usage:** set treatment price into price format ([cc5113e](https://gitlab.com/beautyworld_repository/clinic/commit/cc5113ee4c7d8bc1a817345549f09cfbac8d1cf6))
* **treatment:** fix autoload category on market place ([a51258e](https://gitlab.com/beautyworld_repository/clinic/commit/a51258ee0b34c4eb3eaad57ec58de8dd64d1d891))

## [3.0.0](https://gitlab.com/beautyworld_repository/clinic/compare/v2.3.2...v3.0.0) (2024-04-17)


### ⚠ BREAKING CHANGES

* **treatment:** all the module with services
* **subscriptions:** sales invoice
* **subscriptions:** sales invoice
* **package-form:** - sales invoice
* **treatment-fee-form:** - Sales Invoice
      - Package
      - Appointment
* **treatment-form:** - Sales Invoice
    - Package
    - Appointment
* **treatment-feature:** - Sales Invoice
        - Package
        - Appointment
* **service-item:** - Sales Invoice
      - Package
      - Appointment
* **treatment-feature:** - Sales Invoice
    - Package
    - Appointment
* **services-feature:** - Sales Invoice
  - Package
  - Appointment

### Features

* **commission-report-page:** add new layout for commision report page ([b03c115](https://gitlab.com/beautyworld_repository/clinic/commit/b03c1155b14a0637c671c30fc6e54f75b696a82d))
* **commission-report-page:** add new layout for commision report page ([09ec1f2](https://gitlab.com/beautyworld_repository/clinic/commit/09ec1f28f5341b5fee2bb6f0b1278b6a3e40767d))
* **commission-report-page:** add new layout for commision report page ([9c4b77b](https://gitlab.com/beautyworld_repository/clinic/commit/9c4b77b93e5fa0e3209d3ed33234614df0dc5fe8))
* **commission-report-page:** add new layout for commision report page ([ff39adc](https://gitlab.com/beautyworld_repository/clinic/commit/ff39adc407aedbb7633934a0d8e545ceecdd56bf))
* **commission-report-page:** add new layout for commision report page ([94fb9e6](https://gitlab.com/beautyworld_repository/clinic/commit/94fb9e6958e0e513f2c32143568a749121f3eafd))
* **commission-report-page:** add new layout for commision report page ([0427136](https://gitlab.com/beautyworld_repository/clinic/commit/042713696d80516bf0d0e00db29beb7aa9dc91f1))
* **commission-report-page:** add new layout for commision report page ([ea82b45](https://gitlab.com/beautyworld_repository/clinic/commit/ea82b459e1c00600e8af468f0c198f977a0f6582))
* **commission-report-page:** add new layout for commision report page ([ddccb4e](https://gitlab.com/beautyworld_repository/clinic/commit/ddccb4e342514f4a4f328056877287784c24b2cd))
* **marketplace:** initiate marketplace fee and material usage page ([c3ef699](https://gitlab.com/beautyworld_repository/clinic/commit/c3ef6998fd69414c2d6c3f69ee822192b37384aa))
* **product-fee:** add product fee page ([803acf5](https://gitlab.com/beautyworld_repository/clinic/commit/803acf5665774bf788d6cafae3ed7c95bfd294c9))
* **sales-invoice-api:** add sales invoice api ([80efd1c](https://gitlab.com/beautyworld_repository/clinic/commit/80efd1c728a0bbaae3e3460fe6a1dc52d3233273))
* **sales-invoice-api:** add sales invoice api ([85ca1de](https://gitlab.com/beautyworld_repository/clinic/commit/85ca1dea3580655675e56b22617ba572e43d186c))
* **subscription:** add seeder for subscription transaction ([c9079c5](https://gitlab.com/beautyworld_repository/clinic/commit/c9079c51e78ae4cb51821a946c677591ce0d118d))
* **subscription:** add seeder for subscription transaction ([139286c](https://gitlab.com/beautyworld_repository/clinic/commit/139286c378b8f9cd5bfa21d44d65f4a058d54fbd))
* **subscription:** add subscription seeder ([eff4ffd](https://gitlab.com/beautyworld_repository/clinic/commit/eff4ffdab18508f7391956c09e7dae4e8917029f))
* **subscription:** initiate subscriptions transaction ([82c9bef](https://gitlab.com/beautyworld_repository/clinic/commit/82c9bef137be0519fe6e67ec3df382d5593926ce))
* **subscriptions:** add new feature call subscriptions ([9f20a7a](https://gitlab.com/beautyworld_repository/clinic/commit/9f20a7a6e6712b3304126d17713056d9279a99ff))
* **subscriptions:** add new subscriptions on sales invoice form ([3a04c15](https://gitlab.com/beautyworld_repository/clinic/commit/3a04c1588a64bd350e9d9b5121af8aa6d081a285))
* **treatment:** add treatment fee and treatment material usages ([c30b8b1](https://gitlab.com/beautyworld_repository/clinic/commit/c30b8b1df502ac146045ecfd003b59b8179cbe04))
* **turn over report:** update turn over report ([d6c2aad](https://gitlab.com/beautyworld_repository/clinic/commit/d6c2aad1a365bef666202bcf2b96373ee47a590f))
* **turn-over:** add master turn over ([b8a771a](https://gitlab.com/beautyworld_repository/clinic/commit/b8a771af0b5a3036c2fd4ff469c5f634b33b5d7b))
* **turn-over:** add master turn over ([1cf8bad](https://gitlab.com/beautyworld_repository/clinic/commit/1cf8bad5bd5626f34ac72ce8c5718dab092101c8))


### Bug Fixes

* **appointment-form:** fix appointment form edit not save PIC ([a46d30c](https://gitlab.com/beautyworld_repository/clinic/commit/a46d30c551c40472bd41361ffb17f629698cfefc))
* **appointment-form:** fix appointment form edit not save PIC ([013c66a](https://gitlab.com/beautyworld_repository/clinic/commit/013c66a1da07cdbcafbfff5a78275746354af275))
* **appointment-form:** use livewire customer-search on appointment-form ([3a67739](https://gitlab.com/beautyworld_repository/clinic/commit/3a67739414fa259323680647bc7b599fbdff1d04))
* **category-subcategory:** fix category and subcategory seeder ([ec85353](https://gitlab.com/beautyworld_repository/clinic/commit/ec8535396a6f341c7019bde35b3a01a87db61ade))
* **changelog:** update change log ([f0172e8](https://gitlab.com/beautyworld_repository/clinic/commit/f0172e8edac44c0ba091d3979695cc89ee112f16))
* **changelog:** update change log ([f772085](https://gitlab.com/beautyworld_repository/clinic/commit/f772085ac64d0d7b613f4d8bae9bec4636aa125c))
* **changelog:** update change log ([3138cd1](https://gitlab.com/beautyworld_repository/clinic/commit/3138cd187ce2fd3b39adce0d9ed6f7e9178f7b7b))
* **customer-form:** fix error save customer ([1766939](https://gitlab.com/beautyworld_repository/clinic/commit/17669395c0da71323fb5550fadaa4ed0b4b0d428))
* **customer-form:** fix error save customer ([e66ce97](https://gitlab.com/beautyworld_repository/clinic/commit/e66ce972230c6a94f6d5909f8c31bfbd0b0df4c4))
* **customer-form:** fix error save customer ([1eb3b8a](https://gitlab.com/beautyworld_repository/clinic/commit/1eb3b8a77586b0e015ae3d80fa3f473394a9fea6))
* **data-redemption:** fix error redemption left ([83f853b](https://gitlab.com/beautyworld_repository/clinic/commit/83f853bc97c8bf1bb69acdec03e8440ad721de1d))
* **data-redemption:** fix error redemption left ([fd0f3a5](https://gitlab.com/beautyworld_repository/clinic/commit/fd0f3a5275c7f67fa911223e04c7187ca80c29ae))
* **helpers:** remove unnecessary role ([be6e14a](https://gitlab.com/beautyworld_repository/clinic/commit/be6e14a12b2d7656f1ee5f9df21241f7f77711b0))
* **marketplace-fee:** fix marketplace fee page ([5fae42b](https://gitlab.com/beautyworld_repository/clinic/commit/5fae42b28bc8eacf26db6fab8c02c001bd361798))
* **marketplace-fee:** fix marketplace fee page ([057d058](https://gitlab.com/beautyworld_repository/clinic/commit/057d0582be3f73b137008a8a6a797e8320a62ea1))
* **marketplace-treatment:** fix delete button on marketplace and treatment ([95228c1](https://gitlab.com/beautyworld_repository/clinic/commit/95228c1ffe4a5b4b79ccad2e0500e04d3c62e337))
* **marketplace-treatment:** fix error fee after change fee method on model ([26cbb99](https://gitlab.com/beautyworld_repository/clinic/commit/26cbb99a99ee012b15bbb8a7acadf61af644f626))
* **marketplace-treatment:** fix error treatment and marketplace fee ([4235cab](https://gitlab.com/beautyworld_repository/clinic/commit/4235cab381f9114964c26fb5ff7c0774503616f4))
* **marketplace-treatment:** fix treatment and marketplace fee after change the method name ([7414999](https://gitlab.com/beautyworld_repository/clinic/commit/74149991cc77b2a7a57c6cff84954d379a83231f))
* **marketplace-treatment:** fix treatment and marketplace fee after change the method name ([d254362](https://gitlab.com/beautyworld_repository/clinic/commit/d2543623ea2dde3e99513dd0d0173a5e34486580))
* **marketplace:** update marketplace follow the excel ([98b7748](https://gitlab.com/beautyworld_repository/clinic/commit/98b774868017c9845e69358b7a8aa1505ff5a324))
* **marketplace:** update marketplace follow the excel ([9573e45](https://gitlab.com/beautyworld_repository/clinic/commit/9573e45100006c58f11bafd745efee613bedcde8))
* **package-fee:** fix package fee store ([c6c1242](https://gitlab.com/beautyworld_repository/clinic/commit/c6c124263221077e12ddb4558497aabb64e66941))
* **package-form:** fix autoload package form ([44e0343](https://gitlab.com/beautyworld_repository/clinic/commit/44e03431403198f1e582a1eacccca335330246a5))
* **package-form:** fix crud package ([17fa968](https://gitlab.com/beautyworld_repository/clinic/commit/17fa9680a66e5cda7474f00ae0d72a635d950c9a))
* **package-form:** fix crud package ([e6a715f](https://gitlab.com/beautyworld_repository/clinic/commit/e6a715f9c30db549d094c642d02c685c9d73d7d0))
* **product and treatment:** fix category option allow null value ([01b0f2e](https://gitlab.com/beautyworld_repository/clinic/commit/01b0f2ecfa658978ee1861bbcdfb924e47e9bfbb))
* **product-detail:** fix layout fee ([d317cda](https://gitlab.com/beautyworld_repository/clinic/commit/d317cda681cf9b40a1a16e6d4af4479beec55a14))
* **product-detail:** fix layout fee ([5acb843](https://gitlab.com/beautyworld_repository/clinic/commit/5acb843c5d4f871d648efa77c38027752d94163a))
* **product-treatment:** fix category option allow null value ([17256ba](https://gitlab.com/beautyworld_repository/clinic/commit/17256ba71a83f8362901b58fef8729a1298a9895))
* **product-treatment:** fix category option allow null value ([7767729](https://gitlab.com/beautyworld_repository/clinic/commit/7767729a7f1403e60f9b215ce0d5ee5efeec4d35))
* **product:** refactor product ([2e76885](https://gitlab.com/beautyworld_repository/clinic/commit/2e768854a9be64593339344ae5481ac8993ec033))
* **receipt:** fix wrong date on receipt ([7fb95b3](https://gitlab.com/beautyworld_repository/clinic/commit/7fb95b34dd2016c1ea533074a3149beb84a2efb9))
* **receipt:** fix wrong date on receipt ([a26474c](https://gitlab.com/beautyworld_repository/clinic/commit/a26474c2de00eeb0954b4507510d42af50c0dbfc))
* **receipt:** remove tax ([e8d1868](https://gitlab.com/beautyworld_repository/clinic/commit/e8d1868d76463621a141f6c66942b68572c94190))
* **sales-invoice-table:** add print button on sales invoice table ([be9b894](https://gitlab.com/beautyworld_repository/clinic/commit/be9b894a778258b18b41b19e41ad72908d967830))
* **sales-invoice:** fix error sales invoice page ([8935efb](https://gitlab.com/beautyworld_repository/clinic/commit/8935efb8e0179706d45f977d5ac3972866efcf9a))
* **sales-invoice:** fix error sales invoice page ([60fbe6f](https://gitlab.com/beautyworld_repository/clinic/commit/60fbe6fca9f48b1c7ae49cc3f0f5d8934b23c07b))
* **sales-invoice:** fix error sales invoice page ([a78f46a](https://gitlab.com/beautyworld_repository/clinic/commit/a78f46a0bab800b3df106d3a1e7e55d3bface402))
* **sales-invoice:** fix sales invoice approval ([2e1dc31](https://gitlab.com/beautyworld_repository/clinic/commit/2e1dc3182d0fbca2dcf0b082eb58ede4b50a79b3))
* **sales-invoice:** remove tax ([e6b5098](https://gitlab.com/beautyworld_repository/clinic/commit/e6b5098a8240d9901261444f60b51a2c9cf2e7a6))
* **seeder:** fix category seeder and service seeder ([c78969b](https://gitlab.com/beautyworld_repository/clinic/commit/c78969b1a8a41e55ae20b57fa18352c3b0bf0cb1))
* **subscription-plan-item:** fix crud ([5208cf3](https://gitlab.com/beautyworld_repository/clinic/commit/5208cf329363876dc2cce837d3b622fbae4d9a2b))
* **subscription-plan:** fix hide subscription form on crud ([ac2dd4c](https://gitlab.com/beautyworld_repository/clinic/commit/ac2dd4cc86c30ca5a2d375a89d694cea879fa72e))
* **treatment-feature:** fix insert treatment fee ([e532d8a](https://gitlab.com/beautyworld_repository/clinic/commit/e532d8aa3c6a6b5d94e2c70cf0233d83858119b7))
* **treatment-feature:** show  swal alert when admin try to delete treatment ([9340b92](https://gitlab.com/beautyworld_repository/clinic/commit/9340b9291a5eb43f188bcc176cf077db25d10908))
* **treatment-fee-page:** fix layout fee treatment ([877e3f6](https://gitlab.com/beautyworld_repository/clinic/commit/877e3f6e939a4d8d220c3df7960a2eb9193d4e15))
* **treatment-fee-page:** fix layout fee treatment ([81c10b3](https://gitlab.com/beautyworld_repository/clinic/commit/81c10b35d9f9b25172ed5a8fde19ac5e0819abfa))
* **treatment-fee-page:** layout treatment fee header ([0aa62cb](https://gitlab.com/beautyworld_repository/clinic/commit/0aa62cb095fc55202ed7cbb1f6a4798ca91e7d96))
* **treatment-fee-page:** store treatment fee data ([809f998](https://gitlab.com/beautyworld_repository/clinic/commit/809f998202a187edb04c9e71c68c412e514d6e0d))
* **treatment-fee:** fix auto load fee ([e298d99](https://gitlab.com/beautyworld_repository/clinic/commit/e298d99350ead2999b5a0e67777dfcb7ce983d2a))
* **treatment-fee:** fix auto load fee ([9f53c65](https://gitlab.com/beautyworld_repository/clinic/commit/9f53c651e485cab74593f5a1055fbc7f0dd5c7e8))
* **treatment-fee:** fix auto load fee ([1f549f8](https://gitlab.com/beautyworld_repository/clinic/commit/1f549f8ace878d1c56c9c93242e21579def5dc20))
* **treatment-fee:** fix autoload treatment fee detail ([e738140](https://gitlab.com/beautyworld_repository/clinic/commit/e738140cb7fef45b2eb742d4375cbb2ad17b3f66))
* **treatment-form:** adjust treatment fee form display using tabs ([306a641](https://gitlab.com/beautyworld_repository/clinic/commit/306a641b81c2e2925efccdb4c02570c0decc6fc4))
* **treatment-form:** fix store data ([4bf0b50](https://gitlab.com/beautyworld_repository/clinic/commit/4bf0b5039fa70e9a3b634376abdda0e3f7e6e59d))
* **treatment-form:** fix update data on edit state ([32d06c2](https://gitlab.com/beautyworld_repository/clinic/commit/32d06c2b6ae8cafc9e378d6c42b2a6444ec45495))
* **treatment-form:** fix update data on edit state ([1d17f7f](https://gitlab.com/beautyworld_repository/clinic/commit/1d17f7fcdaf282cd37773636f2a296b3c4e41ff6))
* **treatment-form:** set fee value ([ffe0b1b](https://gitlab.com/beautyworld_repository/clinic/commit/ffe0b1bc50ec60d927d91407279028a4711c9096))
* **treatment-material-usage:** fix autoload treatment material usage detail ([6b80056](https://gitlab.com/beautyworld_repository/clinic/commit/6b800565a01d7de4367ed673199b05a148fdde8c))
* **treatment-material-usage:** fix search product ([669d515](https://gitlab.com/beautyworld_repository/clinic/commit/669d515db5d0821cbcf5481f0239ac2cb5d0542c))
* **treatment-material-usage:** fix treatment material usages table ([1ddfb17](https://gitlab.com/beautyworld_repository/clinic/commit/1ddfb17dcd055b4b085c0c5dc52d9f8d600fb545))
* **treatment-material-usage:** show treatment by id ([f1ece6f](https://gitlab.com/beautyworld_repository/clinic/commit/f1ece6f1a851b92e91ae7ddef38b781e14b55cba))
* **treatment:** adjust treatment table field ([50e1e8e](https://gitlab.com/beautyworld_repository/clinic/commit/50e1e8e3fc7da9fdc399fe5057c995964477be57))
* **treatment:** fix add treatment material usage ([31bfeb5](https://gitlab.com/beautyworld_repository/clinic/commit/31bfeb545b552b016d4455be54c126cfe2141158))
* **treatment:** fix not showing price on product search ([418128f](https://gitlab.com/beautyworld_repository/clinic/commit/418128f1d46b5a22cf5a84372c6fbc491f2f3ad8))
* **treatment:** fix redirect to master treatments after save data ([6f973f6](https://gitlab.com/beautyworld_repository/clinic/commit/6f973f6bf13c6e617466317ee30062402a914b13))
* **treatment:** fix treatment seeder ([5677960](https://gitlab.com/beautyworld_repository/clinic/commit/5677960ae2b2936576125e8dbea97f67d42cd2df))
* **turn-over:** fix delete function ([0b5b7e7](https://gitlab.com/beautyworld_repository/clinic/commit/0b5b7e7c22cc5d967ee9a08779b85fbd35ae2b23))
* **turn-over:** fix delete function ([af7477d](https://gitlab.com/beautyworld_repository/clinic/commit/af7477de81e9e11e04280f3e97006dd8bcc2333d))
* **user-detail:** set unique code on user detail ([562e6d2](https://gitlab.com/beautyworld_repository/clinic/commit/562e6d206b5635fe9725fd810901e5dc9af2f9d0))
* **user-seeder:** add melinda and bu selvi to user seeder ([5d3a704](https://gitlab.com/beautyworld_repository/clinic/commit/5d3a704bf8f43d41d3568978aea4be144390f287))
* **user-seeder:** add melinda and bu selvi to user seeder ([d391608](https://gitlab.com/beautyworld_repository/clinic/commit/d391608b391d3f240c1c13e4d16e3fa284ce8ea0))


* **package-form:** update package-form ([6feb4a3](https://gitlab.com/beautyworld_repository/clinic/commit/6feb4a3c38cd6eef42fef802b2aaa029487f789c))
* **service-item:** rename to treatment_material_usages ([a57fcfa](https://gitlab.com/beautyworld_repository/clinic/commit/a57fcfab240fa692b8cf7c2aa426c89e5c6b7f84))
* **services-feature:** change service to treatment ([3e9ae2b](https://gitlab.com/beautyworld_repository/clinic/commit/3e9ae2b32424a60d70291d4dc0946fce32bd1448))
* **treatment-feature:** refactor ([9d13d56](https://gitlab.com/beautyworld_repository/clinic/commit/9d13d56bef1e5af7218325057b64e19e393c9831))
* **treatment-fee-form:** clear fee value ([04bd04b](https://gitlab.com/beautyworld_repository/clinic/commit/04bd04b9984e43e62d0428b10ab953d6fbf024e6))
* **treatment-form:** use compoenent for material usage, add sample event for Rp agains Percents ([7f5a6e4](https://gitlab.com/beautyworld_repository/clinic/commit/7f5a6e41a496f6a250e997ebafff3bedd62b2c84))
* **treatment:** refactor treatment ([c42a61f](https://gitlab.com/beautyworld_repository/clinic/commit/c42a61fabf01e0daf022e2ec7ebed69571e86b81))

## [3.0.0](https://gitlab.com/beautyworld_repository/clinic/compare/v2.3.2...v5.0.0) (2024-04-17)


### ⚠ BREAKING CHANGES

* **treatment:** all the module with services
* **subscriptions:** sales invoice
* **subscriptions:** sales invoice
* **package-form:** - sales invoice
* **treatment-fee-form:** - Sales Invoice
      - Package
      - Appointment
* **treatment-form:** - Sales Invoice
    - Package
    - Appointment
* **treatment-feature:** - Sales Invoice
        - Package
        - Appointment
* **service-item:** - Sales Invoice
      - Package
      - Appointment
* **treatment-feature:** - Sales Invoice
    - Package
    - Appointment
* **services-feature:** - Sales Invoice
  - Package
  - Appointment

### Features

* **commission-report-page:** add new layout for commision report page ([b03c115](https://gitlab.com/beautyworld_repository/clinic/commit/b03c1155b14a0637c671c30fc6e54f75b696a82d))
* **commission-report-page:** add new layout for commision report page ([09ec1f2](https://gitlab.com/beautyworld_repository/clinic/commit/09ec1f28f5341b5fee2bb6f0b1278b6a3e40767d))
* **commission-report-page:** add new layout for commision report page ([9c4b77b](https://gitlab.com/beautyworld_repository/clinic/commit/9c4b77b93e5fa0e3209d3ed33234614df0dc5fe8))
* **commission-report-page:** add new layout for commision report page ([ff39adc](https://gitlab.com/beautyworld_repository/clinic/commit/ff39adc407aedbb7633934a0d8e545ceecdd56bf))
* **commission-report-page:** add new layout for commision report page ([94fb9e6](https://gitlab.com/beautyworld_repository/clinic/commit/94fb9e6958e0e513f2c32143568a749121f3eafd))
* **commission-report-page:** add new layout for commision report page ([0427136](https://gitlab.com/beautyworld_repository/clinic/commit/042713696d80516bf0d0e00db29beb7aa9dc91f1))
* **commission-report-page:** add new layout for commision report page ([ea82b45](https://gitlab.com/beautyworld_repository/clinic/commit/ea82b459e1c00600e8af468f0c198f977a0f6582))
* **commission-report-page:** add new layout for commision report page ([ddccb4e](https://gitlab.com/beautyworld_repository/clinic/commit/ddccb4e342514f4a4f328056877287784c24b2cd))
* **marketplace:** initiate marketplace fee and material usage page ([c3ef699](https://gitlab.com/beautyworld_repository/clinic/commit/c3ef6998fd69414c2d6c3f69ee822192b37384aa))
* **product-fee:** add product fee page ([803acf5](https://gitlab.com/beautyworld_repository/clinic/commit/803acf5665774bf788d6cafae3ed7c95bfd294c9))
* **sales-invoice-api:** add sales invoice api ([80efd1c](https://gitlab.com/beautyworld_repository/clinic/commit/80efd1c728a0bbaae3e3460fe6a1dc52d3233273))
* **sales-invoice-api:** add sales invoice api ([85ca1de](https://gitlab.com/beautyworld_repository/clinic/commit/85ca1dea3580655675e56b22617ba572e43d186c))
* **subscription:** add seeder for subscription transaction ([c9079c5](https://gitlab.com/beautyworld_repository/clinic/commit/c9079c51e78ae4cb51821a946c677591ce0d118d))
* **subscription:** add seeder for subscription transaction ([139286c](https://gitlab.com/beautyworld_repository/clinic/commit/139286c378b8f9cd5bfa21d44d65f4a058d54fbd))
* **subscription:** add subscription seeder ([eff4ffd](https://gitlab.com/beautyworld_repository/clinic/commit/eff4ffdab18508f7391956c09e7dae4e8917029f))
* **subscription:** initiate subscriptions transaction ([82c9bef](https://gitlab.com/beautyworld_repository/clinic/commit/82c9bef137be0519fe6e67ec3df382d5593926ce))
* **subscriptions:** add new feature call subscriptions ([9f20a7a](https://gitlab.com/beautyworld_repository/clinic/commit/9f20a7a6e6712b3304126d17713056d9279a99ff))
* **subscriptions:** add new subscriptions on sales invoice form ([3a04c15](https://gitlab.com/beautyworld_repository/clinic/commit/3a04c1588a64bd350e9d9b5121af8aa6d081a285))
* **treatment:** add treatment fee and treatment material usages ([c30b8b1](https://gitlab.com/beautyworld_repository/clinic/commit/c30b8b1df502ac146045ecfd003b59b8179cbe04))
* **turn over report:** update turn over report ([d6c2aad](https://gitlab.com/beautyworld_repository/clinic/commit/d6c2aad1a365bef666202bcf2b96373ee47a590f))
* **turn-over:** add master turn over ([b8a771a](https://gitlab.com/beautyworld_repository/clinic/commit/b8a771af0b5a3036c2fd4ff469c5f634b33b5d7b))
* **turn-over:** add master turn over ([1cf8bad](https://gitlab.com/beautyworld_repository/clinic/commit/1cf8bad5bd5626f34ac72ce8c5718dab092101c8))


### Bug Fixes

* **appointment-form:** fix appointment form edit not save PIC ([a46d30c](https://gitlab.com/beautyworld_repository/clinic/commit/a46d30c551c40472bd41361ffb17f629698cfefc))
* **appointment-form:** fix appointment form edit not save PIC ([013c66a](https://gitlab.com/beautyworld_repository/clinic/commit/013c66a1da07cdbcafbfff5a78275746354af275))
* **appointment-form:** use livewire customer-search on appointment-form ([3a67739](https://gitlab.com/beautyworld_repository/clinic/commit/3a67739414fa259323680647bc7b599fbdff1d04))
* **category-subcategory:** fix category and subcategory seeder ([ec85353](https://gitlab.com/beautyworld_repository/clinic/commit/ec8535396a6f341c7019bde35b3a01a87db61ade))
* **changelog:** update change log ([3138cd1](https://gitlab.com/beautyworld_repository/clinic/commit/3138cd187ce2fd3b39adce0d9ed6f7e9178f7b7b))
* **customer-form:** fix error save customer ([1766939](https://gitlab.com/beautyworld_repository/clinic/commit/17669395c0da71323fb5550fadaa4ed0b4b0d428))
* **customer-form:** fix error save customer ([e66ce97](https://gitlab.com/beautyworld_repository/clinic/commit/e66ce972230c6a94f6d5909f8c31bfbd0b0df4c4))
* **customer-form:** fix error save customer ([1eb3b8a](https://gitlab.com/beautyworld_repository/clinic/commit/1eb3b8a77586b0e015ae3d80fa3f473394a9fea6))
* **data-redemption:** fix error redemption left ([83f853b](https://gitlab.com/beautyworld_repository/clinic/commit/83f853bc97c8bf1bb69acdec03e8440ad721de1d))
* **data-redemption:** fix error redemption left ([fd0f3a5](https://gitlab.com/beautyworld_repository/clinic/commit/fd0f3a5275c7f67fa911223e04c7187ca80c29ae))
* **helpers:** remove unnecessary role ([be6e14a](https://gitlab.com/beautyworld_repository/clinic/commit/be6e14a12b2d7656f1ee5f9df21241f7f77711b0))
* **marketplace-fee:** fix marketplace fee page ([5fae42b](https://gitlab.com/beautyworld_repository/clinic/commit/5fae42b28bc8eacf26db6fab8c02c001bd361798))
* **marketplace-fee:** fix marketplace fee page ([057d058](https://gitlab.com/beautyworld_repository/clinic/commit/057d0582be3f73b137008a8a6a797e8320a62ea1))
* **marketplace-treatment:** fix delete button on marketplace and treatment ([95228c1](https://gitlab.com/beautyworld_repository/clinic/commit/95228c1ffe4a5b4b79ccad2e0500e04d3c62e337))
* **marketplace-treatment:** fix error fee after change fee method on model ([26cbb99](https://gitlab.com/beautyworld_repository/clinic/commit/26cbb99a99ee012b15bbb8a7acadf61af644f626))
* **marketplace-treatment:** fix error treatment and marketplace fee ([4235cab](https://gitlab.com/beautyworld_repository/clinic/commit/4235cab381f9114964c26fb5ff7c0774503616f4))
* **marketplace-treatment:** fix treatment and marketplace fee after change the method name ([7414999](https://gitlab.com/beautyworld_repository/clinic/commit/74149991cc77b2a7a57c6cff84954d379a83231f))
* **marketplace-treatment:** fix treatment and marketplace fee after change the method name ([d254362](https://gitlab.com/beautyworld_repository/clinic/commit/d2543623ea2dde3e99513dd0d0173a5e34486580))
* **marketplace:** update marketplace follow the excel ([98b7748](https://gitlab.com/beautyworld_repository/clinic/commit/98b774868017c9845e69358b7a8aa1505ff5a324))
* **marketplace:** update marketplace follow the excel ([9573e45](https://gitlab.com/beautyworld_repository/clinic/commit/9573e45100006c58f11bafd745efee613bedcde8))
* **package-fee:** fix package fee store ([c6c1242](https://gitlab.com/beautyworld_repository/clinic/commit/c6c124263221077e12ddb4558497aabb64e66941))
* **package-form:** fix autoload package form ([44e0343](https://gitlab.com/beautyworld_repository/clinic/commit/44e03431403198f1e582a1eacccca335330246a5))
* **package-form:** fix crud package ([17fa968](https://gitlab.com/beautyworld_repository/clinic/commit/17fa9680a66e5cda7474f00ae0d72a635d950c9a))
* **package-form:** fix crud package ([e6a715f](https://gitlab.com/beautyworld_repository/clinic/commit/e6a715f9c30db549d094c642d02c685c9d73d7d0))
* **product and treatment:** fix category option allow null value ([01b0f2e](https://gitlab.com/beautyworld_repository/clinic/commit/01b0f2ecfa658978ee1861bbcdfb924e47e9bfbb))
* **product-detail:** fix layout fee ([d317cda](https://gitlab.com/beautyworld_repository/clinic/commit/d317cda681cf9b40a1a16e6d4af4479beec55a14))
* **product-detail:** fix layout fee ([5acb843](https://gitlab.com/beautyworld_repository/clinic/commit/5acb843c5d4f871d648efa77c38027752d94163a))
* **product-treatment:** fix category option allow null value ([17256ba](https://gitlab.com/beautyworld_repository/clinic/commit/17256ba71a83f8362901b58fef8729a1298a9895))
* **product-treatment:** fix category option allow null value ([7767729](https://gitlab.com/beautyworld_repository/clinic/commit/7767729a7f1403e60f9b215ce0d5ee5efeec4d35))
* **product:** refactor product ([2e76885](https://gitlab.com/beautyworld_repository/clinic/commit/2e768854a9be64593339344ae5481ac8993ec033))
* **receipt:** fix wrong date on receipt ([7fb95b3](https://gitlab.com/beautyworld_repository/clinic/commit/7fb95b34dd2016c1ea533074a3149beb84a2efb9))
* **receipt:** fix wrong date on receipt ([a26474c](https://gitlab.com/beautyworld_repository/clinic/commit/a26474c2de00eeb0954b4507510d42af50c0dbfc))
* **receipt:** remove tax ([e8d1868](https://gitlab.com/beautyworld_repository/clinic/commit/e8d1868d76463621a141f6c66942b68572c94190))
* **sales-invoice-table:** add print button on sales invoice table ([be9b894](https://gitlab.com/beautyworld_repository/clinic/commit/be9b894a778258b18b41b19e41ad72908d967830))
* **sales-invoice:** fix error sales invoice page ([8935efb](https://gitlab.com/beautyworld_repository/clinic/commit/8935efb8e0179706d45f977d5ac3972866efcf9a))
* **sales-invoice:** fix error sales invoice page ([60fbe6f](https://gitlab.com/beautyworld_repository/clinic/commit/60fbe6fca9f48b1c7ae49cc3f0f5d8934b23c07b))
* **sales-invoice:** fix error sales invoice page ([a78f46a](https://gitlab.com/beautyworld_repository/clinic/commit/a78f46a0bab800b3df106d3a1e7e55d3bface402))
* **sales-invoice:** fix sales invoice approval ([2e1dc31](https://gitlab.com/beautyworld_repository/clinic/commit/2e1dc3182d0fbca2dcf0b082eb58ede4b50a79b3))
* **sales-invoice:** remove tax ([e6b5098](https://gitlab.com/beautyworld_repository/clinic/commit/e6b5098a8240d9901261444f60b51a2c9cf2e7a6))
* **seeder:** fix category seeder and service seeder ([c78969b](https://gitlab.com/beautyworld_repository/clinic/commit/c78969b1a8a41e55ae20b57fa18352c3b0bf0cb1))
* **subscription-plan-item:** fix crud ([5208cf3](https://gitlab.com/beautyworld_repository/clinic/commit/5208cf329363876dc2cce837d3b622fbae4d9a2b))
* **subscription-plan:** fix hide subscription form on crud ([ac2dd4c](https://gitlab.com/beautyworld_repository/clinic/commit/ac2dd4cc86c30ca5a2d375a89d694cea879fa72e))
* **treatment-feature:** fix insert treatment fee ([e532d8a](https://gitlab.com/beautyworld_repository/clinic/commit/e532d8aa3c6a6b5d94e2c70cf0233d83858119b7))
* **treatment-feature:** show  swal alert when admin try to delete treatment ([9340b92](https://gitlab.com/beautyworld_repository/clinic/commit/9340b9291a5eb43f188bcc176cf077db25d10908))
* **treatment-fee-page:** fix layout fee treatment ([877e3f6](https://gitlab.com/beautyworld_repository/clinic/commit/877e3f6e939a4d8d220c3df7960a2eb9193d4e15))
* **treatment-fee-page:** fix layout fee treatment ([81c10b3](https://gitlab.com/beautyworld_repository/clinic/commit/81c10b35d9f9b25172ed5a8fde19ac5e0819abfa))
* **treatment-fee-page:** layout treatment fee header ([0aa62cb](https://gitlab.com/beautyworld_repository/clinic/commit/0aa62cb095fc55202ed7cbb1f6a4798ca91e7d96))
* **treatment-fee-page:** store treatment fee data ([809f998](https://gitlab.com/beautyworld_repository/clinic/commit/809f998202a187edb04c9e71c68c412e514d6e0d))
* **treatment-fee:** fix auto load fee ([e298d99](https://gitlab.com/beautyworld_repository/clinic/commit/e298d99350ead2999b5a0e67777dfcb7ce983d2a))
* **treatment-fee:** fix auto load fee ([9f53c65](https://gitlab.com/beautyworld_repository/clinic/commit/9f53c651e485cab74593f5a1055fbc7f0dd5c7e8))
* **treatment-fee:** fix auto load fee ([1f549f8](https://gitlab.com/beautyworld_repository/clinic/commit/1f549f8ace878d1c56c9c93242e21579def5dc20))
* **treatment-fee:** fix autoload treatment fee detail ([e738140](https://gitlab.com/beautyworld_repository/clinic/commit/e738140cb7fef45b2eb742d4375cbb2ad17b3f66))
* **treatment-form:** adjust treatment fee form display using tabs ([306a641](https://gitlab.com/beautyworld_repository/clinic/commit/306a641b81c2e2925efccdb4c02570c0decc6fc4))
* **treatment-form:** fix store data ([4bf0b50](https://gitlab.com/beautyworld_repository/clinic/commit/4bf0b5039fa70e9a3b634376abdda0e3f7e6e59d))
* **treatment-form:** fix update data on edit state ([32d06c2](https://gitlab.com/beautyworld_repository/clinic/commit/32d06c2b6ae8cafc9e378d6c42b2a6444ec45495))
* **treatment-form:** fix update data on edit state ([1d17f7f](https://gitlab.com/beautyworld_repository/clinic/commit/1d17f7fcdaf282cd37773636f2a296b3c4e41ff6))
* **treatment-form:** set fee value ([ffe0b1b](https://gitlab.com/beautyworld_repository/clinic/commit/ffe0b1bc50ec60d927d91407279028a4711c9096))
* **treatment-material-usage:** fix autoload treatment material usage detail ([6b80056](https://gitlab.com/beautyworld_repository/clinic/commit/6b800565a01d7de4367ed673199b05a148fdde8c))
* **treatment-material-usage:** fix search product ([669d515](https://gitlab.com/beautyworld_repository/clinic/commit/669d515db5d0821cbcf5481f0239ac2cb5d0542c))
* **treatment-material-usage:** fix treatment material usages table ([1ddfb17](https://gitlab.com/beautyworld_repository/clinic/commit/1ddfb17dcd055b4b085c0c5dc52d9f8d600fb545))
* **treatment-material-usage:** show treatment by id ([f1ece6f](https://gitlab.com/beautyworld_repository/clinic/commit/f1ece6f1a851b92e91ae7ddef38b781e14b55cba))
* **treatment:** adjust treatment table field ([50e1e8e](https://gitlab.com/beautyworld_repository/clinic/commit/50e1e8e3fc7da9fdc399fe5057c995964477be57))
* **treatment:** fix add treatment material usage ([31bfeb5](https://gitlab.com/beautyworld_repository/clinic/commit/31bfeb545b552b016d4455be54c126cfe2141158))
* **treatment:** fix not showing price on product search ([418128f](https://gitlab.com/beautyworld_repository/clinic/commit/418128f1d46b5a22cf5a84372c6fbc491f2f3ad8))
* **treatment:** fix redirect to master treatments after save data ([6f973f6](https://gitlab.com/beautyworld_repository/clinic/commit/6f973f6bf13c6e617466317ee30062402a914b13))
* **treatment:** fix treatment seeder ([5677960](https://gitlab.com/beautyworld_repository/clinic/commit/5677960ae2b2936576125e8dbea97f67d42cd2df))
* **turn-over:** fix delete function ([0b5b7e7](https://gitlab.com/beautyworld_repository/clinic/commit/0b5b7e7c22cc5d967ee9a08779b85fbd35ae2b23))
* **turn-over:** fix delete function ([af7477d](https://gitlab.com/beautyworld_repository/clinic/commit/af7477de81e9e11e04280f3e97006dd8bcc2333d))
* **user-detail:** set unique code on user detail ([562e6d2](https://gitlab.com/beautyworld_repository/clinic/commit/562e6d206b5635fe9725fd810901e5dc9af2f9d0))
* **user-seeder:** add melinda and bu selvi to user seeder ([5d3a704](https://gitlab.com/beautyworld_repository/clinic/commit/5d3a704bf8f43d41d3568978aea4be144390f287))
* **user-seeder:** add melinda and bu selvi to user seeder ([d391608](https://gitlab.com/beautyworld_repository/clinic/commit/d391608b391d3f240c1c13e4d16e3fa284ce8ea0))


* **package-form:** update package-form ([6feb4a3](https://gitlab.com/beautyworld_repository/clinic/commit/6feb4a3c38cd6eef42fef802b2aaa029487f789c))
* **service-item:** rename to treatment_material_usages ([a57fcfa](https://gitlab.com/beautyworld_repository/clinic/commit/a57fcfab240fa692b8cf7c2aa426c89e5c6b7f84))
* **services-feature:** change service to treatment ([3e9ae2b](https://gitlab.com/beautyworld_repository/clinic/commit/3e9ae2b32424a60d70291d4dc0946fce32bd1448))
* **treatment-feature:** refactor ([9d13d56](https://gitlab.com/beautyworld_repository/clinic/commit/9d13d56bef1e5af7218325057b64e19e393c9831))
* **treatment-fee-form:** clear fee value ([04bd04b](https://gitlab.com/beautyworld_repository/clinic/commit/04bd04b9984e43e62d0428b10ab953d6fbf024e6))
* **treatment-form:** use compoenent for material usage, add sample event for Rp agains Percents ([7f5a6e4](https://gitlab.com/beautyworld_repository/clinic/commit/7f5a6e41a496f6a250e997ebafff3bedd62b2c84))
* **treatment:** refactor treatment ([c42a61f](https://gitlab.com/beautyworld_repository/clinic/commit/c42a61fabf01e0daf022e2ec7ebed69571e86b81))

## [3.0.0](https://gitlab.com/beautyworld_repository/clinic/compare/v2.3.2...v4.0.0) (2024-04-17)


### ⚠ BREAKING CHANGES

* **treatment:** all the module with services
* **subscriptions:** sales invoice
* **subscriptions:** sales invoice
* **package-form:** - sales invoice
* **treatment-fee-form:** - Sales Invoice
      - Package
      - Appointment
* **treatment-form:** - Sales Invoice
    - Package
    - Appointment
* **treatment-feature:** - Sales Invoice
        - Package
        - Appointment
* **service-item:** - Sales Invoice
      - Package
      - Appointment
* **treatment-feature:** - Sales Invoice
    - Package
    - Appointment
* **services-feature:** - Sales Invoice
  - Package
  - Appointment

### Features

* **commission-report-page:** add new layout for commision report page ([b03c115](https://gitlab.com/beautyworld_repository/clinic/commit/b03c1155b14a0637c671c30fc6e54f75b696a82d))
* **commission-report-page:** add new layout for commision report page ([09ec1f2](https://gitlab.com/beautyworld_repository/clinic/commit/09ec1f28f5341b5fee2bb6f0b1278b6a3e40767d))
* **commission-report-page:** add new layout for commision report page ([9c4b77b](https://gitlab.com/beautyworld_repository/clinic/commit/9c4b77b93e5fa0e3209d3ed33234614df0dc5fe8))
* **commission-report-page:** add new layout for commision report page ([ff39adc](https://gitlab.com/beautyworld_repository/clinic/commit/ff39adc407aedbb7633934a0d8e545ceecdd56bf))
* **commission-report-page:** add new layout for commision report page ([94fb9e6](https://gitlab.com/beautyworld_repository/clinic/commit/94fb9e6958e0e513f2c32143568a749121f3eafd))
* **commission-report-page:** add new layout for commision report page ([0427136](https://gitlab.com/beautyworld_repository/clinic/commit/042713696d80516bf0d0e00db29beb7aa9dc91f1))
* **commission-report-page:** add new layout for commision report page ([ea82b45](https://gitlab.com/beautyworld_repository/clinic/commit/ea82b459e1c00600e8af468f0c198f977a0f6582))
* **commission-report-page:** add new layout for commision report page ([ddccb4e](https://gitlab.com/beautyworld_repository/clinic/commit/ddccb4e342514f4a4f328056877287784c24b2cd))
* **marketplace:** initiate marketplace fee and material usage page ([c3ef699](https://gitlab.com/beautyworld_repository/clinic/commit/c3ef6998fd69414c2d6c3f69ee822192b37384aa))
* **product-fee:** add product fee page ([803acf5](https://gitlab.com/beautyworld_repository/clinic/commit/803acf5665774bf788d6cafae3ed7c95bfd294c9))
* **sales-invoice-api:** add sales invoice api ([80efd1c](https://gitlab.com/beautyworld_repository/clinic/commit/80efd1c728a0bbaae3e3460fe6a1dc52d3233273))
* **sales-invoice-api:** add sales invoice api ([85ca1de](https://gitlab.com/beautyworld_repository/clinic/commit/85ca1dea3580655675e56b22617ba572e43d186c))
* **subscription:** add seeder for subscription transaction ([c9079c5](https://gitlab.com/beautyworld_repository/clinic/commit/c9079c51e78ae4cb51821a946c677591ce0d118d))
* **subscription:** add seeder for subscription transaction ([139286c](https://gitlab.com/beautyworld_repository/clinic/commit/139286c378b8f9cd5bfa21d44d65f4a058d54fbd))
* **subscription:** add subscription seeder ([eff4ffd](https://gitlab.com/beautyworld_repository/clinic/commit/eff4ffdab18508f7391956c09e7dae4e8917029f))
* **subscription:** initiate subscriptions transaction ([82c9bef](https://gitlab.com/beautyworld_repository/clinic/commit/82c9bef137be0519fe6e67ec3df382d5593926ce))
* **subscriptions:** add new feature call subscriptions ([9f20a7a](https://gitlab.com/beautyworld_repository/clinic/commit/9f20a7a6e6712b3304126d17713056d9279a99ff))
* **subscriptions:** add new subscriptions on sales invoice form ([3a04c15](https://gitlab.com/beautyworld_repository/clinic/commit/3a04c1588a64bd350e9d9b5121af8aa6d081a285))
* **treatment:** add treatment fee and treatment material usages ([c30b8b1](https://gitlab.com/beautyworld_repository/clinic/commit/c30b8b1df502ac146045ecfd003b59b8179cbe04))
* **turn over report:** update turn over report ([d6c2aad](https://gitlab.com/beautyworld_repository/clinic/commit/d6c2aad1a365bef666202bcf2b96373ee47a590f))
* **turn-over:** add master turn over ([b8a771a](https://gitlab.com/beautyworld_repository/clinic/commit/b8a771af0b5a3036c2fd4ff469c5f634b33b5d7b))
* **turn-over:** add master turn over ([1cf8bad](https://gitlab.com/beautyworld_repository/clinic/commit/1cf8bad5bd5626f34ac72ce8c5718dab092101c8))


### Bug Fixes

* **appointment-form:** fix appointment form edit not save PIC ([a46d30c](https://gitlab.com/beautyworld_repository/clinic/commit/a46d30c551c40472bd41361ffb17f629698cfefc))
* **appointment-form:** fix appointment form edit not save PIC ([013c66a](https://gitlab.com/beautyworld_repository/clinic/commit/013c66a1da07cdbcafbfff5a78275746354af275))
* **appointment-form:** use livewire customer-search on appointment-form ([3a67739](https://gitlab.com/beautyworld_repository/clinic/commit/3a67739414fa259323680647bc7b599fbdff1d04))
* **category-subcategory:** fix category and subcategory seeder ([ec85353](https://gitlab.com/beautyworld_repository/clinic/commit/ec8535396a6f341c7019bde35b3a01a87db61ade))
* **customer-form:** fix error save customer ([1766939](https://gitlab.com/beautyworld_repository/clinic/commit/17669395c0da71323fb5550fadaa4ed0b4b0d428))
* **customer-form:** fix error save customer ([e66ce97](https://gitlab.com/beautyworld_repository/clinic/commit/e66ce972230c6a94f6d5909f8c31bfbd0b0df4c4))
* **customer-form:** fix error save customer ([1eb3b8a](https://gitlab.com/beautyworld_repository/clinic/commit/1eb3b8a77586b0e015ae3d80fa3f473394a9fea6))
* **data-redemption:** fix error redemption left ([83f853b](https://gitlab.com/beautyworld_repository/clinic/commit/83f853bc97c8bf1bb69acdec03e8440ad721de1d))
* **data-redemption:** fix error redemption left ([fd0f3a5](https://gitlab.com/beautyworld_repository/clinic/commit/fd0f3a5275c7f67fa911223e04c7187ca80c29ae))
* **helpers:** remove unnecessary role ([be6e14a](https://gitlab.com/beautyworld_repository/clinic/commit/be6e14a12b2d7656f1ee5f9df21241f7f77711b0))
* **marketplace-fee:** fix marketplace fee page ([5fae42b](https://gitlab.com/beautyworld_repository/clinic/commit/5fae42b28bc8eacf26db6fab8c02c001bd361798))
* **marketplace-fee:** fix marketplace fee page ([057d058](https://gitlab.com/beautyworld_repository/clinic/commit/057d0582be3f73b137008a8a6a797e8320a62ea1))
* **marketplace-treatment:** fix delete button on marketplace and treatment ([95228c1](https://gitlab.com/beautyworld_repository/clinic/commit/95228c1ffe4a5b4b79ccad2e0500e04d3c62e337))
* **marketplace-treatment:** fix error fee after change fee method on model ([26cbb99](https://gitlab.com/beautyworld_repository/clinic/commit/26cbb99a99ee012b15bbb8a7acadf61af644f626))
* **marketplace-treatment:** fix error treatment and marketplace fee ([4235cab](https://gitlab.com/beautyworld_repository/clinic/commit/4235cab381f9114964c26fb5ff7c0774503616f4))
* **marketplace-treatment:** fix treatment and marketplace fee after change the method name ([7414999](https://gitlab.com/beautyworld_repository/clinic/commit/74149991cc77b2a7a57c6cff84954d379a83231f))
* **marketplace-treatment:** fix treatment and marketplace fee after change the method name ([d254362](https://gitlab.com/beautyworld_repository/clinic/commit/d2543623ea2dde3e99513dd0d0173a5e34486580))
* **marketplace:** update marketplace follow the excel ([98b7748](https://gitlab.com/beautyworld_repository/clinic/commit/98b774868017c9845e69358b7a8aa1505ff5a324))
* **marketplace:** update marketplace follow the excel ([9573e45](https://gitlab.com/beautyworld_repository/clinic/commit/9573e45100006c58f11bafd745efee613bedcde8))
* **package-fee:** fix package fee store ([c6c1242](https://gitlab.com/beautyworld_repository/clinic/commit/c6c124263221077e12ddb4558497aabb64e66941))
* **package-form:** fix autoload package form ([44e0343](https://gitlab.com/beautyworld_repository/clinic/commit/44e03431403198f1e582a1eacccca335330246a5))
* **package-form:** fix crud package ([17fa968](https://gitlab.com/beautyworld_repository/clinic/commit/17fa9680a66e5cda7474f00ae0d72a635d950c9a))
* **package-form:** fix crud package ([e6a715f](https://gitlab.com/beautyworld_repository/clinic/commit/e6a715f9c30db549d094c642d02c685c9d73d7d0))
* **product and treatment:** fix category option allow null value ([01b0f2e](https://gitlab.com/beautyworld_repository/clinic/commit/01b0f2ecfa658978ee1861bbcdfb924e47e9bfbb))
* **product-detail:** fix layout fee ([d317cda](https://gitlab.com/beautyworld_repository/clinic/commit/d317cda681cf9b40a1a16e6d4af4479beec55a14))
* **product-detail:** fix layout fee ([5acb843](https://gitlab.com/beautyworld_repository/clinic/commit/5acb843c5d4f871d648efa77c38027752d94163a))
* **product-treatment:** fix category option allow null value ([17256ba](https://gitlab.com/beautyworld_repository/clinic/commit/17256ba71a83f8362901b58fef8729a1298a9895))
* **product-treatment:** fix category option allow null value ([7767729](https://gitlab.com/beautyworld_repository/clinic/commit/7767729a7f1403e60f9b215ce0d5ee5efeec4d35))
* **product:** refactor product ([2e76885](https://gitlab.com/beautyworld_repository/clinic/commit/2e768854a9be64593339344ae5481ac8993ec033))
* **receipt:** fix wrong date on receipt ([7fb95b3](https://gitlab.com/beautyworld_repository/clinic/commit/7fb95b34dd2016c1ea533074a3149beb84a2efb9))
* **receipt:** fix wrong date on receipt ([a26474c](https://gitlab.com/beautyworld_repository/clinic/commit/a26474c2de00eeb0954b4507510d42af50c0dbfc))
* **receipt:** remove tax ([e8d1868](https://gitlab.com/beautyworld_repository/clinic/commit/e8d1868d76463621a141f6c66942b68572c94190))
* **sales-invoice-table:** add print button on sales invoice table ([be9b894](https://gitlab.com/beautyworld_repository/clinic/commit/be9b894a778258b18b41b19e41ad72908d967830))
* **sales-invoice:** fix error sales invoice page ([8935efb](https://gitlab.com/beautyworld_repository/clinic/commit/8935efb8e0179706d45f977d5ac3972866efcf9a))
* **sales-invoice:** fix error sales invoice page ([60fbe6f](https://gitlab.com/beautyworld_repository/clinic/commit/60fbe6fca9f48b1c7ae49cc3f0f5d8934b23c07b))
* **sales-invoice:** fix error sales invoice page ([a78f46a](https://gitlab.com/beautyworld_repository/clinic/commit/a78f46a0bab800b3df106d3a1e7e55d3bface402))
* **sales-invoice:** fix sales invoice approval ([2e1dc31](https://gitlab.com/beautyworld_repository/clinic/commit/2e1dc3182d0fbca2dcf0b082eb58ede4b50a79b3))
* **sales-invoice:** remove tax ([e6b5098](https://gitlab.com/beautyworld_repository/clinic/commit/e6b5098a8240d9901261444f60b51a2c9cf2e7a6))
* **seeder:** fix category seeder and service seeder ([c78969b](https://gitlab.com/beautyworld_repository/clinic/commit/c78969b1a8a41e55ae20b57fa18352c3b0bf0cb1))
* **subscription-plan-item:** fix crud ([5208cf3](https://gitlab.com/beautyworld_repository/clinic/commit/5208cf329363876dc2cce837d3b622fbae4d9a2b))
* **subscription-plan:** fix hide subscription form on crud ([ac2dd4c](https://gitlab.com/beautyworld_repository/clinic/commit/ac2dd4cc86c30ca5a2d375a89d694cea879fa72e))
* **treatment-feature:** fix insert treatment fee ([e532d8a](https://gitlab.com/beautyworld_repository/clinic/commit/e532d8aa3c6a6b5d94e2c70cf0233d83858119b7))
* **treatment-feature:** show  swal alert when admin try to delete treatment ([9340b92](https://gitlab.com/beautyworld_repository/clinic/commit/9340b9291a5eb43f188bcc176cf077db25d10908))
* **treatment-fee-page:** fix layout fee treatment ([877e3f6](https://gitlab.com/beautyworld_repository/clinic/commit/877e3f6e939a4d8d220c3df7960a2eb9193d4e15))
* **treatment-fee-page:** fix layout fee treatment ([81c10b3](https://gitlab.com/beautyworld_repository/clinic/commit/81c10b35d9f9b25172ed5a8fde19ac5e0819abfa))
* **treatment-fee-page:** layout treatment fee header ([0aa62cb](https://gitlab.com/beautyworld_repository/clinic/commit/0aa62cb095fc55202ed7cbb1f6a4798ca91e7d96))
* **treatment-fee-page:** store treatment fee data ([809f998](https://gitlab.com/beautyworld_repository/clinic/commit/809f998202a187edb04c9e71c68c412e514d6e0d))
* **treatment-fee:** fix auto load fee ([e298d99](https://gitlab.com/beautyworld_repository/clinic/commit/e298d99350ead2999b5a0e67777dfcb7ce983d2a))
* **treatment-fee:** fix auto load fee ([9f53c65](https://gitlab.com/beautyworld_repository/clinic/commit/9f53c651e485cab74593f5a1055fbc7f0dd5c7e8))
* **treatment-fee:** fix auto load fee ([1f549f8](https://gitlab.com/beautyworld_repository/clinic/commit/1f549f8ace878d1c56c9c93242e21579def5dc20))
* **treatment-fee:** fix autoload treatment fee detail ([e738140](https://gitlab.com/beautyworld_repository/clinic/commit/e738140cb7fef45b2eb742d4375cbb2ad17b3f66))
* **treatment-form:** adjust treatment fee form display using tabs ([306a641](https://gitlab.com/beautyworld_repository/clinic/commit/306a641b81c2e2925efccdb4c02570c0decc6fc4))
* **treatment-form:** fix store data ([4bf0b50](https://gitlab.com/beautyworld_repository/clinic/commit/4bf0b5039fa70e9a3b634376abdda0e3f7e6e59d))
* **treatment-form:** fix update data on edit state ([32d06c2](https://gitlab.com/beautyworld_repository/clinic/commit/32d06c2b6ae8cafc9e378d6c42b2a6444ec45495))
* **treatment-form:** fix update data on edit state ([1d17f7f](https://gitlab.com/beautyworld_repository/clinic/commit/1d17f7fcdaf282cd37773636f2a296b3c4e41ff6))
* **treatment-form:** set fee value ([ffe0b1b](https://gitlab.com/beautyworld_repository/clinic/commit/ffe0b1bc50ec60d927d91407279028a4711c9096))
* **treatment-material-usage:** fix autoload treatment material usage detail ([6b80056](https://gitlab.com/beautyworld_repository/clinic/commit/6b800565a01d7de4367ed673199b05a148fdde8c))
* **treatment-material-usage:** fix search product ([669d515](https://gitlab.com/beautyworld_repository/clinic/commit/669d515db5d0821cbcf5481f0239ac2cb5d0542c))
* **treatment-material-usage:** fix treatment material usages table ([1ddfb17](https://gitlab.com/beautyworld_repository/clinic/commit/1ddfb17dcd055b4b085c0c5dc52d9f8d600fb545))
* **treatment-material-usage:** show treatment by id ([f1ece6f](https://gitlab.com/beautyworld_repository/clinic/commit/f1ece6f1a851b92e91ae7ddef38b781e14b55cba))
* **treatment:** adjust treatment table field ([50e1e8e](https://gitlab.com/beautyworld_repository/clinic/commit/50e1e8e3fc7da9fdc399fe5057c995964477be57))
* **treatment:** fix add treatment material usage ([31bfeb5](https://gitlab.com/beautyworld_repository/clinic/commit/31bfeb545b552b016d4455be54c126cfe2141158))
* **treatment:** fix not showing price on product search ([418128f](https://gitlab.com/beautyworld_repository/clinic/commit/418128f1d46b5a22cf5a84372c6fbc491f2f3ad8))
* **treatment:** fix redirect to master treatments after save data ([6f973f6](https://gitlab.com/beautyworld_repository/clinic/commit/6f973f6bf13c6e617466317ee30062402a914b13))
* **treatment:** fix treatment seeder ([5677960](https://gitlab.com/beautyworld_repository/clinic/commit/5677960ae2b2936576125e8dbea97f67d42cd2df))
* **turn-over:** fix delete function ([0b5b7e7](https://gitlab.com/beautyworld_repository/clinic/commit/0b5b7e7c22cc5d967ee9a08779b85fbd35ae2b23))
* **turn-over:** fix delete function ([af7477d](https://gitlab.com/beautyworld_repository/clinic/commit/af7477de81e9e11e04280f3e97006dd8bcc2333d))
* **user-detail:** set unique code on user detail ([562e6d2](https://gitlab.com/beautyworld_repository/clinic/commit/562e6d206b5635fe9725fd810901e5dc9af2f9d0))
* **user-seeder:** add melinda and bu selvi to user seeder ([5d3a704](https://gitlab.com/beautyworld_repository/clinic/commit/5d3a704bf8f43d41d3568978aea4be144390f287))
* **user-seeder:** add melinda and bu selvi to user seeder ([d391608](https://gitlab.com/beautyworld_repository/clinic/commit/d391608b391d3f240c1c13e4d16e3fa284ce8ea0))


* **package-form:** update package-form ([6feb4a3](https://gitlab.com/beautyworld_repository/clinic/commit/6feb4a3c38cd6eef42fef802b2aaa029487f789c))
* **service-item:** rename to treatment_material_usages ([a57fcfa](https://gitlab.com/beautyworld_repository/clinic/commit/a57fcfab240fa692b8cf7c2aa426c89e5c6b7f84))
* **services-feature:** change service to treatment ([3e9ae2b](https://gitlab.com/beautyworld_repository/clinic/commit/3e9ae2b32424a60d70291d4dc0946fce32bd1448))
* **treatment-feature:** refactor ([9d13d56](https://gitlab.com/beautyworld_repository/clinic/commit/9d13d56bef1e5af7218325057b64e19e393c9831))
* **treatment-fee-form:** clear fee value ([04bd04b](https://gitlab.com/beautyworld_repository/clinic/commit/04bd04b9984e43e62d0428b10ab953d6fbf024e6))
* **treatment-form:** use compoenent for material usage, add sample event for Rp agains Percents ([7f5a6e4](https://gitlab.com/beautyworld_repository/clinic/commit/7f5a6e41a496f6a250e997ebafff3bedd62b2c84))
* **treatment:** refactor treatment ([c42a61f](https://gitlab.com/beautyworld_repository/clinic/commit/c42a61fabf01e0daf022e2ec7ebed69571e86b81))


### ⚠ BREAKING CHANGES

* **treatment:** all the module with services
* **subscriptions:** sales invoice
* **subscriptions:** sales invoice
* **package-form:** - sales invoice
* **treatment-fee-form:** - Sales Invoice
      - Package
      - Appointment
* **treatment-form:** - Sales Invoice
    - Package
    - Appointment
* **treatment-feature:** - Sales Invoice
        - Package
        - Appointment
* **service-item:** - Sales Invoice
      - Package
      - Appointment
* **treatment-feature:** - Sales Invoice
    - Package
    - Appointment
* **services-feature:** - Sales Invoice
  - Package
  - Appointment

### Features

* **commission-report-page:** add new layout for commision report page ([b03c115](https://gitlab.com/beautyworld_repository/clinic/commit/b03c1155b14a0637c671c30fc6e54f75b696a82d))
* **commission-report-page:** add new layout for commision report page ([09ec1f2](https://gitlab.com/beautyworld_repository/clinic/commit/09ec1f28f5341b5fee2bb6f0b1278b6a3e40767d))
* **commission-report-page:** add new layout for commision report page ([9c4b77b](https://gitlab.com/beautyworld_repository/clinic/commit/9c4b77b93e5fa0e3209d3ed33234614df0dc5fe8))
* **commission-report-page:** add new layout for commision report page ([ff39adc](https://gitlab.com/beautyworld_repository/clinic/commit/ff39adc407aedbb7633934a0d8e545ceecdd56bf))
* **commission-report-page:** add new layout for commision report page ([94fb9e6](https://gitlab.com/beautyworld_repository/clinic/commit/94fb9e6958e0e513f2c32143568a749121f3eafd))
* **commission-report-page:** add new layout for commision report page ([0427136](https://gitlab.com/beautyworld_repository/clinic/commit/042713696d80516bf0d0e00db29beb7aa9dc91f1))
* **commission-report-page:** add new layout for commision report page ([ea82b45](https://gitlab.com/beautyworld_repository/clinic/commit/ea82b459e1c00600e8af468f0c198f977a0f6582))
* **commission-report-page:** add new layout for commision report page ([ddccb4e](https://gitlab.com/beautyworld_repository/clinic/commit/ddccb4e342514f4a4f328056877287784c24b2cd))
* **marketplace:** initiate marketplace fee and material usage page ([c3ef699](https://gitlab.com/beautyworld_repository/clinic/commit/c3ef6998fd69414c2d6c3f69ee822192b37384aa))
* **product-fee:** add product fee page ([803acf5](https://gitlab.com/beautyworld_repository/clinic/commit/803acf5665774bf788d6cafae3ed7c95bfd294c9))
* **sales-invoice-api:** add sales invoice api ([80efd1c](https://gitlab.com/beautyworld_repository/clinic/commit/80efd1c728a0bbaae3e3460fe6a1dc52d3233273))
* **sales-invoice-api:** add sales invoice api ([85ca1de](https://gitlab.com/beautyworld_repository/clinic/commit/85ca1dea3580655675e56b22617ba572e43d186c))
* **subscription:** add seeder for subscription transaction ([c9079c5](https://gitlab.com/beautyworld_repository/clinic/commit/c9079c51e78ae4cb51821a946c677591ce0d118d))
* **subscription:** add seeder for subscription transaction ([139286c](https://gitlab.com/beautyworld_repository/clinic/commit/139286c378b8f9cd5bfa21d44d65f4a058d54fbd))
* **subscription:** add subscription seeder ([eff4ffd](https://gitlab.com/beautyworld_repository/clinic/commit/eff4ffdab18508f7391956c09e7dae4e8917029f))
* **subscription:** initiate subscriptions transaction ([82c9bef](https://gitlab.com/beautyworld_repository/clinic/commit/82c9bef137be0519fe6e67ec3df382d5593926ce))
* **subscriptions:** add new feature call subscriptions ([9f20a7a](https://gitlab.com/beautyworld_repository/clinic/commit/9f20a7a6e6712b3304126d17713056d9279a99ff))
* **subscriptions:** add new subscriptions on sales invoice form ([3a04c15](https://gitlab.com/beautyworld_repository/clinic/commit/3a04c1588a64bd350e9d9b5121af8aa6d081a285))
* **treatment:** add treatment fee and treatment material usages ([c30b8b1](https://gitlab.com/beautyworld_repository/clinic/commit/c30b8b1df502ac146045ecfd003b59b8179cbe04))
* **turn over report:** update turn over report ([d6c2aad](https://gitlab.com/beautyworld_repository/clinic/commit/d6c2aad1a365bef666202bcf2b96373ee47a590f))
* **turn-over:** add master turn over ([b8a771a](https://gitlab.com/beautyworld_repository/clinic/commit/b8a771af0b5a3036c2fd4ff469c5f634b33b5d7b))
* **turn-over:** add master turn over ([1cf8bad](https://gitlab.com/beautyworld_repository/clinic/commit/1cf8bad5bd5626f34ac72ce8c5718dab092101c8))


### Bug Fixes

* **appointment-form:** fix appointment form edit not save PIC ([a46d30c](https://gitlab.com/beautyworld_repository/clinic/commit/a46d30c551c40472bd41361ffb17f629698cfefc))
* **appointment-form:** fix appointment form edit not save PIC ([013c66a](https://gitlab.com/beautyworld_repository/clinic/commit/013c66a1da07cdbcafbfff5a78275746354af275))
* **appointment-form:** use livewire customer-search on appointment-form ([3a67739](https://gitlab.com/beautyworld_repository/clinic/commit/3a67739414fa259323680647bc7b599fbdff1d04))
* **category-subcategory:** fix category and subcategory seeder ([ec85353](https://gitlab.com/beautyworld_repository/clinic/commit/ec8535396a6f341c7019bde35b3a01a87db61ade))
* **customer-form:** fix error save customer ([1766939](https://gitlab.com/beautyworld_repository/clinic/commit/17669395c0da71323fb5550fadaa4ed0b4b0d428))
* **customer-form:** fix error save customer ([e66ce97](https://gitlab.com/beautyworld_repository/clinic/commit/e66ce972230c6a94f6d5909f8c31bfbd0b0df4c4))
* **customer-form:** fix error save customer ([1eb3b8a](https://gitlab.com/beautyworld_repository/clinic/commit/1eb3b8a77586b0e015ae3d80fa3f473394a9fea6))
* **data-redemption:** fix error redemption left ([83f853b](https://gitlab.com/beautyworld_repository/clinic/commit/83f853bc97c8bf1bb69acdec03e8440ad721de1d))
* **data-redemption:** fix error redemption left ([fd0f3a5](https://gitlab.com/beautyworld_repository/clinic/commit/fd0f3a5275c7f67fa911223e04c7187ca80c29ae))
* **helpers:** remove unnecessary role ([be6e14a](https://gitlab.com/beautyworld_repository/clinic/commit/be6e14a12b2d7656f1ee5f9df21241f7f77711b0))
* **marketplace-fee:** fix marketplace fee page ([5fae42b](https://gitlab.com/beautyworld_repository/clinic/commit/5fae42b28bc8eacf26db6fab8c02c001bd361798))
* **marketplace-fee:** fix marketplace fee page ([057d058](https://gitlab.com/beautyworld_repository/clinic/commit/057d0582be3f73b137008a8a6a797e8320a62ea1))
* **marketplace-treatment:** fix delete button on marketplace and treatment ([95228c1](https://gitlab.com/beautyworld_repository/clinic/commit/95228c1ffe4a5b4b79ccad2e0500e04d3c62e337))
* **marketplace-treatment:** fix error fee after change fee method on model ([26cbb99](https://gitlab.com/beautyworld_repository/clinic/commit/26cbb99a99ee012b15bbb8a7acadf61af644f626))
* **marketplace-treatment:** fix error treatment and marketplace fee ([4235cab](https://gitlab.com/beautyworld_repository/clinic/commit/4235cab381f9114964c26fb5ff7c0774503616f4))
* **marketplace-treatment:** fix treatment and marketplace fee after change the method name ([7414999](https://gitlab.com/beautyworld_repository/clinic/commit/74149991cc77b2a7a57c6cff84954d379a83231f))
* **marketplace-treatment:** fix treatment and marketplace fee after change the method name ([d254362](https://gitlab.com/beautyworld_repository/clinic/commit/d2543623ea2dde3e99513dd0d0173a5e34486580))
* **marketplace:** update marketplace follow the excel ([98b7748](https://gitlab.com/beautyworld_repository/clinic/commit/98b774868017c9845e69358b7a8aa1505ff5a324))
* **marketplace:** update marketplace follow the excel ([9573e45](https://gitlab.com/beautyworld_repository/clinic/commit/9573e45100006c58f11bafd745efee613bedcde8))
* **package-fee:** fix package fee store ([c6c1242](https://gitlab.com/beautyworld_repository/clinic/commit/c6c124263221077e12ddb4558497aabb64e66941))
* **package-form:** fix autoload package form ([44e0343](https://gitlab.com/beautyworld_repository/clinic/commit/44e03431403198f1e582a1eacccca335330246a5))
* **package-form:** fix crud package ([17fa968](https://gitlab.com/beautyworld_repository/clinic/commit/17fa9680a66e5cda7474f00ae0d72a635d950c9a))
* **package-form:** fix crud package ([e6a715f](https://gitlab.com/beautyworld_repository/clinic/commit/e6a715f9c30db549d094c642d02c685c9d73d7d0))
* **product and treatment:** fix category option allow null value ([01b0f2e](https://gitlab.com/beautyworld_repository/clinic/commit/01b0f2ecfa658978ee1861bbcdfb924e47e9bfbb))
* **product-detail:** fix layout fee ([d317cda](https://gitlab.com/beautyworld_repository/clinic/commit/d317cda681cf9b40a1a16e6d4af4479beec55a14))
* **product-detail:** fix layout fee ([5acb843](https://gitlab.com/beautyworld_repository/clinic/commit/5acb843c5d4f871d648efa77c38027752d94163a))
* **product-treatment:** fix category option allow null value ([17256ba](https://gitlab.com/beautyworld_repository/clinic/commit/17256ba71a83f8362901b58fef8729a1298a9895))
* **product-treatment:** fix category option allow null value ([7767729](https://gitlab.com/beautyworld_repository/clinic/commit/7767729a7f1403e60f9b215ce0d5ee5efeec4d35))
* **product:** refactor product ([2e76885](https://gitlab.com/beautyworld_repository/clinic/commit/2e768854a9be64593339344ae5481ac8993ec033))
* **receipt:** fix wrong date on receipt ([7fb95b3](https://gitlab.com/beautyworld_repository/clinic/commit/7fb95b34dd2016c1ea533074a3149beb84a2efb9))
* **receipt:** fix wrong date on receipt ([a26474c](https://gitlab.com/beautyworld_repository/clinic/commit/a26474c2de00eeb0954b4507510d42af50c0dbfc))
* **receipt:** remove tax ([e8d1868](https://gitlab.com/beautyworld_repository/clinic/commit/e8d1868d76463621a141f6c66942b68572c94190))
* **sales-invoice-table:** add print button on sales invoice table ([be9b894](https://gitlab.com/beautyworld_repository/clinic/commit/be9b894a778258b18b41b19e41ad72908d967830))
* **sales-invoice:** fix error sales invoice page ([8935efb](https://gitlab.com/beautyworld_repository/clinic/commit/8935efb8e0179706d45f977d5ac3972866efcf9a))
* **sales-invoice:** fix error sales invoice page ([60fbe6f](https://gitlab.com/beautyworld_repository/clinic/commit/60fbe6fca9f48b1c7ae49cc3f0f5d8934b23c07b))
* **sales-invoice:** fix error sales invoice page ([a78f46a](https://gitlab.com/beautyworld_repository/clinic/commit/a78f46a0bab800b3df106d3a1e7e55d3bface402))
* **sales-invoice:** fix sales invoice approval ([2e1dc31](https://gitlab.com/beautyworld_repository/clinic/commit/2e1dc3182d0fbca2dcf0b082eb58ede4b50a79b3))
* **sales-invoice:** remove tax ([e6b5098](https://gitlab.com/beautyworld_repository/clinic/commit/e6b5098a8240d9901261444f60b51a2c9cf2e7a6))
* **seeder:** fix category seeder and service seeder ([c78969b](https://gitlab.com/beautyworld_repository/clinic/commit/c78969b1a8a41e55ae20b57fa18352c3b0bf0cb1))
* **subscription-plan-item:** fix crud ([5208cf3](https://gitlab.com/beautyworld_repository/clinic/commit/5208cf329363876dc2cce837d3b622fbae4d9a2b))
* **subscription-plan:** fix hide subscription form on crud ([ac2dd4c](https://gitlab.com/beautyworld_repository/clinic/commit/ac2dd4cc86c30ca5a2d375a89d694cea879fa72e))
* **treatment-feature:** fix insert treatment fee ([e532d8a](https://gitlab.com/beautyworld_repository/clinic/commit/e532d8aa3c6a6b5d94e2c70cf0233d83858119b7))
* **treatment-feature:** show  swal alert when admin try to delete treatment ([9340b92](https://gitlab.com/beautyworld_repository/clinic/commit/9340b9291a5eb43f188bcc176cf077db25d10908))
* **treatment-fee-page:** fix layout fee treatment ([877e3f6](https://gitlab.com/beautyworld_repository/clinic/commit/877e3f6e939a4d8d220c3df7960a2eb9193d4e15))
* **treatment-fee-page:** fix layout fee treatment ([81c10b3](https://gitlab.com/beautyworld_repository/clinic/commit/81c10b35d9f9b25172ed5a8fde19ac5e0819abfa))
* **treatment-fee-page:** layout treatment fee header ([0aa62cb](https://gitlab.com/beautyworld_repository/clinic/commit/0aa62cb095fc55202ed7cbb1f6a4798ca91e7d96))
* **treatment-fee-page:** store treatment fee data ([809f998](https://gitlab.com/beautyworld_repository/clinic/commit/809f998202a187edb04c9e71c68c412e514d6e0d))
* **treatment-fee:** fix auto load fee ([e298d99](https://gitlab.com/beautyworld_repository/clinic/commit/e298d99350ead2999b5a0e67777dfcb7ce983d2a))
* **treatment-fee:** fix auto load fee ([9f53c65](https://gitlab.com/beautyworld_repository/clinic/commit/9f53c651e485cab74593f5a1055fbc7f0dd5c7e8))
* **treatment-fee:** fix auto load fee ([1f549f8](https://gitlab.com/beautyworld_repository/clinic/commit/1f549f8ace878d1c56c9c93242e21579def5dc20))
* **treatment-fee:** fix autoload treatment fee detail ([e738140](https://gitlab.com/beautyworld_repository/clinic/commit/e738140cb7fef45b2eb742d4375cbb2ad17b3f66))
* **treatment-form:** adjust treatment fee form display using tabs ([306a641](https://gitlab.com/beautyworld_repository/clinic/commit/306a641b81c2e2925efccdb4c02570c0decc6fc4))
* **treatment-form:** fix store data ([4bf0b50](https://gitlab.com/beautyworld_repository/clinic/commit/4bf0b5039fa70e9a3b634376abdda0e3f7e6e59d))
* **treatment-form:** fix update data on edit state ([32d06c2](https://gitlab.com/beautyworld_repository/clinic/commit/32d06c2b6ae8cafc9e378d6c42b2a6444ec45495))
* **treatment-form:** fix update data on edit state ([1d17f7f](https://gitlab.com/beautyworld_repository/clinic/commit/1d17f7fcdaf282cd37773636f2a296b3c4e41ff6))
* **treatment-form:** set fee value ([ffe0b1b](https://gitlab.com/beautyworld_repository/clinic/commit/ffe0b1bc50ec60d927d91407279028a4711c9096))
* **treatment-material-usage:** fix autoload treatment material usage detail ([6b80056](https://gitlab.com/beautyworld_repository/clinic/commit/6b800565a01d7de4367ed673199b05a148fdde8c))
* **treatment-material-usage:** fix search product ([669d515](https://gitlab.com/beautyworld_repository/clinic/commit/669d515db5d0821cbcf5481f0239ac2cb5d0542c))
* **treatment-material-usage:** fix treatment material usages table ([1ddfb17](https://gitlab.com/beautyworld_repository/clinic/commit/1ddfb17dcd055b4b085c0c5dc52d9f8d600fb545))
* **treatment-material-usage:** show treatment by id ([f1ece6f](https://gitlab.com/beautyworld_repository/clinic/commit/f1ece6f1a851b92e91ae7ddef38b781e14b55cba))
* **treatment:** adjust treatment table field ([50e1e8e](https://gitlab.com/beautyworld_repository/clinic/commit/50e1e8e3fc7da9fdc399fe5057c995964477be57))
* **treatment:** fix add treatment material usage ([31bfeb5](https://gitlab.com/beautyworld_repository/clinic/commit/31bfeb545b552b016d4455be54c126cfe2141158))
* **treatment:** fix not showing price on product search ([418128f](https://gitlab.com/beautyworld_repository/clinic/commit/418128f1d46b5a22cf5a84372c6fbc491f2f3ad8))
* **treatment:** fix redirect to master treatments after save data ([6f973f6](https://gitlab.com/beautyworld_repository/clinic/commit/6f973f6bf13c6e617466317ee30062402a914b13))
* **treatment:** fix treatment seeder ([5677960](https://gitlab.com/beautyworld_repository/clinic/commit/5677960ae2b2936576125e8dbea97f67d42cd2df))
* **turn-over:** fix delete function ([0b5b7e7](https://gitlab.com/beautyworld_repository/clinic/commit/0b5b7e7c22cc5d967ee9a08779b85fbd35ae2b23))
* **turn-over:** fix delete function ([af7477d](https://gitlab.com/beautyworld_repository/clinic/commit/af7477de81e9e11e04280f3e97006dd8bcc2333d))
* **user-detail:** set unique code on user detail ([562e6d2](https://gitlab.com/beautyworld_repository/clinic/commit/562e6d206b5635fe9725fd810901e5dc9af2f9d0))
* **user-seeder:** add melinda and bu selvi to user seeder ([5d3a704](https://gitlab.com/beautyworld_repository/clinic/commit/5d3a704bf8f43d41d3568978aea4be144390f287))
* **user-seeder:** add melinda and bu selvi to user seeder ([d391608](https://gitlab.com/beautyworld_repository/clinic/commit/d391608b391d3f240c1c13e4d16e3fa284ce8ea0))


* **package-form:** update package-form ([6feb4a3](https://gitlab.com/beautyworld_repository/clinic/commit/6feb4a3c38cd6eef42fef802b2aaa029487f789c))
* **service-item:** rename to treatment_material_usages ([a57fcfa](https://gitlab.com/beautyworld_repository/clinic/commit/a57fcfab240fa692b8cf7c2aa426c89e5c6b7f84))
* **services-feature:** change service to treatment ([3e9ae2b](https://gitlab.com/beautyworld_repository/clinic/commit/3e9ae2b32424a60d70291d4dc0946fce32bd1448))
* **treatment-feature:** refactor ([9d13d56](https://gitlab.com/beautyworld_repository/clinic/commit/9d13d56bef1e5af7218325057b64e19e393c9831))
* **treatment-fee-form:** clear fee value ([04bd04b](https://gitlab.com/beautyworld_repository/clinic/commit/04bd04b9984e43e62d0428b10ab953d6fbf024e6))
* **treatment-form:** use compoenent for material usage, add sample event for Rp agains Percents ([7f5a6e4](https://gitlab.com/beautyworld_repository/clinic/commit/7f5a6e41a496f6a250e997ebafff3bedd62b2c84))
* **treatment:** refactor treatment ([c42a61f](https://gitlab.com/beautyworld_repository/clinic/commit/c42a61fabf01e0daf022e2ec7ebed69571e86b81))

### [2.3.2](https://gitlab.com/beautyworld_repository/clinic/compare/v2.3.1...v2.3.2) (2024-02-03)


### Bug Fixes

* **appointment-form:** fix -- Choose -- error when input appointment ([b153631](https://gitlab.com/beautyworld_repository/clinic/commit/b153631cb4c58ce1762398c1d7657e21218d24a6))
* **appointment-sidebar:** fix error time start and room ([65f8683](https://gitlab.com/beautyworld_repository/clinic/commit/65f86835a3d9efcb6358c557fa8262892b848785))
* **customer-transaction-table:** show price by service on customer transaction table ([223444f](https://gitlab.com/beautyworld_repository/clinic/commit/223444f2f2661a299619f03b580922abefa8d0f3))
* **data-redemption-page:** fix error customer data redemption ([298a895](https://gitlab.com/beautyworld_repository/clinic/commit/298a895f77e91d61de7a0d1405396e00ca67c74a))
* **product-form:** fix updated category ([26d3245](https://gitlab.com/beautyworld_repository/clinic/commit/26d3245d547eda30a1af9ce82c58e5fa17f9d6ca))
* **report-module:** fix auto open menu on report and add commission and turnover menu ([c7492ff](https://gitlab.com/beautyworld_repository/clinic/commit/c7492ffaaa25d25e41355dbafc1cdae892cc2fe1))
* **report-sales-detail:** add payment type ([7d3197e](https://gitlab.com/beautyworld_repository/clinic/commit/7d3197e0982eb75844f2f39db0844823f953134d))
* **report-sales-detail:** fix customer price on report ([09019dc](https://gitlab.com/beautyworld_repository/clinic/commit/09019dc26ef15ae603b1e605242b0368b380f046))
* **report-sales-detail:** fix customer price on report ([f6f7740](https://gitlab.com/beautyworld_repository/clinic/commit/f6f774019851b40c204b07f588dbfa570372f082))
* **report-sales-detail:** fix customer price on report ([fd22efe](https://gitlab.com/beautyworld_repository/clinic/commit/fd22efee18b61c7d8fce1d4220cd92070917e439))
* **report-sales-detail:** fix customer price on report ([39c8094](https://gitlab.com/beautyworld_repository/clinic/commit/39c8094f0e72c0fabd34202408a473bf88f87a75))
* **report-sales-detail:** fix double data export ([b8496ae](https://gitlab.com/beautyworld_repository/clinic/commit/b8496ae04dc17c284d4ca080f59143f3cd66529a))
* **report-sales-detail:** seperate PIC into column ([e5c279d](https://gitlab.com/beautyworld_repository/clinic/commit/e5c279d722159376f05d6637232c0f18f665dbe3))
* **sales-invoice-form:** fix apply discount on product, package, service, marketplace ([60b4800](https://gitlab.com/beautyworld_repository/clinic/commit/60b480032eb95c49f1ac9b6b4eb850c964f7094b))
* **sales-invoice-form:** remove checkout button if there are discount by invoice ([d37b4a6](https://gitlab.com/beautyworld_repository/clinic/commit/d37b4a66d8afa310c103f315fb246cb114cf6e55))
* **sales-invoice-form:** remove tax from form ([07178cd](https://gitlab.com/beautyworld_repository/clinic/commit/07178cd17e03206c46b5d7e9c1d98e6c9b8d50bd))
* **sales-invoice-item-table:** fix remove item on sales invoice item table update subtotal and grandtotal ([894efa1](https://gitlab.com/beautyworld_repository/clinic/commit/894efa1f50554e6b31d9ea003dba5545c27e0e6b))

### [2.3.1](https://gitlab.com/beautyworld_repository/clinic/compare/v2.3.0...v2.3.1) (2024-01-10)

## [2.3.0](https://gitlab.com/beautyworld_repository/clinic/compare/v2.2.1...v2.3.0) (2024-01-09)


### Features

* **dashboard-page:** initiate dashboard page ([346e61b](https://gitlab.com/beautyworld_repository/clinic/commit/346e61bdbf0f53ce4696d4144eeeda23685ab131))


### Bug Fixes

* **today-sales:** fix cash drawer sum total and remove discount and discount item ([9ceaa74](https://gitlab.com/beautyworld_repository/clinic/commit/9ceaa74c89e729e784614e4f9f7889c0782b680f))

### [2.2.1](https://gitlab.com/beautyworld_repository/clinic/compare/v2.1.8...v2.2.1) (2023-12-29)


### Features

* **log-viewer:** update logviewer 3 ([00baec4](https://gitlab.com/beautyworld_repository/clinic/commit/00baec48b1bf597d10eada50789d6d180d92d114))


### Bug Fixes

* **appointment:** fix redemption empty module_id ([89d3986](https://gitlab.com/beautyworld_repository/clinic/commit/89d3986ae33405f8065dd45f5740a12abb0f3cc6))
* **customer-table:** fix wrong id customer ([edf381d](https://gitlab.com/beautyworld_repository/clinic/commit/edf381da885c643c8bbea35d37693f395805a60e))
* **sales-invoice-form:** fix button checkout error when discount found ([515d757](https://gitlab.com/beautyworld_repository/clinic/commit/515d75793bf8308b98c242e0c99fef1b63a0e287))
* **sales-invoice-form:** fix checkout button ([8a8709f](https://gitlab.com/beautyworld_repository/clinic/commit/8a8709ff1c75d3e393c0c079d89609d324d572cf))
* **sales-invoice-form:** fix checkout button ([cf9b4e0](https://gitlab.com/beautyworld_repository/clinic/commit/cf9b4e0c90ce41aec28df1700a3e859932a545f3))
* **sales-invoice-form:** fix code and name display ([6869d9c](https://gitlab.com/beautyworld_repository/clinic/commit/6869d9c0cce2deb90c6b28cd95bc99e368ee018a))
* **sales-invoice-form:** remove checkout button if sales Invoice exists ([afdb10f](https://gitlab.com/beautyworld_repository/clinic/commit/afdb10f76ffc556738ba9a095a1c41057f37775c))
* **sales-invoice-form:** show PIC on package and product ([b7b8e2c](https://gitlab.com/beautyworld_repository/clinic/commit/b7b8e2c58948fe5127c4778c63444320f64dc8ab))
* **sales-invoice-form:** write approval data ([c116ce9](https://gitlab.com/beautyworld_repository/clinic/commit/c116ce96cd23932f193a5fe9b45b4409c8a4ad87))

### [2.1.8](https://gitlab.com/beautyworld_repository/clinic/compare/v2.1.7...v2.1.8) (2023-12-25)

### [2.1.7](https://gitlab.com/beautyworld_repository/clinic/compare/v2.1.6...v2.1.7) (2023-12-24)


### ⚠ BREAKING CHANGES

* **sales-invoice-form:** potentially check appointment form, sales invoice form, and purchase order
* **product-search:** potentially check appointment form, sales invoice form, and purchase order
* **sales-invoice-form:** potentially check appointment form, sales invoice form, and purchase order
* **market-place-search:** potentially check appointment form, sales invoice form, and purchase order
* **product-search:** potentially check appointment form, sales invoice form, and purchase order
* **sales-invoice:** potentially check appointment form and sales invoice form
* **sales-invoice-search:** potentially check appointment form and sales invoice form
* **service-search:** potentially check appointment form and sales invoice form
* **package-search:** potentially check appointment form and sales invoice form
* **service-search:** potentially check appointment form and sales invoice form
* **marketplace-search:** potentially check appointment form and sales invoice form
* **marketplace-search:** potentially check appointment form and sales invoice form
* **marketplace-search:** potentially check appointment form and sales invoice form
* **customer-search:** potentially check appointment form and sales invoice form
* **room-search:** potentially check appointment form and sales invoice form
* **customer-search:** potentially check appointment form and sales invoice form
* **marketplace-search:** potentially check appointment form and sales invoice form
* **service-search:** potetianly check appointment form and sales invoice form
* **customer-search:** potetianly check appointment form

### Bug Fixes

* **sales-invoice-form:** refactor using jetbrain ai ([cdeb354](https://gitlab.com/beautyworld_repository/clinic/commit/cdeb354982e9c9b2538b0163dea7e19110927536))
* **sales-invoice-form:** refactor using jetbrain ai ([716925a](https://gitlab.com/beautyworld_repository/clinic/commit/716925af5b07e9c5c6737f2008490f796228f70b))


* **customer-search:** add clear customerSearch if clicked ([f778dcb](https://gitlab.com/beautyworld_repository/clinic/commit/f778dcb9ce0b18a6e3de76527cbf092deb7bd720))
* **customer-search:** update customer search ([7050375](https://gitlab.com/beautyworld_repository/clinic/commit/7050375cb29d58bf4099f3a596126a6d7d1afc55))
* **customer-search:** update customer search change method setSelectedCustomer ([05e3d48](https://gitlab.com/beautyworld_repository/clinic/commit/05e3d484bc02e0e414b248d50f43278d0b20979f))
* **market-place-search:** fix prevent select marketplace after add service ([ba462e1](https://gitlab.com/beautyworld_repository/clinic/commit/ba462e17dd057e11a7fc48b6014b498a61e330a0))
* **marketplace-search:** clear value on click marketPlaceSearch ([c55d84f](https://gitlab.com/beautyworld_repository/clinic/commit/c55d84f5f7ea853719c252f08f4ff060d7987f27))
* **marketplace-search:** update market place search ([46c407c](https://gitlab.com/beautyworld_repository/clinic/commit/46c407cc4e828dc5df941664c8134e2f3be87e3d))
* **marketplace-search:** update marketplace search ([c46a928](https://gitlab.com/beautyworld_repository/clinic/commit/c46a928c374e549d5b7f2a26e6ce567348c4b8a8))
* **marketplace-search:** update sales invoice form ([d2a29a8](https://gitlab.com/beautyworld_repository/clinic/commit/d2a29a8eaf74cbc010b4bdde0b5ea664cabdd4f7))
* **package-search:** extract package serach into livewire ocmponent ([b3b656c](https://gitlab.com/beautyworld_repository/clinic/commit/b3b656c8757b5f62db35e16b931d08c1436e228a))
* **product-search:** add product search required ([3d40dd4](https://gitlab.com/beautyworld_repository/clinic/commit/3d40dd4ae32d7821c625988ee7a9563f57a92584))
* **product-search:** convert to component on product search ([12b6269](https://gitlab.com/beautyworld_repository/clinic/commit/12b62693546a0d892cd2c4cac4b2d2fdd19bf1e7))
* **room-search:** update room search ([ca7013e](https://gitlab.com/beautyworld_repository/clinic/commit/ca7013ef94b86f409710852158667516540e4d64))
* **sales-invoice-form:** add clear items anda clear sales invoice form table ([655a3bd](https://gitlab.com/beautyworld_repository/clinic/commit/655a3bdb215c15d9c19a906504d09deb13d8091b))
* **sales-invoice-form:** refactor ([2ff7c9d](https://gitlab.com/beautyworld_repository/clinic/commit/2ff7c9dc9bbd30b1f0c194a699e0e2ad706c4c60))
* **sales-invoice-search:** rename feeUser to PersonInCharge ([88212b7](https://gitlab.com/beautyworld_repository/clinic/commit/88212b71f5ea5f2ea15b61874205a3c95e7c50b2))
* **sales-invoice:** fix marketplace can add item if no room selected and PIC selected ([698b440](https://gitlab.com/beautyworld_repository/clinic/commit/698b4404259b01521eb74ad70f0df5e829fc7c44))
* **service-search:** clear value on click serviceSearch ([a49504d](https://gitlab.com/beautyworld_repository/clinic/commit/a49504dcaad8086c98f7f0c101f1264f5bd630dc))
* **service-search:** make sure service show notification if value === null or "-- Choose --" ([7994403](https://gitlab.com/beautyworld_repository/clinic/commit/799440302664449d5cc29de9918dc4b815b8f71e))
* **service-search:** update service search ([0631025](https://gitlab.com/beautyworld_repository/clinic/commit/063102578554797c9bb710462fa8a0896831358a))

### [2.1.6](https://gitlab.com/beautyworld_repository/clinic/compare/v2.1.5...v2.1.6) (2023-12-04)

### ⚠ BREAKING CHANGES

* **sales-invoice-form:** potentially check appointment form, sales invoice form, and purchase order
* **product-search:** potentially check appointment form, sales invoice form, and purchase order
* **sales-invoice-form:** potentially check appointment form, sales invoice form, and purchase order
* **market-place-search:** potentially check appointment form, sales invoice form, and purchase order
* **product-search:** potentially check appointment form, sales invoice form, and purchase order
* **sales-invoice:** potentially check appointment form and sales invoice form
* **sales-invoice-search:** potentially check appointment form and sales invoice form
* **service-search:** potentially check appointment form and sales invoice form
* **package-search:** potentially check appointment form and sales invoice form
* **service-search:** potentially check appointment form and sales invoice form
* **marketplace-search:** potentially check appointment form and sales invoice form
* **marketplace-search:** potentially check appointment form and sales invoice form
* **marketplace-search:** potentially check appointment form and sales invoice form
* **customer-search:** potentially check appointment form and sales invoice form
* **room-search:** potentially check appointment form and sales invoice form
* **customer-search:** potentially check appointment form and sales invoice form
* **marketplace-search:** potentially check appointment form and sales invoice form
* **service-search:** potetianly check appointment form and sales invoice form
* **customer-search:** potetianly check appointment form

### Bug Fixes

* **api-user:** tidy up ([b115b4e](https://gitlab.com/beautyworld_repository/clinic/commit/b115b4ed8f05137fdeac3f0a90929ec646381d63))
* **sales-invoice-form:** fix bug required int value on getSalesInvoiceById ([80f427f](https://gitlab.com/beautyworld_repository/clinic/commit/80f427f2341cdd675cc460e4f71533737b4430b1))
* **sales-invoice-form:** fix error pick customer ([d561985](https://gitlab.com/beautyworld_repository/clinic/commit/d5619859bd909c516ec482d576e697eff25a5122))
* **sales-invoice-form:** not write transaction status on user_transactions table ([4034f50](https://gitlab.com/beautyworld_repository/clinic/commit/4034f503104400acd7dc7a4bc9d2ae9de154f7c4))
* **sales-invoice-tool:** add some event filter ([09ec10e](https://gitlab.com/beautyworld_repository/clinic/commit/09ec10ef8926355a491465adee41d3cab4838027))


* **customer-search:** add clear customerSearch if clicked ([f778dcb](https://gitlab.com/beautyworld_repository/clinic/commit/f778dcb9ce0b18a6e3de76527cbf092deb7bd720))
* **customer-search:** update customer search ([7050375](https://gitlab.com/beautyworld_repository/clinic/commit/7050375cb29d58bf4099f3a596126a6d7d1afc55))
* **customer-search:** update customer search change method setSelectedCustomer ([05e3d48](https://gitlab.com/beautyworld_repository/clinic/commit/05e3d484bc02e0e414b248d50f43278d0b20979f))
* **market-place-search:** fix prevent select marketplace after add service ([ba462e1](https://gitlab.com/beautyworld_repository/clinic/commit/ba462e17dd057e11a7fc48b6014b498a61e330a0))
* **marketplace-search:** clear value on click marketPlaceSearch ([c55d84f](https://gitlab.com/beautyworld_repository/clinic/commit/c55d84f5f7ea853719c252f08f4ff060d7987f27))
* **marketplace-search:** update market place search ([46c407c](https://gitlab.com/beautyworld_repository/clinic/commit/46c407cc4e828dc5df941664c8134e2f3be87e3d))
* **marketplace-search:** update marketplace search ([c46a928](https://gitlab.com/beautyworld_repository/clinic/commit/c46a928c374e549d5b7f2a26e6ce567348c4b8a8))
* **marketplace-search:** update sales invoice form ([d2a29a8](https://gitlab.com/beautyworld_repository/clinic/commit/d2a29a8eaf74cbc010b4bdde0b5ea664cabdd4f7))
* **package-search:** extract package serach into livewire ocmponent ([b3b656c](https://gitlab.com/beautyworld_repository/clinic/commit/b3b656c8757b5f62db35e16b931d08c1436e228a))
* **product-search:** add product search required ([3d40dd4](https://gitlab.com/beautyworld_repository/clinic/commit/3d40dd4ae32d7821c625988ee7a9563f57a92584))
* **product-search:** convert to component on product search ([12b6269](https://gitlab.com/beautyworld_repository/clinic/commit/12b62693546a0d892cd2c4cac4b2d2fdd19bf1e7))
* **room-search:** update room search ([ca7013e](https://gitlab.com/beautyworld_repository/clinic/commit/ca7013ef94b86f409710852158667516540e4d64))
* **sales-invoice-form:** add clear items anda clear sales invoice form table ([655a3bd](https://gitlab.com/beautyworld_repository/clinic/commit/655a3bdb215c15d9c19a906504d09deb13d8091b))
* **sales-invoice-form:** refactor ([2ff7c9d](https://gitlab.com/beautyworld_repository/clinic/commit/2ff7c9dc9bbd30b1f0c194a699e0e2ad706c4c60))
* **sales-invoice-search:** rename feeUser to PersonInCharge ([88212b7](https://gitlab.com/beautyworld_repository/clinic/commit/88212b71f5ea5f2ea15b61874205a3c95e7c50b2))
* **sales-invoice:** fix marketplace can add item if no room selected and PIC selected ([698b440](https://gitlab.com/beautyworld_repository/clinic/commit/698b4404259b01521eb74ad70f0df5e829fc7c44))
* **service-search:** clear value on click serviceSearch ([a49504d](https://gitlab.com/beautyworld_repository/clinic/commit/a49504dcaad8086c98f7f0c101f1264f5bd630dc))
* **service-search:** make sure service show notification if value === null or "-- Choose --" ([7994403](https://gitlab.com/beautyworld_repository/clinic/commit/799440302664449d5cc29de9918dc4b815b8f71e))
* **service-search:** update service search ([0631025](https://gitlab.com/beautyworld_repository/clinic/commit/063102578554797c9bb710462fa8a0896831358a))

### [2.1.5](https://gitlab.com/beautyworld_repository/clinic/compare/v2.1.4...v2.1.5) (2023-11-27)


### Features

* **warehouse-api:** initiate warehouse api ([1c784a7](https://gitlab.com/beautyworld_repository/clinic/commit/1c784a7e31783d5f3426a4c6c59e511c83f8a69c))
* **warehouse-test:** initiate warehouse test ([e546e90](https://gitlab.com/beautyworld_repository/clinic/commit/e546e90ae38bad2f6f804ea849200d0071ae5953))


### Bug Fixes

* **appointment-search:** fix autoload appointment search ([d888603](https://gitlab.com/beautyworld_repository/clinic/commit/d8886030c59c4017c961d132f46dca2bef7373a9))

### [2.1.4](https://gitlab.com/beautyworld_repository/clinic/compare/v2.1.3...v2.1.4) (2023-11-27)


### Bug Fixes

* **appointment-form:** fix bug getCompanyId ([a2500b4](https://gitlab.com/beautyworld_repository/clinic/commit/a2500b46f3d84226e0e9e300481da4f413877694))
* **appointment-form:** fix bug wrong customer selected ([56bdb00](https://gitlab.com/beautyworld_repository/clinic/commit/56bdb0057652e7eae6bf0c6880fe6e01a6e03aa5))
* **appointment-search:** fix bug getCompanyId ([4e089f0](https://gitlab.com/beautyworld_repository/clinic/commit/4e089f074e49fea684095921c3701c5b7691611a))
* **appointment-search:** fix bug getCompanyId ([259118b](https://gitlab.com/beautyworld_repository/clinic/commit/259118b1a970506bc360df1fd6edd41a730a0dd4))
* **migration:** fix / on starting file ([789e30c](https://gitlab.com/beautyworld_repository/clinic/commit/789e30ce41c98c0c2c624f2df5009f0256aee346))
* **print:** fix print ([729d226](https://gitlab.com/beautyworld_repository/clinic/commit/729d2267868074363b65e50e1207035c1ede28fe))
* **product:** fix bug parameter int required ([8ee99de](https://gitlab.com/beautyworld_repository/clinic/commit/8ee99de01bedc64b2c1cee76719598101b2ad13a))
* **sales-invoice-form:** add service after get data from appointment ([92645b2](https://gitlab.com/beautyworld_repository/clinic/commit/92645b215a6a6ad62f11e2a4eb3f850bf66e546e))
* **sales-invoice-form:** add service after get data from appointment ([4a1e6f0](https://gitlab.com/beautyworld_repository/clinic/commit/4a1e6f0412a2db605884f9075e6ba9ab2bf902cf))
* **sales-invoice-form:** fix edit sales-invoice ([67d22ea](https://gitlab.com/beautyworld_repository/clinic/commit/67d22ea66318eda7f3b6aa029c7608ea8dfc0e94))
* **sales-invoice-form:** fix release ([118a73d](https://gitlab.com/beautyworld_repository/clinic/commit/118a73d1c87fc68f12dced846d01e20bc769b24d))
* **sales-invoice-form:** fix release ([263fa8e](https://gitlab.com/beautyworld_repository/clinic/commit/263fa8ec5b16a9dc3ac269c7f8328cc139ac98d0))
* **sales-invoice-form:** fix update appointment on sales invoice ([cb607dc](https://gitlab.com/beautyworld_repository/clinic/commit/cb607dc1fcc5b9aaad787b11aaef32ed0c376774))

### [2.1.3](https://gitlab.com/beautyworld_repository/clinic/compare/v2.1.2...v2.1.3) (2023-11-26)


### Features

* **register-company:** make register company works ([ce76a6d](https://gitlab.com/beautyworld_repository/clinic/commit/ce76a6d3fb9f234c0308ec3d9dfb6344abcd6dea))


### Bug Fixes

* **customer-transaction-table:** fix bug getInvoiceById ([5493b12](https://gitlab.com/beautyworld_repository/clinic/commit/5493b12459456c24a9e5301813e2cc943584eb1d))
* **customer-transaction-table:** fix bug getInvoiceById ([45ecb33](https://gitlab.com/beautyworld_repository/clinic/commit/45ecb333c05c658e5597a00511df96859ec527f4))

### [2.1.2](https://gitlab.com/beautyworld_repository/clinic/compare/v2.1.1...v2.1.2) (2023-11-24)


### Bug Fixes

* **province-seeder:** bug province seeder need unique value ([0655c75](https://gitlab.com/beautyworld_repository/clinic/commit/0655c75d695903347104ef2042df58d4c12e2db8))
* **province-seeder:** bug province seeder need unique value ([6c59202](https://gitlab.com/beautyworld_repository/clinic/commit/6c59202d672314d2b3fb7790668e6597b7849a72))

### [2.1.1](https://gitlab.com/beautyworld_repository/clinic/compare/v2.1.0...v2.1.1) (2023-11-24)


### Bug Fixes

* **appointment-form:** hide new customer checkbox ([a5b8c04](https://gitlab.com/beautyworld_repository/clinic/commit/a5b8c04961f6f75cf5a4c0841ba837b4f8ad7582))
* **appointment-form:** prevent add item if no redemption selected ([adfbc95](https://gitlab.com/beautyworld_repository/clinic/commit/adfbc9553ac3b4ba15a00afbccfcbf0a3edb2f12))
* **sales-invoice-form:** allow save without input payment ([bdf5154](https://gitlab.com/beautyworld_repository/clinic/commit/bdf5154cb33b7d93f5522cb62e89908ae8c6e45e))
* **sales-invoice-form:** fix bug no selected customer ([db66dc1](https://gitlab.com/beautyworld_repository/clinic/commit/db66dc1e7b5ff315a6c9dddf42c7cf78934fb2d2))
* **sales-invoice-form:** fix display transactions item table ([5ac11e3](https://gitlab.com/beautyworld_repository/clinic/commit/5ac11e3f0875df25aa7dec4c6df8709fdf7cf0d4))
* **sales-invoice-form:** set default invoice No and Invoice date ([9d9691d](https://gitlab.com/beautyworld_repository/clinic/commit/9d9691d922d2b1261fc8d07b387fd141ff8fbd2d))
* **sales-invoice-table:** remove button void,reopen,undovoid and show date picker for all user ([416bba3](https://gitlab.com/beautyworld_repository/clinic/commit/416bba33b780a55894d5338b24ed3718e6e52ec9))

## [2.1.0](https://gitlab.com/beautyworld_repository/clinic/compare/v2.0.1...v2.1.0) (2023-11-24)


### Features

* **register-company:** initiate register company ([a7e6d2b](https://gitlab.com/beautyworld_repository/clinic/commit/a7e6d2b077a44143dbcb623301c7e15fdc387349))
* **register-company:** initiate register company ([3e30fda](https://gitlab.com/beautyworld_repository/clinic/commit/3e30fda7c3b274d1ea4d28a94cd683ab0c5de290))


### Bug Fixes

* **navbar-top:** fix null first name ([e5e1d0d](https://gitlab.com/beautyworld_repository/clinic/commit/e5e1d0d1faa790fe1344ac0aec738267e1a2218a))
* **navbar-top:** fix null first name ([d51491a](https://gitlab.com/beautyworld_repository/clinic/commit/d51491a867d0d3c1d0e12ddeb8d902f150d6769a))
* **package-table:** fix bug categoryOptions ([682d7a0](https://gitlab.com/beautyworld_repository/clinic/commit/682d7a0396ec36cb834dedce82a3aaf781e11800))
* **sales-detail-report:** fix bug select report buy customer ([d7ab79f](https://gitlab.com/beautyworld_repository/clinic/commit/d7ab79ff36ff6988a4450ba45fa45bae12a92686))
* **sales-detail-report:** fix bug select report buy customer ([d015e47](https://gitlab.com/beautyworld_repository/clinic/commit/d015e47ee3af6d89aa0bbaa56cf332bd6fb1a0a4))
* **sales-detail-report:** fix download button ([3d7bfb6](https://gitlab.com/beautyworld_repository/clinic/commit/3d7bfb6fcf8222d1b67aed5ebc9cd54e6e09ebbd))
* **sales-invoice-item-modal:** fix button approve not showing ([8f0104a](https://gitlab.com/beautyworld_repository/clinic/commit/8f0104a9f7de235abbfc5c002680149a11492beb))
* **sales-summary-report:** fix bug sales summary report change date ([c422442](https://gitlab.com/beautyworld_repository/clinic/commit/c4224424a17ab0c19a2e672dee9b47b10cc99c4c))
* **sales-summary-report:** fix button download sales summary report ([70693c0](https://gitlab.com/beautyworld_repository/clinic/commit/70693c01485e9a7893dd8ca0dcd3b6e12042cc05))

### [2.0.1](https://gitlab.com/beautyworld_repository/clinic/compare/v2.0.0...v2.0.1) (2023-11-15)


### Features

* **sales-invoice-tool:** add get sales invoice details by sales invoice number ([271e39e](https://gitlab.com/beautyworld_repository/clinic/commit/271e39ed9157adefda2c4a7de1b6809ff96c087c))


### Bug Fixes

* **sales-summary-report:** fix download sales summary report ([302dc31](https://gitlab.com/beautyworld_repository/clinic/commit/302dc3144c48c2b4144a1e68c03a9069c4bed0a4))
* **sales-summary-report:** fix download sales summary report using params ([6fef319](https://gitlab.com/beautyworld_repository/clinic/commit/6fef3197211728e10e31eba14d12447a035f6b27))

## [2.0.0](https://gitlab.com/beautyworld_repository/clinic/compare/v1.9.0...v2.0.0) (2023-11-14)


### Bug Fixes

* **sales-invoice-form:** update layout receipt && fix reOpen function ([3c5103b](https://gitlab.com/beautyworld_repository/clinic/commit/3c5103be88c2a2761a2f35efd42453aa71c48e54))
* **today-sales-report:** add date picker ([83d6a37](https://gitlab.com/beautyworld_repository/clinic/commit/83d6a3782d091e84e6723008f3f001789d50a4e6))

## [1.9.0](https://gitlab.com/beautyworld_repository/clinic/compare/v1.8.9...v1.9.0) (2023-11-09)


### Bug Fixes

* **navbar-menu:** typo good receive link ([75840fa](https://gitlab.com/beautyworld_repository/clinic/commit/75840fa4b2ef73bcfe6849549afb45636960d6f2))
* **purchase-order-form:** fix bug update selected vendor ([98b921e](https://gitlab.com/beautyworld_repository/clinic/commit/98b921e6ab8d8b2a12bc83da50cbb5c64c934e35))
* **purchase-order-form:** fix bug update selected vendor ([ee2148f](https://gitlab.com/beautyworld_repository/clinic/commit/ee2148fff475dddee79005ac416fe6632273efd0))
* **purchase-order-form:** fix bug update selected vendor ([bdda597](https://gitlab.com/beautyworld_repository/clinic/commit/bdda597f436f363838ff97b30b889f49761f4621))
* **purchase-order-form:** fix bug update selected vendor ([6fbde5f](https://gitlab.com/beautyworld_repository/clinic/commit/6fbde5f1053f8b62d09c845bf18de04a15e36994))
* **receipt:** add filter where null and company id ([dc22cad](https://gitlab.com/beautyworld_repository/clinic/commit/dc22cad6ecd87f678c2c6048d94ce0041f3d47e7))

### [1.8.8](https://gitlab.com/beautyworld_repository/clinic/compare/v1.8.7...v1.8.8) (2023-11-07)

### [1.8.8](https://gitlab.com/beautyworld_repository/clinic/compare/v1.8.7...v1.8.8) (2023-11-07)


### Bug Fixes

* **sales-invoice-form:** add subtotal ([8bf89fd](https://gitlab.com/beautyworld_repository/clinic/commit/8bf89fdd2bf8c6702185a49d310390899725659d))

### [1.8.7](https://gitlab.com/beautyworld_repository/clinic/compare/v1.8.6...v1.8.7) (2023-11-07)


### Bug Fixes

* **sales-invoice-form:** add room search ([872da59](https://gitlab.com/beautyworld_repository/clinic/commit/872da59817d2a46aa878b45f8d1590b8df7a3edd))

### [1.8.6](https://gitlab.com/beautyworld_repository/clinic/compare/v1.8.5...v1.8.6) (2023-11-07)


### Features

* **sales-invoice-table:** add filter date only if user role is godadmin ([e92dd57](https://gitlab.com/beautyworld_repository/clinic/commit/e92dd57afa70b9d4be23634014878f1bbd5d97cc))


### Bug Fixes

* **footer:** remove position absolute ([42cecf6](https://gitlab.com/beautyworld_repository/clinic/commit/42cecf6c5d50f0b3af574a7bcf2fab59ec4712fc))

### [1.8.5](https://gitlab.com/beautyworld_repository/clinic/compare/v1.8.4...v1.8.5) (2023-11-07)


### Bug Fixes

* **sales-summary-report:** add sales_invoice_status to filter data ([b215f6a](https://gitlab.com/beautyworld_repository/clinic/commit/b215f6a5c823d4b238b436f12063169307341f80))
* **sales-summary-report:** fix bug search customer and date ([295e1db](https://gitlab.com/beautyworld_repository/clinic/commit/295e1db3fadee279e5d7ac3ac02f84c9121b6c9c))
* **sales-summary-report:** fix bug search customer and date ([8e8a0e1](https://gitlab.com/beautyworld_repository/clinic/commit/8e8a0e147341846586ee41ea599b145661a175f8))

### [1.8.4](https://gitlab.com/beautyworld_repository/clinic/compare/v1.8.3...v1.8.4) (2023-11-07)


### Bug Fixes

* **sales-invoice-form:** fix autoload customer when edit sales invoice ([faa13b6](https://gitlab.com/beautyworld_repository/clinic/commit/faa13b6745dc1d9e2f06956a8b5befc2e3d02eeb))
* **sales-summary-report:** fix search customer ([5a0cbe5](https://gitlab.com/beautyworld_repository/clinic/commit/5a0cbe5d9741d5ca613b63a73d9275d18ed052d1))
* **sales-summary-report:** prevent request summary report if date not picked ([04dc708](https://gitlab.com/beautyworld_repository/clinic/commit/04dc7082b82c65e05bd79c8db4b507f44d78d2db))
* **user-form:** fix bug phone exists ([7077a18](https://gitlab.com/beautyworld_repository/clinic/commit/7077a18612f19cddeacb720fd2e38acf040376f2))

### [1.8.3](https://gitlab.com/beautyworld_repository/clinic/compare/v1.8.2...v1.8.3) (2023-11-06)


### Bug Fixes

* **sales-invoice-form:** only show appointment if match with current date ([b2b9a1b](https://gitlab.com/beautyworld_repository/clinic/commit/b2b9a1bdeeeced6fae26243041d6024288167f70))
* **sales-report:** update sales report summary ([2b04a89](https://gitlab.com/beautyworld_repository/clinic/commit/2b04a89f04c2ce770e9b9cb7e7f962433620c7ef))
* **sales-summary-report:** update select sales summary report ([ae4bdbe](https://gitlab.com/beautyworld_repository/clinic/commit/ae4bdbe8e4afa065f0008ba3bb43270c745b447e))

### [1.8.2](https://gitlab.com/beautyworld_repository/clinic/compare/v1.8.1...v1.8.2) (2023-11-02)


### Bug Fixes

* **data-redemption:** fix bug customer-transaction component not found ([9b75982](https://gitlab.com/beautyworld_repository/clinic/commit/9b75982770ca68c28b67ebe92379a49306fb1bb6))
* **sales-invoice-form:** fix bug select product ([2ef6bdf](https://gitlab.com/beautyworld_repository/clinic/commit/2ef6bdfd5e21c1b4026e4360b12e6b85ff0ea688))
* **sales-invoice-form:** fix bug select product ([62027b3](https://gitlab.com/beautyworld_repository/clinic/commit/62027b30a45bfd5c92783f2ceef93ec8c7932eb3))
* **sales-summary-report:** update select sales summary report ([af21125](https://gitlab.com/beautyworld_repository/clinic/commit/af21125df2f89210b37987368f9bd3abde425fe3))

### [1.8.1](https://gitlab.com/beautyworld_repository/clinic/compare/v1.7.3...v1.8.1) (2023-11-02)


### Features

* **data-redemption:** add customer transaction tab ([6dc4016](https://gitlab.com/beautyworld_repository/clinic/commit/6dc4016c44829c4191c23590c30257a80b74a35b))

### [1.7.3](https://gitlab.com/beautyworld_repository/clinic/compare/v1.7.2...v1.7.3) (2023-11-02)


### Bug Fixes

* **sales-invoice-form:** fix incorrect show button approval discount ([0c21674](https://gitlab.com/beautyworld_repository/clinic/commit/0c216743ad8672badfc8a94eb62b735233b1e928))

### [1.7.2](https://gitlab.com/beautyworld_repository/clinic/compare/v1.7.1...v1.7.2) (2023-11-01)


### Features

* **sales-invoice-form:** add discount approval to sales invoice ([5767fd0](https://gitlab.com/beautyworld_repository/clinic/commit/5767fd01c54c8a09a00d0c2f979cddddfc559c15))
* **users-module:** add phone on user form and ignore empty data on users table ([f348bf8](https://gitlab.com/beautyworld_repository/clinic/commit/f348bf8bf410708a172696094620b3fd5856b122))


### Bug Fixes

* **sales-invoice-form:** fix marketplace selected ([d7e6f66](https://gitlab.com/beautyworld_repository/clinic/commit/d7e6f662ea683ae3dd7c1017fdd214a4a38cdc4b))

### [1.7.1](https://gitlab.com/beautyworld_repository/clinic/compare/v1.7.0...v1.7.1) (2023-11-01)


### Features

* **sales-invoice-form:** add discount approval to sales invoice ([a314a4f](https://gitlab.com/beautyworld_repository/clinic/commit/a314a4f600ac7e4e1281bfaab875594d0fc31761))


### Bug Fixes

* **appointment-form:** add loading state ([48509a6](https://gitlab.com/beautyworld_repository/clinic/commit/48509a6766e955639f2eb8ddedc1e6732a14c344))
* **login-page:** fix enter on login form ([98f6204](https://gitlab.com/beautyworld_repository/clinic/commit/98f62048d782859b3e670cbe9962c0fbbfb1ce1b))
* **sales-invoice-form:** fix appointment selected ([628060f](https://gitlab.com/beautyworld_repository/clinic/commit/628060fc758edd1ca65dbb28a6328e9373fa178e))

## [1.7.0](https://gitlab.com/beautyworld_repository/clinic/compare/v5.0.0...v1.7.0) (2023-10-31)


### Features

* **dashboard-page:** force user to change password if they password is default ([80731d2](https://gitlab.com/beautyworld_repository/clinic/commit/80731d2b6d1cc866f7fabccbf4e88fe768bd1a01))



### ⚠ BREAKING CHANGES

* **fee-user:** remove agent and travel agent

### Features

* **assets:** upgrade template to pheonix  v1.13.0 ([0da79fb](https://gitlab.com/beautyworld_repository/clinic/commit/0da79fbff8e3c834faed365fb47ef8fb4617cb50))
* **assets:** upgrade template to pheonix  v1.13.0 ([ed06419](https://gitlab.com/beautyworld_repository/clinic/commit/ed06419626a64bf38b45488236fd68f5cbb3c221))
* **dashboard-page:** force user to change password if they password is default ([9d87f32](https://gitlab.com/beautyworld_repository/clinic/commit/9d87f3292b17c959d1f311b85d47ec39400d0ff1))


### Bug Fixes

* **fee-user:** remove agent and travel agent ([ab04d8e](https://gitlab.com/beautyworld_repository/clinic/commit/ab04d8e1afbfa56764b77547dafec5498ce51585))
* **sales-invoice-form:** bug checkout sales invoice ([21d175e](https://gitlab.com/beautyworld_repository/clinic/commit/21d175e9590ba6f64e474c3004d1b2c55ab1b0ad))
* **sales-invoice-form:** bug store sales invoice ([06de4c4](https://gitlab.com/beautyworld_repository/clinic/commit/06de4c49414ae0de07c6dd30d2675f9edda7a56c))
* **sales-invoice-form:** bug updateFeeUser after refactor code with codemium ([c508a49](https://gitlab.com/beautyworld_repository/clinic/commit/c508a49c8ed055ebdef891354a954991d9d533e4))
* **sales-invoice-form:** update customer search and customer phone when select appointment ([bdfef8c](https://gitlab.com/beautyworld_repository/clinic/commit/bdfef8c060b6acee6213b34f969174c732dabf41))

## [1.6.0](https://gitlab.com/beautyworld_repository/clinic/compare/v1.5.8...v4.0.0) (2023-10-30)


### ⚠ BREAKING CHANGES

* **sales-invoice-form:** - potential break sales invoice form
* **appointment-form:** - potential break appointment form

### Features

* **api-documentation:** add auto documentation api with scribe ([b95ea21](https://gitlab.com/beautyworld_repository/clinic/commit/b95ea2101189700dd85b12183d5a05670f552830))
* **appointment-form:** add appointment sidebar ([061151d](https://gitlab.com/beautyworld_repository/clinic/commit/061151df89bc1a5064248e8fb87d08ac93acd5ee))


### Bug Fixes

* **product-form:** Bug company_id and not selected category, subcategory, unit of measure option ([82f2037](https://gitlab.com/beautyworld_repository/clinic/commit/82f20374ecd9638c80618bc84a1f76a264037e74))
* **receipt:** fix bug print receipt when close sales today ([0e0811a](https://gitlab.com/beautyworld_repository/clinic/commit/0e0811af99b81aac85100fcd76695c13f4681ba1))
* **receipt:** fix bug print receipt when close sales today ([4d97e84](https://gitlab.com/beautyworld_repository/clinic/commit/4d97e843dd39626c79f91489d656b5d38228f612))
* **sales-invoice-form:** adjust sales invoice after add appointment sidebar ([ad4d0e3](https://gitlab.com/beautyworld_repository/clinic/commit/ad4d0e302e85f1d020964f02c8c87327d0b8e0ba))

### [1.5.8](https://gitlab.com/beautyworld_repository/clinic/compare/v1.5.7...v1.5.8) (2023-10-26)


### Bug Fixes

* **product-form:** fix company_id not found ([a84e2e7](https://gitlab.com/beautyworld_repository/clinic/commit/a84e2e73dfc05d5e47bc21cdc4058a8a5ba011ac))

### [1.5.7](https://gitlab.com/beautyworld_repository/clinic/compare/v1.5.6...v1.5.7) (2023-10-26)


### Bug Fixes

* **data-redemption:** fix count user redemption ([a7f5241](https://gitlab.com/beautyworld_repository/clinic/commit/a7f524125f955e976112e78c696fa2fc99b6f0dc))

### [1.5.6](https://gitlab.com/beautyworld_repository/clinic/compare/v1.5.5...v1.5.6) (2023-10-25)


### Features

* **today-sales-report:** initiate open today sales button ([f737dd6](https://gitlab.com/beautyworld_repository/clinic/commit/f737dd6e79fd0993e97e012c06f4c42895a06cde))


### Bug Fixes

* **today-sales-report:** add open today sales report ([e2033d9](https://gitlab.com/beautyworld_repository/clinic/commit/e2033d93a1aca9104995c270259b3442205ab05e))

### [1.5.5](https://gitlab.com/beautyworld_repository/clinic/compare/v1.5.4...v1.5.5) (2023-10-25)

### [1.5.4](https://gitlab.com/beautyworld_repository/clinic/compare/v1.5.3...v1.5.4) (2023-10-25)


### Bug Fixes

* **permission:** add some data on spatie permission ([827a1dc](https://gitlab.com/beautyworld_repository/clinic/commit/827a1dca585591f9c992563450e3e289c1023365))

### [1.5.3](https://gitlab.com/beautyworld_repository/clinic/compare/v1.5.2...v1.5.3) (2023-10-25)


### Bug Fixes

* **user-form:** fix edit role on user form ([f111662](https://gitlab.com/beautyworld_repository/clinic/commit/f111662207611badb53caea976e9fdcc720a3aad))

### [1.5.2](https://gitlab.com/beautyworld_repository/clinic/compare/v1.5.1...v1.5.2) (2023-10-25)


### Bug Fixes

* **laravel-backup:** adjust delete old file if reach 1000 mb ([7d22193](https://gitlab.com/beautyworld_repository/clinic/commit/7d22193afe772c9315235b871296d45d64523d9e))
* **receipt:** user application setting to determine printer name and default redirect page ([09f3c93](https://gitlab.com/beautyworld_repository/clinic/commit/09f3c930a2e74ddca0d157dda4ff8bc297e5bff5))

### [1.5.1](https://gitlab.com/beautyworld_repository/clinic/compare/v1.4.13...v1.5.1) (2023-10-24)


### Features

* **api-sales-invoice:** add api route for sales invoice ([daa3717](https://gitlab.com/beautyworld_repository/clinic/commit/daa3717fa0d75f529b09cd159ee4e327188b78e2))


### Bug Fixes

* **laravel-backup:** adjust delete old file if reach 1000 mb ([ac93de7](https://gitlab.com/beautyworld_repository/clinic/commit/ac93de7b6e4f4cb5a5f271a1d70af8762b51f89c))

### [1.4.13](https://gitlab.com/beautyworld_repository/clinic/compare/v1.4.12...v1.4.13) (2023-10-23)


### ⚠ BREAKING CHANGES

* **data-redemption:**   - appointment-form
  - sales-invoice-form
  - data-redemption

### Bug Fixes

* **data-redemption:** add count data redemption generate by default promo ([83b93c3](https://gitlab.com/beautyworld_repository/clinic/commit/83b93c33dd607f0893cbe1dc2a129299abc3df9e))

### [1.4.12](https://gitlab.com/beautyworld_repository/clinic/compare/v1.4.11...v1.4.12) (2023-10-23)


### ⚠ BREAKING CHANGES

* **data-redemption:**
  - sales invoice form
  - appointment form
  - data redemption

### Bug Fixes

* **admin-footer:** append string v befor version number ([168a843](https://gitlab.com/beautyworld_repository/clinic/commit/168a843d8c2d80a030a9fb9e996682d92a82918a))
* **data-redemption:** fix data redemption left ([835b026](https://gitlab.com/beautyworld_repository/clinic/commit/835b02681797b4cda844cd8164dac9930023a831))
* fix error button today sales report ([ea30fa9](https://gitlab.com/beautyworld_repository/clinic/commit/ea30fa96d249dc1c8b17482a263b6799c25b913b))

### [1.4.11](https://gitlab.com/beautyworld_repository/clinic/compare/v1.4.10...v1.4.11) (2023-10-21)


### Bug Fixes

* **admin-footer:** try use exec because shell_exec not working ([ee00ddf](https://gitlab.com/beautyworld_repository/clinic/commit/ee00ddf6a1b7fe5f6488d4ccc970746a3a13be03))
* **admin-footer:** use package json file to get version ([31cb09e](https://gitlab.com/beautyworld_repository/clinic/commit/31cb09eca5281b2f3209c65cae47b6d88c31b0c4))

### [1.4.10](https://gitlab.com/beautyworld_repository/clinic/compare/v1.4.9...v1.4.10) (2023-10-21)


### Bug Fixes

* **admin-footer:** fix git tag hard code ([ea2cc01](https://gitlab.com/beautyworld_repository/clinic/commit/ea2cc0107421293bc19517f19eca8d317587179a))
* **service-fee:** auto create service fee if null ([38eb490](https://gitlab.com/beautyworld_repository/clinic/commit/38eb4905d1e0ebc096c9791ef5ac6a8b736ba954))
* **service-fee:** auto create service fee if null ([bc8894f](https://gitlab.com/beautyworld_repository/clinic/commit/bc8894f3ac7fd12cd717845560a4a6ac3e1640e6))
* **service-table:** update search now can search by service code ([61426a2](https://gitlab.com/beautyworld_repository/clinic/commit/61426a2a49a56bd021709bd0549a7232a1f15e47))
* **today-sales-report:** remove invoice date and discount from table ([ad626eb](https://gitlab.com/beautyworld_repository/clinic/commit/ad626eb9a34389a9e766dc5ba0adb628668e0586))

### [1.4.9](https://gitlab.com/beautyworld_repository/clinic/compare/v1.4.8...v1.4.9) (2023-10-21)


### Bug Fixes

* **appointment-form:** fix bug redemptionlist when updated customer ([aff7494](https://gitlab.com/beautyworld_repository/clinic/commit/aff749409352fd387701f0a1520bbc346633735a))
* **appointment-form:** fix error search redemption if paste customer search ([8693e40](https://gitlab.com/beautyworld_repository/clinic/commit/8693e40ea77355ecf22d366edf2ddc56c14d3ca3))
* **appointment-form:** fix error search redemption if paste customer search ([a54cc31](https://gitlab.com/beautyworld_repository/clinic/commit/a54cc31b9e0b540f81fd99b972f4fea04ca6b2dc))

### [1.4.8](https://gitlab.com/beautyworld_repository/clinic/compare/v1.4.7...v1.4.8) (2023-10-21)


### Bug Fixes

* **appointment-form:** fix error search redemption if paste customer search ([9d72c5d](https://gitlab.com/beautyworld_repository/clinic/commit/9d72c5d864e840865eca0b8a2d44c85e67cfec86))

### [1.4.7](https://gitlab.com/beautyworld_repository/clinic/compare/v1.4.6...v1.4.7) (2023-10-21)


### Bug Fixes

* **data-redemption:** add flag transaction_status to separate buy or redemption service ([494aa72](https://gitlab.com/beautyworld_repository/clinic/commit/494aa728ad9e24230f85271173d4d81f7587ead9))
* **data-redemption:** fix redemption error for show qty left ([6680816](https://gitlab.com/beautyworld_repository/clinic/commit/6680816ada4335d46b8c571d271add21e943928e))

### [1.4.6](https://gitlab.com/beautyworld_repository/clinic/compare/v1.4.5...v1.4.6) (2023-10-21)


### Bug Fixes

* **data-redemption:** add flag transaction_status to separate buy or redemption service ([98bc300](https://gitlab.com/beautyworld_repository/clinic/commit/98bc300e2a8eb4cc5d8c7bd0dc531624b44a32d0))

### [1.4.5](https://gitlab.com/beautyworld_repository/clinic/compare/v1.4.4...v1.4.5) (2023-10-21)


### Bug Fixes

* **data-redemption:** fix bug arrow on firstname label ([41697e8](https://gitlab.com/beautyworld_repository/clinic/commit/41697e8c82f6bf5b20e7510ff56594d33bc8a089))
* **sales-invoice-form:** fix (-) qty if buy service without appointment ([7a51579](https://gitlab.com/beautyworld_repository/clinic/commit/7a51579076efacbc2cbf8dc6a0dd617991735470))

### [1.4.4](https://gitlab.com/beautyworld_repository/clinic/compare/v1.4.3...v1.4.4) (2023-10-20)

### [1.4.3](https://gitlab.com/beautyworld_repository/clinic/compare/v1.4.2...v1.4.3) (2023-10-20)

### [1.4.2](https://gitlab.com/beautyworld_repository/clinic/compare/v1.4.1...v1.4.2) (2023-10-20)


### Bug Fixes

* **adjust-redemption:** fix bug label load user_id instead of customer_id ([84c11ec](https://gitlab.com/beautyworld_repository/clinic/commit/84c11ecb4f20d90ad959323d40a4c97da33d9179))

### [1.4.1](https://gitlab.com/beautyworld_repository/clinic/compare/v1.3.9...v1.4.1) (2023-10-20)


### Features

* **adjust-redemption:** add adjust redemption ([d1918ca](https://gitlab.com/beautyworld_repository/clinic/commit/d1918ca89a32bdf617b545aec8263f4a1565a9b8))

### [1.3.9](https://gitlab.com/beautyworld_repository/clinic/compare/v1.3.8...v1.3.9) (2023-10-19)


### Bug Fixes

* **appointment-form:** fix auto select service tab after choose redemption ([77953ad](https://gitlab.com/beautyworld_repository/clinic/commit/77953ad1ed435112e5357f3afde2a97a2ee59f99))

### [1.3.8](https://gitlab.com/beautyworld_repository/clinic/compare/v1.3.7...v1.3.8) (2023-10-19)


### Features

* **cash-drawer:** add print function when close sales invoice ([4bd16b9](https://gitlab.com/beautyworld_repository/clinic/commit/4bd16b95b7c2942018eb12e7017e8070c0d88514))

### [1.3.7](https://gitlab.com/beautyworld_repository/clinic/compare/v1.3.6...v1.3.7) (2023-10-19)


### Features

* **user-login:** record user login attempt on database ([9055449](https://gitlab.com/beautyworld_repository/clinic/commit/90554499257cf4b0501f28c23d651bce8b14a20f))


### Bug Fixes

* **customer-search:** Add realtime update when customer by phone is exists ([3237f4a](https://gitlab.com/beautyworld_repository/clinic/commit/3237f4a51b8ae3711d2a30e5e09b4f44790dfcbe))
* **user-login:** typo attempt class ([b86dd9e](https://gitlab.com/beautyworld_repository/clinic/commit/b86dd9e7eb2a28626a5d577b6bc157d86898bbea))
* **user-login:** typo attempt class part 2 ([67aaf2d](https://gitlab.com/beautyworld_repository/clinic/commit/67aaf2dcdbc69a87b4f90194473b808a53190fe3))

### [1.3.6](https://gitlab.com/beautyworld_repository/clinic/compare/v1.3.5...v1.3.6) (2023-10-18)


### Bug Fixes

* **appointment:** fix error product selected ([8f4bd84](https://gitlab.com/beautyworld_repository/clinic/commit/8f4bd8478767dd6d166837c1bb19cf2aa92d265c))

### [1.3.5](https://gitlab.com/beautyworld_repository/clinic/compare/v1.3.4...v1.3.5) (2023-10-17)


### Features

* **sales invoice:** add payment method on struck ([5af8f65](https://gitlab.com/beautyworld_repository/clinic/commit/5af8f6506eabf67d5fbf252a63ea8b135dd7f98e))

### [1.3.4](https://gitlab.com/beautyworld_repository/clinic/compare/v1.3.3...v1.3.4) (2023-10-17)

### [1.3.3](https://gitlab.com/beautyworld_repository/clinic/compare/v1.3.2...v1.3.3) (2023-10-17)


### Bug Fixes

* **sales invoice:** force print to local instead of using wildcard route ([d0a1087](https://gitlab.com/beautyworld_repository/clinic/commit/d0a1087c8f560244b3de8d67e671f3cd781728f1))

### [1.3.2](https://gitlab.com/beautyworld_repository/clinic/compare/v1.3.1...v1.3.2) (2023-10-17)


### Bug Fixes

* **sales invoice:** fix checkout button after reopen sales in voice ([e5d5699](https://gitlab.com/beautyworld_repository/clinic/commit/e5d5699453cf32eef7a5c5a30762eb1dc2840635))

### [1.3.1](https://gitlab.com/beautyworld_repository/clinic/compare/v1.3.0...v1.3.1) (2023-10-17)


### Features

* **report:** update download customer redemption by userId ([d4b59dc](https://gitlab.com/beautyworld_repository/clinic/commit/d4b59dc969574491c0550b0164e41c521b0fe895))
* **report:** update download customer redemption by userId ([e753c35](https://gitlab.com/beautyworld_repository/clinic/commit/e753c352b3559892f98441f24efa1499c4981fe7))


### Bug Fixes

* **laravel-backup:** set laravel backup only 5 files ([4dd3f9c](https://gitlab.com/beautyworld_repository/clinic/commit/4dd3f9cd93f9fd7f8aa455617347075d812d8d48))

## [1.3.0](https://gitlab.com/beautyworld_repository/clinic/compare/v1.2.0...v1.3.0) (2023-10-17)


### Features

* add closing transaction ([60466fa](https://gitlab.com/beautyworld_repository/clinic/commit/60466fa80aaf8e3916891c98bc777fa70858d36c))
* **sales invoice:** add reopen, void sales invoices, filter si by valid status ([4c2f3ad](https://gitlab.com/beautyworld_repository/clinic/commit/4c2f3ad63efe75508b93bf70dee3005d518a2d61))

## [1.2.0](https://gitlab.com/beautyworld_repository/clinic/compare/v1.1.1...v1.2.0) (2023-10-17)


### Features

* **today-sales-report:** initiate close ([6bd9263](https://gitlab.com/beautyworld_repository/clinic/commit/6bd9263301e3113fabd9bbab163897ffa9824a7c))

### [1.1.1](https://gitlab.com/beautyworld_repository/clinic/compare/v2.0.0...v1.1.1) (2023-10-17)


### Bug Fixes

* **today-sales-report:** fix wrong field load data sum payment ([a416b20](https://gitlab.com/beautyworld_repository/clinic/commit/a416b20a4169a6306feb1d4667c9be090e042ed3))

## [1.1.0](https://gitlab.com/beautyworld_repository/clinic/compare/v1.0.0...v1.1.0) (2023-10-16)


### Features

* add GitHub conventions ([9f7ea15](https://gitlab.com/beautyworld_repository/clinic/commit/9f7ea15fe4aa53787051eb638fe95b749a85039c))
