<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\SettingPrinterPage;
use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class SettingPrinterPageTest extends TestCase
{
    public $user;

    /**
     * Sets up the test environment by seeding the database with roles and permissions.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();
    }

    #[Test]
    public function it_checks_if_setting_printer_page_livewire_component_exists()
    {
        $this->assertTrue(class_exists(SettingPrinterPage::class));
    }

    #[Test]
    public function it_checks_if_logged_in_user_can_see_settings_printer_route()
    {
        $response = $this->actingAs($this->user)->get(route('settings.printer'));

        $response->assertStatus(200);
    }

    #[Test]
    public function it_checks_if_not_authenticated_user_cannot_see_settings_printer_route()
    {
        $response = $this->get(route('settings.printer'));

        $response->assertStatus(401);
    }
}
