<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\CustomerTable;
use App\Models\User;
use Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class CustomerPageTest extends TestCase
{
    public $user;

    /**
     * Sets up the test environment by seeding the database with roles and permissions.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();
    }

    /**
     * Test that the title of the customers page is "Customer".
     */
    #[Test]
    public function title_page_is_customers(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('master.customers'));

        $response->assertStatus(200);

        Livewire::test(CustomerTable::class, ['initialTitlePage' => 'Customer'])
            ->assertSet('titlePage', 'Customer');
    }

    /**
     * Assesses if an authenticated user can see the page.
     */
    #[Test]
    public function authenticated_user_can_see_the_page()
    {
        $response = $this->actingAs($this->user)->get(route('master.customers'));

        $response->assertStatus(200);
    }

    /**
     * Assesses if a non-authenticated user cannot see the page.
     */
    #[Test]
    public function non_authenticated_user_cannot_see_the_page()
    {
        auth()->logout();
        $response = $this->get(route('master.customers'));

        $response->assertStatus(401);
    }
}
