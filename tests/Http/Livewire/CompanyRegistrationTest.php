<?php

namespace Tests\Http\Livewire;

use Tests\TestCase;

class CompanyRegistrationTest extends TestCase
{
    /**
     * Test case to verify that a guest user can access the register company page.
     */
    public function test_guest_can_see_register_company_page(): void
    {
        $response = $this->get('/company_register');

        $response->assertStatus(200);
    }

    /**
     * Test if a guest can register a company via the API.
     */
    public function test_guest_can_register_company_via_api(): void
    {
        $companyData = [
            'name' => 'Test Company',
            'email' => 'test@company.com',
            'phone' => '555-555-5555',
            'website' => 'https://testcompany.com',
            'address' => '123 Test Street',
            'code' => '12345',
        ];

        $response = $this->json('POST', '/api/companies', $companyData);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'message',
            'data' => [
                'name',
                'email',
                'phone',
                'website',
                'address',
                'code',
                'created_at',
                'updated_at',
                'id',
            ],
        ]);

        // Exclude 'code' key from comparison
        $this->assertDatabaseHas('companies', array_diff_key($companyData, ['code' => '']));
    }

    /**
     * Test if a guest user can see the "Register Company" text on the /company_register page.
     */
    public function test_guest_can_see_register_company_text_on_page(): void
    {
        $response = $this->get('/company_register');

        $response->assertStatus(200)->assertSeeText('Register Company');
    }

    /**
     * Test if email is required to register a company through API.
     */
    public function test_email_is_required_to_register_a_company(): void
    {
        $companyData = [
            'name' => 'Test Company',
            'phone' => '555-555-5555',
            'website' => 'https://testcompany.com',
            'address' => '123 Test Street',
            'code' => '12345',
        ];

        $response = $this->json('POST', '/api/companies', $companyData);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('email');
    }

    /**
     * Test if the email must be in a valid email format when registering a company through the API.
     */
    public function test_email_must_be_in_valid_email_format_when_registering_a_company(): void
    {
        $companyData = [
            'name' => 'Test Company',
            'email' => 'not_an_email',
            'phone' => '555-555-5555',
            'website' => 'https://testcompany.com',
            'address' => '123 Test Street',
            'code' => '12345',
        ];

        $response = $this->json('POST', '/api/companies', $companyData);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('email');
    }
}
