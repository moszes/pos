<?php

namespace Tests\Http\Livewire;

use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class CompanyDataPageTest extends TestCase
{
    public $user;

    /**
     * Sets up the test environment by seeding the database with roles and permissions.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();
    }

    /**
     * Tests if a logged-in user can see the company data route.
     *
     * @test
     */
    #[Test]
    public function logged_in_user_can_see_company_data_route(): void
    {
        $response = $this->actingAs($this->user)->followingRedirects()->get(route('company_data'));

        $response->assertSuccessful();
    }
}
