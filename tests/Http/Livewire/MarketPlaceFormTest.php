<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\MarketPlaceForm;
use App\Models\Category;
use App\Models\MarketPlace;
use App\Models\SubCategory;
use App\Models\User;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class MarketPlaceFormTest extends TestCase
{
    public $marketplace;

    /**
     * Set up the test environment before each test case.
     *
     * This method creates a new MarketPlace instance using the factory method and assigns it to the $this->marketplace property.
     * It also logs in the first User using the auth()->login() method.
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->marketplace = MarketPlace::first();

        auth()->login(User::first());
    }

    /**
     * Test the store method of the MarketPlaceForm class.
     *
     * This test case verifies that the store method of the MarketPlaceForm class correctly creates a new marketplace
     * record in the database. It uses Livewire to simulate a call to the store method and asserts that the marketplace
     * record has been created with the provided data.
     *
     * @return void
     */
    public function testStore()
    {
        Livewire::test(MarketPlaceForm::class)
            ->set('categoryId', Category::first()->id)
            ->set('subCategoryId', SubCategory::first()->id)
            ->set('code', 'test-code')
            ->set('name', 'test-name')
            ->set('price', rand(1, 10000))
            ->call('store')
            ->assertEmitted('clearMarketPlaceForm');

        $this->assertDatabaseHas('market_places', [
            'code' => 'test-code',
            'name' => 'test-name',
        ]);
    }

    /**
     * Test the destroy method of the MarketPlaceForm class.
     *
     * This test case verifies
     * that the destroy method of the MarketPlaceForm class correctly softly deletes a marketplace
     * record in the database. It uses Livewire to simulate a call to the destroy method with the ID of a marketplace
     * instance created in the setUp method. After the destroy method is called, it asserts that the marketplace record
     * has been softly deleted and its code has been updated with the '-deleted' suffix.
     *
     * @return void
     */
    public function testDestroy()
    {
        Livewire::test(MarketPlaceForm::class)
            ->call('destroy', $this->marketplace->id);

        $this->assertSoftDeleted($this->marketplace, ['code' => $this->marketplace->code.'-deleted']);
    }

    /**
     * Test that the function updates a marketplace.
     *
     * This test case verifies that the update method of the MarketPlaceForm class correctly updates a marketplace
     * record in the database. It uses Livewire to simulate a call to the update method and asserts that the marketplace
     * record has been updated with the provided data. It also asserts that the 'clearMarketPlaceForm' event is emitted.
     */
    #[Test]
    public function it_updates_a_marketplace(): void
    {
        Livewire::test(MarketPlaceForm::class)
            ->set('marketplace', $this->marketplace)
            ->set('code', 'test-code-updated')
            ->set('name', 'test-name-updated')
            ->call('update')
            ->assertEmitted('clearMarketPlaceForm');

        $this->marketplace = MarketPlace::first();

        $this->assertDatabaseHas('market_places', [
            'code' => 'test-code-updated',
            'name' => 'test-name-updated',
        ]);
    }
}
