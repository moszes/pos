<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\PermissionTable;
use App\Models\User;
use Illuminate\Support\Str;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Spatie\Permission\Models\Permission;
use Tests\TestCase;

class PermissionTableTest extends TestCase
{
    public $permission;

    public $user;

    protected $permissionModel;

    /**
     * Set up the test environment before each test method is executed.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();

        // Create a Permission instance for testing
        $this->permissionModel = Permission::create([
            'code' => Str::random(6),
            'name' => Str::random(10),
        ]);

        $this->user->assignRole('godadmin');

    }

    #[Test]
    /**
     * Test the search functionality of the PermissionTable component
     * by searching for a permission by its name.
     */
    public function permission_table_search_by_name(): void
    {
        Livewire::actingAs($this->user)
            ->test(PermissionTable::class)
            ->set('search', $this->permissionModel->name)
            ->call('searchFilter')
            ->assertSee($this->permissionModel->name);
    }

    /**
     * This test verifies that the PermissionTable component contains a search box.
     */
    #[Test]
    public function if_search_box_exists_in_permission_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(PermissionTable::class)
            ->assertSee('search');
    }

    /**
     * This test verifies that the PermissionTable component contains the 'edit' and 'delete' buttons.
     */
    #[Test]
    public function if_edit_and_delete_button_exists_in_permission_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(PermissionTable::class)
            ->assertSee('fa-pencil')     // Check for the Edit button
            ->assertSee('fa-remove');    // Check for the Delete button
    }
}
