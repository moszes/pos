<?php

namespace Tests\Http\Livewire;

use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class StakeHolderTest extends TestCase
{
    public $user;

    /**
     * Set up the test environment.
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::first();

        auth()->login($this->user);
    }

    /**
     * Array of views to check authenticated and unauthenticated access
     *
     * @var array
     */
    protected $views = [
        'master.users',
        'master.customers',
        'master.employees',
        'master.roles',
        'master.permissions',
    ];

    /**
     * If logged-in user can see pages.
     */
    #[Test]
    public function loggedInUserCanViewPages(): void
    {
        foreach ($this->views as $view) {
            $response = $this->get(route($view));
            $response->assertStatus(200);
        }
    }

    /**
     * If non-logged-in user gets 401 error when viewing pages.
     *
     * @return void
     */
    #[Test]
    public function nonAuthenticatedUserCannotViewPages()
    {
        auth()->logout();
        foreach ($this->views as $view) {
            $response = $this->get(route($view));
            $response->assertStatus(401);
        }
    }
}
