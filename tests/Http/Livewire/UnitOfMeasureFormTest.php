<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\UnitOfMeasureForm;
use App\Models\UnitOfMeasure;
use App\Models\User;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class UnitOfMeasureFormTest extends TestCase
{
    protected $unitOfMeasure;

    protected $user;

    /**
     * The setup method will initialize the Unit of Measure.
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::first();
        $this->unitOfMeasure = UnitOfMeasure::first();

    }

    /**
     * This function checks if the application is capable of storing a unit of measure.
     */
    #[Test]
    public function it_can_store_a_unit_of_measure()
    {
        $this->actingAs($this->user);

        Livewire::test(UnitOfMeasureForm::class)->set('code', 'UOM01')->set('name', 'Unit of Measure 01')->call('store');

        $this->assertDatabaseHas('unit_of_measures', ['code' => 'UOM01', 'name' => 'Unit of Measure 01']);
    }

    /**
     * This function verifies that a name and a code are required for storing a unit of measure.
     */
    #[Test]
    public function it_requires_a_name_and_a_code()
    {
        $this->actingAs($this->user);

        Livewire::test(UnitOfMeasureForm::class)->set('name', '')->call('store')->assertHasErrors('name');

        Livewire::test(UnitOfMeasureForm::class)->set('code', '')->call('store')->assertHasErrors('code');
    }

    /**
     * This function confirms that the code out there must be unique.
     */
    #[Test]
    public function it_requires_a_unique_code()
    {
        $this->actingAs($this->user);

        UnitOfMeasure::create(['company_id' => auth()->user()->details->branch_id, 'branch_id' => auth()->user()->details->branch_id, 'code' => 'UOM01', 'name' => 'Unit of Measure 01', 'created_at' => now(), 'updated_at' => now()]);

        Livewire::test(UnitOfMeasureForm::class)->set('code', 'UOM01')->set('name', 'Unit of Measure 02')->call('store')->assertHasErrors('code');
    }

    /**
     * This function checks if an authorized user can delete a unit of measure successfully (when not being used).
     */
    #[Test]
    public function it_can_destroy_a_unit_of_measure()
    {
        // Acting as the created user
        $this->actingAs($this->user);

        $this->unitOfMeasure = UnitOfMeasure::factory()->create();

        Livewire::test(UnitOfMeasureForm::class)->call('destroy', $this->unitOfMeasure->id);

        $this->unitOfMeasure = UnitOfMeasure::find($this->unitOfMeasure->id);

        $this->assertNull($this->unitOfMeasure);

    }

    /**
     * This function verifies that the unit of measure cannot be deleted when being used.
     */
    #[Test]
    public function it_cannot_destroy_a_unit_of_measure_when_being_used()
    {
        // Acting as the created user
        $this->actingAs($this->user);

        Livewire::test(UnitOfMeasureForm::class)->call('destroy', $this->unitOfMeasure->id);

        $this->assertNotNull($this->unitOfMeasure);
    }

    /**
     * This function checks whether the application can update a unit of measure.
     */
    #[Test]
    public function it_can_update_a_unit_of_measure()
    {
        // Act as the created user
        $this->actingAs($this->user);

        // Initialize the UnitOfMeasureForm
        $component = Livewire::test(UnitOfMeasureForm::class);

        // Call the editUnitOfMeasure function in UnitOfMeasureForm
        $component->call('editUnitOfMeasure', $this->unitOfMeasure->id);

        // Update the unit of measure
        $component->set('code', 'UOM02')->set('name', 'Unit of Measure 02')->call('update');

        // Check if the unit of measure has been updated in the database
        $this->assertDatabaseHas('unit_of_measures', ['code' => 'UOM02', 'name' => 'Unit of Measure 02']);
    }
}
