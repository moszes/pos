<?php

namespace Tests\Http\Livewire;

use App\Models\User;
use Tests\TestCase;

class CustomerTest extends TestCase
{
    public $user;

    public $customer;

    /**
     * Set up the test environment.
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::first();

        $this->customer = User::factory()->create();

        $this->customer->assignRole('customer');
    }

    /**
     * Test to verify that a logged-in user can view the master_customer.
     *
     * @return void
     */
    public function test_logged_in_user_can_view_master_customer()
    {
        $response = $this->actingAs($this->user)
            ->get(route('master.customers'));

        $response->assertStatus(200);
    }

    /**
     * Test to verify that a logged-in user can view the customer.data_redemption route.
     *
     * @return void
     */
    public function test_logged_in_user_can_view_customer_data_redemption()
    {
        $response = $this->actingAs($this->user)
            ->get(route('customer.data_redemption', [
                'customerId' => $this->customer->id]
            ));

        $response->assertStatus(200);
    }
}
