<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\ProductForm;
use App\Models\Company;
use App\Models\Product;
use App\Models\User;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class ProductFormTest extends TestCase
{
    public $product;

    /**
     * Set up the test environment before each test case.
     *
     * This function is called before each test case is executed. It performs the following steps:
     * - Calls the parent setUp() function to ensure any necessary setup is performed.
     * - Seeds the database using the 'DatabaseSeeder' class.
     * - Logs in the first user by calling the 'login' method on the 'auth' facade.
     */
    protected function setUp(): void
    {
        parent::setUp();

        auth()->login(User::first());
    }

    /**
     * Test that the function stores a product.
     *
     * This function tests the functionality of the `store` method in the `ProductForm` Livewire component.
     * It performs the following steps:
     * - Creates a Livewire test instance of the `ProductForm` component.
     * - Sets the `code` and `name` properties of the component to 'test-code' and 'test-name' respectively.
     * - Calls the `store` method of the component.
     * - Asserts that the 'refreshProduct' and 'clearProductForm' events are emitted.
     * - Asserts that the 'products' table in the database contains a row with 'code' and 'name' values of 'test-code' and 'test-name' respectively.
     */
    #[Test]
    public function it_stores_a_product(): void
    {
        Livewire::test(ProductForm::class)
            ->set('code', 'test-code')
            ->set('name', 'test-name')
            ->call('store')
            ->assertEmitted('refreshProduct')
            ->assertEmitted('clearProductForm');

        $this->assertDatabaseHas('products', [
            'code' => 'test-code',
            'name' => 'test-name',
        ]);
    }

    /**
     * Test that the function updates a product.
     */
    #[Test]
    public function it_updates_a_product(): void
    {
        $product = Product::factory()->create();

        Livewire::test(ProductForm::class)
            ->set('product', $product)
            ->set('code', 'test-code-updated')
            ->set('name', 'test-name-updated')
            ->call('update')
            ->assertEmitted('refreshProduct')
            ->assertEmitted('clearProductForm');

        $this->assertDatabaseHas('products', [
            'code' => 'test-code-updated',
            'name' => 'test-name-updated',
        ]);
    }

    /**
     * Test the destroy functionality of the ProductForm component.
     *
     * This test creates a new Product instance with a specific company, code, and name.
     * Then it simulates a call to the 'destroy' method of the ProductForm component
     * with the ID of the created product. Finally, it asserts that the product has
     * been softly deleted by checking that a record with the updated code exists in the
     * 'products' table.
     *
     * @return void
     */
    #[Test]
    public function it_soft_deletes_a_product()
    {
        $this->product = Product::create([
            'company_id' => Company::factory()->create()->id,
            'code' => 'P-1',
            'name' => 'Product 1',
        ]);

        Livewire::test(ProductForm::class)
            ->call('destroy', $this->product->id);

        $this->assertSoftDeleted('products', [
            'code' => $this->product->code.'-deleted',
        ]);
    }
}
