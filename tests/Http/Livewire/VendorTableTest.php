<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\VendorTable;
use App\Models\User;
use App\Models\Vendor;
use Illuminate\Support\Str;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class VendorTableTest extends TestCase
{
    public $user;

    protected $vendorModel;

    /**
     * Set up the test environment before each test method is executed.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();

        // Create a Vendor instance for testing
        $this->vendorModel = Vendor::create([
            'name' => Str::random(10),
            'company_id' => 1,
            'branch_id' => 1,
            'code' => Str::random(5),
        ]);
    }

    #[Test]
    /**
     * Test the search functionality of the VendorTable component
     * by searching for a vendor by its name.
     */
    public function vendor_table_search_by_name(): void
    {
        Livewire::actingAs($this->user)
            ->test(VendorTable::class)
            ->set('search', $this->vendorModel->name)
            ->call('searchFilter')
            ->assertSee($this->vendorModel->name);
    }

    #[Test]
    /**
     * Test the VendorTable component's search functionality by code.
     */
    public function vendor_table_search_by_code(): void
    {
        Livewire::actingAs($this->user)
            ->test(VendorTable::class)
            ->set('search', $this->vendorModel->code)
            ->call('searchFilter')
            ->assertSee($this->vendorModel->code);
    }

    /**
     * This test verifies that the VendorTable component contains a search box.
     */
    #[Test]
    public function if_search_box_exists_in_vendor_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(VendorTable::class)
            ->assertSee('search');
    }

    /**
     * This test verifies that the VendorTable component contains the 'edit' and 'delete' buttons.
     */
    #[Test]
    public function if_edit_and_delete_button_exists_in_vendor_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(VendorTable::class)
            ->assertSee('fa-pencil')     // Check for the Edit button
            ->assertSee('fa-remove');    // Check for the Delete button
    }
}
