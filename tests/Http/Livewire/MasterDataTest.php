<?php

namespace Tests\Http\Livewire;

use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class MasterDataTest extends TestCase
{
    public $user;

    /**
     * Sets up the test environment by seeding the database with roles and permissions.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();

        auth()->login($this->user);
    }

    #[Test]
    /**
     * Tests that a logged-in user can access dashboard routes.
     */
    public function logged_in_user_can_access_master_data_routes(): void
    {
        // Iterate over the dashboard routes and test that each route returns a 200 status code.
        foreach (self::master_data_routes() as [$route_name, $route_method]) {
            $response = $this->$route_method(route($route_name));
            $response->assertStatus(200);
        }
    }

    /**
     * Provides a list of dashboard routes and their corresponding HTTP methods for testing.
     */
    public static function master_data_routes(): array
    {
        // Define the dashboard routes and their corresponding HTTP methods.
        return [
            ['master.users', 'get'],
        ];
    }
}
