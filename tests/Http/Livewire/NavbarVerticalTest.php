<?php

namespace Tests\Http\Livewire;

use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

/**
 * Class NavbarVerticalTest
 */
class NavbarVerticalTest extends TestCase
{
    public $user;

    /**
     * Setup testing environment
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();
    }

    /**
     * Test to check if a component exists and has the right permission
     */
    #[Test]
    public function check_if_component_exists_and_has_right_permission(): void
    {
        foreach (self::componentProvider() as $component) {
            $this->components_existence_and_accessibility($component);
        }
    }

    /**
     * Test to check the existence and accessibility of components
     *
     * @dataProvider componentProvider
     */
    public function components_existence_and_accessibility(string $component): void
    {
        $classCheck = 'App\Http\Livewire\\'.$component;

        $this->assertTrue(class_exists($classCheck));
        auth()->logout();

        $response = $this->get(route('dashboard'));
        $response->assertStatus(401);

        // Login as a user with a godadmin role
        auth()->login($this->user);

        // Check if the logged-in user can view the Livewire component
        $response = $this->get(route('dashboard'));
        $response->assertSeeLivewire(strtolower(preg_replace('/(?<!^)[A-Z]/', '-$0', $component)));

    }

    /**
     * Provider for components
     */
    public function componentProvider(): array
    {
        return ['NavbarVertical', 'DashboardLink', 'FaqLink', 'MasterDataLink', 'ReportLink', 'SettingLink', 'ToolLink', 'TransactionLink'];
    }
}
