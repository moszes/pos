<?php

namespace Tests\Http\Livewire;

use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class DataRedemptionPageTest extends TestCase
{
    public $user;

    /**
     * Sets up the test environment by seeding the database with roles and permissions.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();
    }

    /**
     * Assesses if an authenticated user can see the page.
     */
    #[Test]
    public function authenticated_user_can_see_the_page()
    {
        $response = $this->actingAs($this->user)->get(route('customer.data_redemption'));

        $response->assertStatus(200);
    }

    /**
     * Assesses if a non-authenticated user cannot see the page.
     */
    #[Test]
    public function non_authenticated_user_cannot_see_the_page()
    {
        $response = $this->get(route('customer.data_redemption'));

        $response->assertStatus(401);
    }
}
