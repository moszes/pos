<?php

namespace Tests\Http\Livewire;

use Tests\TestCase;

class DashboardWidgetTest extends TestCase
{
    /**
     * Assert that the given Livewire component exists.
     *
     * @return void
     */
    public function test_dashboard_page_can_verify_dashboard_turn_over_component_exists()
    {
        $this->assertLivewireComponentExists('DashboardTurnOver');
    }

    /**
     * Test whether the dashboard page can verify if the DashboardMonthlySales Livewire component exists.
     *
     * @return void
     */
    public function test_dashboard_page_can_verify_dashboard_monthly_sales_component_exists()
    {
        $this->assertLivewireComponentExists('DashboardMonthlySales');
    }

    /**
     * Assert that the given Livewire component exists on the dashboard page.
     *
     * @return void
     */
    public function test_dashboard_page_can_verify_dashboard_today_sales_component_exists()
    {
        $this->assertLivewireComponentExists('DashboardTodaySales');
    }

    /**
     * Assert that the DashboardBusinessTrend Livewire component exists.
     *
     * @return void
     */
    public function test_dashboard_page_can_verify_dashboard_business_trend_component_exists()
    {
        $this->assertLivewireComponentExists('DashboardBusinessTrend');
    }

    /**
     * Test whether the DashboardTurnOverChart Livewire component exists.
     *
     * @return void
     */
    public function test_dashboard_page_can_verify_dashboard_turn_over_chart_component_exists()
    {
        $this->assertLivewireComponentExists('DashboardTurnOverChart');
    }

    /**
     * Assert that the DashboardCustomerBirthday Livewire component exists.
     *
     * @return void
     */
    public function test_dashboard_page_can_verify_dashboard_customer_birthday_component_exists()
    {
        $this->assertLivewireComponentExists('DashboardCustomerBirthday');
    }

    /**
     * Assert that the given Livewire component exists.
     */
    public function assertLivewireComponentExists(string $name): void
    {
        $this->assertTrue(class_exists('App\\Http\\Livewire\\'.$name), "The [$name] Livewire component does not exist.");
    }
}
