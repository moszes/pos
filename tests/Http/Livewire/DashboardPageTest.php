<?php

namespace Tests\Http\Livewire;

use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class DashboardPageTest extends TestCase
{
    public $user;

    /**
     * Sets up the test environment by seeding the database with roles and permissions.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();
    }

    #[Test]
    /**
     * Tests that a logged-in user can access dashboard routes.
     */
    public function logged_in_user_can_access_dashboard_routes(): void
    {
        $this->actingAs($this->user);

        // Iterate over the dashboard routes and test that each route returns a 200 status code.
        foreach (self::dashboard_routes() as [$route_name, $route_method]) {
            $response = $this->$route_method(route($route_name));
            $response->assertStatus(200);
        }
    }

    #[Test]
    /**
     * Tests that a guest user cannot access dashboard routes.
     */
    public function guest_user_cannot_access_dashboard_routes(): void
    {
        auth()->logout();
        // Iterate over the dashboard routes and test that each route returns a 401 status code.
        foreach (self::dashboard_routes() as [$route_name, $route_method]) {
            $response = $this->$route_method(route($route_name));
            $response->assertStatus(401);
        }
    }

    /**
     * Provides a list of dashboard routes and their corresponding HTTP methods for testing.
     */
    public static function dashboard_routes(): array
    {
        // Define the dashboard routes and their corresponding HTTP methods.
        return [
            ['dashboard', 'get'],
            ['transaction.sales_invoices', 'get'],
            ['transaction.appointments', 'get'],
            ['master.employees', 'get'],
            ['master.customers', 'get'],
            ['report.today_sales_report', 'get'],
        ];
    }
}
