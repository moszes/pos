<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\PermissionTable;
use App\Models\User;
use Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class PermissionPageTest extends TestCase
{
    public $user;

    /**
     * Set up the test case by calling the parent's setUp method and
     * seeding the database with RoleSeeder and PermissionSeeder.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();
    }

    /**
     * Test that the title of the permissions page is "Permission".
     */
    #[Test]
    public function title_page_is_permissions(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('master.permissions'));

        $response->assertStatus(200);

        Livewire::test(PermissionTable::class, ['initialTitlePage' => 'Permission'])
            ->assertSet('titlePage', 'Permission');
    }

    /**
     * Assesses if an authenticated user can see the page.
     */
    #[Test]
    public function authenticated_user_can_see_the_page()
    {
        $response = $this->actingAs($this->user)->get(route('master.permissions'));

        $response->assertStatus(200);
    }

    /**
     * Assesses if a non-authenticated user cannot see the page.
     */
    #[Test]
    public function non_authenticated_user_cannot_see_the_page()
    {
        $response = $this->get(route('master.permissions'));

        $response->assertStatus(401);
    }
}
