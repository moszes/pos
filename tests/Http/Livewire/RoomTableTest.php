<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\RoomTable;
use App\Models\Room;
use App\Models\User;
use Illuminate\Support\Str;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class RoomTableTest extends TestCase
{
    public $user;

    protected $roomModel;

    /**
     * Set up the test environment before each test method is executed.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();

        // Create a Room instance for testing
        $this->roomModel = Room::create([
            'name' => Str::random(10),
            'company_id' => 1,
            'branch_id' => 1,
            'code' => Str::random(5),
        ]);
    }

    #[Test]
    /**
     * Test the search functionality of the RoomTable component
     * by searching for a room by its name.
     */
    public function room_table_search_by_name(): void
    {
        Livewire::actingAs($this->user)
            ->test(RoomTable::class)
            ->set('search', $this->roomModel->name)
            ->call('searchFilter')
            ->assertSee($this->roomModel->name);
    }

    #[Test]
    /**
     * Test the RoomTable component's search functionality by code.
     */
    public function room_table_search_by_code(): void
    {
        Livewire::actingAs($this->user)
            ->test(RoomTable::class)
            ->set('search', $this->roomModel->code)
            ->call('searchFilter')
            ->assertSee($this->roomModel->code);
    }

    /**
     * This test verifies that the RoomTable component contains a search box.
     */
    #[Test]
    public function if_search_box_exists_in_room_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(RoomTable::class)
            ->assertSee('search');
    }

    /**
     * This test verifies that the RoomTable component contains the 'edit' and 'delete' buttons.
     */
    #[Test]
    public function if_edit_and_delete_button_exists_in_room_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(RoomTable::class)
            ->assertSee('fa-pencil')     // Check for the Edit button
            ->assertSee('fa-remove');    // Check for the Delete button
    }
}
