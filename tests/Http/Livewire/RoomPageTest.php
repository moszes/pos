<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\RoomTable;
use App\Models\User;
use Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class RoomPageTest extends TestCase
{
    public $user;

    /**
     * Set up the test case by calling the parent's setUp method and
     * seeding the database with RoleSeeder and PermissionSeeder.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();
    }

    /**
     * Test that the title of the rooms page is "Room".
     */
    #[Test]
    public function title_page_is_rooms(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('master.rooms'));

        $response->assertStatus(200);

        Livewire::test(RoomTable::class, ['initialTitlePage' => 'Room'])
            ->assertSet('titlePage', 'Room');
    }

    /**
     * Assesses if an authenticated user can see the page.
     */
    #[Test]
    public function authenticated_user_can_see_the_page()
    {
        $response = $this->actingAs($this->user)->get(route('master.rooms'));

        $response->assertStatus(200);
    }

    /**
     * Assesses if a non-authenticated user cannot see the page.
     */
    #[Test]
    public function non_authenticated_user_cannot_see_the_page()
    {
        $response = $this->get(route('master.rooms'));

        $response->assertStatus(401);
    }
}
