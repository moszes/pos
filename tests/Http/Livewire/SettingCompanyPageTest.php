<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\SettingCompanyPage;
use App\Models\Company;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Livewire\Livewire;
use Tests\TestCase;

class SettingCompanyPageTest extends TestCase
{
    /**
     * Test the 'updateCompany' function in the SettingCompanyPage class
     */
    public function testUpdateCompanyWithoutLogo(): void
    {
        $company = Company::factory()->create();
        $component = Livewire::test(SettingCompanyPage::class,
            ['company' => $company->fresh(),
            ]);
        $data = [
            'companyName' => 'New company',
            'companyEmail' => 'new.email@example.com',
            'companyPhone' => '1234567890',
            'companyAddress' => 'New Company Address',
            'companyWebsite' => 'https://www.newwebsite.com',
        ];

        $dataOnDatabase = [
            'name' => 'New company',
            'email' => 'new.email@example.com',
            'phone' => '1234567890',
            'address' => 'New Company Address',
            'website' => 'https://www.newwebsite.com',
        ];
        foreach ($data as $key => $value) {
            $component->set($key, $value);
        }
        $component->call('updateCompany');
        $this->assertDatabaseHas('companies', $dataOnDatabase);
    }

    /**
     * Test the logo validation and upload in 'updateCompany' method
     */
    public function testUpdateCompanyWithLogo(): void
    {
        Storage::fake('public');
        $company = Company::factory()->create();
        $component = Livewire::test(SettingCompanyPage::class, ['company' => $company->fresh()]);
        $fakePhoto = UploadedFile::fake()->image('logo_new_company.jpg');

        $data = [
            'companyName' => 'New company 1',
            'companyEmail' => 'new.email@example.com',
            'companyPhone' => '1234567890',
            'companyAddress' => 'New Company Address',
            'companyWebsite' => 'https://www.newwebsite.com',
            'companyLogo' => $fakePhoto,
        ];

        foreach ($data as $key => $value) {
            $component->set($key, $value);
        }
        $component->call('updateCompany');

        $dataOnDatabase = [
            'name' => 'New company 1',
            'email' => 'new.email@example.com',
            'phone' => '1234567890',
            'address' => 'New Company Address',
            'website' => 'https://www.newwebsite.com',
            'logo_path' => $component->logoPath,
        ];

        unset($data['companyLogo']);
        $this->assertDatabaseHas('companies', $dataOnDatabase);
    }
}
