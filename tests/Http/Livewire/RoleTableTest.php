<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\RoleTable;
use App\Models\User;
use Illuminate\Support\Str;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class RoleTableTest extends TestCase
{
    public $user;

    protected $roleModel;

    /**
     * Set up the test environment before each test method is executed.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();

        // Create a Role instance for testing
        $this->roleModel = Role::create([
            'code' => Str::random(6),
            'name' => Str::random(10),
        ]);

        $this->user->assignRole('godadmin');

    }

    #[Test]
    /**
     * Test the search functionality of the RoleTable component
     * by searching for a role by its name.
     */
    public function role_table_search_by_name(): void
    {
        Livewire::actingAs($this->user)
            ->test(RoleTable::class)
            ->set('search', $this->roleModel->name)
            ->call('searchFilter')
            ->assertSee($this->roleModel->name);
    }

    /**
     * This test verifies that the RoleTable component contains a search box.
     */
    #[Test]
    public function if_search_box_exists_in_role_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(RoleTable::class)
            ->assertSee('search');
    }

    /**
     * This test verifies that the RoleTable component contains the 'edit' and 'delete' buttons.
     */
    #[Test]
    public function if_edit_and_delete_button_exists_in_role_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(RoleTable::class)
            ->assertSee('fa-pencil')     // Check for the Edit button
            ->assertSee('fa-remove');    // Check for the Delete button
    }
}
