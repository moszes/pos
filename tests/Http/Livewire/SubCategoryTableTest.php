<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\SubCategoryTable;
use App\Models\SubCategory;
use App\Models\User;
use Illuminate\Support\Str;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

/**
 * Test class for SubCategoryTable
 */
class SubCategoryTableTest extends TestCase
{
    public $user;

    protected $subCategoryModel;

    /**
     * Set up the test environment before each test method is executed.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();

        // Create a SubCategory instance for testing
        $this->subCategoryModel = SubCategory::create([
            'category_id' => 1,
            'name' => Str::random(10),
            'company_id' => 1,
            'branch_id' => 1,
            'code' => Str::random(5),
        ]);
    }

    #[Test]
    /**
     * Test the search functionality of the SubCategoryTable component
     * by searching for a subCategory by its name.
     */
    public function sub_category_table_search_by_name(): void
    {
        Livewire::actingAs($this->user)
            ->test(SubCategoryTable::class)
            ->set('search', $this->subCategoryModel->name)
            ->call('searchFilter')
            ->assertSee($this->subCategoryModel->name);
    }

    #[Test]
    /**
     * Test the SubCategoryTable component's search functionality by code.
     */
    public function sub_category_table_search_by_code(): void
    {
        Livewire::actingAs($this->user)
            ->test(SubCategoryTable::class)
            ->set('search', $this->subCategoryModel->code)
            ->call('searchFilter')
            ->assertSee($this->subCategoryModel->code);
    }

    /**
     * This test verifies that the SubCategoryTable component contains a search box.
     */
    #[Test]
    public function if_search_box_exists_in_sub_category_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(SubCategoryTable::class)
            ->assertSee('search');
    }

    /**
     * This test verifies that the SubCategoryTable component contains the 'edit' and 'delete' buttons.
     */
    #[Test]
    public function if_edit_and_delete_button_exists_in_sub_category_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(SubCategoryTable::class)
            ->assertSee('fa-pencil')     // Check for the Edit button
            ->assertSee('fa-remove');    // Check for the Delete button
    }
}
