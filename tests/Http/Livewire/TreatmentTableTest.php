<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\ItemDetail;
use App\Http\Livewire\TreatmentTable;
use App\Models\Treatment;
use App\Models\User;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class TreatmentTableTest extends TestCase
{
    public $user;

    protected $treatmentModel;

    /**
     * Set up the test environment before each test method is executed.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();

        $this->treatmentModel = Treatment::first();

        auth()->login($this->user);
    }

    #[Test]
    /**
     * Test the search functionality of the TreatmentTable component
     * by searching for a treatment by its name.
     */
    public function treatment_table_search_by_name(): void
    {
        Livewire::actingAs($this->user)
            ->test(ItemDetail::class, ['item' => $this->treatmentModel])
            ->set('search', $this->treatmentModel->name)
            ->assertSee($this->treatmentModel->name);
    }

    #[Test]
    /**
     * Test the TreatmentTable component's search functionality by code.
     */
    public function treatment_table_search_by_code(): void
    {
        Livewire::actingAs($this->user)
            ->test(ItemDetail::class, ['item' => $this->treatmentModel])
            ->set('search', $this->treatmentModel->code)
            ->assertSee($this->treatmentModel->code);
    }

    /**
     * This test verifies that the TreatmentTable component contains a search box.
     */
    #[Test]
    public function if_search_box_exists_in_treatment_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(TreatmentTable::class)
            ->assertSee('search');
    }

    /**
     * This test verifies that the TreatmentTable component contains the 'edit' and 'delete' buttons.
     */
    #[Test]
    public function if_edit_and_delete_button_exists_in_treatment_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(TreatmentTable::class)
            ->assertSee('fa-pencil')     // Check for the Edit button
            ->assertSee('fa-remove');    // Check for the Delete button
    }
}
