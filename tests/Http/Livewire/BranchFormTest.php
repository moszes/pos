<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\BranchForm;
use App\Models\Branch;
use App\Models\User;
use Livewire;
use Tests\TestCase;

class BranchFormTest extends TestCase
{
    public $branch;

    /**
     * Set up the test environment before each test case.
     *
     * This function logs in the first user and retrieves the first branch from the database.
     */
    public function setUp(): void
    {
        parent::setUp();

        auth()->login(User::first());

        $this->branch = Branch::first();
    }

    public function testStore()
    {
        Livewire::test(BranchForm::class)
            ->set('code', 'test-code')
            ->set('name', 'test-name')
            ->call('store')
            ->assertEmitted('clearBranchForm');
    }

    /**
     * Test the editBranch method of the BranchForm Livewire component.
     *
     * This test verifies that the editBranch method emits the 'editBranch' event
     * and that the branch with the specified ID is retrieved.
     *
     * @return void
     */
    public function testEditBranch()
    {
        Livewire::test(BranchForm::class)->call('editBranch', 1)->assertEmitted('showBranchForm');
    }

    public function testUpdate()
    {
        Livewire::test(BranchForm::class)
            ->set('branch', $this->branch)
            ->set('code', 'updated-code')
            ->set('name', 'updated-name')
            ->call('update')
            ->assertEmitted('clearBranchForm');

    }

    /**
     * Test the destroy method of the BranchForm Livewire component.
     *
     * This test verifies that the destroy method emits the 'clearBranchForm' event
     * and that the branch with the specified ID is soft deleted.
     *
     * @return void
     */
    public function testDestroy()
    {
        Livewire::test(BranchForm::class)->call('destroy', 1)->assertEmitted('clearBranchForm');

        $this->assertSoftDeleted('branches', ['code' => $this->branch->code.'-deleted']);
    }
}
