<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\TreatmentForm;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Treatment;
use App\Models\User;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class TreatmentFormTest extends TestCase
{
    public $treatment;

    /**
     * Set up the test environment before each test case.
     *
     * This method creates a new Treatment instance using the factory method and assigns it to the $this->treatment property.
     * It also logs in the first User using the auth()->login() method.
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->treatment = Treatment::factory()->create();

        auth()->login(User::first());
    }

    /**
     * Test the store method of the TreatmentForm class.
     *
     * This test case verifies that the store method of the TreatmentForm class correctly creates a new treatment
     * record in the database. It uses Livewire to simulate a call to the store method and asserts that the treatment
     * record has been created with the provided data.
     *
     * @return void
     */
    public function testStore()
    {
        Livewire::test(TreatmentForm::class)
            ->set('categoryId', Category::first()->id)
            ->set('subCategoryId', SubCategory::first()->id)
            ->set('code', 'test-code')
            ->set('name', 'test-name')
            ->set('price', rand(1, 10000))
            ->call('store')
            ->assertEmitted('clearTreatmentForm');

        $this->assertDatabaseHas('treatments', [
            'code' => 'test-code',
            'name' => 'test-name',
        ]);
    }

    /**
     * Test the destroy method of the TreatmentForm class.
     *
     * This test case verifies that the destroy method of the TreatmentForm class correctly softly deletes a treatment
     * record in the database. It uses Livewire to simulate a call to the destroy method with the ID of a treatment
     * instance created in the setUp method. After the destroy method is called, it asserts that the treatment record
     * has been softly deleted and its code has been updated with the '-deleted' suffix.
     *
     * @return void
     */
    public function testDestroy()
    {
        Livewire::test(TreatmentForm::class)
            ->call('destroy', $this->treatment->id);

        $this->assertSoftDeleted($this->treatment, ['code' => $this->treatment->code.'-deleted']);
    }

    /**
     * Test that the function updates a treatment.
     *
     * This test case verifies that the update method of the TreatmentForm class correctly updates a treatment
     * record in the database. It uses Livewire to simulate a call to the update method and asserts that the treatment
     * record has been updated with the provided data. It also asserts that the 'clearTreatmentForm' event is emitted.
     */
    #[Test]
    public function it_updates_a_treatment(): void
    {
        Livewire::test(TreatmentForm::class)
            ->set('treatment', $this->treatment)
            ->set('code', 'test-code-updated')
            ->set('name', 'test-name-updated')
            ->call('update')
            ->assertEmitted('clearTreatmentForm');

        $this->assertDatabaseHas('treatments', [
            'code' => 'test-code-updated',
            'name' => 'test-name-updated',
        ]);
    }
}
