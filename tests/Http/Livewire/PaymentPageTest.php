<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\PaymentTable;
use App\Models\User;
use Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class PaymentPageTest extends TestCase
{
    public $user;

    /**
     * Set up the test case by calling the parent's setUp method and
     * seeding the database with RoleSeeder and PermissionSeeder.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();
    }

    /**
     * Test that the title of the payments page is "Payment".
     */
    #[Test]
    public function title_page_is_payments(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('master.payments'));

        $response->assertStatus(200);

        Livewire::test(PaymentTable::class, ['initialTitlePage' => 'Payment'])
            ->assertSet('titlePage', 'Payment');
    }

    /**
     * Assesses if an authenticated user can see the page.
     */
    #[Test]
    public function authenticated_user_can_see_the_page()
    {
        $response = $this->actingAs($this->user)->get(route('master.payments'));

        $response->assertStatus(200);
    }

    /**
     * Assesses if a non-authenticated user cannot see the page.
     */
    #[Test]
    public function non_authenticated_user_cannot_see_the_page()
    {
        $response = $this->get(route('master.payments'));

        $response->assertStatus(401);
    }
}
