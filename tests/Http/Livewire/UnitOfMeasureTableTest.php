<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\UnitOfMeasureTable;
use App\Models\UnitOfMeasure;
use App\Models\User;
use Illuminate\Support\Str;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class UnitOfMeasureTableTest extends TestCase
{
    public $user;

    protected $unitModel;

    /**
     * Set up the test environment before each test method is executed.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();

        // Create a UnitOfMeasure instance for testing
        $this->unitModel = UnitOfMeasure::create([
            'name' => Str::random(10),
            'company_id' => 1,
            'code' => Str::random(5),
        ]);
    }

    #[Test]
    /**
     * Test the search functionality of the UnitOfMeasureTable component
     * by searching for a unit by its name.
     */
    public function unit_table_search_by_name(): void
    {
        Livewire::actingAs($this->user)
            ->test(UnitOfMeasureTable::class)
            ->set('search', $this->unitModel->name)
            ->call('searchFilter')
            ->assertSee($this->unitModel->name);
    }

    #[Test]
    /**
     * Test the UnitOfMeasureTable component's search functionality by code.
     */
    public function unit_table_search_by_code(): void
    {
        Livewire::actingAs($this->user)
            ->test(UnitOfMeasureTable::class)
            ->set('search', $this->unitModel->code)
            ->call('searchFilter')
            ->assertSee($this->unitModel->code);
    }

    /**
     * This test verifies that the UnitOfMeasureTable component contains a search box.
     */
    #[Test]
    public function if_search_box_exists_in_unit_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(UnitOfMeasureTable::class)
            ->assertSee('search');
    }

    /**
     * This test verifies that the UnitOfMeasureTable component contains the 'edit' and 'delete' buttons.
     */
    #[Test]
    public function if_edit_and_delete_button_exists_in_unit_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(UnitOfMeasureTable::class)
            ->assertSee('fa-pencil')     // Check for the Edit button
            ->assertSee('fa-remove');    // Check for the Delete button
    }
}
