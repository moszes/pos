<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\SubscriptionPlanForm;
use App\Models\SubscriptionPlan;
use App\Models\User;
use Livewire\Livewire;
use Tests\TestCase;

class SubscriptionPlanFormTest extends TestCase
{
    public $subscriptionPlan;

    /**
     * Set up the test environment before each test case.
     *
     * This function is called before each test case is executed. It performs the following steps:
     * 1. Calls the parent's setUp() method to initialize the test environment.
     * 2. Creates a new user using the User factory and assigns it to the $user property.
     * 3. Authenticates the user using the actingAs() method.
     * 4. Retrieves the first subscription plan from the database and assigns it to the $subscriptionPlan property.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
        $this->actingAs($this->user);

        $this->subscriptionPlan = SubscriptionPlan::first();
    }

    /**
     * Test the store method of the SubscriptionPlanForm Livewire component.
     *
     * This test simulates a user calling the store method of the SubscriptionPlanForm
     * Livewire component with valid data. It then asserts that the corresponding
     * subscription plan in the database has been created with the provided data.
     *
     * @return void
     */
    public function testStore()
    {
        Livewire::test(SubscriptionPlanForm::class)->set('actionForm', 'store')->set('code', 'test-code')->set('name', 'test-name')->set('price', rand(100, 1000))->call('store');

        $this->assertDatabaseHas('subscription_plans', ['code' => $this->subscriptionPlan->code, 'name' => $this->subscriptionPlan->name, 'price' => $this->subscriptionPlan->price]);
    }

    /**
     * Test the edit method of the SubscriptionPlanForm Livewire component.
     *
     * This test simulates a user calling the edit method of the SubscriptionPlanForm
     * Livewire component with an ID of 1. It then asserts that the corresponding
     * subscription plan in the database has been retrieved and assigned to the
     * $subscriptionPlan property.
     *
     * @return void
     */
    public function testEdit()
    {
        Livewire::test(SubscriptionPlanForm::class)->call('edit', 1);

        $this->assertNotNull($this->subscriptionPlan);
    }

    /**
     * Test the update method of the SubscriptionPlanForm Livewire component.
     *
     * This test simulates a user calling the update method of the SubscriptionPlanForm
     * Livewire component with valid data. It then asserts that the corresponding
     * subscription plan in the database has been updated with the provided data.
     *
     * @return void
     */
    public function testUpdate()
    {
        Livewire::test(SubscriptionPlanForm::class)
            ->set('actionForm', 'update')
            ->set('subscriptionPlan', $this->subscriptionPlan)
            ->set('code', 'test-code')->set('name', 'test-name')
            ->set('price', rand(100, 1000))
            ->call('update');

        $this->subscriptionPlan = SubscriptionPlan::first();

        $this->assertDatabaseHas('subscription_plans', ['code' => $this->subscriptionPlan->code, 'name' => $this->subscriptionPlan->name, 'price' => $this->subscriptionPlan->price]);
    }

    /**
     * Test the destroy method of the SubscriptionPlanForm Livewire component.
     *
     * This test simulates a user calling the destroy method of the SubscriptionPlanForm
     * Livewire component with an ID of 1. It then asserts that the corresponding
     * subscription plan in the database has been softly deleted with the code
     * appended with '-deleted'.
     *
     * @return void
     */
    public function testDestroy()
    {
        Livewire::test(SubscriptionPlanForm::class)->set('actionForm', 'destroy')->call('destroy', 1);

        $this->assertSoftDeleted('subscription_plans', ['code' => $this->subscriptionPlan->code.'-deleted']);
    }
}
