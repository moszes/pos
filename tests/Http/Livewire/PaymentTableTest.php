<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\PaymentTable;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Support\Str;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

/**
 * Test Class for PaymentTable class
 */
class PaymentTableTest extends TestCase
{
    public $user;

    protected $paymentModel;

    /**
     * Set up the test environment before each test method is executed.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();

        // Create a Payment instance for testing
        $this->paymentModel = Payment::create([
            'name' => Str::random(10),
            'company_id' => 1,
            'branch_id' => 1,
            'code' => Str::random(5),
        ]);
    }

    #[Test]
    /**
     * Test the search functionality of the PaymentTable component
     * by searching for a payment by its name.
     */
    public function payment_table_search_by_name(): void
    {
        Livewire::actingAs($this->user)
            ->test(PaymentTable::class)
            ->set('search', $this->paymentModel->name)
            ->call('searchFilter')
            ->assertSee($this->paymentModel->name);
    }

    #[Test]
    /**
     * Test the PaymentTable component's search functionality by code.
     */
    public function payment_table_search_by_code(): void
    {
        Livewire::actingAs($this->user)
            ->test(PaymentTable::class)
            ->set('search', $this->paymentModel->code)
            ->call('searchFilter')
            ->assertSee($this->paymentModel->code);
    }

    /**
     * This test verifies that the PaymentTable component contains a search box.
     */
    #[Test]
    public function if_search_box_exists_in_payment_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(PaymentTable::class)
            ->assertSee('search');
    }

    /**
     * This test verifies that the PaymentTable component contains the 'edit' and 'delete' buttons.
     */
    #[Test]
    public function if_edit_and_delete_button_exists_in_payment_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(PaymentTable::class)
            ->assertSee('fa-pencil')     // Check for the Edit button
            ->assertSee('fa-remove');    // Check for the Delete button
    }
}
