<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\CategoryTable;
use App\Models\Category;
use App\Models\User;
use Illuminate\Support\Str;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class CategoryTableTest extends TestCase
{
    public $user;

    protected $categoryModel;

    /**
     * Set up the test environment before each test method is executed.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();

        // Create a Category instance for testing
        $this->categoryModel = Category::create([
            'company_id' => 1,
            'branch_id' => 1,
            'code' => Str::random(5),
            'name' => Str::random(10),
            'type' => 'treatment',
        ]);
    }

    #[Test]
    /**
     * Test the search functionality of the CategoryTable component
     * by searching for a category by its name.
     */
    public function category_table_search_by_name(): void
    {
        Livewire::actingAs($this->user)
            ->test(CategoryTable::class)
            ->set('search', $this->categoryModel->name)
            ->call('searchFilter')
            ->assertSee($this->categoryModel->name);
    }

    #[Test]
    /**
     * Test the CategoryTable component's search functionality by code.
     */
    public function category_table_search_by_code(): void
    {
        Livewire::actingAs($this->user)
            ->test(CategoryTable::class)
            ->set('search', $this->categoryModel->code)
            ->call('searchFilter')
            ->assertSee($this->categoryModel->code);
    }

    #[Test]
    /**
     * Test the category table search by type.
     */
    public function category_table_search_by_type(): void
    {
        Livewire::actingAs($this->user)
            ->test(CategoryTable::class)
            ->set('search', $this->categoryModel->type)
            ->call('searchFilter')
            ->assertSee($this->categoryModel->type);
    }

    /**
     * This test verifies that the CategoryTable component contains a search box.
     */
    #[Test]
    public function if_search_box_exists_in_category_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(CategoryTable::class)
            ->assertSee('search');
    }

    /**
     * This test verifies that the CategoryTable component contains the 'edit' and 'delete' buttons.
     */
    #[Test]
    public function if_edit_and_delete_button_exists_in_category_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(CategoryTable::class)
            ->assertSee('fa-pencil')     // Check for the Edit button
            ->assertSee('fa-remove');    // Check for the Delete button
    }
}
