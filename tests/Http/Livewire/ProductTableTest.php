<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\ItemDetail;
use App\Http\Livewire\ProductTable;
use App\Models\Product;
use App\Models\User;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class ProductTableTest extends TestCase
{
    public $user;

    protected $productModel;

    /**
     * Set up the test environment before each test method is executed.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();

        $this->productModel = Product::first();
    }

    #[Test]
    /**
     * Test the search functionality of the ProductTable component
     * by searching for a product by its name.
     */
    public function product_table_search_by_name(): void
    {
        Livewire::actingAs($this->user)
            ->test(ItemDetail::class, ['item' => $this->productModel])
            ->set('search', $this->productModel->name)
            ->assertSee($this->productModel->name);
    }

    #[Test]
    /**
     * Test the ProductTable component's search functionality by code.
     */
    public function product_table_search_by_code(): void
    {
        Livewire::actingAs($this->user)
            ->test(ItemDetail::class, ['item' => $this->productModel])
            ->set('search', $this->productModel->code)
            ->assertSee($this->productModel->code);
    }

    /**
     * This test verifies that the ProductTable component contains a search box.
     */
    #[Test]
    public function if_search_box_exists_in_product_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(ProductTable::class)
            ->assertSee('search');
    }

    /**
     * This test verifies that the ProductTable component contains the 'edit' and 'delete' buttons.
     */
    #[Test]
    public function if_edit_and_delete_button_exists_in_product_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(ProductTable::class)
            ->assertSee('fa-pencil')     // Check for the Edit button
            ->assertSee('fa-remove');    // Check for the Delete button
    }
}
