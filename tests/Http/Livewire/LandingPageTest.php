<?php

namespace Tests\Http\Livewire;

use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class LandingPageTest extends TestCase
{
    #[Test]
    /**
     * Tests that the landing page can be accessed.
     */
    public function can_see_landing_page(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    #[Test]
    /**
     * Tests that the login text is visible on the login page.
     */
    public function can_see_login_text(): void
    {
        $response = $this->get('/login');

        $response->assertSeeText('Log In');
    }

    #[Test]
    /**
     * Tests that the login route returns a 200 status code.
     */
    public function can_see_login_route(): void
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }

    #[Test]
    /**
     * Tests that the "Forgot the password?" text is visible on the landing page.
     */
    public function can_see_forgot_password_text(): void
    {
        $response = $this->get('/');

        $response->assertSeeText('Forgot the password?');
    }

    #[Test]
    /**
     * Tests that the forget password route returns a 200 status code.
     */
    public function can_see_forget_password_route(): void
    {
        $response = $this->get('/forget_password');

        $response->assertStatus(200);
    }

    #[Test]
    /**
     * Tests that the "Register" text is visible on the landing page.
     */
    public function can_see_register_text(): void
    {
        $response = $this->get('/');

        $response->assertSeeText('Register');
    }

    #[Test]
    /**
     * Tests that the company register route returns a 200 status code.
     */
    public function can_see_company_register_route(): void
    {
        $response = $this->get('/company_register');

        $response->assertStatus(200);
    }

    #[Test]
    /**
     * Tests that the company data page returns a 200 status code.
     */
    public function can_see_company_data_page(): void
    {
        $response = $this->get('/company_data');
        $response->assertStatus(200);
    }

    #[Test]
    /**
     * Tests that the "Company" text is visible on the company data page.
     */
    public function can_see_company_text_on_company_data_page(): void
    {
        $response = $this->get('/company_data');

        $response->assertSeeText('Company');
    }
}
