<?php

namespace Tests\Http\Livewire;

use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class BaseDataTest extends TestCase
{
    /**
     * Set up a user for functional testing.
     */
    protected function setUpUser(): void
    {
        $user = User::first();
        auth()->login($user);
    }

    /**
     * Array of views to check authenticated and unauthenticated access
     *
     * @var array
     */
    protected $views = [
        'master.categories',
        'master.sub_categories',
        'master.rooms',
        'master.vendors',
        'master.unit_of_measures',
        'master.payments',
    ];

    /**
     * If logged-in user can see pages.
     */
    #[Test]
    public function loggedInUserCanViewPages(): void
    {
        $this->setUpUser();
        foreach ($this->views as $view) {
            $response = $this->get(route($view));
            $response->assertStatus(200);
        }
    }

    /**
     * If non-logged-in user gets 401 error when viewing pages.
     *
     * @return void
     */
    #[Test]
    public function nonAuthenticatedUserCannotViewPages()
    {
        foreach ($this->views as $view) {
            $response = $this->get(route($view));
            $response->assertStatus(401);
        }
    }
}
