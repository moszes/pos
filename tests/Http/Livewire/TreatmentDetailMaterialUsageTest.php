<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\TreatmentDetailMaterialUsage;
use App\Models\Treatment;
use App\Models\User;
use Tests\TestCase;

class TreatmentDetailMaterialUsageTest extends TestCase
{
    public $user;

    protected $treatmentModel;

    /**
     * Set up the test environment before each test method is executed.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();

        $this->treatmentModel = Treatment::first();
    }

    /**
     * @test
     * Test that the render method in the TreatmentDetailMaterialUsage class returns the correct view
     */
    public function it_renders_the_correct_view_with_the_expected_data(): void
    {
        // Arrange
        $treatmentDetailMaterialUsageComponent = new TreatmentDetailMaterialUsage;

        // Act
        $view = $treatmentDetailMaterialUsageComponent->render();

        // Assert
        $this->assertEquals('livewire.treatment-detail-material-usage', $view->name());
    }

    /**
     * @test
     * Test that the render method in the TreatmentDetailMaterialUsage class sends the correct data to the view
     */
    public function it_sends_the_correct_data_to_the_view(): void
    {
        // Arrange
        $treatmentDetailMaterialUsageComponent = new TreatmentDetailMaterialUsage;

        // Act
        $view = $treatmentDetailMaterialUsageComponent->render();
        $view->with([
            'treatmentMaterialUsages' => $this->treatmentModel->material_usages->toArray(),
        ]);

        // Assert
        $this->assertEquals($this->treatmentModel->material_usages->toArray(), $view->getData()['treatmentMaterialUsages']);
    }
}
