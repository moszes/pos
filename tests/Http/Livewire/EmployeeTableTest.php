<?php

namespace Tests\Http\Livewire;

use App\Http\Livewire\UserTable;
use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Support\Str;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class EmployeeTableTest extends TestCase
{
    public $user;

    protected $userModel;

    /**
     * Set up the test environment before each test method is executed.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();

        // Create a User instance for testing
        $this->userModel = User::create([
            'name' => Str::random(10),
            'password' => bcrypt('treatment'),
            'email' => Str::random(10).'@gmail.com',
        ]);

        $this->userModel->assignRole('sales');

        UserDetail::create([
            'user_id' => $this->userModel->id,
            'company_id' => 1,
            'branch_id' => 1,
            'code' => Str::random(10),
            'phone' => Str::random(10),
        ]);
    }

    #[Test]
    /**
     * Test the search functionality of the UserTable component
     * by searching for a user by its name.
     */
    public function user_table_search_by_name(): void
    {
        Livewire::actingAs($this->user)
            ->test(UserTable::class)
            ->set('search', $this->userModel->name)
            ->call('searchFilter')
            ->assertSee($this->userModel->name);
    }

    #[Test]
    /**
     * Test the UserTable component's search functionality by code.
     */
    public function user_table_search_by_code(): void
    {
        Livewire::actingAs($this->user)
            ->test(UserTable::class)
            ->set('search', $this->userModel->details->code)
            ->call('searchFilter')
            ->assertSee($this->userModel->details->code);
    }

    #[Test]
    /**
     * Test the UserTable component's search functionality by code.
     */
    public function user_table_search_by_phone(): void
    {
        Livewire::actingAs($this->user)
            ->test(UserTable::class)
            ->set('search', $this->userModel->details->phone)
            ->call('searchFilter')
            ->assertSee($this->userModel->details->phone);
    }

    /**
     * This test verifies that the UserTable component contains a search box.
     */
    #[Test]
    public function if_search_box_exists_in_user_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(UserTable::class)
            ->assertSee('search');
    }

    /**
     * This test verifies that the UserTable component contains the 'edit' and 'delete' buttons.
     */
    #[Test]
    public function if_edit_and_delete_button_exists_in_user_table(): void
    {
        Livewire::actingAs($this->user)
            ->test(UserTable::class)
            ->assertSee('fa-pencil')     // Check for the Edit button
            ->assertSee('fa-remove');    // Check for the Delete button
    }
}
