<?php

namespace Tests\Http\Livewire;

use App\Models\User;
use Tests\TestCase;

class UserProfileTest extends TestCase
{
    public $user;

    /**
     * Sets up the test environment by seeding the database with roles and permissions.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();

        auth()->login($this->user);
    }

    /**
     * Test to verify that a logged-in user can view the profile.
     *
     * @return void
     */
    public function test_logged_in_user_can_view_profile()
    {
        $response = $this->get(route('user.profile', ['userCode' => $this->user->details->code]));

        $response->assertStatus(200);
    }

    /**
     * Test to verify that a not authenticated user can't view the profile and gets a 401 status code.
     *
     * @return void
     */
    public function test_not_authenticated_user_cannot_view_profile()
    {
        auth()->logout();
        $response = $this->get(route('user.profile'));

        $response->assertStatus(401);
    }
}
