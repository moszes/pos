<?php

namespace Tests\Http\Livewire;

use App\Events\UserEvent;
use App\Http\Livewire\LoginPage;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Session;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class LoginPageTest extends TestCase
{
    /**
     * Sets up the test environment by seeding the database with roles and permissions.
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Validates if the user is authenticated and redirected to the dashboard.
     */
    #[Test]
    public function loginSuccess()
    {
        User::factory()->create(['name' => 'TestUser', 'password' => bcrypt('password')]);

        // Prevent event broadcast
        Event::fake();

        $testComponent = Livewire::test(LoginPage::class)->set('name', 'TestUser')->set('password', 'password')->call('login');

        $this->assertTrue(Auth::check());
        Event::assertDispatched(UserEvent::class);
        $testComponent->assertRedirect('/dashboard'); // Ensure redirection to the dashboard
    }

    /**
     * Validates if login fails and an error message is thrown.
     */
    #[Test]
    public function loginFailure()
    {
        User::factory()->create(['name' => 'TestUser', 'password' => bcrypt('password')]);

        // Prevent event broadcast
        Event::fake();

        Livewire::test(LoginPage::class)->set('name', 'TestUser')
            ->set('password', 'wrong password')->call('login');

        $this->assertFalse(Auth::check());
        $this->assertEmpty(Session::get('url.intended'));
    }
}
