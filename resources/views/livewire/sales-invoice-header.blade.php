<div class="col card">
  <div class="card-body d-flex gap-3">
    <div class="col d-flex flex-column gap-3">
      <div class="d-flex justify-content-between gap-3">
        <div>
          Sales Invoice Number
        </div>
        <div>
          {{ @$salesInvoice->sales_invoice_number }}
        </div>
      </div>
      <div class="d-flex justify-content-between gap-3">
        <div>
          Date
        </div>
        <div>
          {{ @dateFormat($salesInvoice->sales_invoice_date) }}
        </div>
      </div>
      <div class="d-flex justify-content-between gap-3">
        <div>
          Customer
        </div>
        <div>
          {{ $salesInvoice?->customer->details->first_name }}
        </div>
      </div>
      <div class="d-flex justify-content-between gap-3">
        <div>
          Phone
        </div>
        <div>
          {{ $salesInvoice?->customer->details->phone }}
        </div>
      </div>
      <div class="d-flex justify-content-between gap-3">
        <div>
          Address
        </div>
        <div>
          {{ $salesInvoice?->customer->details->address }}
        </div>
      </div>
    </div>
    <div class="col d-flex flex-column gap-3">
      <div class="d-flex justify-content-between gap-3">
        <div>
          Release By
        </div>
        <div>
          {{ @getUserById($salesInvoice->release_by)->details->first_name }}
        </div>
      </div>
      <div class="d-flex justify-content-between gap-3">
        <div>
          Release Date
        </div>
        <div>
          {{ $salesInvoice?->release_date }}
        </div>
      </div>
    </div>
    <div class="col d-flex flex-column gap-3">
      <div class="d-flex justify-content-between gap-3">
        <div>
          Sub Total
        </div>
        <div>
          {{ @numberFormat($salesInvoice?->items->sum('total_price')) }}
        </div>
      </div>
      <div class="d-flex justify-content-between gap-3">
        <div>
          Tax
        </div>
        <div>
          {{ @numberFormat($salesInvoice->tax) }}
        </div>
      </div>
      <div class="d-flex justify-content-between gap-3">
        <div>
          Others
        </div>
        <div>
          {{ @numberFormat($salesInvoice->others) }}
        </div>
      </div>
      <div class="d-flex justify-content-between gap-3">
        <div>
          Discount
        </div>
        <div>
          {{ @numberFormat($salesInvoice->discount) }}
        </div>
      </div>
      <div class="d-flex justify-content-between gap-3">
        <div>
          Grand Total
        </div>
        <div>
          {{ @numberFormat($salesInvoice->grand_total) }}
        </div>
      </div>
    </div>
  </div>
</div>
