<div class="shadow rounded">
	<livewire:breadcrumb />
	<div class="card">
		<div class="card-body">
			<livewire:toast-notification/>
			<div class="d-flex flex-column">
				<livewire:setting-branch-table />
			</div>
			<div class="loading d-none" wire:loading.class.remove="d-none"></div>
		</div>
	</div>
</div>

@push('scripts')
	<script>
		document.addEventListener("DOMContentLoaded", () => {
			Livewire.emit("openSettingModule");
			Livewire.emit("openBaseDataMenu")
		});
	</script>
@endpush