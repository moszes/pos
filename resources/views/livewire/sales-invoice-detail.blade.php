<div wire:ignore.self class="modal modal-xl fade" id="salesInvoiceDetail" tabindex="-1" aria-hidden="true">
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ $formTitle }}</h5>
        <button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close">
          <span class="fas fa-times fs--1"></span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card mb-3">
          <div class="card-body">
            <div class="d-flex mb-1 gap-3">
              <div class="col d-flex flex-column">
                <div class="d-flex justify-content-between">
                  <p class="fw-bold">Sales Invoice Number</p>
                  <a href="{{ route('tools.salesInvoiceTool') . '?searchSalesInvoiceNumber='. @$salesInvoice->sales_invoice_number }}" wire:click="$emit('copyToClipboard', '{{ @$salesInvoice->sales_invoice_number }}')">{{ @$salesInvoice->sales_invoice_number }}</a>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="fw-bold">Created By</p>
                  <p>{{ @getUserById($salesInvoice->created_by)->name }}</p>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="fw-bold">Created Date</p>
                  <p>{{ @dateFormat($salesInvoice->sales_invoice_date) }}</p>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="fw-bold">Customer Name</p>
                  <p>{{ @getCustomerById($salesInvoice->customer_id)['details']['first_name'] }}</p>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="fw-bold">Customer Phone</p>
                  <p class="text-end">{{ @getCustomerPhone($salesInvoice->customer_id) }}</p>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="fw-bold text-nowrap">Customer Address</p>
                  <p class="text-end">{{ @getCustomerAddress($salesInvoice->customer_id) }}</p>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="fw-bold text-nowrap">Note</p>
                  <p class="text-end">{{ @$salesInvoice->note }}</p>
                </div>
              </div>
              <div class="col d-flex flex-column">
                <div class="d-flex justify-content-between">
                  <p class="fw-bold">Release By</p>
                  <p>{{ @getUserById($salesInvoice->release_by)->name }}</p>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="fw-bold">Release Date</p>
                  <p>{{ @dateFormat($salesInvoice->release_date) }}</p>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="fw-bold">Approved By</p>
                  <p>{{ @getUserById(getSalesInvoiceApproval($salesInvoice->id)->approve_by)->name }}</p>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="fw-bold">Approved Date</p>
                  <p>{{ @dateFormat(getSalesInvoiceApproval($salesInvoice->id)->approve_date) }}</p>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="fw-bold">Discount</p>
                  <p>{{ @numberFormat($salesInvoice->discount) }}</p>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="fw-bold">Others</p>
                  <p>{{ @numberFormat($salesInvoice->others) }}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="table-responsive scrollbar">
          <h5 class="text-center mb-3">Sales Invoice Items</h5>
          <livewire:sales-invoice-item-table />
        </div>
        <div class="table-responsive scrollbar">
          <h5 class="text-center my-3">Sales Invoice Payment</h5>
          <table class="table table-hover table-bordered fs--1 mb-0 ">
            <thead>
              <tr>
                <th class="sort px-3" scope="col">Payment Method</th>
                <th class="sort px-3" scope="col">Reference Number</th>
                <th class="sort text-center" scope="col">Status</th>
                <th class="sort px-2 text-end" scope="col">Amount</th>
              </tr>
            </thead>
            <tbody class="list">
              @if($payments)
              @forelse($payments as $payment)
              <tr>
                <td class="px-3">{{ @getPaymentById($payment->payment_id)->name }}</td>
                <td class="px-3">{{ @$payment->reference_number }}</td>
                <td class="text-center">{{ @$payment->status }}</td>
                <td class="text-end px-2">{{ @numberFormat($payment->amount) }}</td>
              </tr>
              @empty
              <tr>
                <td colspan="4" class="text-center">Empty Data</td>
              </tr>
              @endforelse
              @endif
              <tr>
                <td colspan="3" class="text-end px-3">Exchange</td>
                <td class="text-end px-2  @if($totalPayment - @$salesInvoice->grand_total < 0) text-danger @endif">{{ numberFormat( $totalPayment - @$salesInvoice->grand_total) }}</td>
              </tr>
              <tr>
                <td colspan="3" class="text-end px-3">Total Payment</td>
                <td class="text-end px-2">{{ numberFormat( $totalPayment ) }}</td>
              </tr>
            </tbody>
          </table>
          <div class="d-flex gap-3 mt-3 justify-content-end">
            @if($hasDiscount)
              @if(auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
                <button class="btn btn-primary" wire:click="approveDiscount('{{@$salesInvoice->id}}')">Approve</button>
              @else
                <div class="text-danger">Please ask your manager to approve a discount on this invoice!</div>
              @endif
            @elseif(@!$salesInvoice->release_by)
              <button type="button" wire:click="$emit('releaseSalesInvoice', '{{@$salesInvoice->id}}')" class="btn btn-success">Release</button>
            @else
              <a href="http://clinic.local/print/{{$salesInvoice->id}}" class="btn btn-info">Receipt Print</a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>
  let salesInvoiceDetail = new bootstrap.Modal(
    document.getElementById('salesInvoiceDetail'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('needMorePayment', () => {
	  Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Please complete the payment first!',
      confirmButtonText: 'Okay'
    })
  });
  Livewire.on('showSalesInvoiceDetails', () => {
    salesInvoiceDetail.show();
  })
  Livewire.on('hideSalesInvoiceDetails', () => {
    salesInvoiceDetail.hide();
  })
  Livewire.on('copyToClipboard', (text) => {
    console.log(text)
    copyToClipboard(text);
  })
  document.getElementById('salesInvoiceDetail').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearSalesInvoiceDetails');
    Livewire.emit('clearSalesInvoiceForm');
    Livewire.emit('clearSalesInvoiceDetailModal');
  })
</script>
@endpush