<table class="table table-hover fs--1">
	<thead>
    <tr>
      <th scope="col">Action</th>
      <th scope="col">Name</th>
    </tr>
	</thead>
	<tbody>
	@foreach ($branch_settings as $branch_setting)
		<tr>
			<td>{{ $branch_setting->id }}</td>
			<td>{{ $branch_setting->name }}</td>
			<td>
				<button class="btn btn-primary" wire:click="edit({{ $branch_setting->id }})">Edit</button>
				<button class="btn btn-danger" wire:click="delete({{ $branch_setting->id }})">Delete</button>
			</td>
		</tr>
	@endforeach
	</tbody>
</table>