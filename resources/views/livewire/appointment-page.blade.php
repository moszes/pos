<div>
  <livewire:breadcrumb />
    <div class="d-flex gap-3">
      <div class="col-5 shadow rounded">
        <div class="card">
          <div class="card-body">
            <livewire:appointment-sidebar :customer="$customer"/>
          </div>
        </div>
      </div>
      <div class="col-7 shadow">
        <div class="card">
          <div class="card-body">
            @if($addToCart)
              <livewire:appointment-cart :formAction="$formAction" :appointment="$appointment" :items="$items" :customer="$customer"/>
            @else
              <livewire:appointment-table/>
            @endif
          </div>
        </div>
      </div>
    </div>
</div>