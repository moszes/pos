<div class="d-flex flex-column gap-3">
  <table class="table table-hover">
    <thead>
      <tr>
        <th class="px-3">Item</th>
        <th class="px-3 text-center">Qty</th>
        <th class="px-3 text-end">Normal Price</th>
        <th class="px-3 text-end">Sell Price</th>
        <th class="px-3 text-end">Total</th>
      </tr>
    </thead>
    <tbody>
    @forelse($userTransactions as $key => $userTransaction)
      @php($sellPrice = getSellPrice($userTransaction->id))
      @php($salesInvoice = getSalesInvoiceById($userTransaction->sales_invoice_id))
      <tr>
        <td class="px-3 text-nowrap">
          @if($salesInvoice)
          {{ date('Y-m-d', strtotime(@getSalesInvoiceById($userTransaction->sales_invoice_id)->sales_invoice_date)) }}
          @else
          {{ $userTransaction->created_at->format('Y-m-d') }}
          @endif
          @if($salesInvoice)
          <p> Sales Invoice No.: {{ @getSalesInvoiceById($userTransaction->sales_invoice_id)->sales_invoice_number }} @hasrole('godadmin') ({{$userTransaction->sales_invoice_id}}) @endhasrole</p>
          @if($userTransaction->appointment_id)<p> Appointment No.: {{ @getAppointmentById($userTransaction->appointment_id)->appointment_number }}</p>@endif
          @endif
          <p>{{ $userTransaction->module_code }}</p>
          <p>{{ substr($userTransaction->module_name, 0, 30) }}</p>
        </td>
        <td class="px-3 text-center">{{ @numberFormat($userTransaction->qty) }}</td>
        <td class="px-3 text-end">{{ @numberFormat($userTransaction->treatment->price) }}</td>
        <td class="px-3 text-end">{{ @numberFormat($sellPrice) }}</td>
        <td class="px-3 text-end">{{ @numberFormat($userTransaction->qty * $sellPrice) }}</td>
      </tr>
    @empty
      <tr>
        <td colspan="7" class="text-center">Empty Data</td>
      </tr>
    @endforelse
    </tbody>
  </table>
</div>