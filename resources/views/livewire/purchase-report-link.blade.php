<li class="nav-item">
  <a class="nav-link dropdown-indicator" href="#nv-purchase-report" data-bs-toggle="collapse" aria-expanded="false" aria-controls="nv-purchase-report">
    <div class="d-flex align-items-center">
      <span class="nav-link-text">Purchase</span>
    </div>
  </a>
  <!-- more inner pages-->
  <div class="parent-wrapper">
    <ul class="nav collapse parent" data-bs-parent="#components" id="nv-purchase-report">
      @if( auth()->user()?->can('see purchase report link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
      <li class="nav-item">
        <a class="nav-link" href="{{ route('report.purchaseReport') }}" aria-expanded="false">
          <div class="d-flex align-items-center">
            <span class="nav-link-text">Purchase</span>
          </div>
        </a>
        <!-- more inner pages-->
      </li>
      @endif
    </ul>
  </div>
</li>