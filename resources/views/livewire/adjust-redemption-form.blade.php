<div class="col-3">
  <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#adjustRedemptionFormModal">+</button>
  <div wire:ignore.self class="modal fade" id="adjustRedemptionFormModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5><button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span class="fas fa-times fs--1"></span></button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div class="mb-3">
              <label class="form-label" for="phone">Phone</label>
              <input wire:model.lazy="phone" id="phone" class="form-control" type="text">
              @error('phone') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="treatmentCode">Treatment Code</label>
              <input wire:model.lazy="treatmentCode" id="treatmentCode" class="form-control" type="text">
              @error('treatmentCode') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="qty">Qty</label>
              <input wire:model.lazy="qty" id="qty" class="form-control" type="number">
              @error('qty') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="note">Note</label>
              <textarea wire:model.lazy="note" id="note" class="form-control"></textarea>
              @error('note') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="float-end">
              <button type="button" wire:click="$emit('clearAdjustRedemptionForm')" class="btn btn-light">Clear</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
  <script>
    let adjustRedemptionFormModal = new bootstrap.Modal(
      document.getElementById('adjustRedemptionFormModal'),
      {
        backdrop: 'static',
      }
    )
    Livewire.on('showAdjustRedemptionForm', () => {
      adjustRedemptionFormModal.show();
    })
    Livewire.on('hideAdjustRedemptionForm', () => {
      adjustRedemptionFormModal.hide();
    })
    document.getElementById('adjustRedemptionFormModal').addEventListener('hidden.bs.modal', () => {
      Livewire.emit('clearadjustRedemptionForm');
    });
  </script>
@endpush
