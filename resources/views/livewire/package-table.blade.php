<div class="d-flex flex-column gap-4">
  <div class="row">
    <div class="col">
      <h4>{{ $titlePage }}</h4>
    </div>
    <div class="col-3 d-flex gap-2">
      <input
        wire:model.debounce.200ms="search"
        id="search"
        class="form-control search-input form-control-sm"
        type="search"
        placeholder="Search"
        aria-label="Search"
      >
      <livewire:package-form />
    </div>
  </div>
  <livewire:package-detail />
  <table class="table table-hover fs--1 mb-0 ">
    <thead>
    <tr>
      <th class="sort" scope="col">Item</th>
      <th class="sort text-end" scope="col">Price</th>
      <th class="sort text-end pe-2" scope="col">Action</th>
    </tr>
    </thead>
    <tbody class="list">
    @forelse($packages as $key => $package)
      <tr>
        <td wire:click="$emit('showPackageDetail', '{{$package->id}}')" class="ps-2 pointer">
          <livewire:item-detail
            :moduleName="$moduleName"
            :priceLabel="$priceLabel"
            :moduleLabel="$moduleLabel"
            :item="$package"
            :key="$package->id"
          />
        </td>
        <td wire:click="$emit('showPackageDetail', '{{$package->id}}')"
            class="text-end pointer"
        >
          {{ numberFormat($package->price) }}
        </td>
        <td class="text-end pe-2">
          <a
            class="me-2 pointer"
            wire:click="$emit('packageFee', '{{$package->id}}')"
            data-toogle="tooltip"
            title="Package Fee"
          >
            <i class="far fa-money-bill-alt" aria-hidden="true"></i>
          </a>
          <a
            class="me-2 pointer" wire:click="$emit('packageItem', '{{$package->id}}')"
            data-toogle="tooltip"
            title="Package Material Usage"
          >
            <i class="far fa-file-archive"></i>
          </a>
          <a
            class="me-2 pointer"
            wire:click="$emit('editPackage', '{{$package->id}}')"
            data-toogle="tooltip"
            title="Edit"
          >
            <i class="fa fa-pencil"></i>
          </a>
          <a
            class="text-danger pointer"
            wire:click="$emit('deletePackage', '{{$package->id}}')"
            data-toogle="tooltip"
            title="Delete"
          >
            <i class="fa fa-remove"></i>
          </a>
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="6" class="text-center">Empty Data</td>
      </tr>
    @endforelse
    </tbody>
  </table>
  {{ $packages->links() }}
</div>