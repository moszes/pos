<ul class="navbar-nav navbar-nav-icons flex-row">
	<li class="nav-item dropdown">
		<a
			class="nav-link dropdown-toggle"
			href="#"
			id="navbarDropdownBranch"
			role="button"
			data-bs-toggle="dropdown"
			aria-haspopup="true"
			aria-expanded="false"
		>
			{{ $branch->name }}
		</a>
		<div
			class="dropdown-menu dropdown-menu-end"
			aria-labelledby="navbarDropdownBranch"
		>
			@forelse ($branches as $branch)
				<a
					wire:click="selectBranch('{{@$branch->id}}')"
					class="dropdown-item" href="#"
				>
					{{ $branch->name }}
				</a>
			@empty
				<a
					wire:click="selectBranch('{{@$branch->id}}')"
					class="dropdown-item"
					href="#"
				>
					No other branch
				</a>
			@endforelse
		</div>
	</li>
	<li class="nav-item">
		<div class="theme-control-toggle fa-icon-wait px-2">
			<input
				class="form-check-input ms-0 theme-control-toggle-input"
				type="checkbox"
				data-theme-control="phoenixTheme"
				value="dark"
				id="themeControlToggle"
			/>
			<label
				class="mb-0 theme-control-toggle-label
                  theme-control-toggle-light"
				for="themeControlToggle"
				data-bs-toggle="tooltip"
				data-bs-placement="left"
				title="Switch theme"
			>
				<span class="icon" data-feather="moon"></span>
			</label>
			<label
				class="mb-0 theme-control-toggle-label
                    theme-control-toggle-dark"
				for="themeControlToggle"
				data-bs-toggle="tooltip"
				data-bs-placement="left"
				title="Switch theme"
			>
				<span class="icon" data-feather="sun"></span>
			</label>
		</div>
	</li>
	<li class="nav-item dropdown">
		<a
			class="nav-link lh-1 pe-0"
			id="navbarDropdownUser"
			href="#"
			role="button"
			data-bs-toggle="dropdown"
			data-bs-auto-close="outside"
			aria-haspopup="true"
			aria-expanded="false"
		>
			<div class="avatar avatar-l ">
				<img class="rounded-circle " src="{{ $avatar }}" alt="" />
			</div>
		</a>
		<div
			class="dropdown-menu dropdown-menu-end
                navbar-dropdown-caret py-0 dropdown-profile
                shadow border border-300"
			aria-labelledby="navbarDropdownUser"
			style="width:250px"
		>
			<div class="card position-relative border-0">
				<div class="card-body p-0">
					<div class="text-center pt-4 pb-3">
						<div class="avatar avatar-xl ">
							<img class="rounded-circle"
							     src="{{ $avatar }}"
							     alt="avatar Image"
							/>
						</div>
						<h6 class="mt-2 text-black">{{ auth()->user()?->name }}</h6>
					</div>
				</div>
				<div class="overflow-auto scrollbar" style="height: 7rem;">
					<ul class="nav d-flex flex-column pb-1">
						<li class="nav-item">
							<a
								class="nav-link px-3"
								href="{{
    							route(
                  'user.profile', [
                    'userCode' => auth()->user()->details?->code
                    ])
                }}"
							>
								<span class="me-2 text-900" data-feather="user"></span>
								<span>Profile</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link px-3" href="{{ route('dashboard') }}">
								<span
									class="me-2 text-900"
									data-feather="pie-chart"
								></span>
								<span>Dashboard</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link px-3" href="#">
								<span
									class="me-2 text-900"
									data-feather="settings"></span>
								Settings &amp; Privacy
							</a>
						</li>
					</ul>
				</div>
				<div class="card-footer p-0 border-top pt-3">
					<div class="px-3">
						<a
							class="btn btn-phoenix-secondary d-flex flex-center w-100"
							href="{{ route('logout') }}"
						>
              <span class="me-2" data-feather="log-out"></span>
							<span>Sign out</span>
						</a>
					</div>
					<div class="my-2 text-center fw-bold fs--2 text-600">
						<a class="text-600 me-1" href="#">Privacy policy</a>&bull;
						<a class="text-600 mx-1" href="#">Terms</a>&bull;
						<a class="text-600 ms-1" href="#">Cookies</a>
					</div>
				</div>
			</div>
		</div>
	</li>
</ul>