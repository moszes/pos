<div>
	@forelse($customersBirthOfDay as $customer)
		<div class="d-flex flex-column gap-3">
			<h3>Happy Birth Day <i class="fas fa-birthday-cake text-primary"></i></h3>
			<div class="birthday-wish-card">
				<h5 class="mb-0">{{ $customer->name }}</h5>
				<p>{{ $customer->instagram }}</p>
				<p>{{ $customer->phone }}</p>
				<p>Today is {{ $customer->date_of_birth }} on {{ $customer->place_of_birth }}.</p>
				<p class="birthday-wish">Happy Birthday, {{ $customer->name }}!</p>
			</div>
		</div>
	@empty
		<p>No one has a birthday today.</p>
	@endforelse
</div>