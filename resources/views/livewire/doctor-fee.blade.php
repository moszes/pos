<div class="d-flex gap-3">
	<div class="mb-3 col">
		<label class="form-label" for="doctorRp">Doctor Fee (Rp)</label>
		<input
			wire:model.debounce.200ms="doctorRp"
			wire:click="$set('doctorRp', '')"
			wire:blur="$set('doctorRp', {{$doctorRp === '' ? 0 : $doctorRp}})"
			id="doctorRp"
			class="form-control"
			type="number"
			@if($doctorPercent > 0) disabled @endif
		>
		@error('doctorRp') <span class="text-danger">{{ $message }}</span> @enderror
	</div>
	<div class="mb-3 col">
		<label class="form-label" for="doctorPercent">Doctor Fee (%)</label>
		<input
			wire:model.debounce.200ms="doctorPercent"
			wire:click="$set('doctorPercent', '')"
			wire:blur="$set('doctorPercent', {{$doctorPercent === '' ? 0 : $doctorPercent}})"
			id="doctorPercent"
			class="form-control"
			type="number"
			@if($doctorRp > 0) disabled @endif
		>
		@error('doctorPercent') <span class="text-danger">{{ $message }}</span> @enderror
	</div>
</div>