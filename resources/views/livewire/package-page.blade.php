<div class="shadow rounded">
  <livewire:breadcrumb />
  <div class="card">
    <div class="card-body">
      <livewire:package-table />
    </div>
  </div>
</div>

@push('scripts')
  <script>
    window.addEventListener("DOMContentLoaded", function () {
      Livewire.emit("openMasterDataModule");
      Livewire.emit("openServicesMenu")
    });
    
    Livewire.on('deletePackage', (id) => {
      Swal.fire({
        title: "Are you sure?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!"
      }).then((result) => {
        if (result.isConfirmed) {
          Livewire.emit('destroy', id);
        }
      });
    })
  </script>
@endpush