<div class="col-3">
  <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#subscriptionForm">+</button>
  <div wire:ignore.self class="modal modal-lg fade" id="subscriptionForm" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5>
          <button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span class="fas fa-times fs--1"></span></button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div class="d-flex flex-column gap-3">
              <div class="justify-content-between d-flex gap-3">
                <div class="w-100">
                  <livewire:customer-search />
                </div>
                <div class="w-100">
                  <livewire:subscription-plan-search />
                </div>
              </div>
              <div class="d-flex justify-content-between gap-3">
                <div class="form-group w-100">
                  <label class="form-label" for="code">code</label>
                  <input wire:model.debounce.500ms="code" type="text" class="form-control" id="code">
                </div>
                <div class="form-group w-100">
                  <label class="form-label" for="buyDate">Buy Date</label>
                  <input wire:model.debounce.500ms="buyDate" type="date" class="form-control" id="buyDate">
                </div>
                <div class="form-group w-100">
                  <label class="form-label" for="activeDate">Active Date</label>
                  <input wire:model.debounce.500ms="activeDate" type="date" class="form-control" id="activeDate">
                </div>
                <div class="form-group w-100">
                  <label class="form-label" for="expiredDate">Expired Date</label>
                  <input wire:model.debounce.500ms="expiredDate" type="date" class="form-control" id="expiredDate">
                </div>
              </div>
            </div>
            <div class="float-end mt-3">
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>
  let subscriptionForm = new bootstrap.Modal(
    document.getElementById('subscriptionForm'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showSubscriptionForm', () => {
    subscriptionForm.show();
  })
  Livewire.on('hideSubscriptionForm', () => {
    subscriptionForm.hide();
  })
  
  document.getElementById('subscriptionForm').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearSubscriptionPlanSearch');
    Livewire.emit('clearSubscriptionForm');
  });
  
  document.getElementById('subscriptionForm').addEventListener('show.bs.modal', () => {
    Livewire.emit('selectSubscriptionPlan', 1);
  });
</script>
@endpush
