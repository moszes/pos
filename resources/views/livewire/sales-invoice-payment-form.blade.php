<div class="d-flex gap-3">
  <div class="mb-3 col">
    <label class="form-label" for="paymentId">Payment Options</label>
    <select wire:model.debounce.500ms="paymentId" class="form-select" id="paymentId" aria-label="Select payment">
      {!! paymentOption() !!}
    </select>
    @error('paymentId') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="mb-3 col">
    <label class="form-label" for="paymentAmount">Amount</label>
    <input wire:model.debounce.500ms="paymentAmount" id="paymentAmount" class="form-control" type="number">
    <div class="my-3">
      <h3>{{ priceFormat($paymentAmount) }}</h3>
    </div>
    @error('paymentAmount') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="mb-3 col">
    <label class="form-label" for="referenceNumber">Reference</label>
    <input wire:model.debounce.lazy="referenceNumber" id="referenceNumber" class="form-control" type="text">
    @error('referenceNumber') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="col-2 pt-4">
    <button wire:loading.attr="disabled" class="btn btn-primary float-end" type="button" wire:click="addSalesInvoicePayment">+ Add Payment</button>
  </div>
</div>

@push('scripts')
  <script>
    Livewire.on('paymentRequired', () => {
		  Swal.fire({
         title: 'Error',
         text: 'Payment amount cannot be zero',
         icon: 'error',
         confirmButtonText: 'Ok',
      });
    });
  </script>
@endpush