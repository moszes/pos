<div wire:ignore.self class="modal modal-xl fade" id="productDetail" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ $formTitle }}</h5>
        <button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close">
          <span class="fas fa-times fs--1"></span>
        </button>
      </div>
      <div class="modal-body">
        <div class="ps-2">
          @if($product)
            <livewire:product-detail-info :product="$product" />
            <livewire:product-price-history-table :product="$product" />
          @endif
        </div>
      </div>
    </div>
  </div>
</div>


@push('scripts')
<script>
  let productDetail = new bootstrap.Modal(
    document.getElementById('productDetail'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showProductDetails', () => {
    productDetail.show();
  })
  Livewire.on('hideProductDetails', () => {
    productDetail.hide();
  })

  document.getElementById('productDetail').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearProductDetail')
  })
</script>
@endpush