<div class="shadow rounded">
	<livewire:breadcrumb />
	<div class="card px-0">
		<div class="card-body">
			<livewire:employee-table />
		</div>
	</div>
</div>