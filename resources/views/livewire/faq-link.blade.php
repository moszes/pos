<div class="nav-item-wrapper">
  <a class="nav-link dropdown-indicator label-1" href="#nv-faq" role="button" data-bs-toggle="collapse" aria-expanded="false" aria-controls="nv-faq">
    <div class="d-flex align-items-center">
      <span class="nav-link-icon">
        <span data-feather="help-circle"></span>
      </span>
      <span class="nav-link-text">FAQ</span>
    </div>
  </a>
  <div class="parent-wrapper label-1">
    <ul class="nav collapse parent" data-bs-parent="#navbarVerticalCollapse" id="nv-faq">
      <li class="collapsed-nav-item-title d-none">FAQ</li>
      <li class="nav-item"><a class="nav-link" href="{{ route('faq.application') }}" aria-expanded="false">
          <div class="d-flex align-items-center">
            <span class="nav-link-text">Application</span>
          </div>
        </a>
        <!-- more inner pages-->
      </li>
      <li class="nav-item"><a class="nav-link" href="{{ route('faq.module') }}" aria-expanded="false">
          <div class="d-flex align-items-center">
            <span class="nav-link-text">Module</span>
          </div>
        </a>
        <!-- more inner pages-->
      </li>
    </ul>
  </div>
</div>