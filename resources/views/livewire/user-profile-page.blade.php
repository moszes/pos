<div class="row">
	<div class="col-12 col-xl-4">
		@error('coverImage') <div class="alert alert-danger">{{ $message }}</div>  @enderror
		<div class="card cover-image mb-5">
			<div class="card-header hover-actions-trigger position-relative mb-6" style="min-height: 130px; ">
				<livewire:cover-image-file-upload :user="$user" />
				<livewire:avatar-file-upload :user="$user" />
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<div class="d-flex flex-column gap-2 flex-wrap mb-2">
							<h3 class="me-2">{{ '@'. $user?->name }}</h3>
							<p>Aku Bahagia...</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-12 col-xl-8 card">
		<div class="card-body">
			<ul class="nav nav-underline" id="myTab" role="tablist">
				<li class="nav-item" role="presentation"><a class="nav-link active" id="profile-tab" data-bs-toggle="tab" href="#tab-profile" role="tab" aria-controls="tab-profile" aria-selected="false" tabindex="-1">Profile</a></li>
				<li class="nav-item" role="presentation"><a class="nav-link" id="target-tab" data-bs-toggle="tab" href="#tab-target" role="tab" aria-controls="tab-target" aria-selected="false" tabindex="-1">Target</a></li>
				<li class="nav-item" role="presentation"><a class="nav-link" id="benefit-tab" data-bs-toggle="tab" href="#tab-benefit" role="tab" aria-controls="tab-benefit" aria-selected="true">Benefit</a></li>
			</ul>
			<div class="tab-content mt-3" id="myTabContent">
				<div class="tab-pane fade active show" id="tab-profile" role="tabpanel" aria-labelledby="profile-tab">
					<livewire:user-detail-form :user="$user" />
				</div>
				<div class="tab-pane fade" id="tab-target" role="tabpanel" aria-labelledby="target-tab">
				</div>
				<div class="tab-pane fade" id="tab-benefit" role="tabpanel" aria-labelledby="benefit-tab">
				
				</div>
			</div>
		</div>
	</div>
</div>