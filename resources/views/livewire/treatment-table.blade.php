<div class="d-flex flex-column gap-4">
	<div class="d-flex flex-column">
		<div class="row">
			<div class="col">
				<h4>{{ $titlePage }}</h4>
			</div>
			<div class="col-6">
				<div class="d-flex gap-2">
					<div class="col">
						<livewire:subscription-plan-option />
					</div>
					<div class="col d-flex gap-2">
						<input
							wire:model.debounce.200ms="search"
							id="search"
							class="form-control search-input form-control-sm"
							type="search"
							placeholder="Search"
							aria-label="Search"
						>
						<livewire:treatment-form />
					</div>
				</div>
			</div>
		</div>
	</div>
	<livewire:treatment-detail />
	<table class="table table-hover fs--1 mb-0 ">
		<thead>
			<tr>
				<th class="sort" scope="col">Item</th>
				<th class="sort text-end" scope="col">Price</th>
				<th class="sort text-end pe-2" scope="col">Action</th>
			</tr>
		</thead>
		<tbody class="list">
		@forelse($treatments as $key => $treatment)
			<tr>
				<td wire:click="$emit('showTreatmentDetail', '{{$treatment->id}}')" class="ps-2 pointer">
					<livewire:item-detail
						:moduleName="$moduleName"
						:priceLabel="$priceLabel"
						:moduleLabel="$moduleLabel"
						:item="$treatment"
						:key="$treatment->id"
					/>
				</td>
				<td wire:click="$emit('showTreatmentDetail', '{{$treatment->id}}')"
				    class="text-end pointer"
				>
					{{ numberFormat($treatment->price) }}
				</td>
				<td class="text-end pe-2">
					<a
						class="me-2 pointer"
						wire:click="$emit('treatmentFee', '{{$treatment->id}}')"
						data-toogle="tooltip"
						title="Treatment Fee"
					>
						<i class="far fa-money-bill-alt" aria-hidden="true"></i>
					</a>
					<a
						class="me-2 pointer" wire:click="$emit('treatmentMaterialUsage', '{{$treatment->id}}')"
						data-toogle="tooltip"
						title="Treatment Material Usage"
					>
						<i class="far fa-file-archive"></i>
					</a>
					<a
						class="me-2 pointer"
						wire:click="$emit('editTreatment', '{{$treatment->id}}')"
						data-toogle="tooltip"
						title="Edit"
					>
						<i class="fa fa-pencil"></i>
					</a>
					<a
						class="text-danger pointer"
						wire:click="$emit('deleteTreatment', '{{$treatment->id}}')"
						data-toogle="tooltip"
						title="Delete"
					>
						<i class="fa fa-remove"></i>
					</a>
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="6" class="text-center">Empty Data</td>
			</tr>
		@endforelse
		</tbody>
	</table>
	{{ $treatments->links() }}
</div>