<div class="d-flex flex-column gap-3 justify-content-center">
  <div class="d-flex justify-content-center">
    <h5>Treatment Price History</h5>
  </div>
  <livewire:treatment-price-history-table :treatment="$treatment" />
</div>