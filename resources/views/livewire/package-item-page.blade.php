<div class="d-flex flex-column">
	<div class="row">
		<div class="col-5">
			<div class="card mb-3">
				<div class="card-body">
					<livewire:toast-notification/>
					<livewire:item-detail
						:redirectRoute="$redirectRoute"
						:priceLabel="$priceLabel"
						:moduleName="$moduleName"
						:moduleLabel="$moduleLabel"
						:item="$package"
					/>
				</div>
			</div>
		</div>
		<div class="col-7">
			<livewire:package-item-table :package="$package"/>
		</div>
	</div>
</div>
@push('scripts')
<script>

	window.addEventListener("DOMContentLoaded", function () {
		Livewire.emit("openMasterDataModule");
		Livewire.emit("openServicesMenu")
	});
</script>
@endpush