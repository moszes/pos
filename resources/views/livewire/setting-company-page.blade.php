<div class="shadow rounded">
  <livewire:breadcrumb />
  <div class="card">
    <div class="card-body">
      <livewire:toast-notification/>
      <div class="d-flex flex-column">
        <form id="companyForm" class="col d-flex flex-column gap-3">
          <h3 class="text-center">Company Data</h3>
          <div class="form-group">
            <label for="companyName" class="mb-1">Name</label>
            <input
              wire:model.lazy="companyName"
              type="text"
              id="companyName"
              class="form-control"
            >
            @error('companyName')
              <span class="text-danger"> {{ $message }} </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="companyWebsite" class="mb-1">Website</label>
            <input
              wire:model.lazy="companyWebsite"
              type="text"
              id="companyWebsite"
              class="form-control"
            >
            @error('companyWebsite')
              <span class="text-danger"> {{ $message }} </span>
            @enderror
          </div>
          <div class="d-flex justify-content-between gap-3">
            <div class="form-group col">
              <label for="companyLogo" class="mb-1">Logo</label>
              <input
                wire:model.lazy="companyLogo"
                type="file"
                id="companyLogo"
                class="form-control"
              >
              @error('companyLogo')
                <span class="text-danger"> {{ $message }} </span>
              @enderror
            </div>
            <div class="form-group col">
              <label for="companyEmail" class="mb-1">Email</label>
              <input
                wire:model.lazy="companyEmail"
                type="email"
                id="companyEmail"
                class="form-control"
              >
              @error('companyEmail')
                <span class="text-danger"> {{ $message }} </span>
              @enderror
            </div>
            <div class="form-group col">
              <label for="companyPhone" class="mb-1">Phone</label>
              <input
                type="text"
                wire:model.lazy="companyPhone"
                id="companyPhone"
                class="form-control"
              >
              @error('companyPhone')
                <span class="text-danger"> {{ $message }} </span>
              @enderror
            </div>
          </div>
          <div class="form-group">
            <label
              for="companyAddress"
              class="mb-1"
            >
              Company Address
            </label>
            <textarea
              wire:model.lazy="companyAddress"
              id="companyAddress"
              cols="30"
              rows="10"
              class="form-control"
            ></textarea>
          </div>
          <div class="d-flex justify-content-end">
            <button
              type="button"
              wire:click="updateCompany"
              class="btn btn-primary"
            >
              Update Company
            </button>
          </div>
        </form>
      </div>
      <div class="loading d-none" wire:loading.class.remove="d-none"></div>
    </div>
  </div>
</div>

@push('scripts')
<script>
  document.addEventListener("DOMContentLoaded", () => {
    Livewire.emit("openSettingModule");
    Livewire.emit("openBaseDataMenu")
    Livewire.on('companyUpdated', () => {
      location.reload()
    })
  });
</script>
@endpush