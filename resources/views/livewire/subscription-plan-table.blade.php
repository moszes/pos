<div class="d-flex flex-column gap-4">
	<div class="row">
		<div class="col">
			<h4>{{ $titlePage }}</h4>
		</div>
		<div class="col-3 d-flex gap-2">
			<input
				wire:model.debounce.200ms="search"
				class="form-control search-input search form-control-sm"
				type="search"
				placeholder="Search"
				aria-label="Search"
			>
			<livewire:subscription-plan-form/>
		</div>
	</div>
	<table class="table table-hover fs--1 mb-0 ">
		<thead>
		<tr>
			<th class="sort ps-2" scope="col">Code</th>
			<th class="sort" scope="col">Name</th>
			<th class="sort text-end pe-2" scope="col">Price</th>
			<th class="sort text-end pe-2" scope="col">Action</th>
		</tr>
		</thead>
		<tbody class="list">
		@forelse($subscriptionPlans as $key => $subscriptionPlan)
			<tr>
				<td class="ps-2">{{ $subscriptionPlan->code }}</td>
				<td>{{ $subscriptionPlan->name }}</td>
				<td>{{ countFormat($subscriptionPlan->price) }}</td>
				<td class="text-end pe-2">
					<a class="me-2 pointer" wire:click="$emit('edit', '{{$subscriptionPlan->id}}')">
						<i class="fa fa-pencil"></i>
					</a>
					<a class="text-danger pointer" wire:click="$emit('deleteSubscriptionPlan', '{{$subscriptionPlan->id}}')">
						<i class="fa fa-remove"></i>
					</a>
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="5" class="text-center">Empty Data</td>
			</tr>
		@endforelse
		</tbody>
	</table>
	{{ $subscriptionPlans->links() }}
</div>

@push('scripts')
	<script>
		Livewire.on('deleteSubscriptionPlan', function(id) {
			Swal.fire({
				title: 'Are you sure?',
				text: 'You will not be able to revert this!',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
			}).then((result) => {
				if (result.isConfirmed) {
					Livewire.emit('destroy', id);
				}
			});
		});
	</script>
@endpush