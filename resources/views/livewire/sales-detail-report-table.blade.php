<div class="card">
  <div class="card-body scrollbar-overlay">
    <table class="table table-hover">
      <thead>
        <tr>
          <th class="px-3 text-nowrap">Inv. Date</th>
          <th class="px-3 text-nowrap">Invoice</th>
          <th class="px-3 text-nowrap">Customer</th>
          <th class="px-3 text-nowrap">Phone</th>
          <th class="px-3 text-nowrap">Doctor</th>
          <th class="px-3 text-nowrap">Assistance</th>
          <th class="px-3 text-nowrap">Therapist</th>
          <th class="px-3 text-nowrap">Sales</th>
          <th class="px-3 text-nowrap">Module</th>
          <th class="px-3 text-nowrap">Code</th>
          <th class="px-3 text-nowrap">Name</th>
          <th class="px-3 text-nowrap text-end">Price</th>
          <th class="px-3 text-nowrap">QTY</th>
          <th class="px-3 text-nowrap text-end">Disc. Item</th>
          <th class="px-3 text-nowrap text-end">Total Price</th>
          <th class="px-3 text-nowrap">Payment Type</th>
        </tr>
      </thead>
      <tbody>
      @if($salesDetails)
        @forelse($salesDetails as $salesDetail)
          @foreach($salesDetail->items as $item)
          <tr>
            <td class="px-3 text-nowrap">{{ dateFormat($salesDetail->sales_invoice_date) }}</td>
            <td class="px-3 text-nowrap">{{ $salesDetail->sales_invoice_number }}</td>
            <td class="px-3 text-nowrap">{{ $salesDetail->customer?->details->first_name }}</td>
            <td class="px-3 text-nowrap">{{ $salesDetail->customer?->details->phone }}</td>
            <td class="px-3 text-nowrap">
              {{ @getUserById($item->doctor_id)->details->first_name . ' ' . @getUserById($item->doctor_id)->details->last_name }}
            </td>
            <td class="px-3 text-nowrap">
              {{ @getUserById($item->doctor_assistance_id)->details->first_name . ' ' . @getUserById($item->doctor_assistance_id)->details->last_name }}
            </td>
            <td class="px-3 text-nowrap">
              {{ @getUserById($item->therapist_id)->details->first_name . ' ' . @getUserById($item->therapist_id)->details->last_name }}
            </td>
            <td class="px-3 text-nowrap">
              {{ @getUserById($item->sales_id)->details->first_name . ' ' . @getUserById($item->sales_id)->details->last_name }}
            </td>
            <td class="px-3 text-nowrap">{{ $item->module }}</td>
            <td class="px-3 text-nowrap">{{ $item->code }}</td>
            <td class="px-3 text-nowrap">{{ $item->name }}</td>
            <td class="px-3 text-nowrap text-end">{{ numberFormat($item->price) }}</td>
            <td class="px-3 text-nowrap">{{ numberFormat($item->qty) }}</td>
            <td class="px-3 text-nowrap text-end">{{ numberFormat($item->discount_item) }}</td>
            <td class="px-3 text-nowrap text-end">{{ numberFormat(floatval($item->price) * countFormat($item->qty) - $item->discount_item) }}</td>
            <td class="px-3 text-nowrap">{{ @getPaymentBySalesInvoiceId($salesDetail->id) }}</td>
          </tr>
          @endforeach
        @empty
          <tr>
            <td colspan="16" class="text-center">Empty Data</td>
          </tr>
        @endforelse
      @endif
      </tbody>
    </table>
  </div>
  <div class="card-footer d-flex justify-content-end">
    <a class="btn btn-primary" href="{{ route('generate_excel') }}?salesDetails=1&dateFrom={{ dateFormat($dateFrom) }}&dateTo={{ dateFormat($dateTo) }}@if($customer)&customer={{ $customer['id'] }}@endif">Download Sales Detail Report</a>
  </div>
</div>