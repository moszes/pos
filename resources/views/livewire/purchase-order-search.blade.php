<div class="d-flex flex-column mb-3">
  <div class="col">
    <label class="form-label" for="purchaseOrderSearch">Purchase Order</label>
    <input wire:model.debounce.200ms="purchaseOrderSearch" id="purchaseOrderSearch" class="form-control" type="text" placeholder="Search Purchase Order...">
    @error('purchaseOrderSearch') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
  @if($purchaseOrderSearching)
    <ul class="list-group">
      @forelse($purchaseOrderLists as $purchaseOrderList)
        <li wire:click="selectPurchaseOrderById({{$purchaseOrderList->id}})" class="list-group-item pointer">
          <div>{{ $purchaseOrderList->order_number}}</div>
          <div>{{ @getVendorById($purchaseOrderList->vendor_id)->name }}</div>
          <div>{{ @$purchaseOrderList->note }}</div>
        </li>
      @empty
        <li class="list-group-item">Empty Purchase Order</li>
      @endforelse
    </ul>
  @endif
</div>
