<div class="nav-item-wrapper">
  <a
    class="nav-link dropdown-indicator label-1"
    id="setting-btn"
    href="#nv-app-settings"
    role="button"
    data-bs-toggle="collapse"
    aria-expanded="false"
    aria-controls="nv-app-settings"
  >
    <div class="d-flex align-items-center">
      <span class="nav-link-icon">
        <span data-feather="settings"></span>
      </span>
      <span class="nav-link-text">Setting</span>
    </div>
  </a>
  <div class="parent-wrapper label-1">
    <ul
      class="nav collapse parent"
      data-bs-parent="#navbarVerticalCollapse"
      id="nv-app-settings"
    >
      <li class="collapsed-nav-item-title d-none">Settings</li>
      <li class="nav-item">
        <a
          class="nav-link
                 @if(Route::currentRouteName() == 'settings.company')
                   active
                 @endif
                 "
          href="{{ route('settings.company') }}"
          aria-expanded="false"
        >
          <div class="d-flex align-items-center">
            <span class="nav-link-text">Company</span>
          </div>
        </a>
      </li>
      <li class="nav-item">
        <a
          class="nav-link
                 @if(Route::currentRouteName() == 'settings.branch')
                   active
                 @endif
                 "
          href="{{ route('settings.branch') }}"
          aria-expanded="false"
        >
          <div class="d-flex align-items-center">
            <span class="nav-link-text">Branch</span>
          </div>
        </a>
      </li>
      <li class="nav-item">
        <a
          class="nav-link
                  @if(Route::currentRouteName() == 'settings.printer')
                    active
                   @endif
                 "
          href="{{ route('settings.printer') }}"
          aria-expanded="false"
        >
          <div class="d-flex align-items-center">
            <span class="nav-link-text">Printer</span>
          </div>
        </a>
      </li>
      <li class="nav-item">
        <a
          class="nav-link
                  @if(Route::currentRouteName() == 'settings.module')
                    active
                   @endif
                 "
          href="{{ route('settings.module') }}"
          aria-expanded="false"
        >
          <div class="d-flex align-items-center">
            <span class="nav-link-text">Module</span>
          </div>
        </a>
      </li>
    </ul>
  </div>
</div>


@push('scripts')
  <script>
    Livewire.on('openSettingModule', function(){
      document.getElementById('setting-btn').click();
    })
  </script>
@endpush