<li class="nav-item">
  <a class="nav-link dropdown-indicator" id="sales-report-btn" href="#nv-sales-report" data-bs-toggle="collapse" aria-expanded="false" aria-controls="nv-sales-report">
    <div class="d-flex align-items-center">
      <span class="nav-link-text">Sales</span>
    </div>
  </a>
  <!-- more inner pages-->
  <div class="parent-wrapper">
    <ul class="nav collapse parent" data-bs-parent="#components" id="nv-sales-report">
      @if( auth()->user()?->can('see today sales report link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
      <li class="nav-item">
        <a class="nav-link @if(Route::currentRouteName() == 'report.today_sales_report') active @endif" href="{{ route('report.today_sales_report') }}" aria-expanded="false">
          <div class="d-flex align-items-center">
            <span class="nav-link-text">Today Sales</span>
          </div>
        </a>
        <!-- more inner pages-->
      </li>
      @endif
      @if( auth()->user()?->can('see sales summary report link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
      <li class="nav-item">
        <a class="nav-link @if(Route::currentRouteName() == 'report.salesSummaryReport') active @endif" href="{{ route('report.salesSummaryReport') }}" aria-expanded="false">
          <div class="d-flex align-items-center">
            <span class="nav-link-text">Summary</span>
          </div>
        </a>
        <!-- more inner pages-->
      </li>
      @endif
      @if( auth()->user()?->can('see sales detail report link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
      <li class="nav-item">
        <a class="nav-link @if(Route::currentRouteName() == 'report.salesDetailReport') active @endif" href="{{ route('report.salesDetailReport') }}" aria-expanded="false">
          <div class="d-flex align-items-center">
            <span class="nav-link-text">Detail</span>
          </div>
        </a>
        <!-- more inner pages-->
      </li>
      @endif
      @if( auth()->user()?->can('see subscriptions report link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
      <li class="nav-item">
        <a class="nav-link @if(Route::currentRouteName() == 'report.salesSubscriptionReport') active @endif" href="{{ route('report.salesSubscriptionReport') }}" aria-expanded="false">
          <div class="d-flex align-items-center">
            <span class="nav-link-text">Subscriptions</span>
          </div>
        </a>
        <!-- more inner pages-->
      </li>
      @endif
      @if( auth()->user()?->can('see commission report link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
      <li class="nav-item">
        <a class="nav-link @if(Route::currentRouteName() == 'report.commissionReport') active @endif" href="{{ route('report.commissionReport') }}" aria-expanded="false">
          <div class="d-flex align-items-center">
            <span class="nav-link-text">Commission</span>
          </div>
        </a>
        <!-- more inner pages-->
      </li>
      @endif
      @if( auth()->user()?->can('see turnover report link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
      <li class="nav-item">
        <a class="nav-link @if(Route::currentRouteName() == 'report.turnOverReport') active @endif" href="{{ route('report.turnOverReport') }}" aria-expanded="false">
          <div class="d-flex align-items-center">
            <span class="nav-link-text">Turnover</span>
          </div>
        </a>
        <!-- more inner pages-->
      </li>
      @endif
    </ul>
  </div>
</li>

@push('scripts')
  <script>
    Livewire.on('openSalesReportModule', function(){
      document.getElementById('sales-report-btn').click();
    })
  </script>
@endpush