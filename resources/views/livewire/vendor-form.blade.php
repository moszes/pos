<div>
  <button class="btn btn-primary btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#vendorForm">+</button>
  <div wire:ignore.self class="modal modal-xl fade" id="vendorForm" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5><button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span class="fas fa-times fs--1"></span></button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div class="d-flex flex-column gap-3">
              <div class="d-flex flex-column">
                <h5 class="my-2 text-center">Vendor</h5>
                <div class="d-flex gap-3">
                  <div class="col">
                    <label class="form-label" for="code">Code</label>
                    <input wire:model.debounce.200ms="code" id="code" class="form-control" type="text">
                    @error('code') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                  <div class="col">
                    <label class="form-label" for="name">Name</label>
                    <input wire:model.debounce.200ms="name" id="name" class="form-control" type="text">
                    @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                </div>
                <div class="d-flex gap-3">
                  <div class="col">
                    <label class="form-label" for="contact_person">Contact Person</label>
                    <input wire:model.debounce.200ms="contactPerson" id="contact_person" class="form-control" type="text">
                    @error('contactPerson') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                  <div class="col">
                    <label class="form-label" for="phone">Phone</label>
                    <input wire:model.debounce.200ms="phone" id="phone" class="form-control" type="text">
                    @error('phone') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                </div>
                <div class="col">
                  <label class="form-label" for="address">Address</label>
                  <textarea wire:model.debounce.200ms="address" id="address" class="form-control"></textarea>
                  @error('address') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
              </div>
              <div class="d-flex flex-column">
                <h5 class="my-2 text-center">Taxes</h5>
                <div class="d-flex gap-3">
                  <div class="col">
                    <label class="form-label" for="npwpName">NPWP Name</label>
                    <input wire:model.debounce.200ms="npwpName" id="npwpName" class="form-control" type="text">
                    @error('npwpName') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                  <div class="col">
                    <label class="form-label" for="npwp">NPWP</label>
                    <input wire:model.debounce.200ms="npwp" id="npwp" class="form-control" type="text">
                    @error('npwp') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                  <div class="col">
                    <label class="form-label" for="taxName">Tax Name</label>
                    <input wire:model.debounce.200ms="taxName" id="taxName" class="form-control" type="text">
                    @error('npwpName') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                </div>
                <div class="d-flex gap-3">
                  <div class="col">
                    <label class="form-label" for="taxAddress">Taxes Address</label>
                    <textarea wire:model.debounce.200ms="taxAddress" id="taxAddress" class="form-control"></textarea>
                    @error('taxAddress') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                </div>
              </div>
              <div class="d-flex flex-column">
                <h5 class="my-2 text-center">Payment</h5>
                <div class="d-flex gap-3">
                  <div class="col">
                    <label class="form-label" for="period">Period</label>
                    <input wire:model.debounce.200ms="period" id="period" class="form-control" type="date">
                    @error('period') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                  <div class="col">
                    <label class="form-label" for="bank">Bank</label>
                    <input wire:model.debounce.200ms="bank" id="bank" class="form-control" type="text">
                    @error('bank') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                </div>
                <div class="d-flex gap-3">
                  <div class="col">
                    <label class="form-label" for="payment_name">Name</label>
                    <input wire:model.debounce.200ms="payment_name" id="payment_name" class="form-control" type="text">
                    @error('payment_name') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                  <div class="col">
                    <label class="form-label" for="payment_number">Bank Number</label>
                    <input wire:model.debounce.200ms="payment_number" id="payment_number" class="form-control" type="text">
                    @error('payment_number') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                </div>
              </div>
            </div>
            <div class="float-end mt-3">
              <button wire:loadin.attr="disabled" type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>
	
  let vendorForm = new bootstrap.Modal(
    document.getElementById('vendorForm'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showVendorForm', () => {
    vendorForm.show();
  })
  Livewire.on('hideVendorForm', () => {
    vendorForm.hide();
  })
  document.getElementById('vendorForm').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearVendorForm');
  });
</script>
@endpush