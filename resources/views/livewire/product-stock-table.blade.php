<div class="d-flex flex-column gap-3">
  <div class="d-flex justify-content-between">
    <h3>Product Stock</h3>
    <div>
      <h4>{{ $product->name }}</h4>
      <div class="text-lg">
        Stock Total : {{ $product->stocks->sum('stock_qty') }}
      </div>
    </div>
  </div>
  <div class="d-flex flex-column gap-3">
    <table class="table table-hover">
      <thead>
        <tr>
          <th class="sort px-3" scope="col">Code</th>
          <th class="sort px-3" scope="col">Name</th>
          <th class="sort px-3" scope="col">Category</th>
          <th class="sort px-3" scope="col">Sub Category</th>
          <th class="sort px-3" scope="col">Warehouse</th>
          <th class="sort px-3 text-center" scope="col">Qty</th>
          <th class="sort px-3 text-center" scope="col">Date</th>
        </tr>
      </thead>
      <tbody>
        @forelse($product->stocks as $product_stock)
        <tr>
          <td class="px-3">{{ @getProductById($product_stock->module_id)->code }}</td>
          <td class="px-3">{{ @getProductById($product_stock->module_id)->name }}</td>
          <td class="px-3">{{ @getCategoryById(getProductById($product_stock->module_id)->category_id)->name }}</td>
          <td class="px-3">{{ @getSubCategoryById(getProductById($product_stock->module_id)->sub_category_id)->name }}</td>
          <td class="px-3">{{ @getWarehouseById($product_stock->warehouse_id)->name }}</td>
          <td class="px-3 text-center">{{ countFormat(@$product_stock->stock_qty) }}</td>
          <td class="px-3 text-center">{{ @Carbon\Carbon::create($product_stock->created_at)->format('Y-m-d H:i:s') }}</td>
        </tr>
        @empty
        <tr>
          <td colspan="11" class="text-center">Empty Data</td>
        </tr>
        @endforelse
      </tbody>
    </table>
  </div>
</div>