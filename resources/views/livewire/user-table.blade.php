<div class="d-flex flex-column gap-4">
	<div class="row">
		<div class="col">
			<h4>{{ $titlePage }}</h4>
		</div>
		<div class="col-3 d-flex gap-2">
			<input wire:model.debounce.200ms="search"
			       class="form-control search-input search form-control-sm"
			       type="search"
			       placeholder="Search"
			       aria-label="Search"
			>
			<livewire:user-form/>
		</div>
	</div>
	<table class="table table-hover fs--1 mb-0">
		<thead>
		<tr>
			<th class="sort px-3" scope="col">Code</th>
			<th class="sort px-3" scope="col">Name</th>
			<th class="sort px-3" scope="col">Phone</th>
			<th class="sort px-3" scope="col">Email</th>
			<th class="sort px-3" scope="col">Role</th>
			<th class="sort px-3 text-end pe-2" scope="col">Action</th>
		</tr>
		</thead>
		<tbody class="list">
		@forelse($users as $key => $user)
			<tr>
				<td class="px-3 pointer" wire:click="$emit('goToUser', '{{ $user->id }}')">{{ @$user->details->code }}</td>
				<td class="px-3 pointer" wire:click="$emit('goToUser', '{{ $user->id }}')">{{ @$user->name }}</td>
				<td class="px-3 pointer" wire:click="$emit('goToUser', '{{ $user->id }}')">{{ @$user->email }}</td>
				<td class="px-3 pointer" wire:click="$emit('goToUser', '{{ $user->id }}')">{{ @$user->details->phone }}</td>
				<td class="px-3 pointer" wire:click="$emit('goToUser', '{{ $user->id }}')">{{ @$user->getRoleNames()[0] }}</td>
				<td class="text-end pe-2">
					<a class="me-2 pointer" wire:click="$emit('editUser', '{{$user->id}}')"><i class="fa fa-pencil"></i></a>
					<a class="text-danger pointer" wire:click="$emit('deleteUser', '{{$user->id}}')"><i class="fa fa-remove"></i></a>
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="5" class="text-center">Empty Data</td>
			</tr>
		@endforelse
		</tbody>
	</table>
	{{ $users->links() }}
</div>
 
@push('scripts')
   <script>

	   window.addEventListener("DOMContentLoaded", function () {
		   Livewire.emit("openMasterDataModule");
		   Livewire.emit("openStakeholdersMenu")
		   
		   Livewire.on('deleteUser', function(id) {
			   Swal.fire({
				   title: 'Are you sure?',
				   text: 'You will not be able to revert this!',
				   icon: 'warning',
				   showCancelButton: true,
				   confirmButtonColor: '#3085d6',
				   cancelButtonColor: '#d33',
				   confirmButtonText: 'Yes, delete it!',
				   cancelButtonText: 'No, cancel!',
			   }).then((result) => {
				   if (result.isConfirmed) {
					   Livewire.emit('destroy', id);
				   }
			   });
		   });
			 
	   });
   </script>
@endpush