<div class="px-1">
	<h4>{{ ucfirst($moduleName) }} Fee</h4>
	<table class="table">
		<thead>
		<tr>
			<th></th>
			<th class="text-end fs-lg--0 pe-3">Normal</th>
			<th class="text-end fs-lg--0 pe-3">Discount</th>
			<th class="text-end fs-lg--0 pe-3">Target</th>
		</tr>
		</thead>
		<tbody>
		@foreach($roles as $role)
			<tr>
				<td class="py-0 align-middle ps-2 fs-lg--0 text-nowrap">{{str_replace('_', ' ', ucfirst($role))}}</td>
				<td class="py-1 text-end fs-lg--0">
					<div class="d-flex justify-content-between gap-2">
						<select wire:model.lazy="normalFee.{{$moduleName}}.{{$role}}.fee_symbol" class="w-25 form-control form-control-sm border-0 bg-light">
							<option value="percentage">&nbsp;%</option>
							<option value="fixed">Rp.</option>
						</select>
						<input wire:model.lazy="normalFee.{{$moduleName}}.{{$role}}.value" type="number" class="form-control form-control-sm border-0 bg-light">
					</div>
				</td>
				<td class="py-1 text-end fs-lg--0">
					<div class="d-flex justify-content-between gap-2">
						<select wire:model.lazy="discountFee.{{$moduleName}}.{{$role}}.fee_symbol" class="w-25 form-control form-control-sm border-0 bg-light">
							<option value="percentage">&nbsp;%</option>
							<option value="fixed">Rp.</option>
						</select>
						<input wire:model.lazy="discountFee.{{$moduleName}}.{{$role}}.value" type="number" class="form-control form-control-sm border-0 bg-light">
					</div>
				</td>
				<td class="py-1 text-end fs-lg--0">
					<div class="d-flex justify-content-between gap-2">
						<select wire:model.lazy="targetFee.{{$moduleName}}.{{$role}}.fee_symbol" class="w-25 form-control form-control-sm border-0 bg-light">
							<option value="percentage">&nbsp;%</option>
							<option value="fixed">Rp.</option>
						</select>
						<input wire:model.lazy="targetFee.{{$moduleName}}.{{$role}}.value" type="number" class="form-control form-control-sm border-0 bg-light">
					</div>
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
	<div class="d-flex justify-content-end">
		<span class="text-end fs--1 fw-light">Auto Save* No need to click Save Button</span>
	</div>
</div>



@push('scripts')
	<script>
		Livewire.on('productFeeUpdated', () => {
			Swal.fire({
				icon: 'success',
				title: 'Fee Updated',
				showConfirmButton: false,
				timer: 1500
			})
		})
	</script>
@endpush