<li class="nav-item">
	<a class="nav-link dropdown-indicator" id="basedata-btn" href="#nv-base" data-bs-toggle="collapse" aria-expanded="false" aria-controls="nv-base">
		<div class="d-flex align-items-center">
			<div class="dropdown-indicator-icon">
				<span class="fas fa-caret-right"></span>
			</div>
			<span class="nav-link-text">Base</span>
		</div>
	</a>
	<!-- more inner pages-->
	<div class="parent-wrapper">
		<ul class="nav collapse parent" data-bs-parent="#forms" id="nv-base">
			
			@if( auth()->user()?->can('see category link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales') )
				<li class="nav-item">
					<a class="nav-link @if(Route::currentRouteName() == 'master.categories') active @endif" href="{{ route('master.categories') }}" aria-expanded="false">
						<div class="d-flex align-items-center">
							<span class="nav-link-text">Category</span>
						</div>
					</a>
					<!-- more inner pages-->
				</li>
			@endif
			
			@if( auth()->user()?->can('see sub category link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales') )
				<li class="nav-item">
					<a class="nav-link @if(Route::currentRouteName() == 'master.sub_categories') active @endif" href="{{ route('master.sub_categories') }}" aria-expanded="false">
						<div class="d-flex align-items-center">
							<span class="nav-link-text">Sub Category</span>
						</div>
					</a>
					<!-- more inner pages-->
				</li>
			@endif
			
			@if( auth()->user()?->can('see vendor link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales') )
				<li class="nav-item">
					<a class="nav-link @if(Route::currentRouteName() == 'master.vendors') active @endif" href="{{ route('master.vendors') }}" aria-expanded="false">
						<div class="d-flex align-items-center">
							<span class="nav-link-text">Vendor</span>
						</div>
					</a>
					<!-- more inner pages-->
				</li>
			@endif
			
			@if( auth()->user()?->can('see room link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales') && getSetting('Application Module', 'room'))
				<li class="nav-item">
					<a class="nav-link @if(Route::currentRouteName() == 'master.rooms') active @endif" href="{{ route('master.rooms') }}" aria-expanded="false">
						<div class="d-flex align-items-center">
							<span class="nav-link-text">Room</span>
						</div>
					</a>
					<!-- more inner pages-->
				</li>
			@endif
			
			@if( auth()->user()?->can('see unit of measure link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales') )
				<li class="nav-item">
					<a class="nav-link @if(Route::currentRouteName() == 'master.unit_of_measures') active @endif" href="{{ route('master.unit_of_measures') }}" aria-expanded="false">
						<div class="d-flex align-items-center">
							<span class="nav-link-text">Unit Of Measure</span>
						</div>
					</a>
					<!-- more inner pages-->
				</li>
			@endif
			
			@if( auth()->user()?->can('see payment link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales') )
				<li class="nav-item">
					<a class="nav-link @if(Route::currentRouteName() == 'master.payments') active @endif" href="{{ route('master.payments') }}" aria-expanded="false">
						<div class="d-flex align-items-center">
							<span class="nav-link-text">Payments</span>
						</div>
					</a>
					<!-- more inner pages-->
				</li>
			@endif
		
		</ul>
	</div>
</li>