<div class="d-flex flex-column gap-2">
  <h5 class="text-center mb-3">Material Usages</h5>
  <table class="table table-hover fs--1 mb-0 ">
    <thead>
      <tr>
        <th class="sort ps-2" scope="col">Code</th>
        <th class="sort" scope="col">Name</th>
        <th class="sort text-center" scope="col">Size</th>
        <th class="sort text-center" scope="col">Usage</th>
        <th class="sort text-center" scope="col">Unit</th>
        <th class="sort text-end" scope="col">Normal Price</th>
        <th class="sort text-end" scope="col">Agent Price</th>
        <th class="sort pe-2 text-end" scope="col">Cost</th>
      </tr>
    </thead>
    <tbody class="list">
      @forelse($marketplace->material_usages->toArray() as $materialUsage)
      <tr>
        <td class="ps-2">{{ @getProductById($materialUsage['product_id'])->code }}</td>
        <td>{{ @getProductById($materialUsage['product_id'])->name }}</td>
        <td class=" text-center">{{ @$materialUsage['size'] }}</td>
        <td class=" text-center">{{ @$materialUsage['usage'] }}</td>
        <td class=" text-center">{{ @getUnitOfMeasureById(getProductById($materialUsage['product_id'])->unit_of_measure_id)->name }}</td>
        <td class=" text-end">{{ priceFormat(@$materialUsage['normal_price']) }}</td>
        <td class=" text-end">{{ priceFormat(@$materialUsage['agent_price']) }}</td>
        <td class="pe-2 text-end">{{ priceFormat(@$materialUsage['cost']) }}</td>
      @empty
      <tr>
        <td colspan="8" class="text-center">Empty Data</td>
      </tr>
      @endforelse
    </tbody>
  </table>
</div>