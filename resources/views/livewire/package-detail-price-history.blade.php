<div class="d-flex flex-column gap-3 justify-content-center">
	<div class="d-flex justify-content-center">
		<h5>Package Price History</h5>
	</div>
	<livewire:package-price-history-table :package="$package" />
</div>