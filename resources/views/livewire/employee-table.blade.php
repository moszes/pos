<div class="d-flex flex-column gap-4">
	<div class="row">
		<div class="col">
			<h4>{{ $titlePage }}</h4>
		</div>
		<div class="col-3 d-flex gap-2">
			<input wire:model.debounce.200ms="search"
			       class="form-control search-input search form-control-sm"
			       type="search"
			       placeholder="Search"
			       aria-label="Search"
			>
			<livewire:user-form/>
		</div>
	</div>
	<table class="table table-hover fs--1 mb-0 ">
		<thead>
		<tr>
			<th class="sort px-3" scope="col">Username</th>
			<th class="sort px-3" scope="col">FirstName</th>
			<th class="sort px-3" scope="col">LastName</th>
			<th class="sort px-3" scope="col">Email</th>
			<th class="sort px-3" scope="col">Phone</th>
			<th class="sort px-3" scope="col">Address</th>
			<th class="sort text-end pe-2" scope="col">Action</th>
		</tr>
		</thead>
		<tbody class="list">
		@forelse($employees as $key => $employee)
			<tr>
				<td class="px-3">
					<a
						href="{{ route('master.employee_details', ['userCode' => $employee->code]) }}">{{ $employee->name }}</a>
				</td>
				<td class="px-3">{{ @$employee->first_name }}</td>
				<td class="px-3">{{ @$employee->last_name }}</td>
				<td class="px-3">{{ @$employee->email }}</td>
				<td class="px-3">{{ @$employee->phone }}</td>
				<td class="px-3">{{ @$employee->address }}</td>
				<td class="text-end pe-2">
					<a class="me-2 pointer" wire:click="$emit('editCustomer', '{{$employee->user_id}}')"><i
							class="fa fa-pencil"></i></a>
					<a class="text-danger pointer" wire:click="$emit('deleteCustomer', '{{$employee->user_id}}')"><i
							class="fa fa-remove"></i></a>
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="8" class="text-center">Empty Data</td>
			</tr>
		@endforelse
		</tbody>
	</table>
	{{ $employees->links() }}
</div>

@push('scripts')
	<script>
		window.addEventListener("DOMContentLoaded", function () {

			Livewire.on('deleteCustomer', (id) => {
				Swal.fire({
					title: 'Are you sure?',
					text: "You won't be able to revert this!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!'
				}).then((result) => {
					if (result.isConfirmed) {
						Livewire.emit('destroy', id)
					}
				})
			})
		})
	
	</script>
@endpush