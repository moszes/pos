<a class="navbar-brand me-1 me-sm-3" href="/">
	<div class="d-flex align-items-center">
		<div class="d-flex align-items-center">
			@if(config('app.env') == 'production')
				<img src="{{ @$logoPath }}" alt="POS" width="42" />
			@endif
			<p class="logo-text ms-2 d-none d-sm-block">
				{{ config('app.env') != 'production'
					? 'Point Of Sales'
					: config('app.name') }}
			</p>
		</div>
	</div>
</a>