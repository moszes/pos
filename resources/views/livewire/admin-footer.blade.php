<footer class="footer">
  <div class="row g-0 justify-content-between align-items-center h-100">
    <div class="col-6 col-sm-auto text-center">
      <p class="mb-0 mt-2 mt-sm-0 text-900">2022 &copy;<a class="ms-1" href="{{@$company->website}}">{{ @$company->name }}</a></p>
    </div>
    <div class="col-6 text-center">{{ date('Y-m-d H:i:s') }}</div>
    <div class="col-6 col-sm-auto text-center">
      <p class="mb-0 text-600">{{ $gitTag }}</p>
    </div>
  </div>
</footer>
