<div class="d-flex flex-column gap-4">
  <div class="row">
    <div class="col">
      <h4>{{ $titlePage }}</h4>
    </div>
    <div class="col-3 d-flex gap-2">
      <input wire:model.debounce.200ms="search"
             class="form-control search-input search form-control-sm"
             type="search"
             placeholder="Search"
             aria-label="Search"
      >
      <livewire:product-form/>
    </div>
  </div>
  <livewire:product-detail/>
  <table class="table table-hover fs--1 mb-0 ">
    <thead>
    <tr>
      <th class="sort" scope="col">Item</th>
      <th class="sort text-end" scope="col">Size</th>
      <th class="sort text-end" scope="col">Price</th>
      <th class="sort text-end pe-2" scope="col">Action</th>
    </tr>
    </thead>
    <tbody class="list">
    @forelse($products as $key => $product)
      <tr>
        <td wire:click="$emit('showProductDetails', '{{$product->id}}')" class="pointer ps-3 d-flex flex-column gap-2">
          <livewire:item-detail
            :moduleName="$moduleName"
            :item="$product"
            :key="$product->id"
          />
        </td>
        <td wire:click="$emit('showProductDetails', '{{$product->id}}')"
            class="pointer text-end"
        >
          {{ countFormat($product->size) }} {{ @$product->unitOfMeasure->code }}
        </td>
        <td wire:click="$emit('showProductDetails', '{{$product->id}}')"
            class="pointer text-end p-0 pt-2"
        >
          <div class="d-flex gap-2 justify-content-between">
            <div class="w-50">Normal</div>
            <div class="text-end pe-1">{{ numberFormat($product->normal_price) }}</div>
          </div>
          <div class="d-flex gap-2 justify-content-between">
            <div class="w-50">Agent</div>
            <div class="text-end pe-1">{{ numberFormat($product->agent_price) }}</div>
          </div>
        </td>
        <td class="text-end pe-2">
          <a
            class="me-2 pointer"
            wire:click="$emit('productFee', '{{$product->id}}')"
            data-toogle="tooltip"
            title="Product Fee"
          >
            <i class="far fa-money-bill-alt" aria-hidden="true"></i>
          </a>
          <a
            class="pointer me-2"
            wire:click="$emit('editProduct', '{{$product->id}}')">
            <i class="fa fa-pencil"></i>
          </a>
          <a
            class="pointer text-danger"
            wire:click="$emit('deleteProduct', '{{$product->id}}')">
            <i class="fa fa-remove"></i>
          </a>
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="12" class="text-center">Data Empty</td>
      </tr>
    @endforelse
    </tbody>
  </table>
  {{ $products->links() }}
</div>
 
 @push('scripts')
   <script>
     Livewire.on('deleteProduct', (id) => {
       Swal.fire({
         title: 'Are you sure?',
         text: "You won't be able to revert this!",
         icon: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Yes, delete it!'
       }).then((result) => {
         if (result.isConfirmed) {
           Livewire.emit('destroy', id)
         }
       })
     })
   </script>

@endpush