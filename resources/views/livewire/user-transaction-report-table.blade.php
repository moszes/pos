 <div class="col-12">
  <div class="row align-items-end justify-content-between pb-5">
    <div class="col-3">
      <h3>{{ $titlePage }}</h3>
    </div>
    <div class="col-3">
      <input wire:model.debounce.200ms="search" class="form-control search-input search form-control-sm" type="search" placeholder="Search" aria-label="Search">
    </div>
  </div>
  <div class="table-responsive scrollbar">
    <table class="table table-hover fs--1 mb-0 ">
      <thead>
        <tr>
          <th class="sort px-3 text-nowrap" scope="col">Sales Invoice No.</th>
          <th class="sort px-3 text-nowrap" scope="col">Package No.</th>
          <th class="sort px-3 text-nowrap" scope="col">Appointment No.</th>
          <th class="sort px-3" scope="col">Customer</th>
          <th class="sort px-3" scope="col">Module</th>
          <th class="sort px-3" scope="col">Code</th>
          <th class="sort px-3" scope="col">Name</th>
          <th class="sort px-3 text-center" scope="col">Qty</th>
          <th class="sort px-3 text-end" scope="col">Price</th>
          <th class="sort px-3 text-end" scope="col">Discount</th>
          <th class="sort px-3 text-end text-nowrap" scope="col">Total Price</th>
        </tr>
      </thead>
      <tbody class="list">
        @forelse($user_transactions as $key => $user_transaction)
        <tr>
          <td wire:click="$emit('addItemFromTransaction', '{{ $user_transaction }}')" class="px-3 pointer">{{ @getSalesInvoiceById($user_transaction->sales_invoice_id)->sales_invoice_number }}</td>
          <td wire:click="$emit('addItemFromTransaction', '{{ $user_transaction }}')" class="px-3 pointer text-nowrap">{{ @getPackageById($user_transaction->package_id)->code }}</td>
          <td wire:click="$emit('addItemFromTransaction', '{{ $user_transaction }}')" class="px-3 pointer">{{ @getAppointmentById($user_transaction->appointment_id)->appointment_number }}</td>
          <td wire:click="$emit('addItemFromTransaction', '{{ $user_transaction }}')" class="px-3 pointer">{{ @getUserById($user_transaction->user_id)->name }}</td>
          <td wire:click="$emit('addItemFromTransaction', '{{ $user_transaction }}')" class="px-3 pointer">{{ @$user_transaction->module }}</td>
          <td wire:click="$emit('addItemFromTransaction', '{{ $user_transaction }}')" class="px-3 pointer text-nowrap">{{ @getUserTransaction($user_transaction->module, $user_transaction->module_id)->code }}</td>
          <td wire:click="$emit('addItemFromTransaction', '{{ $user_transaction }}')" class="px-3 pointer">{{ @getUserTransaction($user_transaction->module, $user_transaction->module_id)->name }}</td>
          <td wire:click="$emit('addItemFromTransaction', '{{ $user_transaction }}')" class="px-3 pointer text-center">{{ @$user_transaction->qty }}</td>
          <td wire:click="$emit('addItemFromTransaction', '{{ $user_transaction }}')" class="px-3 pointer text-end">{{ @numberFormat($user_transaction->price) }}</td>
          <td wire:click="$emit('addItemFromTransaction', '{{ $user_transaction }}')" class="px-3 pointer text-end">{{ @numberFormat($user_transaction->discount_item) }}</td>
          <td wire:click="$emit('addItemFromTransaction', '{{ $user_transaction }}')" class="px-3 pointer text-end ">{{ @numberFormat($user_transaction->total_price) }}</td>
        </tr>
        @empty
        <tr>
          <td colspan="11" class="text-center">Empty Data</td>
        </tr>
        @endforelse
      </tbody>
    </table>
    {{ $user_transactions->links() }}
  </div>
</div>