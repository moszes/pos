<div class="d-flex flex-column gap-3 text-end pe-3">
	<h4>Today Sales</h4>
	<h4 class="text-400 fw-bold text-danger ">{{ priceFormat($todaySales) }}</h4>
</div>