<div>
	<div class="row flex-between-center mb-4 g-3">
		<div class="col-auto">
			<h4>Monthly Sales</h4>
			<p class="text-700 lh-sm mb-0">Total Sales Every Month</p>
		</div>
		<div class="col-8 col-sm-4">
			<div class="form-group">
				<label for="selectedMonth">Month</label>
				<input type="month" id="selectedMonth" class="form-control" wire:model="selectedMonth">
			</div>
		</div>
	</div>
	<div class="echart-total-sales-chart" style="min-height: 320px; width: 100%; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); position: relative;" ></div>
</div>

@push('scripts')
	<script>

		document.addEventListener('DOMContentLoaded', function () {

			loadChart()

			function loadChart(){
				
				(function (factory) {
					typeof define === 'function' && define.amd ? define(factory) :
						factory();
				})((function () { 'use strict';
					// import * as echarts from 'echarts';
					const { merge } = window._;
	
					// form config.js
					const echartSetOption = (
						chart,
						userOptions,
						getDefaultOptions,
						responsiveOptions
					) => {
						const { breakpoints, resize } = window.phoenix.utils;
						const handleResize = options => {
							Object.keys(options).forEach(item => {
								if (window.innerWidth > breakpoints[item]) {
									chart.setOption(options[item]);
								}
							});
						};
	
						const themeController = document.body;
						// Merge user options with lodash
						chart.setOption(merge(getDefaultOptions(), userOptions));
	
						const navbarVerticalToggle = document.querySelector(
							'.navbar-vertical-toggle'
						);
						if (navbarVerticalToggle) {
							navbarVerticalToggle.addEventListener('navbar.vertical.toggle', () => {
								chart.resize();
								if (responsiveOptions) {
									handleResize(responsiveOptions);
								}
							});
						}
	
						resize(() => {
							chart.resize();
							if (responsiveOptions) {
								handleResize(responsiveOptions);
							}
						});
						if (responsiveOptions) {
							handleResize(responsiveOptions);
						}
	
						themeController.addEventListener(
							'clickControl',
							({ detail: { control } }) => {
								if (control === 'phoenixTheme') {
									chart.setOption(window._.merge(getDefaultOptions(), userOptions));
								}
							}
						);
					};
					// -------------------end config.js--------------------
	
					const echartTabs = document.querySelectorAll('[data-tab-has-echarts]');
					if (echartTabs) {
						echartTabs.forEach(tab => {
							tab.addEventListener('shown.bs.tab', e => {
								const el = e.target;
								const { hash } = el;
								const id = hash || el.dataset.bsTarget;
								const content = document.getElementById(id.substring(1));
								const chart = content?.querySelector('[data-echart-tab]');
								if (chart) {
									window.echarts.init(chart).resize();
								}
							});
						});
					}
	
					/* -------------------------------------------------------------------------- */
					/*                             Echarts Total Sales                            */
					/* -------------------------------------------------------------------------- */
					
					const totalSalesChartInit = () => {
						const { getColor, getData } = window.phoenix.utils;
						const $totalSalesChart = document.querySelector('.echart-total-sales-chart');
						
						const currentMonthData = @this.salesDataByMonth;
						const prevMonthData = JSON.parse(localStorage.getItem('monthlySalesData'));
	
						const tooltipFormatter = params => {
							const currentDate = window.dayjs(params[0].axisValue);
							const prevDate = window.dayjs(params[0].axisValue).subtract(1, 'month');
	
							const result = params.map((param, index) => ({
								value: param.value,
								date: index > 0 ? prevDate : currentDate,
								color: param.color
							}));
	
							let tooltipItem = ``;
							result.forEach((el, index) => {
								// Format the number as Rupiah
								const formattedAmount = new Intl.NumberFormat('id-ID', {
									style: 'currency',
									currency: 'IDR',
									minimumFractionDigits: 0,
									maximumFractionDigits: 0
								}).format(el.value);
								tooltipItem += `<h6 class="fs--1 text-700 ${
									index > 0 && 'mb-0'
								}"><span class="fas fa-circle me-2" style="color:${el.color}"></span>
							      ${el.date.format('MMM DD')}: ${formattedAmount}
							    </h6>`;
							});
							return `<div class='ms-1'>
	              ${tooltipItem}
	            </div>`;
						};
	
						if ($totalSalesChart) {
							const userOptions = getData($totalSalesChart, 'echarts');
							const chart = window.echarts.init($totalSalesChart);
	
	
	
							const getDefaultOptions = () => ({
								color: [getColor('primary'), getColor('info')],
								tooltip: {
									trigger: 'axis',
									padding: 10,
									backgroundColor: getColor('gray-100'),
									borderColor: getColor('gray-300'),
									textStyle: { color: getColor('dark') },
									borderWidth: 1,
									transitionDuration: 0,
									axisPointer: {
										type: 'none'
									},
									formatter: tooltipFormatter
								},
								xAxis: [
									{
										type: 'category',
										data: @this.allDaysThisMonth,
										axisLabel: {
											formatter: value => window.dayjs(value).format('DD MMM'),
											interval: 13,
											showMinLabel: true,
											showMaxLabel: false,
											color: getColor('gray-800'),
											align: 'left',
											fontFamily: 'Nunito Sans',
											fontWeight: 600,
											fontSize: 12.8
										},
										axisLine: {
											show: true,
											lineStyle: {
												color: getColor('gray-200')
											}
										},
										axisTick: {
											show: false
										},
										splitLine: {
											show: true,
											interval: 0,
											lineStyle: {
												color:
													window.config.config.phoenixTheme === 'dark'
														? getColor('gray-100')
														: getColor('gray-200')
											}
										},
										boundaryGap: false
									},
									{
										type: 'category',
										position: 'bottom',
										data: @this.allDaysThisMonth,
										axisLabel: {
											formatter: value => window.dayjs(value).format('DD MMM'),
											interval: 130,
											showMaxLabel: true,
											showMinLabel: false,
											color: getColor('gray-800'),
											align: 'right',
											fontFamily: 'Nunito Sans',
											fontWeight: 600,
											fontSize: 12.8
										},
										axisLine: {
											show: false
										},
										axisTick: {
											show: false
										},
										splitLine: {
											show: false
										},
										boundaryGap: false
									}
								],
								yAxis: {
									position: 'right',
									axisPointer: { type: 'none' },
									axisTick: 'none',
									splitLine: {
										show: false
									},
									axisLine: { show: false },
									axisLabel: { show: false }
								},
								series: [
									{
										name: 'd',
										type: 'line',
										// data: Array.from(Array(30).keys()).map(() =>
										//   getRandomNumber(100, 300)
										// ),
										data: currentMonthData,
										showSymbol: false,
										symbol: 'circle'
									},
									{
										name: 'e',
										type: 'line',
										// data: Array.from(Array(30).keys()).map(() =>
										//   getRandomNumber(100, 300)
										// ),
										data: prevMonthData,
										// symbol: 'none',
										lineStyle: {
											type: 'dashed',
											width: 1,
											color: getColor('info')
										},
										showSymbol: false,
										symbol: 'circle'
									}
								],
								grid: {
									right: 2,
									left: 5,
									bottom: '20px',
									top: '2%',
									containLabel: false
								},
								animation: true
							});
							
							echartSetOption(chart, userOptions, getDefaultOptions);
						}
					};
	
	
					const { docReady } = window.phoenix.utils;
	
					docReady(totalSalesChartInit);
	
					Livewire.on('loadChart', function(){
						loadChart()
					})
					
				}));
			}
		})
		
	</script>
@endpush