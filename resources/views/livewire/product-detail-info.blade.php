<div class="row">
  <div class="col">
    <livewire:item-detail
      :redirectRoute="$redirectRoute"
      :priceLabel="$priceLabel"
      :moduleName="$moduleName"
      :moduleLabel="$moduleLabel"
      :item="$product"
    />
  </div>
  @role('godadmin')
  <div class="col-6">
    <livewire:service-detail-fee :module="$product"/>
  </div>
  @endrole
</div>