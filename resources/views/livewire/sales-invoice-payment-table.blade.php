<div class="d-flex flex-column">
  <table class="table table-hover table-bordered table-responsive mt-3">
    <thead>
    <tr>
      <th class="px-3 text-center"></th>
      <th class="sort px-3" scope="col">Payment Method</th>
      <th class="sort px-3" scope="col">Reference Number</th>
      <th class="sort text-center" scope="col">Status</th>
      <th class="sort px-3 text-end" scope="col">Amount</th>
    </tr>
    </thead>
    <tbody class="list">
    @forelse($payments as $key => $paymentItem)
      <tr>
        <td class="px-3 text-center">
          <a href="#" wire:click="removeSalesInvoicePayment('{{$key}}')"><i class="fas fa-trash"></i></a>
        </td>
        <td class="px-3">{{ @getPaymentById($paymentItem['payment_id'])->name }}</td>
        <td>{{ @$paymentItem['reference_number'] }}</td>
        <td class="text-center">{{ @$paymentItem['status'] }}</td>
        <td class="text-end px-3">{{ numberFormat(@$paymentItem['amount']) }}</td>
      </tr>
    @empty
      <tr>
        <td colspan="5" class="text-center">Empty Data</td>
      </tr>
    @endforelse
    <tr>
      <td colspan="4" class="text-end px-3">Grand Total</td>
      <td class="text-end px-3">{{ @numberFormat($grandTotal) }}</td>
    </tr>
    <tr>
      <td colspan="4" class="text-end px-3">Diff Payment</td>
      <td class="text-end px-3">{{ @numberFormat($diffPayment) }}</td>
    </tr>
    <tr>
      <td colspan="4" class="text-end px-3">Total Payment</td>
      <td class="text-end px-3">{{ @numberFormat($totalPayment) }}</td>
    </tr>
    </tbody>
  </table>
</div>