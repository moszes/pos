<div class="d-flex flex-column gap-3">
	<div class="card">
		<div class="card-body">
			<div class="d-flex flex-column gap-3">
				<div>
					<div class="d-flex flex-column gap-3">
						<label for="searchPackageItem"
						       class="form-label">
							Search Product Or Treatment
						</label>
						<input type="text"
						       id="searchPackageItem"
						       class="form-control form-control-sm"
						       wire:model.debounce.200ms="searchPackageItem"
						       placeholder="Search Treatment Or Product"
						>
					</div>
					@if($packageItemList)
						<ul class="list-group">
							@forelse($searchData as $data)
								@php($module = $data['module'])
								@php($moduleId = $data['module_id'])
								<li wire:click="selectPackageItem('{{ $module }}', '{{ $moduleId }}')" class="list-group-item pointer"
								    wire:loading.class.remove="disabled" disabled>
									{{ $data['code'] }}, {{ $data['name'] }}, {{ numberFormat($data['price']) }}
								</li>
							@empty
								<li class="list-group-item">Empty Product</li>
							@endforelse
						</ul>
					@endif
				</div>
				@if($packageItemForm)
					<div class="d-flex flex-column justify-content-between gap-3">
						<div class="row">
							<div class="col">
								<div class="d-flex flex-column gap-3 col">
									<label for="itemCode" class="hidden">Item Code</label>
									<input type="text" wire:model="itemCode" id="itemCode" class="form-control" readonly>
								</div>
							</div>
							<div class="col">
								<div class="d-flex flex-column gap-3 col">
									<label for="itemName">Item Name</label>
									<input type="text" wire:model="itemName" id="itemName" class="form-control" readonly>
								</div>
							</div>
							<div class="d-flex flex-column gap-3 col">
								<label for="itemQty">Qty</label>
								<input type="number" wire:model="itemQty" id="itemQty" class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col">
								<div class="d-flex flex-column gap-3 col">
									<label for="itemNormalPrice">Item Normal Price</label>
									<input type="text" wire:model="itemNormalPrice" id="itemNormalPrice" class="form-control" readonly>
								</div>
							</div>
							<div class="col">
								<div class="d-flex flex-column gap-3 col">
									<label for="packageItemPrice">Package Item Price</label>
									<input type="text" wire:model="packageItemPrice" id="packageItemPrice" class="form-control">
								</div>
							</div>
							<div class="col">
								<div class="d-flex flex-column gap-3 col">
									<label for="totalPackagePrice">Total</label>
									<input type="number" wire:model="totalPackagePrice" id="totalPackagePrice" class="form-control"
									       readonly>
								</div>
							</div>
						</div>
					</div>
					<div class="text-end mt">
						<button class="btn btn-primary btn-sm" wire:click="addPackageItem">Add</button>
					</div>
				@endif</div>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<h5 class="mb-5 mt-2 text-center">Package Item List</h5>
			<table class="table table-hover fs--1 mb-0 ">
				<thead>
				<tr>
					<th class="sort ps-2" scope="col"></th>
					<th class="sort" scope="col">Item</th>
					<th class="sort text-end pe-2" scope="col">Total</th>
				</tr>
				</thead>
				<tbody class="list">
				@forelse($items as $key => $item)
					@php($packageItem = getDataByModule($item['module'], $item['module_id']))
					<tr>
						<td class="text-center">
							<button class="btn btn-sm btn-danger" wire:click="removeItem('{{ $key }}')">
								<i class="fa fa-trash"></i>
							</button>
						</td>
						<td>
							<div class="d-flex justify-content-between">
								<div>Code</div>
								<div class="text-end">
									{{ $packageItem->code }}
								</div>
							</div>
							<div class="d-flex justify-content-between">
								<div>
									Name
								</div>
								<div class="text-end">
									{{ $packageItem->name }}
								</div>
							</div>
							<div class="d-flex justify-content-between">
								<div>
									Qty
								</div>
								<div class="text-end">
									{{ countFormat($item['qty']) }}
								</div>
							</div>
							<div class="d-flex justify-content-between">
								<div>Normal Price</div>
								<div class="text-end">
									{{ @numberFormat($item['package_item_price']) }}
								</div>
							</div>
							<div class="d-flex justify-content-between">
								<div>Agent Price</div>
								<div class="text-end">
									{{ @$packageItem->module == 'product' ? @priceFormat($packageItem->normal_price) : @priceFormat($packageItem->price) ?? 0 }}
								</div>
							</div>
						</td>
						<td class="text-end">{{ @numberFormat($item['total_package_price']) }}</td>
					</tr>
				@empty
					<tr>
						<td colspan="11" class="text-center">Empty Data</td>
					</tr>
				@endforelse
				</tbody>
			</table>
			<div class="d-flex justify-content-end gap-3 mt-3">
				<button class="btn btn-primary" wire:click="saveOrUpdate">Save</button>
			</div>
		</div>
	</div>
</div>