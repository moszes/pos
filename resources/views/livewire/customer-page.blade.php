<div class="shadow rounded">
  <livewire:breadcrumb />
  <div class="card">
    <div class="card-body">
      <livewire:customer-table />
    </div>
  </div>
</div>