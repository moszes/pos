<div class="card">
  <div class="card-body scrollbar-overlay">
    <table class="table table-hover">
      <thead>
      <th class="px-3 text-nowrap">Date</th>
      <th class="px-3 text-nowrap">Invoice</th>
      <th class="px-3 text-nowrap">Customer</th>
      <th class="px-3 text-nowrap">Phone</th>
      <th class="px-3 text-nowrap">Sub Total</th>
      <th class="px-3 text-nowrap">Tax</th>
      <th class="px-3 text-nowrap">Others</th>
      <th class="px-3 text-nowrap">Discount</th>
      <th class="px-3 text-nowrap">Grand Total</th>
      <th class="px-3 text-nowrap">Cash</th>
      <th class="px-3 text-nowrap">Card</th>
      <th class="px-3 text-nowrap">Custom</th>
      <th class="px-3 text-nowrap">Prepaid</th>
      <th class="px-3 text-nowrap">QR Code</th>
      <th class="px-3 text-nowrap">Voucher</th>
      </thead>
      <tbody>
      @if($salesSummaries)
      @forelse($salesSummaries as $salesSummary)
        <tr>
          <td class="px-3 text-nowrap">{{ dateFormat($salesSummary->sales_invoice_date) }}</td>
          <td class="px-3 text-nowrap">{{ $salesSummary->sales_invoice_number }}</td>
          <td class="px-3 text-nowrap">{{ $salesSummary->customer->details->first_name }}</td>
          <td class="px-3 text-nowrap">{{ $salesSummary->customer->details->phone }}</td>
          <td class="px-3 text-nowrap">{{ numberFormat($salesSummary->items->sum('total_price')) }}</td>
          <td class="px-3 text-nowrap">{{ numberFormat($salesSummary->tax) }}</td>
          <td class="px-3 text-nowrap">{{ numberFormat($salesSummary->others) }}</td>
          <td class="px-3 text-nowrap">{{ numberFormat($salesSummary->discount) }}</td>
          <td class="px-3 text-nowrap">{{ numberFormat($salesSummary->grand_total) }}</td>
          <td class="px-3 text-nowrap">{{ numberFormat($salesSummary->payments->where('payment_id', '1')->sum('amount')) }}</td>
          <td class="px-3 text-nowrap">{{ numberFormat($salesSummary->payments->where('payment_id', '2')->sum('amount')) }}</td>
          <td class="px-3 text-nowrap">{{ numberFormat($salesSummary->payments->where('payment_id', '3')->sum('amount')) }}</td>
          <td class="px-3 text-nowrap">{{ numberFormat($salesSummary->payments->where('payment_id', '4')->sum('amount')) }}</td>
          <td class="px-3 text-nowrap">{{ numberFormat($salesSummary->payments->where('payment_id', '5')->sum('amount')) }}</td>
          <td class="px-3 text-nowrap">{{ numberFormat($salesSummary->payments->where('payment_id', '6')->sum('amount')) }}</td>
        </tr>
      @empty
        <tr>
          <td colspan="14" class="text-center">Empty Data</td>
        </tr>
      @endforelse
      @endif
      </tbody>
    </table>
  </div>
  <div class="card-footer d-flex justify-content-end">
    <a class="btn btn-primary" href="{{ route('generate_excel') }}?salesSummaries=1&dateFrom={{ dateFormat($dateFrom) }}&dateTo={{ dateFormat($dateTo) }}@if($customer)&customer={{ $customer['id'] }}@endif">Download Sales Summary Report</a>
  </div>
</div>