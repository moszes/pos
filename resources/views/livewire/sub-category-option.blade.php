<div class="mb-3 col">
  <label class="form-label" for="subCategoryId">Sub Category</label>
  <select wire:model.debounce.200ms="subCategoryId" class="form-select" id="subCategoryId" aria-label="Select category">
    <option>-- Choose --</option>
    {!! $subCategoryOption !!}
  </select>
  @error('subCategoryId') <span class="text-danger">{{ $message }}</span> @enderror
  
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>