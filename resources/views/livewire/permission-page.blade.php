<div class="shadow rounded">
  <livewire:breadcrumb />
  <div class="card">
    <div class="card-body">
        <livewire:permission-table/>
    </div>
  </div>
</div>