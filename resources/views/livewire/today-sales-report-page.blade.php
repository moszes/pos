<div class="shadow rounded">
  <livewire:breadcrumb />
  <div class="card">
    <div class="card-body">
      <livewire:today-sales-report-table :titlePage="$titlePage"/>
    </div>
  </div>
</div>