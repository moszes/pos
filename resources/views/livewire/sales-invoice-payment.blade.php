<div class="col card">
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
  <div class="card-body">
    <h3 class="text-center mb-3">Sales Invoice Payments</h3>
    <table class="table table-hover">
      <thead>
      <tr>
        <th class="px-3">Type</th>
        <th class="px-3">Reference</th>
        <th class="px-3">Status</th>
        <th class="px-3">Amount</th>
      </tr>
      </thead>
      <tbody>
      @if($salesInvoice)
      @forelse($salesInvoice->payments as $payment)
        <tr>
          <td class="px-3">{{ getPaymentById($payment->payment_id)->name }}</td>
          <td class="px-3">{{ $payment->reference }}</td>
          <td class="px-3">{{ $payment->status }}</td>
          <td class="px-3 text-end">{{ numberFormat($payment->amount) }}</td>
        </tr>
      @empty
        <tr>
          <td class="text-center">Empty Data</td>
        </tr>
      @endforelse
      @endif
      </tbody>
    </table>
  </div>
</div>