<div wire:ignore.self class="modal modal-xl fade" id="treatmentDetail" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ $formTitle }}</h5>
        <button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close">
          <span class="fas fa-times fs--1"></span>
        </button>
      </div>
      <div class="modal-body d-flex flex-column gap-4">
        @if($treatment)
        <livewire:treatment-detail-info :treatment="$treatment" />
        <livewire:treatment-detail-material-usage :treatment="$treatment" />
        <livewire:treatment-detail-price-history :treatment="$treatment" />
        @endif
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>
  let treatmentDetail = new bootstrap.Modal(
    document.getElementById('treatmentDetail'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showTreatmentDetail', () => {
    treatmentDetail.show();
  })
  Livewire.on('hideTreatmentDetail', () => {
    treatmentDetail.hide();
  })
  document.getElementById('treatmentDetail').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearTreatmentDetail')
  })
</script>
@endpush