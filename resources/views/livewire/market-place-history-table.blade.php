<div class="d-flex flex-column gap-3 justify-content-center">
  <div class="d-flex justify-content-center">
    <h5>Market Place Price History</h5>
  </div>
	<div class="col-12">
		<div class="table-responsive scrollbar">
			<table class="table table-hover fs--1 mb-0 ">
				<thead>
					<tr>
						<th class="sort ps-2" scope="col">Date</th>
						<th class="sort" scope="col">User</th>
						<th class="sort text-center" scope="col">Action</th>
						<th class="sort pe-2 text-end" scope="col">Price</th>
					</tr>
				</thead>
				<tbody class="list">
				@forelse($priceHistories as $key => $priceHistory)
					@php($price = json_decode($priceHistory->properties))
					<tr>
						<td class="ps-2">{{ date('d-m-Y', strtotime($priceHistory->created_at)) }}</td>
						<td>{{ @getUserById($priceHistory->causer_id)->name }}</td>
						<td class=" text-center">{{ $priceHistory->description }}</td>
						<td class="pe-2 text-end">{{ @numberFormat($price->attributes->price) }}</td>
					</tr>
				@empty
					<tr>
						<td colspan="5" class="text-center">Empty Data</td>
					</tr>
				@endforelse
				</tbody>
			</table>
			{{ $priceHistories->links() }}
		</div>
		<div class="loading d-none" wire:loading.class.remove="d-none"></div>
	</div>
</div>