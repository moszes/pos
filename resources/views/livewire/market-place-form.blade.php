<div>
  <button class="btn btn-sm btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#marketPlaceForm">+</button>
  <div wire:ignore.self class="modal modal-xl fade" id="marketPlaceForm" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5>
          <button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close">
            <span class="fas fa-times fs--1"></span>
          </button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div class="d-flex flex-column gap-3">
              <div class="col mt-2">
                <div class="d-flex gap-3">
                  <livewire:category-option :type="$type"/>
                  <livewire:sub-category-option />
                </div>
                <div class="d-flex gap-3">
                  <div class="mb-3 col">
                    <label class="form-label" for="code">Code</label>
                    <input wire:model.debounce.200ms="code" id="code" class="form-control" type="text">
                    @error('code') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                  <div class="mb-3 col">
                    <label class="form-label" for="name">Name</label>
                    <input wire:model.debounce.200ms="name" id="name" class="form-control" type="text" autocomplete="off" >
                    @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                  <div class="mb-3 col">
                    <label class="form-label" for="price">Price</label>
                    <input
                      wire:model.debounce.200ms="price"
                      id="price"
                      class="form-control"
                      type="number"
                    >
                    @error('price') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                </div>
              </div>
            </div>
            
            <div class="float-end mt-5">
              <button type="button" wire:click="$emit('clearMarketPlaceForm')" class="btn btn-light">Clear</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>

@push('scripts')
<script>
	let marketPlaceForm = new bootstrap.Modal(
                        document.getElementById('marketPlaceForm'),
                        {
                          backdrop: 'static',
                        }
                      )
  Livewire.on('showMarketPlaceForm', () => {
    marketPlaceForm.show();
  })
  Livewire.on('hideMarketPlaceForm', () => {
    marketPlaceForm.hide();
  })
  document.getElementById('marketPlaceForm').addEventListener('show.bs.modal', () => {
    Livewire.emit('setCategoryType', 'treatment');
  })
  document.getElementById('marketPlaceForm').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearMarketPlaceForm');
    Livewire.emit('clearItem');
  })
</script>
@endpush