<div class="d-flex flex-column mb-3">
  <div class="col">
    <label class="form-label" for="purchaseInvoiceSearch">Purchase Invoice</label>
    <input wire:model.debounce.100ms="purchaseInvoiceSearch" id="purchaseInvoiceSearch" class="form-control" type="text" placeholder="Search Purchase Invoice...">
    @error('purchaseInvoiceSearch') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
  @if($purchaseInvoiceSearching)
    <ul class="list-group">
      @forelse($purchaseInvoiceLists as $purchaseInvoiceList)
        <li wire:click="purchaseInvoiceSearchSelected({{$purchaseInvoiceList->id}})" class="list-group-item pointer">{{ $purchaseInvoiceList->invoice_number}}, {{ $purchaseInvoiceList->note}}</li>
      @empty
        <li class="list-group-item">Empty Purchase Invoice, You need to approve Purchase Invoice First</li>
      @endforelse
    </ul>
  @endif
</div>
