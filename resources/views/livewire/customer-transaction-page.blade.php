<div class="d-flex flex-column gap-3 p-3">
  <div class="d-flex justify-content-between">
    <h3>User Transaction</h3>
    <h3>{{ $date }}</h3>
    <h4>{{ $user->name }}</h4>
  </div>
  <div class="table-responsive scrollbar">
    <table class="table table-hover fs--1 mb-0 ">
      <thead>
      <tr>
        <th class="sort px-3" scope="col">Sales Invoice Number</th>
        <th class="sort px-3" scope="col">Module</th>
        <th class="sort px-3" scope="col">Code</th>
        <th class="sort px-3" scope="col">Name</th>
        <th class="sort px-3" scope="col">Reference</th>
        <th class="sort px-3 text-center" scope="col">Qty</th>
      </tr>
      </thead>
      <tbody>
      @forelse($user_transactions as $user_transaction)
        <tr>
          <td class="px-3 pointer">{{ @getSalesInvoiceById($user_transaction->sales_invoice_id)->sales_invoice_number }}</td>
          <td class="px-3 pointer">{{ @$user_transaction->module }}</td>
          <td class="px-3 pointer">{{ @getUserTransaction($user_transaction->module, $user_transaction->module_id)->code }}</td>
          <td class="px-3 pointer">{{ @getUserTransaction($user_transaction->module, $user_transaction->module_id)->name }}</td>
          <td class="px-3 pointer">{{ $user_transaction->reference_module }}</td>
          <td class="px-3 pointer text-center">{{ @numberFormat($user_transaction->qty) }}</td>
        </tr>
      @empty
        <tr>
          <td colspan="11" class="text-center">Empty Data</td>
        </tr>
      @endforelse
      </tbody>
    </table>
    {{ $user_transactions->links()  }}
  </div>
</div>