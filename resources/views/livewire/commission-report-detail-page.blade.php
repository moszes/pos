<div class="shadow rounded">
	<livewire:commission-report-detail-table />
</div>


@push('scripts')
	<script>
		window.addEventListener("DOMContentLoaded", function () {
			Livewire.emit("openReportMenu")
			Livewire.emit("openSalesReportModule")
		});
	</script>
@endpush