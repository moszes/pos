<div class="d-flex flex-column gap-6">
  
  <div class="card">
    <div class="card-body">
      <div class="d-flex justify-content-between align-items-center">
        <h4>{{ $companyName }} - {{ $branchName }}</h4>
      </div>
      <div>
      </div>
    </div>
  </div>
  
  @if($widget)
  <div class="row">
    <div class="col-8">
      <livewire:dashboard-turn-over :selectedMonth="$selectedMonth" />
    </div>
    <div class="col">
      <div class="row">
        <div class="col-6">
          <livewire:dashboard-monthly-sales :selectedMonth="$selectedMonth" />
        </div>
        <div class="col-6">
          <livewire:dashboard-today-sales />
        </div>
      </div>
    </div>
  </div>
  
  <div class="row">
    <div class="col-8 card">
      <div class="card-body">
        <livewire:dashboard-business-trend :selectedMonth="$selectedMonth" />
        <hr class="bg-200 mb-6 mt-4">
        <livewire:dashboard-turn-over-chart :selectedMonth="$selectedMonth" />
      </div>
    </div>
    <div class="col-4">
      <div class="card">
        <div class="card-body">
          <livewire:dashboard-customer-birthday />
        </div>
      </div>
    </div>
  </div>
  @endif
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>