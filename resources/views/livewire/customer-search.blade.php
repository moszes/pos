<div class="d-flex flex-column gap-3">
  <div class="d-flex gap-3">
    <div class="col">
      <label class="form-label" for="customerSearch">Customer</label>
      <input wire:model.debounce.100ms="customerSearch" wire:click="clearCustomerSearch" id="customerSearch" class="form-control" type="text" >
      @error('customerSearch') <div class="text-danger">{{ $message }}</div> @enderror
      @if($customerList)
        <ul class="list-group">
          @forelse($customers as $customerData)
            <li wire:click="selectCustomer('{{@$customerData->user_id}}')" class="list-group-item pointer">
              {{ @$customerData->first_name }} {{ @getCustomerPhone($customerData->user_id) }} {{ @$customerData->email }}
            </li>
          @empty
            <li class="list-group-item">Empty Customer</li>
          @endforelse
        </ul>
      @endif
    </div>
  </div>
</div>