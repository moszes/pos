<div class="shadow rounded">
  <livewire:breadcrumb />
  <div class="card">
    <div class="card-body">
      <livewire:branch-table />
    </div>
  </div>
</div>

@push('scripts')
  <script>
    window.addEventListener("DOMContentLoaded", function () {
      Livewire.emit("openMasterDataModule");
      Livewire.emit("openCompaniesMenu")
    });
  </script>
@endpush