<div class="shadow rounded">
  <livewire:breadcrumb />
  <div class="card">
    <div class="card-body">
      <livewire:sub-category-table/>
    </div>
  </div>
</div>