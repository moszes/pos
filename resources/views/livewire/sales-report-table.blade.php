 <div class="col-12">
  <div class="row align-items-end justify-content-between pb-5">
    <div class="col-3">
      <h3>{{ $titlePage }}</h3>
    </div>
  </div>
  @if(auth()->user()?->hasRole('godadmin'))
  <div class="card mb-5 mt-3">
   <div class="card-body">
     <div class="d-flex gap-3">
       <label for="date">Sales Invoice By Date</label>
       <input type="date" id="date" class="form-control w-25" wire:model="date" placeholder="Date">
     </div>
   </div>
 </div>
  @endif
  <div class="table-responsive scrollbar">
    <table class="table table-hover fs--1 mb-0 ">
      <thead>
        <tr>
          <th class="sort px-3" scope="col">INV Date</th>
          <th class="sort px-3" scope="col">Inv Number</th>
          <th class="sort px-3" scope="col">Appointment Number</th>
          <th class="sort px-3" scope="col">Customer</th>
          <th class="sort px-3" scope="col">Discount</th>
          <th class="sort px-3" scope="col">Tax</th>
          <th class="sort px-3" scope="col">Others</th>
          <th class="sort px-3" scope="col">Grand Total</th>
          <th class="sort px-3" scope="col">Total Payment</th>
          <th class="sort px-2 text-center" scope="col">Action</th>
        </tr>
      </thead>
      <tbody class="list">
        @forelse($sales_invoices as $key => $sales_invoice)
        <tr>
          <td class="px-3">{{ $sales_invoice->sales_invoice_date }}</td>
          <td class="px-3">{{ $sales_invoice->sales_invoice_number }}</td>
          <td class="px-3">{{ @$sales_invoice->appointment->appointment_number }}</td>
          <td class="px-3">{{ @$sales_invoice->customer->name }}</td>
          <td class="px-3">{{ numberFormat($sales_invoice->discount) }}</td>
          <td class="px-3">{{ numberFormat($sales_invoice->tax) }}</td>
          <td class="px-3">{{ numberFormat($sales_invoice->others) }}</td>
          <td class="px-3">{{ numberFormat($sales_invoice->grand_total) }}</td>
          <td class="px-3">{{ numberFormat($sales_invoice->total_payment) }}</td>
          <td class="text-center p-0 pt-3">
            <button class="btn btn-sm btn-primary text-center mr-2" wire:click="downloadSalesInvoiceReportByRow('{{$sales_invoice}}')">
              <i class="fa-solid fa-download"></i>
            </button>
          </td>
        </tr>
        @empty
        <tr>
          <td colspan="15" class="text-center">Empty Data</td>
        </tr>
        @endforelse
      </tbody>
    </table>
    <div class="py-4">
      <button class="btn btn-success float-end" wire:click="downloadSalesInvoiceReport('{{$sales_invoices}}')">Download Report</button>
    </div>
  </div>
</div>