<li class="nav-item">
	<a class="nav-link dropdown-indicator" id="services-btn" href="#nv-services" data-bs-toggle="collapse" aria-expanded="false" aria-controls="nv-services">
		<div class="d-flex align-items-center">
			<div class="dropdown-indicator-icon">
				<span class="fas fa-caret-right"></span>
			</div>
			<span class="nav-link-text">Services</span>
		</div>
	</a>
	<!-- more inner pages-->
	<div class="parent-wrapper">
		<ul class="nav collapse parent" data-bs-parent="#forms" id="nv-services">
			<li class="nav-item">
				<a
					class="nav-link @if(Route::currentRouteName() == 'master.products' || Route::currentRouteName() == 'master.product_fees') active @endif"
					href="{{ route('master.products') }}"
					data-bs-toggle=""
					aria-expanded="false"
				>
					<div class="d-flex align-items-center">
						<span class="nav-link-text">Product</span>
					</div>
				</a>
				<!-- more inner pages-->
			</li>
			@if(getSetting('Application Module','treatment'))
			<li class="nav-item">
				<a
					class="nav-link @if(Route::currentRouteName() == 'master.treatments' || Route::currentRouteName() == 'master.treatment_fees' || Route::currentRouteName() == 'master.treatment_material_usages') active @endif"
					href="{{ route('master.treatments') }}"
					data-bs-toggle=""
					aria-expanded="false">
					<div class="d-flex align-items-center">
						<span class="nav-link-text">Treatment</span>
					</div>
				</a>
				<!-- more inner pages-->
			</li>
			@endif
			@if(getSetting('Application Module','marketplace'))
			<li class="nav-item">
				<a
					class="nav-link @if(Route::currentRouteName() == 'master.market_places' || Route::currentRouteName() == 'master.market_place_fees' || Route::currentRouteName() == 'master.market_place_material_usages') active @endif"
					href="{{ route('master.market_places') }}"
					data-bs-toggle=""
					aria-expanded="false"
				>
					<div class="d-flex align-items-center">
						<span class="nav-link-text">Marketplace</span>
					</div>
				</a>
				<!-- more inner pages-->
			</li>
			@endif
			@if(getSetting('Application Module','package'))
			<li class="nav-item">
				<a
					class="nav-link @if(Route::currentRouteName() == 'master.packages'  || Route::currentRouteName() == 'master.package_fees' || Route::currentRouteName() == 'master.packageItems') active @endif"
					href="{{ route('master.packages') }}"
					data-bs-toggle=""
					aria-expanded="false"
				>
					<div class="d-flex align-items-center">
						<span class="nav-link-text">Package</span>
					</div>
				</a>
				<!-- more inner pages-->
			</li>
			@endif
			@if(getSetting('Application Module','subscription'))
			<li class="nav-item">
				<a
					class="nav-link @if(Route::currentRouteName() == 'master.subscriptionPlans' || Route::currentRouteName() == 'master.subscriptionPlanItems')  active @endif"
					href="{{ route('master.subscriptionPlans') }}"
					data-bs-toggle=""
					aria-expanded="false"
				>
					<div class="d-flex align-items-center"><span class="nav-link-text">Subscription Plan</span>
					</div>
				</a>
				<!-- more inner pages-->
			</li>
			@endif
		</ul>
	</div>
</li>