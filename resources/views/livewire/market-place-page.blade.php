<div class="shadow rounded">
  <livewire:breadcrumb />
  <div class="card">
    <div class="card-body">
      <livewire:market-place-table/>
    </div>
  </div>
</div>