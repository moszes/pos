<div class="d-flex justify-content-center align-items-center vh-100 text-center">
  <div>
    <h1>401 - Unauthorized Access</h1>
    <p>You do not have the necessary permissions to view this page.
      <a href="{{ url('/login') }}">Please Login</a>
    </p>
    <img src="{{ asset('401.png') }}" style="max-width: 25%;" alt="Unauthorized access image">
  </div>
</div>