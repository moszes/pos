<div class="card">
  <div class="card-body">
    <div class="card-title text-center">
      <h4>Sales Detail Report</h4>
    </div>
    <form class="d-flex flex-column gap-3">
      <div class="row">
        <div class="col">
          <label for="from">From</label>
        </div>
        <div class="col">
          <input type="date" class="form-control" id="from" wire:model="dateFrom">
        </div>
        <div class="col">
          <div class="d-grid gap-2">
            <button class="btn btn-primary" type="button" wire:click="getSalesDetailReport">View Document</button>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label for="to">To</label>
        </div>
        <div class="col">
          <input type="date" class="form-control" id="to" wire:model="dateTo">
        </div>
        <div class="col">
          <div class="d-grid gap-2">
            <button class="btn btn-success" type="button" wire:click="$emit('refreshSalesDetailReport')">Refresh</button>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label for="customerSearch">Customer</label>
        </div>
        <div class="col">
          <input type="text" class="form-control col" id="customerSearch" wire:model="customerSearch">
          @if($customerSearching)
            <ul class="list-group">
              @forelse($customerLists as $customerList)
                <li wire:click="selectCustomer({{ $customerList }})" class="list-group-item">{{ $customerList->first_name . ' ' . $customerList->phone }}</li>
              @empty
                <li class="list-group-item">Empty Data</li>
              @endforelse
            </ul>
          @endif
        </div>
        <div class="col"></div>
      </div>
    </form>
  </div>
</div>

@push('scripts')
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      Livewire.on('dateFromAndDateToRequired', () => {
        Swal.fire({
          title: 'closed!',
          text: 'Date From and Date To Required',
          icon: 'error',
          confirmButtonText: 'Ok'
        })

      });
    });
  </script>
@endpush
