<div wire:ignore.self class="modal modal-xl fade" id="purchaseOrderItemsModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ $formTitle }}</h5><button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span class="fas fa-times fs--1"></span></button>
      </div>
      <div class="modal-body">
        <div class="card mb-3">
          <div class="card-body">
            <div class="d-flex gap-3">
              <div class="col">
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Order Number</p>
                  <p>{{ @$purchaseOrder->order_number }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Order Date</p>
                  <p>{{ @date('Y-m-d', strtotime($purchaseOrder->date)) }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold text-nowrap">Note</p>
                  <p class="text-end">{{ @$purchaseOrder->note }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Contact Person</p>
                  <p>{{ @getVendorById($purchaseOrder->vendor_id)->contact_person }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Vendor Code</p>
                  <a href="#">{{ @getVendorById($purchaseOrder->vendor_id)->code }}</a>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Vendor Name</p>
                  <p>{{ @getVendorById($purchaseOrder->vendor_id)->name }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Vendor Phone</p>
                  <p>{{ @getVendorById($purchaseOrder->vendor_id)->phone }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold text-nowrap">Vendor Address</p>
                  <p class="text-end">{{ @getVendorById($purchaseOrder->vendor_id)->address }}</p>
                </div>
              </div>
              <div class="col">
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Submitted By</p>
                  <p>{{ @getUserById($purchaseOrder->user_id)->name }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Created Date</p>
                  <p>{{ @$purchaseOrder->created_at }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Approved By</p>
                  <p>{{ @getUserById($purchaseOrder->approved_by)->name }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Approved Date</p>
                  <p>{{ @$purchaseOrder->approved_date }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Discount</p>
                  <p>{{ numberFormat( floatval(@$purchaseOrder->discount) ) }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Tax</p>
                  <p>{{ numberFormat( floatval(@$purchaseOrder->tax) ) }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Other</p>
                  <p>{{ numberFormat( floatval(@$purchaseOrder->others) ) }}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="table-responsive scrollbar">
          <h5 class="text-center mb-3">Purchase Order Items</h5>
          <table class="table table-hover table-bordered fs--1 mb-0 ">
            <thead>
              <tr>
                <th class="sort px-3" scope="col">Product Code</th>
                <th class="sort px-3" scope="col">Product Name</th>
                <th class="sort text-center" scope="col">QTY</th>
                <th class="sort text-center" scope="col">Unit</th>
                <th class="sort text-end px-3" scope="col">Price</th>
                <th class="sort text-end px-3" scope="col">Last Price</th>
                <th class="sort text-end px-3" scope="col">Disc. Item</th>
                <th class="sort text-end px-3" scope="col">Total Price</th>
              </tr>
            </thead>
            <tbody class="list">
              @if(!empty($purchaseOrderItems))
              @forelse($purchaseOrderItems as $purchaseOrderItem)
              <tr>
                <td class="px-3 align-middle">{{ @getProductById($purchaseOrderItem->product_id)->code }}</td>
                <td class="px-3 align-middle">{{ @getProductById($purchaseOrderItem->product_id)->name }}</td>
                <td class="text-center">{{ @countFormat($purchaseOrderItem->qty) }}</td>
                <td class="text-center">{{ @countFormat(getProductById($purchaseOrderItem->product_id)->size) . ' ' . @getUnitOfMeasureById(@getProductById($purchaseOrderItem->product_id)->unit_of_measure_id)->name }}</td>
                <td class="px-3 text-end">{{ @numberFormat($purchaseOrderItem->price) }}</td>
                <td class="px-3 text-end">{{ @numberFormat(getProductById($purchaseOrderItem->product_id)->cogs) }}</td>
                <td class="px-3 text-end">{{ @numberFormat($purchaseOrderItem->discount_item) }}</td>
                <td class="px-3 text-end">{{ @numberFormat($purchaseOrderItem->total_price) }}</td>
              </tr>
              @empty
              <tr>
                <td colspan="8" class="text-center">Empty Data</td>
              </tr>
              @endforelse
              @endif
              <tr>
                <td colspan="7" class="text-end px-3">Sub Total</td>
                <td class="text-end px-3">{{ $subTotal }}</td>
              </tr>
              <tr>
                <td colspan="7" class="text-end px-3">Tax</td>
                <td class="text-end px-3">{{ @numberFormat($purchaseOrder->tax) }}</td>
              </tr>
              <tr>
                <td colspan="7" class="text-end px-3">Others</td>
                <td class="text-end px-3">{{ @numberFormat($purchaseOrder->others) }}</td>
              </tr>
              <tr>
                <td colspan="7" class="text-end px-3">Discount</td>
                <td class="text-end px-3">- {{ @numberFormat($purchaseOrder->discount) }}</td>
              </tr>
              <tr>
                <td colspan="7" class="text-end px-3">Grand Total</td>
                <td class="text-end px-3">{{ $grandTotal }}</td>
              </tr>
            </tbody>
          </table>
          @if(@$approvalPurchaseOrder)
          @if(!@$purchaseOrder->approved_by)
          <div class="my-2 text-end">
            <button class="btn btn-success" wire:click="approve('{{$purchaseOrder->id}}')">Approve</button>
          </div>
          @endif
          @endif
        </div>
      </div>
    </div>
  </div>
</div>


@push('scripts')
<script>
  let purchaseOrderItemsModal = new bootstrap.Modal(
    document.getElementById('purchaseOrderItemsModal'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('approved', () => {
    @this.emit('dataApproved')
  })
  Livewire.on('showPurchaseOrderItems', () => {
    purchaseOrderItemsModal.show();
  })
  Livewire.on('hidePurchaseOrderItems', () => {
    purchaseOrderItemsModal.hide();
  })
  document.getElementById('purchaseOrderItemsModal').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearItems')
  })

</script>
@endpush