<div wire:ignore.self class="modal modal-xl fade" id="purchaseInvoiceItemsModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ $formTitle }}</h5>
        <button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close">
          <span class="fas fa-times fs--1"></span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card mb-3">
          <div class="card-body">
            <div class="d-flex gap-3">
              <div class="col">
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Invoice Number</p>
                  <p>{{ @$purchaseInvoice->invoice_number }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Contact Person</p>
                  <p>{{ @getVendorById($purchaseInvoice->vendor_id)->contact_person }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Note</p>
                  <p>{{ @$purchaseInvoice->note }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Vendor Code</p>
                  <a href="#">
                    {{ @getVendorById($purchaseInvoice->vendor_id)->code }}</a>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Vendor Name</p>
                  <p>{{ @getVendorById($purchaseInvoice->vendor_id)->name }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Vendor Phone</p>
                  <p>{{ @getVendorById($purchaseInvoice->vendor_id)->phone }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold text-nowrap">Vendor Address</p>
                  <p class="text-end">{{ @getVendorById($purchaseInvoice->vendor_id)->address }}</p>
                </div>
              </div>
              <div class="col">
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Invoice Date</p>
                  <p>{{ @dateFormat($purchaseInvoice->date) }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Submitted By</p>
                  <p>{{ @getUserById($purchaseInvoice->user_id)->name }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Approved By</p>
                  <p>{{ @getUserById($purchaseInvoice->approved_by)->name }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Approved Date</p>
                  <p>{{ @dateFormat($purchaseInvoice->approved_date) }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Discount</p>
                  <p>{{ @numberFormat( floatval($purchaseInvoice->discount) ) }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Tax</p>
                  <p>{{ @numberFormat( floatval($purchaseInvoice->tax) ) }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Other</p>
                  <p>{{ @numberFormat( floatval($purchaseInvoice->others) ) }}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="table-responsive scrollbar">
          <h5 class="text-center mb-3">Purchase Invoice Items</h5>
          <table class="table table-hover table-bordered fs--1 mb-0 ">
            <thead>
              <tr>
                <th class="sort px-3" scope="col">Product Code</th>
                <th class="sort px-3" scope="col">Product Name</th>
                <th class="sort text-center" scope="col">QTY</th>
                <th class="sort text-center" scope="col">Unit</th>
                <th class="sort text-end px-3" scope="col">Price</th>
                <th class="sort text-end px-3" scope="col">Last Price</th>
                <th class="sort text-end px-3" scope="col">Disc. Item</th>
                <th class="sort text-end px-3" scope="col">Total</th>
              </tr>
            </thead>
            <tbody class="list">
              @if(!empty($this->purchaseInvoice->items))
              @forelse($this->purchaseInvoice->items as $purchaseInvoiceItem)
              <tr>
                <td class="px-3 align-middle">{{ @getProductById($purchaseInvoiceItem->product_id)->code }}</td>
                <td class="px-3 align-middle">{{ @getProductById($purchaseInvoiceItem->product_id)->name }}</td>
                <td class="px-3 text-center">{{ @numberFormat($purchaseInvoiceItem->qty) }}</td>
                <td class="px-3 text-center">{{  @numberFormat(getProductById($purchaseInvoiceItem->product_id)->size) . ' ' . @getUnitOfMeasureById(@getProductById($purchaseInvoiceItem->product_id)->unit_of_measure_id)->name }}</td>
                <td class="px-3 text-end">{{ @numberFormat($purchaseInvoiceItem->price) }}</td>
                <td class="px-3 text-end">{{ @numberFormat(getProductById($purchaseInvoiceItem->product_id)->cogs) }}</td>
                <td class="px-3 text-end">{{ @numberFormat($purchaseInvoiceItem->discount_item) }}</td>
                <td class="px-3 text-end">{{ @numberFormat(@$purchaseInvoiceItem->total_price) }}</td>
              </tr>
              @empty
              <tr>
                <td colspan="8" class="text-center">Empty Data</td>
              </tr>
              @endforelse
              @endif
              <tr>
                <td colspan="7" class="text-end px-3">SubTotal</td>
                <td class="text-end px-3">{{ numberFormat( @$subTotal ) }}</td>
              </tr>
              <tr>
                <td colspan="7" class="text-end px-3">Tax</td>
                <td class="text-end px-3">{{ numberFormat( @$purchaseInvoice->tax ) }}</td>
              </tr>
              <tr>
                <td colspan="7" class="text-end px-3">Others</td>
                <td class="text-end px-3">{{ numberFormat( @$purchaseInvoice->others ) }}</td>
              </tr>
              <tr>
                <td colspan="7" class="text-end px-3">Discount</td>
                <td class="text-end px-3">- {{ numberFormat( @$purchaseInvoice->discount ) }}</td>
              </tr>
              <tr>
                <td colspan="7" class="text-end px-3">Grand Total</td>
                <td class="text-end px-3">{{numberFormat( @$grandTotal ) }}</td>
              </tr>
            </tbody>
          </table>
          @if(@$approvalPurchaseInvoice)
          @if(@!$purchaseInvoice->approved_by)
          <div class="my-2 text-end">
            <button class="btn btn-success" wire:click="approve('{{$purchaseInvoice->id}}')">Approve</button>
          </div>
          @endif
          @endif
        </div>
      </div>
    </div>
  </div>
</div>


@push('scripts')
<script>
  let purchaseInvoiceItemsModal = new bootstrap.Modal(
    document.getElementById('purchaseInvoiceItemsModal'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showPurchaseInvoiceItems', () => {
    purchaseInvoiceItemsModal.show();
  })
  Livewire.on('hidePurchaseInvoiceItems', () => {
    purchaseInvoiceItemsModal.hide();
  })
  document.getElementById('purchaseInvoiceItemsModal').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearItems')
  })

</script>
@endpush