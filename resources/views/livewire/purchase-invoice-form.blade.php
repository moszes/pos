<div class="col-3">
  <div class="loading" wire:loading.class.remove="d-none"></div>
  <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#purchaseInvoiceFormModal">+</button>
  <div wire:ignore.self class="modal modal-xl fade" id="purchaseInvoiceFormModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5>
          <button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span class="fas fa-times fs--1"></span></button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <livewire:purchase-order-search />
            <livewire:purchase-invoice-header />
            <div class="d-flex flex-column mb-3">
              @if(empty($purchaseOrder))
              <livewire:product-search />
              @endif

              @if(!empty($items))
              <table class="table table-hover table-bordered table-responsive mt-3">
                <thead>
                  <tr>
                    <th class="px-3 text-center"></th>
                    <th class="px-3">Code</th>
                    <th class="px-3">Name</th>
                    <th class="px-3 text-center">Qty</th>
                    <th class="px-3 text-center">Unit</th>
                    <th class="px-3 text-end">Price</th>
                    <th class="px-3 text-end">Last Price</th>
                    <th class="px-3 text-end">Disc. Item</th>
                    <th class="px-3 text-end">Total</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse($items as $key => $item)
                  <tr>
                    <td class="px-3 text-center">
                      <a href="#" wire:click="removeItem({{$key}})"><i class="fas fa-trash"></i></a>
                    </td>
                    <td class="px-3">{{ @$item['itemCode'] }}</td>
                    <td class="px-3 ">{{ @$item['itemName'] }}</td>
                    <td class="px-3 text-center">{{ @$item['qty'] }}</td>
                    <td class="px-3 text-center">{{ @$item['unit'] }}</td>
                    <td class="px-3 text-end">{{ @$item['price'] }}</td>
                    <td class="px-3 text-end">{{ @$item['last_price'] }}</td>
                    <td class="px-3 text-end">{{ @$item['discount_item'] }}</td>
                    <td class="px-3 text-end">{{ @$item['total_price'] }}</td>
                  </tr>
                  @empty
                  <tr>
                    <td colspan="9" class="text-center">Empty Data</td>
                  </tr>
                  @endforelse
                  <tr>
                    <td colspan="8" class="text-end px-3">Sub Total</td>
                    <td class="text-end px-3">{{ @numberFormat($subTotal) }}</td>
                  </tr>
                  <tr>
                    <td colspan="8" class="text-end px-3">Tax</td>
                    <td class="text-end px-3">{{ @numberFormat($tax) }}</td>
                  </tr>
                  <tr>
                    <td colspan="8" class="text-end px-3">Others</td>
                    <td class="text-end px-3">{{ @numberFormat($others) }}</td>
                  </tr>
                  <tr>
                    <td colspan="8" class="px-3 text-end">Discount</td>
                    <td class="px-3 text-end">- {{ @numberFormat($discount) }}</td>
                  </tr>
                  <tr>
                    <td colspan="8" class="text-end">Grant Total</td>
                    <td class="px-3 text-end">{{ @numberFormat($grandTotal) }}</td>
                  </tr>
                </tbody>
              </table>
              @endif
            </div>
            <div class="float-end">
              <button type="button" wire:click="$emit('clearPurchaseInvoiceForm')" class="btn btn-light">Clear</button>
              <button wire:loading.attr="disabled" type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>
  let purchaseInvoiceFormModal = new bootstrap.Modal(
    document.getElementById('purchaseInvoiceFormModal'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showPurchaseInvoiceForm', () => {
    purchaseInvoiceFormModal.show();
  })
  Livewire.on('hidePurchaseInvoiceForm', () => {
    purchaseInvoiceFormModal.hide();
  })
  document.getElementById('purchaseInvoiceFormModal').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearPurchaseInvoiceForm');
  });
</script>
@endpush
