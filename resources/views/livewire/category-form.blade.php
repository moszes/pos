<div>
  <button
    class="btn btn-primary btn-sm"
    type="button" data-bs-toggle="modal"
    data-bs-target="#categoryForm"
  >
    +
  </button>
  <div
    wire:ignore.self class="modal fade"
    id="categoryForm"
    tabindex="-1"
    aria-hidden="true"
  >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5>
          <button
            class="btn"
            type="button"
            data-bs-dismiss="modal"
            aria-label="Close"
          >
            <span class="fas fa-times fs--1"></span>
          </button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div>
              @if (session()->has('message'))
                <div class="alert alert-danger">
                  {{ session('message') }}
                </div>
              @endif
            </div>
            <div class="mb-3">
              <label class="form-label" for="code">Code</label>
              <input
                wire:model.lazy="code"
                id="code"
                class="form-control"
                type="text"
              >
              @error('code')
                <span class="text-danger">{{ $message }}</span>
              @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="name">Name</label>
              <input
                wire:model.lazy="name"
                id="name"
                class="form-control"
                type="text"
              >
              @error('name')
                <span class="text-danger">{{ $message }}</span>
              @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="type">Type</label>
              <select
                wire:model.debounce.200ms="type"
                class="form-select"
                id="type"
                aria-label="Select Type"
              >
                <option selected>-- Choose --</option>
                <option value="product">Product</option>
                <option value="treatment">Treatment</option>
                <option value="package">Package</option>
                <option value="marketplace">Market Place</option>
                <option value="subscription">Subscription</option>
              </select>
              @error('type')
                <span class="text-danger">{{ $message }}</span>
              @enderror
            </div>
            <div class="float-end">
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>
  let categoryForm = new bootstrap.Modal(
    document.getElementById('categoryForm'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showCategoryForm', () => {
    categoryForm.show();
  })
  Livewire.on('hideCategoryForm', () => {
    categoryForm.hide();
  })
  document.getElementById('categoryForm')
    .addEventListener('hidden.bs.modal', () => {
      Livewire.emit('clearCategoryForm');
    });
</script>
@endpush