<div class="nav-item-wrapper">
  <a class="nav-link dropdown-indicator label-1" id="transaction-btn" href="#nv-transactions" role="button" data-bs-toggle="collapse" aria-expanded="false" aria-controls="nv-transactions">
    <div class="d-flex align-items-center">
      <span class="nav-link-icon">
        <span data-feather="dollar-sign"></span>
      </span>
      <span class="nav-link-text">Transaction Data</span>
    </div>
  </a>
  <div class="parent-wrapper label-1">
    <ul class="nav collapse parent" data-bs-parent="#navbarVerticalCollapse" id="nv-transactions">
      <li class="collapsed-nav-item-title d-none">Transactions</li>
      <li class="nav-item">
        <a class="nav-link dropdown-indicator" id="sales-transaction-btn" href="#nv-sales" data-bs-toggle="collapse" aria-expanded="false" aria-controls="nv-sales">
          <div class="d-flex align-items-center">
            <div class="dropdown-indicator-icon">
              <span class="fas fa-caret-right"></span>
            </div>
            <span class="nav-link-text">Sales</span>
          </div>
        </a>
        <!-- more inner pages-->
        <div class="parent-wrapper">
          <ul class="nav collapse parent" data-bs-parent="#forms" id="nv-sales">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('transaction.appointments') }}" aria-expanded="false">
                <div class="d-flex align-items-center"><span class="nav-link-text">Appointment</span>
                </div>
              </a>
              <!-- more inner pages-->
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('transaction.sales_invoices') }}" aria-expanded="false">
                <div class="d-flex align-items-center">
                  <span class="nav-link-text">Sales Invoices</span>
                </div>
              </a>
              <!-- more inner pages-->
            </li>
            <li class="nav-item">
              <a class="nav-link @if(Route::currentRouteName() == 'transaction.subscriptions') active @endif" href="{{ route('transaction.subscriptions') }}" aria-expanded="false">
                <div class="d-flex align-items-center">
                  <span class="nav-link-text">Subscriptions</span>
                </div>
              </a>
              <!-- more inner pages-->
            </li>
            <li class="nav-item">
              <a class="nav-link @if(Route::currentRouteName() == 'transaction.deposits') active @endif" href="{{ route('transaction.deposits') }}" aria-expanded="false">
                <div class="d-flex align-items-center">
                  <span class="nav-link-text">Deposit</span>
                </div>
              </a>
              <!-- more inner pages-->
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link dropdown-indicator" href="#nv-purchase" data-bs-toggle="collapse" aria-expanded="false" aria-controls="nv-purchase">
          <div class="d-flex align-items-center">
            <div class="dropdown-indicator-icon">
              <span class="fas fa-caret-right"></span>
            </div>
            <span class="nav-link-text">Purchase</span>
          </div>
        </a>
        <!-- more inner pages-->
        <div class="parent-wrapper">
          <ul class="nav collapse parent" data-bs-parent="#forms" id="nv-purchase">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('transaction.purchase_orders') }}" aria-expanded="false">
                <div class="d-flex align-items-center"><span class="nav-link-text">Purchase Order</span>
                </div>
              </a>
              <!-- more inner pages-->
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('transaction.purchase_invoices') }}" aria-expanded="false">
                <div class="d-flex align-items-center">
                  <span class="nav-link-text">Purchase Invoice</span>
                </div>
              </a>
              <!-- more inner pages-->
            </li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</div>

@push('scripts')
<script>
  Livewire.on('openTransactionModule', function(){
    document.getElementById('transaction-btn').click();
  })
  Livewire.on('openSalesTransactionMenu', function(){
    document.getElementById('sales-transaction-btn').click();
  })
</script>
@endpush