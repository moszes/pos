<div>
	<h4 class="text-center mt-5 mb-3">Material Usage</h4>
	<div class="d-flex flex-column">
		<div class="col">
			<label class="form-label" for="productSearch">Product</label>
			<input wire:model.debounce.200ms="productSearch" id="productSearch" class="form-control" type="text" placeholder="Search Product...">
			@error('productSearch') <span class="text-danger">{{ $message }}</span> @enderror
		</div>
		@if($productList)
			<ul class="list-group">
				@forelse($products as $product)
					<li wire:click="selectProduct({{$product->id}})" class="list-group-item pointer">{{ $product->code}}, {{ $product->name}}</li>
				@empty
					<li class="list-group-item">Empty Product</li>
				@endforelse
			</ul>
		@endif
		
		@if($itemForm)
			<div class="d-flex gap-3 mt-3">
				<div class="mb-3 col-2">
					<label class="form-label" for="itemCode">Code</label>
					<input wire:model.debounce.200ms="itemCode" id="itemCode" class="form-control" type="text" disabled>
					@error('itemCode') <span class="text-danger">{{ $message }}</span> @enderror
				</div>
				<div class="mb-3 col-3">
					<label class="form-label" for="itemName">Name</label>
					<input wire:model.debounce.200ms="itemName" id="itemName" class="form-control" type="text" disabled>
					@error('itemName') <span class="text-danger">{{ $message }}</span> @enderror
				</div>
				<div class="mb-3 col-2">
					<label class="form-label" for="itemPrice">Price</label>
					<input wire:model.debounce.200ms="itemPrice" id="itemPrice" class="form-control" type="number" onfocus="clearValue(this)" onblur="zeroValue(this)">
					@error('itemPrice') <span class="text-danger">{{ $message }}</span> @enderror
				</div>
				<div class="mb-3 col-1">
					<label class="form-label" for="itemDose">Dose</label>
					<input wire:model.debounce.200ms="itemDose" id="itemDose" class="form-control" type="number" onfocus="clearValue(this)" onblur="zeroValue(this)">
					@error('itemDose') <span class="text-danger">{{ $message }}</span> @enderror
				</div>
				<div class="mb-3 col-2">
					<label class="form-label" for="itemCost">Item Cost</label>
					<input wire:model.debounce.200ms="itemCost" id="itemCost" class="form-control" type="number" onfocus="clearValue(this)" onblur="zeroValue(this)">
					@error('itemCost') <span class="text-danger">{{ $message }}</span> @enderror
				</div>
				<div class="pt-4 col-1">
					<a href="#" wire:click="addItem" class="btn btn-danger px-3">+</a>
				</div>
			</div>
		@endif
		
		<table class="table table-hover table-bordered table-responsive mt-3">
			<thead>
			<tr>
				<th class="px-3 text-center"></th>
				<th class="px-3 text-center">Code</th>
				<th class="px-3">Name</th>
				<th class="px-3 text-center">Unit</th>
				<th class="px-3 text-end">Price</th>
				<th class="px-3 text-center">Dose</th>
				<th class="px-3 text-end">Total Cost</th>
			</tr>
			</thead>
			<tbody>
			@forelse($items as $key => $item)
				<tr>
					<td class="px-3 text-center"><a href="#" wire:click="removeItem({{$key}})"><i class="fas fa-trash"></i></a></td>
					<td class="px-3 text-center">{{ @$item['code'] }}</td>
					<td class="px-3">{{ @$item['name'] }}</td>
					<td class="px-3 text-center">{{ @getUnitOfMeasureById($item['unit_of_measure_id'])->name }}</td>
					<td class="px-3 text-end">{{ @numberFormat($item['itemPrice']) }}</td>
					<td class="px-3 text-center">{{ @numberFormat($item['itemDose']) }}</td>
					<td class="px-3 text-end">{{ @numberFormat($item['itemCost']) }}</td>
				</tr>
			@empty
				<tr>
					<td colspan="7" class="text-center">Empty Data</td>
				</tr>
			@endforelse
			<tr>
				<td colspan="6" class="text-end">Total Cost</td>
				<td class="px-3 text-end">{{ numberFormat($totalCost) }}</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>
