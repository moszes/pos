<div class="table-responsive px-1 scrollbar">
	<h5 class="text-center mb-3">Package Items</h5>
	<table class="table table-hover fs--1 mb-0 ">
		<thead>
		<tr>
			<th class="sort ps-2" scope="col">Services</th>
			<th class="sort" scope="col">Code</th>
			<th class="sort" scope="col">Name</th>
			<th class="sort text-center" scope="col">Qty</th>
			<th class="sort text-end" scope="col">Normal Price</th>
			<th class="sort text-end" scope="col">Package Item Price</th>
			<th class="sort pe-2 text-end" scope="col">Total</th>
		</tr>
		</thead>
		<tbody class="list">
		@if(!empty($package->items))
			@forelse($package->items as $item)
				@php($packageItem = getDataByModule($item['module'], $item['module_id']))
				<tr>
					<td class="ps-2">{{ ucwords(@$item->module) }}</td>
					<td>{{ @$item->code }}</td>
					<td>{{ @$item->name }}</td>
					<td class="text-center">{{ @$item->qty }}</td>
					<td class="text-end">{{ numberFormat(@$packageItem->module == 'product' ? $packageItem->normal_price : $packageItem->price) }}</td>
					<td class="text-end">{{ numberFormat(@$item->package_item_price) }}</td>
					<td class="text-end pe-2">{{ numberFormat(@$item->total_package_price) }}</td>
				</tr>
			@empty
				<tr>
					<td colspan="7" class="text-center">Empty Data</td>
				</tr>
			@endforelse
		@endif
		</tbody>
	</table>
</div>