<div class="d-flex flex-column gap-3">
	<div class="loading d-none" wire:loading.class.remove="d-none" ></div>
	<div class="d-flex gap-3">
		<div class="col">
			<label class="form-label" for="subscriptionPlanSearch">Subscription Plan</label>
			<input
				wire:model.debounce.100ms="subscriptionPlanSearch"
				wire:click="clearSubscriptionPlanSearch"
				id="subscriptionPlanSearch"
				class="form-control"
				type="text"
			>
			@error('subscriptionPlanSearch') <div class="text-danger">{{ $message }}</div> @enderror
			@if($subscriptionPlanList)
				<ul class="list-group">
					@forelse($subscriptionPlans as $subscriptionPla)
						<li wire:click="selectSubscriptionPlan({{@$subscriptionPla->id}})" class="list-group-item pointer">
							{{ @$subscriptionPla->code }} {{ @$subscriptionPla->name }}
						</li>
					@empty
						<li class="list-group-item">Empty Subscription</li>
					@endforelse
				</ul>
			@endif
		</div>
	</div>
</div>

@push('scripts')
	<script>
		Livewire.on('subscriptionPlanRequired', () => {
			Swal.fire({
				title: 'Required!',
				text: 'Choose Subscription',
				icon: 'error',
				confirmButtonText: 'Ok'
			})
		})

		Livewire.on('subscriptionPlanExists', () => {
			Swal.fire({
				title: 'Subscription Exists',
				text: 'Please Check Subscription Data',
				icon: 'error',
				confirmButtonText: 'Ok'
			})
		})
	</script>
@endpush