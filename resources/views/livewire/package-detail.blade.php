<div wire:ignore.self class="modal modal-xl fade" id="packageDetail" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ $formTitle }}</h5>
        <button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close">
          <span class="fas fa-times fs--1"></span>
        </button>
      </div>
      <div class="modal-body d-flex flex-column gap-4">
        @if($package)
          <livewire:package-detail-info :package="$package" />
          <livewire:package-detail-item :package="$package" />
          <livewire:package-detail-price-history :package="$package" />
        @endif
      </div>
    </div>
  </div>
</div>

@push('scripts')
  <script>
    let packageDetail = new bootstrap.Modal(
      document.getElementById('packageDetail'),
      {
        backdrop: 'static',
      }
    )
    Livewire.on('showPackageDetail', () => {
      packageDetail.show();
    })
    Livewire.on('hidePackageDetail', () => {
      packageDetail.hide();
    })
    document.getElementById('packageDetail').addEventListener('hidden.bs.modal', () => {
      Livewire.emit('clearPackageDetail')
    })
  </script>
@endpush