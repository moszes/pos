<div class="col-12">
	<div class="row align-items-end justify-content-between pb-5">
		<div class="col-3">
			<h3>{{ $titlePage }}</h3>
		</div>
	</div>
	<div class="card mb-5 mt-3">
		<div class="card-body">
			<div class="card-body d-flex gap-3 justify-content-start">
				<div class="d-flex gap-3 col">
					<label class="w-25" for="dateFrom">Date Form</label>
					<input type="date" id="dateFrom" class="form-control w-75" wire:model="dateFrom" placeholder="Date">
				</div>
				<div class="d-flex gap-3 col">
					<label class="w-25" for="dateTo">Date To</label>
					<input type="date" id="dateTo" class="form-control w-75" wire:model="dateTo" placeholder="Date">
				</div>
			</div>
		</div>
	</div>
	<div class="table-responsive scrollbar">
		<table class="table table-hover fs--1 mb-0 ">
			<thead>
				<tr>
					<th class="sort px-3 text-start align-middle" scope="col" rowspan="2">Employee</th>
					<th class="sort px-3 text-center align-middle" scope="col" rowspan="2">Role</th>
					<th class="sort px-3 text-end align-middle" scope="col" rowspan="2">Total</th>
				</tr>
			</thead>
			<tbody class="list">
			@forelse($employees as $employee)
				<tr>
					<td class="px-3">
						<a href="{{ route('report.commissionReportDetail', $employee->id) }}">{{ $employee->name }}</a>
					</td>
					<td class="px-3 text-center">{{ $employee->getRoleNames()[0] ?? '' }}</td>
					<td class="px-3 text-end">{{ numberFormat(getCommissionByDate($employee->id, $dateFrom, $dateTo)) }}</td>
				</tr>
			@empty
				<tr>
					<td colspan="15" class="text-center">Empty Data</td>
				</tr>
			@endforelse
			</tbody>
		</table>
		<div class="py-4">
			<button class="btn btn-success float-end" wire:click="downloadSalesInvoiceReport('{{$employees}}')">Download Report</button>
		</div>
	</div>
</div>