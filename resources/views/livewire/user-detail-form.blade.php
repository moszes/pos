<div class="mb-4">
  <form wire:submit.prevent="updateProfile">
    <div class="mb-6">
      <div wire:loading wire:target="updateProfile" class="spinner-border text-secondary mb-2" role="status">
        <span class="visually-hidden">Loading...</span>
      </div>
      <div class="row g-3">
        <div class="col-12 col-sm-6">
          <div class="form-icon-container">
            <div class="form-floating">
              <input wire:model.lazy="username" class="form-control form-icon-input" id="username" type="text" placeholder="username">
              <label class="text-700 form-icon-label" for="username">Username</label>
            </div>
            <span class="fa-solid fa-user-circle text-900 fs--1 form-icon"></span>
          </div>
        </div>
        <div class="col-12 col-sm-6">
          <div class="form-icon-container">
            <div class="form-floating">
              <input wire:model.lazy="instagram" class="form-control form-icon-input" id="instagram" type="text" placeholder="instagram">
              <label class="text-700 form-icon-label" for="instagram">Instagram</label>
            </div>
            <span class="fa-solid fab fa-instagram text-900 fs--1 form-icon"></span>
          </div>
        </div>
	      
	      <!-- For Date of Birth -->
	      <div class="col-12 col-sm-6">
		      <div class="form-icon-container">
			      <div class="form-floating">
				      <input wire:model.lazy="dateOfBirth" class="form-control form-icon-input" id="dateOfBirth" type="date"
				             placeholder="Date of Birth">
				      <label class="text-700 form-icon-label" for="dateOfBirth">Date of Birth</label>
			      </div>
			      <span class="fa-solid fa-calendar text-900 fs--1 form-icon"></span>
		      </div>
	      </div>
	      
	      <!-- For Place of Birth -->
	      <div class="col-12 col-sm-6">
		      <div class="form-icon-container">
			      <div class="form-floating">
				      <input wire:model.lazy="placeOfBirth" class="form-control form-icon-input" id="placeOfBirth" type="text"
				             placeholder="Place of Birth">
				      <label class="text-700 form-icon-label" for="placeOfBirth">Place of Birth</label>
			      </div>
			      <span class="fa-solid fa-map-marker-alt text-900 fs--1 form-icon"></span>
		      </div>
	      </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-icon-container">
            <div class="form-floating">
              <input wire:model.lazy="firstName" class="form-control form-icon-input" id="firstName" type="text" placeholder="First Name">
              <label class="text-700 form-icon-label" for="firstName">First Name</label>
            </div>
            <span class="fa-solid fa-user text-900 fs--1 form-icon"></span>
          </div>
        </div>
        <div class="col-12 col-sm-6">
          <div class="form-icon-container">
            <div class="form-floating">
              <input wire:model.lazy="lastName" class="form-control form-icon-input" id="lastName" type="text" placeholder="Last Name">
              <label class="text-700 form-icon-label" for="lastName">Last Name</label>
            </div>
            <span class="fa-solid fa-user text-900 fs--1 form-icon"></span>
          </div>
        </div>
        <div class="col-12 col-sm-6">
          <div class="form-icon-container">
            <div class="form-floating">
              <input wire:model.lazy="email" class="form-control form-icon-input" id="emailSocial" type="email" placeholder="Email">
              <label class="text-700 form-icon-label" for="emailSocial">ENTER YOUR EMAIL</label>
            </div>
            <span class="fa-solid fa-envelope text-900 fs--1 form-icon"></span>
          </div>
        </div>
        <div class="col-12 col-sm-6">
          <div class="form-icon-container">
            <div class="form-floating">
              <input wire:model.lazy="phone" class="form-control form-icon-input" id="phone" type="tel" placeholder="Phone">
              <label class="text-700 form-icon-label" for="phone">ENTER YOUR PHONE</label>
            </div>
            <span class="fa-solid fa-phone text-900 fs--1 form-icon"></span>
          </div>
        </div>
        <div class="col-12 col-sm-6">
          <div class="form-icon-container">
            <div class="form-floating">
              <textarea wire:model.lazy="address" class="form-control form-icon-input" id="address" style="height: 115px;" placeholder="address"></textarea>
              <label class="text-700 form-icon-label" for="address">Address</label>
            </div>
            <span class="fa-solid fa-circle-info text-900 fs--1 form-icon"></span>
          </div>
        </div>
      </div>
    </div>
    <div class="row g-3 mb-6">
      @hasrole('godadmin')
      <div class="col-12 col-sm-6">
        <h4 class="mb-4">Company Info</h4>
        <div class="form-icon-container mb-3">
          <div class="form-floating">
            <input wire:model.lazt="companyName" class="form-control form-icon-input" id="companyName" type="text" placeholder="Company Name" readonly>
            <label class="text-700 form-icon-label" for="companyName">COMPANY NAME</label>
          </div>
          <span class="fa-solid fa-building text-900 fs--1 form-icon"></span>
        </div>
        <div class="form-icon-container">
          <div class="form-floating">
            <input  wire:model.lazt="companyWebsite" class="form-control form-icon-input" id="website" type="text" placeholder="Website" readonly>
            <label class="text-700 form-icon-label" for="website">Website</label>
          </div>
          <span class="fa-solid fa-globe text-900 fs--1 form-icon"></span>
        </div>
      </div>
      @endhasrole
      <div class="col-12 col-sm-6">
        <h4 class="mb-4">Change Password</h4>
        <div class="form-icon-container mb-3">
          <div class="form-floating">
            <input wire:model.lazy="password" class="form-control form-icon-input" id="oldPassword" type="password" placeholder="Old password"><label class="text-700 form-icon-label" for="oldPassword">Old Password</label>
          </div>
          <span class="fa-solid fa-lock text-900 fs--1 form-icon"></span>
        </div>
        <div class="form-icon-container mb-3">
          <div class="form-floating">
            <input wire:model.lazy="newPassword" class="form-control form-icon-input" id="newPassword" type="password" placeholder="New password"><label class="text-700 form-icon-label" for="newPassword">New Password</label>
          </div>
          <span class="fa-solid fa-key text-900 fs--1 form-icon"></span>
        </div>
        <div class="form-icon-container">
          <div class="form-floating">
            <input wire:model.lazy="newPasswordConfirmation" class="form-control form-icon-input" id="newPassword2" type="password" placeholder="Confirm New password">
            <label class="text-700 form-icon-label" for="newPassword2">Confirm New Password</label>
          </div>
          <span class="fa-solid fa-key text-900 fs--1 form-icon"></span>
        </div>
      </div>
    </div>
    <div class="text-end mb-6">
      <div>
        <button class="btn btn-phoenix-secondary me-2">Cancel Changes</button>
        <button class="btn btn-phoenix-primary">Save Information</button>
      </div>
    </div>
  </form>
</div>