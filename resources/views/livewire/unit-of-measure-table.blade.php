<div class="d-flex flex-column gap-4">
  <div class="row">
    <div class="col">
      <h4>{{ $titlePage }}</h4>
    </div>
    <div class="col-3 d-flex gap-2">
      <input wire:model.debounce.200ms="search"
             class="form-control search-input search form-control-sm"
             type="search" placeholder="Search"
             aria-label="Search"
      >
      <livewire:unit-of-measure-form/>
    </div>
  </div>
  <table class="table table-hover fs--1 mb-0 ">
    <thead>
    <tr>
      <th class="sort px-3" scope="col">Code</th>
      <th class="sort px-3" scope="col">Name</th>
      <th class="sort text-end pe-2" scope="col">Action</th>
    </tr>
    </thead>
    <tbody class="list">
    @forelse($unit_of_measures as $key => $unit_of_measure)
      <tr>
        <td class="px-3">{{ $unit_of_measure->code }}</td>
        <td class="px-3">{{ $unit_of_measure->name }}</td>
        <td class="text-end pe-2">
          <a class="me-2 pointer" wire:click="$emit('editUnitOfMeasure', '{{$unit_of_measure->id}}')"><i
              class="fa fa-pencil"></i></a>
          <a class="text-danger pointer" wire:click="$emit('deleteUnitOfMeasure', '{{$unit_of_measure->id}}')">
            <i class="fa fa-remove"></i>
          </a>
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="3" class="text-center">Empty Data</td>
      </tr>
    @endforelse
    </tbody>
  </table>
  {{ $unit_of_measures->links() }}
</div>
 
@push('scripts')
   <script>
      window.addEventListener("DOMContentLoaded", function () {
      
        Livewire.emit("openMasterDataModule");
        Livewire.emit("openBaseDataMenu")
        
        Livewire.on('deleteUnitOfMeasure', (id) => {
          Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.isConfirmed) {
              Livewire.emit('destroy', id)
            }
          })
        })
      });
   </script>
@endpush