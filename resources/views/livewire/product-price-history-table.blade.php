
<div class="d-flex flex-column gap-3 mt-5 justify-content-center">
  <div class="d-flex justify-content-center">
    <h5>Product Price History</h5>
  </div>
	<div class="col-12">
		<div class="table-responsive scrollbar">
			<table class="table table-hover fs--1 mb-0 ">
				<thead>
					<tr>
						<th rowspan="2" class="align-top sort px-3" scope="col">Date</th>
						<th rowspan="2" class="align-top sort px-3" scope="col">User</th>
						<th rowspan="2" class="align-top sort text-center" scope="col">Action</th>
						<th rowspan="2" class="align-top sort px-3" scope="col">Code</th>
						<th rowspan="2" class="align-top sort px-3" scope="col">Name</th>
						<th rowspan="2" class="align-top sort px-3 text-center" scope="col">Size</th>
						<th rowspan="2" class="align-top sort px-3 text-center" scope="col">Unit</th>
						<th colspan="2" class="sort px-3 text-center" scope="col">Price</th>
					</tr>
					<tr>
						<th  class="sort px-3 text-end" scope="col">Normal</th>
						<th  class="sort px-3 text-end" scope="col">Agent</th>
					</tr>
				</thead>
				<tbody class="list">
				@forelse($priceHistories as $key => $priceHistory)
					@php($price = json_decode($priceHistory->properties))
					@php($unit = json_decode(getUnitOfMeasureById($price->attributes?->unit_of_measure_id))->name)
					<tr>
						<td class="px-3 text-nowrap">{{ date('Y-m-d', strtotime($priceHistory->created_at)) }}</td>
						<td class="px-3">{{ getUserById($priceHistory->causer_id)?->name }}</td>
						<td class="px-3 text-center">{{ $priceHistory->description }}</td>
						<td class="px-3">{{ $price->attributes?->code }}</td>
						<td class="px-3">{{ $price->attributes?->name }}</td>
						<td class="px-3 text-center">{{ $price->attributes?->size }}</td>
						<td class="px-3 text-center">{{ $unit }}</td>
						<td class="px-3 text-end">{{ isset($price->attributes->normal_price) ? numberFormat($price->attributes?->normal_price) : numberFormat($price->attributes?->customer_price) }}</td>
						<td class="px-3 text-end">{{ isset($price->attributes->agent_price) ? numberFormat($price->attributes?->agent_price) : 0 }}</td>
					</tr>
				@empty
					<tr>
						<td colspan="5" class="text-center">Empty Data</td>
					</tr>
				@endforelse
				</tbody>
			</table>
		</div>
		<div class="loading d-none" wire:loading.class.remove="d-none"></div>
	</div>
</div>