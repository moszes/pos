<div>
  <button class="btn btn-primary btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#turnOverForm">+</button>
  <div wire:ignore.self class="modal fade" id="turnOverForm" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5>
          <button class="btn" type="button" data-bs-dismiss="modal" aria-label="Close">
            <span class="fas fa-times fs--1"></span>
          </button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div>
              @if (session()->has('message'))
                <div class="alert alert-danger">
                  {{ session('message') }}
                </div>
              @endif
            </div>
            <div class="mb-3">
              <label class="form-label" for="month">Name</label>
              <input wire:model.lazy="month" id="month" class="form-control" type="month" pattern="\d{4}-\d{2}">
              @error('month') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="code">Amount</label>
              <input wire:model.lazy="amount" id="amount" class="form-control" type="number">
              @error('amount') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="float-end">
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
  <script>
    let turnOverForm = new bootstrap.Modal(
      document.getElementById('turnOverForm'),
      {
        backdrop: 'static',
      }
    )
    Livewire.on('showTurnOverForm', () => {
      turnOverForm.show();
    })
    Livewire.on('hideTurnOverForm', () => {
      turnOverForm.hide();
    })
    document.getElementById('turnOverForm').addEventListener('hidden.bs.modal', () => {
      Livewire.emit('clearTurnOverForm');
    });
  </script>
@endpush