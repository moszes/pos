<div class="bg-holder rounded-top" style="background-image: linear-gradient(0deg, #000000 -3%, rgba(0, 0, 0, 0) 83%), url('{{ $coverImageUrl }}')">
	<input class="d-none" wire:model="coverImage" name="coverImage" id="coverImage" type="file">
	<label class="cover-image-file-input" for="coverImage"></label>
	<div class="hover-actions end-0 bottom-0 pe-1 pb-2 text-white">
		<span class="fa-solid fa-camera me-2"></span>
	</div>
	<div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>