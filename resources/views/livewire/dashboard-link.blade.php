<li class="nav-item">
  <!-- parent pages-->
  <div class="nav-item-wrapper">
    <a class="nav-link dropdown-indicator label-1" href="#nv-home" role="button" data-bs-toggle="collapse" aria-expanded="true" aria-controls="nv-home">
      <div class="d-flex align-items-center">
        <span class="nav-link-icon">
          <span data-feather="pie-chart"></span>
        </span>
        <span class="nav-link-text">Home</span>
      </div>
    </a>
    <div class="parent-wrapper label-1">
      <ul class="nav collapse parent show" data-bs-parent="#navbarVerticalCollapse" id="nv-home">
        <li class="nav-item">
          <a class="nav-link @if(Route::currentRouteName() == 'dashboard') active @endif" href="/" aria-expanded="false">
            <div class="d-flex align-items-center">
              <span class="nav-link-text">Dashboard</span>
            </div>
          </a>
          <!-- more inner pages-->
        </li>
        <li class="nav-item">
          <a class="nav-link @if(Route::currentRouteName() == 'product') active @endif" href="{{ route('master.products') }}" aria-expanded="false">
            <div class="d-flex align-items-center">
              <span class="nav-link-text">Product</span>
            </div>
          </a>
          <!-- more inner pages-->
        </li>
        <li class="nav-item">
          <a class="nav-link @if(Route::currentRouteName() == 'stock') active @endif" href="{{ route('transaction.stocks') }}" aria-expanded="false">
            <div class="d-flex align-items-center">
              <span class="nav-link-text">Stock</span>
            </div>
          </a>
          <!-- more inner pages-->
        </li>
        @if( auth()->user()?->can('see sales invoice link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales') )
        <li class="nav-item">
          <a class="nav-link @if(Route::currentRouteName() == 'transaction.sales_invoices') active @endif" href="{{ route('transaction.sales_invoices') }}" aria-expanded="false">
            <div class="d-flex align-items-center">
              <span class="nav-link-text">Sales Invoice</span>
            </div>
          </a>
          <!-- more inner pages-->
        </li>
        @endif
        @if($treatmentModule)
          @if( auth()->user()?->can('see appointment link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales')  )
          <li class="nav-item">
            <a class="nav-link @if(Route::currentRouteName() == 'transaction.appointments') active @endif" href="{{ route('transaction.appointments') }}" aria-expanded="false">
              <div class="d-flex align-items-center">
                <span class="nav-link-text">Appointment</span>
              </div>
            </a>
            <!-- more inner pages-->
          </li>
          @endif
        @endif
        @if($subscriptionModule)
          @if( auth()->user()?->can('see subscriptions link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales')  )
          <li class="nav-item">
            <a class="nav-link @if(Route::currentRouteName() == 'transaction.subscriptions') active @endif" href="{{ route('transaction.subscriptions') }}" aria-expanded="false">
              <div class="d-flex align-items-center">
                <span class="nav-link-text">Subscription</span>
              </div>
            </a>
            <!-- more inner pages-->
          </li>
          @endif
        @endif
        @if( auth()->user()?->can('see employees link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin')  )
        <li class="nav-item">
          <a class="nav-link @if(Route::currentRouteName() == 'master.employees' || Route::currentRouteName() == 'master.employee_details') active @endif" href="{{ route('master.employees') }}" aria-expanded="false">
            <div class="d-flex align-items-center">
              <span class="nav-link-text">Employee</span>
            </div>
          </a>
          <!-- more inner pages-->
        </li>
        @endif
        @if( auth()->user()?->can('see customer link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales')  )
        <li class="nav-item">
          <a class="nav-link @if(Route::currentRouteName() == 'master.customers' || Route::currentRouteName() == 'customer.data_redemption') active @endif" href="{{ route('master.customers') }}" aria-expanded="false">
            <div class="d-flex align-items-center">
              <span class="nav-link-text">Customer</span>
            </div>
          </a>
          <!-- more inner pages-->
        </li>
        @endif
        @if( auth()->user()?->can('see today sales report link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
          <li class="nav-item">
            <a class="nav-link @if(Route::currentRouteName() == 'report.today_sales_report') active @endif" href="{{ route('report.today_sales_report') }}" aria-expanded="false">
              <div class="d-flex align-items-center">
                <span class="nav-link-text">Today Sales</span>
              </div>
            </a>
            <!-- more inner pages-->
          </li>
        @endif
      </ul>
    </div>
  </div>
</li>