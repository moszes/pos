<div wire:ignore.self class="modal modal-xl fade" id="vendorDetails" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ $formTitle }}</h5><button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span class="fas fa-times fs--1"></span></button>
      </div>
      <div class="modal-body">
        <div class="card mb-3">
          <div class="card-body">
            <div>
              <div class="d-flex align-items-center mb-1 gap-3">
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Code</p>
                  <a href="#">{{ @$vendor->code }}</a>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Name</p>
                  <p>{{ @$vendor->name }}</p>
                </div>
              </div>
              <div class="d-flex align-items-center mb-1 gap-3">
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Phone</p>
                  <p>{{ @$vendor->phone }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Contact Person</p>
                  <p>{{ @$vendor->contact_person }}</p>
                </div>
              </div>
              <div class="d-flex align-items-center mb-1 gap-3">
                <div class="col d-flex flex-column">
                  <p class="fw-bold">Address</p>
                  <p>{{ @$vendor->address }}</p>
                </div>
              </div>
            </div>
            <div>
              <h4 class="text-center mb-5">Taxes</h4>
              <div class="d-flex align-items-center mb-1 gap-3">
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">NPWP Name</p>
                  <a href="#">{{ @$vendor->tax->npwp_name }}</a>
                </div>
              </div>
              <div class="d-flex align-items-center mb-1 gap-3">
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">NPWP</p>
                  <p>{{ @$vendor->tax->npwp }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">NPWP Name</p>
                  <p>{{ @$vendor->tax->npwp_name }}</p>
                </div>
              </div>
              <div class="d-flex align-items-center mb-1 gap-3">
                <div class="col d-flex flex-column">
                  <p class="fw-bold">Address</p>
                  <p>{{ @$vendor->tax->tax_address }}</p>
                </div>
              </div>
            </div>
            <div>
              <h4 class="text-center mb-5">Payment</h4>
              <div class="d-flex align-items-center mb-1 gap-3">
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Period</p>
                  @if($vendor)
                  <p>{{ date('Y-m-d' , strtotime(@$vendor->payment->period)) }}</p>
                  @endif
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Bank</p>
                  <p>{{ @$vendor->payment->bank }}</p>
                </div>
              </div>
              <div class="d-flex align-items-center mb-1 gap-3">
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Payment Name</p>
                  <p>{{ @$vendor->payment->payment_name }}</p>
                </div>
                <div class="col d-flex justify-content-between">
                  <p class="fw-bold">Payment Number</p>
                  <p>{{ @$vendor->payment->payment_number }}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@push('scripts')
<script>
  let vendorDetails = new bootstrap.Modal(
    document.getElementById('vendorDetails'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showVendorDetails', () => {
    vendorDetails.show();
  })
  Livewire.on('hideVendorDetails', () => {
    vendorDetails.hide();
  })

  document.getElementById('vendorDetails').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearVendorDetail')
  })
</script>
@endpush