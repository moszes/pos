<div class="d-flex flex-column gap-4">
  <div class="d-flex flex-column">
    <div class="row">
      <div class="col">
        <h4>{{ $titlePage }}</h4>
      </div>
      <div class="col-4">
        <div class="d-flex gap-2">
          <input wire:model.debounce.200ms="search"
            class="form-control search-input search form-control-sm"
            type="search"
            placeholder="Search"
            aria-label="Search"
          >
          <livewire:market-place-form />
        </div>
      </div>
    </div>
  </div>
 <livewire:market-place-detail />
 <table class="table table-hover fs--1 mb-0 ">
   <thead>
   <tr>
     <th class="sort ps-2" scope="col">Item</th>
     <th class="sort text-end" scope="col">Price</th>
     <th class="sort text-end pe-2" scope="col">Action</th>
   </tr>
   </thead>
   <tbody class="list">
   @forelse($marketplaces as $key => $marketplace)
     <tr>
       <td wire:click="$emit('showMarketPlaceDetail', '{{$marketplace->id}}')" class="ps-2 pointer">
         <livewire:item-detail
           :moduleName="$moduleName"
           :priceLabel="$priceLabel"
           :moduleLabel="$moduleLabel"
           :item="$marketplace"
           :key="$marketplace->id"
         />
       </td>
       <td wire:click="$emit('showMarketPlaceDetail', '{{$marketplace->id}}')"
           class="px-3 text-end pointer">{{ numberFormat($marketplace->price) }}</td>
       <td class="text-end pe-2">
         <a
           class="me-2 pointer"
           wire:click="$emit('marketPlaceFee', '{{$marketplace->id}}')"
           data-toogle="tooltip"
           title="MarketPlace Fee"
         >
           <i class="far fa-money-bill-alt" aria-hidden="true"></i>
         </a>
         <a
           class="me-2 pointer" wire:click="$emit('marketPlaceMaterialUsage', '{{$marketplace->id}}')"
           data-toogle="tooltip"
           title="MarketPlace Material Usage"
         >
           <i class="far fa-file-archive"></i>
         </a>
         <a
           class="me-2 pointer"
           wire:click="$emit('editMarketPlace', '{{$marketplace->id}}')"
           data-toogle="tooltip"
           title="Edit"
         >
           <i class="fa fa-pencil"></i>
         </a>
         <a
           class="text-danger pointer"
           wire:click="$emit('deleteMarketPlace', '{{$marketplace->id}}')"
           data-toogle="tooltip"
           title="Delete"
         >
           <i class="fa fa-remove"></i>
         </a>
       </td>
     </tr>
   @empty
     <tr>
       <td colspan="6" class="text-center">Empty Data</td>
     </tr>
   @endforelse
   </tbody>
 </table>
 {{ $marketplaces->links() }}
</div>



@push('scripts')
  <script>
    
    window.addEventListener("DOMContentLoaded", function () {
      Livewire.emit("openMasterDataModule");
      Livewire.emit("openServicesMenu")
    });
    
    Livewire.on('deleteMarketPlace', (id) => {
      Swal.fire({
        title: "Are you sure?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!"
      }).then((result) => {
        if (result.isConfirmed) {
          Livewire.emit('destroy', id);
        }
      });
    })
  </script>
@endpush