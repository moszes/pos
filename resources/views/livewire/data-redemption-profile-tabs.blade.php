<div class="card">
	<div class="card-body">
		<div class="d-flex flex-column gap-4 p-0">
			<ul class="nav nav-underline" id="myTab" role="tablist">
				<li class="nav-item" role="presentation">
					<a class="nav-link active" id="redemption-data" data-bs-toggle="tab" href="#tab-customer-profile" role="tab" aria-controls="tab-customer-profile" aria-selected="true">
						Customer Profile
					</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link" id="redemption-data" data-bs-toggle="tab" href="#tab-redemption-data" role="tab" aria-controls="tab-redemption-data" aria-selected="true">
						Redemption Data
					</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link" id="transaction-history" data-bs-toggle="tab" href="#tab-transaction-history" role="tab" aria-controls="tab-transaction-history" aria-selected="false" tabindex="-1">
						Transaction History
					</a>
				</li>
			</ul>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane active fade show" id="tab-customer-profile" role="tabpanel" aria-labelledby="customer-profile">
					<livewire:user-detail-form :user="$user" />
				</div>
				<div class="tab-pane fade show" id="tab-redemption-data" role="tabpanel" aria-labelledby="redemption-data">
					<livewire:data-redemption-user :user="$user" />
				</div>
				<div class="tab-pane fade" id="tab-transaction-history" role="tabpanel" aria-labelledby="transaction-history">
					<livewire:customer-transaction-table :user="$user" />
				</div>
			</div>
		</div>
	</div>
</div>