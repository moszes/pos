<div class="col card">
  <div class="card-body  scrollbar-overlay">
    <h3 class="text-center mb-3">Sales Invoice Items</h3>
    <table class="table table-hover">
      <thead>
      <tr>
        <th class="px-3">Module</th>
        <th class="px-3">Code</th>
        <th class="px-3">Name</th>
        <th class="px-3">Status</th>
        <th class="px-3 text-center">Qty</th>
        <th class="px-3 text-end">Price</th>
        <th class="px-3 text-end">Total_price</th>
      </tr>
      </thead>
      <tbody>
      @if($salesInvoice)
      @forelse($salesInvoice->items as $item)
        <tr>
          <td class="px-3">{{ $item->module }}</td>
          <td class="px-3">{{ getDataByModule($item->module,$item->module_id)->code }}</td>
          <td class="px-3">{{ getDataByModule($item->module, $item->module_id)->name }}</td>
          <td class="px-3">{{ $item->transaction_status }}</td>
          <td class="px-3 text-center">{{ numberFormat($item->qty) }}</td>
          <td class="px-3 text-end">{{ numberFormat($item->price) }}</td>
          <td class="px-3 text-end">{{ numberFormat($item->total_price) }}</td>
        </tr>
      @empty
        <tr>
          <td class="text-center">Empty Data</td>
        </tr>
      @endforelse
      @endif
      </tbody>
    </table>
  </div>
</div>