<div class="d-flex gap-3">
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
  <div class="mb-3 col-6">
    <label class="form-label" for="packageSearch">Package Search</label>
    <input wire:model.debounce.100ms="packageSearch" wire:click="$emit('clearPackageSearch')" id="packageSearch" class="form-control" type="text">
    @error('packageSearch') <span class="text-danger">{{ $message }}</span> @enderror
    @if($packageList)
      <ul class="list-group">
        @forelse($packages as $packageItem)
          <li wire:click="selectPackage({{$packageItem->id}})" class="list-group-item pointer" wire:loading.class.remove="disabled" disabled>
            {{ $packageItem->code }}, {{ $packageItem->name }}, {{ number_format($packageItem->price) }}
          </li>
        @empty
          <li class="list-group-item">Empty Package</li>
        @endforelse
      </ul>
    @endif
  </div>
  <div class="mb-3 col">
    <label class="form-label" for="packageQty">Package Qty</label>
    <input wire:model.debounce.100ms="packageQty" id="packageQty" class="form-control" type="number">
    @error('packageQty') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="mb-3 col">
    <label class="form-label" for="packageDiscountItem">Package Disc</label>
    <input wire:model.debounce.100ms="packageDiscountItem" id="packageDiscountItem" class="form-control" type="number">
    @error('packageDiscountItem') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="mb-3 col">
    <label class="form-label" for="packagePrice">Package Price</label>
    <input wire:model.debounce.100ms="packagePrice" id="packagePrice" class="form-control" type="number" disabled>
    @error('packagePrice') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
</div>

@push('scripts')
  <script>
    Livewire.on('packageRequired', () => {
		  Swal.fire({
		  title: 'Required!',
		  text: 'Please select a package.',
      icon: 'error',
      confirmButtonText: 'Ok'
      })
    })
  </script>
@endpush