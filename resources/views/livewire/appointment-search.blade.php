<div class="col">
  <label class="form-label" for="appointmentSearch">Appointment</label>
  <input wire:model.debounce.100ms="appointmentSearch" id="appointmentSearch" class="form-control" type="text">
  @error('appointmentSearch') <div class="text-danger">{{ $message }}</div> @enderror
  @if($appointments)
    <ul class="list-group">
      @forelse($appointments as $appointment)
        <li wire:click="selectAppointment({{$appointment->id}})" class="list-group-item pointer">
          {{ $appointment->appointment_number }}, {{ $appointment->customer->details->first_name }}
        </li>
      @empty
        <li class="list-group-item">Empty Appointment</li>
      @endforelse
    </ul>
  @endif
</div>
