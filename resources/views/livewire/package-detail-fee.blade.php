<div class="card mt-3">
	<div class="card-body">
		<div class="d-flex col gap-4">
			<div class="col d-flex flex-column mb-1 gap-2">
				<h5 class="text-center mb-1">Normal Fee</h5>
				@foreach($package->normalFees as $normalFee)
					<div class="d-flex justify-content-between">
						<p class="fw-bold m-0">{{ str_replace('_', ' ', ucfirst($normalFee->role)) }} Fee</p>
						<p class="fs-sm mb-0 w-50 text-end">{{ $normalFee->fee_symbol == 'fixed' ? '(Rp.)' : '(%)' }} {{ @countFormat($normalFee->value) }}</p>
					</div>
				@endforeach
			</div>
			<div class="col d-flex flex-column mb-1 gap-2">
				<h5 class="text-center mb-1">Discount Fee</h5>
				@foreach($package->discountFees as $discountFee)
					<div class="d-flex justify-content-between">
						<p class="fw-bold m-0">{{ str_replace('_', ' ', ucfirst($discountFee->role)) }} Fee</p>
						<p class="fs-sm mb-0 w-50 text-end"> {{ $discountFee->fee_symbol == 'fixed' ? '(Rp.)' : '(%)' }} {{ @countFormat($discountFee->value) }}</p>
					</div>
				@endforeach
			</div>
			<div class="col d-flex flex-column mb-1 gap-2">
				<h5 class="text-center mb-1">Target Fee</h5>
				@foreach($package->targetFees as $targetFee)
					<div class="d-flex justify-content-between">
						<p class="fw-bold m-0">{{ str_replace('_', ' ', ucfirst($targetFee->role)) }} Fee</p>
						<p class="fs-sm mb-0 w-50 text-end"> {{ $targetFee->fee_symbol == 'fixed' ? '(Rp.)' : '(%)' }} {{ @countFormat($targetFee->value) }}</p>
					</div>
				@endforeach
			</div>
		</div>
	</div>
</div>