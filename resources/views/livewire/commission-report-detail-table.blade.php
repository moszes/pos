<div class="col-12">
	<div class="align-items-end d-flex justify-content-between pb-5">
		<div class="text-start">
			<h3>{{ $titlePage }} - {{ date('d F Y', strtotime($dateFrom)) }} - {{ date('d F Y', strtotime($dateTo)) }}</h3>
		</div>
		<div>
			<h4>{{ $staff->name }} - {{ $staff->getRoleNames()[0] }}</h4>
		</div>
	</div>
	<div class="card mb-5 mt-3">
		<div class="card-body d-flex gap-3 justify-content-start">
			<div class="d-flex gap-3 col">
				<label class="w-25" for="dateFrom">Date Form</label>
				<input type="date" id="dateFrom" class="form-control w-75" wire:model="dateFrom" placeholder="Date">
			</div>
			<div class="d-flex gap-3 col">
				<label class="w-25" for="dateTo">Date To</label>
				<input type="date" id="dateTo" class="form-control w-75" wire:model="dateTo" placeholder="Date">
			</div>
		</div>
	</div>
	<div class="table-responsive scrollbar">
		<table class="table table-hover fs--1 mb-0 ">
			<thead>
			<tr>
				<th class="sort px-3 text-start align-middle" scope="col" rowspan="2">Date</th>
				<th class="sort px-3 text-start align-middle" scope="col" rowspan="2">INV No.</th>
				<th class="sort px-3 text-start align-middle" scope="col" rowspan="2">Type</th>
				<th class="sort px-3 text-start align-middle" scope="col" rowspan="2">Code</th>
				<th class="sort px-3 text-start align-middle" scope="col" rowspan="2">Name</th>
				<th class="sort px-3 text-end align-middle" scope="col" rowspan="2">Normal Price</th>
				<th class="sort px-3 text-end align-middle" scope="col" rowspan="2">Sell Price</th>
				<th class="sort px-3 text-end align-middle" scope="col" rowspan="2">% Fee</th>
				<th class="sort px-3 text-end align-middle" scope="col" rowspan="2">Fee</th>
			</tr>
			</thead>
			<tbody class="list">
			@forelse($commissions as $commission)
				<tr>
					<td class="px-3 text-start">{{ date('d F Y', strtotime(getSalesInvoiceById($commission->sales_invoice_items->sales_invoice_id)->sales_invoice_date)) }}</td>
					<td class="px-3">{{ @getSalesInvoiceById($commission->sales_invoice_items->sales_invoice_id)->sales_invoice_number }}</td>
					<td class="px-3">{{ $commission->type }}</td>
					<td class="px-3">{{ $commission->code }}</td>
					<td class="px-3">{{ $commission->name }}</td>
					<td class="px-3 text-end">{{ $commission->normal_price ?? 0 }}</td>
					<td class="px-3 text-end">{{ $commission->sell_price ?? 0 }}</td>
					<td class="px-3 text-end">{{ countFormat($commission->fee) }}</td>
					<td class="px-3 text-end">{{ countFormat($commission->fee) }}</td>
				</tr>
			@empty
				<tr>
					<td colspan="9" class="text-center">Empty Data</td>
				</tr>
			@endforelse
			</tbody>
		</table>
		{{ $commissions->links() }}
	</div>
</div>