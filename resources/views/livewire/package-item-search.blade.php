<div>
	<h4 class="text-center mt-5 mb-3">Package Items</h4>
	<div class="d-flex flex-column">
		<div class="col">
			<label class="form-label" for="packageItemSearch">Product / Treatment</label>
			<input wire:model.debounce.300ms="packageItemSearch" id="packageItemSearch" class="form-control" type="text" placeholder="Search Product, Treatment...">
			@error('packageItemSearch') <span class="text-danger">{{ $message }}</span> @enderror
		</div>
		@if($packageItemList)
			<ul class="list-group">
				@forelse($packageItemResults as $packageItemResult)
					<li
						wire:click="selectPackageItem('{{$packageItemResult['id']}}', '{{$packageItemResult['code']}}', '{{$packageItemResult['module']}}')"
						class="list-group-item pointer"
						wire:loading.remove.class="disabled"
						disabled
					>
						{{ $packageItemResult['code']}}, {{ $packageItemResult['name']}}, {{ numberFormat($packageItemResult['price']) }}
					</li>
				@empty
					<li class="list-group-item">Empty</li>
				@endforelse
			</ul>
		@endif
		
		@if($packageItemForm)
			<div class="d-flex gap-3 mt-3">
				<div class="mb-3 col-1">
					<label class="form-label" for="packageItemModule">Module</label>
					<input wire:model.debounce.100ms="packageItemModule" id="packageItemModule" class="form-control" type="text" disabled>
					@error('packageItemModule') <span class="text-danger">{{ $message }}</span> @enderror
				</div>
				<div class="mb-3 col-2">
					<label class="form-label" for="packageItemCode">Code</label>
					<input wire:model.debounce.100ms="packageItemCode" id="packageItemCode" class="form-control" type="text" disabled>
					@error('packageItemCode') <span class="text-danger">{{ $message }}</span> @enderror
				</div>
				<div class="mb-3 col-4">
					<label class="form-label" for="packageItemName">Name</label>
					<input wire:model.debounce.100ms="packageItemName" id="packageItemName" class="form-control" type="text" disabled>
					@error('packageItemName') <span class="text-danger">{{ $message }}</span> @enderror
				</div>
				<div class="mb-3 col-2">
					<label class="form-label" for="packageItemPrice">Price</label>
					<input wire:model.debounce.100ms="packageItemPrice" id="packageItemPrice" class="form-control" type="number" onclick="clearValue(this)" onblur="zeroValue(this)">
					@error('packageItemPrice') <span class="text-danger">{{ $message }}</span> @enderror
				</div>
				<div class="mb-3 col-1">
					<label class="form-label" for="packageItemQty">Item Qty</label>
					<input wire:model.debounce.100ms="packageItemQty" id="packageItemQty" class="form-control" type="number" onclick="clearValue(this)" onblur="zeroValue(this)">
					@error('packageItemQty') <span class="text-danger">{{ $message }}</span> @enderror
				</div>
				<div class="pt-4 col">
					<a href="#" wire:click="addPackageItem" class="btn btn-primary px-3 float-end">+</a>
				</div>
			</div>
		@endif
		
		<table class="table table-hover table-bordered table-responsive mt-3">
			<thead>
			<tr>
				<th class="px-3 text-center"></th>
				<th class="px-3">Module</th>
				<th class="px-3">Code</th>
				<th class="px-3">Name</th>
				<th class="px-3 text-end">Qty</th>
				<th class="px-3 text-end">Normal Price</th>
				<th class="px-3 text-end">Sell Price</th>
			</tr>
			</thead>
			<tbody>
				@forelse($packageItems as $key => $packageItem)
				<tr>
					<td class="px-3 text-center">
						<a href="#" wire:click="removePackageItem({{$key}})"><i class="fas fa-trash"></i></a>
					</td>
					<td class="px-3">{{ @$packageItem['module'] }}</td>
					<td class="px-3 text-nowrap">{{ @$packageItem['code'] }}</td>
					<td class="px-3">{{ @$packageItem['name'] }}</td>
					<td class="px-3 text-end">{{ @$packageItem['qty'] }}</td>
					<td class="px-3 text-end">{{ numberFormat(@$packageItem['normal_price']) }}</td>
					<td class="px-3 text-end">{{ numberFormat(@$packageItem['sell_price']) }}</td>
				</tr>
				@empty
				<tr>
					<td colspan="7" class="text-center">Empty Data</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
	<div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>