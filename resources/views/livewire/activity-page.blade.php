<div class="shadow rounded">
  <div class="col-12">
    <div class="row align-items-end justify-content-between pb-5">
      <div class="col-3">
        <h3>Activity Log</h3>
      </div>
      <div class="col-3">
        <input wire:model.debounce.200ms="search" class="form-control search-input search form-control-sm" type="search" placeholder="Search" aria-label="Search">
      </div>
    </div>
    <div class="table-responsive scrollbar">
      <table class="table table-bordered table-responsive table-hover fs--1 mb-0 ">
        <thead>
          <tr>
            <th class="sort px-3" scope="col">#</th>
            <th class="sort align-middle" scope="col">Name</th>
            <th class="sort align-middle" scope="col">Description</th>
            <th class="sort align-middle" scope="col">Subject</th>
            <th class="sort align-middle" scope="col">Causer</th>
            <th class="sort align-middle" scope="col">Properties</th>
            <th class="sort align-middle" scope="col">Created At</th>
            <th class="sort align-middle" scope="col">Updated At</th>
            <th class="sort align-middle" scope="col">Event</th>
          </tr>
        </thead>
        <tbody class="list">
          @foreach($activities as $key => $activity)
          <tr>
            <td class="customer white-space-nowrap px-3">{{ $key + 1 }}</td>
            <td class="customer white-space-nowrap">{{ $activity->log_name }}</td>
            <td class="customer white-space-nowrap">{{ $activity->description }}</td>
            <td class="customer white-space-nowrap">{{ $activity->subject_type }}, {{ $activity->subject_id }}</td>
            <td class="customer white-space-nowrap">{{ $activity->causer_type }}, {{ $activity->causer_id }}</td>
            <td class="customer white-space-wrap px-3">{{ json_encode($activity->properties, JSON_PRETTY_PRINT) }}</td>
            <td class="customer white-space-nowrap">{{ $activity->created_at }}</td>
            <td class="customer white-space-nowrap">{{ $activity->updated_at }}</td>
            <td class="customer white-space-nowrap px-3">{{ $activity->event }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    {{ $activities->links() }}
  </div>
</div>