<div class="d-flex flex-column gap-3">
  <div>
    <h4>Add Appointment</h4>
  </div>
  <div>
    <livewire:customer-search :customer="$customer"/>
  </div>
  <div>
    <livewire:redemption-search />
  </div>
  <div>
    <livewire:room-search/>
  </div>
  <div>
    <livewire:employee-search />
  </div>
  
  <div class="d-flex justify-content-end">
    <button class="btn btn-primary" wire:click="addAppointmentItem">Add</button>
  </div>
</div>