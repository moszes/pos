<div class="d-flex flex-column mb-1 gap-3">
	<h5 class="mb-0">Discount Fee</h5>
	<div class="col d-flex flex-column gap-3">
		<div class="d-flex gap-3 justify-content-between">
			<label for="packageDiscountFeeSeniorDoctor">Senior Doctor</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">%</span>
					<input wire:model.debounce.500ms="packageDiscountFee.senior_doctor" type="number" id="packageDiscountFeeSeniorDoctor" class="form-control" aria-describedby="basic-addon1">
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="packageDiscountFeeDoctor">Doctor</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">%</span>
					<input wire:model.debounce.500ms="packageDiscountFee.doctor" type="number" id="packageDiscountFeeDoctor" class="form-control" aria-describedby="basic-addon1">
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="packageDiscountFeeDoctorAssistance">Doctor Assistance</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Rp</span>
					<input wire:model.debounce.500ms="packageDiscountFee.doctor_assistance" type="number" id="packageDiscountFeeDoctorAssistance" class="form-control" >
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="packageDiscountFeeTherapist">Therapist</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Rp</span>
					<input wire:model.debounce.500ms="packageDiscountFee.therapist" type="number" id="packageDiscountFeeTherapist" class="form-control" >
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="packageDiscountFeeSales">Sales</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">%</span>
					<input wire:model.debounce.500ms="packageDiscountFee.sales" type="number" id="packageDiscountFeeSales" class="form-control" >
				</div>
			</div>
		</div>
	</div>
	<div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>