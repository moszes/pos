<div class="col-4">
  <button wire:loading.attr="disabled" class="btn btn-sm btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#salesInvoiceForm">+</button>
  <div wire:ignore.self class="modal modal-xl fade" id="salesInvoiceForm" tabindex="-1" aria-hidden="true">
    <div class="loading d-none" wire:loading.class.remove="d-none"></div>
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5>
          <button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span class="fas fa-times fs--1"></span></button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div class="d-flex flex-column gap-3 mb-2">
              <div class="d-flex gap-3">
                <div class="col">
                  <livewire:customer-search :customer="$customer" />
                </div>
                <div class="col">
                  <label class="form-label" for="salesInvoiceNumber">Sales Invoice Number</label>
                  <input wire:model.debounce.500ms="salesInvoiceNumber" id="salesInvoiceNumber" class="form-control" type="text" disabled>
                  @error('salesInvoiceNumber') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="col">
                  <label class="form-label" for="salesInvoiceDate">Date</label>
                  <input wire:model.debounce.500ms="salesInvoiceDate" id="salesInvoiceDate" class="form-control" type="date" disabled>
                  @error('salesInvoiceDate') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
              </div>
            </div>
            <div class="d-flex flex-column mt-3">
              <div class="col">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  @if($treatmentModule)
                  <li wire:click="$emit('setModule', 'treatment')" class="nav-item" role="presentation">
                    <button class="nav-link @if($module == 'treatment') active @endif" id="treatment-tab" data-bs-toggle="tab" data-bs-target="#treatment" type="button" role="tab" aria-controls="treatment" aria-selected="true">Treatment</button>
                  </li>
                  @endif
                  @if($marketPlaceModule)
                  <li wire:click="$emit('setModule', 'marketplace')" class="nav-item" role="presentation">
                    <button class="nav-link @if($module == 'marketplace') active @endif" id="marketplace-tab" data-bs-toggle="tab" data-bs-target="#marketplace" type="button" role="tab" aria-controls="marketplace" aria-selected="true">Marketplace</button>
                  </li>
                  @endif
                  @if($packageModule)
                  <li wire:click="$emit('setModule', 'package')" class="nav-item" role="presentation">
                    <button class="nav-link @if($module == 'package') active @endif" id="package-tab" data-bs-toggle="tab" data-bs-target="#package" type="button" role="tab" aria-controls="package" aria-selected="false">Package</button>
                  </li>
                   @endif
                  <li wire:click="$emit('setModule', 'product')" class="nav-item" role="presentation">
                    <button class="nav-link @if($module == 'product') active @endif" id="product-tab" data-bs-toggle="tab" data-bs-target="#product" type="button" role="tab" aria-controls="product" aria-selected="false">Product</button>
                  </li>
                  @if($subscriptionModule)
                  <li wire:click="$emit('setModule', 'subscriptions')" class="nav-item" role="presentation">
                    <button class="nav-link @if($module == 'subscriptions') active @endif" id="subscriptions-tab" data-bs-toggle="tab" data-bs-target="#subscriptions" type="button" role="tab" aria-controls="subscriptions" aria-selected="false">Subscriptions</button>
                  </li>
                  @endif
                </ul>
                <div class="tab-content" id="tabContent">
                  @if($treatmentModule)
                  <div
                    class="tab-pane fade @if($module == 'treatment') show active @endif"
                    id="treatment"
                    role="tabpanel"
                    aria-labelledby="treatment-tab"
                  >
                    <livewire:treatment-search />
                  </div>
                  @endif
                  @if($marketPlaceModule)
                  <div
                    class="tab-pane fade @if($module == 'marketplace') show active @endif"
                    id="marketplace"
                    role="tabpanel"
                    aria-labelledby="marketplace-tab"
                  >
                    <livewire:market-place-search />
                  </div>
                  @endif
                  @if($packageModule)
                  <div
                    class="tab-pane fade @if($module == 'package') show active @endif"
                    id="package"
                    role="tabpanel"
                    aria-labelledby="package-tab"
                  >
                    <livewire:package-search />
                  </div>
                  @endif
                  <div
                    class="tab-pane fade @if($module == 'product') show active @endif"
                    id="product"
                    role="tabpanel"
                    aria-labelledby="product-tab"
                  >
                    <livewire:product-search />
                  </div>
                  @if($subscriptionModule)
                  <div
                    class="tab-pane fade @if($module == 'subscriptions') show active @endif"
                    id="subscriptions"
                    role="tabpanel"
                    aria-labelledby="subscriptions-tab"
                  >
                    <livewire:subscription-search />
                  </div>
                  @endif
                  @if($treatmentModule)
                  <div class="d-flex gap-3">
                    @if($module === 'treatment' || $module === 'marketplace')
                    <livewire:room-search />
                    @endif
                    <livewire:person-in-charge />
                  </div>
                  @endif
                </div>
                <div class="d-flex gap-3">
                  <div class="col d-flex gap-3">
                    <div class="mb-3 col">
                      <label class="form-label" for="salesInvoiceOthers">Others</label>
                      <input wire:model.debounce.500ms="salesInvoiceOthers" id="salesInvoiceOthers" class="form-control" type="number">
                      @error('salesInvoiceOthers') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="mb-3 col">
                      <label class="form-label" for="salesInvoiceDiscount">Disc By Invoice</label>
                      <input wire:model.debounce.500ms="salesInvoiceDiscount" id="salesInvoiceDiscount" class="form-control" type="number">
                      @error('salesInvoiceDiscount') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                  </div>
                </div>
                <div class="d-flex flex-column">
                  <div class="form-check form-switch ps-7">
                    <input wire:model.lazy="noteWidget" class="form-check-input" id="flexSwitchCheckDefault" type="checkbox" />
                    <label class="form-check-label" for="flexSwitchCheckDefault">Note</label>
                  </div>
                  @if($noteWidget)
                  <div class="col mb-3">
                    <label class="form-label" for="note">Note</label>
                    <textarea wire:model.debounce.500ms="note" class="form-control form-icon-input" id="note"></textarea>
                    @error('note') <span class="text-danger">{{ $message }}</span> @enderror
                  </div>
                  @endif
                </div>
                <button  wire:click="addItem" class="btn btn-primary float-end" type="button" @if(!$item) disabled @endif>
                  + Add Item
                </button>
              </div>
            </div>
            <h4 class="text-center mt-5 mb-3">Transaction</h4>
            <livewire:sales-invoice-item-table />
            <h4 class="text-center mt-5 mb-3">Payments</h4>
            <livewire:sales-invoice-payment-form />
            <livewire:sales-invoice-payment-table />
            <div class="float-end mt-5 d-flex gap-1">
              @if(!$hasDiscount && !$salesInvoice)
              <button wire:loading.attr="disabled" class="btn btn-success" type="button" wire:click="checkout">Checkout</button>
              @endif
              <button wire:loading.attr="disabled" type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>

@push('scripts')
<script>
  let salesInvoiceForm = new bootstrap.Modal(
                        document.getElementById('salesInvoiceForm'),
                        {
                          backdrop: 'static',
                        }
                      )
  Livewire.on('needApproval', function(){
    Swal.fire({
      title: 'Need Approval!',
      text: 'Sales Invoice Need Approval',
      icon: 'error',
      confirmButtonText: 'Ok'
    })
  })

  Livewire.on('showSalesInvoiceForm', () => {
    salesInvoiceForm.show();
  })

  Livewire.on('hideSalesInvoiceForm', () => {
    salesInvoiceForm.hide();
  })
  
  Livewire.on('itemsRequired', () => {
    Swal.fire({
      title: 'Items Required!',
      text: 'Please Add Treatment, Marketplace, Package, or Product',
      icon: 'error',
      confirmButtonText: 'Ok'
    })
  })

  Livewire.on('siClosedAlready', () => {
    Swal.fire({
      title: 'closed!',
      text: 'Today Sales Closed Already',
      icon: 'error',
      confirmButtonText: 'Ok'
    })

    window.location.href = '/report/today_sales_report';
  })

  document.getElementById('salesInvoiceForm').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearSalesInvoiceForm');
  });

  document.addEventListener("DOMContentLoaded", function() {
    let appointmentId = '{{ request()->get('appointmentId') }}';

    if(appointmentId){
      Livewire.emit('showSalesInvoiceForm');
      Livewire.emit('appointmentSelected' , appointmentId);
    }
  });
</script>
@endpush