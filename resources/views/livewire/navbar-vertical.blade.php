<nav class="navbar navbar-vertical navbar-expand-lg">
  <script>
    const navbarStyle = window.config.config.phoenixNavbarStyle;
    if (navbarStyle && navbarStyle !== 'transparent') {
      document.querySelector('body').classList.add(`navbar-${navbarStyle}`);
    }
  </script>
  <div class="collapse navbar-collapse" id="navbarVerticalCollapse">
    <!-- scrollbar removed-->
    <div class="navbar-vertical-content">
      <ul class="navbar-nav flex-column" id="navbarVerticalNav">
        @if( auth()->user()?->can('see home menu') || auth()->user()?->hasanyrole('godadmin|business owner|manager|customer service'))
        <livewire:dashboard-link />
        @endif
        <li class="nav-item">
          <!-- label-->
          <p class="navbar-vertical-label">Modules</p>
          <hr class="navbar-vertical-line" />
          <!-- parent pages-->
          @if( auth()->user()?->can('see master data menu') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
          <livewire:master-data-link />
          @endif
          <!-- parent pages-->
          @if( auth()->user()?->can('see transaction menu') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
          <livewire:transaction-link />
          @endif
          <!-- parent pages-->
          @if( auth()->user()?->can('see report menu') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
          <livewire:report-link />
          @endif
          <!-- parent pages-->
          @if( auth()->user()?->can('see setting menu') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
          <livewire:setting-link />
          @endif
          <!-- parent pages-->
          @if( auth()->user()?->can('see tool menu') || auth()->user()?->hasanyrole('godadmin'))
          <livewire:tool-link />
          @endif
          <!-- parent pages-->
          @if( auth()->user()?->can('see faq menu') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
          <livewire:faq-link />
          @endif
        </li>
      </ul>
    </div>
  </div>
  <div class="navbar-vertical-footer">
    <button class="btn navbar-vertical-toggle border-0 fw-semi-bold w-100 white-space-nowrap d-flex align-items-center"><span class="uil uil-left-arrow-to-left fs-0"></span><span class="uil uil-arrow-from-right fs-0"></span><span class="navbar-vertical-footer-text ms-2">Collapsed View</span></button>
  </div>
</nav>