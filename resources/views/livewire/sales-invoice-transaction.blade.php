<div class="col card">
  <div class="card-body scrollbar-overlay">
    <h3 class="text-center mb-3">User Transaction</h3>
    <table class="table table-hover">
      <thead>
      <tr>
        <th class="px-3">Invoice No.</th>
        <th class="px-3 text-center">Promo</th>
        <th class="px-3 text-center">Status</th>
        <th class="px-3 text-center">Module</th>
        <th class="px-3 text-center">Code</th>
        <th class="px-3 text-center">Name</th>
        <th class="px-3 text-center">Reference</th>
        <th class="px-3 text-center">Qty</th>
        <th class="px-3 text-end text-nowrap">Price</th>
        <th class="px-3 text-end text-nowrap">Total Price</th>
      </tr>
      </thead>
      <tbody>
      @if($salesInvoice)
      @forelse($salesInvoice->transactions as $transaction)
        @php($item = getDataByModule($transaction->module, $transaction->module_id))
        @php($total_price = floatval($item->price) * floatval($transaction->qty))
        @php($salesInvoice = getSalesInvoiceById($transaction->sales_invoice_id))
        @php($appointment = getAppointmentById($transaction->appointment_id))
        @if($transaction->reference_module)
        @php($reference = getDataByModule($transaction->reference_module, $transaction->reference_id))
        @endif
        <tr>
          <td class="px-3">
            <p>Sales Invoice Number: {{ $salesInvoice->sales_invoice_number }}</p>
            @if(@$appointment)
            <p>Appointment Number: {{ @$appointment->appointment_number }}</p>
            @endif
          </td>
          <td class="px-3">{{ $transaction->default_promo }}</td>
          <td class="px-3">{{ $transaction->transaction_status }}</td>
          <td class="px-3">{{ $transaction->module }}</td>
          <td class="px-3">{{ $item->code }}</td>
          <td class="px-3">{{ $item->name }}</td>
          @if($transaction->reference_module)
          <td class="px-3">{{ @$reference->code . ' - ' . @$reference->name  }}</td>
          @else
            <td class="px-3"></td>
          @endif
          <td class="px-3 text-center">{{ numberFormat($transaction->qty) }}</td>
          <td class="px-3 text-end">{{ numberFormat($item->price) }}</td>
          <td class="px-3 text-end">{{ numberFormat($total_price) }}</td>
        </tr>
      @empty
        <tr>
          <td class="text-center">Empty Data</td>
        </tr>
      @endforelse
      @endif
      </tbody>
    </table>
  </div>
</div>