 <div class="col-12">
  <div class="row align-items-end justify-content-between pb-5">
    <livewire:company-form />
    <div class="col-3">
      <h3>{{ $titlePage }}</h3>
    </div>
    <div class="col-3">
      <input wire:model.debounce.200ms="search" class="form-control search-input search form-control-sm" type="search" placeholder="Search" aria-label="Search">
    </div>
  </div>
  <div class="table-responsive scrollbar">
    <table class="table table-hover fs--1 mb-0 ">
      <thead>
        <tr>
          <th class="sort px-3" scope="col">Code</th>
          <th class="sort px-3" scope="col">Name</th>
          <th class="sort px-3" scope="col">Email</th>
          <th class="sort px-3" scope="col">Phone</th>
          <th class="sort px-3" scope="col">Address</th>
          <th class="sort px-3" scope="col">Website</th>
          <th class="sort text-center px-2" scope="col">Action</th>
        </tr>
      </thead>
      <tbody class="list">
        @foreach($companies as $key => $company)
        <tr>
          <td class="px-3">{{ $company->code }}</td>
          <td class="px-3">{{ $company->name }}</td>
          <td class="px-3">{{ $company->email }}</td>
          <td class="px-3">{{ $company->phone }}</td>
          <td class="px-3">{{ $company->address }}</td>
          <td class="px-3">{{ $company->website }}</td>
          <td class="text-center px-2">
            <a class="me-2 pointer" wire:click="$emit('editCompany', '{{$company->id}}')"><i class="fa fa-pencil"></i></a>
            <a class="text-danger pointer" wire:click="$emit('destroy', '{{$company->id}}')" onclick="confirm('Are you sure?') || event.stopImmediatePropagation()"><i class="fa fa-remove"></i></a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{ $companies->links() }}
  </div>
</div>