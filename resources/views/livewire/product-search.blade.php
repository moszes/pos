<div class="d-flex gap-3">
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
  <div class="mb-3 col-6">
    <label class="form-label" for="productSearch">Product Search</label>
    <input wire:model.debounce.100ms="productSearch" wire:click="$emit('clearProductSearch')" id="productSearch" class="form-control" type="text">
    @error('productSearch') <span class="text-danger">{{ $message }}</span> @enderror
    @if($productList)
      <ul class="list-group">
        @forelse($products as $productItem)
          <li wire:click="selectProduct({{$productItem->id}})" class="list-group-item pointer" wire:loading.class.remove="disabled" disabled>
            {{ $productItem->code }}, {{ $productItem->name }}, {{ numberFormat($productItem->normal_price) }}
          </li>
        @empty
          <li class="list-group-item">Empty Product</li>
        @endforelse
      </ul>
    @endif
  </div>
  <div class="mb-3 col">
    <label class="form-label" for="productQty">Product Qty</label>
    <input wire:model.debounce.100ms="productQty" id="productQty" class="form-control" type="number">
    @error('productQty') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="mb-3 col">
    <label class="form-label" for="productDiscountItem">Product Disc</label>
    <input wire:model.debounce.100ms="productDiscountItem" id="productDiscountItem" class="form-control" type="number">
    @error('productDiscountItem') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="mb-3 col">
    <label class="form-label" for="productPrice">Product Price</label>
    <input wire:model.debounce.100ms="productPrice" id="productPrice" class="form-control" type="number" disabled>
    @error('productPrice') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
</div>

@push('scripts')
<script>
  Livewire.on('productRequired', () => {
	  Swal.fire({
      title: 'Required!',
      text: 'Please select a product.',
      icon: 'error',
      confirmButtonText: 'Ok'
    })
  });
</script>
@endpush
