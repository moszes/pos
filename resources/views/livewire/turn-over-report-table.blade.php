 <div class="col-12">
  <div class="d-flex align-items-end justify-content-between pb-5">
    <livewire:turn-over-form />
    <div>
      <h3><label for="month">{{ $titlePage }}</label></h3>
    </div>
    <div class="d-flex gap-3">
      <div class="form-group">
        <label for="dateStart" class="form-label">Start</label>
        <input type="datetime-local" id="dateStart" class="form-control" wire:model="dateStart" placeholder="Date Start">
      </div>
      <div class="form-group">
        <label for="dateEnd" class="form-label">End</label>
        <input type="datetime-local" id="dateEnd" class="form-control" wire:model="dateEnd" placeholder="Date End">
      </div>
    </div>
  </div>
  <div class="table-responsive scrollbar">
    <table class="table table-hover fs--1 mb-0 ">
      <thead>
        <tr>
          <th class="sort px-3 text-center" scope="col">Month</th>
          <th class="sort px-3 text-end text-nowrap" scope="col">Invoice Treatment</th>
          <th class="sort px-3 text-end text-nowrap" scope="col">Invoice Market Place</th>
          <th class="sort px-3 text-end text-nowrap" scope="col">Invoice Package</th>
          <th class="sort px-3 text-end text-nowrap" scope="col">Invoice Product</th>
          <th class="sort px-3 text-end text-nowrap" scope="col">Invoice Subscription</th>
          <th class="sort px-3 text-end text-nowrap" scope="col">Cash</th>
          <th class="sort px-3 text-end text-nowrap" scope="col">CC</th>
          <th class="sort px-3 text-end text-nowrap" scope="col">Custom</th>
          <th class="sort px-3 text-end text-nowrap" scope="col">Prepaid</th>
          <th class="sort px-3 text-end text-nowrap" scope="col">QR</th>
          <th class="sort px-3 text-end text-nowrap" scope="col">Voucher</th>
        </tr>
      </thead>
      <tbody class="list">
        @php
            $subTotalTurnOverTreatment = 0;
            $subTotalTurnOverMarketPlace = 0;
            $subTotalTurnOverProduct = 0;
            $subTotalTurnOverPackage = 0;
            $subTotalTurnOverSubscription = 0;
            $subTotalTurnOverCashPayment = 0;
            $subTotalTurnOverCreditDebitPayment = 0;
            $subTotalTurnOverCustomPayment = 0;
            $subTotalTurnOverPrepaidPayment = 0;
            $subTotalTurnOverQRPayment = 0;
            $subTotalTurnOverVoucherPayment = 0;
            $grandTotalInvoice = 0;
            $grandTotalPayment = 0;
        @endphp
        @for($day = 1; $day <= $daysInMonth; $day++)
        @if(!getTurnOverRecordsByDate($day, $dateStart, $dateEnd))
          @continue
        @endif
        @php
          $turnOverTreatment = getTurnOverTreatmentByDate($day, $dateStart, $dateEnd);
          $turnOverMarketPlace = getTurnOverMarketPlaceByDate($day, $dateStart, $dateEnd);
          $turnOverPackage = getTurnOverPackageByDate($day, $dateStart, $dateEnd);
          $turnOverProduct = getTurnOverProductByDate($day, $dateStart, $dateEnd);
          $turnOverSubscription = getTurnOverSubscriptionByDate($day, $dateStart, $dateEnd);
          $turnOverCashPayment = getTurnOverCashPaymentByDate($day, $dateStart, $dateEnd);
          $turnOverCreditDebitPayment = getTurnOverCreditDebitPaymentByDate($day, $dateStart, $dateEnd);
          $turnOverCustomPayment = getTurnOverCustomPaymentByDate($day, $dateStart, $dateEnd);
          $turnOverPrepaidPayment = getTurnOverPrepaidPaymentByDate($day, $dateStart, $dateEnd);
          $turnOverQRPayment = getTurnOverQRPaymentByDate($day, $dateStart, $dateEnd);
          $turnOverVoucherPayment = getTurnOverVoucherPaymentByDate($day, $dateStart, $dateEnd);
          
          $subTotalTurnOverTreatment += $turnOverTreatment;
          $subTotalTurnOverMarketPlace += $turnOverMarketPlace;
          $subTotalTurnOverPackage += $turnOverPackage;
          $subTotalTurnOverProduct += $turnOverProduct;
          $subTotalTurnOverSubscription += $turnOverSubscription;
          $subTotalTurnOverCashPayment += $turnOverCashPayment;
          $subTotalTurnOverCreditDebitPayment += $turnOverCreditDebitPayment;
          $subTotalTurnOverCustomPayment += $turnOverCustomPayment;
          $subTotalTurnOverPrepaidPayment += $turnOverPrepaidPayment;
          $subTotalTurnOverQRPayment += $turnOverQRPayment;
          $subTotalTurnOverVoucherPayment += $turnOverVoucherPayment;
          $grandTotalInvoice += $turnOverTreatment + $turnOverMarketPlace + $turnOverPackage + $turnOverProduct + $turnOverSubscription;
          $grandTotalPayment += $turnOverCashPayment + $turnOverCreditDebitPayment + $turnOverCustomPayment + $turnOverPrepaidPayment + $turnOverQRPayment + $turnOverVoucherPayment;
          @endphp
        
        <tr>
          <td class="px-3 text-center text-nowrap">
            {{$day}} - {{ date('F', strtotime($month)) }}
          </td>
          <td class="px-3 text-end text-nowrap">{{ numberFormat($turnOverTreatment) }}</td>
          <td class="px-3 text-end text-nowrap">{{ numberFormat($turnOverMarketPlace) }}</td>
          <td class="px-3 text-end text-nowrap">{{ numberFormat($turnOverPackage) }}</td>
          <td class="px-3 text-end text-nowrap">{{ numberFormat($turnOverProduct) }}</td>
          <td class="px-3 text-end text-nowrap">{{ numberFormat($turnOverSubscription) }}</td>
          <td class="px-3 text-end text-nowrap">{{ numberFormat($turnOverCashPayment) }}</td>
          <td class="px-3 text-end text-nowrap">{{ numberFormat($turnOverCreditDebitPayment) }}</td>
          <td class="px-3 text-end text-nowrap">{{ numberFormat($turnOverCustomPayment) }}</td>
          <td class="px-3 text-end text-nowrap">{{ numberFormat($turnOverPrepaidPayment) }}</td>
          <td class="px-3 text-end text-nowrap">{{ numberFormat($turnOverQRPayment) }}</td>
          <td class="px-3 text-end text-nowrap">{{ numberFormat($turnOverVoucherPayment) }}</td>
        </tr>
        @endfor
      </tbody>
      <tfoot>
        <tr>
          <th class="text-center">Sub Total</th>
          <th class="px-3 text-end text-nowrap">{{ numberFormat($subTotalTurnOverTreatment) }}</th>
          <th class="px-3 text-end text-nowrap">{{ numberFormat($subTotalTurnOverMarketPlace) }}</th>
          <th class="px-3 text-end text-nowrap">{{ numberFormat($subTotalTurnOverPackage) }}</th>
          <th class="px-3 text-end text-nowrap">{{ numberFormat($subTotalTurnOverProduct) }}</th>
          <th class="px-3 text-end text-nowrap">{{ numberFormat($subTotalTurnOverSubscription) }}</th>
          <th class="px-3 text-end text-nowrap">{{ numberFormat($subTotalTurnOverCashPayment) }}</th>
          <th class="px-3 text-end text-nowrap">{{ numberFormat($subTotalTurnOverCreditDebitPayment) }}</th>
          <th class="px-3 text-end text-nowrap">{{ numberFormat($subTotalTurnOverCustomPayment) }}</th>
          <th class="px-3 text-end text-nowrap">{{ numberFormat($subTotalTurnOverPrepaidPayment) }}</th>
          <th class="px-3 text-end text-nowrap">{{ numberFormat($subTotalTurnOverQRPayment) }}</th>
          <th class="px-3 text-end text-nowrap">{{ numberFormat($subTotalTurnOverVoucherPayment) }}</th>
        </tr>
        <tr>
          <th class="text-center">Grand Total</th>
          <th colspan="5" class="px-3 text-end text-nowrap">
            Invoice : {{ numberFormat($grandTotalInvoice) }}
          </th>
          <th colspan="6" class="px-3 text-end text-nowrap">
            Payment : {{ numberFormat($grandTotalPayment) }}
          </th>
        </tr>
      </tfoot>
    </table>
    {{ $turnovers->links() }}
  </div>
   <div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>