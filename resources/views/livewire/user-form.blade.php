<div class="d-flex gap-2">
  <button class="btn btn-sm btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#userForm">+</button>
  <div wire:ignore.self class="modal fade" id="userForm" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5>
          <button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span class="fas fa-times fs--1"></span></button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div class="mb-3">
              <label class="form-label" for="role">Role</label>
              <select wire:model.debounce.200ms="role" class="form-select" id="role" aria-label="Select role">
                <option selected>-- Choose --</option>
                {!! roleOption() !!}
              </select>
              @error('role') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="name">Name</label>
              <input wire:model.lazy="name" id="name" class="form-control" type="text">
              @error('name') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="phone">Phone</label>
              <input wire:model.lazy="phone" id="phone" class="form-control" type="text">
              @error('phone') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="email">Email</label>
              <input wire:model.lazy="email" id="email" class="form-control" type="text">
              @error('email') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="password">Password</label>
              <input wire:model.lazy="password" id="password" class="form-control" type="password">
              @error('password') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="password_confirmation">Password Confirmation</label>
              <input wire:model.lazy="password_confirmation" id="password_confirmation" class="form-control" type="password">
              @error('password_confirmation') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="float-end">
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>
	
  let userForm = new bootstrap.Modal(
    document.getElementById('userForm'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showUserForm', () => {
    userForm.show();
  })
  Livewire.on('hideUserForm', () => {
    userForm.hide();
    Livewire.emit('clearUserForm')
  })

  document.getElementById('userForm').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearUserForm')
  })
</script>
@endpush