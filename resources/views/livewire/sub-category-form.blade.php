<div class="d-flex justify-content-end">
	<button class="btn btn-sm btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#subCategoryForm">+</button>
	<div wire:ignore.self class="modal fade" id="subCategoryForm" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ $formTitle }}</h5>
					<button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span
							class="fas fa-times fs--1"></span></button>
				</div>
				<div class="modal-body">
					<form wire:submit.prevent="{{ $actionForm }}">
						<div class="mb-3">
							<label class="form-label" for="category">Category</label>
							<select wire:model.debounce.100ms="categoryId" class="form-select" id="category"
							        aria-label="Select Category">
								<option selected>-- Choose --</option>
								{!! getCategoryOptions() !!}
							</select>
							@error('categoryId') <span class="text-danger">{{ $message }}</span> @enderror
						</div>
						<div class="mb-3">
							<label class="form-label" for="code">code</label>
							<input wire:model.debounce.100ms="code" id="code" class="form-control" type="text">
							@error('code') <span class="text-danger">{{ $message }}</span> @enderror
						</div>
						<div class="mb-3">
							<label class="form-label" for="name">Name</label>
							<input wire:model.debounce.100ms="name" id="name" class="form-control" type="text">
							@error('name') <span class="text-danger">{{ $message }}</span> @enderror
						</div>
						<div class="float-end">
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@push('scripts')
	<script>

		let subCategoryForm = new bootstrap.Modal(
			document.getElementById('subCategoryForm'),
			{
				backdrop: 'static',
			}
		)
		Livewire.on('showSubCategoryForm', () => {
			subCategoryForm.show();
		})
		Livewire.on('hideSubCategoryForm', () => {
			subCategoryForm.hide();
		})
		document.getElementById('subCategoryForm').addEventListener('hidden.bs.modal', () => {
			Livewire.emit('clearSubCategoryForm');
		});
	</script>
@endpush