<div>
	<div class="mt-2 mb-6">
		<h3 class="mb-2">Dashboard</h3>
		<h5 class="text-700 fw-semi-bold">Here’s what’s going on at your business right now</h5>
	</div>
	<div class="row align-items-center g-4">
		<div class="col-12 col-md-auto">
			<div class="d-flex align-items-center">
        <span class="fa-stack" style="min-height: 46px;min-width: 46px;">
          <span class="fa-solid fa-square fa-stack-2x text-success-300" data-fa-transform="down-4 rotate--10 left-4"></span>
          <span class="fa-solid fa-circle fa-stack-2x stack-circle text-success-100" data-fa-transform="up-4 right-3 grow-2"></span>
          <span class="fa-stack-1x fa-solid fa-star text-success " data-fa-transform="shrink-2 up-8 right-6"></span>
        </span>
				<div class="ms-3">
					<h4 class="mb-0">{{ $salesInvoices }} Transactions</h4>
					<p class="text-800 fs--1 mb-0">This Month</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-auto">
			<div class="d-flex align-items-center">
        <span class="fa-stack" style="min-height: 46px;min-width: 46px;">
          <span class="fa-solid fa-square fa-stack-2x text-warning-300" data-fa-transform="down-4 rotate--10 left-4"></span>
          <span class="fa-solid fa-circle fa-stack-2x stack-circle text-warning-100" data-fa-transform="right-3 grow-2"></span>
          <span class="fa-stack-1x fa-solid fa-user text-warning " data-fa-transform="shrink-2 up right-6"></span>
        </span>
				<div class="ms-3">
					<h4 class="mb-0">{{$customers}} Customers</h4>
					<p class="text-800 fs--1 mb-0">Registered</p>
				</div>
			</div>
		</div>
	</div>
</div>