<div
	class="search-box navbar-top-search-box d-none d-lg-block"
	style="width:25rem;"
>
	<form
		class="position-relative"
		data-bs-toggle="search"
		data-bs-display="static"
		aria-expanded="true"
	>
		<label class="sr-only" for="posSearch">Search</label>
		<input
			id="posSearch"
			class="form-control search-input fuzzy-search
                  rounded-pill form-control-sm"
			type="search"
			placeholder="Search..."
			aria-label="Search"
		>
		<span class="fas fa-search search-box-icon"></span>
	</form>
	<div
		class="btn-close position-absolute end-0 top-50
        translate-middle cursor-pointer shadow-none"
		data-bs-dismiss="search"
	>
		<button class="btn btn-link btn-close-falcon p-0" aria-label="Close"></button>
	</div>
	<div
		class="dropdown-menu border border-300 font-base
        start-0 py-0 overflow-hidden w-100"
	>
		<div
			class="scrollbar-overlay"
			style="max-height: 30rem;"
			data-simplebar="init"
		>
			<div class="list pb-3">
			</div>
		</div>
	</div>
</div>