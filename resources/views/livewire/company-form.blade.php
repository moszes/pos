<div class="col-3">
  <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#companyFormModal">+</button>
  <div wire:ignore.self class="modal fade" id="companyFormModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5><button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span class="fas fa-times fs--1"></span></button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div class="mb-3">
              <label class="form-label" for="code">Code</label>
              <input wire:model.lazy="code" id="code" class="form-control" type="text">
              @error('code') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="name">Name</label>
              <input wire:model.lazy="name" id="name" class="form-control" type="text">
              @error('name') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="phone">Phone</label>
              <input wire:model.lazy="phone" id="phone" class="form-control" type="text">
              @error('phone') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="address">Address</label>
              <textarea wire:model.lazy="address" id="address" class="form-control"></textarea>
              @error('address') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="website">Website</label>
              <input wire:model.lazy="website" id="website" class="form-control" type="text">
              @error('website') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="float-end">
              <button type="button" wire:click="$emit('clearCompanyForm')" class="btn btn-light">Clear</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>
  let companyFormModal = new bootstrap.Modal(
    document.getElementById('companyFormModal'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showCompanyForm', () => {
    companyFormModal.show();
  })
  Livewire.on('hideCompanyForm', () => {
    companyFormModal.hide();
  })
  document.getElementById('companyFormModal').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearCompanyForm');
  });
</script>
@endpush