<div class="d-flex flex-column gap-3">
	<h4>Target Omset {{ $monthLabel }} - <span class="text-info">{{ priceFormat($turnOverAmount) }}</span></h4>
	<div class="progress" style="height:15px">
		<div class="progress-bar rounded-3" role="progressbar" style="width: {{$monthlyTargetPercentage}}%" aria-valuenow="{{$monthlyTargetPercentage}}" aria-valuemin="0" aria-valuemax="100">{{$monthlyTargetPercentage}}%</div>
	</div>
</div>

@push('styles')
	<style>
		.progress-bar{
        background: linear-gradient(to right, deeppink, greenyellow);
        transition: width 2s;
		}
	</style>
@endpush