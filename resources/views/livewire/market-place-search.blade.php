<div class="d-flex flex-column gap-3 mb-3">
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
  <div class="d-flex gap-3">
    <div class="col-6">
      <label class="form-label" for="marketPlaceSearch">marketplace Search</label>
      <input wire:model.debounce.100ms="marketPlaceSearch" wire:click="$emit('clearMarketPlaceSearch')" id="marketPlaceSearch" class="form-control" type="text">
      @error('marketPlaceSearch') <span class="text-danger">{{ $message }}</span> @enderror
      @if($marketPlaceList)
        <ul class="list-group">
          @forelse($marketPlaces as $marketPlace)
            <li wire:click="selectMarketplace({{$marketPlace->id}})" class="list-group-item pointer">
              {{ $marketPlace->code }}, {{ $marketPlace->name }}, {{ $marketPlace->price }}
            </li>
          @empty
            <li class="list-group-item">Empty marketplace</li>
          @endforelse
        </ul>
      @endif
    </div>
    <div class="col">
      <label class="form-label" for="marketPlaceQty">Qty</label>
      <input wire:model.debounce.100ms="marketPlaceQty" id="marketPlaceQty" class="form-control" type="number">
      @error('marketPlaceQty') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="col">
      <label class="form-label" for="marketPlaceDiscountItem">Disc Item</label>
      <input wire:model.debounce.100ms="marketPlaceDiscountItem" id="marketPlaceDiscountItem" class="form-control" type="number">
      @error('marketPlaceDiscountItem') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="col">
      <label class="form-label" for="marketPlacePrice">Price</label>
      <input wire:model.debounce.100ms="marketPlacePrice" id="marketPlacePrice" class="form-control" type="number" disabled>
      @error('marketPlacePrice') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
  </div>
</div>