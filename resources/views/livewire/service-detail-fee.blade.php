<div class="d-flex flex-column gap-2 px-2">
  <h5>Commission Fee</h5>
  <table class="table table-hover">
    <thead>
      <tr>
        <th></th>
        <th class="text-end fs-lg--1">Normal</th>
        <th class="text-end fs-lg--1">Discount</th>
        <th class="text-end fs-lg--1">Target</th>
      </tr>
    </thead>
    <tbody>
      @foreach($roles as $role)
      <tr>
        <td class="py-0 ps-2 fs-lg--1">{{str_replace('_', ' ', ucfirst($role))}}</td>
        <td class="py-0 text-end fs-lg--1">{{ $normalFees->where('role', $role)->first()?->fee_symbol == 'fixed' ? '(Rp.)' : '(%)' }} {{ @countFormat($normalFees->where('role', $role)->first()?->value) }}</td>
        <td class="py-0 text-end fs-lg--1">{{ $discountFees->where('role', $role)->first()?->fee_symbol == 'fixed' ? '(Rp.)' : '(%)' }} {{ @countFormat($discountFees->where('role', $role)->first()?->value) }}</td>
        <td class="py-0 text-end fs-lg--1">{{ $targetFees->where('role', $role)->first()?->fee_symbol == 'fixed' ? '(Rp.)' : '(%)' }} {{ @countFormat($targetFees->where('role', $role)->first()->value) }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>