<div class="d-flex flex-column gap-4">
  <div class="row">
    <div class="col">
      <h4>{{ $titlePage }}</h4>
    </div>
    <div class="col-4 d-flex gap-2">
      <button class="btn btn-sm btn-success" wire:click="downloadCustomerData" title="Download Customer Data">
        <i class="fa fa-download fs--1"></i>
      </button>
      <input wire:model.debounce.200ms="search"
             class="form-control search-input search form-control-sm"
             type="search"
             placeholder="Search" aria-label="Search"
      >
      <livewire:customer-form/>
    </div>
  </div>
  <table class="table table-hover fs--1 mb-0 ">
    <thead>
    <tr>
      <th class="sort px-3" scope="col">Code</th>
      <th class="sort px-3" scope="col">Username</th>
      <th class="sort px-3" scope="col">FirstName</th>
      <th class="sort px-3" scope="col">LastName</th>
      <th class="sort px-3" scope="col">Email</th>
      <th class="sort px-3" scope="col">Phone</th>
      <th class="sort px-3" scope="col">Address</th>
      <th class="sort text-end pe-2" scope="col">Action</th>
    </tr>
    </thead>
    <tbody class="list">
    @forelse($customers as $key => $customer)
      <tr>
        <td class="px-3">{{ @$customer->code }}</td>
        <td class="px-3">
          <a
            href="{{ route('customer.data_redemption', [ 'customerId' => $customer->user_id]) }}">{{ $customer->name }}</a>
        </td>
        <td class="px-3">{{ @$customer->first_name }}</td>
        <td class="px-3">{{ @$customer->last_name }}</td>
        <td class="px-3">{{ @$customer->email }}</td>
        <td class="px-3">{{ @$customer->phone }}</td>
        <td class="px-3">{{ @$customer->address }}</td>
        <td class="text-end pe-2">
          <a class="me-2 pointer" wire:click="$emit('editCustomer', '{{$customer->user_id}}')"><i
              class="fa fa-pencil"></i></a>
          <a class="text-danger pointer" wire:click="$emit('deleteCustomer', '{{$customer->user_id}}')"><i
              class="fa fa-remove"></i></a>
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="8" class="text-center">Empty Data</td>
      </tr>
    @endforelse
    </tbody>
  </table>
  {{ $customers->links() }}
</div>

 @push('scripts')
   <script>
     window.addEventListener("DOMContentLoaded", function () {
       
       Livewire.on('deleteCustomer', (id) => {
         Swal.fire({
           title: 'Are you sure?',
           text: "You won't be able to revert this!",
           icon: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Yes, delete it!'
         }).then((result) => {
           if (result.isConfirmed) {
             Livewire.emit('destroy', id)
           }
         })
       })
     })
   
   </script>
 @endpush