<div class="col-3">
  <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#subscriptionPlanItemPlanForm">+</button>
  <div wire:ignore.self class="modal fade" id="subscriptionPlanItemPlanForm" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5>
          <button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span class="fas fa-times fs--1"></span></button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div>
              @if (session()->has('message'))
                <div class="alert alert-danger">
                  {{ session('message') }}
                </div>
              @endif
            </div>
            <div class="mb-3">
              <label class="form-label" for="searchTreatment">Search Treatment</label>
              <input wire:model.debounce.200ms="searchTreatment" id="searchTreatment" class="form-control" type="text">
              @error('searchTreatment') <span class="text-danger">{{ $message }}</span> @enderror
              @if($treatmentList)
                <ul class="list-group">
                  @forelse($treatments as $treatment)
                    <li wire:click="selectTreatment({{@$treatment['id']}})" class="list-group-item pointer" wire:loading.class.remove="disabled" disabled>
                      {{ @$treatment['code'] }}, {{ @$treatment['name'] }}, {{ @numberFormat($treatment['price']) }}
                    </li>
                  @empty
                    <li class="list-group-item">Empty Treatment</li>
                  @endforelse
                </ul>
              @endif
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>
  let subscriptionPlanItemPlanForm = new bootstrap.Modal(
    document.getElementById('subscriptionPlanItemPlanForm'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showSubscriptionPlanItemPlanForm', () => {
    subscriptionPlanItemPlanForm.show();
  })
  Livewire.on('hideSubscriptionPlanItemForm', () => {
    subscriptionPlanItemPlanForm.hide();
  })
  document.getElementById('subscriptionPlanItemPlanForm').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearSubscriptionPlanItemForm');
  });
</script>
@endpush