<div class="mb-4">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb mb-0">
			@php
				$segments = request()->segments();
			@endphp
			
			@foreach($segments as $index => $segment)
				<li class="breadcrumb-item {{ (end($segments) == $segment) ? 'active' : '' }}">
					@if(end($segments) == $segment)
						<span>{{ ucwords(str_replace('_', ' ', $segment)) }}</span>
					@else
						<a href="#">{{ ucwords(str_replace('_', ' ', $segment)) }}</a>
					@endif
				</li>
			@endforeach
		</ol>
	</nav>
</div>