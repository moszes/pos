<div class="col mb-3">
    <label class="form-label" for="vendor-id">Vendor</label>
    <select wire:model.debounce.200ms="vendorId" class="form-select" id="vendor-id" aria-label="Select vendor">
        <option selected>-- Choose --</option>
        {!! getVendorOptions() !!}
    </select>
    @error('vendorId') <span class="text-danger">{{ $message }}</span> @enderror
</div>
