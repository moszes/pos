<div class="d-flex flex-column">
  <div class="d-flex gap-3">
    <div class="col">
      <label class="form-label" for="redemptionSearch">Redemption Search</label>
      <input wire:model.debounce.200ms="redemptionSearch" id="redemptionSearch" class="form-control" type="text">
      @error('redemptionSearch')<div class="text-danger mt-1">{{ $message }}</div>@enderror
      @if($redemptionLists)
        <ul class="list-group">
          @forelse($redemptions as $redemptionList)
            <li
              wire:click="redeemSelected('{{@$redemptionList->module_id}}', '{{@$redemptionList->code}}', '{{@$redemptionList->name}}')"
              class="list-group-item pointer"
            >
              {{ @$redemptionList->code }}, {{ @$redemptionList->name }},
              <span class="fw-bold">(Sisa Treatment: {{ @$redemptionList->redemption_left }})</span>
            </li>
          @empty
            <li class="list-group-item">Empty redemption</li>
          @endforelse
        </ul>
      @endif
    </div>
  </div>
</div>