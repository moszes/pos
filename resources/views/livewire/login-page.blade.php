<div class="row flex-center mt-15 py-10 mx-auto">
  <div class="col-sm-10 col-md-8 col-lg-5 col-xl-5 col-xxl-3">
	  <div class="card">
		  <div class="card-body">
			  <form wire:submit.prevent="login">
				  <a class="d-flex flex-center text-decoration-none mb-3" href="/">
					  <div class="d-flex align-items-center fw-bolder fs-5 d-inline-block">
						  <img src="{{ asset('logo.png') }}" alt="phoenix" width="128"/>
					  </div>
				  </a>
				  
				  <div class="text-center mb-3">
					  <h3 class="text-1000">Log In</h3>
				  </div>
				  
				  <div class="mb-3 text-start">
					  <label class="form-label" for="name">Username</label>
					  <div class="form-icon-container">
						  <input
							  wire:model.lazy="name"
							  class="form-control form-icon-input"
							  id="name"
							  type="text"
							  placeholder="John Doe"
						  />
						  <span class="far fa-user text-300 fs--1 form-icon"></span>
						  @error('name')
						  <div class="text-danger">{{ $message }}</div>@enderror
					  </div>
				  </div>
				  
				  <div class="mb-3 text-start">
					  <label class="form-label" for="password">Password</label>
					  <div class="form-icon-container">
						  <input
							  wire:model.lazy="password"
							  class="form-control form-icon-input"
							  id="password"
							  type="password"
							  placeholder="Password"
						  />
						  <span class="fas fa-key text-900 fs--1 form-icon"></span>
						  @error('password')
						  <div class="text-danger">{{ $message }}</div>@enderror
					  </div>
				  </div>
				  <div class="text-end mb-3 d-flex justify-content-between">
					  <a href="/forgot_password" class="text-decoration-none">Forgot the password?</a>
				  </div>
				  
				  <button class="btn btn-primary w-100 mb-3" button="submit">Log In</button>
			  </form>
		  </div>
	  </div>
  </div>
</div>

@push('styles')
  <style>
    body{
      overflow-y: hidden;
    }
  </style>
@endpush