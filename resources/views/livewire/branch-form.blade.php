<div class="d-flex justify-content-between">
  <button class="btn btn-sm btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#branchForm">+</button>
  <div wire:ignore.self class="modal fade" id="branchForm" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5><button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span class="fas fa-times fs--1"></span></button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div class="mb-3">
              <label class="form-label" for="code">Code</label>
              <input wire:model.lazy="code" id="code" class="form-control" type="text">
              @error('code') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="name">Name</label>
              <input wire:model.lazy="name" id="name" class="form-control" type="text">
              @error('name') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="float-end">
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>
  
  let branchForm = new bootstrap.Modal(
    document.getElementById('branchForm'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showBranchForm', () => {
    branchForm.show();
  })
  Livewire.on('hideBranchForm', () => {
    branchForm.hide();
  })
  document.getElementById('branchForm').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearBranchForm');
  });
</script>
@endpush