<div>
	<livewire:toast-notification />
	<livewire:breadcrumb/>
	<div class="card">
		<div class="card-body d-flex flex-column gap-4">
			<livewire:item-detail
				:redirectRoute="$redirectRoute"
				:priceLabel="$priceLabel"
				:moduleName="$moduleName"
				:moduleLabel="$moduleLabel"
				:item="$package"
			/>
			<livewire:service-fee-form :moduleName="$moduleName" :item="$package" />
			<div class="loading d-none" wire:loading.class.remove="d-none"></div>
		</div>
	</div>
</div>

@push('scripts')
	<script>

		window.addEventListener("DOMContentLoaded", function () {
			Livewire.emit("openMasterDataModule");
			Livewire.emit("openServicesMenu")
		});
	
	</script>
@endpush