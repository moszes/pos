<div class="form-group">
  <label for="searchSalesInvoiceNumber" class="form-label">Sales Invoice Number</label>
  <input
    type="text"
    class="form-control"
    id="searchSalesInvoiceNumber"
    wire:model.debounce.300ms="searchSalesInvoiceNumber"
    wire:click="clearSearchBySalesInvoiceNumber"
  >
  @if($salesInvoiceLists)
  <ul class="list-group pointer">
    @forelse($salesInvoices as $salesInvoice)
      <li class="list-group-item" wire:click="$emit('selectSalesInvoice', '{{ $salesInvoice->sales_invoice_number }}')">
        <div>{{ $salesInvoice->customer->details->first_name }}</div>
        <div>{{ $salesInvoice->customer->details->phone }}</div>
        <div>{{ $salesInvoice->sales_invoice_number }}</div>
      </li>
    @empty
      <li class="list-group-item">
        Empty Data
      </li>
    @endforelse
  </ul>
  @endif
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>
