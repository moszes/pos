<div>
  <table class="table table-hover fs--1 mb-0 ">
    <thead>
    <tr>
      <th class="sort px-3" scope="col">Customer</th>
      <th class="sort px-3" scope="col">Subscription Plan</th>
      <th class="sort px-3" scope="col">Code</th>
      <th class="sort px-3" scope="col">Buy Date</th>
      <th class="sort px-3" scope="col">Active Date</th>
      <th class="sort px-3" scope="col">Status</th>
      <th class="sort text-center" scope="col">Action</th>
    </tr>
    </thead>
    <tbody class="list">
    @forelse($subscriptions as $key => $subscription)
      <tr>
        <td class="px-3">{{ @getUserById($subscription->customer_id)->details?->first_name }}</td>
        <td class="px-3">{{ @getSubscriptionById($subscription->subscription_plan_id)->name }}</td>
        <td class="px-3">{{ $subscription->code }}</td>
        <td class="px-3">{{ date('Y-m-d', strtotime($subscription->buy_date)) }}</td>
        <td class="px-3">{{ date('Y-m-d', strtotime($subscription->active_date)) }}</td>
        <td class="px-3">{{ $subscription->status }}</td>
        <td class="text-center">
          <a class="me-2 pointer" wire:click="$emit('editSubscription', '{{$subscription->id}}')"><i
              class="fa fa-pencil"></i></a>
          <a class="text-danger pointer" wire:click="$emit('deleteSubscription', '{{$subscription->id}}')">
            <i class="fa fa-remove"></i>
          </a>
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="7" class="text-center">Empty Data</td>
      </tr>
    @endforelse
    </tbody>
  </table>
  {{ $subscriptions->links() }}
</div>

 @push('scripts')
 <script>
   Livewire.on('deleteSubscription', (id) => {
     Swal.fire({
       title: 'Are you sure?',
       text: "You won't be able to revert this!",
       icon: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes, delete it!'
     }).then((result) => {
       if (result.isConfirmed) {
         Livewire.emit('destroy', id);
       }
     })
   })
 </script>
 @endpush