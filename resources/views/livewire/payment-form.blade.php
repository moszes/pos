<div class="d-flex justify-content-end">
  <button class="btn btn-sm btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#paymentModal">+</button>
  <div wire:ignore.self class="modal fade" id="paymentModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5><button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span class="fas fa-times fs--1"></span></button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div class="mb-3">
              <label class="form-label" for="category-name">Name</label>
              <input wire:model.lazy="name" id="category-name" class="form-control" type="text">
              @error('name') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="float-end">
              <button type="button" wire:click="$emit('clearForm')" class="btn btn-light">Clear</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>
  let paymentModalForm = new bootstrap.Modal(
                        document.getElementById('paymentModal'),
                        {
                          backdrop: 'static',
                        }
                      )
  Livewire.on('showPaymentForm', () => {
    paymentModalForm.show();
  })
  Livewire.on('hidePaymentForm', () => {
    paymentModalForm.hide();
  })
</script>
@endpush