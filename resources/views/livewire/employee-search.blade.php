<div class="d-flex flex-column gap-3">
  <div class="d-flex gap-3">
    <div class="col">
      <label class="form-label" for="doctorId">Doctor</label>
      <select wire:model.lazy="doctorId" class="form-select" id="doctorId" aria-label="Select Doctor">
        <option selected>-- Choose --</option>
        {!! doctorOption() !!}
      </select>
      @error('doctorId')
      <div class="text-danger">{{ $message }}</div> @enderror
    </div>
    <div class="col">
      <label class="form-label" for="doctorAssistanceId">Assistance</label>
      <select wire:model.lazy="doctorAssistanceId" class="form-select" id="doctorAssistanceId" aria-label="Select Assistance Doctor">
        <option selected>-- Choose --</option>
        {!! doctorAssistanceOption() !!}
      </select>
      @error('doctorAssistanceId')
      <div class="text-danger">{{ $message }}</div> @enderror
    </div>
    <div class="col">
      <label class="form-label" for="therapistId">Therapist</label>
      <select wire:model.lazy="therapistId" class="form-select" id="therapistId" aria-label="Select Therapist">
        <option selected>-- Choose --</option>
        {!! therapistOption() !!}
      </select>
      @error('therapistId')
      <div class="text-danger">{{ $message }}</div> @enderror
    </div>
  </div>
</div>