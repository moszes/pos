<li class="nav-item">
	<a class="nav-link dropdown-indicator" id="stakeholders-btn" href="#nv-users" data-bs-toggle="collapse" aria-expanded="false" aria-controls="nv-users">
		<div class="d-flex align-items-center">
			<div class="dropdown-indicator-icon">
				<span class="fas fa-caret-right"></span>
			</div>
			<span class="nav-link-text">Stakeholders</span>
		</div>
	</a>
	<!-- more inner pages-->
	<div class="parent-wrapper">
		<ul class="nav collapse parent" data-bs-parent="#forms" id="nv-users">
			<li class="nav-item">
				<a class="nav-link @if(Route::currentRouteName() == 'master.users') active @endif" href="{{ route('master.users') }}" aria-expanded="false">
					<div class="d-flex align-items-center">
						<span class="nav-link-text">User</span>
					</div>
				</a>
				<!-- more inner pages-->
			</li>
			<li class="nav-item">
				<a class="nav-link @if(Route::currentRouteName() == 'master.customers') active @endif" href="{{ route('master.customers') }}" aria-expanded="false">
					<div class="d-flex align-items-center">
						<span class="nav-link-text">Customer</span>
					</div>
				</a>
				<!-- more inner pages-->
			</li>
			<li class="nav-item">
				<a class="nav-link @if(Route::currentRouteName() == 'master.employees') active @endif" href="{{ route('master.employees') }}" aria-expanded="false">
					<div class="d-flex align-items-center">
						<span class="nav-link-text">Employee</span>
					</div>
				</a>
				<!-- more inner pages-->
			</li>
			<li class="nav-item">
				<a class="nav-link @if(Route::currentRouteName() == 'master.roles') active @endif" href="{{ route('master.roles') }}" aria-expanded="false">
					<div class="d-flex align-items-center">
						<span class="nav-link-text">Role</span>
					</div>
				</a>
				<!-- more inner pages-->
			</li>
			<li class="nav-item">
				<a class="nav-link @if(Route::currentRouteName() == 'master.permissions') active @endif" href="{{ route('master.permissions') }}" aria-expanded="false">
					<div class="d-flex align-items-center">
						<span class="nav-link-text">Permission</span>
					</div>
				</a>
				<!-- more inner pages-->
			</li>
		</ul>
	</div>
</li>