 <div class="col-12">
  <div class="row align-items-end justify-content-between pb-5">
    <livewire:subscription-plan-item-form :subscriptionPlan="$subscriptionPlan" />
    <div class="col-3">
      <h3>{{ $titlePage }}</h3>
    </div>
    <div class="col-3">
      <input wire:model.debounce.200ms="search" class="form-control search-input search form-control-sm" type="search" placeholder="Search" aria-label="Search">
    </div>
  </div>
  <div class="table-responsive scrollbar">
    <table class="table table-hover fs--1 mb-0 ">
      <thead>
        <tr>
          <th class="sort px-3" scope="col">Code</th>
          <th class="sort px-3" scope="col">Name</th>
          <th class="sort px-3 text-end" scope="col">Price</th>
          <th class="sort text-center" scope="col">Action</th>
        </tr>
      </thead>
      <tbody class="list">
        @forelse($subscription_plan_items as $key => $subscription_plan_item)
        <tr>
          <td class="px-3">{{ $subscription_plan_item->code }}</td>
          <td class="px-3">{{ $subscription_plan_item->name }}</td>
          <td class="px-3 text-end">{{ $subscription_plan_item->price }}</td>
          <td class="text-center">
            <a class="text-danger pointer" wire:click="$emit('destroy', '{{$subscription_plan_item->id}}')"><i class="fa fa-remove"></i></a>
          </td>
        </tr>
        @empty
        <tr>
          <td colspan="4" class="text-center">Empty Data</td>
        </tr>
        @endforelse
      </tbody>
    </table>
    {{ $subscription_plan_items->links() }}
  </div>
   <div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>

@push('scripts')
	<script>
		window.addEventListener("DOMContentLoaded", function () {
			Livewire.emit("openMasterDataModule");
			Livewire.emit("openServicesMenu");
		});
	</script>
@endpush