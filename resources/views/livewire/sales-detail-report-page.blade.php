<div class="d-flex flex-column gap-3">
  <livewire:sales-detail-report-form />
  @if($showDocument)
    <livewire:sales-detail-report-table :salesDetails="$salesDetails"/>
  @endif
  
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>

@push('scripts')
  <script>
    window.addEventListener("DOMContentLoaded", function () {
      Livewire.emit("openReportMenu")
      Livewire.emit("openSalesReportModule")
    });
  </script>
@endpush