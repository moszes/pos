<div class="col card">
	<div class="loading d-none" wire:loading.class.remove="d-none"></div>
	<div class="card-body scrollbar">
		<h3 class="text-center mb-3">Sales Invoice Commission</h3>
		<table class="table table-hover">
			<thead>
				<tr>
					<th rowspan="2" class="px-3 align-middle">Date</th>
					<th rowspan="2" class="px-3 align-middle">User</th>
					<th rowspan="2" class="px-3 text-center align-middle">Role</th>
					<th rowspan="2" class="px-3 text-center align-middle">Type</th>
					<th rowspan="2" class="px-3 align-middle">Code</th>
					<th rowspan="2" class="px-3 align-middle">Name</th>
					<th colspan="2" class="px-3 text-center">Price</th>
					<th class="px-3 text-end">% Fee</th>
					<th class="px-3 text-end">Fee</th>
				</tr>
				<tr>
					<th class="px-3 text-center">Normal</th>
					<th class="px-3 text-center">Agent</th>
				</tr>
			</thead>
			<tbody>
			@foreach($salesInvoice->items as $item)
				@forelse($item->commissions as $commission)
				<tr>
					<td class="px-3 text-center">{{ $commission->type }}</td>
					<td class="px-3 text-center">{{ $commission->type }}</td>
					<td class="px-3">{{ getUserById($commission->user_id)->name }}</td>
					<td class="px-3">{{ $commission->code }}</td>
					<td class="px-3">{{ $commission->name }}</td>
					<td class="px-3 text-center">{{ $commission->role }}</td>
					<td class="px-3 text-end">{{ priceFormat($commission->fee) }}</td>
				</tr>
				@empty
				<tr>
					<td colspan="7" class="text-center">Empty Data</td>
				</tr>
				@endforelse
			@endforeach
			</tbody>
		</table>
	</div>
</div>