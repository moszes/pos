<div class="d-flex flex-column gap-2">
  <h5 class="text-center mb-3">Material Usages</h5>
  <table class="table table-hover fs--1">
    <thead>
      <tr>
        <th class="sort ps-2" scope="col">Code</th>
        <th class="sort" scope="col">Name</th>
        <th class="sort text-center" scope="col">Size</th>
        <th class="sort text-center" scope="col">Usage</th>
        <th class="sort text-center" scope="col">Unit</th>
        <th class="sort text-end" scope="col">Normal Price</th>
        <th class="sort text-end" scope="col">Agent Price</th>
        <th class="sort text-end pe-2" scope="col">Cost</th>
      </tr>
    </thead>
    <tbody class="list">
      @php($grandTotal = 0)
      @forelse($treatmentMaterialUsages as $materialUsage)
      <tr>
        <td class="ps-2">{{ @getProductById($materialUsage['product_id'])->code }}</td>
        <td >{{ @getProductById($materialUsage['product_id'])->name }}</td>
        <td class="text-center">{{ @$materialUsage['size'] }}</td>
        <td class="text-center">{{ @$materialUsage['usage'] }}</td>
        <td class="text-center">{{ @$materialUsage['unit_name'] }}</td>
        <td class="text-end">{{ numberFormat(@$materialUsage['normal_price']) }}</td>
        <td class="text-end">{{ numberFormat(@$materialUsage['agent_price']) }}</td>
        <td class="text-end pe-2">{{ numberFormat(@$materialUsage['cost']) }}</td>
        @php($grandTotal += @$materialUsage['cost'])
      </tr>
      @empty
      <tr>
        <td colspan="8" class="text-center">Empty Data</td>
      </tr>
      @endforelse
      <tr>
        <td class="text-end pe-2" colspan="7">Grand Total</td>
        <td class="text-end pe-2">{{ numberFormat($grandTotal) }}</td>
      </tr>
    </tbody>
  </table>
</div>