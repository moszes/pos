<div class="shadow rounded">
  <livewire:breadcrumb />
  <div class="card">
    <div class="card-body">
      <livewire:category-table />
    </div>
  </div>
</div>

@push('scripts')
  <script>
    window.addEventListener("DOMContentLoaded", function () {
      Livewire.emit("openMasterDataModule");
      Livewire.emit("openBaseDataMenu")
    });
  </script>
@endpush