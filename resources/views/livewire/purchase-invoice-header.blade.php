<div class="d-flex flex-column">
  <div class="d-flex gap-3">
    <div class="col mb-3">
      <label class="form-label" for="purchaseInvoiceNumber">Invoice Number</label>
      <input wire:model.debounce.200ms="purchaseInvoiceNumber" id="purchaseInvoiceNumber" class="form-control" type="text">
      @error('invoice_number') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <livewire:vendor-search />
    <div class="col mb-3">
      <label class="form-label" for="date">Date</label>
      <input wire:model.debounce.200ms="date" id="date" class="form-control" type="date">
      @error('date') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
  </div>
  <div class="d-flex gap-3">
    <div class="col mb-3">
      <label class="form-label" for="discount">Discount</label>
      <input wire:model.debounce.200ms="discount" id="discount" class="form-control" type="number">
      @error('discount') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="col mb-3">
      <label class="form-label" for="others">Others</label>
      <input wire:model.debounce.200ms="others" id="others" class="form-control" type="number">
      @error('others') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="col mb-3">
      <label class="form-label" for="tax">Tax</label>
      <input wire:model.debounce.200ms="tax" id="tax" class="form-control" type="number">
      @error('tax') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
  </div>
  <div class="d-flex gap-3">
    <div class="col mb-3">
      <label class="form-label" for="note">Note</label>
      <textarea wire:model.debounce.200ms="note" class="form-control form-icon-input" id="note" style="height: 115px;"></textarea>
      @error('note') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
  </div>
</div>
