<div class="mb-3 col">
	<label class="form-label" for="categoryId">Category 1</label>
	<select wire:model.debounce.200ms="categoryId" class="form-select" id="categoryId" aria-label="Select category">
		<option selected>-- Choose --</option>
		{!! getCategoryOptions() !!}
	</select>
	@error('categoryId') <span class="text-danger">{{ $message }}</span> @enderror
	
	<div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>