<div class="col-3">
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
  <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#purchaseOrderForm">+</button>
  <div wire:ignore.self class="modal modal-xl fade" id="purchaseOrderForm" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5><button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close">
            <span class="fas fa-times fs--1"></span>
          </button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div class="d-flex flex-column">
              <div class="d-flex gap-3">
                <div class="col mb-3">
                  <label class="form-label" for="purchaseOrderNumber">Order Number</label>
                  <input wire:model.debounce.100ms="purchaseOrderNumber" id="purchaseOrderNumber" class="form-control" type="text">
                  @error('purchaseOrderNumber') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <livewire:vendor-search />
                <div class="col mb-3">
                  <label class="form-label" for="date">date</label>
                  <input wire:model.debounce.100ms="date" id="date" class="form-control" type="date">
                  @error('date') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
              </div>
              <div class="d-flex gap-3">
                <div class="col mb-3">
                  <label class="form-label" for="discount">Discount</label>
                  <input wire:model.debounce.100ms="discount" id="discount" class="form-control" type="number">
                  @error('discount') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="col mb-3">
                  <label class="form-label" for="others">Others</label>
                  <input wire:model.debounce.100ms="others" id="others" class="form-control" type="number">
                  @error('others') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="col mb-3">
                  <label class="form-label" for="tax">Tax</label>
                  <input wire:model.debounce.100ms="tax" id="tax" class="form-control" type="number">
                  @error('tax') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
              </div>
              <div class="d-flex gap-3">
                <div class="col mb-3">
                  <label class="form-label" for="note">Note</label>
                  <textarea wire:model.debounce.100ms="note" class="form-control form-icon-input" id="note" style="height: 115px;"></textarea>
                  @error('note') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
              </div>
            </div>
            <div class="d-flex flex-column mb-3">
              <livewire:product-search />

              @if(!empty($items))
              <table class="table table-hover table-bordered table-responsive mt-3">
                <thead>
                  <tr>
                    <th class="px-3 text-center"></th>
                    <th class="px-3">Code</th>
                    <th class="px-3">Name</th>
                    <th class="px-3 text-center">Qty</th>
                    <th class="px-3 text-center">Unit</th>
                    <th class="px-3 text-end">Price</th>
                    <th class="px-3 text-end">Last Price</th>
                    <th class="px-3 text-end">Disc. Item</th>
                    <th class="px-3 text-end">Total</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse($items as $key => $item)
                  <tr>
                    <td class="px-3 text-center"><a href="#" wire:click="removePurchaseOrderItem({{$key}})"><i class="fas fa-trash"></i></a></td>
                    <td class="px-3 text-center">{{ $item['itemCode'] }}</td>
                    <td class="px-3 text-center">{{ $item['itemName'] }}</td>
                    <td class="px-3 text-center">{{ $item['qty'] }}</td>
                    <td class="px-3 text-center">{{ $item['unit'] }}</td>
                    <td class="px-3 text-end">{{ $item['price'] }}</td>
                    <td class="px-3 text-end">{{ $item['last_price'] }}</td>
                    <td class="px-3 text-end">{{ $item['discount_item'] }}</td>
                    <td class="px-3 text-end">{{ $item['total_price'] }}</td>
                  </tr>
                  @empty
                  <tr>
                    <td colspan="9" class="text-center">Empty Data</td>
                  </tr>
                  @endforelse
                  <tr>
                    <td colspan="8" class="text-end px-3">Sub Total</td>
                    <td class="text-end px-3">{{ $subTotal }}</td>
                  </tr>
                  <tr>
                    <td colspan="8" class="text-end px-3">Tax</td>
                    <td class="text-end px-3">{{ $tax }}</td>
                  </tr>
                  <tr>
                    <td colspan="8" class="text-end px-3">Others</td>
                    <td class="text-end px-3">{{ $others }}</td>
                  </tr>
                  <tr>
                    <td colspan="8" class="text-end px-3">Discount</td>
                    <td class="text-end px-3">- {{ $discount }}</td>
                  </tr>
                  <tr>
                    <td colspan="8" class="text-end px-3">Grand Total</td>
                    <td class="text-end px-3">{{ $grandTotal }}</td>
                  </tr>
                </tbody>
              </table>
              @endif
            </div>
            <div class="float-end">
              <button type="button" wire:click="$emit('clearPurchaseOrderForm')" class="btn btn-light">Clear</button>
              <button wire:loading.attr="disabled" type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>
  let purchaseOrderForm = new bootstrap.Modal(
    document.getElementById('purchaseOrderForm'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showPurchaseOrderForm', () => {
    purchaseOrderForm.show();
  })
  Livewire.on('hidePurchaseOrderForm', () => {
    purchaseOrderForm.hide();
  })
  document.getElementById('purchaseOrderForm').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearPurchaseOrderForm');
  });
</script>
@endpush
