<div wire:ignore.self class="modal modal-xl fade" id="marketPlaceDetail" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ $formTitle }}</h5>
        <button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close">
          <span class="fas fa-times fs--1"></span>
        </button>
      </div>
      <div class="modal-body d-flex flex-column gap-4">
        @if($marketplace)
        <livewire:market-place-detail-info :marketplace="$marketplace" />
        <livewire:market-place-detail-material-usage :marketplace="$marketplace" />
        <livewire:market-place-history-table :marketplace="$marketplace" />
        @endif
      </div>
    </div>
  </div>
</div>


@push('scripts')
<script>
  let marketPlaceDetail = new bootstrap.Modal(
    document.getElementById('marketPlaceDetail'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showMarketPlaceDetail', () => {
    marketPlaceDetail.show();
  })
  Livewire.on('hideMarketPlaceDetail', () => {
    marketPlaceDetail.hide();
  })
  document.getElementById('marketPlaceDetail').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearMarketPlaceItems')
  })

</script>
@endpush