<div class="d-flex flex-column gap-4">
  <div class="row">
    <div class="col">
      <h4>{{ $titlePage }}</h4>
    </div>
    <div class="col-3 d-flex gap-2">
      <input
        wire:model.debounce.200ms="search"
        class="form-control search-input search form-control-sm"
        type="search"
        placeholder="Search"
        aria-label="Search"
      >
      <livewire:turn-over-form/>
    </div>
  </div>
  <table class="table table-hover fs--1 mb-0 ">
    <thead>
    <tr>
      <th class="sort px-3" scope="col">Month</th>
      <th class="sort px-3" scope="col">Amount</th>
      <th class="sort text-end pe-2" scope="col">Action</th>
    </tr>
    </thead>
    <tbody class="list">
    @forelse($turnOvers as $key => $turnOver)
      <tr>
        <td class="px-3">{{ $turnOver->month }}</td>
        <td class="px-3">{{ $turnOver->amount }}</td>
        <td class="text-end pe-2">
          <a class="me-2 pointer" wire:click="$emit('editTurnOver', '{{$turnOver->id}}')">
            <i class="fa fa-pencil"></i>
          </a>
          <a class="text-danger pointer" wire:click="$emit('deleteTurnOver', '{{$turnOver->id}}')">
            <i class="fa fa-remove"></i>
          </a>
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="3" class="text-center">Empty Data</td>
      </tr>
    @endforelse
    </tbody>
  </table>
  {{ $turnOvers->links() }}
</div>

@push('scripts')
  <script>
    Livewire.on('deleteTurnOver', function(id) {
      Swal.fire({
        title: 'Are you sure?',
        text: 'You will not be able to revert this!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then((result) => {
        if (result.isConfirmed) {
          Livewire.emit('destroy', id);
        }
      });
    });
  </script>
@endpush