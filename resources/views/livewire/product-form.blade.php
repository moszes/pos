<div>
	<button class="btn btn-sm btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#productForm">+</button>
	<div wire:ignore.self class="modal modal-xl fade" id="productForm" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ $formTitle }}</h5>
					<button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close">
						<span class="fas fa-times fs--1"></span>
					</button>
				</div>
				<div class="modal-body">
					<form wire:submit.prevent="{{ $actionForm }}">
            <div class="d-flex gap-3">
              <livewire:category-option :type="$type"/>
              <livewire:sub-category-option />
            </div>
						<div class="d-flex gap-3">
							<div class="mb-3 col">
								<label class="form-label" for="productCode">Code</label>
								<input wire:model.debounce.200ms="code" id="productCode" class="form-control" type="text">
								@error('code') <span class="text-danger">{{ $message }}</span> @enderror
							</div>
							<div class="mb-3 col">
								<label class="form-label" for="productName">Name</label>
								<input wire:model.debounce.200ms="name" id="productName" class="form-control" type="text">
								@error('name') <span class="text-danger">{{ $message }}</span> @enderror
							</div>
						</div>
						<div class="d-flex gap-3">
							<div class="mb-3 col">
								<label class="form-label" for="size">Size</label>
								<input wire:model.debounce.200ms="size" id="size" class="form-control" type="number">
								@error('size') <span class="text-danger">{{ $message }}</span> @enderror
							</div>
							<div class="mb-3 col">
								<label class="form-label" for="unitOfMeasureId">Unit</label>
								<select wire:model.debounce.200ms="unitOfMeasureId" class="form-select" id="unitOfMeasureId" aria-label="Select category">
									<option selected>-- Choose --</option>
									{!! getUnitOfMeasureOptions() !!}
								</select>
								@error('unitOfMeasureId') <span class="text-danger">{{ $message }}</span> @enderror
							</div>
							<div class="mb-3 col">
								<label class="form-label" for="cost">Cost</label>
								<input wire:model.debounce.200ms="cost" id="cost" class="form-control" type="number">
								@error('cost') <span class="text-danger">{{ $message }}</span> @enderror
							</div>
							<div class="mb-3 col">
								<label class="form-label" for="agentPrice">Agent Price</label>
								<input wire:model.debounce.200ms="agentPrice" id="agentPrice" class="form-control" type="number">
								@error('agentPrice') <span class="text-danger">{{ $message }}</span> @enderror
							</div>
							<div class="mb-3 col">
								<label class="form-label" for="normalPrice">Normal Price</label>
								<input wire:model.debounce.200ms="normalPrice" id="normalPrice" class="form-control" type="number">
								@error('normalPrice') <span class="text-danger">{{ $message }}</span> @enderror
							</div>
						</div>
						<div class="float-end">
							<button type="button" wire:click="$emit('clearProductForm')" class="btn btn-light">Clear</button>
							<button wire:loading.attr="disabled" type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@push('scripts')
	<script>
		
	  let productForm = new bootstrap.Modal(
		  document.getElementById('productForm'),
		  {
			  backdrop: 'static',
		  }
	  )
	  Livewire.on('showProductForm', () => {
		  productForm.show();
	  })
	  Livewire.on('hideProductForm', () => {
		  productForm.hide();
	  })
	  document.getElementById('productForm').addEventListener('hidden.bs.modal', () => {
		  Livewire.emit('clearProductForm');
	  })
	</script>
@endpush