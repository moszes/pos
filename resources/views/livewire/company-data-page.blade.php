<div class="container py-5 d-flex flex-column gap-3">
	<div class="d-flex justify-content-center">
		<h3>Company</h3>
	</div>
	<div class="d-flex justify-content-center">
		<livewire:company-detail-form />
	</div>
</div>