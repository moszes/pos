<div>
  <nav class="mb-2" aria-label="breadcrumb">
    <ol class="breadcrumb mb-0">
      <li class="breadcrumb-item"><a href="#">Pages</a></li>
      <li class="breadcrumb-item active">Faq</li>
    </ol>
  </nav>
  <h2 class="mb-5">FAQ</h2>
  <h5 class="mb-3">Ada yang bisa dibantu?</h5>
  <p class="text-700">
    Cari topic yang anda butuhkan atau <a href="#">Kontak Kami</a>
  </p>
  <div class="search-box mb-8 w-100" style="max-width:25rem;">
    <form
      class="position-relative"
      data-bs-toggle="search"
      data-bs-display="static"
    >
      <input
        class="form-control search-input search"
        type="search"
        placeholder="Search"
        aria-label="Search"
      >
      <span class="fas fa-search search-box-icon"></span>
    </form>
  </div>
  <div class="accordion">
    <div class="accordion-item border-top border-300">
      <h2 class="accordion-header" id="headingOne">
        <button
          class="accordion-button"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#collapseOne"
          aria-expanded="false"
          aria-controls="collapseOne"
        >
          Apakah yang dimaksud dengan Master Data?
        </button>
      </h2>
      <div
        class="accordion-collapse collapse show"
        id="collapseOne"
        aria-labelledby="headingOne"
        data-bs-parent="#accordionExample"
      >
        <div class="accordion-body pt-0">
          <p>
            Master data adalah kumpulan data inti yang penting
            dan diperlukan untuk operasi sehari-hari suatu organisasi.
            Ini adalah data yang digunakan sebagai referensi utama untuk
            menjalankan bisnis dan mengelola proses bisnis yang lebih luas.
            Master data sering kali mencakup informasi seperti pelanggan,
            produk, mitra bisnis, karyawan, lokasi, dan lainnya
            yang dianggap kritis untuk keberhasilan operasional organisasi.
          </p>
          <p>
            Master data biasanya berbeda dari data transaksional,
            yang merupakan data yang dihasilkan dari aktivitas
            bisnis sehari-hari seperti penjualan, pembelian,
            atau pemrosesan transaksi lainnya. Data transaksional
            seringkali bergantung pada master data
            untuk memberikan konteks dan referensi yang diperlukan.
          </p>
          <p>
            Manajemen master data melibatkan pengumpulan, pemeliharaan,
            dan penggunaan data master dengan cara yang konsisten dan akurat
            di seluruh organisasi. Tujuan utama dari manajemen master data
            adalah untuk memastikan kualitas data yang baik, integritas,
            dan konsistensi, sehingga dapat digunakan
            untuk pengambilan keputusan yang akurat dan efektif.
          </p>
        </div>
      </div>
    </div>
    <div class="accordion-item">
      <h2 class="accordion-header" id="headingTwo">
        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseOne">
          Apakah yang dimaksud dengan Transaksi Data?
        </button>
      </h2>
      <div class="accordion-collapse collapse" id="collapseTwo" aria-labelledby="headingTwo" data-bs-parent="#accordionExample" style="">
        <div class="accordion-body pt-0">
          <p>Transaksi data merujuk pada proses atau kejadian yang menghasilkan perubahan pada data. Ini melibatkan tindakan atau peristiwa yang mencatat, memperbarui, atau menghapus informasi dalam basis data atau sistem informasi. Transaksi data biasanya terjadi dalam konteks operasional bisnis, seperti penjualan, pembelian, pemesanan, pembayaran, pengiriman, atau aktivitas bisnis lainnya.</p>
          <p>Transaksi data dapat melibatkan entitas bisnis seperti pelanggan, produk, pemasok, karyawan, atau entitas lainnya yang terkait dengan operasi bisnis. Misalnya, saat seorang pelanggan melakukan pembelian, transaksi data akan mencatat detail pembelian tersebut, seperti barang yang dibeli, jumlah, harga, metode pembayaran, dan lain-lain. Data transaksi ini nantinya dapat digunakan untuk melacak dan menganalisis aktivitas bisnis, membuat laporan keuangan, memperbarui inventaris, mengelola siklus hidup pelanggan, dan sebagainya.</p>
          <p>Transaksi data biasanya memiliki beberapa atribut penting, seperti tanggal dan waktu transaksi, identitas pihak yang terlibat, jumlah atau nilai transaksi, dan informasi terkait lainnya yang relevan. Pentingnya data transaksi adalah untuk mempertahankan catatan yang akurat tentang kegiatan bisnis dan memastikan bahwa informasi tersebut dapat digunakan untuk pelaporan, audit, analisis, dan pengambilan keputusan bisnis yang tepat.</p>
          <p>Dalam sistem informasi atau basis data, transaksi data sering kali dikelola melalui operasi yang disebut "transaksi" yang memastikan integritas data, konsistensi, dan keandalan. Contoh operasi transaksi termasuk "membaca" data dari basis data, "menulis" data baru ke dalam basis data, atau "memperbarui" data yang ada dengan nilai baru. Operasi transaksi juga mencakup pemrosesan yang aman dan terlindungi, sehingga data yang dihasilkan dan dimodifikasi dapat diandalkan dan tidak rusak.</p>
        </div>
      </div>
    </div>
    <div class="accordion-item">
      <h2 class="accordion-header" id="headingThree">
        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseOne">
          Hubungan Master Data dan Transaksi Data
        </button>
      </h2>
      <div class="accordion-collapse collapse" id="collapseThree" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
        <div class="accordion-body pt-0">
        </div>
      </div>
    </div>
    <div class="accordion-item">
      <h2 class="accordion-header" id="headingFour">
        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseOne">
          Pengertian Modul Dalam Aplikasi
        </button>
      </h2>
      <div class="accordion-collapse collapse" id="collapseFour" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
        <div class="accordion-body pt-0">
          <p>Sebuah bagian dari program atau aplikasi yang berfungsi sebagai unit terpisah yang dapat digunakan kembali. Modul dapat berisi fungsi, prosedur, kelas, atau objek yang dapat dipanggil oleh bagian lain dari program atau aplikasi. Dengan menggunakan modul, pengembang dapat memecah program menjadi bagian-bagian yang lebih kecil dan lebih mudah dikelola, serta mempercepat proses pengembangan dan pemeliharaan aplikasi.</p>
          <p>Modul juga dapat membantu dalam mengorganisir kode dan memudahkan pengembangan aplikasi secara tim. Selain itu, modul juga dapat digunakan untuk memperluas fungsionalitas aplikasi dengan menambahkan fitur-fitur baru tanpa harus mengubah kode yang sudah ada. Dalam beberapa bahasa pemrograman, seperti Python, modul juga dapat diimpor dari sumber eksternal atau pihak ketiga untuk menambahkan fungsionalitas yang lebih kompleks ke dalam aplikasi.</p>
          <p>Dalam aplikasi, modul juga dapat digunakan untuk memisahkan tugas-tugas yang berbeda dan memungkinkan pengembang untuk fokus pada satu bagian dari aplikasi tanpa harus khawatir tentang bagian lainnya. Misalnya, dalam sebuah aplikasi web, modul dapat digunakan untuk mengelola database, mengatur tampilan halaman web, atau mengelola interaksi pengguna dengan aplikasi. Dengan menggunakan modul, pengembang dapat mempercepat proses pengembangan dan pemeliharaan aplikasi serta meningkatkan kualitas dan keamanannya.</p>       
        </div>
      </div>
    </div>
  </div>
  <div class="text-center py-11">
    <h3 class="text-black">Apakah yang anda cari tidak ada?</h3>
    <p class="text-900">Kami siap untuk membantu anda.</p><button class="btn btn-sm btn-outline-primary btn-support-chat"><svg class="svg-inline--fa fa-comment me-2" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="comment" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M256 32C114.6 32 .0272 125.1 .0272 240c0 49.63 21.35 94.98 56.97 130.7c-12.5 50.37-54.27 95.27-54.77 95.77c-2.25 2.25-2.875 5.734-1.5 8.734C1.979 478.2 4.75 480 8 480c66.25 0 115.1-31.76 140.6-51.39C181.2 440.9 217.6 448 256 448c141.4 0 255.1-93.13 255.1-208S397.4 32 256 32z"></path></svg><!-- <span class="fas fa-comment me-2"></span> Font Awesome fontawesome.com -->Chat with us</button>
  </div>
</div>