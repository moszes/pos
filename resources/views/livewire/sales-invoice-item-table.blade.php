<div class="d-flex flex-column">
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
  <table class="table table-hover table-bordered table-responsive mt-3">
    <thead>
      <tr>
        <th class="px-3 text-center"></th>
        <th class="px-3">Item</th>
        <th class="px-3 text-center">PIC & Room</th>
        <th class="px-3 text-end">Status</th>
        <th class="px-3 text-end">Cost</th>
        <th class="px-3 text-end">Total Price</th>
      </tr>
    </thead>
    <tbody>
    @forelse($items as $key => $item)
      <tr>
        <td class="px-3 text-center">
          @if($showDetails)
          <a href="#" wire:click="removeItem({{$key}})"><i class="fas fa-trash"></i></a>
          @endif
        </td>
        <td class="px-3">
          <div class="d-flex flex-column gap-3">
            <div>
              Module: {{ @$item['module'] }}
            </div>
            <div>
              Code: {{ @$item['code'] }}
            </div>
            <div>
              Name: {{ @$item['name'] }}
            </div>
            @if($note)
            <div>
              Note: {{ @$note }}
            </div>
            @endif
          </div>
        </td>
        <td class="px-3">
          <div class="d-flex flex-column gap-3">
            <div class="d-flex flex-column gap-3">
              @if(isset($item['doctor_id']) || isset($item['doctor_assistance_id']) || isset($item['therapist_id']) || isset($item['sales_id']))
              <h4>PIC</h4>
              @endif
              @if(isset($item['doctor_id']))
              <div>
                Doctor: {{ @getUserById($item['doctor_id'])->name }}
              </div>
              @endif
              @if(isset($item['doctor_assistance_id']))
              <div>
                Doctor Assistance: {{ @getUserById($item['doctor_assistance_id'])->name }}
              </div>
              @endif
              @if(isset($item['therapist_id']))
              <div>
                Therapist: {{ @getUserById($item['therapist_id'])->name }}
              </div>
              @endif
              @if(isset($item['sales_id']))
              <div>
                Sales: {{ @getUserById($item['sales_id'])->name }}
              </div>
              @endif
            </div>
            @if(@$item['module'] === 'treatment' || @$item['module'] === 'marketplace')
            <div class="d-flex flex-column gap-3">
              @if(isset($item['room_id']) || isset($item['room_start']) || isset($item['room_end']))
              <h4>Room</h4>
              <div>
                {{ @getRoomById($item['room_id'])->name }} {{ @$item['room_start'] }} {{ @$item['room_end'] }}
              </div>
              @endif
            </div>
            @endif
          </div>
        </td>
        <td class="px-3 text-center">{{ @$item['transaction_status'] }}</td>
        <td class="px-3 text-end">
          <div class="d-flex flex-column gap-3">
            <div>
              Price: {{ @numberFormat($item['price']) }}
            </div>
            <div>
              Qty: {{ @$item['qty'] }}
            </div>
            <div>
              Discount: {{ @numberFormat($item['discount_item']) }}
            </div>
          </div>
        <td class="px-3 text-end">{{ @numberFormat($item['total_price']) }}</td>
      </tr>
    @empty
      <tr>
        <td colspan="6" class="text-center">Empty Data</td>
      </tr>
    @endforelse
      <tr>
        <td colspan="5" class="text-end px-3">Sub Total</td>
        <td class="px-3 text-end">{{ @numberFormat($subTotal) }}</td>
      </tr>
      <tr>
        <td colspan="5" class="text-end px-3">Others</td>
        <td class="px-3 text-end">{{ @numberFormat($salesInvoiceOthers) }}</td>
      </tr>
      <tr>
        <td colspan="5" class="text-end px-3">Discount</td>
        <td class="px-3 text-end">{{ @numberFormat($salesInvoiceDiscount * -1) }}</td>
      </tr>
      <tr>
        <td colspan="5" class="text-end px-3">Grand Total</td>
        <td class="px-3 text-end">{{ @numberFormat($grandTotal) }}</td>
      </tr>
    </tbody>
  </table>
</div>