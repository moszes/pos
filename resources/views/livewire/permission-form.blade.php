<div>
  <button class="btn btn-sm btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#permissionForm">+</button>
  <div wire:ignore.self class="modal fade" id="permissionForm" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5>
          <button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close">
            <span class="fas fa-times fs--1"></span>
          </button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div>
              @if (session()->has('message'))
                <div class="alert alert-danger">
                    {{ session('message') }}
                </div>
              @endif
            </div>
            <div class="mb-3">
              <label class="form-label" for="name">Name</label>
              <input wire:model.lazy="name" id="name" class="form-control" type="text">
              @error('name') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="float-end">
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>

	window.addEventListener("DOMContentLoaded", function () {
		Livewire.emit("openMasterDataModule");
		Livewire.emit("openStakeholdersMenu")
	});

	let permissionForm = new bootstrap.Modal(
    document.getElementById('permissionForm'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showPermissionForm', () => {
    permissionForm.show();
  })
  Livewire.on('hidePermissionForm', () => {
    permissionForm.hide();
  })
  document.getElementById('permissionForm').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearPermissionForm');
  });
</script>
@endpush