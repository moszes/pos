<div>
	<input class="d-none" wire:model="customerAvatarImage" id="customerAvatarImage" type="file">
	<label class="avatar avatar-4xl status-online feed-avatar-profile cursor-pointer" for="customerAvatarImage">
		<img class="rounded-circle img-thumbnail bg-white shadow-sm" src="{{ $customerAvatar ?? Avatar::create('dummy')->toBase64() }}" width="200" alt="">
	</label>
	<div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>