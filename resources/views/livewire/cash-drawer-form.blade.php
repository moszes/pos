<div>
  <div wire:ignore.self class="modal modal-xl fade" id="cashDrawerModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5><button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span class="fas fa-times fs--1"></span></button>
        </div>
        <div class="modal-body d-flex flex-column gap-3">
          <div class="d-flex gap-3">
            <div class="col">
              <label class="form-label" for="cash">Cash</label>
              <input wire:model.debounce.500ms="cash" id="cash" class="form-control" type="number">
              @if($cash > 0)
              <div class="text-success pt-1 ps-3">{{ numberFormat($cash) }}</div>
              @endif
              @error('cash') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="col">
              <label class="form-label" for="creditDebit">Credit Debit</label>
              <input wire:model.debounce.500ms="creditDebit" id="creditDebit" class="form-control" type="number">
              @if($creditDebit > 0)
              <div class="text-success pt-1 ps-3">{{ numberFormat($creditDebit) }}</div>
              @endif
              @error('creditDebit') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="col">
              <label class="form-label" for="custom">Custom</label>
              <input wire:model.debounce.500ms="custom" id="custom" class="form-control" type="number">
              @if($custom > 0)
                <div class="text-success pt-1 ps-3">{{ numberFormat($custom) }}</div>
              @endif
              @error('custom') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="col">
              <label class="form-label" for="prepaid">Prepaid</label>
              <input wire:model.debounce.500ms="prepaid" id="prepaid" class="form-control" type="number">
              @if($prepaid > 0)
                <div class="text-success pt-1 ps-3">{{ numberFormat($prepaid) }}</div>
              @endif
              @error('prepaid') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="col">
              <label class="form-label" for="qrcode">QR Code</label>
              <input wire:model.debounce.500ms="qrcode" id="qrcode" class="form-control" type="number">
              @if($qrcode > 0)
                <div class="text-success pt-1 ps-3">{{ numberFormat($qrcode) }}</div>
              @endif
              @error('qrcode') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="col">
              <label class="form-label" for="voucher">Voucher</label>
              <input wire:model.debounce.500ms="voucher" id="voucher" class="form-control" type="number">
              @if($voucher > 0)
                <div class="text-success pt-1 ps-3">{{ numberFormat($voucher) }}</div>
              @endif
              @error('voucher') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
          </div>
          <div class="d-flex gap-3">
            <h3>Grand Total: {{ numberFormat($grandTotal) }}</h3>
          </div>
          <div class="d-flex justify-content-end">
            <button type="button" class="btn btn-primary " wire:click="store">
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="d-flex justify-content-end gap-3">
    @if(auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales') && $closedCashDrawer)
    <button class="btn btn-success" wire:click="$emit('openCashDrawer', '{{ date('Y-m-d') }}')" type="button">Open Today Close</button>
    @else
    <button class="btn btn-primary"  type="button" data-bs-toggle="modal" data-bs-target="#cashDrawerModal">Close Today Sale</button>
    @endif
  </div>
</div>


@push('scripts')
  <script>
    let cashDrawerModal = new bootstrap.Modal(
      document.getElementById('cashDrawerModal'),
      {
        backdrop: 'static',
      }
    )
    Livewire.on('invoiceNotFound', () => {
      Swal.fire({
        title: 'Not Invoice Found!',
        text: 'Please create an invoice',
        icon: 'error',
        confirmButtonText: 'Ok'
      })
    })
    Livewire.on('needMorePayment', () => {
      Swal.fire({
        title: 'Need More Payment!',
        text: 'Please add more payment',
        icon: 'error',
        confirmButtonText: 'Ok'
      })
    })
    Livewire.on('siClosedAlready', () => {
      Swal.fire({
        title: 'closed!',
        text: 'Today Sales Closed Already',
        icon: 'error',
        confirmButtonText: 'Ok'
      })
    })
    Livewire.on('openCashDrawer', () => {
        Swal.fire({
          title: 'Are you sure?',
          text: 'You will not be able to revert this!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, Open it!',
          cancelButtonText: 'No, cancel!',
        }).then((result) => {
          if (result.isConfirmed) {
            Livewire.emit('openTodaySale', '{{ date('Y-m-d') }}');
          }
        });
    })
    document.getElementById('cashDrawerModal').addEventListener('hidden.bs.modal', () => {
      Livewire.emit('clearSalesInvoiceItems')
    })
  </script>
@endpush