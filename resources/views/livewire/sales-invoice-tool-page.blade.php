<div class="d-flex flex-column gap-3">
  <livewire:search-by-sales-invoice-number :salesInvoiceNumber="$salesInvoiceNumber"/>
  
  @if($salesInvoiceWidget)
  <livewire:sales-invoice-header :salesInvoice="$salesInvoice" />
  <livewire:sales-invoice-payment :salesInvoice="$salesInvoice" />
  <livewire:sales-invoice-item :salesInvoice="$salesInvoice" />
  <livewire:sales-invoice-transaction :salesInvoice="$salesInvoice" />
  <livewire:sales-invoice-commission :salesInvoice="$salesInvoice" />
  @endif
  
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>

@push('scripts')
  <script>
    window.addEventListener("DOMContentLoaded", function () {
      Livewire.emit("openToolModule");
    });
  </script>
@endpush