<div class="row">
	<div class="d-flex flex-column gap-4 fs--0 mb-0">
		@if($moduleLabel)
		<h4>{{ ucfirst($moduleLabel) }}</h4>
		@endif
		<div class="d-flex flex-column gap-2">
			<div class="d-flex justify-content-between gap-2">
				<p class="w-30 fs--0 mb-0">Code</p>
				@if($redirectRoute)
				<a class="fs--0 mb-0 fw-bold" href="{{ $redirectRoute }}">{{ @$item->code }}</a>
				@else
				<p class="fs--0 mb-0 fw-bold">{{ @$item->code }}</p>
				@endif
			</div>
			<div class="d-flex justify-content-between gap-2">
				<p class="w-30 fs--0 mb-0">Name</p>
				<p class="fs--0 mb-0 text-end">{{ @$item->name }}</p>
			</div>
			@if($item->category)
			<div class="d-flex justify-content-between gap-2">
				<p class="w-30 fs--0 mb-0">Category</p>
				<p class="fs--0 mb-0">{{ $item->category->name }}</p>
			</div>
			@endif
			@if($item->sub_category)
			<div class="d-flex justify-content-between gap-2">
				<p class="w-30 fs--0 mb-0">Sub Category</p>
				<p class="fs--0 mb-0">{{ $item->sub_category->name }}</p>
			</div>
			@endif
			@if($item->size)
				<div class="d-flex justify-content-between gap-2">
					<p class="w-30 fs--0 mb-0">Size</p>
					<p class="text-end fs--0 mb-0">{{ @countFormat($item->size) }}</p>
				</div>
			@endif
			@if($item->cost)
				<div class="d-flex justify-content-between gap-2">
					<p class="w-30 fs--0 mb-0">Cost</p>
					<p class="text-end fs--0 mb-0">{{ @priceFormat($item->cost) }}</p>
				</div>
			@endif
			@if($priceLabel)
				@if($item->price)
				<div class="d-flex justify-content-between gap-2">
					<p class="w-30 fs--0 mb-0">Price</p>
					<p class="text-end fs--0 mb-0">{{ @priceFormat($item->price) }}</p>
				</div>
				@endif
				@if($item->agent_price)
				<div class="d-flex justify-content-between gap-2">
					<p class="w-30 fs--0 mb-0">Agent Price</p>
					<p class="text-end fs--0 mb-0">{{ @priceFormat($item->agent_price) }}</p>
				</div>
				@endif
				@if($item->normal_price)
				<div class="d-flex justify-content-between gap-2">
					<p class="w-30 fs--0 mb-0">Normal Price</p>
					<p class="text-end fs--0 mb-0">{{ @priceFormat($item->normal_price) }}</p>
				</div>
				@endif
			@endif
		</div>
	</div>
</div>