<div class="shadow rounded">
	<livewire:breadcrumb />
	<div class="card">
		<div class="card-body">
			<livewire:subscription-plan-table />
		</div>
	</div>
</div>