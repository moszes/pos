<div class="d-flex flex-column gap-4">
  <div class="row">
    <div class="col">
      <h4>{{ $titlePage }}</h4>
    </div>
    <div class="col-4 d-flex gap-2">
      <div class="pt-1">
        {{ @$userSelected->name }}
      </div>
      <div>
        <input type="text" wire:model="userSearch" class="form-control form-control-sm" placeholder="Search Employee...">
        @if($searchingUser)
          <div class="pb-1">
            @forelse($users as $user)
              <div class="border-bottom border-start border-end px-2 py-2">
                <p wire:click="userClicked('{{ $user->id }}')" class="mb-0">{{ $user->name }}</p>
              </div>
            @empty
              <div class="p-3 border-bottom border-start border-end">Empty Data</div>
            @endforelse
          </div>
        @endif
      </div>
    </div>
    <div class="col-3 d-flex gap-2 justify-content-end">
      <div class="d-flex gap-2">
        <input wire:model.debounce.200ms="search"
                   class="form-control search-input search form-control-sm"
                   type="search"
                   placeholder="Search"
                   aria-label="Search"
        >
        <livewire:permission-form/>
      </div>
    </div>
  </div>
  <table class="table table-hover fs--1 mb-0 ">
    <thead>
    <tr>
      <th class="sort px-3" scope="col">Permission</th>
      <th class="sort text-end pe-2" scope="col">Action</th>
    </tr>
    </thead>
    <tbody class="list">
    @forelse($permissions as $key => $permission)
      <tr>
        <td class="px-3">
          <div class="form-check">
            <input
              type="checkbox"
              class="form-check-input"
              value="{{ $permission->id }}"
              wire:model="selectedPermissions.'{{$permission->id}}'"
              id="permission_{{ $permission->id }}"
            >
            <label for="permission_{{ $permission->id }}">{{ $permission->name }}</label>
          </div>
        </td>
        <td class="text-end pe-2">
          <a class="me-2 pointer"
             wire:click="$emit('editPermission', '{{$permission->id}}')">
            <i class="fa fa-pencil"></i>
          </a>
          <a class="text-danger pointer"
             wire:click="$emit('deletePermission', '{{$permission->id}}')">
            <i class="fa fa-remove"></i>
          </a>
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="5" class="text-center">Empty Data</td>
      </tr>
    @endforelse
    </tbody>
  </table>
  {{ $permissions->links() }}
</div>


@push('scripts')
<script>
  Livewire.on('userNotSelected', () => {
    alert('Please Select a User')
  })
  
  Livewire.on('deletePermission', (id) => {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        Livewire.emit('destroy', id)
      }
    })
  })
</script>
@endpush