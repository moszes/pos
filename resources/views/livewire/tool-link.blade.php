<div class="nav-item-wrapper">
  <a class="nav-link dropdown-indicator label-1" id="tool-btn" href="#nv-tool" role="button" data-bs-toggle="collapse" aria-expanded="false" aria-controls="nv-tool">
    <div class="d-flex align-items-center">
      <span class="nav-link-icon">
        <span data-feather="package"></span>
      </span>
      <span class="nav-link-text">Tool</span>
    </div>
  </a>
  <div class="parent-wrapper label-1">
    <ul class="nav collapse parent" data-bs-parent="#navbarVerticalCollapse" id="nv-tool">
      <li class="collapsed-nav-item-title d-none">Tools</li>
      @if( auth()->user()?->can('see tool adjustment redemption link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales') )
      <li class="nav-item">
        <a class="nav-link" href="{{ route('tools.adjustRedemption') }}" aria-expanded="false">
          <div class="d-flex align-items-center">
            <span class="nav-link-text">Adjust Redemption</span>
          </div>
        </a>
        <!-- more inner pages-->
      </li>
      @endif
      <li class="nav-item">
        <a class="nav-link @if(Route::currentRouteName() == 'tools.salesInvoiceTool') active @endif" onclick="Livewire.emit('openToolModule')" href="{{ route('tools.salesInvoiceTool') }}" aria-expanded="false">
          <div class="d-flex align-items-center">
            <span class="nav-link-text">Sales Invoice Tools</span>
          </div>
        </a>
        <!-- more inner pages-->
      </li>
    </ul>
  </div>
</div>


@push('scripts')
  <script>
    Livewire.on('openToolModule', function(){
      document.getElementById('tool-btn').click();
    })
  </script>
@endpush