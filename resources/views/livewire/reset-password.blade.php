<div class="container mx-auto card p-5">
  <div class="text-danger mb-3">
    Being accountable with your account is essential because everything you do is recorded. <span class="text-warning">Please update to your Personal Password.</span>
  </div>
  <div class="mb-3">
    <label class="form-label" for="password">Password</label>
    <input wire:model.lazy="password" class="form-control" id="password" type="password" placeholder="Password..." />
    @error('password') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="mb-3">
    <label class="form-label" for="password_confirmation">Password Confirmation</label>
    <input wire:model.lazy="password_confirmation" class="form-control" id="password_confirmation" type="password" placeholder="Password Confirmation..." />
  </div>
  <button wire:click="updatePassword" class="btn btn-primary">Submit</button>
</div>
