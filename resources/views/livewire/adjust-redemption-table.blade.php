<div class="col-12">
  <div class="row align-items-end justify-content-between pb-5">
    <livewire:adjust-redemption-form />
      <div class="col-3">
        <h3>{{ $titlePage }}</h3>
      </div>
      <div class="col-3">
        <input wire:model.debounce.200ms="search" class="form-control search-input search form-control-sm" type="search" placeholder="Search" aria-label="Search">
      </div>
  </div>
  <div class="table-responsive scrollbar">
    <table class="table table-hover fs--1 mb-0 ">
      <thead>
      <tr>
        <th class="sort px-3" scope="col">Customer</th>
        <th class="sort px-3" scope="col">Phone</th>
        <th class="sort px-3" scope="col">Treatment Code</th>
        <th class="sort px-3" scope="col">Service Name</th>
        <th class="sort px-3" scope="col">Qty</th>
        <th class="sort px-3" scope="col">Note</th>
        <th class="sort text-center px-2" scope="col">Action</th>
      </tr>
      </thead>
      <tbody class="list">
        @forelse($adjust_redemptions as $key => $adjust_redemption)
        <tr>
          <td class="px-3">
            <a href="{{ route('customer.data_redemption', $adjust_redemption->customer_id) }}">{{ getUserById($adjust_redemption->customer_id)->details->first_name }}</a>
          </td>
          <td class="px-3">{{ getUserById($adjust_redemption->customer_id)->details->phone }}</td>
          <td class="px-3">{{ getTreatmentById($adjust_redemption->treatment_id)->code }}</td>
          <td class="px-3">{{ getTreatmentById($adjust_redemption->treatment_id)->name }}</td>
          <td class="px-3">{{ numberFormat($adjust_redemption->qty) }}</td>
          <td class="px-3">{{ $adjust_redemption->note }}</td>
          <td class="text-center px-2">
            <a class="me-2 pointer" wire:click="$emit('editAdjustRedemption', '{{$adjust_redemption->id}}')"><i class="fa fa-pencil"></i></a>
            <a class="text-danger pointer" wire:click="$emit('destroy', '{{$adjust_redemption->id}}')"><i class="fa fa-remove"></i></a>
          </td>
        </tr>
        @empty
        <tr>
          <td colspan="7" class="text-center">Empty Data</td>
        </tr>
        @endforelse
      </tbody>
    </table>
    {{ $adjust_redemptions->links() }}
  </div>
</div>