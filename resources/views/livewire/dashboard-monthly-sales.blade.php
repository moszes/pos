<div class="d-flex flex-column gap-3 text-end pe-3">
	<h4>Monthly Sales</h4>
	<h4 class="text-400 fw-bold text-danger text-nowrap">{{ priceFormat($monthlySales )}}</h4>
</div>