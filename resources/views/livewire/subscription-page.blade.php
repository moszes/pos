<div class="shadow rounded">
  <livewire:breadcrumb />
  <div class="card">
    <div class="card-body">
      <div class="col-12">
        <div class="row align-items-end justify-content-between pb-5">
          <livewire:subscription-form/>
          <div class="col-3">
            <h3>{{ $titlePage }}</h3>
          </div>
          <div class="col-3">
            <input wire:model.debounce.200ms="search" class="form-control search-input search form-control-sm"
                   type="search" placeholder="Search" aria-label="Search">
          </div>
        </div>
        <div class="table-responsive scrollbar">
          <livewire:subscription-table/>
        </div>
        <div class="loading d-none" wire:loading.class.remove="d-none"></div>
      </div>
    </div>
  </div>
</div>