<div class="d-flex flex-column gap-3">
	<table class="table table-hover">
		<thead>
		<tr>
			<th class="px-3">Code</th>
			<th class="px-3">Name</th>
			<th class="px-3 text-end">Saldo</th>
		</tr>
		</thead>
		<tbody>
		@forelse($redemptionLists as $key => $redemptionList)
			<tr>
				<td class="px-3">{{ @$redemptionList?->code }}</td>
				<td class="px-3">{{ @$redemptionList?->name }}</td>
				<td class="px-3  text-end">
					{{ countFormat(@$redemptionList?->redemption_left) }}
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="3" class="text-center">Empty Data</td>
			</tr>
		@endforelse
		</tbody>
	</table>
</div>