 <div class="col-12">
  <div class="row align-items-end justify-content-between pb-5">
    <livewire:stock-form />
    <div class="col-3">
      <h3>{{ $titlePage }}</h3>
    </div>
    <div class="col-3">
      <input wire:model.debounce.200ms="search" class="form-control search-input search form-control-sm" type="search" placeholder="Search" aria-label="Search">
    </div>
  </div>
  <div class="table-responsive scrollbar">
    <table class="table table-hover fs--1 mb-0 ">
      <thead>
        <tr>
          <th class="sort px-3" scope="col">Sales Invoice Number</th>
          <th class="sort px-3" scope="col">Appointment Number</th>
          <th class="sort px-3" scope="col">Warehouse</th>
          <th class="sort px-3" scope="col">Module</th>
          <th class="sort px-3" scope="col">Code</th>
          <th class="sort px-3" scope="col">Name</th>
          <th class="sort px-3" scope="col">Stock</th>
          <th class="sort px-3 text-center" scope="col">Created Date</th>
        </tr>
      </thead>
      <tbody class="list">
        @forelse($stocks as $key => $stock)
        <tr>
          <td class="px-3">{{ @getSalesInvoiceById($stock->sales_invoice_id)->sales_invoice_number }}</td>
          <td class="px-3">{{ @getAppointmentById($stock->appointment_id)->appointment_number }}</td>
          <td class="px-3">{{ @$stock->module }}</td>
          <td class="px-3 text-nowrap">
            <a href="{{ route('transaction.product_stocks', $stock->module_id) }}">{{ $stock->module == 'product' ? getProductById($stock->module_id)->code : $stock->module_id }}</a>
          </td>
          <td class="px-3">{{ $stock->module == 'product' ? @getProductById($stock->module_id)->name : $stock->module_id }}</td>
          <td class="px-3 text-center">{{ numberFormat(@$stock->stock_qty) }}</td>
          <td class="px-3 text-nowrap">{{ @$stock->created_at }}</td>
        </tr>
        @empty
        <tr>
          <td colspan="8" class="text-center">Empty Data</td>
        </tr>
        @endforelse
      </tbody>
    </table>
    {{ $stocks->links() }}
  </div>
</div>