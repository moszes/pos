@if( auth()->user()?->can('see report menu') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
<div class="nav-item-wrapper">
  <a class="nav-link dropdown-indicator label-1" id="report-btn" href="#nv-report" role="button" data-bs-toggle="collapse" aria-expanded="false" aria-controls="nv-report">
    <div class="d-flex align-items-center">
      <span class="nav-link-icon">
        <span data-feather="columns"></span>
      </span>
      <span class="nav-link-text">Report</span>
    </div>
  </a>
  <div class="parent-wrapper label-1">
    <ul class="nav collapse parent" data-bs-parent="#navbarVerticalCollapse" id="nv-report">
      <li class="collapsed-nav-item-title d-none">Report</li>
      @if( auth()->user()?->can('see sales report menu') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
      <livewire:sales-report-link />
      @endif
      @if( auth()->user()?->can('see purchase report menu') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
      <livewire:purchase-report-link />
      @endif
    </ul>
  </div>
</div>
@endif

@push('scripts')
  <script>
    Livewire.on('openReportMenu', function(){
      document.getElementById('report-btn').click();
    })
  </script>
@endpush