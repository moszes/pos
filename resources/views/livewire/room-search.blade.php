<div class="d-flex gap-3 mb-3">
  <div class="col">
    <label class="form-label" for="roomId">Room</label>
    <select wire:model.debounce.100ms="roomId" class="form-select" id="roomId" aria-label="Select Room">
      <option selected>-- Choose --</option>
      {!! roomOption() !!}
    </select>
    @error('roomId') <div class="text-danger">{{ $message }}</div> @enderror
  </div>
  <div class="col">
    <label class="form-label" for="roomStart">Start</label>
    <input wire:model.debounce.100ms="roomStart" id="roomStart" class="form-control" type="time">
    @error('roomStart') <div class="text-danger">{{ $message }}</div> @enderror
  </div>
  <div class="col">
    <label class="form-label" for="roomEnd">End</label>
    <input wire:model.debounce.100ms="roomEnd" id="roomEnd" class="form-control" type="time">
    @error('roomEnd')<div class="text-danger">{{ $message }}</div> @enderror
  </div>
</div>

@push('scripts')
<script>
  Livewire.on('roomRequired', () => {
    Swal.fire({
      title: 'Required!',
      text: 'Choose Room',
      icon: 'error',
      confirmButtonText: 'Ok'
    })
  });
  
  Livewire.on('roomStartRequired', () => {
    Swal.fire({
      title: 'Required!',
      text: 'Choose Room Start',
      icon: 'error',
      confirmButtonText: 'Ok'
    })
  });
  
  Livewire.on('roomEndRequired', () => {
    Swal.fire({
      title: 'Required!',
      text: 'Choose Room End',
      icon: 'error',
      confirmButtonText: 'Ok'
    })
  });
</script>

@endpush