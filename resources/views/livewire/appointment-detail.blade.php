<div wire:ignore.self class="modal modal-xl fade" id="appointmentDetail" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ $formTitle }}</h5><button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span class="fas fa-times fs--1"></span></button>
      </div>
      <div class="modal-body">
        <div class="card mb-3">
          <div class="card-body">
            <div class="d-flex mb-1 gap-3">
              <div class="col d-flex flex-column">
                <div class="d-flex justify-content-between">
                  <p class="fw-bold">Date</p>
                  <p>{{ @dateFormat($appointment->appointment_date) }}</p>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="fw-bold">Appointment Number</p>
                  <a href="#">{{ @$appointment->appointment_number }}</a>
                </div>
                <div class="d-flex justify-content-between gap-2">
                  <p class="fw-bold text-nowrap">Note</p>
                  <p class="text-end">{{ @$appointment->note }}</p>
                </div>
              </div>
              <div class="col d-flex flex-column">
                <div class="d-flex justify-content-between">
                  <p class="fw-bold">Customer Name</p>
                  <p>{{ @getCustomerById($appointment->customer_id)['details']['first_name'] }}</p>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="fw-bold">Customer Phone</p>
                  <p>{{ @getCustomerPhone($appointment->customer_id) }}</p>
                </div>
                <div class="d-flex justify-content-between gap-2">
                  <p class="fw-bold text-nowrap">Customer Address</p>
                  <p class="text-end">{{ @getCustomerAddress($appointment->customer_id) }}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="table-responsive scrollbar">
          <h5 class="text-center mb-3">Appointment Items</h5>
          <table class="table table-hover table-bordered fs--1 mb-0 ">
            <thead>
              <tr>
                <th class="sort px-3" scope="col">Code</th>
                <th class="sort px-3" scope="col">Name</th>
                <th class="sort px-3" scope="col">PIC</th>
                <th class="sort px-3" scope="col">Room</th>
                <th class="sort text-center px-3" scope="col">Qty</th>
              </tr>
            </thead>
            <tbody class="list">
              @if(!empty($appointment->items))
              @forelse($appointment->items as $item)
              <tr>
                <td class="px-3">{{ @$item->code }}</td>
                <td class="px-3">{{ @$item->name }}</td>
                <td class="px-3">
                  @if(isset($item->doctor_id))
                  <div>Doctor: {{ @getUserById($item->doctor_id)->name }}</div>
                  @endif
                  @if(isset($item->doctor_assistance_id))
                  <div>Doctor Assistance: {{ @getUserById($item->doctor_assistance_id)->name }}</div>
                  @endif
                  @if(isset($item->therapist_id))
                  <div>Therapist: {{ @getUserById($item->therapist_id)->name }}</div>
                  @endif
                  @if(isset($item->sales_id))
                  <div>Sales: {{ @getUserById($item->sales_id)->name }}</div>
                  @endif
                </td>
                <td class="px-3">
                  @if($item->module == 'treatment' || $item->module == 'marketplace')
                  @if($item->room_id)
                  <div>Room: {{ @getRoomById($item->room_id)->name }}</div>
                  @endif
                  @if($item->room_start)
                  <div>Start: {{ @timeFormat($item->room_start) }}</div>
                  @endif
                  @if($item->room_end)
                  <div>End: {{ @timeFormat($item->room_end) }}</div>
                  @endif
                  @endif
                </td>
                <td class="text-center px-3">{{ numberFormat(@$item->qty) }}</td>
              </tr>
              @empty
              <tr>
                <td colspan="6" class="text-center">Empty Data</td>
              </tr>
              @endforelse
              @endif
            </tbody>
          </table>
          <div class="d-flex justify-content-end my-3">
            <button class="btn btn-primary" wire:click="appointmentPayment('{{@$appointment->id}}')">Payment</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>
  let appointmentDetail = new bootstrap.Modal(
    document.getElementById('appointmentDetail'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showAppointmentItems', () => {
    appointmentDetail.show();
  })
  Livewire.on('hideAppointmentItems', () => {
    appointmentDetail.hide();
  })
  document.getElementById('appointmentDetail').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearAppointmentItems')
  })
</script>
@endpush