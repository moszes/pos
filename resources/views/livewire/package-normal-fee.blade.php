<div class="d-flex flex-column mb-1 gap-3">
	<h5 class="mb-0">Normal Fee</h5>
	<div class="col d-flex flex-column gap-3">
		<div class="d-flex gap-3 justify-content-between">
			<label for="packageNormalFeeSeniorDoctor">Senior Doctor</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">%</span>
					<input wire:model.debounce.500ms="packageNormalFee.senior_doctor" type="number" id="packageNormalFeeSeniorDoctor" class="form-control" aria-describedby="basic-addon1">
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="packageNormalFeeDoctor">Doctor</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">%</span>
					<input wire:model.debounce.500ms="packageNormalFee.doctor" type="number" id="packageNormalFeeDoctor" class="form-control" aria-describedby="basic-addon1">
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="packageNormalFeeDoctorAssistance">Doctor Assistance</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Rp</span>
					<input wire:model.debounce.500ms="packageNormalFee.doctor_assistance" type="number" id="packageNormalFeeDoctorAssistance" class="form-control" >
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="packageNormalFeeTherapist">Therapist</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Rp</span>
					<input wire:model.debounce.500ms="packageNormalFee.therapist" type="number" id="packageNormalFeeTherapist" class="form-control" >
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="packageNormalFeeSales">Sales</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">%</span>
					<input wire:model.debounce.500ms="packageNormalFee.sales" type="number" id="packageNormalFeeSales" class="form-control" >
				</div>
			</div>
		</div>
	</div>
	<div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>