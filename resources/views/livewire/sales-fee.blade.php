<div class="d-flex gap-3">
	<div class="mb-3 col">
		<label class="form-label" for="salesRp">Doctor Fee (Rp)</label>
		<input
			wire:model.debounce.200ms="salesRp"
			wire:click="$set('salesRp', '')"
			wire:blur="$set('salesRp', {{$salesRp === '' ? 0 : $salesRp}})"
			id="salesRp"
			class="form-control"
			type="number"
			@if($salesPercent > 0) disabled @endif
		>
		@error('salesRp') <span class="text-danger">{{ $message }}</span> @enderror
	</div>
	<div class="mb-3 col">
		<label class="form-label" for="salesPercent">Doctor Fee (%)</label>
		<input
			wire:model.debounce.200ms="salesPercent"
			wire:click="$set('salesPercent', '')"
			wire:blur="$set('salesPercent', {{$salesPercent === '' ? 0 : $salesPercent}})"
			id="salesPercent"
			class="form-control"
			type="number"
			@if($salesRp > 0) disabled @endif
		>
		@error('salesPercent') <span class="text-danger">{{ $message }}</span> @enderror
	</div>
</div>