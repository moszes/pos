<div>
  <button class="btn btn-sm btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#roleForm">+</button>
  <div wire:ignore.self class="modal fade" id="roleForm" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5>
          <button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close">
            <span class="fas fa-times fs--1"></span>
          </button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div class="mb-3">
              <label class="form-label" for="role-name">Name</label>
              <input wire:model.lazy="name" id="role-name" class="form-control" type="text">
              @error('name') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="float-end">
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script>

	window.addEventListener("DOMContentLoaded", function () {
		Livewire.emit("openMasterDataModule");
		Livewire.emit("openStakeholdersMenu")
	});

	let roleForm = new bootstrap.Modal(
                        document.getElementById('roleForm'),
                        {
                          backdrop: 'static',
                        }
                      )
  Livewire.on('showRoleForm', () => {
    roleForm.show();
  })
  Livewire.on('hideRoleForm', () => {
    roleForm.hide();
  })
</script>
@endpush