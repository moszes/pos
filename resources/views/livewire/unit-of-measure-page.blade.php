<div class="shadow rounded">
  <livewire:breadcrumb />
  <div class="card">
    <div class="card-body">
      <div class="col-12">
        <livewire:unit-of-measure-table />
      </div>
    </div>
  </div>
</div>