<div class="d-flex gap-3">
  <ul class="list-group w-25">
    <li class="list-group-item" data-bs-toggle="collapse" href="#baseDataModule" role="tab" aria-expanded="false" aria-controls="baseDataModule">Base Data Module</li>
    <li class="list-group-item" data-bs-toggle="collapse" href="#userModule" role="tab" aria-expanded="false" aria-controls="userModule">User Module</li>
    <li class="list-group-item" data-bs-toggle="collapse" href="#employeeModule" role="tab" aria-expanded="false" aria-controls="employeeModule">Employee Module</li>
    <li class="list-group-item" data-bs-toggle="collapse" href="#productModule" role="tab" aria-expanded="false" aria-controls="productModule">Product Module</li>
    <li class="list-group-item" data-bs-toggle="collapse" href="#serviceModule" role="tab" aria-expanded="false" aria-controls="serviceModule">Service Module</li>
    <li class="list-group-item" data-bs-toggle="collapse" href="#reportModule" role="tab" aria-expanded="false" aria-controls="reportModule">Report Module</li>
  </ul>

  <div class="d-flex flex-column gap-3 w-75">

    <div class="collapse" id="baseDataModule">
      <div class="card card-body">
        <object data="{{ asset('/documents/Base Data Module.pdf') }}" type="application/pdf" width="100%" height="500px">
          <p>Unable to display PDF file. <a href="{{ asset('/documents/Base Data Module.pdf') }}">Download</a> instead.</p>
        </object>
      </div>
    </div>
    <div class="collapse" id="userModule">
      <div class="card card-body">
        <object data="{{ asset('/documents/User Module.pdf') }}" type="application/pdf" width="100%" height="500px">
          <p>Unable to display PDF file. <a href="{{ asset('/documents/User Module.pdf') }}">Download</a> instead.</p>
        </object>
      </div>
    </div>

    <div class="collapse" id="employeeModule">
      <div class="card card-body">
        <object data="{{ asset('/documents/Employee Module.pdf') }}" type="application/pdf" width="100%" height="500px">
          <p>Unable to display PDF file. <a href="{{ asset('/documents/Employee Module.pdf') }}">Download</a> instead.</p>
        </object>
      </div>
    </div>

    <div class="collapse" id="productModule">
      <div class="card card-body">
        <object data="{{ asset('/documents/Product.pdf') }}" type="application/pdf" width="100%" height="500px">
          <p>Unable to display PDF file. <a href="{{ asset('/documents/Product.pdf') }}">Download</a> instead.</p>
        </object>
      </div>
    </div>

    <div class="collapse" id="serviceModule">
      <div class="card card-body">
        <object data="{{ asset('/documents/Service Module.pdf') }}" type="application/pdf" width="100%" height="500px">
          <p>Unable to display PDF file. <a href="{{ asset('/documents/Service Module.pdf') }}">Download</a> instead.</p>
        </object>
      </div>
    </div>

    <div class="collapse" id="reportModule">
      <div class="card card-body">
        <object data="{{ asset('/documents/Report Module.pdf') }}" type="application/pdf" width="100%" height="500px">
          <p>Unable to display PDF file. <a href="{{ asset('/documents/Report Module.pdf') }}">Download</a> instead.</p>
        </object>
      </div>
    </div>
  </div>
</div>


@push('scripts')
<script>
  // Get all elements with class "list-group-item"
  const listItems = document.querySelectorAll('.list-group-item');

  // Loop through each element and add a click event listener
  listItems.forEach(item => {
    item.addEventListener('click', () => {
      // Find all elements with class "show" and remove it
      const showElements = document.querySelectorAll('.show');
      showElements.forEach(element => {
        element.classList.remove('show');
      });
    });
  });
</script>
@endpush