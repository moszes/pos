<div>
	<button
		class="btn btn-primary btn-sm"
		type="button"
		data-bs-toggle="modal"
		data-bs-target="#settingModuleForm"
	>
		+
	</button>
	<div
		wire:ignore.self class="modal fade"
		id="settingModuleForm"
		tabindex="-1"
		aria-hidden="true"
	>
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ $formTitle }}</h5>
					<button
						class="btn"
						type="button"
						data-bs-dismiss="modal"
						aria-label="Close"
					>
						<span class="fas fa-times fs--1"></span>
					</button>
				</div>
				<div class="modal-body">
					<form wire:submit.prevent="{{ $actionForm }}">
						<div>
							@if (session()->has('message'))
								<div class="alert alert-danger">
									{{ session('message') }}
								</div>
							@endif
						</div>
						<div class="mb-3">
							<label class="form-label" for="name">Name</label>
							<input
								wire:model.lazy="name"
								id="name"
								class="form-control"
								placeholder="Setting Module Name"
								type="text"
							>
							@error('name')
							  <span class="text-danger">{{ $message }}</span>
							@enderror
						</div>
						<div class="mb-3">
							<label class="form-label" for="type">Module</label>
							<select
								wire:model.debounce.200ms="module"
								class="form-select"
								id="module"
								aria-label="Select Module"
							>
								<option value="branch" selected>Branch</option>
								<option value="printer">Printer</option>
								<option value="treatment">Treatment</option>
								<option value="package">Package</option>
								<option value="subscription">Subscription</option>
								<option value="marketplace">Marketplace</option>
							</select>
							@error('module')
							  <span class="text-danger">{{ $message }}</span>
							@enderror
						</div>
						<div class="mb-3">
							<label class="form-label" for="value">Value</label>
							<input
								wire:model.lazy="value"
								id="value"
								class="form-control"
								placeholder="Enter value"
								type="text"
							>
							@error('value')
							  <span class="text-danger">{{ $message }}</span>
							@enderror
						</div>
						<div class="mb-3">
							<label class="form-label" for="redirectUrl">Redirect URL</label>
							<input
								wire:model.lazy="redirectUrl"
								id="redirectUrl"
								class="form-control"
								type="text"
								placeholder="https://example.com"
							>
							@error('redirectUrl')
							  <span class="text-danger">{{ $message }}</span>
							@enderror
						</div>
						<div class="mb-3">
							<label class="form-label" for="status">Status</label>
							<select
								wire:model.debounce.200ms="status"
								class="form-select"
								id="status"
								aria-label="Select Status"
							>
								<option value="Y" selected>Active</option>
								<option value="N">Inactive</option>
							</select>
							@error('status')
							<span class="text-danger">{{ $message }}</span>
							@enderror
						</div>
						<div class="float-end">
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@push('scripts')
	<script>
		let settingModuleForm = new bootstrap.Modal(
			document.getElementById('settingModuleForm'),
			{
				backdrop: 'static',
			}
		)
		Livewire.on('showSettingModuleForm', () => {
			settingModuleForm.show();
		})
		Livewire.on('hideSettingModuleForm', () => {
			settingModuleForm.hide();
		})
		document.getElementById('settingModuleForm')
			.addEventListener('hidden.bs.modal', () => {
        Livewire.emit('clearSettingModuleForm');
      });
	</script>
@endpush