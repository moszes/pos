<div class="shadow rounded">
	<livewire:breadcrumb />
	<div class="card">
		<div class="card-body">
				<livewire:product-table/>
		</div>
	</div>
</div>

@push('scripts')
<script>
  window.addEventListener("DOMContentLoaded", function () {
    Livewire.emit("openMasterDataModule")
    Livewire.emit("openServicesMenu")
    
	  Livewire.on('productHasBeenSold', () => {
			Swal.fire({
				title: 'Already sold!',
				text: 'This product has already been sold.',
				icon: 'error',
        confirmButtonColor: '#3085d6',
				confirmButtonText: 'Ok',
			})
	  })
   
  });
</script>
@endpush