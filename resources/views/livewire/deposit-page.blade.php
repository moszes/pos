<div>
    {{-- Be like water. --}}
</div>

@push('scripts')
	<script>
		window.addEventListener("DOMContentLoaded", function () {
			Livewire.emit("openTransactionModule");
			Livewire.emit("openSalesTransactionMenu")
		});
	</script>
@endpush