<div class="col-12">
	@error('coverImage') <div class="alert alert-danger">{{ $message }}</div>  @enderror
	<div class="card mb-5">
		<div class="card-header hover-actions-trigger position-relative mb-6" style="min-height: 130px; ">
			<div class="bg-holder rounded-top" style="background-image: linear-gradient(0deg, #000000 -3%, rgba(0, 0, 0, 0) 83%), url({{ asset('../../assets/img/generic/59.png') }})">
				<input class="d-none" id="upload-settings-cover-image" type="file">
				<label class="cover-image-file-input" for="upload-settings-cover-image"></label>
				<div class="hover-actions end-0 bottom-0 pe-1 pb-2 text-white">
					<span class="fa-solid fa-camera me-2"></span>
				</div>
			</div>
			<livewire:customer-avatar-file-upload :user="$user" />
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-12">
					<div class="d-flex flex-wrap mb-2 align-items-center">
						<h3 class="me-2">{{ $user?->details->first_name . ' ' . $user?->details->last_name }}
						</h3><span class="fw-normal fs-0">u/{{$user?->name}}</span>
					</div>
					<div class="d-flex flex-column gap-1">
						<div class="text-wrap d-flex justify-content-between">
							<div>Place, Date Of Birth</div>
							<div>
								{{ $user?->details->place_of_birth }}, {{ $user?->details->date_of_birth }}
							</div>
						</div>
						<div class="text-wrap d-flex justify-content-between">
							<div>Email</div>
							<div>
								{{ $user?->details->email }}
							</div>
						</div>
						<div class="text-wrap d-flex justify-content-between">
							<div>Instagram</div>
							<div>
								<div class="text-wrap">{{ $user?->details->instagram }}</div>
							</div>
						</div>
						<div class="text-wrap d-flex justify-content-between">
							<div>Phone</div>
							<div>
								<div class="text-wrap">{{ $user?->details->phone }}</div>
							</div>
						</div>
						<div class="text-wrap d-flex flex-column justify-content-between">
							<div>Address</div>
							<div>
								<div class="text-wrap">{{ $user?->details->address }}</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@push('scripts')
	<script>
		document.addEventListener('livewire:load', function () {
			// Get the value of the "count" property
			let userId = '{{$customerId}}';
			
			if(!userId){
				window.location.href = '{{ route('master.users') }}';
			}
		})
	</script>
@endpush