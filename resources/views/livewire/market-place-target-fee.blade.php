<div class="d-flex flex-column mb-1 gap-3">
	<h5 class="mb-0">Target Fee</h5>
	<div class="col d-flex flex-column gap-3">
		<div class="d-flex gap-3 justify-content-between">
			<label for="marketplaceTargetFeeSeniorDoctor">Senior Doctor</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">%</span>
					<input wire:model.debounce.500ms="marketPlaceTargetFee.senior_doctor" type="number" id="marketplaceTargetFeeSeniorDoctor" class="form-control" aria-describedby="basic-addon1">
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="marketPlaceTargetFeeDoctor">Doctor</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">%</span>
					<input wire:model.debounce.500ms="marketPlaceTargetFee.doctor" type="number" id="marketPlaceTargetFeeDoctor" class="form-control" aria-describedby="basic-addon1">
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="marketPlaceTargetFeeDoctorAssistance">Doctor Assistance</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Rp</span>
					<input wire:model.debounce.500ms="marketPlaceTargetFee.doctor_assistance" type="number" id="marketPlaceTargetFeeDoctorAssistance" class="form-control" >
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="marketPlaceTargetFeeTherapist">Therapist</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Rp</span>
					<input wire:model.debounce.500ms="marketPlaceTargetFee.therapist" type="number" id="marketPlaceTargetFeeTherapist" class="form-control" >
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="marketPlaceTargetFeeSales">Sales</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">%</span>
					<input wire:model.debounce.500ms="marketPlaceTargetFee.sales" type="number" id="marketPlaceTargetFeeSales" class="form-control" >
				</div>
			</div>
		</div>
	</div>
	<div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>