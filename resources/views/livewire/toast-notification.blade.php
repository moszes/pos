<div class="position-fixed top-0 end-0 p-3" style="z-index: 9999" wire:poll.5s="hideToast">
  <div class="toast {{$toastDisplay}}" role="alert" aria-live="assertive" aria-atomic="true">
    <div class="toast-body d-flex justify-content-between">
      <div>
        <span class="{{$toastIcon}}"></span>
        {{ $toastMessage }}
      </div>
      <button class="btn ms-2 p-0" type="button" data-bs-dismiss="toast" aria-label="Close">
        <span class="uil uil-times fs-1"></span>
      </button>
    </div>
  </div>
</div>