<div>
	<input class="d-none" wire:model="avatarImage" id="avatarImage" type="file">
	<label class="avatar avatar-4xl status-online feed-avatar-profile cursor-pointer" for="avatarImage">
		<img class="rounded-circle img-thumbnail bg-white shadow-sm" src="{{ $avatar }}" width="200" alt="">
	</label>
	<div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>