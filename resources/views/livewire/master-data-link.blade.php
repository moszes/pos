<div class="nav-item-wrapper">
  @if( auth()->user()?->can('see master data menu') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin'))
  <a class="nav-link dropdown-indicator label-1" id="masterdata-btn" href="#nv-masterdata" role="button" data-bs-toggle="collapse" aria-expanded="false" aria-controls="nv-masterdata">
    <div class="d-flex align-items-center">
      <span class="nav-link-icon">
        <span data-feather="file-text"></span>
      </span>
      <span class="nav-link-text">Master Data</span>
    </div>
  </a>
  @endif

  <div class="parent-wrapper label-1">
    <ul class="nav collapse parent" data-bs-parent="#navbarVerticalCollapse" id="nv-masterdata">
      <li class="collapsed-nav-item-title d-none">Master Data</li>

      @if( auth()->user()?->can('see base data link') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales') )
      <livewire:base-data-link />
      @endif

      @if( auth()->user()?->can('see user menu') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales') )
      <livewire:stake-holder-link />
      @endif

      @if( auth()->user()?->can('see service menu') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales') )
      <livewire:service-link />
      @endif

      @if( auth()->user()?->can('see company menu') || auth()->user()?->hasanyrole('godadmin|business owner|manager|admin|sales') )
      <li class="nav-item">
        <a class="nav-link dropdown-indicator" id="companies-btn" href="#nv-companies" data-bs-toggle="collapse" aria-expanded="false" aria-controls="nv-companies">
          <div class="d-flex align-items-center">
            <div class="dropdown-indicator-icon">
              <span class="fas fa-caret-right"></span>
            </div>
            <span class="nav-link-text">Company</span>
          </div>
        </a>
        <!-- more inner pages-->
        <div class="parent-wrapper">
          <ul class="nav collapse parent" data-bs-parent="#forms" id="nv-companies">
            <li class="nav-item">
              <a class="nav-link @if(Route::currentRouteName() == 'master.branches') active @endif" href="{{ route('master.branches') }}" aria-expanded="false">
                <div class="d-flex align-items-center">
                  <span class="nav-link-text">Branch</span>
                </div>
              </a>
              <!-- more inner pages-->
            </li>
            <li class="nav-item">
              <a class="nav-link @if(Route::currentRouteName() == 'master.turnovers') active @endif" href="{{ route('master.turnovers') }}" aria-expanded="false">
                <div class="d-flex align-items-center">
                  <span class="nav-link-text">Turn Over</span>
                </div>
              </a>
              <!-- more inner pages-->
            </li>
          </ul>
        </div>
      </li>
      @endif

    </ul>
  </div>
</div>

@push('scripts')
<script>
  Livewire.on('openMasterDataModule', function(){
    document.getElementById('masterdata-btn').click();
  })
  Livewire.on('openBaseDataMenu', function(){
    document.getElementById('basedata-btn').click();
  })
  if(document.getElementById('stakeholders-btn')){
    Livewire.on('openStakeholdersMenu', function(){
      document.getElementById('stakeholders-btn').click();
    })
  }
  Livewire.on('openServicesMenu', function(){
    document.getElementById('services-btn').click();
  })
  Livewire.on('openCompaniesMenu', function(){
    document.getElementById('companies-btn').click();
  })
</script>
@endpush