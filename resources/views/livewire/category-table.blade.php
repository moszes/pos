<div class="d-flex flex-column gap-4">
  @error('importCategory') <div class="text-danger">{{ $message }}</div> @enderror
  <div class="row">
    <div class="col">
      <h4>{{ $titlePage }}</h4>
    </div>
    <div class="col-3 d-flex gap-2">
      <form wire:submit.prevent="importCategory" enctype="multipart/form-data">
        <input
          type="file"
          id="import-file"
          style="display: none"
          wire:model="importCategory"
        >
        <label
          for="import-file"
          class="mb-0 pointer btn btn-sm btn-success d-flex pointer"
        >
          <i class="fa fa-file-import me-2"></i> <span>Import</span>
        </label>
      </form>
      <input
        wire:model.debounce.200ms="search"
        class="form-control search-input search form-control-sm"
        type="search"
        placeholder="Search"
        aria-label="Search"
      >
      <livewire:category-form/>
    </div>
  </div>
  <table class="table table-hover fs--1 mb-0 ">
    <thead>
    <tr>
      <th class="sort ps-2" scope="col">Code</th>
      <th class="sort" scope="col">Name</th>
      <th class="sort pe-2" scope="col">Type</th>
      <th class="sort text-end pe-2" scope="col">Action</th>
    </tr>
    </thead>
    <tbody class="list">
    @forelse($categories as $key => $category)
      <tr>
        <td class="ps-2">{{ $category->code }}</td>
        <td>{{ $category->name }}</td>
        <td>{{ $category->type }}</td>
        <td class="text-end pe-2">
          <a class="me-2 pointer" wire:click="$emit('editCategory', '{{$category->id}}')">
            <i class="fa fa-pencil"></i>
          </a>
          <a class="text-danger pointer" wire:click="$emit('deleteCategory', '{{$category->id}}')">
            <i class="fa fa-remove"></i>
          </a>
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="5" class="text-center">Empty Data</td>
      </tr>
    @endforelse
    </tbody>
  </table>
  {{ $categories->links() }}
</div>
 
@push('scripts')
  <script>
  Livewire.on('deleteCategory', function(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
    }).then((result) => {
      if (result.isConfirmed) {
        Livewire.emit('destroy', id);
      }
    });
  });
  </script>
@endpush