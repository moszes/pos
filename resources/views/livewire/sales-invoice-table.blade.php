<div class="card">
  <div class="card-body">
    <div class="row pb-5">
      <livewire:sales-invoice-form />
      <div class="col-4">
        <h3>{{ $titlePage }}</h3>
      </div>
      <div class="col-4 d-flex gap-1 justify-content-between">
        <div class="w-100">
          <input type="date" id="date" class="form-control form-control-sm" wire:model="date" placeholder="Date">
        </div>
        <div class="w-100">
          <input wire:model.debounce.200ms="search" class="form-control search-input search form-control-sm" type="search" placeholder="Search" aria-label="Search">
        </div>
      </div>
    </div>
    <livewire:sales-invoice-detail />
    <div class="table-responsive scrollbar">
      <table class="table table-hover fs--1 mb-0 ">
        <thead>
        <tr>
          <th class="sort px-3" scope="col">Invoice Number</th>
          <th class="sort px-3" scope="col">Customer</th>
          <th class="sort px-3 text-end" scope="col">Discount</th>
          <th class="sort px-3 text-end" scope="col">Grand Total</th>
          <th class="sort px-3 text-end" scope="col">Payment</th>
          <th class="sort px-3 text-center" scope="col">Action</th>
        </tr>
        </thead>
        <tbody class="list">
        @forelse($sales_invoices as $key => $sales_invoice)
          <tr>
            <td wire:click="$emit('showSalesInvoiceDetails', '{{$sales_invoice->id}}')" class="px-3 pointer">{{ $sales_invoice->sales_invoice_number }}</td>
            <td class="px-3">
              <div>
                FirstName : <a href="{{ route('customer.data_redemption', ['customerId' => $sales_invoice->customer_id]) }}">
                  {{ @getCustomerById($sales_invoice->customer_id)['details']['first_name'] }}
                </a>
              </div>
              <div>Phone : {{ @getCustomerPhone($sales_invoice->customer_id) }}</div>
            </td>
            <td wire:click="$emit('showSalesInvoiceDetails', '{{$sales_invoice->id}}')" class="px-3 pointer text-end">{{ @numberFormat($sales_invoice->discount) }}</td>
            <td wire:click="$emit('showSalesInvoiceDetails', '{{$sales_invoice->id}}')" class="px-3 pointer text-end">{{ @numberFormat($sales_invoice->grand_total) }}</td>
            <td wire:click="$emit('showSalesInvoiceDetails', '{{$sales_invoice->id}}')" class="px-3 pointer text-end">{{ @numberFormat($sales_invoice->payments->sum('amount')) }}</td>
            <td class="text-center px-3">
              @if(!$sales_invoice->release_by)
                <a class="me-2 pointer" wire:click="$emit('editSalesInvoice', '{{$sales_invoice->id}}')"><i class="fa fa-pencil"></i></a>
                <a class="text-danger pointer" wire:click="$emit('deleteButtonClicked', 'SalesInvoice', '{{$sales_invoice->id}}')"><i class="fa fa-remove"></i></a>
              @else
                <a href="http://clinic.local/print/{{$sales_invoice->id}}"  class="text-gray-400">
                  <i class="fa fa-print"></i>
                </a>
              @endif
            </td>
          </tr>
        @empty
          <tr>
            <td colspan="10" class="text-center">Empty Data</td>
          </tr>
        @endforelse
        </tbody>
      </table>
      {{ $sales_invoices->links() }}
    </div>
    <div class="loading d-none" wire:loading.class.remove="d-none"></div>
  </div>
</div>