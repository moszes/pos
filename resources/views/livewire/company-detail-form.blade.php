<div class="card w-50 p-5">
	
	@error('errorMessage')
	<div class="alert alert-danger" role="alert">
		{{ $message }}
	</div>
	@enderror
	
	<form wire:submit.prevent="updateCompany" class="d-flex flex-column gap-3">

		@if(!request('token'))
			<div class="form-group">
				<label class="form-label" for="token">Token</label>
				<input class="form-control" wire:model="token" id="token" type="text" placeholder="Enter token here"/>
				@error('token')
				<div class="text-danger">{{ $message }}</div>@enderror
			</div>
		@else
			<div class="form-group">
				<label class="form-label" for="email">Company Email</label>
				<input class="form-control" wire:model="email" id="email" type="text" placeholder="pt@example.com" />
				@error('email')<div class="text-danger">{{ $message }}</div>@enderror
			</div>
			
			<div class="form-group">
				<label class="form-label" for="name">Company Name</label>
				<input class="form-control" wire:model="name" id="name" type="text" placeholder="PT Example" />
				@error('name')<div class="text-danger">{{ $message }}</div>@enderror
			</div>
			
			<div class="form-group">
				<label class="form-label" for="phone">Company Phone</label>
				<input class="form-control" wire:model="phone" id="phone" type="text" placeholder="+628123456789" />
				@error('phone')<div class="text-danger">{{ $message }}</div>@enderror
			</div>
		
			<div class="form-group">
				<label class="form-label" for="token">Token</label>
				<input class="form-control" wire:model="token" id="token" type="text" placeholder="Enter token here"/>
				@error('token')
				<div class="text-danger">{{ $message }}</div>@enderror
			</div>
		@endif
		<button class="btn btn-primary" type="submit">Submit</button>
	</form>
</div>