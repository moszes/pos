<div class="d-flex flex-column mb-1 gap-3">
	<h5 class="mb-0">Discount Fee</h5>
	<div class="col d-flex flex-column gap-3">
		<div class="d-flex gap-3 justify-content-between">
			<label for="marketPlaceDiscountFeeSeniorDoctor">Senior Doctor</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">%</span>
					<input wire:model.debounce.500ms="marketPlaceDiscountFee.senior_doctor" type="number" id="marketPlaceDiscountFeeSeniorDoctor" class="form-control" aria-describedby="basic-addon1">
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="marketPlaceDiscountFeeDoctor">Doctor</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">%</span>
					<input wire:model.debounce.500ms="marketPlaceDiscountFee.doctor" type="number" id="marketPlaceDiscountFeeDoctor" class="form-control" aria-describedby="basic-addon1">
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="marketPlaceDiscountFeeDoctorAssistance">Doctor Assistance</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Rp</span>
					<input wire:model.debounce.500ms="marketPlaceDiscountFee.doctor_assistance" type="number" id="marketPlaceDiscountFeeDoctorAssistance" class="form-control" >
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="marketPlaceDiscountFeeTherapist">Therapist</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Rp</span>
					<input wire:model.debounce.500ms="marketPlaceDiscountFee.therapist" type="number" id="marketPlaceDiscountFeeTherapist" class="form-control" >
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="marketPlaceDiscountFeeSales">Sales</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">%</span>
					<input wire:model.debounce.500ms="marketPlaceDiscountFee.sales" type="number" id="marketPlaceDiscountFeeSales" class="form-control" >
				</div>
			</div>
		</div>
	</div>
	<div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>