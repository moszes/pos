 <div class="col-12">
  <div class="row align-items-end justify-content-between pb-5">
    <div class="col-6">
      <h3>{{ $titlePage }}</h3>
    </div>
    <div class="col-6">
      <input wire:model.debounce.200ms="search" class="form-control search-input search form-control-sm" type="search" placeholder="Search" aria-label="Search">
    </div>
  </div>
  <livewire:appointment-detail />
  <div class="table-responsive scrollbar">
    <table class="table table-hover fs--1 mb-0 ">
      <thead>
        <tr>
          <th class="sort px-3" scope="col">Appointment Number</th>
          <th class="sort px-3" scope="col">Customer</th>
          <th class="sort px-3" scope="col">Phone</th>
          <th class="sort text-center px-3" scope="col">Action</th>
        </tr>
      </thead>
      <tbody class="list">
        @forelse($appointments as $key => $appointment)
        <tr>
          <td wire:click="$emit('showAppointmentItems', '{{$appointment->id}}')" class="px-3 pointer">{{ $appointment->appointment_number }}</td>
          <td wire:click="$emit('showAppointmentItems', '{{$appointment->id}}')" class="px-3 pointer">{{ @getCustomerById($appointment->customer_id)['details']['first_name'] }}</td>
          <td wire:click="$emit('showAppointmentItems', '{{$appointment->id}}')" class="px-3 pointer">{{ @getCustomerById($appointment->customer_id)['details']['phone'] }}</td>
          <td class="text-center px-3">
            <a class="me-2 pointer" wire:click="$emit('editAppointment', '{{$appointment->id}}')"><i class="fa fa-pencil"></i></a>
            <a class="text-danger pointer" wire:click="$emit('deleteAppointment', '{{$appointment->id}}')"><i class="fa fa-remove"></i></a>
          </td>
        </tr>
        @empty
        <tr>
          <td colspan="7" class="text-center">Empty Data</td>
        </tr>
        @endforelse
      </tbody>
    </table>
    {{ $appointments->links() }}
  </div>
</div>
 
 @push('scripts')
   <script>
     Livewire.on('deleteAppointment', (id) => {
       Swal.fire({
         title: "Are you sure?",
         icon: "warning",
         showCancelButton: true,
         confirmButtonColor: "#3085d6",
         cancelButtonColor: "#d33",
         confirmButtonText: "Yes, delete it!"
       }).then((result) => {
         if (result.isConfirmed) {
           Livewire.emit('destroy', id);
         }
       });
     })
   </script>
 @endpush