<div class="shadow rounded">
  <livewire:breadcrumb />
  <livewire:sales-invoice-table />
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>