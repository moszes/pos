<div class="d-flex flex-column gap-4">
	<h5>Material Usage</h5>
	<div class="d-flex flex-column">
		<label for="searchForProduct"></label>
		<input type="text" id="searchForProduct" class="form-control" wire:model.debounce.500ms.debounce.200ms="searchForProduct" placeholder="Search Product...">
		@if($productList)
			<ul class="list-group">
				@forelse($products as $productItem)
					<li wire:click="selectProduct('{{$productItem->id}}')" class="list-group-item pointer" wire:loading.class.remove="disabled" disabled>
						{{ $productItem->code }}, {{ $productItem->name }}, {{ $productItem->normal_price }}
					</li>
				@empty
					<li class="list-group-item">Empty Product</li>
				@endforelse
			</ul>
		@endif
	</div>
	@if($productForm)
	<div class="d-flex justify-content-between gap-3">
		<div class="d-flex flex-column gap-3 col">
			<label for="productCode" class="hidden">Product Code</label>
			<input type="text" wire:model.debounce.500ms="productCode" id="productCode" class="form-control" readonly>
		</div>
		<div class="d-flex flex-column gap-3 col">
			<label for="productName">Product Name</label>
			<input type="text" wire:model.debounce.500ms="productName" id="productName" class="form-control" readonly>
		</div>
		<div class="d-flex flex-column gap-3 col">
			<label for="productQty">Product Qty</label>
			<input type="number" wire:model.debounce.500ms="productQty" id="productQty" class="form-control">
		</div>
		<div class="d-flex flex-column gap-3 col">
			<label for="productUsage">Product Usage</label>
			<input type="number" wire:model.debounce.500ms="productUsage" id="productUsage" class="form-control">
		</div>
		<div class="d-flex flex-column gap-3 col">
			<label for="productCost">Product Cost</label>
			<input type="number" wire:model.debounce.500ms="productCost" id="productCost" class="form-control">
		</div>
	</div>
	<div class="text-end">
		<button class="btn btn-primary" wire:click="addMaterialUsage">+</button>
	</div>
	@endif
	<div class="table-responsive scrollbar">
		<h5 class="mb-5 mt-2 text-center">Material Usage Item</h5>
		<table class="table table-hover fs--1 mb-0 ">
			<thead>
			<tr>
				<th class="sort " scope="col"></th>
				<th class="sort " scope="col">Item</th>
				<th class="sort text-end" scope="col">Attribute</th>
				<th class="sort text-end " scope="col">Price</th>
			</tr>
			</thead>
			<tbody class="list">
				@forelse($materialUsages as $key => $materialUsage)
					<tr>
						<td class="text-end" style="width:20px">
							<button class="btn btn-sm" wire:click="removeItem('{{ $key }}')">
								<i class="fa fa-trash text-danger"></i>
							</button>
						</td>
						<td>
							<div class="d-flex justify-content-between">
								<div>Code</div>
								<div class="fw-bold">{{ getProductById($materialUsage['product_id'])->code }}</div>
							</div>
							<div class="d-flex justify-content-between">
								<div>Name</div>
								<div>{{ getProductById($materialUsage['product_id'])->name }}</div>
							</div>
						</td>
						<td class="text-end">
							<div class="d-flex justify-content-between">
								<div>Qty</div>
								<div></div>{{ $materialUsage['qty'] }}
							</div>
							<div class="d-flex justify-content-between">
								<div>Size</div>
								<div>{{ $materialUsage['size'] }}</div>
							</div>
							<div class="d-flex justify-content-between">
								<div>Usage</div>
								<div>{{ $materialUsage['usage'] }}</div>
							</div>
						</td>
						<td class="text-end">
							<div class="d-flex justify-content-between">
								<div>Normal</div>
								<div>{{ numberFormat($materialUsage['normal_price']) }}</div>
							</div>
							<div class="d-flex justify-content-between">
								<div>Agent</div>
								<div>{{ numberFormat($materialUsage['agent_price']) }}</div>
							</div>
							<div class="d-flex justify-content-between">
								<div>Cost</div>
								<div>{{ numberFormat($materialUsage['cost']) }}</div>
							</div>
						</td>
					</tr>
				@empty
					<tr>
						<td colspan="7" class="text-center">Empty Data</td>
					</tr>
				@endforelse
				<tr>
					<td class=" text-end" colspan="3">Grand Total</td>
					<td class=" text-end">{{ numberFormat($grandTotal) }}</td>
				</tr>
			</tbody>
		</table>
		<div class="d-flex justify-content-end gap-3 mt-3">
			<button class="btn btn-primary" wire:click="update">Save</button>
		</div>
	</div>
</div>