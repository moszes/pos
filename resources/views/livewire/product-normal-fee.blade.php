<div class="d-flex flex-column mb-1 gap-3">
	<h5 class="mb-0">Normal Fee</h5>
	<div class="col d-flex flex-column gap-3">
		<div class="d-flex gap-3 justify-content-between">
			<label for="productNormalFeeSeniorDoctor">Senior Doctor</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">%</span>
					<input wire:model.debounce.500ms="productNormalFee.senior_doctor" type="number" id="productNormalFeeSeniorDoctor" class="form-control" aria-describedby="basic-addon1">
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="productNormalFeeDoctor">Doctor</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">%</span>
					<input wire:model.debounce.500ms="productNormalFee.doctor" type="number" id="productNormalFeeDoctor" class="form-control" aria-describedby="basic-addon1">
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="productNormalFeeDoctorAssistance">Doctor Assistance</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Rp</span>
					<input wire:model.debounce.500ms="productNormalFee.doctor_assistance" type="number" id="productNormalFeeDoctorAssistance" class="form-control" >
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="productNormalFeeTherapist">Therapist</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Rp</span>
					<input wire:model.debounce.500ms="productNormalFee.therapist" type="number" id="productNormalFeeTherapist" class="form-control" >
				</div>
			</div>
		</div>
		<div class="d-flex gap-3 justify-content-between">
			<label for="productNormalFeeSales">Sales</label>
			<div>
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">%</span>
					<input wire:model.debounce.500ms="productNormalFee.sales" type="number" id="productNormalFeeSales" class="form-control" >
				</div>
			</div>
		</div>
	</div>
	<div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>