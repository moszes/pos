 <div class="col-12">
  <div class="row align-items-end justify-content-between pb-5">
    <livewire:purchase-invoice-form />
    <div class="col-3">
      <h3>{{ $titlePage }}</h3>
    </div>
    <div class="col-3">
      <input wire:model.debounce.200ms="search" class="form-control search-input search form-control-sm" type="search" placeholder="Search" aria-label="Search">
    </div>
  </div>
  <div class="table-responsive scrollbar">
    <table class="table table-hover fs--1 mb-0 ">
      <thead>
        <tr>
          <th class="sort px-3" scope="col">No</th>
          <th class="sort px-3" scope="col">PO Number</th>
          <th class="sort px-3" scope="col">Vendor</th>
          <th class="sort px-3" scope="col">Submitted</th>
          <th class="sort px-3 text-nowrap" scope="col">Approved</th>
          <th class="sort px-3 text-end" scope="col">tax</th>
          <th class="sort px-3 text-end" scope="col">others</th>
          <th class="sort px-3 text-end" scope="col">discount</th>
          <th class="sort px-3 text-end" scope="col">Sub Total</th>
          <th class="sort px-3 text-end" scope="col">Grand Total</th>
          <th class="sort px-3 text-center" scope="col">Action</th>
        </tr>
      </thead>
      <tbody class="list">
        @forelse($purchase_invoices as $key => $purchase_invoice)
        <tr>
          <td wire:click="$emit('showPurchaseInvoiceItems', '{{$purchase_invoice->id}}')" class="px-3 pointer">{{ $purchase_invoice->invoice_number }}</td>
          <td wire:click="$emit('showPurchaseInvoiceItems', '{{$purchase_invoice->id}}')" class="px-3 pointer">{{ @getPurchaseOrderById($purchase_invoice->purchase_order_id)->order_number }}</td>
          <td wire:click="$emit('showPurchaseInvoiceItems', '{{$purchase_invoice->id}}')" class="px-3 pointer">{{ @getVendorById($purchase_invoice->vendor_id)->name }}</td>
          <td wire:click="$emit('showPurchaseInvoiceItems', '{{$purchase_invoice->id}}')" class="px-3 pointer">
            <div>{{ @getUserById($purchase_invoice->user_id)->name }}</div>
            <div class="text-nowrap">{{ date('Y-m-d', strtotime($purchase_invoice->date)) }}</div>
          </td>
          <td wire:click="$emit('showPurchaseInvoiceItems', '{{$purchase_invoice->id}}')" class="px-3 pointer">
            <div>{{ @getUserById($purchase_invoice->approved_by)->name }}</div>
            <div class="text-nowrap">{{ $purchase_invoice->approved_date ? $purchase_invoice->approved_date : null}}</div>
          </td>
          <td wire:click="$emit('showPurchaseInvoiceItems', '{{$purchase_invoice->id}}')" class="px-3 pointer text-end">{{ numberFormat($purchase_invoice->tax) }}</td>
          <td wire:click="$emit('showPurchaseInvoiceItems', '{{$purchase_invoice->id}}')" class="px-3 pointer text-end">{{ numberFormat($purchase_invoice->discount) }}</td>
          <td wire:click="$emit('showPurchaseInvoiceItems', '{{$purchase_invoice->id}}')" class="px-3 pointer text-end">{{ numberFormat($purchase_invoice->others) }}</td>
          <td wire:click="$emit('showPurchaseInvoiceItems', '{{$purchase_invoice->id}}')" class="px-3 pointer text-end">{{ numberFormat(getPurchaseInvoiceSubTotal($purchase_invoice->id)) }}</td>
          <td wire:click="$emit('showPurchaseInvoiceItems', '{{$purchase_invoice->id}}')" class="px-3 pointer text-end">{{ numberFormat($purchase_invoice->grand_total) }}</td>
          <td class="text-center">
            @if(!$purchase_invoice->approved_by)
            <a class="me-2 pointer" wire:click="$emit('editPurchaseInvoice', '{{$purchase_invoice->id}}')"><i class="fa fa-pencil"></i></a>
            <a class="text-danger pointer" wire:click="$emit('deletePurchaseInvoice', '{{$purchase_invoice->id}}')">
              <i class="fa fa-remove"></i>
            </a>
            @endif
          </td>
        </tr>
        @empty
        <tr>
          <td colspan="11" class="text-center">Empty Data</td>
        </tr>
        @endforelse
      </tbody>
    </table>
    {{ $purchase_invoices->links() }}
  </div>
</div>