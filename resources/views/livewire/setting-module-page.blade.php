<div class="shadow rounded">
	<livewire:breadcrumb />
	<livewire:toast-notification />
	<div class="card">
		<div class="card-body">
			<livewire:setting-module-table
				:companyCode="$companyCode"
				:branchCode="$branchCode"
			/>
		</div>
	</div>
</div>

@push('scripts')
	<script>
		window.addEventListener("DOMContentLoaded", function () {
			Livewire.emit("openSettingModule");
		});
	</script>
@endpush