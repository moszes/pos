<div>
	<livewire:breadcrumb />
	<div class="row">
		<div class="col-4">
			<livewire:customer-card :user="$user"/>
		</div>
		<div class="col-8">
			<div class="row">
				<livewire:data-redemption-profile-tabs :user="$user"/>
			</div>
		</div>
	</div>
</div>

@push('scripts')
  <script>

	  document.addEventListener('livewire:load', function () {
		  let user = '{{ $user }}'; //assuming you are passing the user from the backend

		  if (!user) {
			  window.location.href = "{{ route('dashboard') }}";
		  }
	  });
  </script>
@endpush