<div class="d-flex flex-column gap-4">
	<div class="row">
		<div class="col">
			<h4>{{ $titlePage }}</h4>
		</div>
		<div class="col-3 d-flex gap-2">
			<input
				wire:model.debounce.200ms="search"
				class="form-control search-input search form-control-sm"
				type="search"
				placeholder="Search"
				aria-label="Search"
			>
			<livewire:setting-module-form
				:companyCode="$companyCode"
				:branchCode="$branchCode"
			/>
		</div>
	</div>
	<table class="table table-hover fs--1 mb-0 ">
		<thead>
		<tr>
			<th class="sort ps-2" scope="col"></th>
			<th class="sort" scope="col">Name</th>
			<th class="sort" scope="col">Module</th>
			<th class="sort pe-2" scope="col">Value</th>
			<th class="sort pe-2" scope="col">Redirect Url</th>
			<th class="sort text-end pe-2" scope="col">Action</th>
		</tr>
		</thead>
		<tbody class="list">
		@forelse($settings as $key => $setting)
			<tr>
				<td class="ps-2">
					<div class="form-check form-switch">
						<input
							class="form-check-input"
							type="checkbox"
							{{ $setting->status == 'Y' ? 'checked' : '' }}
							wire:change="updateStatus({{ $setting }})"
						>
					</div>
				</td>
				<td>{{ $setting->name }}</td>
				<td>{{ $setting->module }}</td>
				<td>{{ $setting->value }}</td>
				<td>{{ $setting->redirect_url }}</td>
				<td class="text-end pe-2">
					<a class="me-2 pointer" wire:click="$emit('editSetting', '{{$setting->id}}')">
						<i class="fa fa-pencil"></i>
					</a>
					<a class="text-danger pointer" wire:click="$emit('deleteSetting', '{{$setting->id}}')">
						<i class="fa fa-remove"></i>
					</a>
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="6" class="text-center">Empty Data</td>
			</tr>
		@endforelse
		</tbody>
	</table>
	{{ $settings->links() }}
	<div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>

@push('scripts')
	<script>
		Livewire.on('deleteSetting', function(id) {
			Swal.fire({
				title: 'Are you sure?',
				text: 'You will not be able to revert this!',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
			}).then((result) => {
				if (result.isConfirmed) {
					Livewire.emit('destroy', id);
				}
			});
		});
	</script>
@endpush