 <div class="col-12">
   @if(auth()->user()?->hasRole('godadmin'))
     <div class="card mb-5 mt-3">
       <div class="card-body">
         <div class="d-flex gap-3">
           <label for="date">Date</label>
           <input type="date" id="date" class="form-control w-25" wire:model="date" placeholder="Date">
         </div>
       </div>
     </div>
   @endif
  <div class="row align-items-end justify-content-between pb-5">
    <div class="col d-flex justify-content-between">
      <h3>{{ $titlePage }}</h3>
      <h4>Total Sales Invoice: {{ priceFormat($totalSalesInvoice) }}</h4>
      <h4>{{ date('Y-m-d') }}</h4>
    </div>
  </div>
  <div class="table-responsive scrollbar">
    <table class="table table-hover fs--1 mb-0 ">
      <thead>
        <tr>
          <th class="sort px-3" scope="col">INV Number</th>
          <th class="sort px-3" scope="col">Customer</th>
          <th class="sort px-3 text-end" scope="col">Cash</th>
          <th class="sort px-3 text-end" scope="col">Credit/Debit</th>
          <th class="sort px-3 text-end" scope="col">Custom</th>
          <th class="sort px-3 text-end" scope="col">Prepaid</th>
          <th class="sort px-3 text-end" scope="col">QR Code</th>
          <th class="sort px-3 text-end" scope="col">Voucher</th>
        </tr>
      </thead>
      <tbody class="list">
        @forelse($salesLists as $key => $salesList)
        <tr>
          <td class="px-3">{{ $salesList->sales_invoice_number }}</td>
          <td class="px-3">{{ getUserById($salesList->customer_id)->details?->first_name }}</td>
          <td class="px-3 text-end">{{ numberFormat($salesList->payments?->where('payment_id', 1)->sum('amount')) }}</td>
          <td class="px-3 text-end">{{ numberFormat($salesList->payments?->where('payment_id', 2)->sum('amount')) }}</td>
          <td class="px-3 text-end">{{ numberFormat($salesList->payments?->where('payment_id', 3)->sum('amount')) }}</td>
          <td class="px-3 text-end">{{ numberFormat($salesList->payments?->where('payment_id', 4)->sum('amount')) }}</td>
          <td class="px-3 text-end">{{ numberFormat($salesList->payments?->where('payment_id', 5)->sum('amount')) }}</td>
          <td class="px-3 text-end">{{ numberFormat($salesList->payments?->where('payment_id', 6)->sum('amount')) }}</td>
        </tr>
        @empty
        <tr>
          <td colspan="11" class="text-center">Empty Data</td>
        </tr>
        @endforelse
        <tr>
          <td class="px-3 text-end" colspan="2"></td>
          <td class="px-3 text-end">{{ numberFormat($sumCash) }}</td>
          <td class="px-3 text-end">{{ numberFormat($sumCreditDebit) }}</td>
          <td class="px-3 text-end">{{ numberFormat($sumCustom) }}</td>
          <td class="px-3 text-end">{{ numberFormat($sumPrepaid) }}</td>
          <td class="px-3 text-end">{{ numberFormat($sumQrcode) }}</td>
          <td class="px-3 text-end">{{ numberFormat($sumVoucher) }}</td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <th class="sort px-3" scope="col">INV Number</th>
          <th class="sort px-3" scope="col">Customer</th>
          <th class="sort px-3 text-end" scope="col">Cash</th>
          <th class="sort px-3 text-end" scope="col">Credit/Debit</th>
          <th class="sort px-3 text-end" scope="col">Custom</th>
          <th class="sort px-3 text-end" scope="col">Prepaid</th>
          <th class="sort px-3 text-end" scope="col">QR Code</th>
          <th class="sort px-3 text-end" scope="col">Voucher</th>
        </tr>
        <tr>
          <th class="text-end">Total Payment</th>
          <th colspan="10" class="text-start">{{ priceFormat($totalPayment) }}</th>
        </tr>
      </tfoot>
    </table>
    <div class="py-4">
      @if(date('Y-m-d') == date('Y-m-d', strtotime($date)))
      <livewire:cash-drawer-form :grandTotal="$grandTotal"/>
      @endif
    </div>
  </div>
</div>