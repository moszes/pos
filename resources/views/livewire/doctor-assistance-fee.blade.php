<div class="d-flex gap-3">
	<div class="mb-3 col">
		<label class="form-label" for="doctorAssistanceRp">Doctor Fee (Rp)</label>
		<input
			wire:model.debounce.200ms="doctorAssistanceRp"
			wire:click="$set('doctorAssistanceRp', '')"
			wire:blur="$set('doctorAssistanceRp', {{$doctorAssistanceRp === '' ? 0 : $doctorAssistanceRp}})"
			id="doctorAssistanceRp"
			class="form-control"
			type="number"
			@if($doctorAssistancePercent > 0) disabled @endif
		>
		@error('doctorAssistanceRp') <span class="text-danger">{{ $message }}</span> @enderror
	</div>
	<div class="mb-3 col">
		<label class="form-label" for="doctorAssistancePercent">Doctor Assistance Fee (%)</label>
		<input
			wire:model.debounce.200ms="doctorAssistancePercent"
			wire:click="$set('doctorAssistancePercent', '')"
			wire:blur="$set('doctorAssistancePercent', {{$doctorAssistancePercent === '' ? 0 : $doctorAssistancePercent}})"
			id="doctorAssistancePercent"
			class="form-control"
			type="number"
			@if($doctorAssistanceRp > 0) disabled @endif
		>
		@error('doctorAssistancePercent') <span class="text-danger">{{ $message }}</span> @enderror
	</div>
</div>