<div class="d-flex gap-3 col">
  <div class="mb-3 col">
    <label class="form-label" for="doctorId">Doctor</label>
    <select
      wire:model.debounce.100ms="doctorId"
      class="form-select"
      id="doctorId"
      aria-label="Select Doctor"
      {{ $docOption ? '' : 'disabled' }}
    >
      <option selected>-- Choose --</option>
      {!! doctorOption() !!}
    </select>
    @error('doctorId') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="mb-3 col">
    <label class="form-label" for="doctorAssistanceId">Assistance</label>
    <select
      wire:model.debounce.100ms="doctorAssistanceId"
      class="form-select"
      id="doctorAssistanceId"
      aria-label="Select Doctor Assistance"
      {{ $docAssOption ? '' : 'disabled' }}
    >
      <option selected>-- Choose --</option>
      {!! doctorAssistanceOption() !!}
    </select>
    @error('doctorAssistanceId') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="mb-3 col">
    <label class="form-label" for="therapistId">Therapist</label>
    <select
      wire:model.debounce.100ms="therapistId"
      class="form-select"
      id="therapistId"
      aria-label="Select Therapist"
      {{ $therapistOption ? '' : 'disabled' }}
    >
      <option selected>-- Choose --</option>
      {!! therapistOption() !!}
    </select>
    @error('therapistId') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
  <div class="mb-3 col">
    <label class="form-label" for="salesId">Sales</label>
    <select
      wire:model.debounce.100ms="salesId"
      class="form-select"
      id="salesId"
      aria-label="Select Sales"
      {{ $salesOption ? '' : 'disabled' }}
    >
      <option selected>-- Choose --</option>
      {!! salesOption() !!}
    </select>
    @error('salesId') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
</div>

@push('scripts')
<script>
  Livewire.on('salesRequired', () => {
    Swal.fire({
      title: 'Required!',
      text: 'Choose Sales',
      icon: 'error',
      confirmButtonText: 'Ok'
    })
  });
</script>
@endpush