<div class="d-flex gap-3">
	<div class="loading d-none" wire:loading.class.remove="d-none"></div>
	<div class="mb-3 col-6">
		<label class="form-label" for="subscriptionSearch">Subscription Search</label>
		<input wire:model.debounce.100ms="subscriptionSearch" wire:click="$emit('clearProductSearch')" id="subscriptionSearch" class="form-control" type="text">
		@error('subscriptionSearch') <span class="text-danger">{{ $message }}</span> @enderror
		@if($subscriptionList)
			<ul class="list-group">
				@forelse($subscriptions as $subscriptionItem)
					<li wire:click="selectSubscription('{{$subscriptionItem->id}}')" class="list-group-item pointer" wire:loading.class.remove="disabled" disabled>
						{{ $subscriptionItem->code }}, {{ $subscriptionItem->name }}, {{ $subscriptionItem->price }}
					</li>
				@empty
					<li class="list-group-item">Empty Product</li>
				@endforelse
			</ul>
		@endif
	</div>
	<div class="mb-3 col">
		<label class="form-label" for="subscriptionPrice">Subscription Price</label>
		<input wire:model.debounce.100ms="subscriptionPrice" id="subscriptionPrice" class="form-control" type="number" disabled>
		@error('subscriptionPrice') <span class="text-danger">{{ $message }}</span> @enderror
	</div>
</div>

@push('scripts')
	<script>
		Livewire.on('subscriptionRequired', () => {
			Swal.fire({
				title: 'Required!',
				text: 'Please select a subscription.',
				icon: 'error',
				confirmButtonText: 'Ok'
			})
		});
	</script>
@endpush