 <div class="col-12">
  <div class="row align-items-end justify-content-between pb-5">
    <livewire:purchase-order-form />
    <div class="col-3">
      <h3>{{ $titlePage }}</h3>
    </div>
    <div class="col-3">
      <input wire:model.debounce.200ms="search" class="form-control search-input search form-control-sm" type="search" placeholder="Search" aria-label="Search">
    </div>
  </div>
  <div class="table-responsive scrollbar">
    <table class="table table-hover fs--1 mb-0 ">
      <thead>
        <tr>
          <th class="sort px-3" scope="col">No</th>
          <th class="sort px-3" scope="col">Date</th>
          <th class="sort px-3" scope="col">Vendor</th>
          <th class="sort px-3" scope="col">Submited</th>
          <th class="sort px-3" scope="col">Approved</th>
          <th class="sort text-end" scope="col">Price</th>
          <th class="sort text-end" scope="col">Sub Total</th>
          <th class="sort text-end" scope="col">Grant Total</th>
          <th class="sort text-center px-3" scope="col">Action</th>
        </tr>
      </thead>
      <tbody class="list">
        @forelse($purchase_orders as $key => $purchase_order)
        <tr>
          <td wire:click="$emit('showPurchaseOrderItems', '{{$purchase_order->id}}')" class="px-3 pointer">{{ $purchase_order->order_number }}</td>
          <td wire:click="$emit('showPurchaseOrderItems', '{{$purchase_order->id}}')" class="px-3 pointer">{{ date('Y-m-d', strtotime($purchase_order->date)) }}</td>
          <td wire:click="$emit('showPurchaseOrderItems', '{{$purchase_order->id}}')" class="px-3 pointer">{{ @getVendorById($purchase_order->vendor_id)->name }}</td>
          <td wire:click="$emit('showPurchaseOrderItems', '{{$purchase_order->id}}')" class="px-3 pointer">
            <div class="d-flex flex-column gap-1">
              <div>By : {{ @getUserById($purchase_order->user_id)->name }}</div>
              <div class="text-nowrap">Date : {{ @$purchase_order->created_at }}</div>
            </div>
          </td>
          <td wire:click="$emit('showPurchaseOrderItems', '{{$purchase_order->id}}')" class="px-3 pointer">
            <div class="d-flex flex-column gap-1">
              @if($purchase_order->approved_date)
              <div>By: {{ @getUserById($purchase_order->approved_by)->name }}</div>
              <div class="text-nowrap">Date: {{ $purchase_order->approved_date ? @date('Y-m-d', strtotime($purchase_order->approved_date)) : null}}</div>
              @endif
            </div>
          </td>
          <td wire:click="$emit('showPurchaseOrderItems', '{{$purchase_order->id}}')" class="px-3 pointer text-end">
            <div class="d-flex flex-column">
              <div class="text-nowrap">Discount: {{ numberFormat($purchase_order->discount) }}</div>
            </div>
            <div class="d-flex flex-column">
              <div>Tax: {{ numberFormat($purchase_order->tax) }}</div>
            </div>
            <div class="d-flex flex-column">
              <div>Others: {{ numberFormat($purchase_order->others) }}</div>
            </div>
          </td>
          <td wire:click="$emit('showPurchaseOrderItems', '{{$purchase_order->id}}')" class="px-3 pointer text-end">{{ numberFormat(getPurchaseOrderSubTotal($purchase_order->id)) }}</td>
          <td wire:click="$emit('showPurchaseOrderItems', '{{$purchase_order->id}}')" class="px-3 pointer text-end">{{ numberFormat($purchase_order->grand_total) }}</td>
          <td class="text-center">
            @if(!$purchase_order->approved_by)
            <a class="me-2 pointer" wire:click="$emit('editPurchaseOrder', '{{$purchase_order->id}}')"><i class="fa fa-pencil"></i></a>
            <a class="text-danger pointer" wire:click="$emit('destroy', '{{$purchase_order->id}}')">
              <i class="fa fa-remove"></i>
            </a>
            @endif
          </td>
        </tr>
        @empty
        <tr>
          <td colspan="10" class="text-center">Empty Data</td>
        </tr>
        @endforelse
      </tbody>
    </table>
    {{ $purchase_orders->links() }}
  </div>
</div>