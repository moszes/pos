<div class="row">
	<div class="col-6">
		<livewire:item-detail
			:redirectRoute="$redirectRoute"
			:priceLabel="$priceLabel"
			:moduleName="$moduleName"
			:moduleLabel="$moduleLabel"
			:item="$package"
		/>
	</div>
	@role('godadmin')
	<div class="col-6">
		<livewire:service-detail-fee :module="$package"/>
	</div>
	@endrole
</div>