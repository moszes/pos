<div class="d-flex flex-column">
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
  <div class="d-flex gap-3">
    <div class="mb-3 col-6">
      <label class="form-label" for="treatmentSearch">Treatment Search</label>
      <input wire:model.debounce.500ms="treatmentSearch" wire:click="$emit('clearTreatmentSearch')" id="treatmentSearch" class="form-control" type="text">
      @error('treatmentSearch') <span class="text-danger">{{ $message }}</span> @enderror
      @if($treatmentList)
        <ul class="list-group">
          @forelse($treatments as $treatmentItem)
            <li wire:click="selectTreatment({{$treatmentItem->id}})" class="list-group-item pointer" wire:loading.class.remove="disabled" disabled>
              {{ $treatmentItem->code }}, {{ $treatmentItem->name }}, {{ numberFormat($treatmentItem->price) }}
            </li>
          @empty
            <li class="list-group-item">Empty Treatment</li>
          @endforelse
        </ul>
      @endif
    </div>
    <div class="mb-3 col">
      <label class="form-label" for="treatmentQty">Treatment Qty</label>
      <input wire:model.debounce.500ms="treatmentQty" id="treatmentQty" class="form-control" type="number">
      @error('treatmentQty') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="mb-3 col">
      <label class="form-label" for="treatmentDiscountItem">Treatment Disc</label>
      <input wire:model.debounce.500ms="treatmentDiscountItem" id="treatmentDiscountItem" wire:click="$emit('discountInActive')" class="form-control" type="number">
      @error('treatmentDiscountItem') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="mb-3 col">
      <label class="form-label" for="treatmentPrice">Treatment Price</label>
      <input wire:model.lazy="treatmentPrice" id="treatmentPrice" class="form-control" type="number" disabled>
      @error('treatmentPrice') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
  </div>
</div>

@push('scripts')
  <script>
    Livewire.on('treatmentRequired', () => {
      Swal.fire({
        title: 'Required!',
        text: 'Choose Treatment First',
        icon: 'error',
        confirmButtonText: 'Ok'
      })
    })
  </script>
@endpush
