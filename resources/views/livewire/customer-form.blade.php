<div class="d-flex">
  <button class="btn btn-sm btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#customerForm">+</button>
  <div wire:ignore.self class="modal fade" id="customerForm" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $formTitle }}</h5><button class="btn p-1" type="button" data-bs-dismiss="modal" aria-label="Close"><span class="fas fa-times fs--1"></span></button>
        </div>
        <div class="modal-body">
          <form wire:submit.prevent="{{ $actionForm }}">
            <div>
              @if (session()->has('message'))
                <div class="alert alert-danger">
                    {{ session('message') }}
                </div>
              @endif
            </div>
            <div class="mb-3">
              <label class="form-label" for="userName">Username</label>
              <input wire:model="userName" id="userName" class="form-control" type="text">
              @error('userName') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="email">Email</label>
              <input wire:model="email" id="email" class="form-control" type="text">
              @error('email') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="firstName">First Name</label>
              <input wire:model="firstName" id="firstName" class="form-control" type="text">
              @error('firstName') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="lastName">Last Name</label>
              <input wire:model="lastName" id="lastName" class="form-control" type="text">
              @error('lastName') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
	          <div class="mb-3">
		          <label class="form-label" for="instagram">Instagram</label>
		          <input wire:model="instagram" id="instagram" class="form-control" type="text">
		          @error('instagram') <span class="text-danger">{{ $message }}</span> @enderror
	          </div>
	          <div class="mb-3">
		          <label class="form-label" for="dateOfBirth">Date of Birth</label>
		          <input wire:model="dateOfBirth" id="dateOfBirth" class="form-control" type="date">
		          @error('dateOfBirth') <span class="text-danger">{{ $message }}</span> @enderror
	          </div>
	          <div class="mb-3">
		          <label class="form-label" for="placeOfBirth">Place of Birth</label>
		          <input wire:model="placeOfBirth" id="placeOfBirth" class="form-control" type="text">
		          @error('placeOfBirth') <span class="text-danger">{{ $message }}</span> @enderror
	          </div>
            <div class="mb-3">
              <label class="form-label" for="phone">Phone</label>
              <input wire:model="phone" id="phone" class="form-control" type="text">
              @error('phone') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="mb-3">
              <label class="form-label" for="address">Address</label>
              <textarea wire:model="address" class="form-control form-icon-input" id="address" style="height: 115px;"></textarea>
              @error('address') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="float-end">
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>

@push('scripts')
<script>
  
  let customerForm = new bootstrap.Modal(
    document.getElementById('customerForm'),
    {
      backdrop: 'static',
    }
  )
  Livewire.on('showCustomerForm', () => {
    customerForm.show();
  })
  Livewire.on('hideCustomerForm', () => {
    customerForm.hide();
  })
  document.getElementById('customerForm').addEventListener('hidden.bs.modal', () => {
    Livewire.emit('clearCustomerForm');
  });
</script>
@endpush