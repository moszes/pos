<div class="d-flex flex-column gap-3">
  <livewire:sales-summary-report-form />
  @if($showDocument)
  <livewire:sales-summary-report-table :salesSummaries="$salesSummaries"/>
  @endif
  
  <div class="loading d-none" wire:loading.class.remove="d-none"></div>
</div>

@push('scripts')
  <script>
    window.addEventListener("DOMContentLoaded", function () {
      Livewire.emit("openReportMenu")
      Livewire.emit("openSalesReportModule")
    });
  </script>
@endpush
