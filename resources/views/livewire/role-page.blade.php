<div class="shadow rounded">
  <livewire:breadcrumb />
  <div class="card">
    <div class="card-body">
      <livewire:role-table/>
    </div>
  </div>
</div>