<div class="d-flex gap-3">
	<div class="mb-3 col">
		<label class="form-label" for="therapistRp">Doctor Fee (Rp)</label>
		<input
			wire:model.debounce.200ms="therapistRp"
			wire:click="$set('therapistRp', '')"
			wire:blur="$set('therapistRp', {{$therapistRp === '' ? 0 : $therapistRp}})"
			id="therapistRp"
			class="form-control"
			type="number"
			@if($therapistPercent > 0) disabled @endif
		>
		@error('therapistRp') <span class="text-danger">{{ $message }}</span> @enderror
	</div>
	<div class="mb-3 col">
		<label class="form-label" for="therapistPercent">Doctor Fee (%)</label>
		<input
			wire:model.debounce.200ms="therapistPercent"
			wire:click="$set('therapistPercent', '')"
			wire:blur="$set('therapistPercent', {{$therapistPercent === '' ? 0 : $therapistPercent}})"
			id="therapistPercent"
			class="form-control"
			type="number"
			@if($therapistRp > 0) disabled @endif
		>
		@error('therapistPercent') <span class="text-danger">{{ $message }}</span> @enderror
	</div>
</div>
