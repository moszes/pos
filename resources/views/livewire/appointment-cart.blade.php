<div>
  <div class="d-flex gap-3 justify-between">
    <div class="col">
      <h4 class="text-left mt-5 mb-3">{{ @getCustomerById($customer['id'])['details']['first_name'] }}</h4>
    </div>
    <div class="col">
      <h4 class="text-center mt-5 mb-3">Transaction</h4>
    </div>
    <div class="col">
      <h4 class="text-end mt-5 mb-3">{{ generateAppointmentNumber() }}</h4>
    </div>
  </div>
  <div class="d-flex flex-column">
    <table class="table table-hover table-bordered table-responsive mt-3">
      <thead>
        <tr>
          <th class="px-3 text-center"></th>
          <th class="px-3 text-sm">Code</th>
          <th class="px-3">Name</th>
          <th class="px-3">PIC</th>
          <th class="px-3">Room</th>
          <th class="px-3 text-center">Qty</th>
        </tr>
      </thead>
      <tbody>
      @forelse($items as $key => $item)
        <tr>
          <td class="px-3 text-center">
            <a href="#" wire:click="removeItem({{$key}})"><i class="fas fa-trash"></i></a>
          </td>
          <td class="px-3 text-nowrap">{{ $item['code'] }}</td>
          <td class="px-3">{{ $item['name'] }}</td>
          <td class="px-3">
            @if(isset($item['doctor_id']))
            <div>Doctor: {{ @getUserById($item['doctor_id'])->name }}</div>
            @endif
            @if(isset($item['doctor_assistance_id']))
            <div>Doctor Assistance: {{ @getUserById($item['doctor_assistance_id'])->name }}</div>
            @endif
            @if(isset($item['therapist_id']))
            <div>Therapist: {{ @getUserById($item['therapist_id'])->name }}</div>
            @endif
            @if(isset($item['sales_id']))
            <div>Sales: {{ @getUserById($item['sales_id'])->name }}</div>
            @endif
          </td>
          <td class="px-3">
              @if(isset($item['room_id']))
              <div>{{ @getRoomById($item['room_id'])->name }}</div>
              @endif
              @if(isset($item['room_start']) && $item['room_end'])
              <div>{{ @timeFormat($item['room_start']) }} - {{ @timeFormat($item['room_end']) }}</div>
              @endif
          </td>
          <td class="px-3 text-center">{{ @numberFormat($item['qty']) }}</td>
        </tr>
      @empty
        <tr>
          <td colspan="6" class="text-center">Empty Data</td>
        </tr>
      @endforelse
      </tbody>
    </table>
  </div>
  <div class="float-end mt-5">
    <button type="button" wire:click="$emit('clearAppointmentItems')" class="btn btn-light">Clear</button>
    <button wire:loading.attr="disabled" type="button" wire:click="{{$formAction}}" class="btn btn-primary">Save</button>
  </div>
</div>
