<!DOCTYPE html>
<html lang="en-US" dir="ltr">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- ===============================================-->
  <!--    Document Title-->
  <!-- ===============================================-->
  <title>{{ config('app.env') == 'production' ? config('app.name') : config('app.name') . ' - ' . config('app.env') }}</title>

  <!-- ===============================================-->
  <!--    Favicons-->
  <!-- ===============================================-->
  <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/assets/img/favicons/apple-touch-icon.png') }}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/assets/img/favicons/favicon-32x32.png') }}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/assets/img/favicons/favicon-16x16.png') }}">
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/assets/img/favicons/favicon.ico') }}">
  <meta name="msapplication-TileImage" content="/assets/img/favicons/mstile-150x150.png') }}">
  <meta name="theme-color" content="#ffffff">
  <script src="{{ asset('/assets/vendors/imagesloaded/imagesloaded.pkgd.min.js') }}"></script>
  <script src="{{ asset('/assets/vendors/simplebar/simplebar.min.js') }}"></script>
  <script src="{{ asset('/assets/js/config.js') }}"></script>


  <!-- ===============================================-->
  <!--    Stylesheets-->
  <!-- ===============================================-->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">
  <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&amp;display=swap" rel="stylesheet">
  <link href="{{ asset('/assets/vendors/simplebar/simplebar.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.8/css/line.css">
  <link href="{{ asset('/assets/css/theme-rtl.min.css') }}" type="text/css" rel="stylesheet" id="style-rtl">
  <link href="{{ asset('/assets/css/theme.min.css') }}" type="text/css" rel="stylesheet" id="style-default">
  <link href="{{ asset('/assets/css/user-rtl.min.css') }}" type="text/css" rel="stylesheet" id="user-style-rtl">
  <link href="{{ asset('/assets/css/user.min.css') }}" type="text/css" rel="stylesheet" id="user-style-default">
  <script>
	  let phoenixIsRTL = window.config.config.phoenixIsRTL;
	  if (phoenixIsRTL) {
	    let linkDefault = document.getElementById('style-default');
	    let userLinkDefault = document.getElementById('user-style-default');
	    linkDefault.setAttribute('disabled', 'true');
      userLinkDefault.setAttribute('disabled', 'true');
      document.querySelector('html').setAttribute('dir', 'rtl');
    } else {
	    let linkRTL = document.getElementById('style-rtl');
	    let userLinkRTL = document.getElementById('user-style-rtl');
	    linkRTL.setAttribute('disabled', 'true');
      userLinkRTL.setAttribute('disabled', 'true');
    }
  </script>
  <link href="{{ asset('assets/vendors/leaflet/leaflet.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendors/leaflet.markercluster/MarkerCluster.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendors/leaflet.markercluster/MarkerCluster.Default.css') }}" rel="stylesheet">
  @vite(['resources/css/app.css', 'resources/js/app.js'])
  @stack('styles')
  @livewireStyles
</head>
<body>

<!-- ===============================================-->
<!--    Main Content-->
<!-- ===============================================-->
<main class="main" id="top">
  
  @livewire('navbar-vertical')
  @livewire('navbar-top')
  
  <div class="content pt-12">
    {{ $slot ?? '' }}
  </div>
  <script>
	  let navbarTopStyle = window.config.config.phoenixNavbarTopStyle;
    let navbarTop = document.querySelector('.navbar-top');
    if (navbarTopStyle === 'darker') {
      navbarTop.classList.add('navbar-darker');
    }

    let navbarVerticalStyle = window.config.config.phoenixNavbarVerticalStyle;
    let navbarVertical = document.querySelector('.navbar-vertical');
    if (navbarVertical && navbarVerticalStyle === 'darker') {
      navbarVertical.classList.add('navbar-darker');
    }
  </script>
</main>
<footer>
  <div class="d-flex justify-content-center gap-3 mb-3">
    <p> © Ministry {{ date('Y') }}. All rights reserved. <span>{{ getApplicationVersion() }}</span> </p>
  </div>
</footer>
<!-- ===============================================-->
<!--    End of Main Content-->
<!-- ===============================================-->


<!-- ===============================================-->
<!--    JavaScripts-->
<!-- ===============================================-->
<script src="{{ asset('/assets/vendors/popper/popper.min.js') }}"></script>
<script src="{{ asset('/assets/vendors/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/vendors/anchorjs/anchor.min.js') }}"></script>
<script src="{{ asset('/assets/vendors/is/is.min.js') }}"></script>
<script src="{{ asset('/assets/vendors/fontawesome/all.min.js') }}"></script>
<script src="{{ asset('/assets/vendors/lodash/lodash.min.js') }}"></script>
<script src="{{ asset('/assets/vendors/list.js/list.min.js') }}"></script>
<script src="{{ asset('/assets/vendors/feather-icons/feather.min.js') }}"></script>
<script src="{{ asset('/assets/vendors/dayjs/dayjs.min.js') }}"></script>
<script src="{{ asset('/assets/js/phoenix.js') }}"></script>
<script src="{{ asset('/assets/vendors/echarts/echarts.min.js') }}"></script>
<script src="{{ asset('/assets/vendors/leaflet/leaflet.js') }}"></script>
<script src="{{ asset('/assets/vendors/leaflet.markercluster/leaflet.markercluster.js') }}"></script>
<script src="{{ asset('/assets/vendors/leaflet.tilelayer.colorfilter/leaflet-tilelayer-colorfilter.min.js') }}"></script>
<script src="{{ asset('/assets/js/sweetalert2.all.min.js') }}"></script>
@livewireScripts
<script>
  document.addEventListener('DOMContentLoaded', () => {
    window.addEventListener('keydown', preventFormSubmitOnEnter);
  });

  function preventFormSubmitOnEnter(event) {
    // If the user presses the Enter key, prevent the form from submitting
    if (event.keyCode === 13) {
      event.preventDefault();
    }
  }

  function copyToClipboard(text) {
    navigator.clipboard.writeText(text)
       .then(() => {
      console.log('Text successfully copied to clipboard:', text);
    })
    .catch(err => {
      console.error('Failed to copy text to clipboard:', err);
    });
  }
  
  Livewire.on('redirectToAppointmentMenu', () => {
    window.location.href = '{{ route('transaction.appointments') }}';
  })

  Livewire.on('deleteButtonClicked', function(module, id) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
    }).then((result) => {

    if (result.isConfirmed) {
        Livewire.emit('deleteByModule', module, id);
      
      }
    });
  });
</script>
@stack('scripts')
</body>
</html>